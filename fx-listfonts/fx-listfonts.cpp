#include <swift/sw_main.h>

#include <FltkExt/FXFontFamily.h>

int
sw_main(const SWString &prog, const SWStringArray &args)
 		{
		SWArray<FXFontFamily>	fontlist;

		FXFontFamily::getFamilyList(fontlist);

		for (int i=0;i<(int)fontlist.size();i++)
			{
			FXFontFamily	&ff=fontlist[i];

			printf("%d: %s [", i, ff.name().c_str());

			for (int j=0;j<4;j++)
				{
				if (ff.hasStyle(j))
					{
					printf(" %s (%d)", ff.variantName(j).c_str(), ff.fl_font_face(j));
					}
				}


			SWIntegerArray	fs=ff.getFontSizes();

			if (fs.size() != 0)
				{
				printf(" ] with sizes [");
				for (int j=0;j<(int)fs.size();j++)
					{
					printf("%s%d", j?" ":"", fs[j]);
					}
				}

			printf("]\n");
			}

		return 0;
		}

