/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the implementation of the FXSplitterPane class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "FXSplitterPane.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>


FXSplitterPane::FXSplitterPane(FXSplitterWnd *pOwner) :
	m_pOwner(pOwner),
	m_pWidget(NULL),
	m_currSize(0),
	m_prefSize(0),
	m_minSize(0),
	m_maxSize(0),
	m_offset(0),
	m_fixed(false),
	m_vertical(false)
		{
		}


FXSplitterPane::~FXSplitterPane()
		{
		clear();
		}


void
FXSplitterPane::clear()
		{
		m_currSize = 0;
		m_prefSize = 0;
		m_minSize = 0;
		m_maxSize = 0;
		m_offset = 0;
		m_fixed = false;
		}


int
FXSplitterPane::minSize() const
		{
		int		res=0;

		if (m_pWidget != 0)
			{
			FXSize	sz=fx_getMinSize(m_pWidget);

			res = m_vertical?sz.cy:sz.cx;
			}

		if (m_minSize) res = sw_math_max(res, m_minSize);

		return res;
		}


int
FXSplitterPane::maxSize() const
		{
		int		res=0;

		if (m_pWidget != 0)
			{
			FXSize	sz=fx_getMaxSize(m_pWidget);

			res = m_vertical?sz.cy:sz.cx;
			}

		if (m_maxSize) res = sw_math_max(res, m_maxSize);

		return res;
		}

void
FXSplitterPane::closeWindow()
		{
/*
		if (m_pWidget != NULL && m_pWidget->GetSafeHwnd())
			{
			m_pWidget->CloseWindow();
			}
*/
		}

FXSize
FXSplitterPane::minWidgetSize() const
		{
		FXSize	sz(0, 0);

		if (m_pWidget != 0)
			{
			sz = fx_getMinSize(m_pWidget);
			}

		if (m_minSize)
			{
			if (m_vertical) sz.cy = sw_math_max(sz.cy, m_minSize);
			else sz.cx = sw_math_max(sz.cx, m_minSize);
			}

		return sz;
		}

void
FXSplitterPane::currentSize(int v)
		{
		m_currSize = v;
		}