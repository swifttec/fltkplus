#include "FXColorDialog.h"
#include "FXTextButton.h"
#include "FXStaticText.h"
#include "FXInputString.h"


static FXColor	colors[] =
	{
	"#ff0000",
	"#ff4000",
	"#ff8000",
	"#ffbf00",
	"#ffff00",
	"#bfff00",
	"#80ff00",
	"#40ff00",
	"#00ff00",
	"#00ff40",
	"#00ff80",
	"#00ffbf",
	"#00ffff",
	"#00bfff",
	"#0080ff",
	"#0040ff",
	"#0000ff",
	"#4000ff",
	"#8000ff",
	"#bf00ff",
	"#ff00ff",
	"#ff00bf",
	"#ff0080",
	"#ff0040",

	"#fff",
	"#ddd",
	"#bbb",
	"#888",
	"#444",
	"#000",
	
	// Last one
	FX_COLOR_NONE
	};

#define SWATCH_SIZE	32
#define NCOLS		5

FXColorDialog::FXColorDialog(const FXColor &color, const SWString &stitle, Fl_Widget *pOwner) :
	FXDialog(stitle, 400, 250, pOwner),
	m_color(color)
		{
		end();

		int		xpos=10, ypos=10;
		FXFont	font;

		for (int i=0;colors[i] != FX_COLOR_NONE;i++)
			{
			FXTextButton	*p=new FXTextButton("", font, xpos, ypos, SWATCH_SIZE, SWATCH_SIZE, "#000", colors[i], "#000");

			p->setBackgroundColor(colors[i]);

			addItem(200+i, p);
			xpos += SWATCH_SIZE + 2;
			
			if ((i % NCOLS) == (NCOLS-1))
				{
				ypos += SWATCH_SIZE+2;
				xpos = 10;
				}
			}

		xpos = 20 + (NCOLS * (SWATCH_SIZE + 2)) - 2;

		int	width=w()-xpos-10;
		int	height=width * 2 / 3;

		addItem(100,			m_pChooser = new Fl_Color_Chooser(xpos, 10, width, height));

		ypos = height + 20;

		addItem(0, new FXStaticText(w()-210, ypos, 130, 24, "Colour name:", FX_VALIGN_CENTER | FX_HALIGN_RIGHT, "#000"));
		addItem(101, new FXInputString(w()-70, ypos, 60, 24));
		m_pChooser->rgb((double)m_color.red() / 255.0, (double)m_color.green() / 255.0, (double)m_color.blue() / 255.0);
		m_pChooser->mode(1);

		ypos += 28;
		addItem(0, new FXStaticText(w()-210, ypos, 130, 24, "Selected Colour:", FX_VALIGN_CENTER | FX_HALIGN_RIGHT, "#000"));
		addItem(102, new FXStaticText(w()-70, ypos, 60, 24, ""));
		((FXStaticText *)getItem(102))->setBorder(1, "#000", 1, 0);

		addButton(FX_IDOK,		"OK",		1, w()-180, h()-34, 80, 24);
		addButton(FX_IDCANCEL,	"Cancel",	0, w()-90, h()-34, 80, 24);
		}


FXColorDialog::~FXColorDialog()
		{
		}



void
FXColorDialog::onOK()
		{
		updateData();
		bool	ok=true;


		FXDialog::onOK();
		}


FXColor
FXColorDialog::getColor()
		{
		return m_color;
		}


void
FXColorDialog::doDataExchange(FXDataExchange &ddx)
		{
		if (!ddx.isSaving())
			{
			m_pChooser->rgb((double)m_color.red() / 255.0, (double)m_color.green() / 255.0, (double)m_color.blue() / 255.0);
			m_name = m_color.name();
			}

		ddx.value(101, m_name);
		((FXStaticText *)getItem(102))->setBackgroundColor(m_color);
		}


void
FXColorDialog::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		bool	handled=false, changed=false;

		SW_TRACE("onItemCallback - ID=%d, p=%x\n", pWidget->id(), p);

		updateData();

		switch (pWidget->id())
			{
			case 100: // Picker
				m_color = FXColor((int)(m_pChooser->r() * 255.0), (int)(m_pChooser->g() * 255.0), (int)(m_pChooser->b() * 255.0));
				changed = handled = true;
				break;

			case 101:
				m_color = m_name;
				m_pChooser->rgb((double)m_color.red() / 255.0, (double)m_color.green() / 255.0, (double)m_color.blue() / 255.0);
				((FXStaticText *)getItem(102))->setBackgroundColor(m_color);
				handled = true;
				invalidate();
				break;

			default:
				if (pWidget->id() >= 200)
					{
					int	idx=pWidget->id()-200;

					if (idx >= 0)
						{
						m_color = colors[idx];
						changed = true;
						}

					handled = true;
					}
				break;
			}
		
		if (changed)
			{
			updateData(false);
			invalidate();
			}

		if (!handled) FXDialog::onItemCallback(pWidget, p);
		}