/*
**	FXTimePicker.h	A generic dialog class
*/

#ifndef __FltkExt_FXTimePicker_h__
#define __FltkExt_FXTimePicker_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXTimePicker : public FXWidget
		{
public:
		enum Type
			{
			HoursAndMinutes=0,
			HoursMinutesAndSeconds,
			HoursAndZeroMinutes
			};

public:
		class Field
			{
		public:
			int		type;
			FXRect	rect;
			SWString	value;
			bool	selected;
			};

public:
		FXTimePicker(int X, int Y, int W, int H, int type=0);

		void			set(int h, int m, int s);
		void			set(const SWTime &t);
		void			get(int &h, int &m, int &s);

		int				hour() const	{ return ((m_time / 3600) % 24); }
		int				minute() const	{ return ((m_time / 60) % 60); }
		int				second() const	{ return (m_time % 60); }

protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onFocus();

		/// Called to handle a shortcut
		virtual int		onUnfocus();


protected:
		void			init(int t);
		int				handleKey(int code, const SWString &text, const FXPoint &pt);

protected:
		int					m_type;			///< Selection type 0=HH:MM, 1=HH:MM:SS
		int					m_time;			///< The time in seconds since midnight 0 = 00:00:00
		FXFont				m_font;			///< The font to use
		SWArray<Field>	m_fields;
		FXColor				m_btnColor;
		FXColor				m_arrowColor;
		FXRect				m_rButton;
		FXColor				m_selectedBgColor;
		FXColor				m_selectedFgColor;
		int					m_inputValue;
		int					m_selectedField;
		};


#endif // __FltkExt_FXTimePicker_h__
