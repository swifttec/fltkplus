/*
**	FXCmdUI.h	A generic dialog class
*/

#ifndef __FltkExt_FXCmdUI_h__
#define __FltkExt_FXCmdUI_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>

#include <swift/SWString.h>

class FLTKEXT_DLL_EXPORT FXCmdUI
		{
public:
		FXCmdUI(sw_uint32_t id=0);
		virtual ~FXCmdUI();

		sw_uint32_t	id() const		{ return m_id; }

		void		enabled(bool v)	{ m_enabled = v; }
		bool		enabled() const	{ return m_enabled; }

		void		checked(bool v)	{ m_checked = v; }
		bool		checked() const	{ return m_checked; }

protected:
		sw_uint32_t	m_id;
		bool		m_enabled;
		bool		m_checked;
		};


#endif // __FltkExt_FXCmdUI_h__
