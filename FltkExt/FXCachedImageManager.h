/****************************************************************************
**	FXCachedImage.h	An class to represent a cached image object
****************************************************************************/


#include <FltkExt/FltkExt.h>
#include <FltkExt/FXImage.h>

#include <swift/SWLockableObject.h>

#ifndef __FltkExt_FXCachedImageManager_h__
#define __FltkExt_FXCachedImageManager_h__


#include <FltkExt/FXCachedImage.h>

class FLTKEXT_DLL_EXPORT FXCachedImageManager : public SWLockableObject
		{
public:
		FXCachedImageManager();
		~FXCachedImageManager();

		FXCachedImage *		add(const SWString &filename);
		FXCachedImage *		add(FXCachedImage *pObject);
		void				remove(const SWString &filename);
		void				remove(FXCachedImage *pObject);

		FXCachedImage *		find(const SWString &filename);
		FXCachedImage *		findOrCreate(const SWString &filename);
		
protected:
		FXCachedImage	*m_pHead;
		FXCachedImage	*m_pTail;
		int				m_count;			// the number of imaged
		int				m_nextid;
		int				m_referenced;		// the number of referenced images
		int				m_unreferenced;		// the number of unreferenced images
		};


extern FLTKEXT_DLL_EXPORT FXCachedImageManager	theImageMgr;

#endif // __FltkExt_FXCachedImageManager_h__
