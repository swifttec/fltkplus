/*
**	FXSize.h	A generic 2D size
*/

#ifndef __FltkExt_FXSize_h__
#define __FltkExt_FXSize_h__

#include <FltkExt/FltkExt.h>

class FLTKEXT_DLL_EXPORT FXSize
		{
public:
		FXSize();
		FXSize(int w, int h);

		FXSize &	operator=(const FXSize &other);
		bool		operator==(const FXSize &other);
		bool		operator!=(const FXSize &other);

		void		set(int w, int h);

		void		width(int v);
		int			width() const;

		void		height(int v);
		int			height() const;

public: //MFC Compatability methods
		void		SetSize(int w, int h);

public:
		int		cx;
		int		cy;
		};


class FLTKEXT_DLL_EXPORT FXWidgetSizes
		{
public:
		FXSize	currSize;
		FXSize	minSize;
		FXSize	maxSize;
		};


#endif // __FltkExt_FXSize_h__