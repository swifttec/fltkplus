#include "FXButton.h"
#include "FXGroup.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWFileInfo.h>

FXButton::FXButton(sw_uint32_t bid, const SWString &text, const SWString &image, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &frcolor) :
	FXWidget(X, Y, W, H),
	m_text(text),
	m_selected(false),
	m_styleChanged(false),
	m_imageChanged(false),
	m_lastKeyDown(0),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		id(bid);
		init();

		FXButtonStyle	bs;

		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		setBorder(bs.bdStyle, frcolor, bs.bdWidth, bs.bdCornerSize);
		setButtonImage(image, false);
		}


FXButton::FXButton(const SWString &text, const SWString &image, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &frcolor) :
	FXWidget(X, Y, W, H),
	m_text(text),
	m_selected(false),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		init();

		FXButtonStyle	bs;

		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		setBorder(bs.bdStyle, frcolor, bs.bdWidth, bs.bdCornerSize);
		setButtonImage(image, false);
		}

FXButton::FXButton(sw_uint32_t bid, const SWString &text, const SWString &image, int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_text(text),
	m_selected(false),
	m_styleChanged(false),
	m_imageChanged(false),
	m_lastKeyDown(0),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		id(bid);
		init();
		setButtonImage(image, false);
		}


FXButton::FXButton(const SWString &text, const SWString &image, int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_text(text),
	m_selected(false),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		init();
		setButtonImage(image, false);
		}


FXButton::FXButton(const SWString &text, const SWString &image) :
	m_text(text),
	m_selected(false),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		init();
		setButtonImage(image, false);
		}


FXButton::FXButton(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_selected(false),
	m_hasText(true),
	m_pSharedImage(NULL),
	m_handleKeys(true)
		{
		init();
		}


FXButton::~FXButton()
		{
		if (m_pSharedImage != NULL)
			{
			m_pSharedImage->release();
			m_pSharedImage = NULL;
			}

		for (int i=0;i<MaxStyle;i++)
			{
			FXButtonStyle	&style=m_style[i];

			style.clearImages();
			}
		}

void
FXButton::init()
		{
		FXWidget::init();

		FXButtonStyle	*pStyle;
		FXFont			font=FXFont::getDefaultFont();

		pStyle = &m_style[Disabled];
		pStyle->bdColor = RGB(150, 150, 150);
		pStyle->bdStyle = 1;
		pStyle->bdWidth = 1;
		pStyle->bgColor = RGB(200, 200, 200);
		pStyle->fgColor = RGB(220, 220, 220);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Normal];
		pStyle->bdColor = "#aaa";
		pStyle->bdStyle = 1;
		pStyle->bdWidth = 1;
		pStyle->bgColor = "#ddd";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->bdWidth = 1;
		pStyle->font = font;

		pStyle = &m_style[Focused];
		pStyle->bdColor = "#248";
		pStyle->bdStyle = 1;
		pStyle->bdWidth = 2;
		pStyle->bgColor = "#ddd";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Hover];
		pStyle->bdColor = "#9ce";
		pStyle->bdStyle = 1;
		pStyle->bdWidth = 1;
		pStyle->bgColor = "#def";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Pressed];
		pStyle->bdColor = "#8ac";
		pStyle->bdWidth = 1;
		pStyle->bdStyle = 1;
		pStyle->bgColor = "#cde";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		m_style[SelectedNormal] = m_style[Normal];
		m_style[SelectedDisabled] = m_style[Disabled];
		m_style[SelectedFocused] = m_style[Focused];
		m_style[SelectedHover] = m_style[Hover];
		m_style[SelectedPressed] = m_style[Pressed];

		m_state = 0;
		m_pSharedImage = NULL;
		m_lastDrawnState = -1;
		m_styleChanged = false;
		m_imageChanged = false;
		m_mouseOver = false;
		m_focused = false;
		m_selected = false;

		fl_register_images();
		}


void
FXButton::setBackgroundColor(const FXColor &v)
		{
		m_style[FXButton::Normal].bgColor = m_style[FXButton::SelectedNormal].bgColor = v;
		m_style[FXButton::Focused].bgColor = m_style[FXButton::SelectedFocused].bgColor = v;
		m_style[FXButton::Hover].bgColor = m_style[FXButton::SelectedHover].bgColor = v.toShade(1.1);
		m_style[FXButton::Pressed].bgColor = m_style[FXButton::SelectedPressed].bgColor = v.toShade(0.9);
		m_style[FXButton::Disabled].bgColor = m_style[FXButton::SelectedDisabled].bgColor = v.toGrayscale();
		}


void
FXButton::setBorder(int bdstyle, const FXColor &color, int width, int corner)
		{
		for (int i=0;i<MaxStyle;i++)
			{
			FXButtonStyle	&style=m_style[i];

			switch (i)
				{
				case Normal:
				case SelectedNormal:
				case Focused:
				case SelectedFocused:
					style.bdColor = color;
					break;

				case Hover:
				case SelectedHover:
					style.bdColor = color.toShade(1.1);
					break;

				case Pressed:
				case SelectedPressed:
					style.bdColor = color.toShade(0.9);
					break;

				case Disabled:
				case SelectedDisabled:
					style.bdColor = color.toGrayscale();
					break;
				}
			
			style.bdStyle = bdstyle;
			style.bdWidth = width;
			style.bdCornerSize = corner;
			}
		}


void
FXButton::setFont(const FXFont &font)
		{
		for (int i=0;i<FXButton::MaxStyle;i++)
			{
			m_style[i].font = font;
			}

		m_styleChanged = true;
		}




static char *suffixlist[] =
		{
		"-disabled",
		"-normal",
		"-over",
		"-pressed",
		"-selected-disabled",
		"-selected-normal",
		"-selected-over",
		"-selected-pressed",

		// Last One
		NULL
		};

static char *extlist[] =
		{
		".png",
		".jpg",
		".gif",

		// Last One
		NULL
		};


SWString
FXButton::getButtonImagePath(const SWString &imagefolder, const SWString &imagename)
		{
		SWString	res, filename;

		for (int j=0;extlist[j];j++)
			{
			filename = SWFilename::concatPaths(imagefolder, imagename);
			filename += extlist[j];

			if (SWFileInfo::isFile(filename))
				{
				res = filename;
				break;
				}
			}

		return res;
		}


void
FXButton::setButtonImages(const SWString &imagefolder, const SWString &imagebase, bool redrawflag)
		{
		SWString			imagename, filename;

		for (int i=0;i<MaxStyle;i++)
			{
			switch (i)
				{
				case Normal:
					filename = getButtonImagePath(imagefolder, imagebase + "-normal");
					break;
				
				case Focused:
					filename = getButtonImagePath(imagefolder, imagebase + "-focused");
					break;
				
				case Hover:
					filename = getButtonImagePath(imagefolder, imagebase + "-hover");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-over");
					break;
				
				case Pressed:
					filename = getButtonImagePath(imagefolder, imagebase + "-pressed");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-down");
					break;
				
				case Disabled:
					filename = getButtonImagePath(imagefolder, imagebase + "-disabled");
					break;

				case SelectedNormal:
					filename = getButtonImagePath(imagefolder, imagebase + "-selected-normal");
					break;

				case SelectedFocused:
					filename = getButtonImagePath(imagefolder, imagebase + "-selected-focused");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-focused");
					break;

				case SelectedHover:
					filename = getButtonImagePath(imagefolder, imagebase + "-selected-hover");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-selected-over");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-hover");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-over");
					break;

				case SelectedPressed:
					filename = getButtonImagePath(imagefolder, imagebase + "-selected-pressed");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-pressed");
					break;

				case SelectedDisabled:
					filename = getButtonImagePath(imagefolder, imagebase + "-selected-disabled");
					if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-disabled");
					break;
				}

			if (!SWFileInfo::isFile(filename) && i > SelectedNormal) filename = getButtonImagePath(imagefolder, imagebase + "-selected-normal");
			if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase + "-normal");
			if (!SWFileInfo::isFile(filename)) filename = getButtonImagePath(imagefolder, imagebase);

			if (SWFileInfo::isFile(filename))
				{
				m_style[i].setSharedImage(filename);
				}
			}
		}


void
FXButton::setButtonImage(const SWString &filename, bool redrawflag)
		{
		if (filename != m_sharedImageFilename)
			{
			if (m_pSharedImage != NULL)
				{
				m_pSharedImage->release();
				m_pSharedImage = NULL;
				}

			if (SWFileInfo::isFile(filename))
				{
				m_pSharedImage = Fl_Shared_Image::get(filename);
				}

			if (m_pSharedImage != NULL)
				{
				m_sharedImageFilename = filename;
				}
			else
				{
				m_sharedImageFilename.clear();
				}
		
			m_imageChanged = true;		

			if (redrawflag)
				{
				redraw();
				}
			}
		}


void
FXButton::setButtonBackgroundColor(int state, const FXColor &color, bool redrawflag)
		{
		if (state >= 0 && state < FXButton::MaxStyle)
			{
			m_style[state].bgColor = color;
			m_styleChanged = true;
			}
		
		if (redrawflag)
			{
			redraw();
			}
		}


void
FXButton::setButtonStyle(int state, const FXButtonStyle &style, bool redrawflag)
		{
		if (state >= 0 && state < FXButton::MaxStyle)
			{
			m_style[state] = style;
			m_styleChanged = true;
			}
		
		if (redrawflag)
			{
			redraw();
			}
		}

void
FXButton::getButtonStyle(int state, FXButtonStyle &style) const
		{
		if (state >= 0 && state < 4)
			{
			style = m_style[state];
			}
		}


void
FXButton::select(bool v)
		{
		m_selected = v;
//		redraw();
		if (visible_r()) invalidate();
		}


void
FXButton::setText(const SWString &v)
		{
		m_text = v;
		if (visible_r()) invalidate();
		}

void
FXButton::enable(bool v, bool redrawflag)
		{
		if (v)
			{
			activate();
			if (m_mouseOver)
				{
				m_state = Hover;
				}
			else
				{
				m_state = Normal;
				}
			}
		else
			{
			deactivate();
			m_state = Disabled;
			}
		
		if (redrawflag && visible_r()) invalidate();
		}

int
FXButton::state() const
		{
		int		res=Normal;

		if (disabled()) res = Disabled;
		else res = m_state;

		if (selected() && m_state < 5) res += 5;
		
		return res;
		}


void
FXButton::padding(int v)
		{
		m_padding.left = m_padding.right = m_padding.top = m_padding.bottom = v;
		m_styleChanged = true;
		}


void
FXButton::padding(const FXPadding &v)
		{
		m_padding = v;
		m_styleChanged = true;
		}


void
FXButton::draw()
		{
		int				ds=state();
		FXButtonStyle	&style=m_style[ds];

		if (m_styleChanged || ds != m_lastDrawnState)
			{
			m_lastDrawnState = ds;

			FXWidget::setForegroundColor(style.fgColor);
			FXWidget::setBackgroundColor(style.bgColor);
			FXWidget::setBorder(style.bdStyle, style.bdColor, style.bdWidth, style.bdCornerSize);
			m_styleChanged = false;
			}
		
		if (m_imageChanged)
			{
			for (int i=0;i<MaxStyle;i++)
				{
				m_style[i].clearImages();
				}
			}


		if (getBackgroundColor() == FX_COLOR_NONE || getBackgroundColor() == FX_COLOR_DEFAULT)
			{
			FXGroup	*pGroup=dynamic_cast<FXGroup *>(parent());

			if (pGroup != NULL)
				{
				FXWidget::setBackgroundColor(pGroup->getBackgroundColor());
				}
			}

		FXWidget::draw();
		}


void
FXButton::onDraw()
		{
		int				ds=state();
		FXButtonStyle	&style=m_style[ds];
		int				X=x(), Y=y(), W=w(), H=h();

		X += m_padding.left;
		Y += m_padding.top;
		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;

		int		ypos=0;
		int		fontheight=0;

		fontheight = style.font.height();
		
		Fl_Shared_Image	*psi=style.getSharedImage();
		Fl_Image		*pi=NULL;
		int				ix=0, iy=0, iw=0, ih=0;

		if (psi == NULL) psi = m_pSharedImage;

		if (psi != NULL)
			{
			iw = psi->w();
			ih = psi->h();

			// Adjust image height to match the button
			if (m_hasText) ih = H-fontheight-10;
			else ih = H;
			iw = psi->w() * ih / psi->h();
			ix = X + ((W-iw) / 2);
			iy = Y;

			pi = style.getImage(psi, iw, ih, (ds == Disabled));
			}

		if (pi != NULL)
			{
			pi->draw(ix, iy);

			ypos = ih + 5;
			}

		if (m_hasText)
			{
			FXRect	tr(X, Y+ypos, X+W-1, Y+H-1);

			fx_draw_text(m_text, tr, FX_VALIGN_MIDDLE|FX_HALIGN_CENTRE|FX_SHRINKTOFIT, style.font, m_fgColor);
			}
		}



int
FXButton::onMouseEnter()
		{
		m_mouseOver = true;
		if (m_state != Disabled)
			{
			m_state = Hover;
			redraw();
			}

		return 1;
		}


int
FXButton::onMouseLeave()
		{
		m_mouseOver = false;
		if (m_state != Disabled)
			{
			m_state = Normal;
			redraw();
			}

		return 1;
		}

int
FXButton::onFocus()
		{
		int		res=0;

		if (m_handleKeys)
			{
			res = 1;
			m_focused = true;
			if (m_state != Disabled)
				{
				m_state = Focused;
				redraw();
				}
			}

		return res;
		}


int
FXButton::onUnfocus()
		{
		int		res=0;

		if (m_handleKeys)
			{
			res = 1;
			m_focused = false;
			if (m_state != Disabled)
				{
				m_state = Normal;
				redraw();
				}
			}

		return res;
		}


int
FXButton::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (m_handleKeys && m_focused && (code == ' ' || code == FL_Enter || code == FL_KP_Enter))
			{
			m_lastKeyDown = code;
			SW_TRACE("FXButton::handle key down\n");
			onMouseDown(FL_LEFT_MOUSE, pt);
			res = 1;
			}

		return res;
		}


int
FXButton::onKeyUp(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (m_handleKeys && m_focused && code == m_lastKeyDown)
			{
			m_lastKeyDown = 0;
			SW_TRACE("FXButton::handle key up\n");
			onMouseUp(FL_LEFT_MOUSE, pt);
			res = 1;
			}

		return res;
		}


void
FXButton::addShortcutKey(int v)
		{
		m_shortcutKeys.add(v);
		}


bool
FXButton::handleShortcutKey(int v)
		{
		bool	res=false;

		if (m_handleKeys)
			{
			for (int i=0;i<(int)m_shortcutKeys.size();i++)
				{
				if (m_shortcutKeys[i] == v)
					{
					res = true;
					break;
					}
				}
			}

		return res;
		}


int
FXButton::onShortcut(int code, const FXPoint &pt)
		{
		int		res=0;

		if (handleShortcutKey(code))
			{
			do_callback();
			res = 1;
			}

		return res;
		}

int
FXButton::onMouseDown(int button, const FXPoint &pt)
		{
		if (m_state != Disabled)
			{
			m_state = m_selected?SelectedPressed:Pressed;
			redraw();
			}

		return 1;
		}


int
FXButton::onMouseUp(int button, const FXPoint &pt)
		{
		if (m_state != Disabled)
			{
			int	oldstate=m_state;

			if (m_selected)
				{
				m_state = hasMouseOver()?SelectedHover:(m_focused?SelectedFocused:SelectedNormal);
				}
			else
				{
				m_state = hasMouseOver()?Hover:(m_focused?Focused:Normal);
				}

			redraw();
			if (hasMouseOver() && (oldstate == Pressed || oldstate == SelectedPressed)) do_callback();
			}

		return 1;
		}


void
FXButton::setAllStyles(const FXButtonStyle &style, bool autovariance)
		{
		for (int i=0;i<MaxStyle;i++)
			{
			FXButtonStyle	bs=style;

			switch (i)
				{
				case Disabled:
				case SelectedDisabled:
					// Convert to grayscale
					bs.bdColor = FXColor::getGrayscale(style.bdColor);
					bs.bgColor = FXColor::getGrayscale(style.bgColor);
					bs.fgColor = FXColor::getGrayscale(style.fgColor);
					break;

				case Hover:
				case SelectedHover:
					// Lighten a little
					bs.bdColor = FXColor::getShade(style.bdColor, 1.1);
					bs.bgColor = FXColor::getShade(style.bgColor, 1.1);
					bs.fgColor = FXColor::getShade(style.fgColor, 1.1);
					break;

				case Pressed:
				case SelectedPressed:
					// Darken a little
					bs.bdColor = FXColor::getShade(style.bdColor, 0.9);
					bs.bgColor = FXColor::getShade(style.bgColor, 0.9);
					bs.fgColor = FXColor::getShade(style.fgColor, 0.9);
					break;
				}
			
			setButtonStyle(i, bs);
			}
		}


void
FXButton::setAllSelectedStyles(const FXButtonStyle &style, bool autovariance)
		{
		for (int i=0;i<MaxStyle;i++)
			{
			FXButtonStyle	bs=style;
			bool			apply=false;

			switch (i)
				{
				case SelectedDisabled:
					// Convert to grayscale
					bs.bdColor = FXColor::getGrayscale(style.bdColor);
					bs.bgColor = FXColor::getGrayscale(style.bgColor);
					bs.fgColor = FXColor::getGrayscale(style.fgColor);
					apply = true;
					break;

				case SelectedHover:
					// Lighten a little
					bs.bdColor = FXColor::getShade(style.bdColor, 1.1);
					bs.bgColor = FXColor::getShade(style.bgColor, 1.1);
					bs.fgColor = FXColor::getShade(style.fgColor, 1.1);
					apply = true;
					break;

				case SelectedPressed:
					// Darken a little
					bs.bdColor = FXColor::getShade(style.bdColor, 0.9);
					bs.bgColor = FXColor::getShade(style.bgColor, 0.9);
					bs.fgColor = FXColor::getShade(style.fgColor, 0.9);
					apply = true;
					break;

				case SelectedNormal:
				case SelectedFocused:
					apply = true;
					break;
				}
			
			if (apply) setButtonStyle(i, bs);
			}
		}

void
FXButton::setAllUnselectedStyles(const FXButtonStyle &style, bool autovariance)
		{
		for (int i=0;i<MaxStyle;i++)
			{
			FXButtonStyle	bs=style;
			bool			apply=false;

			switch (i)
				{
				case Disabled:
					// Convert to grayscale
					bs.bdColor = FXColor::getGrayscale(style.bdColor);
					bs.bgColor = FXColor::getGrayscale(style.bgColor);
					bs.fgColor = FXColor::getGrayscale(style.fgColor);
					apply = true;
					break;

				case Hover:
					// Lighten a little
					if (autovariance)
						{
						bs.bdColor = FXColor::getShade(style.bdColor, 1.1);
						bs.bgColor = FXColor::getShade(style.bgColor, 1.1);
						bs.fgColor = FXColor::getShade(style.fgColor, 1.1);
						}
					else
						{
						bs.bdColor = style.bdColor;
						bs.bgColor = style.bgColor;
						bs.fgColor = style.fgColor;
						}
					apply = true;
					break;

				case Pressed:
					// Darken a little
					if (autovariance)
						{
						bs.bdColor = FXColor::getShade(style.bdColor, 0.9);
						bs.bgColor = FXColor::getShade(style.bgColor, 0.9);
						bs.fgColor = FXColor::getShade(style.fgColor, 0.9);
						}
					else
						{
						bs.bdColor = style.bdColor;
						bs.bgColor = style.bgColor;
						bs.fgColor = style.fgColor;
						}
					apply = true;
					break;

				case Normal:
				case Focused:
					apply = true;
					break;
				}
			
			if (apply) setButtonStyle(i, bs);
			}
		}
