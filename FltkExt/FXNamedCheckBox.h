/*
**	FXNamedCheckBox.h	A generic dialog class
*/

#ifndef __FltkExt_FXNamedCheckBox_h__
#define __FltkExt_FXNamedCheckBox_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXNamedWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXNamedCheckBox : public FXNamedWidget
		{
public:
		FXNamedCheckBox(const SWString &name, int nameW, bool value, int valueW, int H);
		FXNamedCheckBox(int X, int Y, const SWString &name, int nameW, bool value, int valueW, int H);

protected:
		void		init(const SWString &name, bool value);
		};


#endif // __FltkExt_FXNamedCheckBox_h__
