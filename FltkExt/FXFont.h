#ifndef __FltkExt_FXFont_h__
#define __FltkExt_FXFont_h__

#include <FltkExt/FXColor.h>

#include <swift/SWString.h>

/**
*** A class to represent a font
***
*** A font can be defined in a couple of ways, by using a point size
*** of by using a height.
**/
class FLTKEXT_DLL_EXPORT FXFont
		{
public:
		enum Styles
			{
			Normal=0,
			Bold=1,
			Italic=2,
			BoldItalic=3
			};

		enum DefaultFontIds
			{
			General=0,	///< The general default font
			Dialog,		///< The default font for dialogs
			StaticText,	///< The default font for static text
			Button,		///< The The default font for buttons

			// Last one
			MaxDefaultFonts
			};

public:
		/// Default constructor
		FXFont();

		/// Point size constructor
		FXFont(const SWString &name, double ptsize, int style=0);

		/// Line height constructor
		FXFont(const SWString &name, int lineheight, int style=0);

		/// Copy constructor
		FXFont(const FXFont &other);

		/// Destructor
		virtual ~FXFont();

		/// Assignment operator
		FXFont &			operator=(const FXFont &other);

		/// Fl_Font constructor
		void				set(int fontid, int height);

		/// Point size constructor
		void				set(const SWString &name, double ptsize, int style);

		/// Line height constructor
		void				set(const SWString &name, int lineheight, int style);

		/// Select this font
		virtual void		select() const;

		/// Set the point size
		void				pointsize(double v);

		/// Get the point size
		double				pointsize() const;

		/// Set the height of the font in pixels
		void				height(int v);

		/// Get the height of the font in pixels
		int					height() const;

		/// set the style of the font
		void				style(int v);
		
		/// return the style of the font
		int					style() const;

		/// Add the attribute to the style
		void				addStyle(int v);
		
		/// Remove the attribute from the style
		void				removeStyle(int v);

		/// Get the font name
		const SWString &	name() const				{ return m_name; }
		
		/// Set the font name
		void				name(const SWString &v);

		// Return the font face as expected by FLTK
		int					fl_face() const				{ return m_flface; }

		// Return the font family index (see FXFontFamily)
		int					family() const				{ return m_family; }

		/// Return a version of this font in the given style if it exists
		FXFont				toStyle(int style) const;

		/// Return a bold version of this font
		FXFont				toNormal() const			{ return toStyle(Normal); }

		/// Return a bold version of this font
		FXFont				toBold() const				{ return toStyle(Bold); }

		/// Return a bold version of this font
		FXFont				toItalic() const			{ return toStyle(Italic); }

		/// Return a bold version of this font
		FXFont				toBoldItalic() const		{ return toStyle(BoldItalic); }

public:
		static double		pixelsToPointSize(int v, double dpi);
		static double		pixelsToPointSize(int v);
		static int			pointSizeToPixels(double v, double dpi);
		static int			pointSizeToPixels(double v);
		static FXFont		getDefaultFont(int idx=0);
		static void			setDefaultFont(int idx, const FXFont &font);

private: // Members
		SWString	m_name;		///< The font name
		int			m_style;	///< The font style attributes (bit field)
		double		m_ptsize;	///< The point size of the font
		int			m_flface;	///< The FLTK font face
		int			m_family;	///< The index into the FXFontFamily list
		};

#define GFONT_NORMAL		FXFont::Normal
#define GFONT_BOLD			FXFont::Bold
#define GFONT_ITALIC		FXFont::Italic
#define GFONT_BOLDITALIC	FXFont::BoldItalic

#endif // __FXFont_h__
