/****************************************************************************
**	fx_timer.cpp	Message box functions
****************************************************************************/

#include <FltkExt/fx_timer.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXWidget.h>

#include <swift/SWList.h>
#include <swift/SWThreadMutex.h>

class TimerEntry
		{
public:
		sw_uint32_t	timerId;
		int			timerType;
		double		interval;
		Fl_Widget	*pWidget;
		sw_uint32_t	localId;
		void		*pData;
		};

static SWList<TimerEntry *>	*g_pTimerList=NULL;
static SWThreadMutex		*g_pThreadMutex=NULL;
static int					g_timerId=0;

static void
timeoutHandler(void *data)
		{
		g_pThreadMutex->acquire();

		TimerEntry		*pEntry=(TimerEntry *)data;

		// Make a copy of the entry just in case the caller deletes it
		TimerEntry		entry=*pEntry;

		g_pThreadMutex->release();
		
		bool			handled=false;
		bool			repeat=true;

		if (!handled)
			{
			FXWnd	*pWidget=dynamic_cast<FXWnd *>(entry.pWidget);

			if (pWidget != NULL)
				{
				handled = true;
				repeat = pWidget->onTimer(entry.localId, entry.pData);
				}
			}

		if (!handled)
			{
			FXGroup	*pWidget=dynamic_cast<FXGroup *>(entry.pWidget);

			if (pWidget != NULL)
				{
				handled = true;
				repeat = pWidget->onTimer(entry.localId, entry.pData);
				}
			}

		if (!handled)
			{
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(entry.pWidget);

			if (pWidget != NULL)
				{
				handled = true;
				repeat = pWidget->onTimer(entry.localId, entry.pData);
				}
			}

		if (!handled)
			{
			entry.pWidget->do_callback();
			repeat = true;
			}
		
		SWMutexGuard				guard(g_pThreadMutex);
		SWIterator<TimerEntry *>	it=g_pTimerList->iterator();

		while (it.hasNext())
			{
			TimerEntry	*pListEntry=*it.next();

			if (pListEntry == pEntry)
				{
				if (repeat && entry.timerType == FX_TIMER_INTERVAL)
					{
					Fl::repeat_timeout(entry.interval, timeoutHandler, pEntry);
					}
				else
					{
					Fl::remove_timeout(timeoutHandler, pEntry);
					it.remove();
					delete pEntry;
					}

				break;
				}
			}

		}


FLTKEXT_DLL_EXPORT void
fx_timer_init()
		{
		if (g_pThreadMutex == NULL)
			{
			g_pThreadMutex = new SWThreadMutex(true);
			}

		SWMutexGuard	guard(g_pThreadMutex);

		if (g_pTimerList == NULL)
			{
			g_pTimerList = new SWList<TimerEntry *>();
			}
		}
		
		
FLTKEXT_DLL_EXPORT sw_uint32_t
fx_timer_create(sw_uint32_t localid, int type, double interval, Fl_Widget *pWidget, void *pData)
		{
		if (type != FX_TIMER_INTERVAL && type != FX_TIMER_ONESHOT) return 0;

		fx_timer_init();

		SWMutexGuard	guard(g_pThreadMutex);
		TimerEntry		*pEntry=new TimerEntry();
		
		pEntry->timerId = ++g_timerId;
		pEntry->timerType = type;
		pEntry->interval = interval;
		pEntry->pWidget = pWidget;
		pEntry->localId = localid;
		pEntry->pData = pData;

		g_pTimerList->add(pEntry);

		Fl::add_timeout(interval, timeoutHandler, pEntry);

		return pEntry->timerId;
		}
		
		
FLTKEXT_DLL_EXPORT void
fx_timer_destroy(sw_uint32_t id)
		{
		fx_timer_init();

		SWMutexGuard				guard(g_pThreadMutex);
		SWIterator<TimerEntry *>	it=g_pTimerList->iterator();

		while (it.hasNext())
			{
			TimerEntry	*pEntry=*it.next();

			if (pEntry->timerId == id)
				{
				Fl::remove_timeout(timeoutHandler, pEntry);
				it.remove();
				delete pEntry;
				break;
				}
			}
		}
		
		
FLTKEXT_DLL_EXPORT void
fx_timer_destroyAll(Fl_Widget *pWidget)
		{
		fx_timer_init();

		SWMutexGuard				guard(g_pThreadMutex);
		SWIterator<TimerEntry *>	it=g_pTimerList->iterator();

		while (it.hasNext())
			{
			TimerEntry	*pEntry=*it.next();

			if (pWidget == NULL || pEntry->pWidget == pWidget)
				{
				Fl::remove_timeout(timeoutHandler, pEntry);
				it.remove();
				delete pEntry;
				}
			}
		}
		
		
FLTKEXT_DLL_EXPORT void
fx_timer_destroy(Fl_Widget *pWidget, sw_uint32_t localid)
		{
		fx_timer_init();

		SWMutexGuard				guard(g_pThreadMutex);
		SWIterator<TimerEntry *>	it=g_pTimerList->iterator();

		while (it.hasNext())
			{
			TimerEntry	*pEntry=*it.next();

			if (pEntry->pWidget == pWidget && pEntry->localId == localid)
				{
				Fl::remove_timeout(timeoutHandler, pEntry);
				it.remove();
				delete pEntry;
				break;
				}
			}
		}
