/*
**	FXPropertySheet.h	A generic dialog class
*/

#ifndef __FltkExt_FXPropertySheet_h__
#define __FltkExt_FXPropertySheet_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXPropertyPage.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include <FL/Fl_Group.H>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Button.H>

class FLTKEXT_DLL_EXPORT FXPropertySheet : public Fl_Double_Window
		{
friend class FXPropertyPage;

public:
		enum Buttons
			{
			BtnOK=1,
			BtnApply=2,
			BtnCancel=4,
			BtnClose=8
			};

public:
		FXPropertySheet(int x, int y, int w, int h, const SWString &title, Fl_Widget *pOwner=NULL, int buttons=(BtnOK|BtnCancel|BtnApply));
		~FXPropertySheet();

		void				addPage(FXPropertyPage *pPage);
		FXPropertyPage *	getPageByID(int id);

		virtual int		doModal();
		virtual void	endDialog(int r);

		virtual void	onApply();
		virtual void	onOK();
		virtual void	onCancel();

		virtual void	draw();

		virtual void	setModified();
		
		virtual void	updateIcon();
		
		virtual void	loadAllData();
		virtual void	saveAllData();

		/// Set the title of the wizard
		void			setTitle(const SWString &text);

protected:
		static void		classCallback(Fl_Widget *pWidget, void *pUserData);
		virtual void	onCallback(Fl_Widget *pWidget);

protected:
		Fl_Widget					*m_pOwner;
		Fl_Tabs						*m_pTabs;
		Fl_Button					*m_pBtnApply;
		Fl_Button					*m_pBtnOK;
		Fl_Button					*m_pBtnCancel;
		Fl_Button					*m_pBtnClose;
		int							m_tabHeight;
		int							m_btnHeight;
		int							m_btnSpace;
		int							m_clientHeight;
		int							m_gapTop;
		int							m_gapBottom;
		bool						m_loaded;
		SWArray<FXPropertyPage *>	m_pages;
		int							m_result;
		SWString					m_title;
		};

#endif // __FltkExt_FXPropertySheet_h__

