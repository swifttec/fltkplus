#include "FXNamedInputInteger.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Int_Input.H>

FXNamedInputInteger::FXNamedInputInteger(int X, int Y, const SWString &name, int nameW, int value, int valueW, int H) :
	FXNamedWidget(X, Y, nameW, valueW, H)
		{
		init(name, value);
		}


FXNamedInputInteger::FXNamedInputInteger(const SWString &name, int nameW, int value, int valueW, int H) :
	FXNamedWidget(0, 0, nameW, valueW, H)
		{
		init(name, value);
		}

void
FXNamedInputInteger::init(const SWString &name, int value)
		{
		Fl_Input	*pInput;

		pInput = new Fl_Int_Input(0, 0, m_nameW, h()-2);
		pInput->when(FL_WHEN_CHANGED);
		FXNamedWidget::init(name, pInput, value);
		}
