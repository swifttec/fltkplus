#include "FXCheckList.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Scrollbar.H>
#include <FL/Fl_Shared_Image.H>

#include <FltkExt/FXPanelLayout.h>


class FLTKEXT_DLL_EXPORT FXCheckListWidget : public FXWidget
		{
public:
		class Item : public FXCheckListItem
			{
		public:
			Item();
			Item(const Item &other);
			~Item();

			Item &		operator=(const Item &other);

			void		clear();

			void		draw(const FXRect &cr, const FXRect &rParent, const FXRect &rPrevious, FXRect &dr, int level, bool firstInGroup, bool lastInGroup);
			void		drawTick(const FXRect &dr);
			void		drawCross(const FXRect &dr);
			
			int			height(bool itemonly=false) const;

			void		handleClickBox(FXCheckListWidget *pOwner);
			void		handleClickItem(FXCheckListWidget *pOwner);

			void		add(const Item &item);
			
			int			getItemCount(bool includeself);

			bool		removeItemByID(int id);

			Item *		getItemByID(int id, bool ignoreself=false);
			Item *		getItemAt(int idx, bool ignoreself=false);
			Item *		getItemAt(const FXPoint &pt, bool ignoreself=false);

			int			handleMouseDown(FXCheckListWidget *pCheckList, const FXPoint &pt, int &idx, int &selected);

			bool		allItemsHaveState(int state, bool ignoreself=false);

			void		setState(int s);
			void		setImage(const SWString &image);
			void		clearImage();

			int			updateStates();

		public:
			FXRect				rItem;		///< The rect of the checkbox
			FXRect				rCheckbox;	///< The rect of the checkbox
			FXRect				rControl;	///< The rect of the group control box
			SWArray<Item>	items;
			bool				expanded;
			bool				visible;
			bool				selected;
			Fl_Shared_Image		*m_pImage;		///< The original background image
			Fl_Image			*m_pImageCopy;	///< The resized background image used for drawing
			};

public:
		FXCheckListWidget(const FXColor &fgcolor, const FXColor &bgcolor=FXColor::NoColor);

		void		addItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded);
		void		addItemTo(int group, int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded);
		void		updateItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor);
		void		updateItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor);
		bool		containsItem(int id);
		bool		removeItem(int id);
		void		unselectAllItems();

//		bool		getItemByID(int id, Item &item);
//		bool		getItemAt(int idx, Item &item);
		Item *		getItemByID(int id);
		Item *		getItemAt(int idx);
		bool		getItemStateByID(int id, int &state);

		int			getItemCount();
		bool		getItemIdAt(int idx, int &id);
		bool		getItemStateAt(int idx, int &state);
		bool		setItemStateAt(int idx, int state);
		bool		setItemState(int id, int state);
		void		removeAllItems();

		int			getTotalItemHeight()	{ return m_tree.height(); }

		void		invalidate()			{ ((FXWidget *)parent())->invalidate(); }

protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

protected:
		Item	m_tree;
		int		m_selectedRow;
		};


FXCheckListWidget::Item::Item() :
	m_pImage(NULL),
	m_pImageCopy(NULL)
		{
		clear();
		}


FXCheckListWidget::Item::Item(const Item &other) :
	FXCheckListItem(other),
	m_pImage(NULL),
	m_pImageCopy(NULL)
		{
		clear();
		*this = other;

		}

FXCheckListWidget::Item::~Item()
		{
		clearImage();
		}


FXCheckListWidget::Item &
FXCheckListWidget::Item::operator=(const Item &other)
		{
		FXCheckListItem::operator=(other);

#define COPY(x)	x = other.x
			COPY(rItem);
			COPY(rCheckbox);
			COPY(rControl);
			COPY(items);
			COPY(expanded);
			COPY(visible);
			COPY(selected);

			setImage(other.image);
#undef COPY
		
		return *this;
		}


void
FXCheckListWidget::Item::clearImage()
		{
		if (m_pImage != NULL)
			{
			m_pImage->release();
			m_pImage = NULL;
			}

		if (m_pImageCopy != NULL)
			{
			delete m_pImageCopy;
			m_pImageCopy = NULL;
			}

		image = "";
		}


void
FXCheckListWidget::Item::clear()
		{
		FXCheckListItem::clear();

		expanded = false;
		visible = false;
		selected = false;
		rItem = rCheckbox = rControl = FXRect();
		items.clear();
		clearImage();
		}


bool
FXCheckListWidget::Item::removeItemByID(int id)
		{
		bool	res=false;

		for (int i=0;i<(int)items.size();i++)
			{
			if (items[i].id == id)
				{
				items.remove(i, 1);
				--i;
				res = true;
				}
			}
		
		return res;
		}

FXCheckListWidget::Item *
FXCheckListWidget::Item::getItemByID(int search, bool ignoreself)
		{
		Item	*pItem=NULL;

		if (id == search && !ignoreself)
			{
			pItem = this;
			}
		else
			{
			for (int i=0;pItem==NULL&&i<(int)items.size();i++)
				{
				pItem = items[i].getItemByID(search);
				}
			}
		
		return pItem;
		}



FXCheckListWidget::Item *
FXCheckListWidget::Item::getItemAt(int idx, bool ignoreself)
		{
		Item	*pItem=NULL;

		if (idx >= 0)
			{
			if (idx == 0 && !ignoreself)
				{
				pItem = this;
				}
			else
				{
				if (!ignoreself) idx--;
				for (int i=0;pItem==NULL&&i<(int)items.size();i++)
					{
					pItem = items[i].getItemAt(idx);
					idx -= items[i].getItemCount(false)+1;
					}
				}
			}
		
		return pItem;
		}



FXCheckListWidget::Item *
FXCheckListWidget::Item::getItemAt(const FXPoint &pt, bool ignoreself)
		{
		Item	*pItem=NULL;

		if (!ignoreself && rItem.contains(pt))
			{
			pItem = this;
			}
		else
			{
			for (int i=0;pItem==NULL&&i<(int)items.size();i++)
				{
				pItem = items[i].getItemAt(pt);
				}
			}
		
		return pItem;
		}


bool
FXCheckListWidget::Item::allItemsHaveState(int check, bool ignoreself)
		{
		bool	res=true;

		if (!ignoreself)
			{
			res = (check == state);
			}
		
		if (res)
			{
			for (int i=0;res&&i<(int)items.size();i++)
				{
				res = items[i].allItemsHaveState(check);
				}
			}
		
		return res;
		}


void
FXCheckListWidget::Item::setState(int s)
		{
		state = s;
		for (int i=0;i<(int)items.size();i++)
			{
			items[i].setState(s);
			}
		}


void
FXCheckListWidget::Item::setImage(const SWString &filename)
		{
		if (image != filename)
			{
			clearImage();
			}

		if (SWFileInfo::isFile(filename))
			{
			m_pImage = Fl_Shared_Image::get(filename);
			image = filename;
			}
		}



int
FXCheckListWidget::Item::getItemCount(bool includeself)
		{
		int		res=includeself?1:0;

		for (int i=0;i<(int)items.size();i++)
			{
			res += items[i].getItemCount(true);
			}

		return res;
		}


int
FXCheckListWidget::Item::handleMouseDown(FXCheckListWidget *pCheckList, const FXPoint &pt, int &idx, int &selected)
		{
		int		res=0;

		if (visible && rControl.contains(pt) && items.size() > 0)
			{
			expanded = !expanded;
			pCheckList->invalidate();
			((FXCheckList *)(pCheckList->parent()))->adjustScrollBar();
			res = 1;
			}
		else if (visible && rCheckbox.contains(pt))
			{
			handleClickBox(pCheckList);
			selected = idx;
			pCheckList->m_tree.updateStates();
			pCheckList->invalidate();
			((FXCheckList *)(pCheckList->parent()))->onItemClicked(id, selected);
			res = 1;
			}
		else if (visible && rItem.contains(pt))
			{
			handleClickItem(pCheckList);
			selected = idx;
			pCheckList->invalidate();
			res = 1;
			}
		else if (expanded)
			{
			for (int i=0;!res&&i<(int)items.size();i++)
				{
				res = items[i].handleMouseDown(pCheckList, pt, ++idx, selected);
				}
			}
		
		return res;
		}

int
FXCheckListWidget::Item::height(bool itemonly) const
		{
		int		res=0;

		res = font.height() + 2;
		
		if (expanded && !itemonly)
			{
			for (int i=0;i<(int)items.size();i++)
				{
				res += items.get(i).height();
				}
			}
		
		return res;
		}
/*

bool
FXCheckListWidget::Item::getItemByID(int search, Item &item)
		{
		Item	*pItem=getItemByID(search);
		
		if (pItem != NULL) item = *pItem;

		return (pItem != NULL);
		}


bool
FXCheckListWidget::Item::getItemAt(int idx, Item &item)
		{
		Item	*pItem=getItemAt(idx);
		
		if (pItem != NULL) item = *pItem;

		return (pItem != NULL);
		}
*/


void
FXCheckListWidget::Item::add(const Item &v)
		{
		items.add(v);
		}


int
FXCheckListWidget::Item::updateStates()
		{
		int		v;

		for (int i=0;i<(int)items.size();i++)
			{
			v = items[i].updateStates();
			if (i == 0) state = v;
			else if (v != state)
				{
				state = FXCheckList::ItemMixed;
				}
			}

		return state;

		
		/*
		if (items.size())
			{
			state = 0;
			if (allItemsHaveState(FXCheckList::ItemTicked, true)) state = FXCheckList::ItemTicked;
			else if (allItemsHaveState(FXCheckList::ItemUnselected, true)) state = FXCheckList::ItemUnselected;
			else if (allItemsHaveState(FXCheckList::ItemCrossed, true)) state = FXCheckList::ItemCrossed;
			else state = FXCheckList::ItemMixed;
			}
		*/
		}

void
FXCheckListWidget::Item::draw(const FXRect &rClientArea, const FXRect &rParent, const FXRect &rPrevious, FXRect &dr, int level, bool firstInGroup, bool lastInGroup)
		{
		FXRect	rect(dr);
		int		fh=font.height();

		rect.bottom = rect.top + height(true);

		if (level > 0)
			{
			if (selected)
				{
				fx_draw_fillRect(rect, "#bdf");
				}

#define CWIDTH		16
#define CHEIGHT		12
#define GROUP_LINE_COLOR	"#999"
#define GROUP_LINE_STYLE	FX_LINESTYLE_DOTS
#define GROUP_LINE_WIDTH	1

			rItem = rect;

			rCheckbox.left = rect.left + 2 + (CWIDTH * level); // (level + ((items.size()!=0)?1:0)));
			rCheckbox.right = rCheckbox.left + fh;
			rCheckbox.top = rect.top + ((rect.height() - fh) / 2);
			rCheckbox.bottom = rCheckbox.top + fh;

			FXColor	boxfillcolor="#fff";
			FXColor	fgcolor=textcolor;

			if (state < 0)
				{
				boxfillcolor = "#ccc";
				fgcolor = "#888";
				}
			else if (style == 1)
				{
				switch (state)
					{
					case FXCheckList::ItemTicked:
						boxfillcolor = "#0f0";
						break;

					case FXCheckList::ItemCrossed:
						boxfillcolor = "#f00";
						break;
					}
				}

			/*
			for (int i=0;i<level;i+=2)
				{
				fx_draw_line(rect.left + 2 + (CWIDTH/2) + (CWIDTH * i), rect.top, rect.left + 2 + (CWIDTH/2) + (CWIDTH * i), rect.bottom, "#888", FX_LINESTYLE_DOTS, 1);
				}
			*/

			if (items.size())
				{
				int		ch=CHEIGHT;
				
				rControl.left = rect.left + 2 + (CWIDTH * (level-1)) + ((CWIDTH-CHEIGHT) / 2);
				rControl.right = rControl.left + ch;
				rControl.top = rect.top + ((rect.height() - ch) / 2);
				rControl.bottom = rControl.top + ch;

				fx_draw_box(rControl, boxcolor, 1, FX_COLOR_NONE, 0);

				FXRect	r=rControl;
				int		adj=rCheckbox.width() / 4;

				r.deflate(adj, adj, adj, adj);
				fx_draw_line(r.left, r.top + r.height()/2, r.right, r.top + r.height() / 2, boxcolor, FX_LINESTYLE_SOLID, 1);
				if (!expanded)
					{
					fx_draw_line(r.left + r.width()/2, r.top, r.left + r.width()/2, r.bottom, boxcolor, FX_LINESTYLE_SOLID, 1);
					}
				}
			else
				{
				rControl = FXRect();
				}
			
			if (level > 1)
				{
				int		midY=rect.top + (rect.height() / 2);

				if (items.size() == 0)
					{
					// Draw horizontal line back to group line
					fx_draw_line(rect.left + 2 + (CWIDTH/2) + (CWIDTH * (level-1)), midY, rect.left + 2 + (CWIDTH * (level)), midY, GROUP_LINE_COLOR, GROUP_LINE_STYLE, GROUP_LINE_WIDTH);
					}

				if (!firstInGroup && rect.top > (rPrevious.bottom + 1))
					{
					// fx_draw_line(rect.left + 2 + (CWIDTH/2) + (CWIDTH * (level-1)), rect.top, rect.left + 2 + (CWIDTH/2) + (CWIDTH * (level-1)), rPrevious.bottom + 1, GROUP_LINE_COLOR, GROUP_LINE_STYLE, GROUP_LINE_WIDTH);
					}
			
				if ((lastInGroup || (items.size() > 0 && (rect.top - rParent.bottom) >= rect.height())) && !firstInGroup)
					{
					int	fromY=midY;

					if (items.size() > 0) fromY = rControl.top;

					fx_draw_line(rect.left + 2 + (CWIDTH/2) + (CWIDTH * (level-1)), fromY, rect.left + 2 + (CWIDTH/2) + (CWIDTH * (level-1)), rParent.bottom + 1, GROUP_LINE_COLOR, GROUP_LINE_STYLE, GROUP_LINE_WIDTH);
					}
				}

			fx_draw_box(rCheckbox, boxcolor, boxwidth, boxfillcolor, 0);

			if (style == 0)
				{
				switch (state)
					{
					case FXCheckList::ItemTicked:
						drawTick(rCheckbox);
						break;

					case FXCheckList::ItemCrossed:
						drawCross(rCheckbox);
						break;

					case FXCheckList::ItemMixed:
						{
						FXRect	r=rCheckbox;
						int		adj=rCheckbox.width() / 4;

						r.deflate(adj, adj, adj, adj);
						fx_draw_box(r, FX_COLOR_NONE, 0, fgcolor, 0);
						}
						break;
					}
				}

			rect.left = rCheckbox.right + 2;

			if (m_pImage != NULL)
				{
				int		iw, ih;

				iw = ih = rect.height();

				if (m_pImageCopy != NULL)
					{
					if (m_pImageCopy->w() != iw || m_pImageCopy->h() != ih)
						{
						delete m_pImageCopy;
						m_pImageCopy = NULL;
						}
					}

				if (m_pImageCopy == NULL)
					{
					m_pImageCopy = m_pImage->copy(iw, ih);
					}
			
				if (m_pImageCopy != NULL)
					{
					m_pImageCopy->draw(rect.left, rect.top, iw, ih, 0, 0);
					}
				
				rect.left += iw + 2;
				}

			fx_select(font);
			fx_select(fgcolor);
			fx_draw_text(text, rect, FX_HALIGN_LEFT|FX_VALIGN_CENTER);

			dr.top = rect.bottom + 1;
			}

		if (expanded && items.size() != 0)
			{
			FXRect	rPrev=rCheckbox;
			FXRect	rParent=rCheckbox;

			for (int i=0;i<(int)items.size();i++)
				{
				Item	&item=items[i];

				dr.bottom = dr.top + item.height(true);
				item.selected = false; // (m_selectedRow == i);
				item.visible = dr.bottom >= rClientArea.top && dr.top <= rClientArea.bottom;
				item.draw(rClientArea, rParent, rPrev, dr, level+1, i==0, i==((int)items.size()-1));

				if (item.items.size() != 0) rParent = item.rControl;
				rPrev = item.rCheckbox;
				dr.top = dr.bottom + 1;
				}
			}
		}


void
FXCheckListWidget::Item::drawTick(const FXRect &dr)
		{
		FXRect	br(dr);

		br.DeflateRect(boxwidth * 2, boxwidth * 2, boxwidth * 2, boxwidth * 2);
		fx_draw_tick(br, textcolor);
		}

void
FXCheckListWidget::Item::drawCross(const FXRect &dr)
		{
		FXRect	br(dr);

		br.DeflateRect(boxwidth * 2, boxwidth * 2, boxwidth * 2, boxwidth * 2);
		fx_draw_cross(br, textcolor);
		}


void
FXCheckListWidget::Item::handleClickBox(FXCheckListWidget *pOwner)
		{
		if (state >= 0)
			{
			if (state == FXCheckList::ItemTicked) setState(FXCheckList::ItemUnselected);
			else setState(FXCheckList::ItemTicked);
			}
		}


void
FXCheckListWidget::Item::handleClickItem(FXCheckListWidget *pOwner)
		{
		}



FXCheckListWidget::FXCheckListWidget(const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(100, 20),
	m_selectedRow(-1)
		{
		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		m_tree.expanded = true;
		}


void
FXCheckListWidget::unselectAllItems()
		{
		m_tree.setState(0);
		m_tree.updateStates();
		}


void
FXCheckListWidget::addItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		Item	item;

		item.id = id;
		item.text = text;
		item.state = state;
		item.font = font;
		item.textcolor = textcolor;
		item.boxcolor = boxcolor;
		item.expanded = expanded;
		item.setImage(image);

		m_tree.add(item);
		m_tree.updateStates();
		}


void
FXCheckListWidget::addItemTo(int group, int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		Item	*pGroup;

		if ((pGroup = getItemByID(group)) != NULL)
			{
			Item	item;

			item.id = id;
			item.text = text;
			item.state = state;
			item.font = font;
			item.textcolor = textcolor;
			item.boxcolor = boxcolor;
			item.expanded = expanded;
			item.setImage(image);

			pGroup->add(item);
			m_tree.updateStates();
			}
		}


void
FXCheckListWidget::updateItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor)
		{
		Item	*pItem;

		if ((pItem = getItemByID(id)) != NULL)
			{
			pItem->id = id;
			pItem->text = text;
			pItem->state = state;
			pItem->font = font;
			pItem->textcolor = textcolor;
			pItem->boxcolor = boxcolor;
			m_tree.updateStates();
			}
		}


void
FXCheckListWidget::updateItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor)
		{
		Item	*pItem;

		if ((pItem = getItemByID(id)) != NULL)
			{
			pItem->id = id;
			pItem->text = text;
			pItem->state = state;
			pItem->font = font;
			pItem->textcolor = textcolor;
			pItem->boxcolor = boxcolor;
			
			pItem->setImage(image);
			m_tree.updateStates();
			}
		}
bool
FXCheckListWidget::containsItem(int id)
		{
		return (getItemByID(id) != NULL);
		}


void
FXCheckListWidget::removeAllItems()
		{
		m_tree.clear();
		m_tree.expanded = true;
		}


bool
FXCheckListWidget::getItemIdAt(int idx, int &id)
		{
		Item	*pItem;

		if ((pItem = getItemAt(idx)) != NULL)
			{
			id = pItem->id;
			}

		return (pItem != NULL);
		}


bool
FXCheckListWidget::getItemStateAt(int idx, int &state)
		{
		Item	*pItem;

		if ((pItem = getItemAt(idx)) != NULL)
			{
			state = pItem->state;
			}
		else
			{
			state = 0;
			}

		invalidate();

		return (pItem != NULL);
		}


bool
FXCheckListWidget::setItemState(int id, int state)
		{
		Item	*pItem;

		if ((pItem = getItemByID(id)) != NULL)
			{
			pItem->state = state;
			m_tree.updateStates();
			}

		invalidate();

		return (pItem != NULL);
		}


bool
FXCheckListWidget::setItemStateAt(int idx, int state)
		{
		Item	*pItem;

		if ((pItem = getItemAt(idx)) != NULL)
			{
			pItem->state = state;
			m_tree.updateStates();
			}

		invalidate();

		return (pItem != NULL);
		}
int
FXCheckListWidget::getItemCount()
		{
		return m_tree.getItemCount(false);
		}



FXCheckListWidget::Item *
FXCheckListWidget::getItemByID(int id)
		{
		return m_tree.getItemByID(id, true);
		}



FXCheckListWidget::Item *
FXCheckListWidget::getItemAt(int idx)
		{
		return m_tree.getItemAt(idx, true);
		}


bool
FXCheckListWidget::getItemStateByID(int id, int &state)
		{
		FXCheckListWidget::Item	*pItem=getItemByID(id);

		if (pItem != NULL) state = pItem->state;

		return (pItem != NULL);
		}

bool
FXCheckListWidget::removeItem(int id)
		{
		bool	res=false;

		if (m_tree.removeItemByID(id))
			{
			m_tree.updateStates();
			}

		return res;
		}


void
FXCheckListWidget::onDraw()
		{
		FXRect	cr, dr;

		getClientRect(cr);

		dr = cr;

		FXScrollInfo	si;
		FXScrollBar		*pScrollBar=(FXScrollBar *)(parent()->child(1));

		pScrollBar->getScrollInfo(si);

		dr.top -= si.position();
		m_tree.draw(cr, FXRect(), FXRect(), dr, 0, false, false);
		}


int
FXCheckListWidget::onMouseDown(int button, const FXPoint &pt)
		{
		if (button == FL_LEFT_MOUSE)
			{
			int		idx=0;

			m_selectedRow = -1;
			m_tree.handleMouseDown(this, pt, idx, m_selectedRow);
			}

		return 0;
		}



FXCheckList::FXCheckList(int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXGroup(X, Y, W, H, bgcolor, fgcolor)
		{
		end();
		
		m_defaultFont = FXFont::getDefaultFont(FXFont::StaticText);
		m_defaultBoxColor = m_defaultTextColor = "#000";
		m_defaultItemStyle = 0;

		setBorder(0, "#000", 1, 0);

		setLayout(new FXPanelLayout(false));
		
		FXCheckListWidget	*pWidget;

		pWidget = new FXCheckListWidget(fgcolor, bgcolor);
		add(pWidget);

		setUseLayoutSizes(false);

		FXScrollBar		*pScrollBar;
		FXScrollInfo	si;

		add(pScrollBar = new FXScrollBar(0, 0, 16, h(), true, this));
		pScrollBar->getScrollInfo(si);
		si.setLineSize(m_defaultFont.height());
		pScrollBar->setScrollInfo(si);
		}




FXCheckList::~FXCheckList()
		{
		}


void
FXCheckList::addItemTo(int group, int id, const SWString &text, const SWString &image, int state, bool expanded)
		{
		addItemTo(group, id, text, image, state, m_defaultFont, m_defaultTextColor, m_defaultBoxColor, expanded);
		}


void
FXCheckList::addItem(int id, const SWString &text, const SWString &image, int state, bool expanded)
		{
		addItem(id, text, image, state, m_defaultFont, m_defaultTextColor, m_defaultBoxColor, expanded);
		}


void
FXCheckList::addItem(const FXCheckListEntry &entry)
		{
		addItem(entry.id, entry.text, entry.image, entry.state, m_defaultFont, m_defaultTextColor, m_defaultBoxColor, false);
		}

void
FXCheckList::adjustScrollBar()
		{
		FXCheckListWidget	*pListWidget=(FXCheckListWidget *)child(0);
		FXScrollBar			*pScrollBar=(FXScrollBar *)child(1);

		if (pScrollBar != NULL) pScrollBar->setScrollRange(0, pListWidget->getTotalItemHeight(), h());
		}


void
FXCheckList::addItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		addItem(id, text, "", state, font, textcolor, boxcolor, expanded);
		}


void
FXCheckList::addItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		pWidget->addItem(id, text, image, state, font, textcolor, boxcolor, expanded);
		adjustScrollBar();
		}


void
FXCheckList::addItemTo(int group, int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		addItemTo(group, id, text, "", state, font, textcolor, boxcolor, expanded);
		}

void
FXCheckList::addItemTo(int group, int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor, bool expanded)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		pWidget->addItemTo(group, id, text, image, state, font, textcolor, boxcolor, expanded);
		adjustScrollBar();
		}

void
FXCheckList::updateItem(int id, const SWString &text, const SWString &image, int state)
		{
		updateItem(id, text, image, state, m_defaultFont, m_defaultTextColor, m_defaultBoxColor);
		}


void
FXCheckList::updateItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor)
		{
		FXCheckListWidget	*pListWidget=(FXCheckListWidget *)child(0);

		pListWidget->updateItem(id, text, state, font, textcolor, boxcolor);
		adjustScrollBar();
		}

void
FXCheckList::updateItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor)
		{
		FXCheckListWidget	*pListWidget=(FXCheckListWidget *)child(0);

		pListWidget->updateItem(id, text, image, state, font, textcolor, boxcolor);
		adjustScrollBar();
		}


void
FXCheckList::unselectAllItems()
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		pWidget->unselectAllItems();
		}


int
FXCheckList::getItemCount()
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->getItemCount();
		}

int
FXCheckList::getItemState(int id)
		{
		int		res=-2;

		if (!getItemStateByID(id, res)) res = -2;

		return res;
		}


bool
FXCheckList::getItemStateByID(int id, int &state)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->getItemStateByID(id, state);
		}

bool
FXCheckList::getItemIdAt(int idx, int &id)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->getItemIdAt(idx, id);
		}


bool
FXCheckList::getItemStateAt(int idx, int &state)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->getItemStateAt(idx, state);
		}


bool
FXCheckList::setItemState(int id, int state)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->setItemState(id, state);
		}


bool
FXCheckList::setItemStateAt(int idx, int state)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->setItemStateAt(idx, state);
		}


bool
FXCheckList::getItemByID(int id, FXCheckListItem &item)
		{
		FXCheckListWidget	*pWidget;
		FXCheckListItem		*pItem;

		pWidget = (FXCheckListWidget *)child(0);
		pItem = pWidget->getItemByID(id);
		if (pItem != NULL) item = *pItem;

		return (pItem != NULL);
		}


bool
FXCheckList::getItemAt(int idx, FXCheckListItem &item)
		{
		FXCheckListWidget	*pWidget;
		FXCheckListItem		*pItem;

		pWidget = (FXCheckListWidget *)child(0);
		pItem = pWidget->getItemAt(idx);
		if (pItem != NULL) item = *pItem;

		return (pItem != NULL);
		}


bool
FXCheckList::containsItem(int id)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->containsItem(id);
		}

bool
FXCheckList::removeItem(int id)
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		return pWidget->removeItem(id);
		}

void
FXCheckList::removeAllItems()
		{
		FXCheckListWidget	*pWidget;

		pWidget = (FXCheckListWidget *)child(0);
		pWidget->removeAllItems();
		}

void
FXCheckList::resize(int X, int Y, int W, int H)
		{
		FXGroup::resize(X, Y, W, H);
		adjustScrollBar();
		}


void
FXCheckList::onItemClicked(int id, int state)
		{
		m_clickedItemID = id;
		m_clickedItemState = state;
		do_callback();
		}


void
FXCheckList::getClickedItemInfo(int &id, int &state)
		{
		id = m_clickedItemID;
		state = m_clickedItemState;
		}
