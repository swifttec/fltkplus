/*
**	FXCheckList.h	A generic dialog class
*/

#ifndef __FltkExt_FXCheckList_h__
#define __FltkExt_FXCheckList_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXScrollBar.h>

#include <FL/Fl_Box.H>


class FLTKEXT_DLL_EXPORT FXCheckListEntry
		{
public:
		FXCheckListEntry()	{ clear(); }
		
		void clear()
				{
				id = 0;
				state = 0;
				text.clear();
				image.clear();
				}

public:
		int			id;			///< The item ID
		int			state;		///< The check state
		SWString	text;		///< The actual text
		SWString	image;		///< The image to use
		};


class FLTKEXT_DLL_EXPORT FXCheckListItem : public FXCheckListEntry
		{
public:
		FXCheckListItem()	{ clear(); }
		
		void clear()
				{
				FXCheckListEntry::clear();
				style = 0;
				boxwidth = 1;
				}

public:
		int			style;		///< The draw style
		FXFont		font;		///< The font to use
		FXColor		textcolor;	///< The text color
		FXColor		boxcolor;	///< The box color
		int			boxwidth;
		};


class FLTKEXT_DLL_EXPORT FXCheckList : public FXGroup
		{
public:
		enum
			{
			ItemDisabled=-1,
			ItemUnselected=0,
			ItemTicked=1,
			ItemCrossed=2,
			ItemMixed=3
			};

public:
		FXCheckList(int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor=FXColor::NoColor);
//		FXCheckList(int W, int H,				const FXColor &fgcolor, const FXColor &bgcolor=FXColor::NoColor);

		virtual ~FXCheckList();

		/// Set the default font for all items
		void		setDefaultFont(const FXFont &v)			{ m_defaultFont = v; }

		/// Set the default font for all items
		void		setDefaultTextColor(const FXColor &v)	{ m_defaultTextColor = v; }

		/// Set the default font for all items
		void		setDefaultBoxColor(const FXColor &v)	{ m_defaultBoxColor = v; }

		/// Remove all the items from the checklist
		void		removeAllItems();

		/// Remove all the items from the checklist
		void		unselectAllItems();

		/// Test to see if the checklist contains the item with the given ID
		bool		containsItem(int id);

		/// Remove the item with the given ID from the checklist
		bool		removeItem(int id);

		/// Get the number of items in the checklist
		int			getItemCount();

		/// An an item to the check list using the default font and color settings
		void		addItem(const FXCheckListEntry &entry);
		void		addItem(int id, const SWString &text, const SWString &image="", int state=0, bool expanded=false);
		void		addItemTo(int group, int id, const SWString &text, const SWString &image="", int state=0, bool expanded=false);

		void		addItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000", bool expanded=false);
		void		addItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000", bool expanded=false);
		void		addItemTo(int group, int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000", bool expanded=false);
		void		addItemTo(int group, int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000", bool expanded=false);

		void		updateItem(int id, const SWString &text, const SWString &image, int state);
		void		updateItem(int id, const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000");
		void		updateItem(int id, const SWString &text, const SWString &image, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000");

		int			getItemState(int id);
		bool		getItemStateByID(int id, int &state);
		bool		getItemIdAt(int idx, int &id);
		bool		getItemStateAt(int idx, int &state);

		bool		getItemByID(int id, FXCheckListItem &item);
		bool		getItemAt(int id, FXCheckListItem &item);

		bool		setItemState(int id, int state);
		bool		setItemStateAt(int idx, int state);

		void		getClickedItemInfo(int &id, int &state);

		void		adjustScrollBar();
		///
		virtual void	onItemClicked(int id, int state);

protected:
		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);

protected:
		SWString		m_text;				///< The actual text
		int			m_align;			///< The text alignment
		FXFont		m_defaultFont;		///< The default font for items
		FXColor		m_defaultTextColor;	///< The default text color for items
		FXColor		m_defaultBoxColor;	///< The default box color for items
		int			m_defaultItemStyle;	///< The default style for items
		int			m_clickedItemID;
		int			m_clickedItemState;
		};


#endif // __FltkExt_FXCheckList_h__
