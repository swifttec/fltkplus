/*
**	FXPen.h	A generic 2D size
*/

#ifndef __FltkExt_FXPen_h__
#define __FltkExt_FXPen_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>

class FLTKEXT_DLL_EXPORT FXPen
		{
public:
		FXPen() :
			style(FX_PEN_SOLID),
			width(1),
			color(FXColor(0, 0, 0))
				{
				}

		FXPen(int vstyle, int vwidth, const FXColor &vcolor) :
			style(vstyle),
			width(vwidth),
			color(vcolor)
				{
				}

		void set(int vstyle, int vwidth, const FXColor &vcolor)
				{
				style = vstyle;
				width = vwidth;
				color = vcolor;
				}


public:
		int		style;
		int		width;
		FXColor	color;
		};

#endif // __FltkExt_FXPen_h__