/*
**	FXApp.h	A generic windows application class
*/

#ifndef __FltkExt_FXApp_h__
#define __FltkExt_FXApp_h__

#include <FltkExt/FltkExt.h>

#include <swift/SWString.h>
#include <swift/SWStringArray.h>
#include <swift/sw_main.h>
#include <swift/SWApp.h>

/**
*** Generic application class for a GUI application using FLTK and FltkExt.
***
*** This class is designed for an application developer to derive a class from to
*** execute a FLTK GUI. A typical program would have the following construct:
***
***		class MyApp : public FXApp
***			{
***		public:
***			virtual bool initInstance(const SWString &prog, const SWStringArray &args);
***			};
***
***		MyApp	theApp;
***
***		FX_APP_EXECUTE(theApp);
***
***		bool
***		MyApp::initInstance(const SWString &prog, const SWStringArray &args)
***				{
***				bool	res=false;
***
***				if (FXApp::initInstance(prog, args))
***					{
***					new MainWindow(600, 400);
***					m_pMainWnd->show();
***
***					res = true;
***					}
***
***				return res;
***				}
***
*** The underlying code will initialise the FLTK subsystem and then call initInstance
*** to initialise the application. If initInstance returns true the code will then go
*** into a loop executing the windowing code which will exit whe the main window is closed.
*** Finally the underlying code will call exitInstance to do any cleanup needed.
***
*** In general the only method the developer needs to override is the initInstance as shown
*** in the above example.
**/
class FLTKEXT_DLL_EXPORT FXApp : public SWApp
		{
public:
		/// Constructor
		FXApp(SWAppConfig *pAppConfig=NULL, bool mainapp=true);


		/**
		*** Initialise the application instance
		***
		*** This method is called by the execute code and should be overwritten by the developer
		*** in order to create the primary window. 
		***
		*** @param	prog	The path of the program being executed
		*** @param	args	The command line args
		**/
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/**
		*** Initialise the application instance
		***
		*** This method calls the underlying FLTK method to run the windowing loop code.
		**/
		virtual void	runInstance();

		/**
		*** Set the main window pointer to monitor in runInstance
		**/
		void			setMainWindow(void **ppMainWnd)	{ m_ppMainWnd = ppMainWnd; }

protected:
		void	**m_ppMainWnd;
		};

#define FX_APP_EXECUTE(app)	SW_APP_EXECUTE(app)

#endif // __FltkExt_FXApp_h__
