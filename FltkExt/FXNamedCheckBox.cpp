#include "FXNamedCheckBox.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <FltkExt/FXPackedLayout.h>
#include <FltkExt/FXCheckBox.h>

FXNamedCheckBox::FXNamedCheckBox(int X, int Y, const SWString &name, int nameW, bool value, int valueW, int H) :
	FXNamedWidget(X, Y, nameW, valueW, H)
		{
		init(name, value);
		}


FXNamedCheckBox::FXNamedCheckBox(const SWString &name, int nameW, bool value, int valueW, int H) :
	FXNamedWidget(0, 0, nameW, valueW, H)
		{
		init(name, value);
		}

void
FXNamedCheckBox::init(const SWString &name, bool value)
		{
		FXNamedWidget::init(name, new FXCheckBox(0, 0, m_nameW, h()-2), value);
		}
