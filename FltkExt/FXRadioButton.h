#ifndef __FltkExt_FXRadioButton_h__
#define __FltkExt_FXRadioButton_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>

#include <FL/Fl_Light_Button.H>


class FLTKEXT_DLL_EXPORT FXRadioButton : public Fl_Light_Button
		{
public:
		FXRadioButton(const SWString &text, int x=0, int y=0, int w=100, int h=100, const FXColor &textcolor=FXColor(0, 0, 0));
		virtual ~FXRadioButton();

		/// Set the font for the button text
		void		setFont(const FXFont &font);

protected:
		SWString		m_text;
		};

#endif // __FltkExt_FXRadioButton_h__
