/****************************************************************************
**	fx_timer.h	Message box functions
****************************************************************************/


#ifndef __FltkExt_fx_timer_h__
#define __FltkExt_fx_timer_h__

#include <FltkExt/FltkExt.h>
#include <FL/Fl.H>

#include <swift/SWString.h>

#define FX_TIMER_ONESHOT	1
#define FX_TIMER_INTERVAL	2

/// Initialise the timers (called automatically)
FLTKEXT_DLL_EXPORT void				fx_timer_init();

/// Create a timer - returns a global timer id
FLTKEXT_DLL_EXPORT sw_uint32_t		fx_timer_create(sw_uint32_t localid, int type, double seconds, Fl_Widget *pWidget, void *pData);

/// Destroy a timer based on the global id
FLTKEXT_DLL_EXPORT void				fx_timer_destroy(sw_uint32_t globalid);

/// Destroy a timer based on the widget and local id
FLTKEXT_DLL_EXPORT void				fx_timer_destroy(Fl_Widget *pWidget, sw_uint32_t localid);

/// Destroy all timers based on the widget
FLTKEXT_DLL_EXPORT void				fx_timer_destroyAll(Fl_Widget *pWidget);

#endif // __FltkExt_fx_timer_h__
