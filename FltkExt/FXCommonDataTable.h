#ifndef __FltkExt_FXCommonDataTable_h__
#define __FltkExt_FXCommonDataTable_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXButtonBar.h>
#include <FltkExt/FXDataTable.h>
#include <FltkExt/FXTestWidget.h>

#include <swift/SWArray.h>
#include <swift/SWStringArray.h>

/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXCommonDataTable : public FXGroup
		{
public:
		FXCommonDataTable();
		virtual ~FXCommonDataTable();
		
		void			addColumn(const SWString &title, int align);

		int				getColumnCount() const	{ return m_pDataTable->getColumnCount(); }
		int				getRowCount() const		{ return m_pDataTable->getRowCount(); }

		void			removeAllRows();
		int				addRow(const SWStringArray &values);
		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);

		void			setCell(int row, int col, const SWString &contents, const FXColor &fgcolor=FX_COLOR_DEFAULT, const FXColor &bgcolor=FX_COLOR_DEFAULT);
		void			setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor);
		void			setCellString(int row, int col, const SWString &contents);
		SWString			getCellString(int row, int col) const;

		void			addButton(int id, const SWString &text, const SWString &image, bool enabled=true);

		FXDataTable *	getDataTable() const		{ return m_pDataTable; }
		FXButtonBar *	getButtonBar() const		{ return m_pButtonBar; }

protected: /// FLTK overrides

		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);


protected:
		int						m_buttonWidth;				///< The width of the button area
		int						m_buttonGapX;				///< The gap between the buttons and the table

		FXDataTable				*m_pDataTable;				///< The data table
		FXButtonBar				*m_pButtonBar;				///< A bar for holding the action buttons
		FXPadding				m_padding;
		};

#endif // __FltkExt_FXCommonDataTable_h__
