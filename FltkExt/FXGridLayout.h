#ifndef __FltkExt_FXGridLayout_h__
#define __FltkExt_FXGridLayout_h__

#include <FltkExt/FXLayout.h>

#include <swift/SWArray.h>

/**
*** A layout class based on a table-like grid of rows and columns
**/
class FLTKEXT_DLL_EXPORT FXGridLayout : public FXLayout
		{
public:
		class Column
			{
		public:
			int		align;		///< The column alignment

		public:
			Column() : align(0) { }
			};

		class Cell
			{
		public:
			Fl_Widget		*pWidget;	///< The cell contents
			int				align;		///< The cell alignment
			FXWidgetSizes	sizes;		///< The cached cell sizes

		public:
			Cell() : pWidget(NULL), align(0) { }
			};

			
public:
		/// Constructor
		FXGridLayout(int ncols, int nrows, bool floatChildren=false);
		
		/// Destructor
		virtual ~FXGridLayout();

		/// Set the dimensions of the grid layout
		void	setDimensions(int ncols, int nrows);

		/**
		*** Add a child widget at the given available postion
		**/
		virtual void		setWidget(Fl_Widget *pWidget, int align, int col, int row);

		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget, int align);

public: // FXLayout overrides

		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget);

		/**
		*** Get the minimum size of this layout based on the current children.
		***
		*** The minimum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		**/
		virtual FXSize		getMinSize() const;


		/**
		*** Get the maximum size of this layout based on the current children.
		***
		*** The maximum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		***
		*** If a dimension is zero this implies that there is no maximum size for that
		*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
		*** the width is restricted to 1000 pixels, but the height is unrestricted.
		**/
		virtual FXSize		getMaxSize() const;

		/// Called to layout the children
		virtual void		layoutChildren();

private:
		/// Get the cell at the given column/row index
		Cell &				getCell(int colidx, int rowidx) const;

		/// Remove the specified rows
		void				removeRows(int idx, int count);

		/// Remove the specified columns
		void				removeColumns(int idx, int count);

		/// Insert blank rows at the specified index
		void				insertRows(int idx, int count);

		/// Insert blank columns at the specified index
		void				insertColumns(int idx, int count);

		/// Internal method to set the widget at the specified position
		void				setWidgetAt(Fl_Widget *pWidget, int align, int col, int row);

		/// Get the min/max sizes of the column
		FXWidgetSizes		getCellSizes(int col, int row, bool useCachedValues);

		/// Get the min/max sizes of the column
		FXWidgetSizes		getColumnSizes(int idx, bool useCachedValues);

		/// Get the min/max sizes of the row
		FXWidgetSizes		getRowSizes(int idx, bool useCachedValues);

protected:
		SWArray<Cell>			m_cells;		///< The array of cells of size (rows * cols) arranged as r0(c0,c1,c2),r1(c0,c1,c2)...
		SWArray<Column>			m_columns;		///< The columns array
		int						m_nRows;		///< The number of rows
		int						m_nColumns;		///< The number of columns
		int						m_lastRow;		///< The row index of the last cell added
		int						m_lastColumn;	///< The column index of the last cell added
		bool					m_floatChildren;	///< If true the children float withing their cells
		};

#endif // __FltkExt_FXGridLayout_h__
