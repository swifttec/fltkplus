/*
**	FXStaticNamedValue.h	A generic dialog class
*/

#ifndef __FltkExt_FXStaticNamedValue_h__
#define __FltkExt_FXStaticNamedValue_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXStaticText.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXStaticNamedValue : public FXGroup
		{
public:
		FXStaticNamedValue(const SWString &name, int nameW, const SWString &value, int valueW, int H);
		FXStaticNamedValue(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H);

		void				setName(const SWString &v)		{ m_pName->setText(v); }
		SWString				getName()						{ return m_pName->text(); }

		void				setValue(const SWString &v)		{ m_pValue->setText(v); }
		SWString				getValue()						{ return m_pValue->text(); }

protected:
		void			init(const SWString &name, int nameW, const SWString &value, int valueW, int H);

protected:
		FXStaticText	*m_pName;	///< The name widget
		FXStaticText	*m_pValue;	///< The value widget
		};


#endif // __FltkExt_FXStaticNamedValue_h__
