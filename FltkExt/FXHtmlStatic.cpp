#include "FXHtmlStatic.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>



FXHtmlStatic::FXHtmlStatic(int X, int Y, int W, int H, const SWString &text, Fl_Align textalign, Fl_Color fgcolor, Fl_Color bgcolor, int textheight, Fl_Font font, Fl_Boxtype boxtype) :
	Fl_Box(X, Y, W, H),
	m_text(text)
		{
		int		tw=0, th=0, dx=0, dy=0;

		if (fgcolor != 0) labelcolor(fgcolor);
		if (bgcolor != 0) color(bgcolor);
		label(m_text);
		box(boxtype);
		if (textheight != 0) labelsize(textheight);
		labelfont(font);
		align(FL_ALIGN_INSIDE|FL_ALIGN_CLIP|FL_ALIGN_WRAP|textalign);
		}


FXHtmlStatic::~FXHtmlStatic()
		{
		}

void
FXHtmlStatic::setTextColor(Fl_Color v)
		{
		labelcolor(v);
		}


void
FXHtmlStatic::setText(const SWString &v)
		{
		m_text = v;
		label(m_text);
		}


void
FXHtmlStatic::setText(const SWString &v, Fl_Color c)
		{
		m_text = v;
		label(m_text);
		labelcolor(c);
		}


