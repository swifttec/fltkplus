#ifndef __FltkExt_FXTextButton_h__
#define __FltkExt_FXTextButton_h__

#include <FltkExt/FXButton.h>


class FLTKEXT_DLL_EXPORT FXTextButton : public FXButton
		{
public:
		FXTextButton(sw_uint32_t id, const SWString &text, const FXFont &font=FXFont::getDefaultFont(FXFont::Button), int x=0, int y=0, int w=100, int h=100, const FXColor &fgcolor=FXColor(255, 255, 255), const FXColor &bgcolor=FXColor(100, 100, 100), const FXColor &frcolor=FXColor(0, 0, 0));
		FXTextButton(const SWString &text, const FXFont &font=FXFont::getDefaultFont(FXFont::Button), int x=0, int y=0, int w=100, int h=100, const FXColor &fgcolor=FXColor(255, 255, 255), const FXColor &bgcolor=FXColor(100, 100, 100), const FXColor &frcolor=FXColor(0, 0, 0));
		FXTextButton(int x, int y, int w, int h, const SWString &text, const FXFont &font=FXFont::getDefaultFont(FXFont::Button), const FXColor &fgcolor=FXColor(255, 255, 255), const FXColor &bgcolor=FXColor(100, 100, 100), const FXColor &frcolor=FXColor(0, 0, 0));
		virtual ~FXTextButton();

protected:
		void		initStyles(const FXFont &font);
		};

#endif // __FltkExt_FXTextButton_h__
