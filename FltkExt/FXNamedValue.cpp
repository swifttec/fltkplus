#include "FXNamedValue.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

#include <FltkExt/FXPackedLayout.h>

FXNamedValue::FXNamedValue(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(X, Y, nameW, valueW, H)
		{
		init(name, value);
		}


FXNamedValue::FXNamedValue(const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(0, 0, nameW, valueW, H)
		{
		init(name, value);
		}

void
FXNamedValue::init(const SWString &name, const SWString &value)
		{
		FXStaticText	*pValue;

		pValue = new FXStaticText(m_valueW, h()-2, value, FXFont("Arial", 11, FXFont::Normal), FX_VALIGN_MIDDLE|FX_HALIGN_LEFT, "#000", "#eee");
		pValue->setBorder(0, "#000", 1, 0);
		pValue->padding(FXPadding(0, 0, 4, 4));

		FXNamedWidget::init(name, pValue, value);
		}
