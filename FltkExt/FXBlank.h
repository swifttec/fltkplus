/*
**	FXBlank.h	A generic dialog class
*/

#ifndef __FltkExt_FXBlank_h__
#define __FltkExt_FXBlank_h__

#include <FltkExt/FXWidget.h>

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXBlank : public FXWidget
		{
public:
		FXBlank();
		FXBlank(int W, int H);
		FXBlank(int X, int Y, int W, int H);
		};


#endif // __FltkExt_FXBlank_h__
