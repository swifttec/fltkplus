#include "FXBox.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>


FXBox::FXBox(const FXColor &bgcolor)
		{
		setBackgroundColor(bgcolor);
		}

FXBox::FXBox(int X, int Y, int W, int H, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H)
		{
		setBackgroundColor(bgcolor);
		}