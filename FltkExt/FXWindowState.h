/*
**	FXWindowState.h		A class representing the state of a window
*/

#ifndef __FltkExt_FXWindowState_h__
#define __FltkExt_FXWindowState_h__

#include <FltkExt/FXPoint.h>
#include <FltkExt/FXSize.h>

#include <swift/sw_registry.h>

class FLTKEXT_DLL_EXPORT FXWindowState
		{
public:
		enum Flags
			{
			Visible		= 1,
			Minimized	= 2,
			Maximized	= 4,
			Fullscreen	= 8
			};

public:
		FXWindowState();

		bool			isMinimized() const		{ return ((flags & Minimized) != 0); }
		bool			isNormal() const		{ return isVisible() && !isMinimized() && !isMaximized() && !isFullscreen(); }
		bool			isMaximized() const		{ return ((flags & Maximized) != 0); }
		bool			isFullscreen() const	{ return ((flags & Fullscreen) != 0); }
		bool			isVisible() const		{ return ((flags & Visible) != 0); }

		/// Load the window state from the given file.
		sw_status_t		loadFromFile(const SWString &filename);

		/// Save the window state to the given file.
		sw_status_t		saveToFile(const SWString &filename);

		/// Load the window state from the given file.
		sw_status_t		loadFromRegistry(SWRegistryHive hive, const SWString &key);

		/// Save the window state to the given file.
		sw_status_t		saveToRegistry(SWRegistryHive hive, const SWString &key);

		/// Adjust the window state to ensure the window is descibed would be on a valid screen
		void			ensureOnScreen();

		/// Adjust the window state to ensure the window is maxmized
		void			maximizeOnScreen();

public:
		FXPoint		position;	///< The normal window position if not maximized or minimized
		FXSize		size;		///< The normal window size if not maximized or minimized
		int			flags;		///< The window flags (1=Normal, 2=Maximised, 3=Fullscreen).
								///< If negative then window is minimized (-1=Normal, -2=Maximised, -3=Fullscreen)
		};


#endif // __FltkExt_FXWindowState_h__