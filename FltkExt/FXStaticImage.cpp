#include "FXStaticImage.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

FXStaticImage::FXStaticImage(int X, int Y, int W, int H, const SWString &filename, int flags, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H),
	m_flags(flags),
	m_pImage(NULL),
	m_pImageCopy(NULL)
		{
		setBackgroundColor(bgcolor);
		load(filename);
		}



FXStaticImage::FXStaticImage(const SWString &filename, int flags, const FXColor &bgcolor) :
	FXWidget(0, 0, 32, 32),
	m_flags(flags),
	m_pImage(NULL),
	m_pImageCopy(NULL)
		{
		setBackgroundColor(bgcolor);
		load(filename);
		}



FXStaticImage::~FXStaticImage()
		{
		unload();
		}


void
FXStaticImage::unload()
		{
		if (m_pImageCopy != NULL)
			{
			delete m_pImageCopy;
			m_pImageCopy = NULL;
			}

		if (m_pImage != NULL)
			{
			m_pImage->release();
			m_pImage = NULL;
			}
		}


bool
FXStaticImage::load(const SWString &filename)
		{
		unload();

		m_pImage = Fl_Shared_Image::get(filename);

		return (m_pImage != NULL);
		}


void
FXStaticImage::onDraw()
		{
		if (m_pImage != NULL)
			{
			FXRect	rcDest;
			FXSize	szOriginal(m_pImage->w(), m_pImage->h());
			FXSize	szImage(szOriginal);

			getClientRect(rcDest);

			if ((m_flags & (FX_IMAGE_STRETCH_TO_WIDTH|FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_PROPORTIONAL)) == (FX_IMAGE_STRETCH_TO_WIDTH|FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_PROPORTIONAL))
				{
				szImage.cx = rcDest.width();
				szImage.cy = (rcDest.width() * szOriginal.height()) / szOriginal.width();

				if (szImage.cy < rcDest.height())
					{
					szImage.cy = rcDest.height();
					szImage.cx = (rcDest.height() * szOriginal.width()) / szOriginal.height();
					}

				if (szImage.cx > rcDest.width())
					{
					szImage.cx = rcDest.width();
					szImage.cy = (rcDest.width() * szOriginal.height()) / szOriginal.width();
					}
				}
			else
				{
				if ((m_flags & FX_IMAGE_STRETCH_TO_WIDTH) != 0)
					{
					szImage.cx = rcDest.width();

					if ((m_flags & FX_IMAGE_PROPORTIONAL) != 0)
						{
						szImage.cy = (rcDest.width() * szOriginal.height()) / szOriginal.width();
						}
					}

				if ((m_flags & FX_IMAGE_STRETCH_TO_HEIGHT) != 0)
					{
					szImage.cy = rcDest.height();

					if ((m_flags & FX_IMAGE_PROPORTIONAL) != 0)
						{
						szImage.cx = (rcDest.height() * szOriginal.width()) / szOriginal.height();
						}
					}
				}

			if (m_pImageCopy != NULL)
				{
				if (m_pImageCopy->w() != szImage.width() || m_pImageCopy->h() != szImage.height())
					{
					delete m_pImageCopy;
					m_pImageCopy = NULL;
					}
				}

			if (m_pImageCopy == NULL)
				{
				m_pImageCopy = m_pImage->copy(szImage.width(), szImage.height());
				}
			
			FXRect	rcDraw(rcDest);
			int		xoff=0, yoff=0;


			// Horizontal position
			if ((m_flags & FX_HALIGN_MASK) == FX_HALIGN_CENTRE)
				{
				int w = (rcDest.Width() - szImage.width()) / 2;

				if (w < 0)
					{
					xoff -= w;
					}
				else
					{
					rcDraw.left += w;
					rcDraw.right += w;
					}
				}
			else if ((m_flags & FX_HALIGN_MASK) == FX_HALIGN_RIGHT)
				{
				int w = rcDest.Width() - szImage.width();

				if (w < 0)
					{
					xoff -= w;
					}
				else
					{
					rcDraw.left += w;
					rcDraw.right += w;
					}
				}

			// Vertical position
			if ((m_flags & FX_VALIGN_MASK) == FX_VALIGN_CENTRE)
				{
				int h = (rcDest.Height() - szImage.height()) / 2;

				if (h < 0)
					{
					yoff -= h;
					}
				else
					{
					rcDraw.top += h;
					rcDraw.bottom += h;
					}
				}
			else if ((m_flags & FX_VALIGN_MASK) == FX_VALIGN_BOTTOM)
				{
				int h = rcDest.Height() - szImage.height();

				if (h < 0)
					{
					yoff -= h;
					}
				else
					{
					rcDraw.top += h;
					rcDraw.bottom += h;
					}
				}
				
			m_pImageCopy->draw(rcDraw.left, rcDraw.top, rcDraw.width(), rcDraw.height(), xoff, yoff);
			}
		}
