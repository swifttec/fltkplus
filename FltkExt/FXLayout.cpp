#include <FltkExt/FXLayout.h>

FXLayout::FXLayout(int flags) :
	m_pOwner(NULL),
	m_flags(flags)
		{
		}


FXLayout::~FXLayout()
		{
		}


FXSize
FXLayout::getMinSize() const
		{
		FXSize	res;

		return res;
		}


FXSize
FXLayout::getMaxSize() const
		{
		FXSize	res;

		return res;
		}
		

void
FXLayout::setOwner(Fl_Group *pOwner)
		{
		if (m_pOwner != NULL && pOwner != NULL)
			{
			/// We cannot have 2 owners - throw an exception
			throw SW_EXCEPTION_TEXT(SWException, "FXLayout already has non-null owner");
			}

		m_pOwner = pOwner;
		}


void
FXLayout::addWidget(Fl_Widget *pWidget)
		{
		if (m_pOwner != NULL)
			{
			m_pOwner->add(pWidget);
			}
		}


int
FXLayout::getChildCount() const
		{
		int		res=0;

		if (m_pOwner != NULL)
			{
			res = m_pOwner->children();
			}

		return res;
		}


void
FXLayout::layoutChildren()
		{
		// DO nothing
		}
