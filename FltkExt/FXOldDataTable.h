#ifndef __FltkExt_FXDataTable_h__
#define __FltkExt_FXDataTable_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXButtonBar.h>

#include <FL/Fl_Table.H>

#include <swift/SWArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWThreadMutex.h>

/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXDataTable : public Fl_Table
		{
public:
		class Column
			{
		public:
			SWString	title;		///< The column title
			int		align;		///< The column alignment
			int		minWidth;	///< The minimum column width
			int		maxWidth;	///< The maximum column width (0=unlimited)
			int		width;		///< The current width
			};

		class Cell
			{
		public:
			Cell()
				{
				align = FX_HALIGN_LEFT;
				fgcolor = bgcolor = FX_COLOR_DEFAULT;
				}

		public:
			SWString	contents;	///< The cell contents
			int		align;		///< The cell alignment
			FXSize	size;		///< The current size
			FXColor	fgcolor;	///< The cell foreground color
			FXColor	bgcolor;	///< The cell background color
			};

		class Row
			{
		public:
			Row()
				{
				fgcolor = bgcolor = FX_COLOR_DEFAULT;
				}

		public:
			SWArray<Cell>	cells;		///< The cell array
			FXSize				size;		///< The current size
			FXColor				fgcolor;	///< The row foreground color
			FXColor				bgcolor;	///< The row background color
			};

public:
		FXDataTable(int X=0, int Y=0, int W=10, int H=10);
		virtual ~FXDataTable();

		/// Set the border color
		void			setBorderColor(const FXColor &v)		{ m_bdColor = v; }

		/// Get the border color
		const FXColor &	getBorderColor() const					{ return m_bdColor; }

		/// Set the foreground / text color
		void			setForegroundColor(const FXColor &v)	{ m_fgColor = v; }

		/// Get the foreground color
		const FXColor &	getForegroundColor() const				{ return m_fgColor; }

		/// Set the foreground / text color
		void			setBackgroundColor(const FXColor &v)	{ m_bgColor = v; }

		/// Get the background color
		const FXColor &	getBackgroundColor() const				{ return m_bgColor; }
		
		void			addColumn(const SWString &title, int align);

		int				getColumnCount() const	{ return (int)m_columns.size(); }
		int				getRowCount() const		{ return (int)m_rows.size(); }

		void			removeAllRows();
		int				addRow(bool batchmode=false);
		int				addRow(const SWStringArray &values, bool batchmode=false);
		void			setRowValues(int row, const SWStringArray &values);
		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);
		int				insertRow(int pos, bool batchmode=false);
		int				insertRow(int pos, const SWStringArray &values, bool batchmode=false);
		void			deleteRow(int pos, bool batchmode=false);
		void			endBatchMode();

		void			setCell(int row, int col, const SWString &contents, const FXColor &fgcolor=FX_COLOR_DEFAULT, const FXColor &bgcolor=FX_COLOR_DEFAULT);
		void			setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor);
		void			setCellString(int row, int col, const SWString &contents);
		SWString			getCellString(int row, int col) const;

		void			invertRowColorsOnSelection(bool v)	{ m_invertRowColorsOnSelection = v; }
		void			invertCellColorsOnSelection(bool v)	{ m_invertCellColorsOnSelection = v; }

protected: /// FLTK overrides

		virtual void	draw_cell(TableContext context,
								int  	R = 0,
								int  	C = 0,
								int  	X = 0,
								int  	Y = 0,
								int  	W = 0,
								int  	H = 0);

		virtual int		handle(int event);
		virtual void	draw();


protected:
		void			recalculate();
		void			calculateRowSize(Row &row);
		void			removeAllRowData();
		void			getCellColors(int row, int col, FXColor &fgcolor, FXColor &bgcolor);

		static void		onEventCallback(Fl_Widget*, void* data);
		virtual void	onCallback();

protected:
		mutable SWThreadMutex	m_mutex;
		SWArray<Column>	m_columns;
		SWArray<Row *>		m_rows;
		Row						m_headerRow;				///< The header row

		bool					m_cellsChanged;
		bool					m_columnsChanged;
		FXFont					m_headerFont;				///< The font for the header cells
		FXColor					m_rowForegroundColor[2];	///< The colour of the even/odd row text
		FXColor					m_rowBackgroundColor[2];	///< The colour of the even/odd row background
		FXColor					m_selectedRowForegroundColor[2];	///< The colour of the even/odd row text
		FXColor					m_selectedRowBackgroundColor[2];	///< The colour of the even/odd row background
		FXFont					m_cellFont;					///< The font for table cells
		int						m_columnGap;				///< The gap between columns
		int						m_rowGap;					///< The gap between rows
		FXRect					m_tableRect;				///< The table area rect
		FXColor					m_tableBackgroundColor;		///< The background colour of the table area
		FXColor					m_tableBorderColor;			///< The border color of the table area
		FXPadding				m_cellPadding;				///< The padding to apply to each cell

		FXColor			m_bdColor;		///< Border color
		FXColor			m_bgColor;		///< Background color
		FXColor			m_fgColor;		///< Foreground/Text color
		bool			m_invertCellColorsOnSelection;
		bool			m_invertRowColorsOnSelection;
		};

#endif // __FltkExt_FXDataTable_h__
