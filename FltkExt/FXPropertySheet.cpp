#include "FXPropertySheet.h"

#include <FL/Fl.H>
#include <FL/Fl_Box.H>

#include <swift/sw_math.h>


void
FXPropertySheet::classCallback(Fl_Widget *pWidget, void *pUserData)
		{
		FXPropertySheet	*pSheet=(FXPropertySheet *)pUserData;

		pSheet->onCallback(pWidget);
		}


FXPropertySheet::FXPropertySheet(int X, int Y, int W, int H, const SWString &title, Fl_Widget *pOwner, int buttons) :
	Fl_Double_Window(X, Y, W, H),
	m_title(title),
	m_pOwner(pOwner),
	m_tabHeight(24),
	m_btnSpace(32),
	m_btnHeight(24),
	m_gapTop(1),
	m_gapBottom(4),
	m_loaded(false),
	m_result(0)
		{
		label(m_title);

		// Set up a FX callback
		callback(classCallback, this);

		m_clientHeight = H-m_tabHeight-m_btnSpace-m_gapTop-m_gapBottom;

		m_pTabs = new Fl_Tabs(X, Y, W, H-m_btnSpace);
		m_pTabs->end();

		int		xpos=X+W-104;
		int		ypos=Y+H-m_btnSpace;

		m_pBtnApply = m_pBtnCancel = m_pBtnClose = m_pBtnOK = NULL;

		if ((buttons & BtnApply) != 0)
			{
			m_pBtnApply = new Fl_Button(xpos, ypos + ((m_btnSpace - m_btnHeight) / 2), 100, m_btnHeight, "Apply");
			m_pBtnApply->callback(classCallback, this);
			xpos -= 104;
			}

		if ((buttons & BtnCancel) != 0)
			{
			m_pBtnCancel = new Fl_Button(xpos, ypos + ((m_btnSpace - m_btnHeight) / 2), 100, m_btnHeight, "Cancel");
			m_pBtnCancel->callback(classCallback, this);
			xpos -= 104;
			}

		if ((buttons & BtnOK) != 0)
			{
			m_pBtnOK = new Fl_Button(xpos, ypos + ((m_btnSpace - m_btnHeight) / 2), 100, m_btnHeight, "OK");
			m_pBtnOK->callback(classCallback, this);
			xpos -= 104;
			}

		if ((buttons & BtnClose) != 0)
			{
			m_pBtnClose = new Fl_Button(xpos, ypos + ((m_btnSpace - m_btnHeight) / 2), 100, m_btnHeight, "Close");
			m_pBtnClose->callback(classCallback, this);
			xpos -= 104;
			}

		end();
		}


FXPropertySheet::~FXPropertySheet()
		{
		}



void
FXPropertySheet::onCallback(Fl_Widget *pWidget)
		{
		if (pWidget == m_pBtnOK || pWidget == m_pBtnClose)
			{
			onOK();
			}
		else if (pWidget == m_pBtnApply)
			{
			onApply();
			}
		else if (pWidget == m_pBtnCancel || pWidget == this)
			{
			onCancel();
			}
		}


void
FXPropertySheet::addPage(FXPropertyPage *pPage)
		{
		int		rx=0, ry=m_tabHeight, rw=w(), rh=h();

		// m_pTabs->client_area(rx, ry, rw, rh);

		Fl_Group	*pGroup = new Fl_Group(rx, ry, rw, rh, pPage->pagetitle());

		if (pPage->pageid() == 0)
			{
			int	maxid=0;

			for (int i=0;i<(int)m_pages.size();i++)
				{
				maxid = sw_math_max(maxid, m_pages[i]->pageid());
				}

			pPage->m_pageid = maxid + 1;
			}

		// Make sure the page knows who owns it.
		pPage->m_pSheet = this;

		pGroup->add(pPage);

		// Fl_Tabs seems to mess with sizes and position, so we do the following:
		// Add the group, then resize it back to calculated size
		// then resize the page we added to the group to the same size
		m_pTabs->add(pGroup);
 		pGroup->resize(rx, ry, rw, rh);

		// Resize and reposition the widget to match the sheet
 		pPage->resize(rx, ry, rw, rh);
		pPage->onCreate();
		m_pages.add(pPage);
		}


FXPropertyPage *
FXPropertySheet::getPageByID(int id)
		{
		FXPropertyPage	*pPage=NULL, *p;

		for (int i=0;i<(int)m_pages.size();i++)
			{
			p = m_pages[i];
			if (p->pageid() == id)
				{
				pPage = p;
				break;
				}
			}

		return pPage;
		}


void
FXPropertySheet::loadAllData()
		{
		for (int i=0;i<(int)m_pages.size();i++)
			{
			m_pages[i]->onLoadData();
			}
		}


void
FXPropertySheet::saveAllData()
		{
		for (int i=0;i<(int)m_pages.size();i++)
			{
			m_pages[i]->onSaveData();
			}
		}

void
FXPropertySheet::draw()
		{
		if (!m_loaded)
			{
			loadAllData();
			m_loaded = true;

			setModified();
			}

		Fl_Double_Window::draw();
		}


void
FXPropertySheet::onOK()
		{
		if (m_loaded)
			{
			saveAllData();
			}
		
		endDialog(0);
		}


void
FXPropertySheet::onApply()
		{
		if (m_loaded)
			{
			saveAllData();
			}
		}


void
FXPropertySheet::onCancel()
		{
		endDialog(-1);
		}


void
FXPropertySheet::updateIcon()
		{
		}

int
FXPropertySheet::doModal()
		{
		fx_widget_centerOver(this, m_pOwner);

		set_modal();
		show();
		
		// updateIcon only works after a show
		updateIcon();

		while (shown())
			{
			fx_invalidate_process();
			Fl::wait();
			}
		
		set_non_modal();

		return m_result;
		}


void
FXPropertySheet::endDialog(int r)
		{
		m_result = r;
		hide();	
		}


void
FXPropertySheet::setModified()
		{
		bool	changed=false, enabled=true;

		for (int i=0;i<(int)m_pages.size();i++)
			{
			FXPropertyPage	*pPage=m_pages[i];
			
			if (pPage->modified()) changed = true;
			if (!pPage->canEnableOK()) enabled = false;
			}

		if (changed && enabled)
			{
			if (m_pBtnOK != NULL) m_pBtnOK->activate();
			if (m_pBtnApply != NULL) m_pBtnApply->activate();
			}
		else
			{
			if (m_pBtnOK != NULL) m_pBtnOK->deactivate();
			if (m_pBtnApply != NULL) m_pBtnApply->deactivate();
			}
		}


void
FXPropertySheet::setTitle(const SWString &text)
		{
		copy_label(text);
		}

