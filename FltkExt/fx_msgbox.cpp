/****************************************************************************
**	fx_msgbox.cpp	Message box functions
****************************************************************************/

#include <FltkExt/fx_msgbox.h>
#include <FltkExt/fx_generic_ids.h>

//#include <FltkExt/CHtmlMessageBox.h>
#include <FL/fl_ask.H>

FLTKEXT_DLL_EXPORT int
fx_msgbox_text(const SWString &text, sw_uint32_t nType, sw_uint32_t nIDHelp, double maxRatio)
		{
		int		r=0;

		switch (nType & 0xf)
			{
			case FXMB_OK:
				fl_alert(text);
				r = FX_IDOK;
				break;

			case FXMB_OKCANCEL:
				r = fl_choice(text, "OK", "Cancel", NULL);
				if (r == 0) r = FX_IDOK;
				else r = FX_IDCANCEL;
				break;

			case FXMB_ABORTRETRYIGNORE:
				r = fl_choice(text, "Abort", "Retry", "Ignore", NULL);
				if (r == 0) r = FX_IDABORT;
				else if (r == 1) r = FX_IDRETRY;
				else r = FX_IDIGNORE;
				break;

			case FXMB_YESNOCANCEL:
				r = fl_choice(text, "Yes", "No", "Cancel", NULL);
				if (r == 0) r = FX_IDYES;
				else if (r == 1) r = FX_IDNO;
				else r = FX_IDCANCEL;
				break;
			
			case FXMB_YESNO:
				r = fl_choice(text, "Yes", "No", NULL);
				if (r == 0) r = FX_IDYES;
				else r = FX_IDNO;
				break;
			
			case FXMB_RETRYCANCEL:
				r = fl_choice(text, "Retry", "Cancel", NULL);
				if (r == 0) r = FX_IDRETRY;
				else r = FX_IDCANCEL;
				break;
			}

		return r;
		}


FLTKEXT_DLL_EXPORT int
fx_msgbox_text(sw_uint32_t id, sw_uint32_t nType, sw_uint32_t nIDHelp, double maxRatio)
		{
		SWString	s;

		s.LoadString(id);

		return fx_msgbox_text(s, nType, nIDHelp, maxRatio);
		}

/*
FLTKEXT_DLL_EXPORT int
fx_msgbox_html(sw_uint32_t id, sw_uint32_t nType, sw_uint32_t nIDHelp, double maxRatio)
		{
		SWString	s;

		s.LoadString(id);

		return FXHtmlMessageBox(s, nType, nIDHelp, maxRatio);
		}



FLTKEXT_DLL_EXPORT int
fx_msgbox_html(const SWString &text, sw_uint32_t nType, sw_uint32_t nIDHelp, double maxRatio)
		{
		CHtmlMessageBox	dlg(text, nType, nIDHelp, maxRatio);
		int		r;

		dlg.Create();
		r = dlg.DoModal();

		return r;
		}
*/
