#include "FXPropertyPage.h"
#include "FXPropertySheet.h"
#include "FXStaticText.h"

#include <FL/Fl.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Check_Button.H>

void
FXPropertyPage::childCallback(Fl_Widget *pWidget)
		{
		FXPropertyPage	*pSheet=(FXPropertyPage *)pWidget->parent();

		pSheet->onCallback(pWidget);
		}


FXPropertyPage::FXPropertyPage(const SWString &title) :
	Fl_Group(0, 0, 100, 100),
	m_pageid(0),
	m_pagetitle(title),
	m_modified(false)
		{
		resizable(NULL);
		}


FXPropertyPage::FXPropertyPage(int id, const SWString &title) :
	Fl_Group(0, 0, 100, 100),
	m_pageid(id),
	m_pagetitle(title),
	m_modified(false)
		{
		resizable(NULL);
		}

FXPropertyPage::~FXPropertyPage()
		{
		}

void
FXPropertyPage::onCreate()
		{
		}


bool
FXPropertyPage::canEnableOK()
		{
		// By default we can enable ok
		return true;
		}

void
FXPropertyPage::setModified()
		{
		FXPropertySheet	*pSheet=(FXPropertySheet *)parent();

		m_modified = true;
		m_pSheet->setModified();
		}



void
FXPropertyPage::doDataExchange(FXDataExchange *pDX)
		{
		// By default do nothing - should be overridden by any deriving class
		}


void
FXPropertyPage::updateData(bool save)
		{
		FXDataExchange	dx(this, save?FXDataExchange::Save:FXDataExchange::Load);

		doDataExchange(&dx);
		}


void
FXPropertyPage::onCallback(Fl_Widget *pWidget)
		{
		if (pWidget->changed()) setModified();
		}


void
FXPropertyPage::addItem(int itemid, Fl_Widget *pItem)
		{
		fx_group_addItem(this, itemid, pItem);

		pItem->callback(childCallback);
		pItem->when(FL_WHEN_CHANGED);
		}

void
FXPropertyPage::enableItem(int itemid, bool v)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}

void
FXPropertyPage::onLoadData()
		{
		// By default do nothing
		}


void
FXPropertyPage::onSaveData()
		{
		// By default do nothing
		}

void
FXPropertyPage::onShowPage()
		{
		// By default do nothing
		}


FXColor
FXPropertyPage::getBackgroundColor()
		{
		sw_byte_t	r, g, b;

		Fl::get_color(color(), r, g, b);
		
		return FXColor(r, g, b);
		}

