#ifndef __FltkExt_FXFontFamily_h__
#define __FltkExt_FXFontFamily_h__

#include <FltkExt/FltkExt.h>
#include <swift/SWString.h>
#include <swift/SWArray.h>

/**
*** A class to represent a font family
**/
class FLTKEXT_DLL_EXPORT FXFontFamily
		{
public:
		/// Default constructor
		FXFontFamily();

		const SWString &		name() const			{ return m_name; }

		int						fl_font_face(int v) const;

		bool					hasStyle(int v) const;

		bool					isScalable() const		{ return (m_fontsizes.size() == 0); }
		const SWIntegerArray &	getFontSizes() const	{ return m_fontsizes; }

public: // Static methods
		static bool					isFamilyListLoaded();
		static void					loadFamilyList(bool reload=false);
		static void					getFamilyList(SWArray<FXFontFamily> &fontlist);
		static int					getFamilyCount();
		static const FXFontFamily &	getFamily(int idx);
		static int					find(const SWString &name);
		static void					find(int fontid, int &family, int &style);

		static SWString	variantName(int v);

private: // Members
		SWString		m_name;			///< The font name
		SWIntegerArray	m_fontnum;		///< The FLTK font numbers (one per attribute) (0=Normal, 1=
		SWIntegerArray	m_fontsizes;	///< The font sizes
		};


#endif // __FXFontFamily_h__
