#include <FltkExt/FXImageBuffer.h>

#include <FL/fl_draw.H>

#define RGBA_TO_UINT32(r, g, b, a)		((a << 24) | (b << 16) | (g << 8) | r)
#define RGBA_RED(v)						(v & 0xff)
#define RGBA_GREEN(v)					((v >> 8) & 0xff)
#define RGBA_BLUE(v)					((v >> 16) & 0xff)
#define RGBA_ALPHA(v)					((v >> 24) & 0xff)
#define CLAMP(v, vmin, vmax)			(v < vmin?vmin:(v > vmax?vmax:v))
 
FXImageBuffer::FXImageBuffer() :
	m_bytesPerPixel(4),
	m_pData(NULL)
		{
		}


FXImageBuffer::~FXImageBuffer()
		{
		safe_delete_array(m_pData);
		}


void
FXImageBuffer::setSize(int w, int h)
		{
		if (m_size.cx != w || m_size.cy != h)
			{
			sw_uint32_t	*pOldData=m_pData;

			if (w != m_size.cx || h != m_size.cy)
				{
				if (w != 0 && h != 0)
					{
					m_pData = new sw_uint32_t[w * h];
					memset(m_pData, 0, w * h * m_bytesPerPixel);
					}
				else
					{
					m_pData = NULL;
					}

				if (pOldData != NULL && m_pData != NULL)
					{
					int			oldBytesPerLine=m_size.cx * m_bytesPerPixel;
					int			newBytesPerLine=w * m_bytesPerPixel;
					int			bytesToCopy=oldBytesPerLine;
					int			linesToCopy=m_size.cy;

					if (w < m_size.cx) bytesToCopy = newBytesPerLine;
					if (linesToCopy > h) linesToCopy = h;

					sw_byte_t	*in=(sw_byte_t *)pOldData;
					sw_byte_t	*out=(sw_byte_t *)m_pData;

					for (int i=0;i<linesToCopy;i++)
						{
						memcpy(out, in, bytesToCopy);
						out += newBytesPerLine;
						in += oldBytesPerLine;
						}
					}

				m_size.cx = w;
				m_size.cy = h;
				}

			safe_delete_array(pOldData);
			}
		}


void
FXImageBuffer::load(sw_byte_t *data, int width, int height, int bytesPerPixel)
		{
		int		npixels=width * height;

		setSize(width, height);
		if (bytesPerPixel == 4)
			{
			memcpy(m_pData, data, npixels * bytesPerPixel);
			}
		else if (bytesPerPixel == 3)
			{
			// Convert from RGB to RGBA
			sw_uint32_t	*bp=m_pData;
			sw_byte_t	*dp=data, r, g, b;

			while (npixels-- > 0)
				{
				r = *dp++;
				g = *dp++;
				b = *dp++;
				*bp++ = RGBA_TO_UINT32(r, g, b, 255);
				}
			}
		}


void
FXImageBuffer::draw(const FXRect &dr, const FXRect &sr)
		{
		draw(dr.left, dr.top, dr.width(), dr.height(), sr.left, sr.top);
		}


void
FXImageBuffer::draw(int posX, int posY, int W, int H, int offX, int offY)
		{
		if ((offX + W) > m_size.cx) W = m_size.cx - offX;
		if ((offY + H) > m_size.cy) H = m_size.cy - offY;

		if (W > 0 && H > 0)
			{
			sw_byte_t	*ip=(sw_byte_t*)m_pData + (offY * m_size.cx * m_bytesPerPixel) + (offX * m_bytesPerPixel);

			fl_draw_image(ip, posX, posY, W, H, m_bytesPerPixel, m_size.cx * m_bytesPerPixel);
			}
		}


void
FXImageBuffer::setPixel(int x, int y, const FXColor &color)
		{
		int		r, g, b;

		color.getComponents(8, r, g, b);

		if (x >= 0 && x < m_size.cx && y >= 0 && y < m_size.cy)
			{
			sw_uint32_t	*bp=m_pData + x + (y * m_size.cx);

			*bp = RGBA_TO_UINT32(r, g, b, 255);
			}
		}

void
FXImageBuffer::setPixel(int x, int y, sw_byte_t r, sw_byte_t g, sw_byte_t b)
		{
		if (x >= 0 && x < m_size.cx && y >= 0 && y < m_size.cy)
			{
			sw_uint32_t	*bp=m_pData + x + (y * m_size.cx);

			*bp = RGBA_TO_UINT32(r, g, b, 255);
			}
		}


void
FXImageBuffer::fill(sw_byte_t r, sw_byte_t g, sw_byte_t b)
		{
		if (m_pData != NULL)
			{
			sw_uint32_t	v=RGBA_TO_UINT32(r, g, b, 255);
			sw_uint32_t	*bp=m_pData;
			int			npixels=m_size.cx * m_size.cy;

			while (npixels-- > 0)
				{
				*bp++ = v;
				/*
				memcpy(bp, &v, 4);
				bp += 4;
				*/
				}
			}
		}


/// Fill the entire image with the specified color
void
FXImageBuffer::fill(const FXColor &color)
		{
		int		r, g, b;

		color.getComponents(8, r, g, b);

		fill(r, g, b);
		}


void
FXImageBuffer::clear()
		{
		safe_delete_array(m_pData);
		m_size.cx = m_size.cy = 0;
		}


void
FXImageBuffer::copy(const FXImageBuffer &other)
		{
		clear();
		if (other.m_pData != NULL && other.m_size.cx != 0 && other.m_size.cy != 0)
			{
			setSize(other.m_size.cx, other.m_size.cy);
			memcpy(m_pData, other.m_pData, m_bytesPerPixel * m_size.cx * m_size.cy);
			}
		}



void
FXImageBuffer::alphaBlend(const FXImageBuffer &other, int alphalevel)
		{
		if (m_pData != NULL && other.m_pData != NULL)
			{
			if (m_size.cx == other.m_size.cx && m_size.cy == other.m_size.cy && m_bytesPerPixel == other.m_bytesPerPixel)
				{
				// Quick method for all pixels
				/*
				sw_byte_t	*p=(sw_byte_t *)m_pData;
				sw_byte_t	*q=(sw_byte_t *)other.m_pData;
				int			npixels=m_size.cx * m_size.cy;
				int			nbytes=npixels * m_bytesPerPixel;

				while (nbytes-- > 0)
					{
					*p++ = CLAMP(*p + ((*q * alphalevel) / 255), 0, 255);
					q++;
					}
				*/
				sw_uint32_t	*p=m_pData;
				sw_uint32_t	*q=other.m_pData;
				sw_uint32_t	vq, vp;
				int			npixels=m_size.cx * m_size.cy;

				while (npixels-- > 0)
					{
					vq = *q++;
					vp = *p;
					*p++ = RGBA_TO_UINT32(
						CLAMP(RGBA_RED(vp) + ((RGBA_RED(vq) * alphalevel) / 255), 0, 255),
						CLAMP(RGBA_GREEN(vp) + ((RGBA_GREEN(vq) * alphalevel) / 255), 0, 255),
						CLAMP(RGBA_BLUE(vp) + ((RGBA_BLUE(vq) * alphalevel) / 255), 0, 255),
						255);
					}
				}
			else
				{
				int		w=m_size.cx, h=m_size.cy;

				if (w > other.m_size.cx) w = other.m_size.cx;
				if (h > other.m_size.cy) h = other.m_size.cy;

				for (int y=0;y<h;y++)
					{
					sw_uint32_t	*p=m_pData + (y * m_size.cx);
					sw_uint32_t	*q=other.m_pData + (y * other.m_size.cx);
					sw_uint32_t	vq, vp;
					int			npixels=m_size.cx;

					while (npixels-- > 0)
						{
						vq = *q++;
						vp = *p;
						*p++ = RGBA_TO_UINT32(
							CLAMP(RGBA_RED(vp) + ((RGBA_RED(vq) * alphalevel) / 255), 0, 255),
							CLAMP(RGBA_GREEN(vp) + ((RGBA_GREEN(vq) * alphalevel) / 255), 0, 255),
							CLAMP(RGBA_BLUE(vp) + ((RGBA_BLUE(vq) * alphalevel) / 255), 0, 255),
							255);
						}
					}
				}
			}
		}

