#include "FXMenu.h"
#include "FXMenuItem.h"
#include "FXMenuWindow.h"

#include <swift/SWTokenizer.h>


FXFont	*FXMenu::m_pDefaultFont = NULL;		///< The default font for items in this menu
FXColor	*FXMenu::m_pDefaultTextColor = NULL;	///< The default text color for items in this menu
FXColor	*FXMenu::m_pDefaultBackgroundColor = NULL;	///< The default text color for items in this menu
FXColor	*FXMenu::m_pDefaultSelectedBackgroundColor = NULL;	///< The default text color for items in this menu
int		FXMenu::m_defaultBoxType = FL_UP_BOX;	///< The default text color for items in this menu
int		FXMenu::m_defaultDownBoxType = FL_FLAT_BOX;	///< The default text color for items in this menu


FXFont *
FXMenu::defaultFont()
		{
		if (m_pDefaultFont == NULL) m_pDefaultFont = new FXFont("Arial", 12, FXFont::Normal);

		return m_pDefaultFont;
		}


FXColor *
FXMenu::defaultTextColor()
		{
		if (m_pDefaultTextColor == NULL) m_pDefaultTextColor = new FXColor("#000");

		return m_pDefaultTextColor;
		}


FXColor *
FXMenu::defaultBackgroundColor()
		{
		if (m_pDefaultBackgroundColor == NULL) m_pDefaultBackgroundColor = new FXColor(FL_GRAY);

		return m_pDefaultBackgroundColor;
		}


FXColor *
FXMenu::defaultSelectedBackgroundColor()
		{
		if (m_pDefaultSelectedBackgroundColor == NULL) m_pDefaultSelectedBackgroundColor = new FXColor(FL_SELECTION_COLOR);

		return m_pDefaultSelectedBackgroundColor;
		}


Fl_Boxtype
FXMenu::defaultDownBoxType()
		{
		return (Fl_Boxtype)m_defaultDownBoxType;
		}


Fl_Boxtype
FXMenu::defaultBoxType()
		{
		return (Fl_Boxtype)m_defaultBoxType;
		}


FXMenu::FXMenu(FXMenuItem *pOwner) :
	m_pOwner(pOwner),
	m_pFont(NULL),
	m_pTextColor(NULL),
	m_pBackgroundColor(NULL),
	m_pSelectedBackgroundColor(NULL),
	m_downBoxType(FL_FLAT_BOX)
		{
		}


FXMenu::FXMenu(const FXMenu &other)
		{
		*this = other;
		}


FXMenu::~FXMenu()
		{
		clear();
		}


FXMenu &
FXMenu::operator=(const FXMenu &other)
		{
		clear();
		for (int i = 0; i < (int)other.m_itemlist.size(); i++)
			{
			add(*other.m_itemlist.get(i));
			}

		return *this;
		}


void
FXMenu::clear()
		{
		for (int i = 0; i < (int)m_itemlist.size(); i++)
			{
			safe_delete(m_itemlist[i]);
			}

		m_itemlist.clear();
		}


FXMenuItem *
FXMenu::add(const FXMenuItem &item)
		{
		return insert(-1, item);
		}


FXMenuItem *
FXMenu::add(sw_uint32_t id, const SWString &text, sw_uint32_t shortcut, Fl_Callback *pCallback, void *pUserData, sw_uint32_t flags)
		{
		return insert(-1, id, text, shortcut, pCallback, pUserData, flags);
		}


FXMenuItem *
FXMenu::insert(int pos, const FXMenuItem &item)
		{
		if (pos < 0 || pos > (int)size()) pos = (int)size();

		FXMenuItem	*pItem = new FXMenuItem(item);

		m_itemlist.insert(pItem, pos);
		pItem->m_pOwner = this;

		return pItem;
		}


FXMenuItem *
FXMenu::insert(int pos, sw_uint32_t id, const SWString &text, sw_uint32_t shortcut, Fl_Callback *pCallback, void *pUserData, sw_uint32_t flags)
		{
		FXMenu		*pMenu=this;
		SWString	itemtext;

		if (text.first('/') > 0)
			{
			SWTokenizer	tok(text, "/|");
			int			ntokens=tok.getTokenCount();
			FXMenuItem	*pItem;

			for (int i=0;i<ntokens-1;i++)
				{
				SWString	name=tok.getToken(i);
				bool		subdivider=false;

				if (name.startsWith("_"))
					{
					name.remove(0, 1);
					subdivider = true;
					}

				pItem = pMenu->findItemByPath(name);

				if (pItem == NULL)
					{
					FXMenuItem	subitem;

					subitem.text(name);
					pItem = pMenu->add(subitem);
					}

				if (pItem->m_pSubMenu == NULL)
					{
					pItem->m_pSubMenu = new FXMenu(pItem);
					}

				pMenu = pItem->m_pSubMenu;

				if (subdivider)
					{
					pItem->flags(pItem->flags() | FXMenuItem::SeparatorAfter);
					}
				}

			itemtext = tok.getToken(ntokens - 1);
			}
		else
			{
			itemtext = text;
			}

		while (itemtext.startsWith("_")
			|| itemtext.startsWith(".")
			|| itemtext.startsWith(":")
			)
			{
			if (itemtext.startsWith("_"))
				{
				itemtext.remove(0, 1);
				flags |= FXMenuItem::SeparatorAfter;
				}

			if (itemtext.startsWith("."))
				{
				itemtext.remove(0, 1);
				flags |= FXMenuItem::CheckBox;
				flags &= ~FXMenuItem::Radio;
				}

			if (itemtext.startsWith(":"))
				{
				itemtext.remove(0, 1);
				flags |= FXMenuItem::Radio;
				flags &= ~FXMenuItem::CheckBox;
				}
			}

		FXMenuItem	item;

		item.id(id);
		item.text(itemtext);
		item.shortcut(shortcut);
		item.callback(pCallback);
		item.user_data(pUserData);
		item.flags(flags);

		return pMenu->insert(pos, item);
		}


int
FXMenu::indexOf(FXMenuItem *pItem) const
		{
		int		res=-1;

		for (int i=0;i<(int)m_itemlist.size();i++)
			{
			if (m_itemlist.get(i) == pItem)
				{
				res = i;
				break;
				}
			}
		
		return res;
		}


FXSize
FXMenu::measureItemAt(int idx) const
		{
		FXSize		res;
		FXMenuItem	*pItem=getItemAt(idx);

		if (pItem != NULL) res = pItem->measure(this);

		return res;
		}


FXMenuItem *
FXMenu::getItemAt(int idx) const
		{
		FXMenuItem	*pItem = NULL;

		if (idx >= 0 && idx < (int)m_itemlist.size())
			{
			pItem = m_itemlist.get(idx);
			}

		return pItem;
		}


void
FXMenu::removeItemAt(int idx)
		{
		if (idx >= 0 && idx < (int)m_itemlist.size())
			{
			safe_delete(m_itemlist[idx]);
			m_itemlist.remove(idx);
			}
		}


FXFont *
FXMenu::font() const
		{
		FXFont	*p = FXMenu::defaultFont();

		if (m_pFont != NULL) p = m_pFont;
		else if (m_pOwner != NULL) p = m_pOwner->font();

		return p;
		}


FXColor *
FXMenu::textColor() const
		{
		FXColor	*p = FXMenu::defaultTextColor();

		if (m_pTextColor != NULL) p = m_pTextColor;
		else if (m_pOwner != NULL) p = m_pOwner->textColor();

		return p;
		}


FXColor *
FXMenu::backgroundColor() const
		{
		FXColor	*p = FXMenu::defaultBackgroundColor();

		if (m_pBackgroundColor != NULL) p = m_pBackgroundColor;
		else if (m_pOwner != NULL) p = m_pOwner->backgroundColor();

		return p;
		}


FXColor *
FXMenu::selectedBackgroundColor() const
		{
		FXColor	*p = FXMenu::defaultSelectedBackgroundColor();

		if (m_pSelectedBackgroundColor != NULL) p = m_pSelectedBackgroundColor;
		else if (m_pOwner != NULL) p = m_pOwner->selectedBackgroundColor();

		return p;
		}


Fl_Boxtype
FXMenu::box() const
		{
		Fl_Boxtype	res = (Fl_Boxtype)m_defaultBoxType;

		if (m_boxType >= 0) res = (Fl_Boxtype)m_boxType;
		else if (m_pOwner != NULL && m_pOwner->m_pOwner != NULL) res = m_pOwner->m_pOwner->box();

		return res;
		}


Fl_Boxtype
FXMenu::down_box() const
		{
		Fl_Boxtype	res = (Fl_Boxtype)m_defaultDownBoxType;

		if (m_downBoxType >= 0) res = (Fl_Boxtype)m_downBoxType;
		else if (m_pOwner != NULL && m_pOwner->m_pOwner != NULL) res = m_pOwner->m_pOwner->down_box();

		return res;
		}


FXMenuItem *
FXMenu::findItemByPath(const SWString &path) const
		{
		FXMenuItem	*pFoundItem = NULL;
		SWString	itemname, subpath;
		int			p;

		if ((p = path.first('/')) > 0)
			{
			itemname = path.left(p);
			subpath = path.mid(p + 1);
			}
		else
			{
			itemname = path;
			}

		for (int i = 0; i < (int)m_itemlist.size(); i++)
			{
			FXMenuItem	*pItem = m_itemlist.get(i);

			if (pItem->text() == itemname)
				{
				if (subpath.isEmpty())
					{
					pFoundItem = pItem;
					}
				else
					{
					if (pItem->m_pSubMenu != NULL)
						{
						pFoundItem = pItem->m_pSubMenu->findItemByPath(subpath);
						}
					}
				break;
				}
			}

		return pFoundItem;
		}



FXMenuItem *
FXMenu::findItemByCallback(Fl_Callback *cb) const
		{
		FXMenuItem	*pFoundItem = NULL, *pItem = NULL;

		for (int i = 0; pFoundItem == NULL&&i < (int)m_itemlist.size(); i++)
			{
			FXMenuItem	*pItem = m_itemlist.get(i);

			if (pItem->callback() == cb)
				{
				pFoundItem = pItem;
				}
			else if (pItem->m_pSubMenu != NULL)
				{
				pFoundItem = pItem->m_pSubMenu->findItemByCallback(cb);
				}
			}

		return pFoundItem;
		}


FXMenuItem *
FXMenu::findItemByShortcut(sw_uint32_t id) const
		{
		FXMenuItem	*pFoundItem = NULL, *pItem = NULL;

		for (int i = 0; pFoundItem == NULL&&i < (int)m_itemlist.size(); i++)
			{
			FXMenuItem	*pItem = m_itemlist.get(i);

			if (pItem->shortcut() == id)
				{
				pFoundItem = pItem;
				}
			else if (pItem->m_pSubMenu != NULL)
				{
				pFoundItem = pItem->m_pSubMenu->findItemByShortcut(id);
				}
			}

		return pFoundItem;
		}


FXMenuItem *
FXMenu::findItemByID(sw_uint32_t id) const
		{
		FXMenuItem	*pFoundItem = NULL, *pItem = NULL;

		for (int i = 0; pFoundItem == NULL&&i < (int)m_itemlist.size(); i++)
			{
			FXMenuItem	*pItem = m_itemlist.get(i);

			if (pItem->id() == id)
				{
				pFoundItem = pItem;
				}
			else if (pItem->m_pSubMenu != NULL)
				{
				pFoundItem = pItem->m_pSubMenu->findItemByID(id);
				}
			}

		return pFoundItem;
		}
		
		
////////////////////////////////////////////////////////////////
// FXMenuItem::popup(...)

// Because Fl::grab() is done, all events go to one of the menu windows.
// But the handle method needs to look at all of them to find out
// what item the user is pointing at.  And it needs a whole lot
// of other state variables to determine what is going on with
// the currently displayed menus.
// So the main loop (handlemenu()) puts all the state in a structure
// and puts a pointer to it in a static location, so the handle()
// on menus can refer to it and alter it.  The handle() method
// changes variables in this state to indicate what item is
// picked, but does not actually alter the display, instead the
// main loop does that.  This is because the X mapping and unmapping
// of windows is slow, and we don't want to fall behind the events.


class InternalFXMenuTitle : public FXMenuWindow
		{
public:
		InternalFXMenuTitle(int X, int Y, int W, int H, FXMenuItem *);

protected:
		virtual void draw();

protected:
		FXMenuItem	*m_pItem;
		};


// each vertical menu has one of these:
class InternalFXMenuWindow : public FXMenuWindow
		{
public:

#if defined (__APPLE__) || defined (USE_X11)
		int early_hide_handle(int);
#endif

		InternalFXMenuWindow(FXMenu *m, int X, int Y, int W, int H, FXMenuItem *picked, FXMenuItem *title, int menubar = 0, int menubar_title = 0, int right_edge = 0);
		~InternalFXMenuWindow();

		void	set_selected(int);
		int		find_selected(int mx, int my);
		int		titlex(int);
		void	autoscroll(int);
		void	position(int x, int y);
		int		is_inside(int x, int y);
		int				handle(int);

protected:
		virtual void	draw();

		void			drawentry(FXMenuItem *, int i, int erase);
		void			measureItems();

public:
		bool				m_menuBar;
		int					m_itemheight;	// zero == menubar
		int					numitems;
		int					selected;
		int					drawn_selected;	// last redraw has this selected
		int					shortcutWidth;
		FXMenu				*menu;
		InternalFXMenuTitle	*title;
		FXMenu				*button;
		};

// values for InternalFXMenuState.state:
#define INITIAL_STATE 0	// no mouse up or down since popup() called
#define PUSH_STATE 1	// mouse has been pushed on a normal item
#define DONE_STATE 2	// exit the popup, the current item was picked
#define MENU_PUSH_STATE 3 // mouse has been pushed on a menu title

class InternalFXMenuState
		{
public:
		FXMenuItem				*current_item; // what mouse is pointing at
		int						menu_number; // which menu it is in
		int						item_number; // which item in that menu, -1 if none
		InternalFXMenuWindow	*p[20]; // pointers to menus
		int						nummenus;
		int						menubar; // if true p[0] is a menubar
		int						state;
		InternalFXMenuWindow	*fakemenu; // kludge for buttons in menubar

public:
		int		is_inside(int mx, int my);
		};

static InternalFXMenuState	*pMenuState = 0;

// return 1 if the coordinates are inside any of the InternalFXMenuWindows
int
InternalFXMenuState::is_inside(int mx, int my)
		{
		int i;

		for (i = nummenus - 1; i >= 0; i--)
			{
			if (p[i]->is_inside(mx, my))
				return 1;
			}

		return 0;
		}

static void
setitem(FXMenuItem *i, int m, int n)
		{
		pMenuState->current_item = i;
		pMenuState->menu_number = m;
		pMenuState->item_number = n;
		}

static void
setitem(int m, int n)
		{
		InternalFXMenuState &pp = *pMenuState;

		pp.current_item = (n >= 0) ? pp.p[m]->menu->getItemAt(n) : NULL;
		pp.menu_number = m;
		pp.item_number = n;
		}

/*
static int
forward(int menu)
		{ // go to next item in menu menu if possible
		InternalFXMenuState &pp = *pMenuState;

		// FXMenuBarButton can generate menu=-1. This line fixes it and selectes the first item.
		if (menu == -1)
			menu = 0;

		InternalFXMenuWindow &m = *(pp.p[menu]);
		int item = (menu == pp.menu_number) ? pp.item_number : m.selected;
		while (++item < m.numitems)
			{
			FXMenuItem* m1 = m.menu->next(item);
			if (m1->activevisible()) { setitem(m1, menu, item); return 1; }
			}
		return 0;
		}

static int
backward(int menu)
		{ // previous item in menu menu if possible
		InternalFXMenuState	&pp = *pMenuState;
		InternalFXMenuWindow	&m = *(pp.p[menu]);
		int			item = (menu == pp.menu_number) ? pp.item_number : m.selected;

		if (item < 0) item = m.numitems;
		while (--item >= 0)
			{
			FXMenuItem* m1 = m.menu->next(item);
			if (m1->activevisible()) { setitem(m1, menu, item); return 1; }
			}

		return 0;
		}
*/



InternalFXMenuTitle::InternalFXMenuTitle(int X, int Y, int W, int H, FXMenuItem *L) :
	FXMenuWindow(X, Y, W, H, 0)
		{
		end();
		set_modal();
		clear_border();
		set_menu_window();
		m_pItem = L;
		if (L->textColor()->to_fl_color() || Fl::scheme() || L->labeltype() > FL_NO_LABEL) clear_overlay();
		}


void
InternalFXMenuTitle::draw()
		{
		m_pItem->draw(0, 0, w(), h(), m_pItem->owner()/* , 2 */);
		}


#define LEADING	4

InternalFXMenuWindow::InternalFXMenuWindow(FXMenu *m, int X, int Y, int Wp, int Hp,
			   FXMenuItem* picked, FXMenuItem* t,
			   int menubar, int menubar_title, int right_edge) :
	FXMenuWindow(X, Y, Wp, Hp, 0),
	m_menuBar(menubar != 0),
	m_itemheight(0)
		{
		button = NULL;

		int scr_x, scr_y, scr_w, scr_h;
		int tx = X, ty = Y;

		Fl::screen_work_area(scr_x, scr_y, scr_w, scr_h);
		if (!right_edge || right_edge > scr_x + scr_w) right_edge = scr_x + scr_w;

		end();
		set_modal();
		clear_border();
		set_menu_window();
		menu = m;

		drawn_selected = -1;
		if (button)
			{
			box(button->box());
			if (box() == FL_NO_BOX || box() == FL_FLAT_BOX) box(FL_UP_BOX);
			}
		else
			{
			box(FL_UP_BOX);
			}
		color((button && !Fl::scheme()) ? button->backgroundColor()->to_fl_color() : FL_GRAY);
		selected = -1;
		numitems = (m != NULL)?(int)m->size():0;

		if (m_menuBar)
			{
			title = 0;
			return;
			}

		int hotKeysw = 0;
		int hotModsw = 0;
		FXSize	szTitle;

		int Wtitle = 0;
		int Htitle = 0;
		if (t)
			{
			szTitle = t->measure(button);
			szTitle.cx += 12;
			}

		int W = 0;
		if (m != NULL)
			{
			for (int i=0;i<numitems;i++)
				{
				FXMenuItem	*pItem=m->getItemAt(i);

				if (pItem == picked) selected = i;

				FXSize		szItem=pItem->measure(m);

				if (szItem.cy + LEADING > m_itemheight) m_itemheight = szItem.cy + LEADING;
				
				if (pItem->hasSubMenu()) szItem.cx += FL_NORMAL_SIZE;
				if (szItem.cx > W) W = szItem.cx;
				// calculate the maximum width of all shortcuts
				if (pItem->shortcut())
					{
					// s is a pointer to the UTF-8 string for the entire shortcut
					// k points only to the key part (minus the modifier keys)
					const char *k, *s = fl_shortcut_label(pItem->shortcut(), &k);
					if (fl_utf_nb_char((const unsigned char*)k, (int)strlen(k)) <= 4)
						{
						// a regular shortcut has a right-justified modifier followed by a left-justified key
						szItem.cx = int(fl_width(s, (int)(k - s)));
						if (szItem.cx > hotModsw) hotModsw = szItem.cx;
						szItem.cx = int(fl_width(k)) + 4;
						if (szItem.cx > hotKeysw) hotKeysw = szItem.cx;
						}
					else
						{
						// a shortcut with a long modifier is right-justified to the menu
						szItem.cx = int(fl_width(s)) + 4;
						if (szItem.cx > (hotModsw + hotKeysw))
							{
							hotModsw = szItem.cx - hotKeysw;
							}
						}
					}
				
				if (m->textColor()->to_fl_color() || Fl::scheme() || pItem->labeltype() > FL_NO_LABEL) clear_overlay();
				}
			}
		shortcutWidth = hotKeysw;
		if (selected >= 0 && !Wp) X -= W / 2;
		int BW = Fl::box_dx(box());
		W += hotKeysw + hotModsw + 2 * BW + 7;
		if (Wp > W) W = Wp;
		if (Wtitle > W) W = Wtitle;

		if (X < scr_x) X = scr_x;
		// this change improves popup submenu positioning at right screen edge, 
		// but it makes right_edge argument useless
		//if (X > scr_x+scr_w-W) X = right_edge-W;
		if (X > scr_x + scr_w - W) X = scr_x + scr_w - W;
		x(X); w(W);
		h((numitems ? m_itemheight*numitems - LEADING : 0) + 2 * BW + 3);
		if (selected >= 0)
			{
			Y = Y + (Hp - m_itemheight) / 2 - selected*m_itemheight - BW;
			}
		else
			{
			Y = Y + Hp;
			// if the menu hits the bottom of the screen, we try to draw
			// it above the menubar instead. We will not adjust any menu
			// that has a selected item.
			if (Y + h() > scr_y + scr_h && Y - h() >= scr_y)
				{
				if (Hp > 1)
					{
					// if we know the height of the Fl_Menu_, use it
					Y = Y - Hp - h();
					}
				else if (t)
					{
					// assume that the menubar item height relates to the first
					// menuitem as well
					Y = Y - m_itemheight - h() - Fl::box_dh(box());
					}
				else
					{
					// draw the menu to the right
					Y = Y - h() + m_itemheight + Fl::box_dy(box());
					}
				}
			}
		if (m)
			{
			y(Y);
			}
		else
			{
			y(Y - 2);
			w(1);
			h(1);
			}

		if (t)
			{
			/*
			if (menubar_title)
				{
				int dy = Fl::box_dy(button->box()) + 1;
				int ht = button->h() - dy * 2;
				title = new InternalFXMenuTitle(tx, ty - ht - dy, Wtitle, ht, t);
				}
			else
			*/
				{
				int dy = 2;
				int ht = Htitle + 2 * BW + 3;
				title = new InternalFXMenuTitle(X, Y - ht - dy, Wtitle, ht, t);
				}
			}
		else
			{
			title = 0;
			}
		}

InternalFXMenuWindow::~InternalFXMenuWindow()
		{
		hide();
		safe_delete(title);
		}

void
InternalFXMenuWindow::position(int X, int Y)
		{
		if (title)
			{
			title->position(X, title->y() + Y - y());
			}
		
		FXMenuWindow::position(X, Y);
		// x(X); y(Y); // don't wait for response from X
		}

// scroll so item i is visible on screen
void
InternalFXMenuWindow::autoscroll(int n)
		{
		int scr_y, scr_h;
		int Y = y() + Fl::box_dx(box()) + 2 + n*m_itemheight;

		int xx, ww;
		Fl::screen_work_area(xx, scr_y, ww, scr_h);
		if (Y <= scr_y) Y = scr_y - Y + 10;
		else
			{
			Y = Y + m_itemheight - scr_h - scr_y;
			if (Y < 0) return;
			Y = -Y - 10;
			}
		FXMenuWindow::position(x(), y() + Y);
		// y(y()+Y); // don't wait for response from X
		}

////////////////////////////////////////////////////////////////

void
InternalFXMenuWindow::drawentry(FXMenuItem *m, int n, int eraseit)
		{
		if (!m) return; // this happens if -1 is selected item and redrawn

		int BW = Fl::box_dx(box());
		int xx = BW;
		int W = w();
		int ww = W - 2 * BW - 1;
		int yy = BW + 1 + n*m_itemheight;
		int hh = m_itemheight - LEADING;

		if (eraseit && n != selected)
			{
			fl_push_clip(xx + 1, yy - (LEADING - 2) / 2, ww - 2, hh + (LEADING - 2));
			draw_box(box(), 0, 0, w(), h(), button ? button->backgroundColor()->to_fl_color() : color());
			fl_pop_clip();
			}

		m->draw(xx, yy, ww, hh, button /*, n == selected */);

		// the shortcuts and arrows assume fl_color() was left set by draw():
		if (m->submenu())
			{
			int sz = (hh - 7)&-2;
			int y1 = yy + (hh - sz) / 2;
			int x1 = xx + ww - sz - 3;
			fl_polygon(x1 + 2, y1, x1 + 2, y1 + sz, x1 + sz / 2 + 2, y1 + sz / 2);
			}
		else if (m->shortcut())
			{
			Fl_Font f = m->labelsize() || m->labelfont() ? (Fl_Font)m->labelfont() :
				button ? button->textfont() : FL_HELVETICA;
			fl_font(f, m->labelsize() ? m->labelsize() :
						   button ? button->textsize() : FL_NORMAL_SIZE);
			const char *k, *s = fl_shortcut_label(m->shortcut(), &k);
			if (fl_utf_nb_char((const unsigned char*)k, (int)strlen(k)) <= 4)
				{
				// right-align the modifiers and left-align the key
				char *buf = (char*)malloc(k - s + 1);
				memcpy(buf, s, k - s); buf[k - s] = 0;
				fl_draw(buf, xx, yy, ww - shortcutWidth, hh, FL_ALIGN_RIGHT);
				fl_draw(k, xx + ww - shortcutWidth, yy, shortcutWidth, hh, FL_ALIGN_LEFT);
				free(buf);
				}
			else
				{
				// right-align to the menu
				fl_draw(s, xx, yy, ww - 4, hh, FL_ALIGN_RIGHT);
				}
			}

		if (m->isFlagSet(FXMenuItem::SeparatorAfter))
			{
			fl_color(FL_DARK3);
			fl_xyline(BW - 1, yy + hh + (LEADING - 2) / 2, W - 2 * BW + 2);
			fl_color(FL_LIGHT3);
			fl_xyline(BW - 1, yy + hh + ((LEADING - 2) / 2 + 1), W - 2 * BW + 2);
			}
		}

void InternalFXMenuWindow::draw()
		{
		if (m_itemheight == 0) measureItems();

		if (damage() != FL_DAMAGE_CHILD)
			{	// complete redraw
			fl_draw_box(box(), 0, 0, w(), h(), button ? button->color() : color());
			if (menu)
				{
				for (int i=0;i<(int)menu->size();i++)
					{
					drawentry(menu->getItemAt(i), i, 0);
					}
				}
			}
		else
			{
			if (damage() & FL_DAMAGE_CHILD && selected != drawn_selected)
				{ // change selection
				drawentry(menu->getItemAt(drawn_selected), drawn_selected, 1);
				drawentry(menu->getItemAt(selected), selected, 1);
				}
			}
		drawn_selected = selected;
		}

void
InternalFXMenuWindow::set_selected(int n)
		{
		if (n != selected)
			{
			selected = n;
			damage(FL_DAMAGE_CHILD);
			}
		}

////////////////////////////////////////////////////////////////

void
InternalFXMenuWindow::measureItems()
		{
		m_itemheight = 0;
		for (int i=0;i<menu->size();i++)
			{
			FXSize	sz;

			sz = menu->measureItemAt(i);
			if ((sz.cy + LEADING) > m_itemheight) m_itemheight = sz.cy + LEADING;
			}
		}


int
InternalFXMenuWindow::find_selected(int mx, int my)
		{
		if (!menu) return -1;
		mx -= x();
		my -= y();
		if (my < 0 || my >= h()) return -1;
		if (m_menuBar)
			{ // menubar
			int xx = 3;
			int n = 0;

			for (int i=0;i<menu->size();i++)
				{
				if (menu->getItemAt(i)->lastDrawnRect().contains(mx, my))
					{
					n = i;
					break;
					}
				}

			return n;
			}
		if (mx < Fl::box_dx(box()) || mx >= w()) return -1;
		int n = (my - Fl::box_dx(box()) - 1) / m_itemheight;
		if (n < 0 || n >= numitems) return -1;
		return n;
		}

// return horizontal position for item n in a menubar:
int
InternalFXMenuWindow::titlex(int n)
		{
		int		xx = 3;
		
		for (int i=0;i<n&&i<menu->size();i++)
			{
			xx += menu->measureItemAt(i).cx + 16;
			}
		
		return xx;
		}


// return 1, if the given root coordinates are inside the window
int
InternalFXMenuWindow::is_inside(int mx, int my)
		{
		if (mx < x_root() || mx >= x_root() + w() ||
			 my < y_root() || my >= y_root() + h())
			{
			return 0;
			}
		
		if (m_menuBar && find_selected(mx, my) == -1)
			{
			// in the menubar but out from any menu header
			return 0;
			}
		
		return 1;
		}


int
InternalFXMenuWindow::handle(int e)
		{
#if defined (__APPLE__) || defined (USE_X11)
		// This off-route takes care of the "detached menu" bug on OS X.
		// Apple event handler requires that we hide all menu windows right
		// now, so that Carbon can continue undisturbed with handling window
		// manager events, like dragging the application window.
		int ret = early_hide_handle(e);
		InternalFXMenuState &pp = *pMenuState;
		if (pp.state == DONE_STATE)
			{
			hide();
			if (pp.fakemenu)
				{
				pp.fakemenu->hide();
				if (pp.fakemenu->title)
					pp.fakemenu->title->hide();
				}
			int i = pp.nummenus;
			while (i > 0)
				{
				InternalFXMenuWindow *mw = pp.p[--i];
				if (mw)
					{
					mw->hide();
					if (mw->title)
						mw->title->hide();
					}
				}
			}
		return ret;
		}


int
InternalFXMenuWindow::early_hide_handle(int e)
		{
	#endif
		InternalFXMenuState &pp = *pMenuState;

		switch (e)
			{
			case FL_KEYBOARD:
#ifdef NOT_YET
				switch (Fl::event_key())
					{
					case FL_BackSpace:
						BACKTAB:
							if (!backward(pp.menu_number)) { pp.item_number = -1; backward(pp.menu_number); }
							return 1;
						case FL_Up:
							if (pp.menubar && pp.menu_number == 0)
								{
								// Do nothing...
								}
							else if (backward(pp.menu_number))
								{
								// Do nothing...
								}
							else if (pp.menubar && pp.menu_number == 1)
								{
								setitem(0, pp.p[0]->selected);
								}
							return 1;
						case FL_Tab:
							if (Fl::event_shift()) goto BACKTAB;
						case FL_Down:
							if (pp.menu_number || !pp.menubar)
								{
								if (!forward(pp.menu_number) && Fl::event_key() == FL_Tab)
									{
									pp.item_number = -1;
									forward(pp.menu_number);
									}
								}
							else if (pp.menu_number < pp.nummenus - 1)
								{
								forward(pp.menu_number + 1);
								}
							return 1;
						case FL_Right:
							if (pp.menubar && (pp.menu_number <= 0 || (pp.menu_number == 1 && pp.nummenus == 2)))
								forward(0);
							else if (pp.menu_number < pp.nummenus - 1) forward(pp.menu_number + 1);
							return 1;
						case FL_Left:
							if (pp.menubar && pp.menu_number <= 1) backward(0);
							else if (pp.menu_number > 0)
								setitem(pp.menu_number - 1, pp.p[pp.menu_number - 1]->selected);
							return 1;
						case FL_Enter:
						case FL_KP_Enter:
						case ' ':
							pp.state = DONE_STATE;
							return 1;
						case FL_Escape:
							setitem(0, -1, 0);
							pp.state = DONE_STATE;
							return 1;
					}
				break;

			case FL_SHORTCUT:
				{
				for (int mymenu = pp.nummenus; mymenu--;)
					{
					menuwindow &mw = *(pp.p[mymenu]);
					int item; FXMenuBarItem* m = mw.menu->find_shortcut(&item);
					if (m)
						{
						setitem(m, mymenu, item);
						if (!m->submenu()) pp.state = DONE_STATE;
						return 1;
						}
					}
				}
#endif // NOT_YET
				break;
				
			case FL_MOVE:
#if ! (defined(WIN32) || defined(__APPLE__))
				if (pp.state == DONE_STATE)
					{
					return 1; // Fix for STR #2619
					}
				/* FALLTHROUGH */
#endif
			case FL_ENTER:
			case FL_PUSH:
			case FL_DRAG:
				{
				int mx = Fl::event_x_root();
				int my = Fl::event_y_root();
				int item = 0; int mymenu = pp.nummenus - 1;
				// Clicking or dragging outside menu cancels it...
				if ((!pp.menubar || mymenu) && !pp.is_inside(mx, my))
					{
					setitem(0, -1, 0);
					if (e == FL_PUSH)
						pp.state = DONE_STATE;
					return 1;
					}
				for (mymenu = pp.nummenus - 1; ; mymenu--)
					{
					item = pp.p[mymenu]->find_selected(mx, my);
					if (item >= 0)
						break;
					if (mymenu <= 0)
						{
						// buttons in menubars must be deselected if we move outside of them!
						if (pp.menu_number == -1 && e == FL_PUSH)
							{
							pp.state = DONE_STATE;
							return 1;
							}
						if (pp.current_item && pp.menu_number == 0 && !pp.current_item->submenu())
							{
							if (e == FL_PUSH)
								pp.state = DONE_STATE;
							setitem(0, -1, 0);
							return 1;
							}
						// all others can stay selected
						return 0;
						}
					}
				if (my == 0 && item > 0) setitem(mymenu, item - 1);
				else setitem(mymenu, item);
				if (e == FL_PUSH)
					{
					if (pp.current_item && pp.current_item->submenu() // this is a menu title
						&& item != pp.p[mymenu]->selected // and it is not already on
						&& !pp.current_item->callback()) // and it does not have a callback
						pp.state = MENU_PUSH_STATE;
					else
						pp.state = PUSH_STATE;
					}
				}
				return 1;
			case FL_RELEASE:
				// Mouse must either be held down/dragged some, or this must be
				// the second click (not the one that popped up the menu):
				if (!Fl::event_is_click()
					|| pp.state == PUSH_STATE
					|| (pp.menubar && pp.current_item && !pp.current_item->submenu()) // button
				)
					{
#if 0 // makes the check/radio items leave the menu up
					FXMenuBarItem* m = pp.current_item;
					if (m && button && (m->flags & (FL_MENU_TOGGLE | FL_MENU_RADIO)))
						{
						((FXMenuBar*)button)->picked(m);
						pp.p[pp.menu_number]->redraw();
						}
					else
#endif
						// do nothing if they try to pick inactive items
						if (!pp.current_item || pp.current_item->activevisible())
							pp.state = DONE_STATE;
					}
				return 1;
			}


		return Fl_Window::handle(e);
		}


/**
  Pulldown() is similar to popup(), but a rectangle is
  provided to position the menu.  The menu is made at least W
  wide, and the picked item is centered over the rectangle
  (like Fl_Choice uses).  If picked is zero or not
  found, the menu is aligned just below the rectangle (like a pulldown
  menu).
  <P>The title and menubar arguments are used
  internally by the FXMenuBarBar widget.
*/
FXMenuItem *
FXMenu::pulldown(int X, int Y, int W, int H, FXMenuItem *initial_item, Fl_Widget *pbutton, FXMenuItem* t, int menubar)
		{
		Fl_Group::current(0); // fix possible user error...

		// button = pbutton;
		if (pbutton)
			{
			if (pbutton->window())
				{
				for (Fl_Window* w = pbutton->window(); w; w = w->window())
					{
					X += w->x();
					Y += w->y();
					}
				}
			else if (pbutton->type() >= FL_WINDOW)
				{
				X += pbutton->x();
				Y += pbutton->y();
				}
			}
		else
			{
			X += Fl::event_x_root() - Fl::event_x();
			Y += Fl::event_y_root() - Fl::event_y();
			}

		InternalFXMenuWindow mw(this, X, Y, W, H, initial_item, t, menubar);

		Fl::grab(mw);
		InternalFXMenuState pp;
	
		pMenuState = &pp;
		pp.p[0] = &mw;
		pp.nummenus = 1;
		pp.menubar = menubar;
		pp.state = INITIAL_STATE;
		pp.fakemenu = 0; // kludge for buttons in menubar

		// preselected item, pop up submenus if necessary:
		if (initial_item && mw.selected >= 0)
			{
			setitem(0, mw.selected);
			goto STARTUP;
			}

		pp.current_item = 0; pp.menu_number = 0; pp.item_number = -1;
		if (menubar)
			{
			// find the initial menu
			if (!mw.handle(FL_DRAG))
				{
				Fl::grab(0);
				return 0;
				}
			}
		initial_item = pp.current_item;
		if (initial_item) goto STARTUP;

		// the main loop, runs until p.state goes to DONE_STATE:
		for (;;)
			{

			// make sure all the menus are shown:
					{
					for (int k = 1; k < pp.nummenus; k++)
						{
						if (!pp.p[k]->shown())
							{
							if (pp.p[k]->title) pp.p[k]->title->show();
							pp.p[k]->show();
							}
						}
					}

					// get events:
					{
					const FXMenuItem* oldi = pp.current_item;
					Fl::wait();
					if (pp.state == DONE_STATE) break; // done.
					if (pp.current_item == oldi) continue;
					}

					// only do rest if item changes:
					if (pp.fakemenu) { delete pp.fakemenu; pp.fakemenu = 0; } // turn off "menubar button"

					if (!pp.current_item)
						{ // pointing at nothing
	// turn off selection in deepest menu, but don't erase other menus:
						pp.p[pp.nummenus - 1]->set_selected(-1);
						continue;
						}

					if (pp.fakemenu) { delete pp.fakemenu; pp.fakemenu = 0; }
					initial_item = 0; // stop the startup code
					pp.p[pp.menu_number]->autoscroll(pp.item_number);

STARTUP:
					InternalFXMenuWindow& cw = *pp.p[pp.menu_number];
					FXMenuItem *m = pp.current_item;

					if (!m->activevisible())
						{ // pointing at inactive item
						cw.set_selected(-1);
						initial_item = 0; // turn off startup code
						continue;
						}
					cw.set_selected(pp.item_number);

					if (m == initial_item) initial_item = 0; // stop the startup code if item found
					if (m->submenu())
						{
						FXMenuItem	*title=m;
						FXMenu		*menutable=m->m_pSubMenu;

						// figure out where new menu goes:
						int nX, nY;
						if (!pp.menu_number && pp.menubar)
							{	// menu off a menubar:
							nX = cw.x() + cw.titlex(pp.item_number);
							nY = cw.y() + cw.h();
							initial_item = 0;
							}
						else
							{
							nX = cw.x() + cw.w();
							nY = cw.y() + pp.item_number * cw.m_itemheight;
							title = 0;
							}
						if (initial_item)
							{ // bring up submenu containing initial item:
							InternalFXMenuWindow* n = new InternalFXMenuWindow(menutable, X, Y, W, H, initial_item, title, 0, 0, cw.x());
							pp.p[pp.nummenus++] = n;
							// move all earlier menus to line up with this new one:
							if (n->selected >= 0)
								{
								int dy = n->y() - nY;
								int dx = n->x() - nX;
								int waX, waY, waW, waH;
								Fl::screen_work_area(waX, waY, waW, waH, X, Y);
								for (int menu = 0; menu <= pp.menu_number; menu++)
									{
									InternalFXMenuWindow* tt = pp.p[menu];
									int nx = tt->x() + dx; if (nx < waX) { nx = waX; dx = -tt->x() + waX; }
									int ny = tt->y() + dy; if (ny < waY) { ny = waY; dy = -tt->y() + waY; }
									tt->position(nx, ny);
									}
								setitem(pp.nummenus - 1, n->selected);
								goto STARTUP;
								}
							}
						else if (pp.nummenus > pp.menu_number + 1 &&
						pp.p[pp.menu_number + 1]->menu == menutable)
							{
							// the menu is already up:
							while (pp.nummenus > pp.menu_number + 2) delete pp.p[--pp.nummenus];
							pp.p[pp.nummenus - 1]->set_selected(-1);
							}
						else
							{
							// delete all the old menus and create new one:
							while (pp.nummenus > pp.menu_number + 1) delete pp.p[--pp.nummenus];
							pp.p[pp.nummenus++] = new InternalFXMenuWindow(menutable, nX, nY,
											  title ? 1 : 0, 0, 0, title, 0, menubar,
										  (title ? 0 : cw.x()));
							}
						}
					else
						{ // !m->submenu():
						while (pp.nummenus > pp.menu_number + 1) delete pp.p[--pp.nummenus];
						if (!pp.menu_number && pp.menubar)
							{
							// kludge so "menubar buttons" turn "on" by using menu title:
							pp.fakemenu = new InternalFXMenuWindow(0,
										  cw.x() + cw.titlex(pp.item_number),
										  cw.y() + cw.h(), 0, 0,
										  0, m, 0, 1);
							pp.fakemenu->title->show();
							}
						}
			}

		FXMenuItem	*m = pp.current_item;
		delete pp.fakemenu;
		while (pp.nummenus > 1) delete pp.p[--pp.nummenus];
		mw.hide();
		Fl::grab(0);

		return m;
		}
