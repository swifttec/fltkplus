#include "FXImageButton.h"
#include "FXDialog.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXImageButton::FXImageButton(const SWString &filename, int W, int H) :
	FXButton("", filename, 0, 0, W, H)
		{
		m_hasText = false;

		FXButtonStyle	bs;

		bs.bdColor = FX_COLOR_NONE;
		bs.fgColor = FX_COLOR_NONE;		// Foreground / text color
		bs.bgColor = FX_COLOR_NONE;		// Background color
		bs.bdColor = FX_COLOR_NONE;		// Border color
		bs.bdStyle = 0;
		bs.bdWidth = 0;
		bs.bdCornerSize = 0;

		for (int i=0;i<MaxStyle;i++) setButtonStyle(i, bs);

		m_padding = FXPadding(1, 1, 1, 1);
		}


FXImageButton::FXImageButton(const SWString &filename, int X, int Y, int W, int H, bool stdbutton) :
	FXButton("", filename, X, Y, W, H)
		{
		m_hasText = false;

		if (!stdbutton)
			{
			FXButtonStyle	bs;

			bs.bdColor = FX_COLOR_NONE;
			bs.fgColor = FX_COLOR_NONE;		// Foreground / text color
			bs.bgColor = FX_COLOR_NONE;		// Background color
			bs.bdColor = FX_COLOR_NONE;		// Border color
			bs.bdStyle = 0;
			bs.bdWidth = 0;
			bs.bdCornerSize = 0;

			for (int i=0;i<MaxStyle;i++) setButtonStyle(i, bs);
			}

		m_padding = FXPadding(1, 1, 1, 1);
		}


FXImageButton::FXImageButton(sw_uint32_t id, const SWString &filename, int W, int H) :
	FXButton(id, "", filename, 0, 0, W, H)
		{
		m_hasText = false;

		FXButtonStyle	bs;

		bs.bdColor = FX_COLOR_NONE;
		bs.fgColor = FX_COLOR_NONE;		// Foreground / text color
		bs.bgColor = FX_COLOR_NONE;		// Background color
		bs.bdColor = FX_COLOR_NONE;		// Border color
		bs.bdStyle = 0;
		bs.bdWidth = 0;
		bs.bdCornerSize = 0;

		for (int i=0;i<MaxStyle;i++) setButtonStyle(i, bs);

		m_padding = FXPadding(1, 1, 1, 1);
		}


FXImageButton::FXImageButton(sw_uint32_t id, const SWString &filename, int X, int Y, int W, int H, bool stdbutton) :
	FXButton(id, "", filename, X, Y, W, H)
		{
		m_hasText = false;

		if (!stdbutton)
			{
			FXButtonStyle	bs;

			bs.bdColor = FX_COLOR_NONE;
			bs.fgColor = FX_COLOR_NONE;		// Foreground / text color
			bs.bgColor = FX_COLOR_NONE;		// Background color
			bs.bdColor = FX_COLOR_NONE;		// Border color
			bs.bdStyle = 0;
			bs.bdWidth = 0;
			bs.bdCornerSize = 0;

			for (int i=0;i<MaxStyle;i++) setButtonStyle(i, bs);
			}

		m_padding = FXPadding(1, 1, 1, 1);
		}


FXImageButton::FXImageButton(const SWString &imagefolder, const SWString &imagebase, int W, int H) :
	FXButton("", "", 0, 0, W, H)
		{
		m_hasText = false;

		FXButtonStyle	bs;

		bs.bdColor = FX_COLOR_NONE;
		bs.fgColor = FX_COLOR_NONE;		// Foreground / text color
		bs.bgColor = FX_COLOR_NONE;		// Background color
		bs.bdColor = FX_COLOR_NONE;		// Border color
		bs.bdStyle = 0;
		bs.bdWidth = 0;
		bs.bdCornerSize = 0;

		for (int i=0;i<MaxStyle;i++) setButtonStyle(i, bs);
		setButtonImages(imagefolder, imagebase, false);

		m_padding = FXPadding(1, 1, 1, 1);
		}


/*
void
FXImageButton::onDraw()
		{
		int				X=x(), Y=y(), W=w(), H=h();
		int				ds=state();
		FXButtonStyle	&style=m_style[ds];

		X += m_padding.left;
		Y += m_padding.top;
		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;

		int		ypos=0;

		if (m_pSharedImage != NULL)
			{
			int		ix, iy, iw, ih;

			iw = m_pSharedImage->w();
			ih = m_pSharedImage->h();

			// Adjust image height to match the button
			ih = H;
			iw = m_pSharedImage->w() * ih / m_pSharedImage->h();
			ix = X + ((W-iw) / 2);
			iy = Y + 1;

			if (style.pImage != NULL)
				{
				if (style.pImage->w() != iw || style.pImage->h() != ih)
					{
					delete style.pImage;
					style.pImage = NULL;
					}
				}

			if (style.pImage == NULL)
				{
				style.pImage = m_pSharedImage->copy(iw, ih);
				if (ds == Disabled)
					{
					style.pImage->desaturate();
					}
				}

			style.pImage->draw(ix, iy, iw, ih, 0, 0);
			}
		}
*/