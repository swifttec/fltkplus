#include "FXStaticNamedValue.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

#include <FltkExt/FXPackedLayout.h>

FXStaticNamedValue::FXStaticNamedValue(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXGroup(X, Y, nameW + valueW+8, H),
	m_pName(NULL),
	m_pValue(NULL)
		{
		init(name, nameW, value, valueW, H);
		}


FXStaticNamedValue::FXStaticNamedValue(const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXGroup(0, 0, nameW + valueW+8, H),
	m_pName(NULL),
	m_pValue(NULL)
		{
		init(name, nameW, value, valueW, H);
		}

void
FXStaticNamedValue::init(const SWString &name, int nameW, const SWString &value, int valueW, int H)
		{
		set_output();

		setForegroundColor("#000");
		setBackgroundColor(FX_COLOR_NONE);
		setBorderColor(FX_COLOR_NONE);

		FXPackedLayout	*pLayout;

		setLayout(pLayout=new FXPackedLayout(FXPackedLayout::ByColumns));
		pLayout->gapBetweenChildren(FXSize(2, 2));
		pLayout->padding(FXPadding(0, 0, 0, 0));

		add(m_pName = new FXStaticText(nameW, h()-2, name, FXFont("Arial", 11, FXFont::Bold), FX_VALIGN_MIDDLE|FX_HALIGN_RIGHT, "#000"));
		m_pName->padding(FXPadding(0, 0, 4, 4));
		add(m_pValue = new FXStaticText(valueW, h()-2, value, FXFont("Arial", 11, FXFont::Normal), FX_VALIGN_MIDDLE|FX_HALIGN_LEFT, "#000", "#eee"));
		m_pValue->setBorder(0, "#000", 1, 0);
		m_pValue->padding(FXPadding(0, 0, 4, 4));
		}
