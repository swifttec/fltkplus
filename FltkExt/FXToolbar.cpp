#include "FXToolbar.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <FltkExt/FXSeparator.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXWnd.h>


void
FXToolbar::genericCallback(Fl_Widget *pWidget, void *pUserData)
		{
		FXToolbar	*pSheet=(FXToolbar *)pUserData;

		pSheet->onCallback(pWidget);
		}


void
FXToolbar::itemCallback(Fl_Widget *pWidget)
		{
		FXToolbar	*pSheet=(FXToolbar *)pWidget->parent();

		pSheet->onCallback(pWidget);
		}


FXToolbar::FXToolbar(int H) :
	FXGroup(0, 0, H, H),
	m_gap(5),
	m_pos(0)
		{
		setMaxSize(0, H);
		setMinSize(0, H);
		setBackgroundColor("#ccc");
		setBorder(FX_BORDER_NONE, FX_COLOR_NONE, 0, 0);

		// Set up a generic callback
		callback(genericCallback, this);
		}


FXToolbar::~FXToolbar()
		{
		}


void
FXToolbar::addLabel(const SWString &text, const FXFont &font, const FXColor &textcolor, int flags)
		{
		FXSize			sz=fx_getTextExtent(text, font);
		FXStaticText	*pItem=new FXStaticText(sz.cx, sz.cy, text, font, flags, textcolor);

		fx_group_addItem(this, 0, pItem);

		int	ypos=((h()-pItem->h())/2);

		pItem->setBackgroundColor(getBackgroundColor());
		pItem->resize(m_pos, 1, pItem->w(), h()-2);
		m_pos += pItem->w() + m_gap;
		}


void
FXToolbar::addSeparator()
		{
		FXSeparator	*pItem=new FXSeparator();

		fx_group_addItem(this, 0, pItem);

		int	ypos=((h()-pItem->h())/2);

		pItem->setBackgroundColor(getBackgroundColor());
		pItem->resize(m_pos, 0, 1, h());
		m_pos += pItem->w() + m_gap;
		}


void
FXToolbar::addItem(int itemid, Fl_Widget *pItem)
		{
		fx_group_addItem(this, itemid, pItem);
		pItem->callback(itemCallback);

		int	ypos=((h()-pItem->h())/2);

		pItem->resize(m_pos, ypos, pItem->w(), pItem->h());
		m_pos += pItem->w() + m_gap;
		}


void
FXToolbar::selectItem(int itemid, bool v, bool exclusive)
		{
		if (exclusive)
			{
			int		nwidgets=children();
			Fl_Widget *const*a=array();

			for (int i=0;i<nwidgets;i++)
				{
				Fl_Widget	*pWidget=*a++;

				FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

				if (pButton != NULL)
					{
					pButton->select((int)pButton->id()==itemid?v:false);
					}
				}
			}
		else
			{
			FXButton	*pButton=dynamic_cast<FXButton *>(getItem(itemid));

			if (pButton != NULL)
				{
				pButton->select(v);
				}
			}
		}


void
FXToolbar::deselectAllItems()
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;
			FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

			if (pButton != NULL)
				{
				pButton->select(false);
				}
			}
		}

void
FXToolbar::enableItem(int itemid, bool v)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}

void
FXToolbar::enableAllItems(bool v)
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;

			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}


void
FXToolbar::onCallback(Fl_Widget *pWidget)
		{
		Fl_Widget	*pParent=parent();
		bool		handled=false;

		if (!handled)
			{
			FXWnd	*pWnd=dynamic_cast<FXWnd *>(pParent);

			if (pWnd != NULL)
				{
				pWnd->onItemCallback(pWidget, NULL);
				handled = true;
				}
			}

		if (!handled)
			{
			FXGroup	*pGroup=dynamic_cast<FXGroup *>(pParent);

			if (pGroup != NULL)
				{
				pGroup->onItemCallback(pWidget, NULL);
				handled = true;
				}
			}
		}




void
FXToolbar::resize(int X, int Y, int W, int H)
		{
		// We are handling the resizing, so disable it whilst the underlying code works
		Fl_Widget	*tmp=resizable();

		resizable(NULL);
		Fl_Group::resize(X,Y,W,H);
		resizable(tmp);
		}
