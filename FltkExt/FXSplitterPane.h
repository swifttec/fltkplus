/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the definition of the FXSplitterPane class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXSplitterPane_h__
#define __FltkExt_FXSplitterPane_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Group.H>

class FXSplitterWnd;

class FLTKEXT_DLL_EXPORT FXSplitterPane
		{
friend class FXSplitterWnd;

public:
		FXSplitterPane(FXSplitterWnd *pOwner=NULL);
		~FXSplitterPane();
		
		void			clear();

		bool			fixed() const			{ return m_fixed; }
		void			fixed(bool v)			{ m_fixed = v; }

		bool			vertical() const		{ return m_vertical; }
		void			vertical(bool v)		{ m_vertical = v; }

		int				minSize() const;
		void			minSize(int v)			{ m_minSize = v; }

		int				maxSize() const;
		void			maxSize(int v)			{ m_maxSize = v; }
		
		int				preferredSize() const	{ return m_prefSize; }
		void			preferredSize(int v)	{ m_prefSize = v; }
		
		int				currentSize() const		{ return m_currSize; }
		void			currentSize(int v);
		
		int				offset() const			{ return m_offset; }
		void			offset(int v)			{ m_offset = v; }

		Fl_Widget *		getWidget() const		{ return m_pWidget; }
		void			setWidget(Fl_Widget *p)	{ m_pWidget = p; }

		void			closeWindow();

		FXSize			minWidgetSize() const;

protected:
		FXSplitterWnd	*m_pOwner;
		Fl_Widget		*m_pWidget;
		int				m_currSize;
		int				m_prefSize;
		int				m_minSize;
		int				m_maxSize;
		int				m_offset;
		bool			m_fixed;
		bool			m_vertical;
		};

#endif // __FltkExt_FXSplitterPane_h__
