/**
*** @file		FltkExt.h
*** @brief		General header for the FltkExt library.
*** @version	1.0
*** @author		Simon Sparkes
***
*** This file is the general header used by all of the components of the
*** FltkExt library.
**/

#ifndef __FltkExt_h__
#define __FltkExt_h__

#include <FltkExt/fx_types.h>



#if defined(SW_PLATFORM_WINDOWS)

	// Determine the import/export type for a DLL build
	#if defined(FLTKEXT_BUILD_DLL)
		#define FLTKEXT_DLL_EXPORT __declspec(dllexport)
	#else
		#define FLTKEXT_DLL_EXPORT __declspec(dllimport)
	#endif // defined(UTIL_BUILD_DLL)

#else // defined(SW_PLATFORM_WINDOWS)

	// On non-windows platforms we just define this to be nothing
	#define FLTKEXT_DLL_EXPORT

#endif // defined(SW_PLATFORM_WINDOWS)


#define FIT_FAILED	-1
#define FIT_OK		0
#define FIT_OK_EOL	1

#define FX_OK		0

// Horizontal Alignment Bits 0-1
#define FX_HALIGN_MASK				0x00000003
#define FX_HALIGN_DEFAULT			0x00000000
#define FX_HALIGN_LEFT				0x00000001
#define FX_HALIGN_CENTER			0x00000002
#define FX_HALIGN_CENTRE			FX_HALIGN_CENTER
#define FX_HALIGN_RIGHT				0x00000003

// Vertical Alignment Bits 2-3
#define FX_VALIGN_MASK				0x0000000c
#define FX_VALIGN_DEFAULT			0x00000000
#define FX_VALIGN_TOP				0x00000004
#define FX_VALIGN_MIDDLE			0x00000008
#define FX_VALIGN_CENTRE			FX_VALIGN_MIDDLE
#define FX_VALIGN_CENTER			FX_VALIGN_MIDDLE
#define FX_VALIGN_BOTTOM			0x0000000c

// Combined alignments (common shortcuts)
#define FX_ALIGN_MIDDLE_LEFT				(FX_VALIGN_MIDDLE | FX_HALIGN_LEFT)
#define FX_ALIGN_MIDDLE_CENTRE				(FX_VALIGN_MIDDLE | FX_HALIGN_CENTER)
#define FX_ALIGN_MIDDLE_RIGHT				(FX_VALIGN_MIDDLE | FX_HALIGN_RIGHT)
#define FX_ALIGN_MASK						(FX_HALIGN_MASK | FX_VALIGN_MASK)

// Misc flags Bit 4-11
#define FX_BG_OPAQUE				0x00000010
#define FX_BG_TRANSPARENT			0x00000020
// #define FX_OUTLINE					0x00000040 - FLTK does not support outlining text at present
#define FX_SHADOW					0x00000080
#define FX_NOCLIP					0x00000100
#define FX_NOWRAP					0x00000200
#define FX_SHRINKTOFIT				0x00000400
#define FX_OPAQUE_TEXT				0x00000800

// Border flags Bits 12-15
#define FX_BORDER_NONE				0x00000000
#define FX_BORDER_LEFT				0x00001000
#define FX_BORDER_RIGHT				0x00002000
#define FX_BORDER_TOP				0x00004000
#define FX_BORDER_BOTTOM			0x00008000
#define FX_BORDER_ALL				0x0000f000
#define FX_BORDER_MASK				0x0000f000

// Image flags - bits 16-20
#define FX_IMAGE_STRETCH_TO_HEIGHT	0x00010000
#define FX_IMAGE_STRETCH_TO_WIDTH	0x00020000
#define FX_IMAGE_PROPORTIONAL		0x00040000	// If set the picture is always displayed in it's original proportions
#define FX_IMAGE_USE_ALPHA			0x00080000
#define FX_IMAGE_TILE				0x00100000

#define FX_IMAGE_STRETCH_TO_FIT		(FX_IMAGE_STRETCH_TO_WIDTH|FX_IMAGE_STRETCH_TO_HEIGHT)
#define FX_IMAGE_FIT_TO_WIDTH		(FX_IMAGE_PROPORTIONAL | FX_IMAGE_STRETCH_TO_WIDTH)
#define FX_IMAGE_FIT_TO_HEIGHT		(FX_IMAGE_PROPORTIONAL | FX_IMAGE_STRETCH_TO_HEIGHT)

// Spare - Bits 20-23

// Misc flags - bits (24-31)
#define FX_FLAG_KEEP_WITH_NEXT		0x01000000
#define FX_FLAG_SHRINK				0x02000000
#define FX_FLAG_EXPAND				0x04000000
#define FX_FLAG_KEEP_TOGETHER		0x08000000
#define FX_FLAG_SPACE_BEFORE		0x10000000
#define FX_FLAG_LAST				0x20000000
#define FX_FLAG_FIRST				0x40000000
#define FX_FLAG_CHANGED				0x80000000

// Mouse button actions
#define FX_BUTTON_DOWN			1
#define FX_BUTTON_UP			2
#define	FX_BUTTON_DOUBLE_CLICK	3

// Scroll bars
#define FX_SCROLLBAR_VERTICAL	1
#define FX_SCROLLBAR_HORIZONTAL	2

#define FX_SCROLL_NONE			0
#define FX_SCROLL_UP			1
#define FX_SCROLL_DOWN			2
#define FX_SCROLL_FIRST			3
#define FX_SCROLL_LAST			4
#define FX_SCROLL_PAGEUP		5
#define FX_SCROLL_PAGEDOWN		6
#define FX_SCROLL_TRACK			7

#define FX_ARROW_NONE			0
#define FX_ARROW_UP				1
#define FX_ARROW_DOWN			2
#define FX_ARROW_LEFT			3
#define	FX_ARROW_RIGHT			4

#define FX_LINESTYLE_SOLID		0
#define FX_LINESTYLE_DASHES		1
#define FX_LINESTYLE_DASHDOTS	2
#define FX_LINESTYLE_DOTS		3
#define FX_LINESTYLE_FINEDASHES	4

// #define FX_PEN_NULL				-1
#define FX_PEN_SOLID			FX_LINESTYLE_SOLID


#include <FltkExt/fx_generic_ids.h>

#endif // __FltkExt_h__
