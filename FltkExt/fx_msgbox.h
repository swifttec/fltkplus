/****************************************************************************
**	fx_msgbox.h	Message box functions
****************************************************************************/


#ifndef __FltkExt_fx_msgbox_h__
#define __FltkExt_fx_msgbox_h__

#include <FltkExt/FltkExt.h>

#include <swift/SWString.h>

#define FXMB_OK					0x00000000
#define FXMB_OKCANCEL			0x00000001
#define FXMB_ABORTRETRYIGNORE	0x00000002
#define FXMB_YESNOCANCEL		0x00000003
#define FXMB_YESNO				0x00000004
#define FXMB_RETRYCANCEL		0x00000005

#define FXMB_ICONHAND			0x00000010
#define FXMB_ICONQUESTION		0x00000020
#define FXMB_ICONEXCLAMATION	0x00000030
#define FXMB_ICONASTERISK		0x00000040
#define FXMB_ICONINFORMATION	0x00000050
#define FXMB_ICONWARNING		0x00000060
#define FXMB_ICONERROR			0x00000070

FLTKEXT_DLL_EXPORT int fx_msgbox_text(const SWString &text, sw_uint32_t nType=FXMB_OK, sw_uint32_t nIDHelp=0, double maxRatio=1.0);
FLTKEXT_DLL_EXPORT int fx_msgbox_text(sw_uint32_t stringId, sw_uint32_t nType=FXMB_OK, sw_uint32_t nIDHelp=0, double maxRatio=1.0);
/*
FLTKEXT_DLL_EXPORT int FXHtmlMessageBox(const SWString &text, sw_uint32_t nType=MB_OK, sw_uint32_t nIDHelp=0, double maxRatio=10.0);
FLTKEXT_DLL_EXPORT int FXHtmlMessageBox(sw_uint32_t stringId, sw_uint32_t nType=MB_OK, sw_uint32_t nIDHelp = (sw_uint32_t)-1, double maxRatio=10.0);
*/

#endif // __FltkExt_fx_msgbox_h__
