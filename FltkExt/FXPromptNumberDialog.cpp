#include "FXPromptNumberDialog.h"

#include "FXStaticText.h"

#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/fl_ask.H>

#include <swift/sw_functions.h>


FXPromptNumberDialog::FXPromptNumberDialog(const SWString &title, const SWString &prompt, int value, Fl_Widget *pParent) :
	FXDialog(title, 400, 90, pParent),
	m_value(value),
	m_hasminmax(false),
	m_minValue(0),
	m_maxValue(0)
		{
		FXSize	sz=fx_getTextSize(prompt);

		addStaticText(FX_IDSTATIC, prompt, 10, 10, sz.cx+10, sz.cy+8);
		addNumberInput(100, sz.cx+30, 10, w()-40-sz.cx, sz.cy+8);
		addButton(FX_IDOK, "OK", 1, 200, 50, 90, 30);
		addButton(FX_IDCANCEL, "Cancel", 0, 300, 50, 90, 30);

		setItemValue(100, value);
		setItemFocus(100);
		}


FXPromptNumberDialog::~FXPromptNumberDialog()
		{
		}


void
FXPromptNumberDialog::setValueRange(int minv, int maxv)
		{
		m_hasminmax = true;
		m_minValue = minv;
		m_maxValue = maxv;
		}


void
FXPromptNumberDialog::onOK()
		{
		bool	ok=true;

		getItemValue(100, m_value);

		if (m_hasminmax)
			{
			if (m_value < m_minValue || m_value > m_maxValue)
				{
				SWString	msg;

				msg.format("Please enter a value in the range %d - %d", m_minValue, m_maxValue);
				fl_message("Value out of range", msg.c_str());
				ok = false;
				}

			}
		
		if (ok) FXDialog::onOK();
		}
