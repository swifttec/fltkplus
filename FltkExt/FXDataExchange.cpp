#include "FXDataExchange.h"

#include <FL/Fl_Button.H>
#include <FL/Fl_Group.H>

FXDataExchange::FXDataExchange(Fl_Group *pGroup, int action) :
	m_pGroup(pGroup),
	m_action(action)
		{
		}


FXDataExchange::~FXDataExchange()
		{
		}


bool
FXDataExchange::value(int itemid, SWString &value, int limitsize)
		{
		bool	res=false;

		switch (m_action)
			{
			case Save:	res = fx_group_getItemValue(m_pGroup, itemid, value);				break;
			case Load:	res = fx_group_setItemValue(m_pGroup, itemid, value, limitsize);	break;
			}
		
		return res;
		}


bool
FXDataExchange::value(int itemid, int &value, int limitsize)
		{
		bool	res=false;

		switch (m_action)
			{
			case Save:	res = fx_group_getItemValue(m_pGroup, itemid, value);				break;
			case Load:	res = fx_group_setItemValue(m_pGroup, itemid, value, limitsize);	break;
			}
		
		return res;
		}


bool
FXDataExchange::value(int itemid, double &value, int limitsize)
		{
		bool	res=false;

		switch (m_action)
			{
			case Save:	res = fx_group_getItemValue(m_pGroup, itemid, value);				break;
			case Load:	res = fx_group_setItemValue(m_pGroup, itemid, value, limitsize);	break;
			}
		
		return res;
		}


bool
FXDataExchange::value(int itemid, bool &value, int limitsize)
		{
		bool	res=false;

		switch (m_action)
			{
			case Save:	res = fx_group_getItemValue(m_pGroup, itemid, value);				break;
			case Load:	res = fx_group_setItemValue(m_pGroup, itemid, value, limitsize);	break;
			}
		
		return res;
		}



bool
FXDataExchange::radioGroupValue(int itemid, int &value)
		{
		bool		res=false;
		int			nchildren=m_pGroup->children(), firstidx=-1, lastidx=-1;
		Fl_Widget	*p=NULL;

		for (int i=0;i<nchildren;i++)
			{
			p = m_pGroup->child(i);
			if (p->id() == itemid)
				{
				if (p->type() == FL_RADIO_BUTTON)
					{
					firstidx = lastidx = i;

					while (firstidx > 0)
						{
						if (m_pGroup->child(firstidx-1)->type() == FL_RADIO_BUTTON) --firstidx;
						else break;
						}
					
					while (lastidx < (nchildren-1))
						{
						if (m_pGroup->child(lastidx+1)->type() == FL_RADIO_BUTTON) lastidx++;
						else break;
						}
					}
				break;
				}
			}

		if (firstidx >= 0)
			{
			switch (m_action)
				{
				case Save:
					{
					value = 0;
					for (int i=firstidx;i<=lastidx;i++)
						{
						Fl_Button	*pb=(Fl_Button *)(m_pGroup->child(i));

						if (pb->value())
							{
							value = i - firstidx;
							break;
							}
						}
					}
					break;

				case Load:
					{
					if (value >= 0 && value <= (lastidx - firstidx))
						{
						int		idx=0;

						for (int i=firstidx;i<=lastidx;i++)
							{
							Fl_Button	*pb=(Fl_Button *)(m_pGroup->child(i));

							pb->value((idx == value)?1:0);
							idx++;
							}
						}
					}
					break;
				}
			}

		return res;
		}
