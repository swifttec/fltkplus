/*
**	FXPoint.h	A generic 2D point
*/

#ifndef __FltkExt_FXPoint_h__
#define __FltkExt_FXPoint_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

class FLTKEXT_DLL_EXPORT FXPoint
		{
public:
		FXPoint()				:	x(0), y(0)		{ }
		FXPoint(int vx, int vy) :	x(vx), y(vy)	{ }

		void	set(int vx, int vy)	{ x = vx; y = vy; }

		bool	operator==(const FXPoint &other)	{ return (other.x == x && other.y == y); }
		bool	operator!=(const FXPoint &other)	{ return (other.x != x || other.y != y); }

public:
		int		x;
		int		y;
		};


#endif // __FltkExt_FXPoint_h__