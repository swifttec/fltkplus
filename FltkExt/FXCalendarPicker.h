/*
**	FXCalendarPicker.h	A generic dialog class
*/

#ifndef __FltkExt_FXCalendarPicker_h__
#define __FltkExt_FXCalendarPicker_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXCalendarPicker : public FXWidget
		{
public:
		class Field
			{
		public:
			int		type;
			FXRect	rect;
			SWString	value;
			bool	selected;
			};

public:
		FXCalendarPicker(int X, int Y);

		SWDate		getSelectedDate()	{ return m_selectedDate; }

protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		void			init(const SWDate &dt);
		void			drawButton(const FXRect &dr, int style, bool selected);

protected:
		int					m_day;
		int					m_month;
		int					m_year;
		FXFont				m_fTitle;
		FXFont				m_fDays;
		FXFont				m_fDigits;
		FXColor				m_btnColor;
		FXColor				m_arrowColor;
		FXColor				m_selectedBgColor;
		FXColor				m_selectedFgColor;
		int					m_inputValue;
		int					m_selectedField;
		FXRect				m_rLeftArrow;
		FXRect				m_rRightArrow;
		FXRect				m_rMonth;
		FXRect				m_rYear;
		SWDate				m_selectedDate;
		};


#endif // __FltkExt_FXCalendarPicker_h__
