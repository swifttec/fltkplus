#include <FltkExt/FXPanelLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>

FXPanelLayout::FXPanelLayout(bool vertical, int gap, const FXPadding &pad) :
	m_vertical(vertical)
		{
		gapBetweenChildren(FXSize(gap, gap));
		padding(pad);
		}


FXPanelLayout::~FXPanelLayout()
		{
		}


FXSize
FXPanelLayout::getMinSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		sz=fx_getMinSize(pWidget);

			if (m_vertical)
				{
				res.cx = sw_math_max(res.cx, sz.cx);
				res.cy += sz.cy;
				}
			else
				{
				res.cx += sz.cx;
				res.cy = sw_math_max(res.cy, sz.cy);
				}
			}

		
		FXMargin	border;
		
		fx_getBorderWidths(m_pOwner, border);

		res.cx += m_padding.left + m_padding.right + border.left + border.right;
		res.cy += m_padding.top + m_padding.bottom + border.top + border.bottom;

		if (nchildren > 1)
			{
			if (m_vertical) res.cy += (nchildren-1) * m_gapBetweenChildren.cy;
			else res.cx += (nchildren-1) * m_gapBetweenChildren.cx;
			}

		return res;
		}


FXSize
FXPanelLayout::getMaxSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();
		bool	unlimitedWidth=false;
		bool	unlimitedHeight=false;

		for (int i=0;!(unlimitedWidth && unlimitedHeight)&&i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		szMin=fx_getMinSize(pWidget);
			FXSize		szMax=fx_getMaxSize(pWidget);

			if (m_vertical)
				{
				if (szMax.cx != 0)
					{
					if (res.cx == 0) res.cx = szMax.cx;
					else res.cx = sw_math_min(res.cx, szMax.cx);
					}
				if (res.cx != 0) res.cx = sw_math_max(res.cx, szMin.cx);

				if (szMax.cy == 0) unlimitedHeight = true;
				else res.cy += szMax.cy;
				}
			else
				{
				if (szMax.cy != 0)
					{
					if (res.cy == 0) res.cy = szMax.cy;
					else res.cy = sw_math_min(res.cy, szMax.cy);
					}
				if (res.cy != 0) res.cy = sw_math_max(res.cy, szMin.cy);

				if (szMax.cx == 0) unlimitedWidth = true;
				else res.cx += szMax.cx;
				}
			}

		FXMargin	border;
		
		fx_getBorderWidths(m_pOwner, border);

		if (unlimitedWidth)
			{
			res.cx = 0;
			}
		else if (res.cx != 0)
			{
			res.cx += m_padding.left + m_padding.right + border.left + border.right;
			if (nchildren > 1) res.cx += (nchildren-1) * m_gapBetweenChildren.cx;
			}

		if (unlimitedHeight)
			{
			res.cy = 0;
			}
		else if (res.cy != 0)
			{
			res.cy += m_padding.top + m_padding.bottom + border.top + border.bottom;
			if (nchildren > 1) res.cy += (nchildren-1) * m_gapBetweenChildren.cy;
			}

		return res;
		}


void
FXPanelLayout::addWidget(Fl_Widget *pWidget)
		{
		m_pOwner->add(pWidget);
		}

		
FXPanelLayout::ChildSizes
FXPanelLayout::getChildSizes(int idx)
		{
		ChildSizes	res;

		if (idx >= 0 && idx < getChildCount())
			{
			Fl_Widget	*pWidget=m_pOwner->child(idx);
			FXSize		szMin=fx_getMinSize(pWidget);
			FXSize		szMax=fx_getMaxSize(pWidget);

			if (m_vertical)
				{
				res.minsize = szMin.cy;
				res.maxsize = szMax.cy;
				}
			else
				{
				res.minsize = szMin.cx;
				res.maxsize = szMax.cx;
				}
			}
		else
			{
			res.maxsize = 0;
			res.minsize = 0;
			}

		return res;
		}


void
FXPanelLayout::layoutChildren()
		{
		Fl_Widget	*const *a;
		int			nchildren=m_pOwner->children();
		int			minsize=0, maxsize=0, unlimited=0;
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (nchildren > 1)
			{
			if (m_vertical) H -= (nchildren-1) * m_gapBetweenChildren.cx;
			else W -= (nchildren-1) * m_gapBetweenChildren.cy;
			}

		a = m_pOwner->array();
		for (int i=0;i<nchildren;i++)
			{
			ChildSizes		cs=getChildSizes(i);

			minsize += cs.minsize;
			maxsize += cs.maxsize;
			if (cs.maxsize == 0) unlimited++;
			}

		if (m_vertical)
			{
			if (maxsize > 0 && H > maxsize && !unlimited) H = maxsize;
			else if (H < minsize) H = minsize;
			}
		else
			{
			if (maxsize > 0 && W > maxsize && !unlimited) W = maxsize;
			else if (W < minsize) W = minsize;
			}

		if (nchildren > 0)
			{
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalExtraSpace=0;

			a = m_pOwner->array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				if (m_vertical) totalChildSpace += o->h();
				else totalChildSpace += o->w();
				}

			if (m_vertical) ds = H-totalChildSpace;
			else ds = W-totalChildSpace;

			int		canchange=0;
			SWFlags	changeflags;

			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				a = m_pOwner->array();
				for (int i=0;i<nchildren;i++)
					{
					Fl_Widget	*o=*a++;
					int			width=o->w(), height=o->h();

					if (pass == 0 || changeflags.getBit(i))
						{
						ChildSizes	cs=getChildSizes(i);

						if (ds != 0)
							{
							if (pass == 0)
								{
								cds = ds / (nchildren-i);
								}
							else
								{
								cds = ds / canchange;
								}

							if (cds == 0)
								{
								if (ds < 0) cds = -1;
								else cds = 1;
								}
							}

						if (m_vertical)
							{
							// Fixed width (same as parent)
							width = W;

							// calculate new height
							int		old=height;

							height += cds;
							if (height < cs.minsize) height = cs.minsize;
							if (cs.maxsize != 0 && height > cs.maxsize) height = cs.maxsize;
							else if (pass == 0 && (cs.maxsize == 0 || height < cs.maxsize))
								{
								canchange++;
								changeflags.setBit(i);
								}
							ds -= height-old;
							}
						else
							{
							// Fixed height (same as parent)
							height = H;

							// Variable width
							int		old=width;

							width += cds;
							if (width < cs.minsize) width = cs.minsize;
							if (cs.maxsize != 0 && width > cs.maxsize) width = cs.maxsize;
							else if (pass == 0 && (cs.maxsize == 0 || width < cs.maxsize))
								{
								canchange++;
								changeflags.setBit(i);
								}
							ds -= width-old;
							}
					
						if (pass == 1) canchange--;
						}

					int		ow=width;
					int		oh=height;
					FXSize	szMax=fx_getMaxSize(o);

					if (szMax.cx != 0 && szMax.cx < ow) ow = szMax.cx;
					if (szMax.cy != 0 && szMax.cy < oh) oh = szMax.cy;
					o->resize(X+xpos, Y+ypos, ow, oh);

					if (m_vertical) ypos += o->h() + m_gapBetweenChildren.cx;
					else xpos += o->w() + m_gapBetweenChildren.cy;
					}
				}
			}
		}
