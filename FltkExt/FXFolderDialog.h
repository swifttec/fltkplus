#ifndef __FltkExt_FXFolderDialog_h__
#define __FltkExt_FXFolderDialog_h__

#include <FltkExt/FltkExt.h>

#include <FL/Fl_Native_File_Chooser.H>

class FLTKEXT_DLL_EXPORT FXFolderDialog : public Fl_Native_File_Chooser
		{
public:
		FXFolderDialog(const SWString &folder, const SWString &title);
		~FXFolderDialog();

		int			doModal();
		
		SWString	getFolder()	{ return filename(); }

protected:
		SWString	m_title;
		SWString	m_folder;
		};


#endif // __FltkExt_FXFolderDialog_h__
