/****************************************************************************
**	fx_common_dialogs.h	Message box functions
****************************************************************************/


#ifndef __FltkExt_fx_common_dialogs_h__
#define __FltkExt_fx_common_dialogs_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/fx_msgbox.h>

#include <swift/SWString.h>

FLTKEXT_DLL_EXPORT int fx_selectColor(FXColor &color, const SWString &title, Fl_Widget *pOwner);

#endif // __FltkExt_fx_common_dialogs_h__
