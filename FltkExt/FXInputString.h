/*
**	FXInputString.h	A generic dialog class
*/

#ifndef __FltkExt_FXInputString_h__
#define __FltkExt_FXInputString_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>

class FLTKEXT_DLL_EXPORT FXInputString : public Fl_Input
		{
public:
		enum FilterMode
			{
			NoFilterChars=0,
			Exclude=1,
			Include=2
			};

		enum CaseMode
			{
			Unchanged=0,
			ToUpper,
			ToLower
			};

public:
		FXInputString(int W, int H);
		FXInputString(int X, int Y, int W, int H, const SWString &filterChars="", FilterMode filterMode=Exclude, CaseMode caseMode=Unchanged, bool multiline=false);

		/// Set the filter params
		void				setFilter(const SWString &chars, FilterMode mode, CaseMode cmode);

		void				setFilterChars(const SWString &v)					{ m_filterChars = v; }
		const SWString &	getFilterChars() const								{ return m_filterChars; }

		void				setFilterMode(FilterMode mode)						{ m_filterMode = mode; }
		FilterMode			getFilterMode() const								{ return (FilterMode)m_filterMode; }

		void				setCaseMode(CaseMode mode)							{ m_caseMode = mode; }
		CaseMode			setCaseMode() const									{ return (CaseMode)m_caseMode; }

protected:
		/// Overrides Fl_Input's filterChar method
		virtual bool		filterChar(int &nChar);

protected:
		SWString	m_filterChars;	///! the set of chars to filter
		int			m_filterMode;			///! the filter mode
		int			m_caseMode;
		};


#endif // __FltkExt_FXInputString_h__
