#include "FXGroup.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <FltkExt/fx_functions.h>
#include <FltkExt/FXLayout.h>
#include <FltkExt/fx_timer.h>

#include <swift/SWFileInfo.h>


static void
itemCallback(Fl_Widget *pWidget, void *p)
		{
		FXGroup	*pGroup=(FXGroup *)pWidget->parent();

		pGroup->onItemCallback(pWidget, p);
		}


FXGroup::FXGroup() :
	Fl_Group(0, 0, 0, 0),
	m_pLayout(NULL),
	m_deleteLayoutOnClose(false),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();

		end();
		}


FXGroup::FXGroup(int X, int Y, int W, int H) :
	Fl_Group(X, Y, W, H),
	m_pLayout(NULL),
	m_deleteLayoutOnClose(false),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();

		end();
		}



FXGroup::FXGroup(int X, int Y, int W, int H, const FXColor &bgcolor, const FXColor &bdcolor, const FXColor &fgcolor) :
	Fl_Group(X, Y, W, H),
	m_pLayout(NULL),
	m_deleteLayoutOnClose(false),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();
		setBackgroundColor(bgcolor);
		setBorderColor(bdcolor);
		setForegroundColor(fgcolor);

		end();
		}


FXGroup::FXGroup(int W, int H) :
	Fl_Group(0, 0, W, H),
	m_pLayout(NULL),
	m_deleteLayoutOnClose(false),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();

		end();
		}

FXGroup::~FXGroup()
		{
		fx_timer_destroyAll(this);
		clearLayout();
		clearBackgroundImage();
		}



void
FXGroup::init()
		{
		m_flags = 0;
		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, FX_COLOR_NONE, 0, 0);
		}



void
FXGroup::getContentsRect(FXRect &rect) const
		{
		FXRect	ir;
		FXRect	cr;

		getInnerRect(ir);
		getClientRect(cr);

		ir.deflate(m_padding.left, m_padding.top, m_padding.right, m_padding.bottom);
		rect.left = sw_math_max(ir.left, cr.left);
		rect.top = sw_math_max(ir.top, cr.top);
		rect.right = sw_math_min(ir.right, cr.right);
		rect.bottom = sw_math_min(ir.bottom, cr.bottom);
		}


void
FXGroup::getClientRect(FXRect &rect) const
		{
		getInnerRect(rect);

		int	bw=getBorderWidth();
		int	cw=getCornerWidth();
		
		if (cw > bw)
			{
			bw = (int)((cw-bw) * 0.293);
			rect.deflate(bw, bw, bw, bw);
			}
		}

void
FXGroup::getInnerRect(FXRect &rect) const
		{
		getWidgetRect(rect);
		
		int	bw=getBorderWidth();

		rect.deflate(bw, bw, bw, bw);
		}


void
FXGroup::getWidgetRect(FXRect &rect) const
		{
		rect.left = x();
		rect.right = x()+w()-1;
		rect.top = y();
		rect.bottom = y()+h()-1;
		}


// Definitions to support FXWidget FXGroup and FXWnd Common Methods
// to make the job of keeping FXWidget and FXWnd in sync easier

#define FLAG_LEFT_MOUSE				0x01		///< The left mouse button is down
#define FLAG_MIDDLE_MOUSE			0x02		///< The middle mouse button is down
#define FLAG_RIGHT_MOUSE			0x04		///< The right mouse button is down
#define FLAG_MOUSE_OVER				0x08		///< The mouse is over the widget
#define FLAG_FOCUS					0x10		///< The widget has focus
#define FLAG_NO_USE_LAYOUTSIZES		0x20		///< The widget has focus


// FXGroup Common Methods

void
FXGroup::setUseLayoutSizes(bool v)
		{
		if (!v) m_flags |= FLAG_NO_USE_LAYOUTSIZES;
		else m_flags &= ~FLAG_NO_USE_LAYOUTSIZES;
		}


FXSize
FXGroup::getMinSize() const
		{
		FXSize	res;

		if (m_pLayout != NULL && (m_flags & FLAG_NO_USE_LAYOUTSIZES) == 0)
			{
			res = m_pLayout->getMinSize();
			}
		else
			{
			res = m_minSize;
			}

		return res;
		}


FXSize
FXGroup::getMaxSize() const
		{
		FXSize	res;

		if (m_pLayout != NULL && (m_flags & FLAG_NO_USE_LAYOUTSIZES) == 0)
			{
			res = m_pLayout->getMaxSize();
			}
		else
			{
			res = m_maxSize;
			}

		return res;
		}


FXSize
FXGroup::getChildrenSize() const
		{
		FXRect	cr;

		getChildrenRect(cr);

		return cr.size();
		}


void
FXGroup::getChildrenRect(FXRect &rect) const
		{
		rect = FXRect(x(), y(), x(), y());
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=child(i);
			int			l=pWidget->x();
			int			t=pWidget->y();
			int			r=l+pWidget->w()-1;
			int			b=t+pWidget->h()-1;

			if (i == 0 || l < rect.left) rect.left = l;
			if (i == 0 || t < rect.top) rect.top = t;
			if (i == 0 || r > rect.right) rect.right = r;
			if (i == 0 || b > rect.bottom) rect.bottom = b;
			}
		}


void
FXGroup::layoutChildren()
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->layoutChildren();
			}
		}
		

void
FXGroup::clearLayout()
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->setOwner(NULL);
			if (m_deleteLayoutOnClose) delete m_pLayout;
			m_pLayout = NULL;
			m_deleteLayoutOnClose = false;
			}
		}

void
FXGroup::setLayout(FXLayout *pLayout, bool delflag)
		{
		clearLayout();
		if (pLayout != NULL)
			{
			m_pLayout = pLayout;
			m_deleteLayoutOnClose = delflag;
			m_pLayout->setOwner(this);
			}
		}


void
FXGroup::add(sw_uint32_t id, Fl_Widget *pWidget, bool addToGroupCallback)
		{
		pWidget->id(id);
		add(pWidget, addToGroupCallback);
		}


void
FXGroup::add(Fl_Widget *pWidget, bool addToGroupCallback)
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->addWidget(pWidget);
			}
		else
			{
			Fl_Group::add(pWidget);
			}

		if (addToGroupCallback)
			{
			pWidget->callback(itemCallback);
			}
		}



int
FXGroup::getWidgetCount() const
		{
		return children();
		}



Fl_Widget *
FXGroup::getWidget(int idx) const
		{
		Fl_Widget *pWidget=NULL;

		if (idx >= 0 && idx < children())
			{
			pWidget = child(idx);
			}

		return pWidget;
		}


int
FXGroup::getWidgetIndexAt(const FXPoint &pt) const
		{
		int		res=-1;
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget *pWidget=child(i);

			if (pWidget!= NULL)
				{
				FXRect		wr(FXPoint(pWidget->x(), pWidget->y()), FXSize(pWidget->w(), pWidget->h()));

				if (wr.contains(pt))
					{
					res = i;
					break;
					}
				}
			}

		return res;
		}


Fl_Widget *
FXGroup::getWidgetByID(int id) const
		{
		Fl_Widget	*pFoundWidget=NULL;
		int			nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget *pWidget=child(i);

			if (pWidget != NULL)
				{
				if ((int)pWidget->id() == id)
					{
					pFoundWidget = pWidget;
					break;
					}
				}
			}

		return pFoundWidget;
		}

// FXWidget Common Methods



sw_uint32_t
FXGroup::setIntervalTimer(sw_uint32_t localid, double seconds, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, seconds, this, pData);
		}


sw_uint32_t
FXGroup::setIntervalTimerMS(sw_uint32_t localid, int ms, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, (double)ms / 1000.0, this, pData);
		}


void
FXGroup::killIntervalTimer(sw_uint32_t id)
		{
		fx_timer_destroy(this, id);
		}
		

void
FXGroup::invalidate()
		{
		fx_invalidate(this);
		}


// FXWidget Common Methods
void
FXGroup::invalidateAll()
		{
		for (int i=0;i<children();i++)
			{
			fx_invalidate(child(i));
			}

		fx_invalidate(this);
		}


void
FXGroup::clearBackgroundImage()
		{
		if (m_pBgImage != NULL)
			{
			m_pBgImage->release();
			m_pBgImage = NULL;
			}

		delete m_pBgImageCopy;
		m_pBgImageCopy = NULL;
		}


void
FXGroup::setBackgroundImage(const SWString &filename)
		{
		clearBackgroundImage();
		if (SWFileInfo::isFile(filename))
			{
			m_pBgImage = Fl_Shared_Image::get(filename);
			}
		}


void
FXGroup::setBorder(int style, const FXColor &color, int width, int corner)
		{
		m_bdStyle = style;
		m_bdColor = color;
		m_bdWidth = width;
		m_bdCorner = corner;
		}


void
FXGroup::onDrawBorder()
		{
		if (m_bdColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;
		
			fx_draw_rect(FXRect(x(), y(), x()+w()-1, y()+h()-1), m_bdWidth, corner, m_bdColor, FX_COLOR_NONE); 
			}
		}


void
FXGroup::onDrawBackground()
		{
		if (m_bgColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;

			fx_draw_rect(FXRect(x(), y(), x()+w()-1, y()+h()-1), m_bdWidth, corner, FX_COLOR_NONE, m_bgColor); 
			}

		if (m_pBgImage != NULL)
			{
			if (m_pBgImageCopy != NULL)
				{
				if (m_pBgImageCopy->w() != w() || m_pBgImageCopy->h() != h())
					{
					delete m_pBgImageCopy;
					m_pBgImageCopy = NULL;
					}
				}

			if (m_pBgImageCopy == NULL)
				{
				m_pBgImageCopy = m_pBgImage->copy(w(), h());
				}
			
			if (m_pBgImageCopy != NULL)
				{
				m_pBgImageCopy->draw(x(), y(), w(), h(), 0, 0);
				}
			}
		}


void
FXGroup::onDraw()
		{
		Fl_Group::draw();
		}


void
FXGroup::draw()
		{
        fl_push_clip(x(),y(),w(),h());

		if (damage() & ~FL_DAMAGE_CHILD)
			{
			onDrawBackground();
			onDrawBorder();
			}

		onDraw();

		fl_pop_clip();
		}


void
FXGroup::onClose()
		{
		// Call onClose for all our children
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*p=*a++;
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(p);
			FXGroup		*pGroup=dynamic_cast<FXGroup *>(p);
			FXWnd		*pWnd=dynamic_cast<FXWnd *>(p);

			if (pWidget != NULL) pWidget->onClose();
			else if (pGroup != NULL) pGroup->onClose();
			else if (pWnd != NULL) pWnd->onClose();
			}
		}



void
FXGroup::onCreate()
		{
		// Call onCreate for all our children
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*p=*a++;
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(p);
			FXGroup		*pGroup=dynamic_cast<FXGroup *>(p);
			FXWnd		*pWnd=dynamic_cast<FXWnd *>(p);

			if (pWidget != NULL) pWidget->onCreate();
			else if (pGroup != NULL) pGroup->onCreate();
//			else if (pWnd != NULL) pWnd->onCreate();
			}
		}


bool		FXGroup::hasFocus() const									{ return ((m_flags & FLAG_FOCUS) != 0); }
bool		FXGroup::hasMouseOver() const								{ return ((m_flags & FLAG_MOUSE_OVER) != 0); }
int			FXGroup::onFocus()											{ return 0; }
int			FXGroup::onUnfocus()										{ return 0; }
int			FXGroup::onMouseEnter()										{ return 1; }
int			FXGroup::onMouseLeave()										{ return 1; }
int			FXGroup::onLeftMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXGroup::onMiddleMouseButton(int action, const FXPoint &pt)			{ return 0; }
int			FXGroup::onRightMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXGroup::onMouseMove(const FXPoint &pt)						{ return 0; }
int			FXGroup::onMouseDrag(const FXPoint &pt)						{ return 0; }
int			FXGroup::onMouseWheel(int dx, int dy, const FXPoint &pt)	{ return 0; }
bool		FXGroup::disabled() const									{ return !enabled(); }
bool		FXGroup::enabled() const									{ return (active() != 0); }
int			FXGroup::onDragAndDrop(int action, const FXPoint &pt)					{ return 0; }
int			FXGroup::onPaste(const SWString &text, const FXPoint &pt)				{ return 0; }
int			FXGroup::onKeyDown(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
int			FXGroup::onKeyUp(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
int			FXGroup::onShortcut(int code, const FXPoint &pt)						{ return 0; }
bool		FXGroup::onTimer(sw_uint32_t id, void *data)							{ return true; }
void		FXGroup::onItemCallback(Fl_Widget *pWidget, void *p)					{ }
int			FXGroup::onCommand(sw_uint32_t id)										{ return 0; }
int			FXGroup::onShow()														{ return 0; }
int			FXGroup::onHide()														{ return 0; }


int
FXGroup::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;
			}

		return res;
		}


int
FXGroup::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(FX_BUTTON_UP, pt);
				break;
			}

		return res;
		}


// Scrolling support
void		FXGroup::onHScroll(int pos)									{ }
void		FXGroup::onVScroll(int pos)									{ }
void		FXGroup::onScrollUp()										{ }
void		FXGroup::onScrollDown()										{ }
void		FXGroup::onScrollLeft()										{ }
void		FXGroup::onScrollRight()									{ }
void		FXGroup::onScrollHome()										{ }
void		FXGroup::onScrollEnd()										{ }
void		FXGroup::onScrollPageUp()									{ }
void		FXGroup::onScrollPageDown()									{ }
void		FXGroup::onScrollPageHome()									{ }
void		FXGroup::onScrollPageEnd()									{ }
void		FXGroup::onScrollPageLeft()									{ }
void		FXGroup::onScrollPageRight()								{ }


void
FXGroup::onScroll(int bar, int action, int pos)
		{

		if (bar == FX_SCROLLBAR_HORIZONTAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollLeft();			break;
				case FX_SCROLL_DOWN	:		onScrollRight();		break;
				case FX_SCROLL_FIRST:		onScrollHome();			break;
				case FX_SCROLL_LAST:		onScrollEnd();			break;
				case FX_SCROLL_PAGEUP:		onScrollPageLeft();		break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageRight();	break;
				case FX_SCROLL_TRACK:		onHScroll(pos);			break;
				}
			}
		else if (bar == FX_SCROLLBAR_VERTICAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollUp();		break;
				case FX_SCROLL_DOWN	:		onScrollDown();		break;
				case FX_SCROLL_FIRST:		onScrollPageHome();	break;
				case FX_SCROLL_LAST:		onScrollPageEnd();	break;
				case FX_SCROLL_PAGEUP:		onScrollPageUp();	break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageDown();	break;
				case FX_SCROLL_TRACK:		onVScroll(pos);		break;
				}
			}

		invalidate();
		}

int
FXGroup::handle(int eventcode)
		{
		int		res=0;

		m_lastEventCode = eventcode;
		m_lastEventPoint.x = Fl::event_x();
		m_lastEventPoint.y = Fl::event_y();

		switch (eventcode)
			{
			case FL_FOCUS:
				m_flags |= FLAG_FOCUS;
				res = onFocus();
				break;
			
			case FL_UNFOCUS:
				m_flags &= ~FLAG_FOCUS;
				res = onUnfocus();
				break;
			
			case FL_ENTER:
				m_flags |= FLAG_MOUSE_OVER;
				res = onMouseEnter();
				break;
			
			case FL_LEAVE:
				m_flags &= ~(FLAG_MOUSE_OVER|FLAG_LEFT_MOUSE|FLAG_MIDDLE_MOUSE|FLAG_RIGHT_MOUSE);
				res = onMouseLeave();
				break;
				
			case FL_MOVE:
				res = onMouseMove(m_lastEventPoint);
				break;
				
			case FL_DRAG:
				res = onMouseDrag(m_lastEventPoint);
				break;
		
			case FL_PUSH:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags |= FLAG_LEFT_MOUSE;		break;
					case FL_MIDDLE_MOUSE:	m_flags |= FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags |= FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseDown(button, m_lastEventPoint);
				}
				break;

			case FL_RELEASE:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags &= ~FLAG_LEFT_MOUSE;	break;
					case FL_MIDDLE_MOUSE:	m_flags &= ~FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags &= ~FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseUp(button, m_lastEventPoint);
				}
				break;
			
			case FL_MOUSEWHEEL:
				res = onMouseWheel(Fl::event_dx(), Fl::event_dy(), m_lastEventPoint);
				break;

			case FL_CLOSE:
				onClose();
				break;

			case FL_DND_ENTER:
				res = onDragAndDrop(1, m_lastEventPoint);
				break;

			case FL_DND_DRAG:
				res = onDragAndDrop(2, m_lastEventPoint);
				break;

			case FL_DND_LEAVE:
				res = onDragAndDrop(3, m_lastEventPoint);
				break;

			case FL_DND_RELEASE:
				res = onDragAndDrop(4, m_lastEventPoint);
				break;

			case FL_PASTE:
				res = onPaste(Fl::event_text(), m_lastEventPoint);
				break;
   			
			case FL_KEYDOWN:
				res = onKeyDown(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;
   			
			case FL_KEYUP:
				res = onKeyUp(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;

			case FL_SHORTCUT:
				res = onShortcut(Fl::event_key(), m_lastEventPoint);
				break;
			
			case FL_SHOW:
				res = onShow();
				break;
			
			case FL_HIDE:
				res = onHide();
				break;
			}

		// TRACE("FXGroup(%x)::handle(%s) at %d,%d = %d\n", this, fx_event_name(eventcode).c_str(), m_lastEventPoint.x, m_lastEventPoint.y, res);
		if (res == 0) res = Fl_Group::handle(eventcode);

		return res;
		}


void
FXGroup::resize(int X, int Y, int W, int H)
		{
		if (m_pLayout != NULL)
			{
			// We are handling the resizing, so disable it whilst the underlying code works
			Fl_Widget	*tmp=resizable();

			resizable(NULL);
			Fl_Group::resize(X,Y,W,H);
			resizable(tmp);

			FXSize	minsize, maxsize;

			minsize = m_pLayout->getMinSize();
			maxsize = m_pLayout->getMaxSize();

			int		width=W, height=H;

			if (maxsize.cx != 0 && width > maxsize.cx) width = maxsize.cx;
			if (width < minsize.cx) width = minsize.cx;

			if (maxsize.cy != 0 && height > maxsize.cy) height = maxsize.cy;
			if (height < minsize.cy) height = minsize.cy;

//			Fl_Group::resize(X,Y,width,height);
			m_pLayout->layoutChildren();
			}
		else
			{
			Fl_Group::resize(X,Y,W,H);
			}
		}


void
FXGroup::setItemFocus(int itemid)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			pWidget->take_focus();
			}
		}
