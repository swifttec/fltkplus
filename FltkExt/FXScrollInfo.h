/*
**	FXScrollInfo.h		A class representing scroll info
*/

#ifndef __FltkExt_FXScrollInfo_h__
#define __FltkExt_FXScrollInfo_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <xplatform/sw_math.h>

class FLTKEXT_DLL_EXPORT FXScrollInfo
		{
public:
		FXScrollInfo();
		FXScrollInfo(int from, int to, int page, int pos);

		void	clear();

		int		firstPosition() const	{ return m_rangeFrom; }
		int		lastPosition() const	{ return sw_math_max(m_rangeFrom, m_rangeTo - m_pageSize); }

		void	position(int v)			{ setPosition(v); }

		int		position() const		{ return m_position; }
		int		rangeFrom() const		{ return m_rangeFrom; }
		int		rangeTo() const			{ return m_rangeTo; }
		int		pageSize() const		{ return m_pageSize; }
		int		lineSize() const		{ return m_lineSize; }
		int		rangeSize() const		{ return m_rangeTo - m_rangeFrom; }

		void	setRange(int from, int to);
		void	setRange(int from, int to, int page);
		void	setPosition(int pos);
		void	setLineSize(int pos);
		void	setPageSize(int pos);

		void	maxPos(int v)						{ setRange(m_rangeFrom, v); }
		int		maxPos() const						{ return m_rangeTo; }

		void	minPos(int v)						{ setRange(v, m_rangeTo); }
		int		minPos() const						{ return m_rangeFrom; }

		void	currentPos(int v)					{ setPosition(v); }
		int		currentPos() const					{ return m_position; }

		void	changePosition(int v)				{ setPosition(position() + v); }
		void	changePositionByLines(int nlines)	{ setPosition(position() + (lineSize() * nlines)); }
		void	changePositionByPages(int npages)	{ setPosition(position() + (pageSize() * npages)); }

protected:
		int		m_position;
		int		m_rangeFrom;
		int		m_rangeTo;
		int		m_lineSize;
		int		m_pageSize;
		};

#endif // __FltkExt_FXScrollInfo_h__