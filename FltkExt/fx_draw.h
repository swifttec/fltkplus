/**
***	fx_draw.h
**/

#ifndef __FltkExt_fx_draw_h__
#define __FltkExt_fx_draw_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXRect.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXTextStyle.h>
#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXPen.h>
#include <FltkExt/FXBrush.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

/// Test to see if the current drawing context is for printing
FLTKEXT_DLL_EXPORT bool			fx_draw_isPrinting();

/// Get the DPI settings for the current drawing context
FLTKEXT_DLL_EXPORT void			fx_draw_getDPI(double &dpiX, double &dpiY);

/// Convert the specified number of mm into horizontal pixels for the current drawing context
FLTKEXT_DLL_EXPORT double		fx_draw_mmToPixelsX(double v);

/// Convert the specified number of mm into vertical pixels for the current drawing context
FLTKEXT_DLL_EXPORT double		fx_draw_mmToPixelsY(double v);

/// Get the extent (size) of the given text using the current font
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text);

/// Get the extent (size) of the given text using the current font when constrained to the given width
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, int width, int flags=0);

/// Get the extent (size) of the given text using the font specified
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, const FXFont &font);

/// Get the extent (size) of the given text using the text style specified
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, const FXTextStyle &style);

/// Get the height of the given text using the text style specified when constrained to the given width
FLTKEXT_DLL_EXPORT int			fx_getTextHeight(const SWString &text, int width, const FXTextStyle &style);

/// Get the height of the given text using the text style specified when constrained to the given width
FLTKEXT_DLL_EXPORT int			fx_getTextHeight(const SWString &text, int width);

/// Get the extent (size) of the given text using the font specified when constrained to the given width
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, int width, const FXFont &font);

/// Get the extent (size) of the given text using the font specified when constrained to the given width
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, int width, const FXColorFont &font);

/// Get the extent (size) of the given text using the text style specified when constrained to the given width
FLTKEXT_DLL_EXPORT FXSize		fx_getTextExtent(const SWString &text, int width, const FXTextStyle &style);

/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &rect, const FXTextStyle &style);

/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &rect, int align);

/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXFont &font);

/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXColor &color);

/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXColorFont &font);


/// Draw the text in the given rect using the alignment specified using the current color and font
FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, int left, int top, int width, int height, int align);

FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXColorFont &font, double minsize=4.0, double *pSizeUsed=NULL);

FLTKEXT_DLL_EXPORT void			fx_draw_text(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXFont &font, const FXColor &textcolor=FXColor(0, 0, 0), double minsize=4.0, double *pSizeUsed=NULL);


/// Draw a rectangle with optional border and fill
FLTKEXT_DLL_EXPORT void			fx_draw_fillRect(const FXRect &rect, const FXColor &fillColor);

/// Draw a rectangle
FLTKEXT_DLL_EXPORT void			fx_draw_frameRect(const FXRect &rect, const FXColor &fillColor);

/// Draw a rectangle with optional border and fill
FLTKEXT_DLL_EXPORT void			fx_draw_fillRect(const FXRect &rect, const FXBrush &brush);

/// Draw a rectangle with optional border and fill
FLTKEXT_DLL_EXPORT void			fx_draw_frameRect(const FXRect &rect, const FXBrush &brush);

/// Draw a rectangle with optional border and fill
FLTKEXT_DLL_EXPORT void			fx_draw_rect(const FXRect &rect, int borderWidth, int cornerDepth, const FXColor &borderColor, const FXColor &fillColor=FX_COLOR_NONE);

FLTKEXT_DLL_EXPORT void			fx_draw_arrowButton(const FXRect &dr, int style, const FXColor &btnColor, const FXColor &arrowColor, bool selected);
FLTKEXT_DLL_EXPORT void			fx_draw_tick(const FXRect &dr, const FXColor &color);
FLTKEXT_DLL_EXPORT void			fx_draw_cross(const FXRect &dr, const FXColor &color);
FLTKEXT_DLL_EXPORT void			fx_draw_triangle(int x1, int y1, int x2, int y2, int x3, int y3, const FXColor &color, const FXColor &fillColor=FX_COLOR_NONE);

FLTKEXT_DLL_EXPORT void			fx_draw_ellipse(const FXRect &rect);
FLTKEXT_DLL_EXPORT void			fx_draw_ellipse(const FXRect &rect, const FXColor &color);

FLTKEXT_DLL_EXPORT void			fx_draw_arc(const FXRect &rect, double aStart, double aFinish);
FLTKEXT_DLL_EXPORT void			fx_draw_arc(const FXRect &rect, const FXColor &color, double aStart, double aFinish);

FLTKEXT_DLL_EXPORT void			fx_draw_pie(const FXRect &rect, double aStart, double aFinish);
FLTKEXT_DLL_EXPORT void			fx_draw_pie(const FXRect &rect, const FXColor &color, double aStart, double aFinish);

FLTKEXT_DLL_EXPORT void			fx_draw_image(sw_byte_t *pData, const FXRect &dr, const FXRect &sr, int bytesPerPixel, int bytesPerLine);
FLTKEXT_DLL_EXPORT void			fx_draw_image(sw_byte_t *pData, int posX, int posY, int W, int H, int offX, int offY, int bytesPerPixel, int bytesPerLine);

FLTKEXT_DLL_EXPORT void			fx_draw_line(int fromX, int fromY, int toX, int toY, const FXColor &color, int style=FX_LINESTYLE_SOLID, int linewidth=1);
FLTKEXT_DLL_EXPORT void			fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo, const FXColor &color, int style=FX_LINESTYLE_SOLID, int linewidth=1);
FLTKEXT_DLL_EXPORT void			fx_draw_line(int fromX, int fromY, int toX, int toY, const FXPen &pen);
FLTKEXT_DLL_EXPORT void			fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo, const FXPen &pen);
FLTKEXT_DLL_EXPORT void			fx_draw_line(int fromX, int fromY, int toX, int toY);
FLTKEXT_DLL_EXPORT void			fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo);
FLTKEXT_DLL_EXPORT void			fx_draw_moveTo(int X, int Y);
FLTKEXT_DLL_EXPORT void			fx_draw_lineTo(int X, int Y);

FLTKEXT_DLL_EXPORT void			fx_draw_setDefaultLineStyle();
FLTKEXT_DLL_EXPORT void			fx_draw_setLineStyle(const FXPen &pen);
FLTKEXT_DLL_EXPORT void			fx_draw_setLineStyle(const FXColor &color, int linestyle, int linewidth);

FLTKEXT_DLL_EXPORT void			fx_draw_box(const FXRect &br, const FXColor &borderColor, int borderWidth, const FXColor &bgcolor, int cornerDepth);

FLTKEXT_DLL_EXPORT void			fx_setTextColor(const FXColor &obj);
FLTKEXT_DLL_EXPORT void			fx_setBkColor(const FXColor &obj);

FLTKEXT_DLL_EXPORT void			fx_select(const FXFont &obj);
FLTKEXT_DLL_EXPORT void			fx_select(const FXColor &obj);

#endif // __FltkExt_fx_draw_h__
