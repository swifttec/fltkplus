#include "FXButtonBar.h"
#include "FXGroup.h"
#include "FXWnd.h"
#include "FXSeparator.h"
#include "FXImageButton.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


void
FXButtonBar::itemCallback(Fl_Widget *pWidget)
		{
		FXButtonBar	*pSheet=(FXButtonBar *)pWidget->parent();

		pSheet->onCallback(pWidget);
		}


FXButtonBar::FXButtonBar(int X, int Y, int W, int H, bool vertical, const FXMinMaxSize &mxButtonSizes, int minGap, int maxGap, const FXPadding &barPadding, const FXPadding &btnPadding) :
	FXGroup(X, Y, W, H),
	m_vertical(vertical),
	m_minGap(minGap),
	m_maxGap(maxGap),
	m_padding(barPadding),
	m_btnpadding(btnPadding),
	m_align(0),
	m_minMaxButtonSize(mxButtonSizes),
	m_handleKeys(true)
		{
		}


FXButtonBar::FXButtonBar(int X, int Y, int W, int H, bool vertical, int defbtnsize, int gap, int btnpad) :
	FXGroup(X, Y, W, H),
	m_vertical(vertical),
	m_minGap(gap),
	m_maxGap(gap),
	m_padding(gap, gap, gap, gap),
	m_btnpadding(FXPadding(btnpad, btnpad, btnpad, btnpad)),
	m_align(0),
	m_handleKeys(true)
		{
		setBackgroundColor("#000");
		setForegroundColor("#fff");
		setBorderColor("#000");

		m_minMaxButtonSize.maxSize.cx = 0;
		m_minMaxButtonSize.maxSize.cy = 0;
		if (m_vertical)
			{
			m_minMaxButtonSize.minSize.cx = 16;
			m_minMaxButtonSize.minSize.cy = defbtnsize;
			}
		else
			{
			m_minMaxButtonSize.minSize.cx = defbtnsize;
			m_minMaxButtonSize.minSize.cy = 16;
			}

		end();

		// Initialise the default button styles
		m_defaultButtonStyles[FXButton::Normal] = FXButtonStyle("#000", "#ccc");
		m_defaultButtonStyles[FXButton::Hover] = FXButtonStyle("#000", "#ddd");
		m_defaultButtonStyles[FXButton::Pressed] = FXButtonStyle("#000", "#eee");
		m_defaultButtonStyles[FXButton::Disabled] = FXButtonStyle("#aaa", "#ccc");

		m_defaultButtonStyles[FXButton::SelectedNormal] = FXButtonStyle("#000", "#9df");
		m_defaultButtonStyles[FXButton::SelectedHover] = FXButtonStyle("#000", "#ddd");
		m_defaultButtonStyles[FXButton::SelectedPressed] = FXButtonStyle("#000", "#eee");
		m_defaultButtonStyles[FXButton::SelectedDisabled] = FXButtonStyle("#aaa", "#ccc");
		}


FXButtonBar::~FXButtonBar()
		{
		}

void
FXButtonBar::enableKeys(bool v)
		{
		m_handleKeys = v;

		int				nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;

			FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

			if (pButton != NULL)
				{
				pButton->enableKeys(m_handleKeys);
				}
			}
		}


void
FXButtonBar::setDefaultButtonStyle(int idx, const FXButtonStyle &style)
		{
		if (idx >= 0 && idx < FXButton::MaxStyle)
			{
			m_defaultButtonStyles[idx] = style;
			}
		}


void
FXButtonBar::getDefaultButtonStyle(int idx, FXButtonStyle &style)
		{
		if (idx >= 0 && idx < FXButton::MaxStyle)
			{
			m_defaultButtonStyles[idx] = style;
			}
		}


void
FXButtonBar::addSeparator(int width)
		{
		FXSeparator	*pItem=new FXSeparator();

		addItem(0, pItem);

		pItem->setBackgroundColor(getBackgroundColor());
		if (m_vertical) pItem->size(w(), width);
		else pItem->size(width, h());
		}


FXButton *
FXButtonBar::addButton(int id, const SWString &text, const SWString &image, bool enabled)
		{
		FXButton	*pButton = new FXButton(text, image);

		pButton->size(m_minMaxButtonSize.minSize.cx, m_minMaxButtonSize.minSize.cy);
		pButton->padding(m_btnpadding);
		pButton->enableKeys(m_handleKeys);

		for (int i=0;i<FXButton::MaxStyle;i++)
			{
			pButton->setButtonStyle(i, m_defaultButtonStyles[i], false);
			}
		
		if (enabled) pButton->activate();
		else pButton->deactivate();

		addItem(id, pButton);

		resize(x(), y(), w(), h());
		redraw();

		return pButton;
		}


FXImageButton *
FXButtonBar::addImageButton(int id, const SWString &image, bool enabled)
		{
		FXImageButton	*pButton = new FXImageButton(id, image, m_minMaxButtonSize.minSize.cx, m_minMaxButtonSize.minSize.cy);

		pButton->size(m_minMaxButtonSize.minSize.cx, m_minMaxButtonSize.minSize.cy);
		pButton->padding(m_btnpadding);
		pButton->enableKeys(m_handleKeys);

		for (int i=0;i<FXButton::MaxStyle;i++)
			{
			pButton->setButtonStyle(i, m_defaultButtonStyles[i], false);
			}
		
		if (enabled) pButton->activate();
		else pButton->deactivate();

		addItem(id, pButton);

		resize(x(), y(), w(), h());
		redraw();

		return pButton;
		}


void
FXButtonBar::addItem(int itemid, Fl_Widget *pItem)
		{
		fx_group_addItem(this, itemid, pItem);
		pItem->callback(itemCallback);
		}


void
FXButtonBar::selectItem(int itemid, bool v)
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;

			FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

			if (pButton != NULL)
				{
				pButton->select((int)pButton->id()==itemid?v:false);
				}
			}
		}


void
FXButtonBar::deselectAllItems()
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;
			FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

			if (pButton != NULL)
				{
				pButton->select(false);
				}
			}
		}

void
FXButtonBar::enableItem(int itemid, bool v)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}

void
FXButtonBar::enableAllItems(bool v)
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;

			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}


void
FXButtonBar::onCallback(Fl_Widget *pWidget)
		{
		if (pWidget != NULL)
			{
			do_callback(pWidget);

			bool	done=false;

			if (!done)
				{
				FXGroup	*p=dynamic_cast<FXGroup *>(parent());

				if (p != NULL)
					{
					p->onItemCallback(pWidget, NULL);
					done = true;
					}
				}

			if (!done)
				{
				FXWnd	*p=dynamic_cast<FXWnd *>(parent());

				if (p != NULL)
					{
					p->onItemCallback(pWidget, NULL);
					done = true;
					}
				}
			}
		}

int
FXButtonBar::getSeparatorCount() const
		{
		int			nchildren=children();
		Fl_Widget	*const *a = array();
		int			count=0;

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*o=*a++;

			if (dynamic_cast<FXSeparator *>(o) != NULL) count++;
			}

		return count;
		}


int
FXButtonBar::getButtonCount() const
		{
		int			nchildren=children();
		Fl_Widget	*const *a = array();
		int			count=0;

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*o=*a++;

			if (dynamic_cast<FXButton *>(o) != NULL) count++;
			}

		return count;
		}


int
FXButtonBar::getSeparatorSpace() const
		{
		int			nchildren=children();
		Fl_Widget	*const *a = array();
		int			count=0;

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*o=*a++;
			FXSeparator	*p=dynamic_cast<FXSeparator *>(o);

			if (p != NULL)
				{
				if (m_vertical) count += p->h();
				else count += p->w();
				}
			}

		return count;
		}


void
FXButtonBar::resize(int X, int Y, int W, int H)
		{
		int		nchildren=children();
		int		nbuttons=getButtonCount();

		Fl_Widget::resize(X,Y,W,H); // make new xywh values visible for children


		if (nbuttons > 0)
			{
			Fl_Widget	*const *a = array();
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalSeparatorSpace=getSeparatorSpace();
			int			nseparators=getSeparatorCount();
			int			nbuttons=getButtonCount();
			FXSize		szButton, szSpace;
			int			gap=m_maxGap;

			szSpace.cx = W - m_padding.left - m_padding.right;
			szSpace.cy = H - m_padding.top - m_padding.bottom;

			if (m_vertical) szSpace.cy -= totalSeparatorSpace;
			else  szSpace.cx -= totalSeparatorSpace;
			szButton.cx = m_minMaxButtonSize.maxSize.cx;
			szButton.cy = m_minMaxButtonSize.maxSize.cy;

			if (m_vertical)
				{
				if (szButton.cx == 0) szButton.cx = szSpace.cx;
				if (szButton.cy == 0) szButton.cy = szSpace.cy / nbuttons;
				if (szButton.cy < m_minMaxButtonSize.minSize.cy) szButton.cy = m_minMaxButtonSize.minSize.cy;
				if (szButton.cx > szSpace.cx) szButton.cx = szSpace.cx;
				if (szButton.cx < m_minMaxButtonSize.minSize.cx) szButton.cx = m_minMaxButtonSize.minSize.cx;

				for (bool adjgap=true;;adjgap=!adjgap)
					{
					totalChildSpace = (nbuttons * szButton.cy);
					if (nbuttons > 1) totalChildSpace += (nbuttons-1) * gap;
					if (nseparators > 1) totalChildSpace += (nseparators-1) * gap;
					if (totalChildSpace <= szSpace.cy) break;

					if (adjgap && gap > m_minGap) gap--;
					else if (szButton.cy > m_minMaxButtonSize.minSize.cy) szButton.cy--;
					else break;
					}
				}
			else
				{
				if (szButton.cy == 0) szButton.cy = szSpace.cy;
				if (szButton.cx == 0) szButton.cx = szSpace.cx / nbuttons;
				if (szButton.cx < m_minMaxButtonSize.minSize.cx) szButton.cx = m_minMaxButtonSize.minSize.cx;
				if (szButton.cy > szSpace.cy) szButton.cy = szSpace.cy;
				if (szButton.cy < m_minMaxButtonSize.minSize.cy) szButton.cy = m_minMaxButtonSize.minSize.cy;

				for (bool adjgap=true;;adjgap=!adjgap)
					{
					totalChildSpace = (nbuttons * szButton.cx);
					if (nbuttons > 1) totalChildSpace += (nbuttons-1) * gap;
					if (nseparators > 1) totalChildSpace += (nseparators-1) * gap;
					if (totalChildSpace <= szSpace.cx) break;

					if (adjgap && gap > m_minGap) gap--;
					else if (szButton.cx > m_minMaxButtonSize.minSize.cx) szButton.cx--;
					else break;
					}
				}

			if (m_vertical) ds = H-totalChildSpace-totalSeparatorSpace;
			else ds = W-totalChildSpace-totalSeparatorSpace;

			int			xpos=m_padding.left, ypos=m_padding.top;

			if (m_vertical)
				{
				if (m_align == 2) ypos=H-ds;
				else if (m_align == 1) ypos=ds / 2;
				else ypos=m_padding.top;
				}
			else
				{
				if (m_align == 2) xpos=W-ds;
				else if (m_align == 1) xpos=ds / 2;
				else xpos=m_padding.left;
				}

			a = array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				if (dynamic_cast<FXButton *>(o) != NULL)
					{
					o->resize(X+xpos, Y+ypos, szButton.cx, szButton.cy);
					}
				else
					{
					int		ow=m_vertical?szButton.cx:o->w();
					int		oh=m_vertical?o->h():szButton.cy;

					o->resize(X+xpos, Y+ypos, ow, oh);
					}

				if (m_vertical) ypos += o->h()+gap;
				else xpos += o->w()+gap;
				}
			}
		}
