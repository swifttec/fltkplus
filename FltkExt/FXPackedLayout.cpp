#include <FltkExt/FXPackedLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>

FXPackedLayout::FXPackedLayout(int packing, int alignment, int xgap, int ygap) :
	m_packing(packing),
	m_alignment(alignment)
		{
		gapBetweenChildren(FXSize(xgap, ygap));
		}


FXPackedLayout::~FXPackedLayout()
		{
		}


FXSize
FXPackedLayout::getMinSize() const
		{
		FXSize	res;

		if (!isFlagSet(NoMinSize))
			{
			int		nchildren=getChildCount();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*pWidget=m_pOwner->child(i);

				res.cx = sw_math_max(res.cx, pWidget->w());
				res.cy = sw_math_max(res.cy, pWidget->h());
				}
			}

		return res;
		}


FXSize
FXPackedLayout::getMaxSize() const
		{
		FXSize	res;

		if (!isFlagSet(NoMaxSize))
			{
			int		nchildren=getChildCount();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*pWidget=m_pOwner->child(i);

				if (m_packing == 1)
					{
					if (i != 0) res.cy += m_gapBetweenChildren.cy;
					res.cy += pWidget->h();
					}
				else
					{
					if (i != 0) res.cx += m_gapBetweenChildren.cx;
					res.cx += pWidget->w();
					}
				}
			}

		return res;
		}


void
FXPackedLayout::addWidget(Fl_Widget *pWidget)
		{
		m_pOwner->add(pWidget);
		}



void
FXPackedLayout::addWidget(Fl_Widget *pWidget, int xpos, int ypos)
		{
		addWidget(FX_IDSTATIC, pWidget, xpos, ypos);
		}


void
FXPackedLayout::addWidget(ulong id, Fl_Widget *pWidget, int xpos, int ypos)
		{
		addWidget(id, pWidget, xpos, ypos, pWidget->w(), pWidget->h());
		}


void
FXPackedLayout::addWidget(ulong id, Fl_Widget *pWidget, int X, int Y, int W, int H)
		{
		int	xpos=0, ypos=0;

		if (m_pOwner != NULL)
			{
			xpos = m_pOwner->x();
			ypos = m_pOwner->y();
			}

		pWidget->id(id);
		pWidget->resize(xpos + X, ypos + Y, W, H);
		m_pOwner->add(pWidget);
		}


void
FXPackedLayout::layoutChildren()
		{
		if (m_packing == 1) layoutChildrenInColumns();
		else layoutChildrenInRows();
		}


class FXPackedLayoutGroup
	{
public:
	SWArray<Fl_Widget *>	children;
	FXSize					size;
	};

void
FXPackedLayout::layoutChildrenInRows()
		{
		int			nchildren=m_pOwner->children();
		Fl_Widget	*const *a;
		int			xpos=0, ypos=0;
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (nchildren > 0)
			{
			SWArray<FXPackedLayoutGroup>	grouplist;
			int								groupcount=0;
			int								totalheight=0;
			FXPackedLayoutGroup				*pGroup=&grouplist[groupcount++];

			a = m_pOwner->array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;
				int			width=o->w(), height=o->h();

				if (pGroup->children.size() != 0)
					{
					xpos += m_gapBetweenChildren.cx;
					}

				if (xpos > 0 && (xpos + width) > W)
					{
					xpos = 0;
					ypos += pGroup->size.cy + m_gapBetweenChildren.cy;
					if (totalheight != 0) totalheight += m_gapBetweenChildren.cy;
					totalheight += pGroup->size.cy;
					
					pGroup = &grouplist[groupcount++];
					}

				if (pGroup->children.size() != 0)
					{
					pGroup->size.cx += m_gapBetweenChildren.cx;
					}

				pGroup->children.add(o);
				xpos += width;
				pGroup->size.cx += width;
				pGroup->size.cy = sw_math_max(pGroup->size.cy, height);
				}

			if (totalheight != 0) totalheight += m_gapBetweenChildren.cy;
			totalheight += pGroup->size.cy;

			xpos = ypos = 0;

			switch (m_alignment & FX_VALIGN_MASK)
				{
				case FX_VALIGN_CENTER:
					ypos = (H - totalheight) / 2;
					break;

				case FX_VALIGN_BOTTOM:
					ypos = H - totalheight;
					break;
				}

			for (int i=0;i<groupcount;i++)
				{
				FXPackedLayoutGroup	&group=grouplist[i];
				xpos = 0;

				switch (m_alignment & FX_HALIGN_MASK)
					{
					case FX_HALIGN_CENTER:
						xpos = (W - group.size.cx) / 2;
						break;

					case FX_HALIGN_RIGHT:
						xpos = W - group.size.cx;
						break;
					}

				for (int j=0;j<(int)group.children.size();j++)
					{
					Fl_Widget	*o=group.children[j];

					if (j != 0) xpos += m_gapBetweenChildren.cx;
					o->resize(X+xpos, Y+ypos, o->w(), o->h());
					xpos += o->w();
					}

				ypos += group.size.cy + m_gapBetweenChildren.cy;
				}
  			}
		}

void
FXPackedLayout::layoutChildrenInColumns()
		{
		int			nchildren=m_pOwner->children();
		Fl_Widget	*const *a;
		int			xpos=0, ypos=0;
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (nchildren > 0)
			{
			SWArray<FXPackedLayoutGroup>	grouplist;
			int								groupcount=0;
			int								totalwidth=0;
			FXPackedLayoutGroup				*pGroup=&grouplist[groupcount++];

			a = m_pOwner->array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;
				int			width=o->w(), height=o->h();

				if (pGroup->children.size() != 0)
					{
					ypos += m_gapBetweenChildren.cy;
					}

				if (ypos > 0 && (ypos + height) > H)
					{
					ypos = 0;
					xpos += pGroup->size.cx + m_gapBetweenChildren.cx;
					if (totalwidth != 0) totalwidth += m_gapBetweenChildren.cx;
					totalwidth += pGroup->size.cx;
					
					pGroup = &grouplist[groupcount++];
					}

				if (pGroup->children.size() != 0)
					{
					pGroup->size.cy += m_gapBetweenChildren.cy;
					}

				pGroup->children.add(o);
				ypos += height;
				pGroup->size.cy += height;
				pGroup->size.cx = sw_math_max(pGroup->size.cx, width);
				}

			if (totalwidth != 0) totalwidth += m_gapBetweenChildren.cx;
			totalwidth += pGroup->size.cx;

			xpos = ypos = 0;

			switch (m_alignment & FX_HALIGN_MASK)
				{
				case FX_HALIGN_CENTER:
					xpos = (W - totalwidth) / 2;
					break;

				case FX_HALIGN_RIGHT:
					xpos = W - totalwidth;
					break;
				}

			for (int i=0;i<groupcount;i++)
				{
				FXPackedLayoutGroup	&group=grouplist[i];
				ypos = 0;

				switch (m_alignment & FX_VALIGN_MASK)
					{
					case FX_VALIGN_CENTER:
						ypos = (H - group.size.cy) / 2;
						break;

					case FX_VALIGN_BOTTOM:
						ypos = H - group.size.cy;
						break;
					}

				for (int j=0;j<(int)group.children.size();j++)
					{
					Fl_Widget	*o=group.children[j];

					if (j != 0) ypos += m_gapBetweenChildren.cy;
					o->resize(X+xpos, Y+ypos, o->w(), o->h());
					ypos += o->h();
					}

				xpos += group.size.cx + m_gapBetweenChildren.cx;
				}
  			}
		}
