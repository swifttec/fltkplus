#include "FXPopupMenu.h"

#include <FL/Fl_Menu.H>

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>


FXPopupMenu::FXPopupMenu(Fl_Widget *pOwner) :
	m_pOwner(pOwner)
		{
		FXMenuItem	*pDummy=m_topLevelMenu.add(FXMenuItem(0, "dummy"));

		m_pMenu = pDummy->createSubMenu();
		}


FXPopupMenu::~FXPopupMenu()
		{
		}



void
FXPopupMenu::add(sw_uint32_t id, const SWString &text)
		{
		m_pMenu->add(id, text, 0, menuCallback);
		}


void
FXPopupMenu::add(const FXMenuItem &item)
		{
		m_pMenu->add(item);
		}


void
FXPopupMenu::addSeparator()
		{
		if (m_pMenu->size() != 0)
			{
			m_pMenu->getItemAt((int)m_pMenu->size()-1)->setFlags(FXMenuItem::SeparatorAfter);
			}
		}



void
FXPopupMenu::check(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::CheckBox);
			pItem->setFlags(FXMenuItem::Checked, v);
			}
		}


void
FXPopupMenu::enable(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::Disabled, !v);
			}
		}


FXMenuItem *
FXPopupMenu::findItem(sw_uint32_t id)
		{
		return m_pMenu->findItemByID(id);
		}




void
FXPopupMenu::menuCallback(Fl_Widget *pWidget, void *userdata)
		{
		FXMenuItem	*pItem=(FXMenuItem *)userdata;

		fx_widget_handleCommand(pWidget->parent(), pItem->id());

		}


void
FXPopupMenu::displayAndTrack(const FXPoint &pt)
		{
		/*
		int			nitems=(int)m_menuItems.size();
        Fl_Menu_Item *menu;
		int			idx=0;

		menu = new Fl_Menu_Item[nitems+1];
		memset(menu, 0, (nitems+1) * sizeof(Fl_Menu_Item));

		for (int i=0;i<nitems;i++)
			{
			FXMenuItem		&item=m_menuItems[i];
			Fl_Menu_Item	*p=&menu[idx];
			
			item.setFltkMenuItem(p);
			p->shortcut_ = 0;
			p->flags = 0;
			p->labeltype_ = 0;
			p->labelfont_ = 0;
			p->labelsize_ = 0;
			p->labelcolor_ = 0;
			p->text = item.text();
			p->callback_ = menuCallback;
			p->user_data_ = this;

			if (item.isFlagSet(FXMenuItem::CheckBox)) p->flags |= FL_MENU_TOGGLE;
 			if (item.isFlagSet(FXMenuItem::Checked)) p->flags |= FL_MENU_VALUE;
 			if (item.isFlagSet(FXMenuItem::Disabled)) p->flags |= FL_MENU_INACTIVE;
 			if (item.isFlagSet(FXMenuItem::SeparatorAfter)) p->flags |= FL_MENU_DIVIDER;
			idx++;
			}

        const Fl_Menu_Item *m = menu->popup(pt.x, pt.y, 0, 0, 0);
        if (m != NULL)
			{
			sw_uint32_t	id=0;

			for (int i=0;i<nitems;i++)
				{
				FXMenuItem		&item=m_menuItems[i];
				
				if (item.getFltkMenuItem() == m)
					{
					id = item.id();
					break;
					}
				}

			if (id != 0)
				{
				fx_widget_handleCommand(m_pOwner, id);
				}
			
			m->do_callback(0, m->user_data());
			}

		delete [] menu;
		*/
		FXMenuItem	*pSelectedItem;

		pSelectedItem = m_topLevelMenu.pulldown(pt.x, pt.y, 0, 0, m_topLevelMenu.getItemAt(0), m_pOwner, NULL, 0);

		if (pSelectedItem != NULL)
			{
			void	*p=pSelectedItem;

			if (pSelectedItem->user_data() != NULL) p = pSelectedItem->user_data();

			if (pSelectedItem->callback())
				{
				pSelectedItem->do_callback((Fl_Widget*)this, p);
				}
			else if (m_pOwner != NULL)
				{
				FXWnd	*pWnd=dynamic_cast<FXWnd *>(m_pOwner);
				FXGroup	*pGroup=dynamic_cast<FXGroup *>(m_pOwner);

				if (pWnd != NULL)
					{
					pWnd->onCommand(pSelectedItem->id());
					}
				else if (pGroup != NULL)
					{
					pGroup->onCommand(pSelectedItem->id());
					}
				else if (m_pOwner->callback())
					{
					m_pOwner->do_callback((Fl_Widget*)this, p);
					}
				}
			}
		}
