#include <FltkExt/FXListBox.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXTestWidget.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>


#define FLAG_CTRL			1
#define FLAG_SHIFT			2
#define FLAG_DOUBLECLICK	4

class FLTKEXT_DLL_EXPORT FXListBoxTable : public Fl_Table
		{
public:
		class Row
			{
		public:
			SWStringArray	text;
			FXColor			fgColor;
			FXColor			bgColor;
			bool			selected;
			};

		class Column
			{
		public:
			SWString	text;
			int			width;
			int			align;
			};

public:
		FXListBoxTable(int X, int Y, int W, int H, int mode);
		~FXListBoxTable();

		void			removeAllRows();
		void			removeAllColumns();

		int				addRow(const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		int				insertRow(int row, const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		int				updateRow(int row, const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		void			updateRow(int row, const SWStringArray &values);
		void			removeRow(int pos, bool autoSizeColumns=false);

		void			addColumn(const SWString &title, int align=FX_HALIGN_LEFT|FX_VALIGN_CENTER, int width=0, bool autoSizeColumns=false);
		void			setColumnTitle(int col, const SWString &contents, bool autoSizeColumns=false);
		SWString		getColumnTitle(int col) const;
		void			resizeColumns();

		int				getColumnCount() const	{ return (int)m_columns.size(); }
		int				getRowCount() const		{ return (int)m_rows.size(); }

		void			clickRow(int row, int flags);
		void			setSelectMode(int mode);

		int				getFirstSelectedRow();
		int				getLastSelectedRow();
		int				getSelectedRows(SWIntegerArray &ia);
		void			selectRow(int row, bool selected=true);
		bool			isRowSelected(int row);
		void			unselectAllRows();

		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);

/*		
		int				addRow(bool batchmode=false);
		void			updateRow(int row, const SWStringArray &values);
		void			setRowValues(int row, const SWStringArray &values);
		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);
		int				insertRow(int pos, bool batchmode=false);
		int				insertRow(int pos, const SWStringArray &values, bool batchmode=false);
		void			deleteRow(int pos, bool batchmode=false);
		void			endBatchMode();

		void			setCell(int row, int col, const SWString &contents, const FXColor &fgcolor=FX_COLOR_DEFAULT, const FXColor &bgcolor=FX_COLOR_DEFAULT);
		void			setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor);
		void			setCellString(int row, int col, const SWString &contents);
		SWString		getCellString(int row, int col) const;

		void			invertRowColorsOnSelection(bool v)	{ m_invertRowColorsOnSelection = v; }
		void			invertCellColorsOnSelection(bool v)	{ m_invertCellColorsOnSelection = v; }

		bool			isCellSelected(int row, int col);
		void			setSelectedRow(int row);
		void			setSelectedColumn(int col);
		int				getSelectedColumn();
		void			setSelection(int firstCol, int firstRow, int lastCol, int lastRow);
		void			getSelection(int &firstCol, int &firstRow, int &lastCol, int &lastRow);
		void			clearSelection();

		void			setTopRow(int row);

		void			setSelectionMode(int v)				{ m_selectionMode = v; }

		void			getLastClickedCell(int &row, int &col)	{ col = m_lastClickedColumn; row = m_lastClickedRow; }
		void			setLastClickedCell(int row, int col)	{ m_lastClickedColumn = col; m_lastClickedRow = row; }
*/

protected:
		virtual void draw_cell(TableContext context, int R=0,int C=0, int X=0, int Y=0, int W=0, int H=0);
		
		static void	handleCallback(Fl_Widget *pWidget, void *pData);
		void		handleCallback();

protected:
		SWThreadMutex		m_mutex;
		SWArray<Column *>	m_columns;
		SWArray<Row *>		m_rows;
		FXFont				m_font;
		int					m_rowHeight;
		FXPadding			m_cellPadding;
		FXColor				m_selectedFgColor;
		FXColor				m_selectedBgColor;
		int					m_mode;
		int					m_lastRowClicked;
		};


FXListBoxTable::FXListBoxTable(int X, int Y, int W, int H, int mode) :
	Fl_Table(X, Y, W, H),
	m_mutex(true),
	m_cellPadding(2, 2, 2, 2),
	m_mode(mode),
	m_lastRowClicked(-1)
		{
		m_font = FXFont::getDefaultFont(FXFont::General);
		
		FXSize	sz;

		fx_select(m_font);
		sz = fx_getTextExtent("Ay");

		m_rowHeight = sz.cy + m_cellPadding.top + m_cellPadding.bottom;

		col_header(0);
		row_header(0);

		callback(handleCallback, this);
		when(FL_WHEN_CHANGED|FL_WHEN_RELEASE);

		m_selectedFgColor = "#000";
		m_selectedBgColor = "#ddf";
		}


FXListBoxTable::~FXListBoxTable()
		{
		removeAllColumns();
		}


void
FXListBoxTable::removeAllRows()
		{
		SWMutexGuard	guard(m_mutex);

		for (int i=0;i<(int)m_rows.size();i++)
			{
			safe_delete(m_rows[i]);
			}

		m_rows.clear();
		rows(0);
		m_lastRowClicked = -1;
		}


void
FXListBoxTable::removeAllColumns()
		{
		SWMutexGuard	guard(m_mutex);

		removeAllRows();

		for (int i=0;i<(int)m_columns.size();i++)
			{
			safe_delete(m_columns[i]);
			}

		m_columns.clear();
		cols(0);
		}



void
FXListBoxTable::addColumn(const SWString &title, int align, int width, bool autoSizeColumns)
		{
		SWMutexGuard	guard(m_mutex);
		Column			*pColumn=new Column();

		pColumn->text = title;
		pColumn->align = align;
		pColumn->width = width;
		m_columns.add(pColumn);

		for (int i=0;i<(int)m_rows.size();i++)
			{
			m_rows[i]->text.add("");
			}

		bool	havetitle=false;

		for (int col=0;col<(int)m_columns.size();col++)
			{
			if (!m_columns[col]->text.isEmpty())
				{
				havetitle = true;
				break;
				}
			}

		cols((int)m_columns.size());
		col_header(havetitle?1:0);

		if (autoSizeColumns) resizeColumns();
		}


int
FXListBoxTable::addRow(const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		SWMutexGuard	guard(m_mutex);

		while (m_columns.size() < values.size())
			{
			addColumn("", FX_HALIGN_LEFT|FX_VALIGN_CENTER);
			}

		Row		*pRow=new Row();

		pRow->text = values;
		pRow->fgColor = fgColor;
		pRow->bgColor = bgColor;
		pRow->selected = false;

		int		rowidx=(int)m_rows.size();
		
		m_rows.add(pRow);
		rows(rowidx + 1);
		row_height(rowidx, m_rowHeight);

		if (autoSizeColumns) resizeColumns();

		return rowidx;
		}


int
FXListBoxTable::insertRow(int row, const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		SWMutexGuard	guard(m_mutex);

		while (m_columns.size() < values.size())
			{
			addColumn("", FX_HALIGN_LEFT|FX_VALIGN_CENTER);
			}

		int		nrows=(int)m_rows.size();

		while (row > nrows)
			{
			SWStringArray	sa;

			addRow(sa);
			nrows = (int)m_rows.size();
			}

		Row		*pRow=new Row();

		pRow->text = values;
		pRow->fgColor = fgColor;
		pRow->bgColor = bgColor;
		pRow->selected = false;


		m_rows.insert(pRow, row);
		rows(nrows + 1);
		row_height(nrows, m_rowHeight);

		if (autoSizeColumns) resizeColumns();

		return row;
		}


int
FXListBoxTable::updateRow(int row, const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		SWMutexGuard	guard(m_mutex);
		int				nrows=(int)m_rows.size();
		int				res=0;

		if (row >= 0 && row < nrows)
			{
			m_rows[row]->text = values;
			}

		if (autoSizeColumns) resizeColumns();

		return res;
		}


void
FXListBoxTable::removeRow(int row, bool autoSizeColumns)
		{
		SWMutexGuard	guard(m_mutex);
		int				nrows=(int)m_rows.size();
		int				res=0;

		if (row >= 0 && row < nrows)
			{
			safe_delete(m_rows[row]);
			m_rows.remove(row);
			rows(nrows-1);
			}

		if (autoSizeColumns) resizeColumns();
		}


void
FXListBoxTable::resizeColumns()
		{
		SWMutexGuard	guard(m_mutex);
		int				nrows=(int)m_rows.size();
		int				ncols=(int)m_columns.size();

		if (nrows > 0 && ncols > 0)
			{
			SWIntegerArray	widths;
			FXSize			sz;

			fx_select(m_font);

			for (int col=0;col<ncols;col++)
				{
				if (m_columns[col]->width == 0)
					{
					sz = fx_getTextExtent(m_columns[col]->text);
					widths[col] = sw_math_max(widths[col], sz.cx + 4);
					}
				else
					{
					widths[col] = m_columns[col]->width;
					}
				}

			for (int row=0;row<nrows;row++)
				{
				for (int col=0;col<ncols;col++)
					{
					if (m_columns[col]->width == 0)
						{
						sz = fx_getTextExtent(m_rows[row]->text[col]);
						widths[col] = sw_math_max(widths[col], sz.cx + 4);
						}
					}
				}

			int		totalwidth=0;
			int		adjustablecols=0;

			for (int col=0;col<ncols;col++)
				{
				totalwidth += widths[col];
				if (m_columns[col]->width == 0) adjustablecols++;
				}

			if (adjustablecols > 0)
				{
				int		space=w()-totalwidth;

				if (vscrollbar != NULL) space -= vscrollbar->w()+2;

				int		adj=space / adjustablecols;

				if (adj == 0) adj = 1;

				for (int col=0;space>0&&col<ncols;col++)
					{
					if (m_columns[col]->width == 0)
						{
						widths[col] += adj;
						space -= adj;
						}
					}
				}

			for (int col=0;col<(int)m_columns.size();col++)
				{
				col_width(col, widths[col]);
				}
			}
		}


void
FXListBoxTable::draw_cell(TableContext context, int R, int C, int X, int Y, int W, int H)
		{
		switch (context)
			{
			case CONTEXT_STARTPAGE:
				m_font.select();
				break;

    
			case CONTEXT_ROW_HEADER:            // Fl_Table telling us to draw row/col headers
				break;

			case CONTEXT_COL_HEADER:
				fl_push_clip(X, Y, W, H);
				fl_draw_box(FL_THIN_UP_BOX, X, Y, W, H, color());
				fl_color(FL_BLACK);
				fx_draw_text(m_columns[C]->text, X+m_cellPadding.left, Y+m_cellPadding.top, W-m_cellPadding.left-m_cellPadding.right, H-m_cellPadding.top-m_cellPadding.bottom, m_columns[C]->align);
				fl_pop_clip();
				break;
        
			case CONTEXT_CELL:                  // Fl_Table telling us to draw cells
				fl_push_clip(X, Y, W, H);

				// BG COLOR
				fl_color(m_rows[R]->selected?m_selectedBgColor.to_fl_color():m_rows[R]->bgColor.to_fl_color());
				fl_rectf(X, Y, W, H);
           
				// TEXT
				fl_color(m_rows[R]->selected?m_selectedFgColor.to_fl_color():m_rows[R]->fgColor.to_fl_color());
				fx_draw_text(m_rows[R]->text[C], X+m_cellPadding.left, Y+m_cellPadding.top, W-m_cellPadding.left-m_cellPadding.right, H-m_cellPadding.top-m_cellPadding.bottom, m_columns[C]->align);
           
				// BORDER
				fl_color(FL_LIGHT2);
				fl_rect(X, Y, W, H);
				fl_pop_clip();
				break;
			}
		}

void
FXListBoxTable::handleCallback(Fl_Widget *pWidget, void *pData)
		{
		((FXListBoxTable *)pWidget)->handleCallback();
		}


int
FXListBoxTable::getFirstSelectedRow()
		{
		int		nrows=(int)m_rows.size();
		int		res=-1;

		for (int i=0;i<nrows;i++)
			{
			Row	*pRow=m_rows[i];

			if (pRow->selected)
				{
				res = i;
				break;
				}
			}
		
		return res;
		}


int
FXListBoxTable::getLastSelectedRow()
		{
		int		nrows=(int)m_rows.size();
		int		res=-1;

		for (int i=0;i<nrows;i++)
			{
			Row	*pRow=m_rows[i];

			if (pRow->selected)
				{
				res = i;
				}
			}
		
		return res;
		}

int
FXListBoxTable::getSelectedRows(SWIntegerArray &ia)
		{
		int		nrows=(int)m_rows.size();

		ia.clear();
		for (int i=0;i<nrows;i++)
			{
			Row	*pRow=m_rows[i];

			if (pRow->selected)
				{
				ia.add(i);
				}
			}
		
		return (int)ia.size();
		}


void
FXListBoxTable::selectRow(int row, bool selected)
		{
		int		nrows=(int)m_rows.size();

		if (row >= 0 && row < nrows)
			{
			Row	*pRow=m_rows[row];

			switch (m_mode)
				{
				case FXListBox::ModeSingleSelect:
					if (selected && !pRow->selected)
						{
						for (int i=0;i<nrows;i++)
							{
							m_rows[i]->selected = false;
							}
						pRow->selected = true;
						}
					else
						{
						pRow->selected = selected;
						}
					redraw();
					break;

				case FXListBox::ModeMultiSelect:
					pRow->selected = selected;
					redraw();
					break;
				}
			}
		}


void
FXListBoxTable::unselectAllRows()
		{
		int		nrows=(int)m_rows.size();
		bool	changed=false;

		for (int row=0;row<nrows;row++)
			{
			Row	*pRow=m_rows[row];

			if (pRow->selected)
				{
				pRow->selected = false;
				changed = true;
				}
			}

		if (changed) redraw();
		}

bool
FXListBoxTable::isRowSelected(int row)
		{
		bool	res=false;

		if (row >= 0 && row < (int)m_rows.size())
			{
			res = m_rows[row]->selected;
			}

		return res;
		}


void
FXListBoxTable::setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		int		nrows=(int)m_rows.size();

		if (row >= 0 && row < nrows)
			{
			Row	*pRow=m_rows[row];

			pRow->bgColor = bgcolor;
			pRow->fgColor = fgcolor;
			}
		}


void
FXListBoxTable::clickRow(int row, int flags)
		{
		int		nrows=(int)m_rows.size();

		if (row >= 0 && row < nrows)
			{
			Row	*pRow=m_rows[row];
			int	notifycode=FXListBox::NotifySelectionChanged;

			switch (m_mode)
				{
				case FXListBox::ModeNoSelect:
					// Do nothing
					break;

				case FXListBox::ModeSingleSelect:
					if (!pRow->selected)
						{
						for (int i=0;i<nrows;i++)
							{
							m_rows[i]->selected = false;
							}
						pRow->selected = true;
						redraw();
						}
					else if ((flags & FLAG_DOUBLECLICK) != 0)
						{
						notifycode = FXListBox::NotifyDoubleClick;
						}
					break;

				case FXListBox::ModeMultiSelect:
					if (flags == 0)
						{
						for (int i=0;i<nrows;i++)
							{
							m_rows[i]->selected = false;
							}
						pRow->selected = true;
						redraw();
						}
					else if ((flags & FLAG_CTRL) != 0)
						{
						pRow->selected = !pRow->selected;
						redraw();
						}
					else if ((flags & FLAG_SHIFT) != 0)
						{
						int	f, t;

						f = m_lastRowClicked;
						if (f < 0) f = 0;
						t = row;

						if (f > t)
							{
							f = row;
							t = m_lastRowClicked;
							}

						bool	v=true;

						for (int i=f;i<=t;i++)
							{
							m_rows[i]->selected = v;
							}
						redraw();
						}
					break;
				}
			
			m_lastRowClicked = row;
			parent()->do_callback(parent(), notifycode);
			}
		}


void
FXListBoxTable::handleCallback()
		{
		int				R=callback_row();
		int				C=callback_col();
		int				E=(int)Fl::event();
		int				B=Fl::event_button();
		int				N=Fl::event_clicks();
		TableContext	context = callback_context();      // which part of table

		// SW_TRACE("callback: Row=%d Col=%d Context=%d Event=%d, B=%d, N=%d\n", R, C, (int)context, E, B, N);

		if (B == FL_LEFT_MOUSE && E == FX_BUTTON_DOWN)
			{
			if ((context & CONTEXT_CELL) != 0)
				{
				int		flags=0;

				if (Fl::event_ctrl()) flags |= FLAG_CTRL;
				if (Fl::event_shift()) flags |= FLAG_SHIFT;
				if (Fl::event_clicks()) flags |= FLAG_DOUBLECLICK;

				clickRow(R, flags);
				}
			else
				{
				int		nrows=(int)m_rows.size();

				for (int i=0;i<nrows;i++)
					{
					m_rows[i]->selected = false;
					}

				redraw();

				m_lastRowClicked = -1;
				parent()->do_callback(parent(), FXListBox::NotifySelectionChanged);
				}
			}
		}


void
FXListBoxTable::setColumnTitle(int col, const SWString &contents, bool autoSizeColumns)
		{
		if (col >= 0 && col < (int)m_columns.size())
			{
			m_columns[col]->text = contents;
			if (autoSizeColumns) resizeColumns();
			}
		}


SWString
FXListBoxTable::getColumnTitle(int col) const
		{
		SWString	res;

		if (col >= 0 && col < (int)m_columns.size())
			{
			res = m_columns.get(col)->text;
			}
		
		return res;
		}


void
FXListBoxTable::setSelectMode(int mode)
		{
		m_mode = mode;
		for (int i=0;i<(int)m_rows.size();i++)
			{
			m_rows[i]->selected = false;
			}

		redraw();
		}


FXListBox::FXListBox(int X, int Y, int W, int H, int mode) :
	FXGroup(X, Y, W, H)
		{
		setBackgroundColor("#fed");
		setBorder(0, "#f00", 1, 0);
		setLayout(new FXPanelLayout(true));
		add(m_pTable = new FXListBoxTable(X, Y, W, H, mode));
		}

void
FXListBox::removeAllRows()
		{
		if (m_pTable != NULL) m_pTable->removeAllRows();
		}


void
FXListBox::removeAllColumns()
		{
		if (m_pTable != NULL) m_pTable->removeAllColumns();
		}


int
FXListBox::addRow(const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		int	res=0;

		if (m_pTable != NULL) res = m_pTable->addRow(values, fgColor, bgColor, autoSizeColumns);

		return res;
		}


int
FXListBox::insertRow(int row, const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		int	res=0;

		if (m_pTable != NULL) res = m_pTable->insertRow(row, values, fgColor, bgColor, autoSizeColumns);

		return res;
		}


int
FXListBox::updateRow(int row, const SWStringArray &values, const FXColor &fgColor, const FXColor &bgColor, bool autoSizeColumns)
		{
		int	res=0;

		if (m_pTable != NULL) res = m_pTable->updateRow(row, values, fgColor, bgColor, autoSizeColumns);

		return res;
		}


void
FXListBox::removeRow(int row, bool autoSizeColumns)
		{
		if (m_pTable != NULL) m_pTable->removeRow(row, autoSizeColumns);
		}


void
FXListBox::addColumn(const SWString &title, int align, int width, bool autoSizeColumns)
		{
		if (m_pTable != NULL)
			{
			m_pTable->addColumn(title, align, width);
			if (autoSizeColumns) resizeColumns();
			}
		}


void
FXListBox::setColumnTitle(int col, const SWString &contents, bool autoSizeColumns)
		{
		if (m_pTable != NULL)
			{
			m_pTable->setColumnTitle(col, contents);
			if (autoSizeColumns) resizeColumns();
			}
		}


SWString
FXListBox::getColumnTitle(int col) const
		{
		SWString	res;

		if (m_pTable != NULL) res = m_pTable->getColumnTitle(col);

		return res;
		}


void
FXListBox::resizeColumns()
		{
		if (m_pTable != NULL) m_pTable->resizeColumns();
		}

int
FXListBox::getColumnCount() const
		{
		int	res=0;

		if (m_pTable != NULL) res = m_pTable->getColumnCount();

		return res;
		}


int
FXListBox::getRowCount() const
		{
		int	res=0;

		if (m_pTable != NULL) res = m_pTable->getRowCount();

		return res;
		}



void
FXListBox::setSelectMode(int mode)
		{
		if (m_pTable != NULL) m_pTable->setSelectMode(mode);
		}



int
FXListBox::getSelectedRow()
		{
		int	res=-1;

		if (m_pTable != NULL) res = m_pTable->getFirstSelectedRow();

		return res;
		}


int
FXListBox::getFirstSelectedRow()
		{
		int	res=-1;

		if (m_pTable != NULL) res = m_pTable->getFirstSelectedRow();

		return res;
		}


int
FXListBox::getLastSelectedRow()
		{
		int	res=-1;

		if (m_pTable != NULL) res = m_pTable->getLastSelectedRow();

		return res;
		}

int
FXListBox::getSelectedRows(SWIntegerArray &ia)
		{
		int	res=0;

		ia.clear();
		if (m_pTable != NULL) res = m_pTable->getSelectedRows(ia);

		return res;
		}


void
FXListBox::setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		if (m_pTable != NULL) m_pTable->setRowColors(row, fgcolor, bgcolor);
		}

void
FXListBox::selectRow(int row, bool selected)
		{
		if (m_pTable != NULL) m_pTable->selectRow(row, selected);
		}

void
FXListBox::unselectAllRows()
		{
		if (m_pTable != NULL) m_pTable->unselectAllRows();
		}

bool
FXListBox::isRowSelected(int row)
		{
		bool	res=false;

		if (m_pTable != NULL) res = m_pTable->isRowSelected(row);

		return res;
		}


