#include "FXStaticText.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

FXStaticText::FXStaticText(const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(0, 0, 100, 20)
		{
		init(text, font, align, fgcolor, bgcolor);

		FXSize	sz=fx_getTextExtent(text);

		size(sz.cx, sz.cy);
		}


FXStaticText::FXStaticText(int X, int Y, int W, int H, const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H)
		{
		init(text, font, align, fgcolor, bgcolor);
		}


FXStaticText::FXStaticText(int W, int H, const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(W, H)
		{
		init(text, font, align, fgcolor, bgcolor);
		}




FXStaticText::FXStaticText(int W, int H, const SWString &text, int align, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(W, H)
		{
		init(text, FXFont::getDefaultFont(FXFont::StaticText), align, fgcolor, bgcolor);
		}


FXStaticText::FXStaticText(int X, int Y, int W, int H, const SWString &text, int align, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H)
		{
		init(text, FXFont::getDefaultFont(FXFont::StaticText), align, fgcolor, bgcolor);
		}


void
FXStaticText::init(const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		set_output();
		m_text = text;
		m_style.flags = FX_ALIGN_MIDDLE_LEFT;
		m_style.font = font;
		m_style.fgColor = fgcolor;
		m_style.bgColor = bgcolor;
		m_style.outlineColor = FX_COLOR_NONE;
		m_style.outlineWidth = 0;
		m_style.shadowColor = FX_COLOR_NONE;
		m_style.shadowXOffset = 0;
		m_style.shadowYOffset = 0;

		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		}


FXStaticText::~FXStaticText()
		{
		}


void
FXStaticText::setTextColor(Fl_Color v)
		{
		setForegroundColor(v);
		}


void
FXStaticText::setTextColor(const FXColor &v)
		{
		setForegroundColor(v);
		}


void
FXStaticText::setText(const SWString &v)
		{
		m_text = v;
		}


void
FXStaticText::setText(const SWString &v, Fl_Color c)
		{
		m_text = v;
		setForegroundColor(c);
		}


void
FXStaticText::setText(const SWString &v, const FXColor &c)
		{
		m_text = v;
		setForegroundColor(c);
		}


#ifdef FX_OUTLINE
void
FXStaticText::setTextOutline(int width, const FXColor &color)
		{
		m_style.outlineColor = color;
		m_style.outlineWidth = width;
		}
#endif // FX_OUTLINE


void
FXStaticText::setTextShadow(int xoff, int yoff, const FXColor &color)
		{
		m_style.shadowXOffset = xoff;
		m_style.shadowYOffset = yoff;
		m_style.shadowColor = color;
		}

void
FXStaticText::setTextStyle(const FXTextStyle &v)
		{
		m_style = v;
		setForegroundColor(v.fgColor);
		setBackgroundColor(v.bgColor);
		}


void
FXStaticText::onDraw()
		{
		FXRect	cr;

		getClientRect(cr);
	
		cr.deflate(m_padding.left, m_padding.top, m_padding.right, m_padding.bottom);

		FXTextStyle	ts(m_style);

		ts.bgColor = getBackgroundColor();
		ts.fgColor = getForegroundColor();

		if (ts.bgColor != FX_COLOR_NONE) ts.flags |= FX_BG_OPAQUE;

		if (!active())
			{
			ts.fgColor = ts.fgColor.toGrayscale().toOpposite().toShade(0.5);
			if (ts.outlineColor != FX_COLOR_NONE) ts.outlineColor = ts.outlineColor.toGrayscale().toOpposite().toShade(0.5);
			if (ts.shadowColor != FX_COLOR_NONE) ts.shadowColor = ts.shadowColor.toGrayscale().toOpposite().toShade(0.5);
			}

		fx_draw_text(m_text, cr, ts);
		/*
		FXColor	color=getForegroundColor();

		if (!active())
			{
			color = color.toGrayscale().toOpposite().toShade(0.5);
			}
		
		fx_draw_text(m_text, cr, m_align, m_font, color);
		*/
		}


void
FXStaticText::calcSizes(int width)
		{
		m_minSize = fx_getTextExtent(m_text, width, m_style);
		m_minSize.cx += m_padding.left + m_padding.right;
		m_minSize.cy += m_padding.top + m_padding.bottom;
		}
