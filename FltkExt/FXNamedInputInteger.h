/*
**	FXNamedInputInteger.h	A generic dialog class
*/

#ifndef __FltkExt_FXNamedInputInteger_h__
#define __FltkExt_FXNamedInputInteger_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXNamedWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXNamedInputInteger : public FXNamedWidget
		{
public:
		FXNamedInputInteger(const SWString &name, int nameW, int value, int valueW, int H);
		FXNamedInputInteger(int X, int Y, const SWString &name, int nameW, int value, int valueW, int H);

protected:
		void		init(const SWString &name, int value);
		};


#endif // __FltkExt_FXNamedInputInteger_h__
