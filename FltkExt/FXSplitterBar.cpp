/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the implementation of the FXSplitterBar class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "FXSplitterBar.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>


FXSplitterBar::FXSplitterBar() :
	vertical(false),
	width(2),
	padding(2)
		{
		bgColor = FX_COLOR_NONE;
		fgColor = FXColor(200, 200, 200);
		}


FXSplitterBar::FXSplitterBar(bool vflag, int w, int p, const FXColor &fg, const FXColor &bg) :
	vertical(vflag),
	width(w),
	padding(p)
		{
		bgColor = bg;
		fgColor = fg;
		}


FXSplitterBar::FXSplitterBar(const FXSplitterBar &other)	
		{
		*this = other;
		}


FXSplitterBar &
FXSplitterBar::operator=(const FXSplitterBar &other)	
		{
#define COPY(x)	x = other.x
		COPY(vertical);
		COPY(width);
		COPY(padding);
		COPY(rect);
		COPY(fgColor);
		COPY(bgColor);
#undef COPY

		return *this;
		}


void
FXSplitterBar::draw()
		{
		if (bgColor != FX_COLOR_NONE)
			{
			fx_draw_fillRect(rect, bgColor);
			}

		if (fgColor != FX_COLOR_NONE)
			{
			FXRect	dr(rect);

			if (vertical)
				{
				dr.top += padding;
				dr.bottom -= padding;
				}
			else
				{
				dr.left += padding;
				dr.right -= padding;
				}

			if (dr.Width() > 0 && dr.Height() > 0)
				{
				fx_draw_fillRect(dr, fgColor);
				}
			}
		}