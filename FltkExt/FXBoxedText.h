#ifndef __FltkExt_BoxedText_h__
#define __FltkExt_BoxedText_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>

#include <FL/Fl_Box.H>


class FLTKEXT_DLL_EXPORT FXBoxedText : public Fl_Box
		{
public:
		FXBoxedText(int X, int Y, int W, int H, const SWString &text, Fl_Align textalign=FL_ALIGN_LEFT, Fl_Color fgcolor=FL_BLACK, Fl_Color bgcolor=FL_DARK2, int textheight=0);
		virtual ~FXBoxedText();

		void			text(const SWString &v)	{ m_text = v; label(m_text); }
		const SWString &	text() const			{ return m_text; }

protected:
		SWString		m_text;
		};

#endif // __FltkExt_BoxedText_h__
