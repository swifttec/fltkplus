/*
**	FXNamedInputString.h	A generic dialog class
*/

#ifndef __FltkExt_FXNamedInputString_h__
#define __FltkExt_FXNamedInputString_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXNamedWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXNamedInputString : public FXNamedWidget
		{
public:
		FXNamedInputString(const SWString &name, int nameW, const SWString &value, int valueW, int H);
		FXNamedInputString(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H);

protected:
		void		init(const SWString &name, const SWString &value);
		};


#endif // __FltkExt_FXNamedInputString_h__
