/*
**	FXDataExchange.h	A generic dialog class
*/

#ifndef __FltkExt_FXDataExchange_h__
#define __FltkExt_FXDataExchange_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>

#include <swift/SWString.h>

class FLTKEXT_DLL_EXPORT FXDataExchange
		{
public:
		enum Action
			{
			Load,
			Save
			};

public:
		FXDataExchange(Fl_Group *pGroup, int action);
		virtual ~FXDataExchange();

		bool		isLoading() const		{ return m_action == Load; }
		bool		isSaving() const		{ return m_action == Save; }
		bool		saveAndValidate() const	{ return m_action == Save; }

		/// Get/Set a string value
		bool		value(int itemid, SWString &value, int limitsize=0);

		/// Get/Set an integer value
		bool		value(int itemid, int &value, int limitsize=0);

		/// Get/Set an double value
		bool		value(int itemid, double &value, int limitsize=0);

		/// Get/Set a bool value
		bool		value(int itemid, bool &value, int limitsize=0);

		/// Get/Set a bool value in a radio button group
		bool		radioGroupValue(int itemid, int &value);

protected:
		Fl_Group	*m_pGroup;
		int			m_action;
		};


#define	DDX_Control(pDX, id, p)	
#define	DDX_CBIndex(pDX, id, p)			pDX->value(id, p)
#define	DDX_DropdownList(pDX, id, p)	pDX->value(id, p)
#define	DDX_Radio(pDX, id, p)			pDX->radioGroupValue(id, p)
#define	DDX_Text(pDX, id, p)			pDX->value(id, p)
#define	DDX_Check(pDX, id, p)			pDX->value(id, p)

#endif // __FltkExt_FXDataExchange_h__
