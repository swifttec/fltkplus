/*
**	FXStaticText.h	A generic dialog class
*/

#ifndef __FltkExt_FXRect_h__
#define __FltkExt_FXRect_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXSize.h>
#include <FltkExt/FXPoint.h>
#include <FltkExt/FXSize.h>


class FLTKEXT_DLL_EXPORT FXRect
		{
public:
		FXRect();
		FXRect(int vleft, int vtop, int vright, int vbottom);
		FXRect(const FXPoint &pt, const FXSize &sz);

		int		width() const;
		int		height() const;
		
		bool	contains(int x, int y) const;
		bool	contains(const FXPoint &pt) const;
		bool	PtInRect(const FXPoint &pt) const;
		bool	overlaps(const FXRect &pt) const;

		FXSize	size() const;
		FXPoint	point() const;
		FXPoint	center() const;

		void	deflate(int l, int t, int r, int b);
		void	inflate(int l, int t, int r, int b);
		void	moveBy(int xoff, int yoff);
		void	moveTo(int xpos, int ypos);

		void	setXYWH(int x, int y, int w, int h);
		void	getXYWH(int &x, int &y, int &w, int &h);

public: //MFC Compatability methods
		int		Width() const								{ return width(); }
		int		Height() const								{ return height(); }
		void	DeflateRect(int l, int t, int r, int b)		{ deflate(l, t, r, b); }
		void	InflateRect(int l, int t, int r, int b)		{ inflate(l, t, r, b); }


public:
		int		left;
		int		top;
		int		right;
		int		bottom;
		};

#endif // __FltkExt_FXRect_h__