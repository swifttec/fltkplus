#ifndef __FltkExt_FXButton_h__
#define __FltkExt_FXButton_h__

#include <FltkExt/FXWidget.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/FXButtonStyle.h>

#include <FL/Fl_Group.H>
#include <FL/Fl_Shared_Image.H>



/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXButton : public FXWidget
		{
public:
		enum State
			{
			Normal=0,
			Focused,
			Hover,
			Pressed,
			Disabled,

			SelectedNormal,
			SelectedFocused,
			SelectedHover,
			SelectedPressed,
			SelectedDisabled,

			// Last one
			MaxStyle
			};

public:
		FXButton(int X, int Y, int W, int H);
		FXButton(const SWString &text, const SWString &image);
		FXButton(const SWString &text, const SWString &image, int X, int Y, int W, int H);
		FXButton(sw_uint32_t id, const SWString &text, const SWString &image, int X, int Y, int W, int H);
		FXButton(const SWString &text, const SWString &image, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor=FXColor(100, 100, 100), const FXColor &frcolor=FXColor(0, 0, 0));
		FXButton(sw_uint32_t id, const SWString &text, const SWString &image, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor=FXColor(100, 100, 100), const FXColor &frcolor=FXColor(0, 0, 0));
		virtual ~FXButton();
		
		
		/// Set the background / text color
		void					setBackgroundColor(const FXColor &v);

		/// Set the border style and color
		void					setBorder(int style, const FXColor &color, int width, int corner);
		
		// Set the button style for the given state
		void					setButtonStyle(int state, const FXButtonStyle &style, bool redrawflag=false);

		// Set the button style for the given state
		void					getButtonStyle(int state, FXButtonStyle &) const;
		
		// Set the button style for the given state
		void					setButtonBackgroundColor(int state, const FXColor &color, bool redrawflag=false);

		// Get the button state
		int						state() const;

		// Set the internal padding
		void					padding(int v);
		void					padding(const FXPadding &v);

		void					select(bool v=true);

		bool					selected() const				{ return m_selected; }

		void					setFont(const FXFont &font);

		void					setText(const SWString &v);
		const SWString &		getText() const					{ return m_text; }

		void					enable(bool v, bool redrawflag=false);
		void					enabled(bool v)					{ enable(v, false); }
		bool					enabled() const					{ return (active() && m_state != Disabled && m_state != SelectedDisabled); }

		void					disabled(bool v)				{ enabled(!v); }
		bool					disabled() const				{ return !enabled(); }

		bool					isEnabled() const				{ return enabled(); }

		void					setButtonImage(const SWString &filename, bool redrawflag=false);
		void					setButtonImages(const SWString &folder, const SWString &filename, bool redrawflag=false);

		void					setAllStyles(const FXButtonStyle &style, bool autovariance=true);
		void					setAllSelectedStyles(const FXButtonStyle &style, bool autovariance=true);
		void					setAllUnselectedStyles(const FXButtonStyle &style, bool autovariance=true);

		/// Add a shortkey this button responds to
		void					addShortcutKey(int v);

		void					enableKeys(bool v)	{ m_handleKeys = v; }

		/// Allow us to call handle from public code - for internal use only
		void					handleEvent(int eventcode)	{ handle(eventcode); }

protected:
		virtual void	init();
		
		/// Call the drawing code
		virtual void	draw();

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the mouse moves over the widget
		virtual int		onMouseEnter();

		/// Called when the mouse moves over the widget
		virtual int		onMouseLeave();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the widget gets the focus
		virtual int		onFocus();

		/// Called when the widget loses the focus
		virtual int		onUnfocus();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyUp(int code, const SWString &text, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

protected:
		bool			handleShortcutKey(int v);
		SWString		getButtonImagePath(const SWString &imagefolder, const SWString &imagename);

protected:
		SWString		m_text;
		Fl_Shared_Image	*m_pSharedImage;
		SWString		m_sharedImageFilename;

		FXButtonStyle	m_style[MaxStyle];
		int				m_state;
		int				m_lastDrawnState;
		bool			m_mouseOver;
		bool			m_focused;
		bool			m_selected;
		bool			m_styleChanged;
		bool			m_imageChanged;
		bool			m_hasText;
		SWIntegerArray	m_shortcutKeys;
		int				m_lastKeyDown;
		bool			m_handleKeys;
		};

#endif // __FltkExt_FXButton_h__
