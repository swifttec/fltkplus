#ifndef __FltkExt_FXWidget_h__
#define __FltkExt_FXWidget_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/XDC.h>
#include <FltkExt/FXImage.h>
#include <FltkExt/FXCursor.h>

#include <FL/Fl_Group.H>
#include <FL/Fl_Shared_Image.H>

/*
** A generic FltkExt widget with some useful general extensions to Fl_Widget
*/
class FLTKEXT_DLL_EXPORT FXWidget : public Fl_Widget
		{
public:
		/// Default Constructor
		FXWidget();

		/// Constructor with width only
		FXWidget(int w, int h);

		/// Constructor with width and position
		FXWidget(int x, int y, int w, int h);

		/// Destructor
		virtual ~FXWidget();

public: // FXWidget Methods

		/// Invalidate this widget
		void			invalidate();

		/// Invalidate this widget
		void			Invalidate(int bv=0)					{ invalidate(); }

		/// Mark this widget for redraw
		void			redraw()								{ invalidate(); }

		/// Set the border style and color
		void			setBorder(int style, const FXColor &color, int width, int corner);

		/// Set the border color
		void			setBorderColor(const FXColor &v)		{ m_bdColor = v; }

		/// Get the border color
		const FXColor &	getBorderColor() const					{ return m_bdColor; }

		/// Set the border width
		void			setBorderWidth(int v)					{ m_bdWidth = v; }

		/// Get the border width
		int				getBorderWidth() const					{ return (m_bdColor==FX_COLOR_NONE?0:m_bdWidth); }

		/// Set the corner width
		void			setCornerWidth(int v)					{ m_bdCorner = v; }

		/// Get the corner width
		int				getCornerWidth() const					{ return (m_bdStyle==0?0:m_bdCorner); }

		/// Set the foreground / text color
		void			setForegroundColor(const FXColor &v)	{ m_fgColor = v; }

		/// Get the foreground color
		const FXColor &	getForegroundColor() const				{ return m_fgColor; }

		/// Set the foreground / text color
		void			setBackgroundColor(const FXColor &v)	{ m_bgColor = v; }

		/// Get the background color
		const FXColor &	getBackgroundColor() const				{ return m_bgColor; }

		/// Set the contents padding
		void			setPadding(const FXPadding &v)			{ m_padding = v; }

		/// Get the contents padding
		void			getPadding(FXPadding &v)				{ v = m_padding; }

		/// Test to see if we have the focus
		bool			hasFocus() const;

		/// Test to see if we have the mouse over us
		bool			hasMouseOver() const;

		/// Test to see if we have the mouse over us
		bool			isMouseOver() const						{ return hasMouseOver(); }

		/// Test to see if we are disabled
		bool			disabled() const;

		/// Test to see if we are enabled
		bool			enabled() const;

		/// Set the current widget position
		void			setPosition(int X, int Y)		{ resize(X, Y, w(), h()); }

		/// Set the current widget size
		void			setSize(int W, int H)			{ resize(x(), y(), W, H); }

		/// Set the current widget size
		void			setSize(const FXSize &sz)		{ resize(x(), y(), sz.cx, sz.cy); }

		/// Get the current widget size
		FXSize			getSize() const					{ return FXSize(w(), h()); }

		/// Set the minimum widget size
		void			setMinSize(int W, int H)		{ m_minSize.cx = W; m_minSize.cy = H; }

		/// Set the minimum widget size
		void			setMaxSize(int W, int H)		{ m_maxSize.cx = W; m_maxSize.cy = H; }

		/// Set the minimum widget size
		void			setFixedSize(int W, int H)		{ setMinSize(W, H); setMaxSize(W, H); }

		/// Set the minimum/maximum sizes of this widget, primarily used by layout managers
		void			setMinMaxSizes(const FXSize &minsize, const FXSize &maxsize)	{ m_minSize = minsize;  m_maxSize = maxsize; }

		/// Set the minimum/maximum sizes of this widget, primarily used by layout managers
		void			setMinMaxSizes(int minW, int minH, int maxW, int maxH)	{ m_minSize.cx = minW; m_minSize.cy = minH; m_maxSize.cx = maxW; m_maxSize.cy = maxH; }

		/// Get the minimum size of this widget, primarily used by layout managers
		const FXSize &	getMinSize() const				{ return m_minSize; }

		/// Get the maximum size of this widget, primarily used by layout managers
		const FXSize &	getMaxSize() const				{ return m_maxSize; }

		/// Set the minimum size of this widget, primarily used by layout managers
		void			setMinSize(const FXSize &sz)	{ m_minSize = sz; }

		/// Set the maximum size of this widget, primarily used by layout managers
		void			setMaxSize(const FXSize &sz)	{ m_maxSize = sz; }

		/// Set the maximum size of this widget, primarily used by layout managers
		void			setFixedSize(const FXSize &sz)	{ setMinSize(sz); setMaxSize(sz); }

		/// Set the maximum size of this widget, primarily used by layout managers
		void			setFixedWidth(int v)			{ m_minSize.cx = m_maxSize.cx = v; }

		/// Set the maximum size of this widget, primarily used by layout managers
		void			setFixedHeight(int v)			{ m_minSize.cy = m_maxSize.cy = v; }

		/// Get the widget rect for this widget - the overall space occupied by the widget
		virtual void	getWidgetRect(FXRect &rect) const;

		/// Get the inner rect for this widget - the space just inside the border ignoring rounded corners
		virtual void	getInnerRect(FXRect &rect) const;

		/// Get the client rect for this widget - the space just inside the border allowing for rounded corners
		virtual void	getClientRect(FXRect &rect) const;
		
		/// Get the client rect for this widget - the space just inside the border allowing for rounded corners
		virtual void	GetClientRect(FXRect *pRect) const	{ return getClientRect(*pRect); }

		/// Get the contents rect for this widget - the space for the widget contents including adjustments for padding and rounded corners
		virtual void	getContentsRect(FXRect &rect) const;

		/// Clears the background image
		void			clearBackgroundImage();

		/// Set the background image - can be null
		void			setBackgroundImage(const SWString &filename);

		/// Get the position of the widget
		void			getPosition(int &X, int &Y) const		{ X = x(); Y = y(); }

		/// Set up an interval timer specifically for this widget
		sw_uint32_t		setIntervalTimer(sw_uint32_t localid, double seconds, void *pData);

		/// Set up an interval timer specifically for this widget
		sw_uint32_t		setIntervalTimerMS(sw_uint32_t localid, int ms, void *pData);

		/// Kill an interval timer previously created for this widget
		void			killIntervalTimer(sw_uint32_t id);

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);

		/// Set the cursor for this widget
		virtual void	setCursor(Fl_Cursor v);

		/// Set the cursor for this widget
		virtual void	setCursor(const FXCursor &v);

public:
		/// Called to handle a command
		virtual int		onCommand(sw_uint32_t code);

		/// Called when the widget is closed
		virtual void	onClose();

		/// Called when the widget is created
		virtual void	onCreate();

public: /// FXWidget overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called to draw the widget border
		virtual void	onDrawBorder();

		/// Called to draw the widget background
		virtual void	onDrawBackground();

		/// Called when the widget gets the focus
		virtual int		onFocus();

		/// Called when the widget loses the focus
		virtual int		onUnfocus();

		/// Called when the mouse moves over the widget
		virtual int		onMouseEnter();

		/// Called when the mouse moves over the widget
		virtual int		onMouseLeave();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is moved
		virtual int		onMouseMove(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onMiddleMouseButton(int action, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onRightMouseButton(int action, const FXPoint &pt);

		/// Called when the widget is resized
		virtual void	onSize(const FXPoint &pos, const FXSize &sz);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyUp(int code, const SWString &text, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

		/// Called when the widget is shown
		virtual int		onShow();

		/// Called when the widget is hidden
		virtual int		onHide();

public:
		// Scrolling support
		virtual void		onScroll(int bar, int action, int pos);
		virtual void		onHScroll(int pos);
		virtual void		onVScroll(int pos);
		virtual void		onScrollUp();
		virtual void		onScrollDown();
		virtual void		onScrollLeft();
		virtual void		onScrollRight();
		virtual void		onScrollHome();
		virtual void		onScrollEnd();
		virtual void		onScrollPageUp();
		virtual void		onScrollPageDown();
		virtual void		onScrollPageHome();
		virtual void		onScrollPageEnd();
		virtual void		onScrollPageLeft();
		virtual void		onScrollPageRight();

protected: // FLTK Overrides
		/// Handle the request to draw this widget
		virtual void	draw();

		/// Handle the given event
		virtual int		handle(int event);


public: // FLTK Overrides

		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);

public: // Static methods
		static SWString		eventName(int code);


protected:
		/// Initialise this widget
		void			init();

protected: // FXWidget members
		int				m_flags;			///< Various flags
		FXPadding		m_padding;			///< Internal padding
		FXColor			m_bdColor;			///< Border color
		FXColor			m_bgColor;			///< Background color
		FXColor			m_fgColor;			///< Foreground/Text color
		Fl_Shared_Image	*m_pBgImage;		///< The original background image
		Fl_Image		*m_pBgImageCopy;	///< The resized background image used for drawing
		int				m_bdStyle;
		int				m_bdWidth;
		int				m_bdCorner;
		FXSize			m_minSize;			///< The minimum size of the widget in pixels
		FXSize			m_maxSize;			///< The maximum size of the widget in pixels, if zero then there is no maximum size
		int				m_lastEventCode;	///< The last event code received
		FXPoint			m_lastEventPoint;	///< The last event coordinates
		FXCursor		m_cursor;
		};

#endif // __FltkExt_FXWidget_h__
