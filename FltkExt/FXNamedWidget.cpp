#include "FXNamedWidget.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

#include <FltkExt/FXPackedLayout.h>


static void
itemCallback(Fl_Widget *pWidget)
		{
		FXNamedWidget *pPanel=dynamic_cast<FXNamedWidget *>(pWidget->parent());

		if (pPanel != NULL)
			{
			pPanel->onItemCallback(pWidget, NULL);
			}
		}


FXNamedWidget::FXNamedWidget(int X, int Y, int nameW, int valueW, int H) :
	FXGroup(X, Y, nameW + valueW + 8, H),
	m_nameW(nameW),
	m_valueW(valueW)
		{
		}


void
FXNamedWidget::init(const SWString &name, Fl_Widget *pValueWidget, const SWValue &value)
		{
		setForegroundColor("#000");
		setBackgroundColor(FX_COLOR_NONE);
		setBorderColor(FX_COLOR_NONE);

		FXPackedLayout	*pLayout;

		setLayout(pLayout=new FXPackedLayout(FXPackedLayout::ByRows));
		pLayout->gapBetweenChildren(FXSize(2, 2));
		pLayout->padding(FXPadding(0, 0, 0, 0));

		add(m_pName = new FXStaticText(m_nameW, h()-2, name, FXFont("Arial", 11, FXFont::Bold), FX_VALIGN_MIDDLE|FX_HALIGN_RIGHT, "#000"));
		m_pName->padding(FXPadding(0, 0, 4, 4));

		add(m_pValue = pValueWidget);
		m_pValue->size(m_valueW, h()-2);
		m_pValue->callback(itemCallback);

		setValue(value);
		}


void
FXNamedWidget::setValue(const SWValue &v, int limitsize)
		{
		fx_widget_setValue(m_pValue, v, limitsize);
		}


SWValue
FXNamedWidget::getValue()
		{
		SWValue	v;

		fx_widget_getValue(m_pValue, v);
		
		return v;
		}



void
FXNamedWidget::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		do_callback(this, p);
		}



void
FXNamedWidget::activate()
		{
		m_pName->activate();
		m_pValue->activate();
		}


void
FXNamedWidget::deactivate()
		{
		m_pName->deactivate();
		m_pValue->deactivate();
		}

