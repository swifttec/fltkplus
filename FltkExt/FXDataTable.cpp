#include <FltkExt/FXDataTable.h>
#include <FltkExt/FXWnd.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

enum Flags
	{
	VScrollBar=1,
	HScrollBar=2
	};

FXDataTable::Attributes::Attributes()
		{
		attrFlags = 0;
		drawFlags = 0;
		fgcolor = bgcolor = FX_COLOR_DEFAULT;
		}

void
FXDataTable::Attributes::setFont(const FXFont &v)
		{
		font = v;
		attrFlags |= HasFont;
		}

void
FXDataTable::Attributes::setFgColor(const FXColor &v)
		{
		fgcolor = v;
		attrFlags |= HasFgColor;
		}

void
FXDataTable::Attributes::setBgColor(const FXColor &v)
		{
		bgcolor = v;
		attrFlags |= HasBgColor;
		}

bool
FXDataTable::Attributes::hasFgColor()
		{
		return ((attrFlags & HasFgColor) != 0);
		}

bool
FXDataTable::Attributes::hasBgColor()
		{
		return ((attrFlags & HasBgColor) != 0);
		}

bool
FXDataTable::Attributes::hasFont()
		{
		return ((attrFlags & HasFont) != 0);
		}



FXDataTable::Cell::Cell()
		{
		pAttributes = NULL;
		}

FXDataTable::Cell::~Cell()
		{
		safe_delete(pAttributes);
		}


bool
FXDataTable::Cell::getFont(Row &row, Column &col, FXFont &font)
		{
		bool	res=false;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFont())
				{
				font = pAttributes->font;
				res = true;
				}
			}

		if (!res)
			{
			res = row.getFont(font);
			}

		if (!res)
			{
			res = col.getFont(font);
			}

		return res;
		}


FXColor
FXDataTable::Cell::getFgColor(Row &row, Column &col)
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFgColor()) res = pAttributes->fgcolor;
			}

		if (res == FX_COLOR_DEFAULT)
			{
			res = row.getFgColor();
			}

		if (res == FX_COLOR_DEFAULT)
			{
			res = col.getFgColor();
			}

		return res;
		}

FXColor
FXDataTable::Cell::getBgColor(Row &row, Column &col)
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasBgColor()) res = pAttributes->bgcolor;
			}

		if (res == FX_COLOR_DEFAULT)
			{
			res = row.getBgColor();
			}

		if (res == FX_COLOR_DEFAULT)
			{
			res = col.getBgColor();
			}

		return res;
		}


sw_uint32_t
FXDataTable::Cell::getDrawFlags(Row &row, Column &col)
		{
		sw_uint32_t	res=0;

		if (pAttributes != NULL)
			{
			res = pAttributes->drawFlags;
			}

		if (res == 0)
			{
			res = row.getDrawFlags();
			}

		if (res == 0)
			{
			res = col.getDrawFlags();
			}

		return res;
		}


FXDataTable::Row::Row()
		{
		height = 0;
		flags = 0;
		pAttributes = NULL;
		}


FXDataTable::Row::~Row()
		{
		safe_delete(pAttributes);
		}


FXColor
FXDataTable::Row::getFgColor()
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFgColor()) res = pAttributes->fgcolor;
			}

		return res;
		}


FXColor
FXDataTable::Row::getBgColor()
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasBgColor()) res = pAttributes->bgcolor;
			}

		return res;
		}


sw_uint32_t
FXDataTable::Row::getDrawFlags()
		{
		sw_uint32_t	res=0;

		if (pAttributes != NULL)
			{
			res = pAttributes->drawFlags;
			}

		return res;
		}


bool
FXDataTable::Row::getFont(FXFont &font)
		{
		bool	res=false;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFont())
				{
				font = pAttributes->font;
				res = true;
				}
			}

		return res;
		}





FXDataTable::Column::Column()
		{
		width = 0;
		minWidth = 0;
		maxWidth = 0;
		pAttributes = NULL;
		}


FXDataTable::Column::~Column()
		{
		safe_delete(pAttributes);
		}


FXColor
FXDataTable::Column::getFgColor()
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFgColor()) res = pAttributes->fgcolor;
			}

		return res;
		}


FXColor
FXDataTable::Column::getBgColor()
		{
		FXColor	res=FX_COLOR_DEFAULT;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasBgColor()) res = pAttributes->bgcolor;
			}

		return res;
		}


sw_uint32_t
FXDataTable::Column::getDrawFlags()
		{
		sw_uint32_t	res=0;

		if (pAttributes != NULL)
			{
			res = pAttributes->drawFlags;
			}

		return res;
		}


bool
FXDataTable::Column::getFont(FXFont &font)
		{
		bool	res=false;

		if (pAttributes != NULL)
			{
			if (pAttributes->hasFont())
				{
				font = pAttributes->font;
				res = true;
				}
			}

		return res;
		}



FXDataTableSelection::FXDataTableSelection()
		{
		firstRow = lastRow = firstColumn = lastColumn = 0;
		}


FXDataTableSelection::FXDataTableSelection(const FXDataTableSelection &other)
		{
		*this = other;
		}


FXDataTableSelection &
FXDataTableSelection::operator=(const FXDataTableSelection &other)
		{
#define COPY(x)	x = other.x
		COPY(firstRow);
		COPY(lastRow);
		COPY(firstColumn);
		COPY(lastColumn);
#undef COPY

		return *this;
		}


bool
FXDataTableSelection::contains(int row, int col)
		{
		return (row >= firstRow && row <= lastRow && col >= firstColumn && col <= lastColumn);
		}


bool
FXDataTableSelection::containsRow(int row)
		{
		return (row >= firstRow && row <= lastRow);
		}


bool
FXDataTableSelection::containsColumn(int col)
		{
		return (col >= firstColumn && col <= lastColumn);
		}



void
FXDataTable::onEventCallback(Fl_Widget*, void* data)
		{
		FXDataTable	*o = (FXDataTable *)data;
		
		o->onCallback();
		}

void
FXDataTable::onCallback()
		{
/*
		int				R = callback_row();
		int				C = callback_col();
		TableContext	context = callback_context();

		if (context == Fl_Table::CONTEXT_COL_HEADER)
			{
			set_selection(-1, -1, -1, -1);
			}
*/
		
		FXWnd	*pWnd=dynamic_cast<FXWnd *>(parent());
		FXGroup	*pGroup=dynamic_cast<FXGroup *>(parent());

		if (pGroup != NULL)
			{
			pGroup->onItemCallback(this, NULL);
			}
		else if (pWnd != NULL)
			{
			pWnd->onItemCallback(this, NULL);
			}
		}



FXDataTable::FXDataTable(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_columnsChanged(false),
	m_cellsChanged(false),
	m_mutex(true),
	m_vScrollBar(true),
	m_hScrollBar(false),
	m_scrollflags(0),
	m_selectionMode(0),
	m_lastClickedColumn(-1),
	m_lastClickedRow(-1)
		{
		setBackgroundColor("#ccc");
		setMinSize(32, 32);

		m_columnGap = 0;
		m_rowGap = 1;
		m_tableBackgroundColor = "#444";
		m_tableBorderColor = "#222";
		m_cellPadding.left = m_cellPadding.right = 4;
		m_cellPadding.top = m_cellPadding.bottom = 2;

		m_headerRow.pAttributes = new Attributes();
		m_headerRow.pAttributes->setFont(FXFont("Arial", 12, FXFont::Bold));
		m_headerRow.pAttributes->setBgColor("#bbb");
		m_headerRow.pAttributes->setFgColor("#000");

		m_tableFont = FXFont("Arial", 12, FXFont::Normal);
		m_rowForegroundColor[0] = m_rowForegroundColor[1] = "#000";
		m_rowBackgroundColor[0] = "#f8f8f8";
		m_rowBackgroundColor[1] = "#fff";

		m_selectedRowForegroundColor[0] = m_selectedRowForegroundColor[1] = "#000";
		m_selectedRowBackgroundColor[0] = "#def";
		m_selectedRowBackgroundColor[1] = "#def";

		m_invertCellColorsOnSelection = false;
		//m_invertRowColorsOnSelection = false;

		m_idTimer = 0;
		clearSelection();
		callback(onEventCallback, this);
		}


FXDataTable::~FXDataTable()
		{
		clearSelection();
		removeAllRowData();
		}


void
FXDataTable::addColumn(const SWString &title, int flags)
		{
		SWMutexGuard	guard(m_mutex);
		int				index=getColumnCount();
		Column			&col=m_columns[index];

		col.pAttributes = new Attributes();
		col.title = title;
		col.pAttributes->drawFlags = flags;
		col.width = col.maxWidth = col.minWidth = 0;

		m_columnsChanged = true;

		FXFont	font;

		if (!m_headerRow.getFont(font)) font = m_tableFont;

		font.select();

		Cell	&cell=m_headerRow.cells[index];

		cell.contents = title;
		cell.size = fx_getTextSize(cell.contents);
		cell.size.cx += m_cellPadding.left + m_cellPadding.right;
		cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;

		col.width = cell.size.cx;
		m_tableSize.cx += cell.size.cx;
		m_headerRow.height = sw_math_max(m_headerRow.height, cell.size.cy);

		int		nrows=getRowCount();
		int		ncols=getColumnCount();

		for (int i=0;i<nrows;i++)
			{
			m_rows[i]->cells[index].size = FXSize(cell.size.cx, m_rows[i]->height);
			}

/*
		cols((int)m_columns.size());
		col_header(1);
*/
		}


void
FXDataTable::setColumnTitle(int index, const SWString &title)
		{
		SWMutexGuard	guard(m_mutex);

		if (index >= 0 && index < getColumnCount())
			{
			Column	&col=m_columns[index];

			fl_font(m_tableFont.fl_face(), m_tableFont.height());
			col.title = title;

			m_columnsChanged = true;

			FXFont	font;

			if (!m_headerRow.getFont(font)) font = m_tableFont;

			font.select();

			Cell	&cell=m_headerRow.cells[index];
			FXSize	sz=fx_getTextSize(cell.contents);

			sz.cx += m_cellPadding.left + m_cellPadding.right;
			sz.cy += m_cellPadding.top + m_cellPadding.bottom;
			cell.contents = title;
			if (sz.cx > col.width)
				{
				int		xdiff=sz.cx - col.width;

				cell.size = sz;
				col.width = sz.cx;
				m_tableSize.cx += xdiff;
				m_headerRow.height = sw_math_max(m_headerRow.height, cell.size.cy);

				int		nrows=getRowCount();
				int		ncols=getColumnCount();

				for (int i=0;i<nrows;i++)
					{
					m_rows[i]->cells[index].size = FXSize(cell.size.cx, m_rows[i]->height);
					}
				}
			}
		}


SWString
FXDataTable::getColumnTitle(int col) const
		{
		SWMutexGuard	guard(m_mutex);
		SWString		res;

		if (col >= 0 && col < getColumnCount())
			{
			res = m_columns.get(col).title;
			}
		
		return res;
		}


void
FXDataTable::calculateRowSize(Row &row)
		{
		SWMutexGuard	guard(m_mutex);

		int		ncols=getColumnCount();

		row.height = 0;
		for (int i=0;i<ncols;i++)
			{
			Cell	&cell=row.cells[i];
			Column	&column=m_columns[i];

			column.width = sw_math_max(column.width, cell.size.cx);
			row.height = sw_math_max(row.height, cell.size.cy);
			}
		}


void
FXDataTable::recalculate()
		{
		SWMutexGuard	guard(m_mutex);

		int		ncols=getColumnCount();
		int		nrows=getRowCount();

		m_cellsChanged = m_columnsChanged = false;
		m_tableSize = FXSize();
		for (int i=0;i<ncols;i++)
			{
			Column	&column=m_columns[i];

			column.width = 0;
			}

		calculateRowSize(m_headerRow);

		for (int j=0;j<nrows;j++)
			{
			calculateRowSize(*m_rows[j]);
			}

/*
		for (int i=0;i<ncols;i++)
			{
			col_width(i, m_columns[i].width, true);
			}

		table_resized();
*/
		}



void
FXDataTable::removeAllRowData()
		{
		SWMutexGuard	guard(m_mutex);

		for (int i=0;i<(int)m_rows.size();i++)
			{
			delete m_rows[i];
			}

		m_rows.clear();
//		rows(0);
		}


void
FXDataTable::removeAllRows()
		{
		SWMutexGuard	guard(m_mutex);

		removeAllRowData();
		recalculate();
		}


void
FXDataTable::updateRow(int row, const SWStringArray &values)
		{
		setRowValues(row, values);
		}


void
FXDataTable::setRowValues(int row, const SWStringArray &values)
		{
		SWMutexGuard	guard(m_mutex);

		for (int i=0;i<(int)values.size();i++)
			{
			setCellString(row, i, values.get(i));
			}
		}

int
FXDataTable::addRow(bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);
		SWStringArray	values;
		int				index=getRowCount();

		insertRow(getRowCount(), values, batchmode);

		return index;
		}

int
FXDataTable::addRow(const SWStringArray &values, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);
		int				index=getRowCount();

		insertRow(getRowCount(), values, batchmode);

		return index;
		}


int
FXDataTable::insertRow(int atidx, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);
		SWStringArray	values;

		insertRow(atidx, values, batchmode);

		return atidx;
		}


void
FXDataTable::deleteRow(int row, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >= 0 && row < (int)m_rows.size())
			{
			delete m_rows[row];
			m_rows.remove(row);
			}
		}


void
FXDataTable::endBatchMode()
		{
//		table_resized();
		}



int
FXDataTable::insertRow(int atidx, const SWStringArray &values, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);
		Row				*pRow=new Row();
		int				ncols=getColumnCount();
		FXFont			font;

		m_rows.insert(pRow, atidx);

		for (int i=0;i<ncols;i++)
			{
			Column		&column=m_columns[i];
			Cell		&cell=pRow->cells[i];
			SWString	s;

			if (!cell.getFont(*pRow, column, font))
				{
				font = m_tableFont;
				}

			font.select();

			if (i < (int)values.size()) s = values.get(i);
			cell.contents = s;
			cell.size = fx_getTextSize(s);
			cell.size.cx += m_cellPadding.left + m_cellPadding.right;
			cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;

			if (cell.size.cx > column.width)
				{
				m_tableSize.cx += cell.size.cx - column.width;
 				column.width = cell.size.cx;
				}
			pRow->height = sw_math_max(pRow->height, cell.size.cy);
			}

		m_cellsChanged = true;

		m_tableSize.cy += pRow->height;

		m_vScrollBar.setScrollRange(0, getRowCount()-1);

		return atidx;
		}

void
FXDataTable::setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			Row	*pRow=m_rows[row];

			if (pRow->pAttributes == NULL)
				{
				pRow->pAttributes = new Attributes();
				}

			pRow->pAttributes->setFgColor(fgcolor);
			pRow->pAttributes->setBgColor(bgcolor);
			}
		}


void
FXDataTable::setCell(int row, int col, const SWString &contents, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		setCellString(row, col, contents);
		setCellColors(row, col, fgcolor, bgcolor);
		}


void
FXDataTable::setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				Cell	&cell=m_rows[row]->cells[col];

				if (cell.pAttributes == NULL)
					{
					cell.pAttributes = new Attributes();
					}

				cell.pAttributes->setFgColor(fgcolor);
				cell.pAttributes->setBgColor(bgcolor);
				}
			}
		}

void
FXDataTable::setCellString(int row, int col, const SWString &contents)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				Column		&column=m_columns[col];
				Row			*pRow=m_rows[row];
				Cell		&cell=pRow->cells[col];
				SWString	s;
				FXFont		font;

				if (!cell.getFont(*pRow, column, font))
					{
					font = m_tableFont;
					}

				font.select();

				fl_font(m_tableFont.fl_face(), m_tableFont.height());
				cell.contents = contents;
				cell.size = fx_getTextSize(contents);
				cell.size.cx += m_cellPadding.left + m_cellPadding.right;
				cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;

				if (cell.size.cx > column.width)
					{
					m_tableSize.cx += cell.size.cx - column.width;
 					column.width = cell.size.cx;
					}
				pRow->height = sw_math_max(pRow->height, cell.size.cy);

				m_cellsChanged = true;
				}
			}
		}


SWString
FXDataTable::getCellString(int row, int col) const
		{
		SWMutexGuard	guard(m_mutex);
		SWString		res;

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				res = m_rows.get(row)->cells.get(col).contents;
				}
			}
		
		return res;
		}



bool
FXDataTable::isCellSelected(int row, int col)
		{
		bool	res=false;

		for (int i=0;!res&&i<(int)m_selected.size();i++)
			{
			FXDataTableSelection	*pSelection=m_selected[i];

			res = (row >= pSelection->firstRow && row <= pSelection->lastRow && col >= pSelection->firstColumn && col <= pSelection->lastColumn);
			}

		return res;
		}

void
FXDataTable::setSelectedRow(int row)
		{
		clearSelection();
		addRowToSelection(row);
		}


int
FXDataTable::getSelectedRow()
		{
		int		res=-1;

		if (m_selected.size() != 0)
			{
			res = m_selected[0]->firstRow;
			}

		return res;
		}

void
FXDataTable::setSelectedColumn(int col)
		{
		clearSelection();
		addColumnToSelection(col);
		}


int
FXDataTable::getSelectedColumn()
		{
		int		res=-1;

		if (m_selected.size() != 0)
			{
			res = m_selected[0]->firstColumn;
			}

		return res;
		}

void
FXDataTable::setSelection(int firstCol, int firstRow, int lastCol, int lastRow)
		{
		clearSelection();

		FXDataTableSelection	*pSelection=new FXDataTableSelection();
		
		pSelection->firstColumn = firstCol;
		pSelection->lastColumn = lastCol;
		pSelection->firstRow = firstRow;
		pSelection->lastRow = lastRow;

		m_selected.add(pSelection);
		}


void
FXDataTable::getSelection(SWArray<FXDataTableSelection> &selection)
		{
		selection.clear();
		
		for (int i=0;i<(int)m_selected.size();i++)
			{
			selection.add(*m_selected[i]);
			}
		}


void
FXDataTable::getSelection(int &firstCol, int &firstRow, int &lastCol, int &lastRow)
		{
		if (m_selected.size() != 0)
			{
			FXDataTableSelection	*pSelection=m_selected[0];

			firstCol = pSelection->firstColumn;
			firstRow = pSelection->firstRow;
			lastCol = pSelection->lastColumn;
			lastRow = pSelection->lastRow;
			}
		else
			{
			firstCol = firstRow = lastCol = lastRow = -1;
			}
		}


void
FXDataTable::clearSelection()
		{
		for (int i=0;i<(int)m_selected.size();i++)
			{
			safe_delete(m_selected[i]);
			}

		m_selected.clear();
		}


void
FXDataTable::setTopRow(int row)
		{
		m_vScrollBar.setScrollPosition(row);
		}

void
FXDataTable::getCellColors(Row &row, int R, int C, FXColor &fgcolor, FXColor &bgcolor)
		{
		Cell	&cell=row.cells[C];
		Column	&column=m_columns[C];

		if (isCellSelected(R, C))
			{
			bgcolor = FX_COLOR_DEFAULT;
			fgcolor = FX_COLOR_DEFAULT;
			if (m_invertCellColorsOnSelection)
				{
				fgcolor = cell.getBgColor(row, column);
				bgcolor = cell.getFgColor(row, column);
				}
			
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = m_selectedRowBackgroundColor[R % 2];
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = "#def";

			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = m_selectedRowForegroundColor[R % 2];
			}
		else
			{
			bgcolor = cell.getBgColor(row, column);
			fgcolor = cell.getFgColor(row, column);
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = m_rowBackgroundColor[R % 2];
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = "#fff";

			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = m_rowForegroundColor[R % 2];
			}

		if (fgcolor == FX_COLOR_DEFAULT) fgcolor = "#000";
		}


void
FXDataTable::onDraw()
		{
		SWMutexGuard	guard(m_mutex);
		FXRect			cr, vr, hr;
		int				adjx=0, adjy=0;
		FXSize			szTable=m_tableSize;

		szTable.cy += m_headerRow.height;

		getContentsRect(cr);

		for (int pass=0;pass<2;pass++)
			{
			if (szTable.cy > (cr.height() - adjy))
				{
				m_scrollflags |= VScrollBar;
				adjx = 16;
				}
			else
				{
				m_scrollflags &= ~VScrollBar;
				}

			if (szTable.cx > (cr.width() - adjx))
				{
				m_scrollflags |= HScrollBar;
				adjy = 16;
				}
			else
				{
				m_scrollflags &= ~HScrollBar;
				}
			}

		cr.deflate(0, 0, adjx, adjy);

		fl_push_clip(cr.left, cr.top, cr.width(), cr.height());
		
		int		ypos=cr.top;

		ypos += drawRow(m_headerRow, -1, cr.left, ypos, cr.right);

		int		nrows=getRowCount();
		int		ncols=getColumnCount();
		int		rowcount=0;
		int		firstrow=0;

		if ((m_scrollflags & VScrollBar) != 0)
			{
			FXScrollInfo	si;

			m_vScrollBar.getScrollInfo(si);
			firstrow = si.position();
			}

		bool	hitend=false;

		for (int i=firstrow;i<nrows;i++)
			{
			Row		&row=*m_rows[i];

			ypos += row.height;
			if (ypos > cr.bottom)
				{
				hitend = true;
				break;
				}
			}

		if (hitend)
			{
			while (firstrow > 0)
				{
				Row		&row=*m_rows[firstrow-1];

				ypos += row.height;
				if (ypos > cr.bottom)
					{
					break;
					}

				firstrow -= 1;
				}
			}

		ypos = cr.top + m_headerRow.height;

		for (int i=firstrow;i<nrows;i++)
			{
			Row		&row=*m_rows[i];

			ypos += drawRow(row, i, cr.left, ypos, cr.right);
			if (ypos >= cr.bottom) break;
			rowcount++;
			}

		fl_pop_clip();

		cr.inflate(0, 0, adjx, adjy);

		if ((m_scrollflags & VScrollBar) != 0 && rowcount > 0)
			{
			vr.right = cr.right;
			vr.left = vr.right - 15;
			vr.top = cr.top + m_headerRow.height;
			vr.bottom = cr.bottom - adjy;

			m_vScrollBar.setScrollRange(0, getRowCount()-1, rowcount-1);
			m_vScrollBar.onDraw(vr);
			}

		if ((m_scrollflags & HScrollBar) != 0)
			{
			hr.right = cr.right - 16;
			hr.left = cr.left;
			hr.top = cr.bottom - 15;
			hr.bottom = cr.bottom;
			m_hScrollBar.setScrollRange(0, m_tableSize.cx, hr.width());
			m_hScrollBar.onDraw(hr);
			}

		}

int
FXDataTable::drawRow(Row &row, int R, int xpos, int ypos, int xlimit)
		{
		int		ncols=getColumnCount();
		int		xleft=xpos;

		if ((m_scrollflags & HScrollBar) != 0)
			{
			FXScrollInfo	si;

			m_hScrollBar.getScrollInfo(si);
			xleft -= si.position();
			}

		for (int i=0;i<ncols;i++)
			{
			Column	&column=m_columns[i];
			int		xright=xleft + column.width - 1;

			if (xright < xpos)
				{
				xleft += column.width;
				}
			else
				{
				xleft += drawCell(row, R, i, xleft, ypos);
				}

			if (xleft > xlimit) break;
			}

		return row.height;
		}


int
FXDataTable::drawCell(Row &row, int R, int C, int xpos, int ypos)
		{
		Column	&column=m_columns[C];
		Cell	&cell=row.cells[C];
		FXColor	fgcolor, bgcolor;
		FXFont	font;
		FXRect	dr(xpos, ypos, xpos + column.width-1, ypos + row.height - 1);

		if (!cell.getFont(row, column, font)) font = m_tableFont;

		getCellColors(row, R, C, fgcolor, bgcolor);

		fx_draw_rect(dr, 0, 0, FX_COLOR_NONE, bgcolor);
		
		dr.deflate(m_cellPadding.left, m_cellPadding.top, m_cellPadding.right, m_cellPadding.bottom);
		fx_draw_text(cell.contents, dr, cell.getDrawFlags(row, column), font, fgcolor);
//		fx_draw_text(cell.contents, dr, cell.getDrawFlags(row, column));

		return column.width;
		}


void
FXDataTable::getCellAddressAt(const FXPoint &pt, int &R, int &C)
		{
		FXRect	cr;

		R = C = -1;
		getContentsRect(cr);

		if ((m_scrollflags & VScrollBar) != 0)
			{
			cr.right -= 16;
			}

		if ((m_scrollflags & HScrollBar) != 0)
			{
			cr.bottom -= 16;
			}

		int		xleft=cr.left;
		int		ncols=getColumnCount();
		int		nrows=getRowCount();

		if ((m_scrollflags & HScrollBar) != 0)
			{
			FXScrollInfo	si;

			m_hScrollBar.getScrollInfo(si);
			xleft -= si.position();
			}

		for (int i=0;i<ncols;i++)
			{
			Column	&column=m_columns[i];
			int		xright=xleft + column.width - 1;

			if (pt.x >= xleft && pt.x <= xright)
				{
				C = i;
				break;
				}

			xleft += column.width;
			if (xleft >= cr.right) break;
			}

		if (pt.y >= cr.top && pt.y <= (cr.top + m_headerRow.height))
			{
			R = -2;
			}
		else
			{
			int		firstrow=0;
			int		ytop=cr.top + m_headerRow.height;

			if ((m_scrollflags & VScrollBar) != 0)
				{
				FXScrollInfo	si;

				m_vScrollBar.getScrollInfo(si);
				firstrow = si.position();
				}

			for (int i=firstrow;i<nrows;i++)
				{
				Row		&row=*m_rows[i];
				int		ybottom=ytop + row.height - 1;

				if (pt.y >= ytop && pt.y <= ybottom)
					{
					R = i;
					break;
					}

				ytop += row.height;
				if (ytop >= cr.bottom) break;
				}
			}
		}


void
FXDataTable::addColumnToSelection(int col)
		{
		int						action=1;
		int						pos=-1;
		FXDataTableSelection	*pSelection;

		for (int i=0;i<(int)m_selected.size();i++)
			{
			pSelection = m_selected[i];

			if (col > (pSelection->lastColumn + 1))
				{
				continue;
				}
			else if (col == (pSelection->lastColumn + 1))
				{
				// Join to the current col
				pSelection->lastColumn = col;
				action = 0;
				break;
				}
			else if (col < pSelection->firstColumn)
				{
				if (pSelection->firstColumn == (col + 1))
					{
					pSelection->firstColumn = col;
					action = 0;
					}
				else
					{
					pos = i;
					action = 2;
					}
				break;
				}
			}

		if (action != 0)
			{
			pSelection = new FXDataTableSelection();

			pSelection->firstColumn = pSelection->lastColumn = col;
			pSelection->firstRow = 0;
			pSelection->lastRow = getRowCount()-1;
			
			if (action == 2) m_selected.insert(pSelection, pos);
			else m_selected.add(pSelection);
			}

		pos = 0;
		while (pos < (int)(m_selected.size()-1))
			{
			FXDataTableSelection	*p1=m_selected[pos];
			FXDataTableSelection	*p2=m_selected[pos+1];

			if ((p1->lastColumn + 1) == p2->firstColumn)
				{
				p1->lastColumn = p2->lastColumn;
				safe_delete(p2);
				m_selected.remove(pos+1);
				}
			else
				{
				pos++;
				}
			}
		}


void
FXDataTable::removeColumnFromSelection(int col)
		{
		for (int i=0;i<(int)m_selected.size();i++)
			{
			FXDataTableSelection	*pSelection=m_selected[i];

			if (pSelection->containsColumn(col))
				{
				if (pSelection->firstColumn == col && pSelection->lastColumn == col)
					{
					safe_delete(pSelection);
					m_selected.remove(i);
					}
				else if (pSelection->firstColumn == col)
					{
					pSelection->firstColumn++;
					}
				else if (pSelection->lastColumn == col)
					{
					pSelection->lastColumn--;
					}
				else
					{
					// Split in 2
					FXDataTableSelection	*p=new FXDataTableSelection();

					p->firstRow = pSelection->firstRow;
					p->lastRow = pSelection->lastRow;
					p->firstColumn = col+1;
					p->lastColumn = pSelection->lastColumn;
					pSelection->lastColumn = col-1;
					m_selected.insert(p, i+1);
					}
				break;
				}
			}
		}

void
FXDataTable::addRowToSelection(int row)
		{
		int						action=1;
		int						pos=-1;
		FXDataTableSelection	*pSelection;

		for (int i=0;i<(int)m_selected.size();i++)
			{
			pSelection = m_selected[i];

			if (row > (pSelection->lastRow + 1))
				{
				continue;
				}
			else if (row == (pSelection->lastRow + 1))
				{
				// Join to the current row
				pSelection->lastRow = row;
				action = 0;
				break;
				}
			else if (row < pSelection->firstRow)
				{
				if (pSelection->firstRow == (row + 1))
					{
					pSelection->firstRow = row;
					action = 0;
					}
				else
					{
					pos = i;
					action = 2;
					}
				break;
				}
			}

		if (action != 0)
			{
			pSelection = new FXDataTableSelection();

			pSelection->firstRow = pSelection->lastRow = row;
			pSelection->firstColumn = 0;
			pSelection->lastColumn = getColumnCount()-1;
			
			if (action == 2) m_selected.insert(pSelection, pos);
			else m_selected.add(pSelection);
			}

		pos = 0;
		while (pos < (int)(m_selected.size()-1))
			{
			FXDataTableSelection	*p1=m_selected[pos];
			FXDataTableSelection	*p2=m_selected[pos+1];

			if ((p1->lastRow + 1) == p2->firstRow)
				{
				p1->lastRow = p2->lastRow;
				safe_delete(p2);
				m_selected.remove(pos+1);
				}
			else
				{
				pos++;
				}
			}
		}


void
FXDataTable::removeRowFromSelection(int row)
		{
		for (int i=0;i<(int)m_selected.size();i++)
			{
			FXDataTableSelection	*pSelection=m_selected[i];

			if (pSelection->containsRow(row))
				{
				if (pSelection->firstRow == row && pSelection->lastRow == row)
					{
					safe_delete(pSelection);
					m_selected.remove(i);
					}
				else if (pSelection->firstRow == row)
					{
					pSelection->firstRow++;
					}
				else if (pSelection->lastRow == row)
					{
					pSelection->lastRow--;
					}
				else
					{
					// Split in 2
					FXDataTableSelection	*p=new FXDataTableSelection();

					p->firstColumn = pSelection->firstColumn;
					p->lastColumn = pSelection->lastColumn;
					p->firstRow = row+1;
					p->lastRow = pSelection->lastRow;
					pSelection->lastRow = row-1;
					m_selected.insert(p, i+1);
					}
				break;
				}
			}
		}


void
FXDataTable::updateRowSelection(int row, bool multi, bool ctrl, bool shift)
		{
		bool	add=false;

		if (multi && (ctrl || shift))
			{
			if (shift)
				{
				int		lastrow=m_lastClickedRow;

				if (lastrow < 0) lastrow = 0;

				if (!ctrl) clearSelection();

				while (lastrow != row)
					{
					addRowToSelection(lastrow);
					if (lastrow < row) lastrow++;
					else lastrow--;
					}
				
				addRowToSelection(row);
				}
			else if (ctrl)
				{
				if (!isCellSelected(row, 0))
					{
					addRowToSelection(row);
					}
				else
					{
					removeRowFromSelection(row);
					}
				}
			}
		else
			{
			clearSelection();
			addRowToSelection(row);
			}
		}


void
FXDataTable::updateColumnSelection(int col, bool multi, bool ctrl, bool shift)
		{
		bool	add=false;

		if (multi && (ctrl || shift))
			{
			if (shift)
				{
				int		lastcol=m_lastClickedColumn;

				if (lastcol < 0) lastcol = 0;

				if (!ctrl) clearSelection();

				while (lastcol != col)
					{
					addColumnToSelection(lastcol);
					if (lastcol < col) lastcol++;
					else lastcol--;
					}
				
				addColumnToSelection(col);
				}
			else if (ctrl)
				{
				if (!isCellSelected(0, col))
					{
					addColumnToSelection(col);
					}
				else
					{
					removeColumnFromSelection(col);
					}
				}
			}
		else
			{
			clearSelection();
			addColumnToSelection(col);
			}
		}


void
FXDataTable::addCellToSelection(int row, int col)
		{
		int						action=1;
		int						pos=-1;
		FXDataTableSelection	*pSelection;

		/*
		for (int i=0;i<(int)m_selected.size();i++)
			{
			pSelection = m_selected[i];

			if (row > (pSelection->lastRow + 1))
				{
				continue;
				}
			else if (row == (pSelection->lastRow + 1))
				{
				// Join to the current row
				pSelection->lastRow = row;
				action = 0;
				break;
				}
			else if (row < pSelection->firstRow)
				{
				if (pSelection->firstRow == (row + 1))
					{
					pSelection->firstRow = row;
					action = 0;
					}
				else
					{
					pos = i;
					action = 2;
					}
				break;
				}
			}
		*/

		if (action != 0)
			{
			pSelection = new FXDataTableSelection();

			pSelection->firstRow = pSelection->lastRow = row;
			pSelection->firstColumn = pSelection->lastColumn = col;
			
			if (action == 2) m_selected.insert(pSelection, pos);
			else m_selected.add(pSelection);
			}

		/*
		pos = 0;
		while (pos < (int)(m_selected.size()-1))
			{
			Selection	*p1=m_selected[pos];
			Selection	*p2=m_selected[pos+1];

			if ((p1->lastRow + 1) == p2->firstRow)
				{
				p1->lastRow = p2->lastRow;
				safe_delete(p2);
				m_selected.remove(pos+1);
				}
			else
				{
				pos++;
				}
			}
		*/
		}


void
FXDataTable::removeCellFromSelection(int row, int col)
		{
		for (int i=0;i<(int)m_selected.size();i++)
			{
			FXDataTableSelection	*pSelection=m_selected[i];

			if (pSelection->contains(row, col))
				{
				safe_delete(pSelection);
				m_selected.remove(i);
				break;
				}
			}
		}


void
FXDataTable::updateCellSelection(int row, int col, bool multi, bool ctrl, bool shift)
		{
		bool	add=false;

		if (multi && (ctrl || shift))
			{
			if (shift)
				{
				int		colfrom=m_lastClickedColumn;
				int		colto=col;
				int		rowfrom=m_lastClickedRow;
				int		rowto=row;

				if (colfrom < 0) colfrom = 0;
				if (rowfrom < 0) rowfrom = 0;

				if (colfrom > colto)
					{
					int	v;

					v = colfrom;
					colfrom = colto;
					colto = v;
					}

				if (rowfrom > rowto)
					{
					int	v;

					v = rowfrom;
					rowfrom = rowto;
					rowto = v;
					}


				if (!ctrl) clearSelection();

				for (row=rowfrom;row<=rowto;row++)
					{
					for (col=colfrom;col<=colto;col++)
						{
						addCellToSelection(row, col);
						}
					}
				}
			else if (ctrl)
				{
				if (!isCellSelected(row, col))
					{
					addCellToSelection(row, col);
					}
				else
					{
					removeCellFromSelection(row, col);
					}
				}
			}
		else
			{
			clearSelection();
			addCellToSelection(row, col);
			}
		}


int
FXDataTable::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		if ((m_scrollflags & VScrollBar) != 0 && m_vScrollBar.contains(pt))
			{
			res = m_vScrollBar.onMouseDown(button, pt);
			if (res) m_vTrackMouse = true;
			}
		else if ((m_scrollflags & HScrollBar) != 0 && m_hScrollBar.contains(pt))
			{
			res = m_hScrollBar.onMouseDown(button, pt);
			if (res) m_hTrackMouse = true;
			}
		else
			{
			int		row, col;

			getCellAddressAt(pt, row, col);

			if (col >= 0 && row >= 0)
				{
				int		mode=m_selectionMode & (SelectRows|SelectColumns|SelectCells);
				bool	multi=((m_selectionMode & SelectMultiple) != 0);
				bool	ctrlkey=(Fl::event_ctrl() != 0);
				bool	shiftkey=(Fl::event_shift() != 0);

				if (mode == SelectRows)
					{
					updateRowSelection(row, multi, ctrlkey, shiftkey);
					do_callback();
					}
				else if (mode == SelectColumns)
					{
					updateColumnSelection(col, multi, ctrlkey, shiftkey);
					do_callback();
					}
				else if (mode == SelectCells)
					{
					updateCellSelection(row, col, multi, ctrlkey, shiftkey);
					do_callback();
					}
				else
					{
					clearSelection();
					do_callback();
					}

				if (!shiftkey)
					{
					m_lastClickedColumn = col;
					m_lastClickedRow = row;
					}
				}
			else
				{
				clearSelection();
				do_callback();
				}

			res = 1;
			}
		
		if (res)
			{
			if (res > 1) m_idTimer = setIntervalTimerMS(1, 100, NULL);
			invalidate();
			}

		return res;
		}


int
FXDataTable::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=0;
		
		if (m_vTrackMouse)
			{
			res = m_vScrollBar.onMouseUp(button, pt);
			m_vTrackMouse = false;
			}
		else if (m_hTrackMouse)
			{
			res = m_hScrollBar.onMouseUp(button, pt);
			m_hTrackMouse = false;
			}

		if (res)
			{
			invalidate();
			}

		if (m_idTimer != 0)
			{
			killIntervalTimer(1);
			m_idTimer = 0;
			}

		return res;
		}


int
FXDataTable::onMouseDrag(const FXPoint &pt)
		{
		int		res=0;
		
		if (m_vTrackMouse)
			{
			res = m_vScrollBar.onMouseDrag(pt);
			}
		else if (m_hTrackMouse)
			{
			res = m_hScrollBar.onMouseDrag(pt);
			}

		if (res) invalidate();

		return res;
		}

int
FXDataTable::onMouseWheel(int dx, int dy, const FXPoint &pt)
		{
		int		res=0;

		if ((m_scrollflags & VScrollBar) != 0 && dy != 0)
			{
			res = m_vScrollBar.onMouseWheel(dx, dy, pt);
			}
		else if ((m_scrollflags & HScrollBar) != 0 && dx != 0)
			{
			res = m_hScrollBar.onMouseWheel(dx, dy, pt);
			}
		
		if (res) invalidate();

		return res;
		}


bool
FXDataTable::onTimer(sw_uint32_t id, void *data)
		{
		bool	changed=false;

		if (m_vScrollBar.onTimer(id, data)) changed = true;
		if (m_hScrollBar.onTimer(id, data)) changed = true;

		if (changed) invalidate();

		return true;
		}