/*
**	FXPromptStringDialog.h	A dialog to prompt for a string
*/

#ifndef __FltkExt_FXPromptStringDialog_h__
#define __FltkExt_FXPromptStringDialog_h__

#include <FltkExt/FXDialog.h>


/**
*** A popup dialog window for entering a number
**/
class FLTKEXT_DLL_EXPORT FXPromptStringDialog : public FXDialog
		{
public:
		FXPromptStringDialog(const SWString &title, const SWString &prompt, const SWString &value, Fl_Widget *pParent=NULL);
		virtual ~FXPromptStringDialog();

		virtual void		onOK();

		const SWString &	value() const	{ return m_value; }

protected:
		SWString		m_prompt;
		SWString		m_title;
		SWString		m_value;
		bool			m_hasminmax;
		int				m_minValue;
		int				m_maxValue;
		};


#endif // __FltkExt_FXPromptStringDialog_h__
