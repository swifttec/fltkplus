#include "FXInputString.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <FltkExt/FXPackedLayout.h>

#include <ctype.h>

FXInputString::FXInputString(int X, int Y, int W, int H, const SWString &filterChars, FilterMode filterMode, CaseMode caseMode, bool multiline) :
	Fl_Input(X, Y, W, H),
    m_filterChars(filterChars),
    m_filterMode(filterMode),
    m_caseMode(caseMode)
		{
		when(FL_WHEN_CHANGED);

		if (multiline) type(FL_MULTILINE_INPUT);
		}


FXInputString::FXInputString(int W, int H) :
	Fl_Input(0, 0, W, H),
    m_filterChars(""),
    m_filterMode(0),
    m_caseMode(0)
		{
		when(FL_WHEN_CHANGED);
		}



void
FXInputString::setFilter(const SWString &chars, FilterMode fmode, CaseMode cmode)
		{
		setFilterChars(chars);
		setFilterMode(fmode);
		setCaseMode(cmode);
		}


bool
FXInputString::filterChar(int &nChar)
		{
		bool	res=true;

		switch (m_caseMode)
			{
			case ToUpper:
				if (islower(nChar))
					{
					nChar = toupper(nChar);
					}
				break;

			case ToLower:
				if (isupper(nChar))
					{
					nChar = tolower(nChar);
					}
				break;
			}

		int	pos=m_filterChars.Find(nChar);

		res = (nChar == '\b'
			|| (m_filterMode == Exclude && pos < 0) 
			|| (m_filterMode == Include && pos >= 0)
			|| (m_filterMode == NoFilterChars));

		return res;
		}
