#include "FXCursor.h"

#include <FL/Fl_Window.H>


SWList<FXInternalCursor *>	*FXInternalCursor::m_pCursorList=NULL;

FXInternalCursor *
FXInternalCursor::create(Fl_Cursor cursor)
		{
		if (m_pCursorList == NULL)
			{
			m_pCursorList = new SWList<FXInternalCursor *>();
			m_pCursorList->synchronize();
			}

		FXInternalCursor	*pCursor=NULL;

		m_pCursorList->lock();

		SWIterator<FXInternalCursor *>	it=m_pCursorList->iterator();

		while (it.hasNext())
			{
			FXInternalCursor	*p=*it.next();

			if (p->m_type == FXInternalCursor::BuiltIn && p->m_cursor == cursor)
				{
				pCursor = p;
				break;
				}
			}

		if (pCursor == NULL)
			{
			pCursor = new FXInternalCursor(cursor);
			m_pCursorList->add(pCursor);
			}
		else
			{
			pCursor->m_refcount++;
			}

		m_pCursorList->unlock();

		return pCursor;
		}

FXInternalCursor *
FXInternalCursor::create(const SWString &filename, int imageHotspotX, int imageHotspotY)
		{
		if (m_pCursorList == NULL)
			{
			m_pCursorList = new SWList<FXInternalCursor *>();
			m_pCursorList->synchronize();
			}

		FXInternalCursor	*pCursor=NULL;

		m_pCursorList->lock();

		SWIterator<FXInternalCursor *>	it=m_pCursorList->iterator();

		while (it.hasNext())
			{
			FXInternalCursor	*p=*it.next();

			if (p->m_type == FXInternalCursor::Image && p->m_imagefile == filename)
				{
				pCursor = p;
				break;
				}
			}

		if (pCursor == NULL)
			{
			pCursor = new FXInternalCursor(filename, imageHotspotX, imageHotspotY);
			m_pCursorList->add(pCursor);
			}
		else
			{
			pCursor->m_refcount++;
			}

		m_pCursorList->unlock();

		return pCursor;
		}


FXInternalCursor *
FXInternalCursor::create(FXInternalCursor *pFindCursor)
		{
		if (m_pCursorList == NULL)
			{
			m_pCursorList = new SWList<FXInternalCursor *>();
			m_pCursorList->synchronize();
			}

		FXInternalCursor	*pCursor=NULL;

		m_pCursorList->lock();

		SWIterator<FXInternalCursor *>	it=m_pCursorList->iterator();

		while (it.hasNext())
			{
			FXInternalCursor	*p=*it.next();

			if (p == pFindCursor)
				{
				pCursor = p;
				break;
				}
			}

		if (pCursor == NULL)
			{
			// Should not happen - but just in case
			pCursor = new FXInternalCursor();
			
			pCursor->m_type = pFindCursor->m_type;
			pCursor->m_cursor = pFindCursor->m_cursor;
			pCursor->m_pImage = pFindCursor->m_pImage;
			pCursor->m_imageHotspotX = pFindCursor->m_imageHotspotX;
			pCursor->m_imageHotspotY = pFindCursor->m_imageHotspotY;

			m_pCursorList->add(pCursor);
			}
		else
			{
			pCursor->m_refcount++;
			}

		m_pCursorList->unlock();

		return pCursor;
		}

void
FXInternalCursor::destroy(FXInternalCursor *pCursor)
		{
		if (m_pCursorList == NULL)
			{
			m_pCursorList = new SWList<FXInternalCursor *>();
			m_pCursorList->synchronize();
			}

		m_pCursorList->lock();

		SWIterator<FXInternalCursor *>	it=m_pCursorList->iterator();

		while (it.hasNext())
			{
			FXInternalCursor	*p=*it.next();

			if (p == pCursor)
				{
				if (--(p->m_refcount) <= 0)
					{
					safe_delete(p);
					it.remove();
					}
				break;
				}
			}

		m_pCursorList->unlock();
		}



FXInternalCursor::FXInternalCursor() :
	m_type(BuiltIn),
	m_cursor(FL_CURSOR_DEFAULT),
	m_refcount(1),
	m_pImage(NULL),
	m_imageHotspotX(0),
	m_imageHotspotY(0)
		{
		}



FXInternalCursor::FXInternalCursor(Fl_Cursor cursor) :
	m_type(BuiltIn),
	m_cursor(cursor),
	m_refcount(1),
	m_pImage(NULL),
	m_imageHotspotX(0),
	m_imageHotspotY(0)
		{
		}


FXInternalCursor::FXInternalCursor(const SWString &filename, int imageHotspotX, int imageHotspotY) :
	m_type(Image),
	m_cursor(FL_CURSOR_DEFAULT),
	m_refcount(1),
	m_pImage(NULL),
	m_imageHotspotX(imageHotspotX),
	m_imageHotspotY(imageHotspotY)
		{
		m_pImage = new Fl_PNG_Image(filename);
		m_imagefile = filename;
		}

FXInternalCursor::~FXInternalCursor()
		{
		safe_delete(m_pImage);
		}



FXCursor::FXCursor() :
	m_pCursor(NULL)
		{
		}

		
FXCursor::FXCursor(Fl_Cursor cursor) :
	m_pCursor(NULL)
		{
		}


		
FXCursor::~FXCursor()
		{
		if (m_pCursor != NULL)
			{
			FXInternalCursor::destroy(m_pCursor);
			m_pCursor = NULL;
			}
		}


FXCursor &
FXCursor::operator=(const FXCursor &other)
		{
		if (m_pCursor != other.m_pCursor)
			{
			if (m_pCursor != NULL) FXInternalCursor::destroy(m_pCursor);
			if (other.m_pCursor != NULL) m_pCursor = FXInternalCursor::create(other.m_pCursor);
			else m_pCursor = NULL;
			}

		return *this;
		}


FXCursor &
FXCursor::operator=(Fl_Cursor cursor)
		{
		if (m_pCursor != NULL)
			{
			if (m_pCursor->m_type != FXInternalCursor::BuiltIn || m_pCursor->m_cursor != cursor)
				{
				FXInternalCursor::destroy(m_pCursor);
				m_pCursor = NULL;
				}
			}

		m_pCursor = FXInternalCursor::create(cursor);

		return *this;
		}


bool
FXCursor::operator==(const FXCursor &other)
		{
		return (m_pCursor == other.m_pCursor);
		}


bool
FXCursor::operator!=(const FXCursor &other)
		{
		return (m_pCursor != other.m_pCursor);
		}


void
FXCursor::applyToWindow(Fl_Window *pWnd)
		{
		if (pWnd != NULL)
			{
			bool	applied=false;

			if (m_pCursor != NULL)
				{
				switch (m_pCursor->m_type)
					{
					case FXInternalCursor::BuiltIn:
						// This is a simple built in cursor
						pWnd->cursor(m_pCursor->m_cursor);
						applied = true;
						break;
					
					case FXInternalCursor::Image:
						// This is a cursor loaded from an image
						if (m_pCursor->m_pImage != NULL && m_pCursor->m_pImage->w() != 0 && m_pCursor->m_pImage->h() != 0)
							{
							pWnd->cursor(m_pCursor->m_pImage, m_pCursor->m_imageHotspotX, m_pCursor->m_imageHotspotY);
							applied = true;
							}
						break;
					}
				}
			
			if (!applied)
				{
				pWnd->cursor(FL_CURSOR_DEFAULT);
				}
			}
		}


void
FXCursor::load(const SWString &filename, int imageHotspotX, int imageHotspotY)
		{
		if (m_pCursor != NULL)
			{
			if (m_pCursor->m_type != FXInternalCursor::Image || m_pCursor->m_imagefile != filename)
				{
				FXInternalCursor::destroy(m_pCursor);
				m_pCursor = NULL;
				}
			}

		m_pCursor = FXInternalCursor::create(filename, imageHotspotX, imageHotspotY);
		}

