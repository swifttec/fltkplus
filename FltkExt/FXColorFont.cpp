#include <FltkExt/FXColorFont.h>
#include <FltkExt/XDC.h>
#include <FL/Fl.H>

FXColorFont::FXColorFont()
		{
		m_color = FXColor("#000");
		}


FXColorFont::FXColorFont(const SWString &facename, double ptsize, int nstyle, const FXColor &color) :
	FXFont(facename, ptsize, nstyle),
	m_color(color)
		{
		}


FXColorFont::FXColorFont(const SWString &facename, int ptsize, int nstyle, const FXColor &color) :
	FXFont(facename, ptsize, nstyle),
	m_color(color)
		{
		}


FXColorFont::~FXColorFont()
		{
		}




/**
*** Copy constructor
**/
FXColorFont::FXColorFont(const FXColorFont &other) :
	FXFont(other)
		{
		*this = other;
		}



/**
*** Assignment operator
**/
FXColorFont &	
FXColorFont::operator=(const FXColorFont &other)
		{
		FXFont::operator=(other);

#define COPY(x)	x = other.x
		COPY(m_color);
#undef COPY

		return *this;
		}


void
FXColorFont::select() const
		{
		FXFont::select();
		m_color.select();
		}

