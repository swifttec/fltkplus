#include "FXBoxedText.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

FXBoxedText::FXBoxedText(int X, int Y, int W, int H, const SWString &text, Fl_Align textalign, Fl_Color fgcolor, Fl_Color bgcolor, int textheight) :
	Fl_Box(X, Y, W, H),
	m_text(text)
		{
		int		tw=0, th=0, dx=0, dy=0;

//		if (textheight != 0) fl_font(FL_HELVETICA, textheight);

//		fl_text_extents(text, dx, dy, tw, th);
		
		labelcolor(fgcolor);
		color(bgcolor);
		box(FL_ENGRAVED_BOX);

//		labelfont(FL_HELVETICA);
//		if (textheight != 0) labelsize(textheight);
		label(m_text);
		align(FL_ALIGN_INSIDE|FL_ALIGN_CLIP|FL_ALIGN_WRAP|textalign);
		}


FXBoxedText::~FXBoxedText()
		{
		}
