#include "FXFileDialog.h"

#include <FltkExt/fx_functions.h>

FXFileDialog::FXFileDialog(const SWString &folder, const SWString &pattern, Action action, const SWString &stitle) :
	m_folder(folder),
	m_title(stitle)
		{
		switch (action)
			{
			case OpenFile:
				type(BROWSE_FILE);
				break;

			case OpenMulti:
				type(BROWSE_MULTI_FILE);
				break;

			case SaveFile:
				type(BROWSE_SAVE_FILE);
				options(SAVEAS_CONFIRM|NEW_FOLDER);
				break;
			}

		directory(folder);
		filter(pattern);
		title(m_title);
		}


FXFileDialog::~FXFileDialog()
		{
		}


int
FXFileDialog::doModal()
		{
		return (show() == 0?FX_IDOK:FX_IDCANCEL);
		}