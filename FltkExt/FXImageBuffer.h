/****************************************************************************
**	FXImageBuffer.h	An abstract class to represent a number as an object
****************************************************************************/


#ifndef __FltkExt_FXImageBuffer_h__
#define __FltkExt_FXImageBuffer_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/XDC.h>
#include <FL/Fl_Shared_Image.H>

#include <swift/SWDataBuffer.h>

class FLTKEXT_DLL_EXPORT FXImageBuffer : public SWLockableObject
		{
public:
		FXImageBuffer();
		~FXImageBuffer();

		// Clear this object
		virtual void	clear();

		void			copy(const FXImageBuffer &other);

		/// Get the size of the image buffer in pixels
		const FXSize &	size() const		{ return m_size; }
		
		/// Set the size of the image buffer in pixels
		void			size(int w, int h)	{ setSize(w, h); }

		/// Set the size of the image buffer in pixels
		void			setSize(int w, int h);

		/// Get the width of the image buffer in pixels
		int				width() const		{ return m_size.cx; }

		/// Get the height of the image buffer in pixels
		int				height() const		{ return m_size.cy; }

		/// Draw the buffer
		void			draw(int posX, int posY, int W, int H, int offX=0, int offY=0);

		/// Draw the buffer
		void			draw(const FXRect &dr, const FXRect &sr);

		/// Set the specified pixel to the specified RGB value
		void			setPixel(int x, int y, sw_byte_t r, sw_byte_t g, sw_byte_t b);

		/// Set the specified pixel to the specified RGB value
		void			setPixel(int x, int y, const FXColor &color);

/*
		/// Get a pointer to the internal image buffer
		sw_byte_t *		getBuffer()			{ return m_pData; }

		/// Get the size of the internal image buffer in bytes
		sw_size_t		getBufferSize()		{ return (m_size.cx * m_size.cy * m_bytesPerPixel); }
*/

		/// Load the image from another buffer
		void			load(sw_byte_t *data, int width, int height, int bytesPerPixel);

		/// Test to see if this is a null image (has a zero dimension)
		bool			isNull() const		{ return (width() == 0 || height() == 0); }

		/// Fill the entire image with the specified color
		void			fill(sw_byte_t r, sw_byte_t g, sw_byte_t b);

		/// Fill the entire image with the specified color
		void			fill(const FXColor &color);

		/// Blend the current image into this one
		void			alphaBlend(const FXImageBuffer &other, int alphalevel);

protected:
		FXSize			m_size;				///< The dimensions of the image in pixels (W X H)
		int				m_bytesPerPixel;	///< The number of bytes per pixel
		sw_uint32_t		*m_pData;			///< The image data in byte order RGB
		};


#endif // __FltkExt_FXImageBuffer_h__
