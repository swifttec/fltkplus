#include <FltkExt/FXMinMaxSize.h>
#include <FltkExt/FXSize.h>

FXMinMaxSize::FXMinMaxSize(int minW, int minH, int maxW, int maxH) :
	minSize(minW, minH),
	maxSize(maxW, maxH)
		{
		}


FXMinMaxSize::FXMinMaxSize(const FXSize &vmin, const FXSize &vmax) :
	minSize(vmin),
	maxSize(vmax)
		{
		}


FXMinMaxSize::FXMinMaxSize(const FXMinMaxSize &other)
		{
		*this = other;
		}


FXMinMaxSize::~FXMinMaxSize()
		{
		}


FXMinMaxSize &
FXMinMaxSize::operator=(const FXMinMaxSize &other)
		{
#define COPY(x)	x = other.x
		COPY(minSize);
		COPY(maxSize);
#undef COPY

		return *this;
		}

bool
FXMinMaxSize::operator==(const FXMinMaxSize &other)
		{
		return (minSize == other.minSize && maxSize == other.maxSize);
		}


void
FXMinMaxSize::clear()
		{
		minSize = maxSize = FXSize();
		}
