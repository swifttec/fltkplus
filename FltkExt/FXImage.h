/****************************************************************************
**	FXImage.h	An abstract class to represent a number as an object
****************************************************************************/


#ifndef __FltkExt_FXImage_h__
#define __FltkExt_FXImage_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/XDC.h>
#include <FL/Fl_Shared_Image.H>

#include <swift/SWTempFile.h>



#define XI_STRETCH_TO_FIT		FX_IMAGE_STRETCH_TO_FIT
#define XI_PROPORTIONAL			FX_IMAGE_PROPORTIONAL
#define XI_STRETCH_TO_WIDTH		FX_IMAGE_STRETCH_TO_WIDTH
#define XI_STRETCH_TO_HEIGHT	FX_IMAGE_STRETCH_TO_HEIGHT

#define XI_FIT_TO_WIDTH			FX_IMAGE_FIT_TO_WIDTH
#define XI_FIT_TO_HEIGHT		FX_IMAGE_FIT_TO_HEIGHT

#define XI_USE_ALPHA			0x00008000
#define XI_TILE					0x00004000


// Define this to use CxImage class
#undef FX_USE_CXIMAGE

// Forward declaration
#ifdef FX_USE_CXIMAGE
class CxImage;
#endif

class FLTKEXT_DLL_EXPORT FXImage
		{
public:
		enum ImageType
			{
			ImageNone=0,
			ImageFile=1,
			ImageResource=2,
			ImageOther=3
			};

public:
		FXImage();
		FXImage(sw_uint32_t id);
		FXImage(const FXImage &other);
		FXImage(const SWString &filename, sw_uint32_t id=0);
		~FXImage();

		bool				operator==(const FXImage &other);
		FXImage &			operator=(const FXImage &other);

		bool				load(const SWString &filename, sw_uint32_t id=0);

		void				clear();
		void				unload()			{ clear(); }

		void				id(sw_uint32_t v)	{ m_idPicture = v; }
		sw_uint32_t			id() const			{ return m_idPicture; }
		int					type() const		{ return m_type; }
		const SWString &	filename() const	{ return m_filename; }

		bool				isNull() const		{ return !m_loaded; }
		bool				isLoaded() const	{ return m_loaded; }
		bool				loaded() const		{ return m_loaded; }

		FXSize				getImageSize();
		void				size(int width, int height);
		void				size(const FXSize &sz);
		FXSize				size() const		{ return m_size; }
		int					width() const		{ return m_size.cx; }
		int					height() const		{ return m_size.cy; }

		void				draw(const FXRect &rcDraw, sw_uint32_t flags=0, sw_int16_t alpha=255, FXRect *pImageRect=NULL);
		void				draw(const FXRect &rcDraw, const FXRect &rcSrc, sw_uint32_t flags=0, sw_int16_t alpha=255, FXRect *pImageRect=NULL);
		FXRect				render(FXRect &rcDraw, int flags);

		void				copy(const FXImage &other);

		Fl_Image *			fl_image()			{ return m_pImage; }

protected:
		void				createSizedImage(int w, int h);

protected:
		int			m_idPicture;
		SWString	m_filename;
		bool		m_tempfile;
		bool		m_loaded;
		ImageType	m_type;
		FXSize		m_size;
#ifdef FX_USE_CXIMAGE
		CxImage		*m_pImage;
#else
		Fl_Shared_Image	*m_pImage;
		Fl_Image		*m_pSizedImage;
		FXSize			m_szSizedImage;
#endif

private:
		static bool	m_initialised;
		static void	initialise();
		};


#endif // __FltkExt_FXImage_h__
