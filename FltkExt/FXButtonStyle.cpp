/*
**	FXWindowState.cpp		A class representing the state of a window
*/

#include <FltkExt/FXButtonStyle.h>
#include <FltkExt/FXSize.h>

FXButtonStyle::FXButtonStyle() :
	m_pImage(NULL),
	m_pSharedImage(NULL)
		{
		fgColor = FXColor("#000");
		bdColor = FXColor::NoColor;
		bgColor = FXColor("#ccc");
		font = FXFont("Arial", 10, FXFont::Normal);
		bdStyle = 1;
		bdWidth = 1;
		bdCornerSize = 8;
		}


FXButtonStyle::FXButtonStyle(const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &bdcolor, int bstyle, int bwidth, int corner, const FXFont &textfont) :
	m_pImage(NULL),
	m_pSharedImage(NULL)
		{
		fgColor = fgcolor;
		bdColor = bdcolor;
		bgColor = bgcolor;
		font = textfont;
		bdStyle = bstyle;
		bdWidth = bwidth;
		bdCornerSize = corner;
		}


FXButtonStyle::~FXButtonStyle()
		{
		clearImages();
		}


void
FXButtonStyle::clearImages()
		{
		if (m_pSharedImage != NULL)
			{
			m_pSharedImage->release();
			m_pSharedImage = NULL;
			}

		if (m_pImage != NULL)
			{
			safe_delete(m_pImage);
			}
		}


void
FXButtonStyle::setSharedImage(const SWString &filename)
		{
		clearImages();
		m_pSharedImage = Fl_Shared_Image::get(filename);
		}


Fl_Image *
FXButtonStyle::getImage(Fl_Shared_Image *pImage, int iw, int ih, bool desaturate)
		{
		if (m_pImage != NULL)
			{
			if (m_pImage->w() != iw || m_pImage->h() != ih)
				{
				delete m_pImage;
				m_pImage = NULL;
				}
			}

		if (m_pImage == NULL && m_pSharedImage != NULL)
			{
			m_pImage = m_pSharedImage->copy(iw, ih);
			if (desaturate)
				{
				m_pImage->desaturate();
				}
			}

		if (m_pImage == NULL && pImage != NULL)
			{
			m_pImage = pImage->copy(iw, ih);
			if (desaturate)
				{
				m_pImage->desaturate();
				}
			}

		return m_pImage;
		}

