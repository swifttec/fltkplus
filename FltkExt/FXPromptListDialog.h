/*
**	FXDialog.h	A generic dialog class
*/

#ifndef __FltkExt_FXPromptListDialog_h__
#define __FltkExt_FXPromptListDialog_h__

#include <FltkExt/FXDialog.h>


/**
*** A popup dialog window for entering a number
**/
class FLTKEXT_DLL_EXPORT FXPromptListDialog : public FXDialog
		{
public:
		FXPromptListDialog(const SWString &title, const SWString &prompt, const SWStringArray &values, Fl_Widget *pParent=NULL);
		virtual ~FXPromptListDialog();

		int				selected() const	{ return m_selected; }

		virtual void	onOK();

protected:
		int			m_selected;
		};


#endif // __FltkExt_FXPromptListDialog_h__
