/*
**	FXDatePicker.h	A generic dialog class
*/

#ifndef __FltkExt_FXDatePicker_h__
#define __FltkExt_FXDatePicker_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXDatePicker : public FXWidget
		{
public:
		class Field
			{
		public:
			int		type;
			FXRect	rect;
			SWString	value;
			bool	selected;
			};

public:
		FXDatePicker(int X, int Y, int W, int H);

		void			set(const SWDate &v)	{ m_date = v; }
		void			set(const SWTime &v)	{ m_date = v.toDate(); }
		void			get(SWDate &v) const	{ v = m_date; }


protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onFocus();

		/// Called to handle a shortcut
		virtual int		onUnfocus();

protected:
		void			init(const SWDate &dt);
		int				handleKey(int code, const SWString &text, const FXPoint &pt);

protected:
		SWDate				m_date;			///< The actual date
		FXFont				m_font;			///< The font to use
		SWArray<Field>	m_fields;
		FXColor				m_btnColor;
		FXColor				m_arrowColor;
		FXRect				m_rButton;
		FXColor				m_selectedBgColor;
		FXColor				m_selectedFgColor;
		int					m_inputValue;
		int					m_selectedField;
		};


#endif // __FltkExt_FXDatePicker_h__
