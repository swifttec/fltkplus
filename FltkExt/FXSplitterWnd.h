/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the definition of the FXSplitterWnd class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXSplitterWnd_h__
#define __FltkExt_FXSplitterWnd_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXSplitterBar.h>
#include <FltkExt/FXSplitterPane.h>

#include <FL/Fl_Group.H>

#include <swift/sw_registry.h>


class FLTKEXT_DLL_EXPORT FXSplitterWnd : public Fl_Group
		{
public:
		enum Flags
			{
			FLAG_VERTICAL		= 0x01,
			FLAG_LBUTTON_DOWN	= 0x02,
			FLAG_DRAGGING		= 0x04,
			FLAG_RESIZING		= 0x08,
			FLAG_MOUSEOVER		= 0x10,
			FLAG_LEFT_MOUSE		= 0x20,
			FLAG_MIDDLE_MOUSE	= 0x40,
			FLAG_RIGHT_MOUSE	= 0x80
			};

public:
		FXSplitterWnd(int x, int y, int w, int h, bool vertical);

		int					getPaneCount() const;
		int					getPaneIndex(Fl_Widget *v);
		FXSplitterPane *	getPane(int idx)					{ return &m_panes[idx]; }
		FXSplitterPane *	findPane(Fl_Widget *v);
		void				setPane(int idx, Fl_Widget *v);
		FXSplitterBar *		getBar(int idx)						{ return &m_bars[idx]; }
		void				adjustPanes()						{ movePanes(); }
		void				setPaneSize(int idx, int size);
		void				setPaneSize(int idx, int size, bool fixed);
		void				setPaneSizes(int idx, int prefsize, int minsize, int maxsize=0);

		void				addPane(Fl_Widget *v, int prefSize, int barWidth=3, int barPadding=1, const FXColor &fgBarColor=RGB(200, 200, 200), const FXColor &bgBarColor=RGB(150, 150, 150), bool fixedSize=false);
		void				clearPanes();

		bool				isHorizontalSplit() const			{ return ((m_flags & FLAG_VERTICAL) == 0); }
		bool				isVerticalSplit() const				{ return ((m_flags & FLAG_VERTICAL) != 0); }

		int					barIndex(const FXPoint &pt);
		void				barColor(COLORREF v)				{ m_barColor = v; }

		virtual FXSize		getMinSize() const;
		void				calcMinSize(FXSize &sz, int &panesize, int &ngrowable, int &nshrinkable) const;
				

//		virtual FXSize		getMinSize() const;

		bool				dragging() const		{ return isFlagSet(FLAG_DRAGGING); }
		void				dragging(bool v)		{ setFlag(FLAG_DRAGGING, v); }
		
		bool				mouseover() const		{ return isFlagSet(FLAG_MOUSEOVER); }
		void				mouseover(bool v)		{ setFlag(FLAG_MOUSEOVER, v); }

		bool				isFlagSet(int v) const	{ return ((m_flags & v) != 0); }
		void				setFlag(int f, bool v)	{ if (v) { m_flags |= f; } else { m_flags &= ~f; } }

		bool				canAdjustSize(int idx, int off, int step);
		bool				adjustSize(int idx, int off, int step);
		void				movePanes();


		/// Set the foreground / text color
		void			setBackgroundColor(const FXColor &v)	{ m_bgColor = v; }

		/// Get the background color
		const FXColor &	getBackgroundColor() const				{ return m_bgColor; }

		void			saveSizesToRegistry(SWRegistryHive reghive, const SWString &regkey, const SWString &param);
		void			loadSizesFromRegistry(SWRegistryHive reghive, const SWString &regkey, const SWString &param);

protected:
		int				paneSize(Fl_Widget *pWidget);

		/// Get the client rect for this widget
		virtual void	getClientRect(FXRect &rect);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is moved
		virtual int		onMouseMove(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onMiddleMouseButton(int action, const FXPoint &pt);

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onRightMouseButton(int action, const FXPoint &pt);

protected: // FLTK Overrides
		/// Handle the request to draw this widget
		virtual void	draw();

		/// Handle a resize event
		virtual void	resize(int X, int Y, int W, int H);

		/// Handle the given event
		virtual int		handle(int event);

protected:
		SWRegistryHive			m_reghive;
		SWString				m_regkey;
		int						m_flags;
		int						m_dragOffset;
		int						m_dragPos;
		int						m_dragBar;
		FXColor					m_barColor;
		FXColor					m_bgColor;
		SWArray<FXSplitterPane>	m_panes;
		SWArray<FXSplitterBar>	m_bars;
		int						m_lastEventCode;	///< The last event code received
		FXPoint					m_lastEventPoint;	///< The last event coordinates
		int						m_cursor;
		};

#endif // __FltkExt_FXSplitterWnd_h__
