/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the definition of the FXUniformLayout class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include <FltkExt/FXUniformLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>

FXUniformLayout::FXUniformLayout(bool vertical, int gap, int maxsize) :
	m_vertical(vertical),
	m_gap(gap),
	m_maxsize(maxsize)
		{
		}


FXSize
FXUniformLayout::getMinSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		sz=fx_getMinSize(pWidget);

			if (m_vertical)
				{
				res.cx = sw_math_max(res.cx, sz.cx);
				res.cy += sz.cy;
				}
			else
				{
				res.cx += sz.cx;
				res.cy = sw_math_max(res.cy, sz.cy);
				}
			}
	
		FXMargin	border;
		
		fx_getBorderWidths(m_pOwner, border);

		res.cx += m_padding.left + m_padding.right + border.left + border.right;
		res.cy += m_padding.top + m_padding.bottom + border.top + border.bottom;

		if (nchildren > 1)
			{
			if (m_vertical) res.cy += (nchildren-1) * m_gapBetweenChildren.cy;
			else res.cx += (nchildren-1) * m_gapBetweenChildren.cx;
			}

		return res;
		}


FXSize
FXUniformLayout::getMaxSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();
		bool	unlimitedWidth=false;
		bool	unlimitedHeight=false;

		for (int i=0;!(unlimitedWidth && unlimitedHeight)&&i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		sz=fx_getMaxSize(pWidget);

			if (sz.cx == 0) unlimitedWidth = true;
			else res.cx += sz.cx;

			if (sz.cy == 0) unlimitedHeight = true;
			else res.cy += sz.cy;
			}

		FXMargin	border;
		
		fx_getBorderWidths(m_pOwner, border);

		if (unlimitedWidth)
			{
			res.cx = 0;
			}
		else
			{
			res.cx += m_padding.left + m_padding.right + border.left + border.right;
			if (nchildren > 1) res.cx += (nchildren-1) * m_gapBetweenChildren.cx;
			}

		if (unlimitedHeight)
			{
			res.cy = 0;
			}
		else
			{
			res.cy += m_padding.top + m_padding.bottom + border.top + border.bottom;
			if (nchildren > 1) res.cy += (nchildren-1) * m_gapBetweenChildren.cy;
			}

		return res;
		}


void
FXUniformLayout::addWidget(Fl_Widget *pWidget)
		{
		m_pOwner->add(pWidget);
		}


void
FXUniformLayout::layoutChildren()
		{
		int			nchildren=m_pOwner->children();
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (nchildren > 0)
			{
			Fl_Widget	*const *a;
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalExtraSpace=0;
			FXSize		szChild;

			if (m_vertical)
				{
				szChild.cx = W;
				szChild.cy = (H - (m_gap * (nchildren-1))) / nchildren;
				if (szChild.cy > m_maxsize) szChild.cy = m_maxsize;
				}
			else
				{
				szChild.cy = H;
				szChild.cx = (W - (m_gap * (nchildren-1))) / nchildren;
				if (szChild.cx > m_maxsize) szChild.cy = m_maxsize;
				}

			int		xpos=0;
			int		ypos=0;

			a = m_pOwner->array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				o->resize(X+xpos, Y+ypos, szChild.cx, szChild.cy);

				if (m_vertical) ypos += o->h() + m_gap;
				else xpos += o->w() + m_gap;
				}
			}
		}
