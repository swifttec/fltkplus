#ifndef __FltkExt_FXTextStyle_h__
#define __FltkExt_FXTextStyle_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>

#include <swift/SWString.h>

class FLTKEXT_DLL_EXPORT FXTextStyle
		{
public:
		FXTextStyle();

		/// Set the text font
		void				setFont(const FXFont &v)								{ font = v; }

		/// Set the text color
		void				setTextColor(const FXColor &color)						{ fgColor = color; } 

		/// Set the text background color (opaque background mode only)
		void				setBackgroundColor(const FXColor &color)				{ bgColor = color; } 

		/// Set the text alignment
		void				setTextAlign(int v)										{ flags = (flags & ~FX_ALIGN_MASK) | (v & FX_ALIGN_MASK); }

		/// Set the text outline style
		void				setTextOutline(int width, const FXColor &color)			{ outlineWidth = width; outlineColor = color; } 

		/// Set the text shadow style
		void				setTextShadow(int xoff, int yoff, const FXColor &color)	{ shadowXOffset = xoff; shadowYOffset = yoff; shadowColor = color;}

public:
		FXFont		font;				///< The text font
		sw_uint32_t	flags;				///< The style flags (valign, halign, etc)
		FXColor		fgColor;			///< The text color
		FXColor		bgColor;			///< The text bg color (opaque mode only)
		FXColor		shadowColor;		///< The text shadow color
		int			shadowXOffset;		///< The text shadow X offset
		int			shadowYOffset;		///< The text shadow Y offset
		FXColor		outlineColor;		///< The text outline color
		int			outlineWidth;		///< The text outline width
		};


#endif // __FltkExt_FXTextStyle_h__
