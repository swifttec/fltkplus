#ifndef __FltkExt_FXColorFont_h__
#define __FltkExt_FXColorFont_h__

#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>

/**
*** A font class with color information
**/
class FLTKEXT_DLL_EXPORT FXColorFont : public FXFont
		{
public:
		// Constructors, destructors and operators
		FXColorFont();
		FXColorFont(const SWString &name, int size, int style=0, const FXColor &color=FXColor("#000"));
		FXColorFont(const SWString &name, double size, int style=0, const FXColor &color=FXColor("#000"));
		FXColorFont(const FXColorFont &other);
		virtual ~FXColorFont(void);

		// Assignment operators
		FXColorFont &		operator=(const FXColorFont &other);

		virtual void		select() const;

		void				color(const FXColor &v)		{ m_color = v; }
		const FXColor &		color() const				{ return m_color; }

private: // Members
		FXColor		m_color;
		};


#endif // __FltkExt_FXColorFont_h__
