#ifndef __FltkExt_FXButtonBar_h__
#define __FltkExt_FXButtonBar_h__

#include <FltkExt/FXSize.h>
#include <FltkExt/FXButton.h>
#include <FltkExt/FXImageButton.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXMinMaxSize.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>

#include <FL/Fl_Group.H>

class FLTKEXT_DLL_EXPORT FXButtonBar : public FXGroup
		{
public:
		FXButtonBar(int X, int Y, int W, int H, bool vertical, const FXMinMaxSize &mxButtonSizes=FXMinMaxSize(24, 24, 0, 0), int minGap=1, int maxGap=8, const FXPadding &barPadding=FXPadding(0, 0, 0, 0), const FXPadding &btnPadding=FXPadding(0, 0, 0, 0));
		FXButtonBar(int X, int Y, int W, int H, bool vertical, int defbtnsize, int gap, int btnpad);
		virtual ~FXButtonBar();

		FXButton *			addButton(int id, const SWString &text, const SWString &image, bool enabled=true);
		FXImageButton *		addImageButton(int id, const SWString &image, bool enabled=true);
		int					getButtonCount() const;
		void				addSeparator(int width);

		void				addItem(int itemid, Fl_Widget *pItem);
		Fl_Widget *			getItem(int itemid)												{ return fx_group_getItem(this, itemid); }
		FXButton *			getButton(int itemid)											{ return dynamic_cast<FXButton *>(getItem(itemid)); }

		void				enableItem(int itemid, bool v=true);
		void				enableAllItems(bool v);

		void				deselectAllItems();
		void				selectItem(int itemid, bool v=true);

		void				setBackgroundColor(const FXColor &v)	{ m_bgColor = v; }
		void				setPadding(const FXMargin &v)			{ m_padding = v; }
		void				setButtonAlignment(int v)				{ m_align = v; }

		virtual void		resize(int X, int Y, int W, int H);

		void				setDefaultButtonStyle(int idx, const FXButtonStyle &style);
		void				getDefaultButtonStyle(int idx, FXButtonStyle &style);
//		void				setDefaultButtonSize(int v)				{ m_defaultButtonSize = FXSize(v, v); }
		void				setMinMaxButtonSize(const FXMinMaxSize &sz)	{ m_minMaxButtonSize = sz; }

		void				enableKeys(bool v);

protected:
		static void			itemCallback(Fl_Widget *pWidget);
		virtual void		onCallback(Fl_Widget *pWidget);

		int					getSeparatorSpace() const;
		int					getSeparatorCount() const;

protected:
		FXMinMaxSize	m_minMaxButtonSize;
		bool			m_vertical;
		int				m_minGap;
		int				m_maxGap;
		FXPadding		m_padding;
		FXPadding		m_btnpadding;
		int				m_align;
		FXButtonStyle	m_defaultButtonStyles[FXButton::MaxStyle];
		bool			m_handleKeys;
		};

#endif // __FltkExt_FXButtonBar_h__
