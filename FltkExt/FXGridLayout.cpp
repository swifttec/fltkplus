#include <FltkExt/FXGridLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>


FXGridLayout::FXGridLayout(int ncols, int nrows, bool floatChildren) :
	FXLayout(0),
	m_nColumns(0),
	m_nRows(0),
	m_lastColumn(-1),
	m_lastRow(-1),
	m_floatChildren(floatChildren)
		{
		setDimensions(ncols, nrows);
		}


FXGridLayout::~FXGridLayout()
		{
		}


FXWidgetSizes				
FXGridLayout::getCellSizes(int col, int row, bool useCachedValues)
		{
		FXWidgetSizes	res;
		
		if (row >= 0 && row < m_nRows && col >= 0 && col < m_nColumns)
			{
			Cell			&cell=getCell(col, row);

			if (cell.pWidget != NULL)
				{
				if (!useCachedValues)
					{
					cell.sizes = fx_getWidgetSizes(cell.pWidget);
					}

				res = cell.sizes;
				}
			else if (!useCachedValues)
				{
				cell.sizes = FXWidgetSizes();
				}
			}

		return res;
		}


FXWidgetSizes				
FXGridLayout::getColumnSizes(int col, bool useCachedValues)
		{
		FXWidgetSizes	res;
		bool			unlimitedWidth=false;
		bool			unlimitedHeight=false;

		for (int row=0;row<m_nRows;row++)
			{
			int	width=0, height=0;

			Cell	&cell=getCell(col, row);

			if (cell.pWidget != NULL)
				{
				if (!useCachedValues) cell.sizes=fx_getWidgetSizes(cell.pWidget);

				FXWidgetSizes	&sizes=cell.sizes;

				res.currSize.cx = sw_math_max(res.currSize.cx, sizes.currSize.cx);
				res.currSize.cy += sizes.currSize.cy;

				res.minSize.cx = sw_math_max(res.minSize.cx, sizes.minSize.cx);
				res.minSize.cy += sizes.minSize.cy;

				if (sizes.maxSize.cx == 0)
					{
					unlimitedWidth = true;
					res.maxSize.cx = 0;
					}
				else if (!unlimitedWidth && !m_floatChildren)
					{
					res.maxSize.cx = sw_math_max(res.maxSize.cx, sizes.maxSize.cx);
					}

				if (sizes.maxSize.cy == 0)
					{
					unlimitedHeight = true;
					res.maxSize.cy = 0;
					}
				else if (!unlimitedHeight && !m_floatChildren)
					{
					res.maxSize.cy += res.maxSize.cy;
					}
				}
			}

		return res;
		}


FXWidgetSizes
FXGridLayout::getRowSizes(int row, bool useCachedValues)
		{
		FXWidgetSizes	res;
		bool			unlimitedWidth=false;
		bool			unlimitedHeight=false;

		for (int col=0;col<m_nColumns;col++)
			{
			int	width=0, height=0;

			Cell	&cell=getCell(col, row);

			if (cell.pWidget != NULL)
				{
				if (!useCachedValues) cell.sizes=fx_getWidgetSizes(cell.pWidget);

				FXWidgetSizes	&sizes=cell.sizes;

				res.currSize.cy = sw_math_max(res.currSize.cy, sizes.currSize.cy);
				res.currSize.cx += sizes.currSize.cx;

				res.minSize.cy = sw_math_max(res.minSize.cy, sizes.minSize.cy);
				res.minSize.cx += sizes.minSize.cx;

				if (sizes.maxSize.cx == 0)
					{
					unlimitedWidth = true;
					res.maxSize.cx = 0;
					}
				else if (!unlimitedWidth && !m_floatChildren)
					{
					res.maxSize.cx += res.maxSize.cx;
					}

				if (sizes.maxSize.cy == 0)
					{
					unlimitedHeight = true;
					res.maxSize.cy = 0;
					}
				else if (!unlimitedHeight && !m_floatChildren)
					{
					res.maxSize.cy = sw_math_max(res.maxSize.cy, sizes.maxSize.cy);
					}
				}
			}

		return res;
		}



FXSize
FXGridLayout::getMinSize() const
		{
		FXSize	res;

		for (int row=0;row<m_nRows;row++)
			{
			int	width=0, height=0;

			for (int col=0;col<m_nColumns;col++)
				{
				Cell	&cell=getCell(col, row);

				if (cell.pWidget != NULL)
					{
					FXSize	sz=fx_getMinSize(cell.pWidget);

					width += sz.cx;
					height = sw_math_max(height, sz.cy);
					}
				}
			
			res.cx = sw_math_max(width, res.cx);
			res.cy += height;
			}

		res.cx += m_padding.left + m_padding.right + ((m_nColumns-1) * m_gapBetweenChildren.cx);
		res.cy += m_padding.top + m_padding.bottom + ((m_nRows-1) * m_gapBetweenChildren.cy);

		return res;
		}


FXSize
FXGridLayout::getMaxSize() const
		{
		FXSize	res;

		if (!m_floatChildren)
			{
			bool	unlimitedWidth=false;
			bool	unlimitedHeight=false;

			for (int row=0;!(unlimitedWidth && unlimitedHeight)&&row<m_nRows;row++)
				{
				int	width=0, height=0;

				for (int col=0;!(unlimitedWidth && unlimitedHeight)&&col<m_nColumns;col++)
					{
					Cell	&cell=getCell(col, row);

					if (cell.pWidget != NULL)
						{
						FXSize	sz=fx_getMaxSize(cell.pWidget);

						if (sz.cx == 0) unlimitedWidth = true;
						else width += sz.cx;

						if (sz.cy == 0) unlimitedHeight = true;
						height = sw_math_max(height, sz.cy);
						}
					}
			
				res.cx = sw_math_max(width, res.cx);
				res.cy += height;
				}


			if (unlimitedWidth) res.cx = 0;
			if (unlimitedHeight) res.cy = 0;
			}

		return res;
		}

	
void
FXGridLayout::insertRows(int idx, int count)
		{
		if (count > 0 && idx >= 0 && idx <= m_nRows)
			{
			Cell	cell;
			int		cellcount=m_nColumns * count;

			for (int i=0;i<cellcount;i++)
				{
				m_cells.insert(cell, (idx * m_nColumns) + i);
				}

			m_nRows += count;
			}
		}

	
void
FXGridLayout::removeRows(int idx, int count)
		{
		if (idx >= 0 && idx < m_nRows)
			{
			if ((idx + count) > m_nRows)
				{
				count = m_nRows - idx;
				}

			if (count > 0)
				{
				// Clear the widgets
				for (int i=0;i<m_nColumns;i++)
					{
					for (int j=0;j<count;j++)
						{
						setWidgetAt(NULL, 0, i, idx+j);
						}
					}

				// Remove the cells
				m_cells.remove(idx * m_nColumns, m_nColumns * count);

				m_nRows -= count;
				}
			}
		}

	
void
FXGridLayout::insertColumns(int idx, int count)
		{
		if (count > 0 && idx >= 0 && idx <= m_nColumns)
			{
			Cell	cell;
			Column	column;

			// Insert the cells by working backwards through the cells and inserting cells as we go
			for (int i=m_nRows-1;i>=0;i--)
				{
				for (int j=0;j<count;j++)
					{
					m_cells.insert(cell, (i * m_nColumns) + idx + j);
					}
				}

			for (int j=0;j<count;j++)
				{
				m_columns.insert(column, idx+j);
				}

			m_nColumns += count;
			}
		}


void
FXGridLayout::removeColumns(int idx, int count)
		{
		if (idx >= 0 && idx < m_nColumns)
			{
			if ((idx + count) > m_nColumns)
				{
				count = m_nColumns - idx;
				}

			if (count > 0)
				{
				// Clear the widgets in the column
				for (int i=0;i<m_nRows;i++)
					{
					for (int j=0;j<count;j++)
						{
						setWidgetAt(NULL, 0, idx+j, i);
						}
					}

				// Remove the cells by working backwards through the cells and removing each entry
				for (int i=m_nRows-1;i>=0;i--)
					{
					m_cells.remove((i * m_nColumns) + idx, count);
					}

				// Remove the column entry
				m_columns.remove(idx, count);

				m_nColumns -= count;
				}
			}
		}


void
FXGridLayout::setDimensions(int ncols, int nrows)
		{
		// Remove the excess rows
		if (m_nRows > nrows)
			{
			removeRows(nrows, m_nRows-nrows);
			}

		// Remove the excess columns
		if (m_nColumns > ncols)
			{
			removeColumns(ncols, m_nColumns-ncols);
			}

		// Add the extra columns
		if (m_nColumns < ncols)
			{
			insertColumns(m_nColumns, ncols-m_nColumns);
			}

		// Add the extra columns
		if (m_nRows < nrows)
			{
			insertRows(m_nRows, nrows-m_nRows);
			}
		}




void
FXGridLayout::addWidget(Fl_Widget *pWidget)
		{
		addWidget(pWidget, 0);
		}


void
FXGridLayout::addWidget(Fl_Widget *pWidget, int align)
		{
		int		colidx=m_lastColumn;
		int		rowidx=m_lastRow;

		if (colidx < 0) colidx = 0;
		if (rowidx < 0) rowidx = 0;

		bool	done=false;

		while (!done)
			{
			if (colidx >= m_nColumns)
				{
				colidx = 0;
				rowidx++;
				}
			
			if (rowidx >= m_nRows)
				{
				// No where left to go!
				break;
				}

			if (getCell(colidx, rowidx).pWidget == NULL)
				{
				setWidgetAt(pWidget, align, colidx, rowidx);
				done = true;
				}
			else
				{
				colidx++;
				}
			}
		}



void
FXGridLayout::setWidget(Fl_Widget *pWidget, int align, int colidx, int rowidx)
		{
		if (rowidx >= 0 && rowidx < m_nRows && colidx >= 0 && colidx < m_nColumns)
			{
			setWidgetAt(pWidget, align, colidx, rowidx);
			m_lastColumn = colidx;
			m_lastRow = rowidx;
			}
		}


void
FXGridLayout::setWidgetAt(Fl_Widget *pWidget, int align, int colidx, int rowidx)
		{
		Cell	&cell=getCell(colidx, rowidx);

		if (cell.pWidget != NULL && cell.pWidget != pWidget)
			{
			m_pOwner->remove(cell.pWidget);
			cell.pWidget = NULL;
			}

		if (pWidget != NULL)
			{
			cell.pWidget = pWidget;
			m_pOwner->add(cell.pWidget);
			}

		cell.align = align;
		}



FXGridLayout::Cell &
FXGridLayout::getCell(int colidx, int rowidx) const
		{
		return m_cells.get((rowidx * m_nColumns) + colidx);
		}



void
FXGridLayout::layoutChildren()
		{
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (m_nColumns > 0 && m_nRows > 0)
			{
			// Cache the sizes for all cells
			for (int i=0;i<(int)m_cells.size();i++)
				{
				Cell	&cell=m_cells[i];

				cell.sizes = fx_getWidgetSizes(cell.pWidget);
				}

			SWArray<FXWidgetSizes>		rowSizes, colSizes;
			int							unlimitedRows=0, unlimitedCols=0;
			SWIntegerArray				rowHeights, colWidths;

			// Get the sizes for rows and columns
			for (int i=0;i<m_nColumns;i++)
				{
				colSizes[i] = getColumnSizes(i, true);
				if (colSizes[i].maxSize.cx == 0) unlimitedCols++;
				}

			for (int i=0;i<m_nRows;i++)
				{
				rowSizes[i] = getRowSizes(i, true);
				if (rowSizes[i].maxSize.cy == 0) unlimitedRows++;
				}

			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			canchange=0;
			SWFlags		changeflags;

			// Re-size the columns
			ds = cds = 0;
			totalChildSpace = 0;
			canchange = 0;
			changeflags.clear();

			for (int i=0;i<m_nColumns;i++)
				{
				colWidths[i] = colSizes[i].currSize.cx;
				totalChildSpace += colWidths[i];
				}

			ds = W - totalChildSpace;


			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				for (int i=0;i<m_nColumns;i++)
					{
					int		width=colWidths[i];

					if (pass == 0 || changeflags.getBit(i))
						{

						if (ds != 0)
							{
							if (pass == 0)
								{
								cds = ds / (m_nColumns - i);
								}
							else
								{
								cds = ds / canchange;
								}

							if (cds == 0)
								{
								if (ds < 0) cds = -1;
								else cds = 1;
								}
							}

						// Variable width
						int		old=width;

						width += cds;
						if (width < colSizes[i].minSize.cx) width = colSizes[i].minSize.cx;
						if (colSizes[i].maxSize.cx != 0 && width > colSizes[i].maxSize.cx) width = colSizes[i].maxSize.cx;
						else if (pass == 0 && (colSizes[i].maxSize.cx == 0 || width < colSizes[i].maxSize.cx))
							{
							canchange++;
							changeflags.setBit(i);
							}
						
						ds -= width-old;
					
						if (pass == 1) canchange--;
						}
					
					colWidths[i] = width;
					}
				}


			// Resize the rows
			ds = cds = 0;
			totalChildSpace = 0;
			canchange = 0;
			changeflags.clear();

			for (int i=0;i<m_nRows;i++)
				{
				rowHeights[i] = rowSizes[i].currSize.cy;
				totalChildSpace += rowHeights[i];
				}

			ds = H - totalChildSpace;

			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				for (int i=0;i<m_nRows;i++)
					{
					int		height=rowHeights[i];

					if (pass == 0 || changeflags.getBit(i))
						{
						if (ds != 0)
							{
							if (pass == 0)
								{
								cds = ds / (m_nRows - i);
								}
							else
								{
								cds = ds / canchange;
								}

							if (cds == 0)
								{
								if (ds < 0) cds = -1;
								else cds = 1;
								}
							}

						// Variable width
						int		old=height;

						height += cds;
						if (height < rowSizes[i].minSize.cy) height = rowSizes[i].minSize.cy;
						if (rowSizes[i].maxSize.cy != 0 && height > rowSizes[i].maxSize.cy) height = rowSizes[i].maxSize.cy;
						else if (pass == 0 && (rowSizes[i].maxSize.cy == 0 || height < rowSizes[i].maxSize.cy))
							{
							canchange++;
							changeflags.setBit(i);
							}
						
						ds -= height-old;
					
						if (pass == 1) canchange--;
						}
					
					rowHeights[i] = height;
					}
				}

			
			// Finally resize all of the widgets
			int		ypos=0;

			for (int row=0;row<m_nRows;row++)
				{
				int		xpos=0;

				for (int col=0;col<m_nColumns;col++)
					{
					Cell	&cell=getCell(col, row);

					if (cell.pWidget != NULL)
						{
						int		cx=xpos, cy=ypos, cw=colWidths[col], ch=rowHeights[row];
						int		mw=cell.sizes.maxSize.cx;

						if (mw != 0 && cw > mw)
							{
							switch (cell.align & FX_HALIGN_MASK)
								{
								case FX_HALIGN_LEFT:
									// Do nothing
									break;

								case FX_HALIGN_RIGHT:
									cx += (cw-mw);
									break;
								
								default:
									cx += (cw-mw)/2;
									break;
								}

							cw = mw;
							}

						int		mh=cell.sizes.maxSize.cy;

						if (mh != 0 && ch > mh)
							{
							switch (cell.align & FX_VALIGN_MASK)
								{
								case FX_VALIGN_TOP:
									// Do nothing
									break;

								case FX_VALIGN_BOTTOM:
									cy += (ch-mh);
									break;

								default:
									cy += (ch-mh)/2;
									break;
								}

							ch = mh;
							}

						cell.pWidget->resize(X+cx, Y+cy, cw, ch);
						}

					xpos += colWidths[col] + m_gapBetweenChildren.cx;
					}
				
				ypos += rowHeights[row] + m_gapBetweenChildren.cy;
				}
			}
		}

