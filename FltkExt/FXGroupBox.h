/*
**	FXGroupBox.h	A generic dialog class
*/

#ifndef __FltkExt_FXGroupBox_h__
#define __FltkExt_FXGroupBox_h__

#include <FltkExt/FXWidget.h>

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXGroupBox : public FXWidget
		{
public:
		FXGroupBox(int X, int Y, int W, int H, const SWString &title, const FXColor &bgcolor=FX_COLOR_NONE);

public: /// FXWidget overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

protected:
		SWString	m_title;
		FXFont		m_font;
		};


#endif // __FltkExt_FXGroupBox_h__
