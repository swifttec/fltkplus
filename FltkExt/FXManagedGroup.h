#ifndef __FltkExt_FXManagedGroup_h__
#define __FltkExt_FXManagedGroup_h__

#include <FltkExt/FltkExt.h>
#include <FL/Fl_Group.H>

class FLTKEXT_DLL_EXPORT FXManagedGroup : public Fl_Group
		{
public:
		class Sizes
			{
		public:
			int		minsize;	// The minimum size
			int		maxsize;	// The maximum size (if zero no limit)
			};

public:
		FXManagedGroup(int x, int y, int w, int h, bool vertical);
		virtual ~FXManagedGroup();

				
		uchar horizontal() const {return type();}
		
		virtual void	draw();
		virtual void	resize(int X, int Y, int W, int H);

		void			setChildSizes(int idx, int minsize, int maxsize);
		Sizes &			getChildSizes(int idx);

protected:
		SWArray<Sizes>	m_childSizes;
		bool			m_vertical;
		};

#endif // __FltkExt_FXManagedGroup_h__
