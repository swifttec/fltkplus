/**
*** @file		FXMenuItem.h
*** @brief		A generic menu item class
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXMenuItem_h__
#define __FltkExt_FXMenuItem_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXRect.h>

#include <FL/Fl_Widget.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl.H>

class FXMenu;

class FLTKEXT_DLL_EXPORT FXMenuItem
		{
friend class FXMenu;

public:
		enum Flags
			{
			Disabled		= 0x01,		///< Item is disabled
			CheckBox		= 0x02,		///< Item is a checkbox toggle (shows checkbox for on/off state)
			Checked			= 0x04,		///< The on/off state for checkbox/radio buttons (if set, state is 'on')
			Radio			= 0x08,		///< Item is a radio button (one checkbox of many can be on)
			Selected		= 0x10,		///< Item is selected by mouse position or keyboard
			SeparatorAfter	= 0x20,		///< Creates divider line below this item. Also ends a group of radio buttons.
			// Invisible		= 0x40		///< Item will not show up (shortcut will work)
			};

public:
		FXMenuItem();
		FXMenuItem(const FXMenuItem &item);
		FXMenuItem(sw_uint32_t id, const SWString &text, sw_uint32_t flags=0);
		virtual ~FXMenuItem();

		FXMenuItem &		operator=(const FXMenuItem &item);

		void				clear();

		sw_uint32_t			id() const										{ return m_id; }
		void				id(sw_uint32_t v)								{ m_id = v; }

		void				text(const SWString &v)							{ m_text = v; }
		const SWString &	text() const									{ return m_text; }

		sw_uint32_t			flags() const									{ return m_flags; }
		void				flags(sw_uint32_t v)							{ m_flags = v; }
		void				setFlags(sw_uint32_t flags, bool v=true)		{ if (v) m_flags |= flags; else m_flags &= ~flags; }
		void				clearFlags(sw_uint32_t v)						{ m_flags &= ~v; }
		bool				isFlagSet(sw_uint32_t flags) const				{ return (m_flags & flags) != 0; }

		void				setFltkMenuItem(void *p)						{ m_pFltkMenuItem = p; }
		void *				getFltkMenuItem()								{ return m_pFltkMenuItem; }

		/// Return the owning menu if any
		FXMenu *			owner() const									{ return m_pOwner; }

		/// Return a boolean indicating if this item has a submenu
		bool				hasSubMenu() const								{ return (m_pSubMenu != NULL); }

		/// Create and return a submenu for this item or NULL if none
		FXMenu *			createSubMenu();

		/// Return the submenu for this item or NULL if none
		FXMenu *			submenu() const									{ return m_pSubMenu; }

		/** Gets whether or not the item can be picked. */
		int					active() const									{ return !(m_flags & FL_MENU_INACTIVE); }

		/** Allows a menu item to be picked. */
		void				activate()										{ m_flags &= ~FL_MENU_INACTIVE; }

		/**
		Prevents a menu item from being picked. Note that this will also cause
		the menu item to appear grayed-out.
		*/
		void				deactivate() {m_flags |= FL_MENU_INACTIVE;}

		/** Returns non 0 if FL_INACTIVE and FL_INVISIBLE are cleared, 0 otherwise. */
		int					activevisible() const {return !(m_flags & (FL_MENU_INACTIVE|FL_MENU_INVISIBLE));}

		/**
		Returns true if a checkbox will be drawn next to this item.
		This is true if FL_MENU_TOGGLE or FL_MENU_RADIO is set in the flags.
		*/
		int					checkbox() const								{ return m_flags&FL_MENU_TOGGLE; }
		
		/**
		Returns true if this item is a radio item.
		When a radio button is selected all "adjacent" radio buttons are
		turned off.  A set of radio items is delimited by an item that has
		radio() false, or by an item with FL_MENU_DIVIDER turned on.
		*/
		int					radio() const									{ return m_flags&FL_MENU_RADIO; }
		
		/** Returns the current value of the check or radio item.
		This is zero (0) if the menu item is not checked and
		non-zero otherwise. You should not rely on a particular value,
		only zero or non-zero.
		\note The returned value for a checked menu item as of FLTK 1.3.2
		is FL_MENU_VALUE (4), but may be 1 in a future version.
		*/
		int					value() const									{ return m_flags&FL_MENU_VALUE; }

		/** Gets what key combination shortcut will trigger the menu item. */
		sw_uint32_t			shortcut() const								{ return m_shortcut; }

		/**
		Sets exactly what key combination will trigger the menu item.  The
		value is a logical 'or' of a key and a set of shift flags, for instance 
		FL_ALT+'a' or FL_ALT+FL_F+10 or just 'a'.  A value of
		zero disables the shortcut.

		The key can be any value returned by Fl::event_key(), but will usually 
		be an ASCII letter. Use a lower-case letter unless you require the shift 
		key to be held down.

		The shift flags can be any set of values accepted by Fl::event_state().
		If the bit is on that shift key must be pushed.  Meta, Alt, Ctrl, 
		and Shift must be off if they are not in the shift flags (zero for the 
		other bits indicates a "don't care" setting).
		*/
		void				shortcut(sw_uint32_t s)							{ m_shortcut = s; }

		/**
		Returns the menu item's labeltype.
		A labeltype identifies a routine that draws the label of the
		widget.  This can be used for special effects such as emboss, or to use
		the label() pointer as another form of data such as a bitmap.
		The value FL_NORMAL_LABEL prints the label as text.
		*/
		Fl_Labeltype		labeltype() const								{ return (Fl_Labeltype)m_labeltype; }

		/**
		Sets the menu item's labeltype.
		A labeltype identifies a routine that draws the label of the
		widget.  This can be used for special effects such as emboss, or to use
		the label() pointer as another form of data such as a bitmap.
		The value FL_NORMAL_LABEL prints the label as text.
		*/
		void				labeltype(Fl_Labeltype a)						{ m_labeltype = a; }
		/**
		Returns the callback function that is set for the menu item.
		Each item has space for a callback function and an argument for that
		function. Due to back compatibility, the FXMenuBarItem itself
		is not passed to the callback, instead you have to get it by calling
		((FXMenuBar*)w)->mvalue() where w is the widget argument.
		*/
		Fl_Callback *		callback() const {return m_pCallback;}

		/**
		Sets the menu item's callback function and userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void				callback(Fl_Callback* c, void* p) {m_pCallback=c; m_pUserData=p;}

		/**
		Sets the menu item's callback function.
		This method does not set the userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void				callback(Fl_Callback* c) {m_pCallback=c;}

		/**
		Sets the menu item's callback function.
		This method does not set the userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void				callback(Fl_Callback0*c) {m_pCallback=(Fl_Callback*)c;}

		/**
		Sets the menu item's callback function and userdata() argument.
		This method does not set the userdata() argument.
		The argument \p is cast to void* and stored as the userdata()
		for the menu item's callback function.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void				callback(Fl_Callback1*c, long p=0) {m_pCallback=(Fl_Callback*)c; m_pUserData=(void*)(fl_intptr_t)p;}

		/**
		Gets the user_data() argument that is sent to the callback function.
		*/
		void *				user_data() const {return m_pUserData;}
		/**
		Sets the user_data() argument that is sent to the callback function.
		*/
		void				user_data(void* v) {m_pUserData = v;}
		/**
		Gets the user_data() argument that is sent to the callback function.
		For convenience you can also define the callback as taking a long
		argument.  This method casts the stored userdata() argument to long
		and returns it as a \e long value.
		*/
		long				argument() const {return (long)(fl_intptr_t)m_pUserData;}
		/**
		Sets the user_data() argument that is sent to the callback function.
		For convenience you can also define the callback as taking a long
		argument.  This method casts the given argument \p v to void*
		and stores it in the menu item's userdata() member.
		This may not be portable to some machines.
		*/
		void				argument(long v)					{m_pUserData = (void*)(fl_intptr_t)v;}

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		The callback is called with the stored user_data() as its second argument.
		You must first check that callback() is non-zero before calling this.
		*/
		void				do_callback(Fl_Widget* o) const;

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		This call overrides the callback's second argument with the given value \p arg.
		You must first check that callback() is non-zero before calling this.
		*/
		void				do_callback(Fl_Widget* o,void* arg) const;

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		This call overrides the callback's second argument with the
		given value \p arg. long \p arg is cast to void* when calling
		the callback.
		You must first check that callback() is non-zero before calling this.
		*/
		void				do_callback(Fl_Widget* o,long arg) const;

		/** Gets the visibility of an item. */
		int					visible() const {return !(m_flags & FL_MENU_INVISIBLE);}

		/** Makes an item visible in the menu. */
		void				show() { m_flags &= ~FL_MENU_INVISIBLE;}

		/** Hides an item in the menu. */
		void				hide() { m_flags |= FL_MENU_INVISIBLE;}

		FXSize				measure(const FXMenu *m) const;
		void				draw(int x, int y, int w, int h, const FXMenu *m);

		/// Get the font for this item
		FXFont *			font() const;

		/// Get the text color for this item
		FXColor *			textColor() const;

		/// Get the background color for this item
		FXColor *			backgroundColor() const;

		/// Get the background color for this item
		FXColor *			selectedBackgroundColor() const;

		/// Return the path for this item
		SWString			path() const;

		int					labelsize() const		{ return font()->height(); }
		int					labelfont() const		{ return font()->fl_face(); }
		Fl_Color			color() const			{ return backgroundColor()->to_fl_color(); }
		const FXRect &		lastDrawnRect() const	{ return m_lastDrawnRect; }

protected:
		void				init(sw_uint32_t id, const SWString &text, sw_uint32_t flags);

protected:
		FXMenu			*m_pOwner;				///< A pointer to the menu which owns this item (may be NULL)
		sw_uint32_t		m_flags;				///< menu item flags like FL_MENU_TOGGLE, FL_MENU_RADIO
		sw_uint32_t		m_id;					///< The menu item ID
		sw_uint32_t		m_shortcut;				///< The menu item shortcut
		SWString		m_text;					///< menu item text, returned by label()
		Fl_Callback		*m_pCallback;			///< menu item callback
		void			*m_pUserData;			///< menu item user_data for the menu's callback
		uchar			m_labeltype;			///< how the menu item text looks like
		FXFont			*m_pFont;				///< which font for this menu item text
		FXColor			*m_pTextColor;			///< which color text for this menu item text
		FXColor			*m_pBackgroundColor;	///< The background color for this item
		FXColor			*m_pSelectedBackgroundColor;	///< The background color for this item
		FXMenu			*m_pSubMenu;			///< A pointer to the submenu (may be NULL)
		FXRect			m_lastDrawnRect;

		/// Used by FXPopupMenu but to be phased out
		void		*m_pFltkMenuItem;
		};

#endif // __FltkExt_FXMenuItem_h__