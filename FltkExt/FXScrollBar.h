/*
**	FXScrollBar.h	A generic dialog class
*/

#ifndef __FltkExt_FXScrollBar_h__
#define __FltkExt_FXScrollBar_h__

#include <FltkExt/FXWidget.h>
#include <FltkExt/FXScrollInfo.h>

/**
*** A common scroll bar object
**/
class FLTKEXT_DLL_EXPORT FXScrollBarObject
		{
public:
		FXScrollBarObject(bool vertical, Fl_Widget *pNotify=NULL);

		void	setScrollInfo(const FXScrollInfo &info)		{ m_scrollInfo = info; }
		void	getScrollInfo(FXScrollInfo &info) const		{ info = m_scrollInfo; }

		void	scrollRange(int from, int to)				{ m_scrollInfo.setRange(from, to); }
		void	scrollRange(int from, int to, int page)		{ m_scrollInfo.setRange(from, to, page); }

		void	scrollPosition(int pos)						{ m_scrollInfo.position(pos); }
		int		scrollPosition() const						{ return m_scrollInfo.position(); }

		void	scrollLineSize(int v)						{ m_scrollInfo.setLineSize(v); }

		int		scrollPageSize() const						{ return m_scrollInfo.pageSize(); }
		void	scrollPageSize(int v)						{ m_scrollInfo.setPageSize(v); }

		void	scrollChangePosition(int v)					{ m_scrollInfo.changePosition(v); }

		void	scrollMaxPos(int v)							{ m_scrollInfo.maxPos(v); }
		int		scrollMaxPos() const						{ return m_scrollInfo.maxPos(); }

		void	setScrollRange(int from, int to)			{ m_scrollInfo.setRange(from, to); }
		void	setScrollRange(int from, int to, int page)	{ m_scrollInfo.setRange(from, to, page); }
		void	setScrollPosition(int pos)					{ m_scrollInfo.position(pos); }
		void	setScrollLineSize(int v)					{ m_scrollInfo.setLineSize(v); }

public:

		/// Called to draw the widget which by default does nothing
		void	onDraw(const FXRect &cr);

		/// Called when a mouse button is pressed, return non-zero to track
		int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		bool	onTimer(sw_uint32_t id, void *data);

		bool	contains(const FXPoint &pt)		{ return m_rect.contains(pt); }

protected:
		void	drawButton(int X, int Y, int W, int H, int style, FXRect &br, bool selected);
		void	doNotify(int action);

protected:
		bool			m_vertical;
		FXColor			m_btnColor;
		FXColor			m_arrowColor;
		FXScrollInfo	m_scrollInfo;
		FXRect			m_rect;
		FXRect			m_btnRect[3];	// 0=Top/Left, 1=Bottom/Right, 2=Slider
		FXRect			m_gapRect[2];
		int				m_btnIndex;
		FXPoint			m_dragStartPoint;
		int				m_dragStartPosition;
		int				m_barSize;
		Fl_Widget		*m_pNotifyWidget;
		};

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXScrollBar : public FXWidget
		{
public:
		FXScrollBar(int X, int Y, int W, int H, bool vertical, Fl_Widget *pNotify=NULL);

		void	setScrollRange(int from, int to, int page)	{ m_object.setScrollRange(from, to, page); }
		void	setScrollPosition(int pos)					{ m_object.setScrollPosition(pos); }

		void	setScrollInfo(const FXScrollInfo &info)		{ m_object.setScrollInfo(info); }
		void	getScrollInfo(FXScrollInfo &info)			{ m_object.getScrollInfo(info); }

protected: /// FXWidget overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);

protected:
		void	drawButton(int X, int Y, int W, int H, int style, FXRect &br, bool selected);
		void	doNotify(int action);

protected:
		FXScrollBarObject	m_object;
		sw_uint32_t			m_idTimer;
		};


#endif // __FltkExt_FXScrollBar_h__
