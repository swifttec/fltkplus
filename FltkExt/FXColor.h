#ifndef __FltkExt_FXColor_h__
#define __FltkExt_FXColor_h__

#include <FltkExt/FltkExt.h>

#if !defined(SW_PLATFORM_WINDOWS)
	typedef sw_uint32_t	COLORREF;
		
	#define GetRValue(rgb)      (rgb & 0xff)
	#define GetGValue(rgb)      ((rgb >> 8) & 0xff)
	#define GetBValue(rgb)      ((rgb >> 16) & 0xff)

	#define RGB(r,g,b)          ((COLORREF)(((sw_uint32_t)r & 0xff)|(((sw_uint32_t)g & 0xff) << 8)|(((sw_uint32_t)b & 0xff) << 16)))
#endif

/**
*** A class to represent a color
**/
class FLTKEXT_DLL_EXPORT FXColor
		{
public:
		// Constructors, destructors and operators
		FXColor();
		FXColor(const FXColor &other);
		FXColor(COLORREF c);
		FXColor(int r, int g, int b, int a=~0);
		FXColor(int bitsPerComponent, int r, int g, int b, int a);
		FXColor(const char *name);
		FXColor(const wchar_t *name);
		FXColor(const SWString &name);
		FXColor(const SWString &name, const FXColor &defaultColor);

		FXColor(int flags, int bitsPerComponent, int r, int g, int b, int a);

		// operators
		bool		operator==(const FXColor &other) const;
		bool		operator!=(const FXColor &other) const;

		FXColor &	operator=(const FXColor &other);
		FXColor &	operator|=(const FXColor &other);
		FXColor &	operator=(COLORREF colorref);
		FXColor &	operator|=(COLORREF colorref);

		void		clear();
		
		void		set(int bitsPerComponent, int r, int g, int b, int a);
		void		set(int r, int g, int b, int a);
		void		set(COLORREF color);
		void		set(const SWString &name, const FXColor &defaultColor=FXColor());
		void		set(const FXColor &other);

		int			red() const				{ return m_red; }
		void		red(int v)				{ m_red = v & m_mask; }

		int			green() const			{ return m_green; }
		void		green(int v)			{ m_green = v & m_mask; }

		int			blue() const			{ return m_blue; }
		void		blue(int v)				{ m_blue = v & m_mask; }

		int			alpha() const			{ return m_alpha; }
		void		alpha(int v)			{ m_alpha = v & m_mask; }

		void		getComponents(int nbits, int &r, int &g, int &b) const;
		void		getComponents(int nbits, int &r, int &g, int &b, int &a) const;

		SWString	name(bool webonly=true) const;

		FXColor		toMonochrome() const;
		FXColor		toGrayscale() const;
		FXColor		toOpposite() const;
		FXColor		toShade(double factor) const;

		int			bits() const;

		void		select() const;
		int			to_fl_color() const;
		COLORREF	toCOLORREF() const;
		SWString	toString(bool webonly=true) const	{ return name(webonly); }
		sw_uint32_t	toRGBA32() const;
		sw_uint32_t	toRGB32() const;

		operator sw_int32_t() const						{ return (sw_int32_t)toRGBA32(); }
		operator sw_uint32_t() const					{ return toRGBA32(); }


public: // Static methods
		static FXColor		getColorByName(const SWString &name, const FXColor &defaultColor=FXColor());
		static SWString		getColorName(const FXColor &color, bool webonly=true);
		static FXColor		getGrayscale(const FXColor &cr);
		static FXColor		getShade(const FXColor &cr, double factor);
		static FXColor		fromFlColor(unsigned int v);
		static sw_uint32_t	rgba32(sw_byte_t r, sw_byte_t g, sw_byte_t b, sw_byte_t a);

//		bool		SelectColor(COLORREF &color, CWnd *pParent=NULL);

protected: // Members
		sw_uint16_t		m_red;		// The red component
		sw_uint16_t		m_green;	// The green component
		sw_uint16_t		m_blue;		// The blue component
		sw_uint16_t		m_alpha;	// The alpha component (0=transparent)
		sw_uint16_t		m_mask;		// The component mask
		sw_uint16_t		m_flags;	// The flags

public: // Constants
		static const FXColor NoColor;
		static const FXColor DefaultColor;
		};

#define FX_COLOR_NONE		FXColor::NoColor
#define FX_COLOR_DEFAULT	FXColor::DefaultColor

#endif // __FltkExt_FXColor_h__
