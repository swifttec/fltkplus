#include "FXFolderDialog.h"



FXFolderDialog::FXFolderDialog(const SWString &folder, const SWString &stitle) :
	Fl_Native_File_Chooser(),
	m_folder(folder),
	m_title(stitle)
		{
		type(BROWSE_DIRECTORY);
		options(NEW_FOLDER);
		directory(folder);
		title(m_title);
		}


FXFolderDialog::~FXFolderDialog()
		{
		}


int
FXFolderDialog::doModal()
		{
		return (show() == 0?FX_IDOK:FX_IDCANCEL);
		}