#include "FXRect.h"

FXRect::FXRect() :
	left(0),
	top(0),
	right(0),
	bottom(0)
		{
		}


FXRect::FXRect(int vleft, int vtop, int vright, int vbottom) :
	left(vleft),
	top(vtop),
	right(vright),
	bottom(vbottom)
		{
		}


FXRect::FXRect(const FXPoint &pt, const FXSize &sz) :
	left(pt.x),
	top(pt.y),
	right(pt.x+sz.cx-1),
	bottom(pt.y+sz.cy-1)
		{
		}

		
bool
FXRect::overlaps(const FXRect &r) const
		{
		return (
				contains(r.left, r.top) || contains(r.right, r.top) 
				|| contains(r.left, r.bottom) || contains(r.right, r.bottom)
				|| r.contains(left, top) || r.contains(right, top) 
				|| r.contains(left, bottom) || r.contains(right, bottom)
				);
		}
		
bool
FXRect::contains(const FXPoint &pt) const
		{
		return contains(pt.x, pt.y);
		}

		
bool
FXRect::PtInRect(const FXPoint &pt) const
		{
		return contains(pt.x, pt.y);
		}
		
bool
FXRect::contains(int x, int y) const
		{
		bool	inx, iny;

		inx = ((x >= left && x <= right) || (x >= right && x <= left));
		iny = ((y >= top && y <= bottom) || (y >= bottom && y <= top));

		return (inx && iny);
		}

void
FXRect::setXYWH(int x, int y, int w, int h)
		{
		left = x;
		top = y;
		right = x + w - 1;
		bottom = y + h - 1;
		}


void
FXRect::getXYWH(int &x, int &y, int &w, int &h)
		{
		x = left;
		y = top;
		w = width();
		h = height();
		}


int
FXRect::width() const
		{
		int	res=0;

		if (left < right)
			{
			res = right - left + 1;
			}
		else
			{
			res = left - right + 1;
			}
		
		return res;
		}



int
FXRect::height() const
		{
		int	res=0;

		if (top < bottom)
			{
			res = bottom - top + 1;
			}
		else
			{
			res = top - bottom + 1;
			}
		
		return res;
		}


FXSize
FXRect::size() const
		{
		return FXSize(width(), height());
		}


FXPoint
FXRect::point() const
		{
		return FXPoint(left, top);
		}


FXPoint
FXRect::center() const
		{
		return FXPoint(left + (width()/2), top + (height()/2));
		}

void
FXRect::inflate(int l, int t, int r, int b)
		{
		left -= l;
		top -= t;
		right += r;
		bottom +=b;
		}

void
FXRect::deflate(int l, int t, int r, int b)
		{
		left += l;
		top += t;
		right -= r;
		bottom -=b;
		}


void
FXRect::moveBy(int xoff, int yoff)
		{
		left += xoff;
		right += xoff;
		top += yoff;
		bottom += yoff;
		}

void
FXRect::moveTo(int xpos, int ypos)
		{
		moveBy(xpos-left, ypos-top);
		}
