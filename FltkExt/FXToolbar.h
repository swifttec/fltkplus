#ifndef __FltkExt_FXToolBar_h__
#define __FltkExt_FXToolBar_h__

#include <FltkExt/FXSize.h>
#include <FltkExt/FXButton.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>

#include <FL/Fl_Group.H>

class FLTKEXT_DLL_EXPORT FXToolbar : public FXGroup
		{
public:
		FXToolbar(int H);
		virtual ~FXToolbar();

		void			addSeparator();
		void			addLabel(const SWString &text, const FXFont &font=FXFont::getDefaultFont(FXFont::StaticText), const FXColor &textcolor="#000", int flags=FX_ALIGN_MIDDLE_RIGHT);
		void			addItem(int itemid, Fl_Widget *pItem);
		Fl_Widget *		getItem(int itemid)												{ return fx_group_getItem(this, itemid); }
		int				getItemCount() const	{ return children(); }

		void			enableItem(int itemid, bool v=true);
		void			enableAllItems(bool v);

		void			deselectAllItems();
		void			selectItem(int itemid, bool v=true, bool exclusive=false);


public: /// FLTK overrides

		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);

protected:
		static void		genericCallback(Fl_Widget *pWidget, void *pUserData);
		static void		itemCallback(Fl_Widget *pWidget);
		virtual void	onCallback(Fl_Widget *pWidget);

protected:
		int				m_gap;
		int				m_pos;
		};

#endif // __FltkExt_FXToolBar_h__
