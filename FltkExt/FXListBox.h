#ifndef __FltkExt_FXListBox_h__
#define __FltkExt_FXListBox_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXButtonBar.h>
#include <FltkExt/FXScrollBar.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWThreadMutex.h>

#include <FL/Fl_Table.H>

class FXListBoxTable;

class FLTKEXT_DLL_EXPORT FXListBox : public FXGroup
		{
public:
		enum SelectionMode
			{
			ModeNoSelect=0,
			ModeSingleSelect,
			ModeMultiSelect
			};

		enum NotificationCodes
			{
			NotifySelectionChanged=1,
			NotifyDoubleClick
			};


public:
		FXListBox(int X=0, int Y=0, int W=10, int H=10, int mode=ModeSingleSelect);

		void			setSelectMode(int mode);

		void			removeAllRows();
		void			removeAllColumns();

		int				addRow(const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		int				insertRow(int row, const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		int				updateRow(int row, const SWStringArray &values, const FXColor &fgColor="#000", const FXColor &bgColor="#fff", bool autoSizeColumns=false);
		void			updateRowValues(int row, const SWStringArray &values, bool autoSizeColumns=false);
		void			removeRow(int pos, bool autoSizeColumns=false);

		void			addColumn(const SWString &title, int align=FX_HALIGN_LEFT|FX_VALIGN_CENTER, int width=0, bool autoSizeColumns=false);
		void			setColumnTitle(int col, const SWString &contents, bool autoSizeColumns=false);
		SWString		getColumnTitle(int col) const;
		void			resizeColumns();

		int				getColumnCount() const;
		int				getRowCount() const;

		int				getSelectedRow();
		int				getSelectedRows(SWIntegerArray &ia);
		int				getFirstSelectedRow();
		int				getLastSelectedRow();
		void			selectRow(int row, bool selected=true);
		bool			isRowSelected(int row);
		void			unselectAllRows();

		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);

/*		
		int				addRow(bool batchmode=false);
		void			setRowValues(int row, const SWStringArray &values);
		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);
		int				insertRow(int pos, bool batchmode=false);
		int				insertRow(int pos, const SWStringArray &values, bool batchmode=false);
		void			endBatchMode();

		void			setCell(int row, int col, const SWString &contents, const FXColor &fgcolor=FX_COLOR_DEFAULT, const FXColor &bgcolor=FX_COLOR_DEFAULT);
		void			setCellString(int row, int col, const SWString &contents);
		SWString		getCellString(int row, int col) const;

		void			invertRowColorsOnSelection(bool v)	{ m_invertRowColorsOnSelection = v; }
		void			invertCellColorsOnSelection(bool v)	{ m_invertCellColorsOnSelection = v; }

		bool			isCellSelected(int row, int col);
		void			setSelectedRow(int row);
		void			setSelectedColumn(int col);
		int				getSelectedColumn();
		void			setSelection(int firstCol, int firstRow, int lastCol, int lastRow);
		void			getSelection(int &firstCol, int &firstRow, int &lastCol, int &lastRow);
		void			clearSelection();

		void			setTopRow(int row);

		void			setSelectionMode(int v)				{ m_selectionMode = v; }

		void			getLastClickedCell(int &row, int &col)	{ col = m_lastClickedColumn; row = m_lastClickedRow; }
		void			setLastClickedCell(int row, int col)	{ m_lastClickedColumn = col; m_lastClickedRow = row; }
*/

protected:
		FXListBoxTable	*m_pTable;
		};

#endif // __FltkExt_FXListBox_h__
