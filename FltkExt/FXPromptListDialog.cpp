#include "FXPromptListDialog.h"

#include "FXStaticText.h"

#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Hold_Browser.H>

#include <swift/sw_functions.h>


FXPromptListDialog::FXPromptListDialog(const SWString &title, const SWString &prompt, const SWStringArray &values, Fl_Widget *pParent) :
	FXDialog(title, 400, 200, pParent)
		{
		FXSize	sz=fx_getTextSize(prompt);

		addStaticText(FX_IDSTATIC, prompt, 10, 10, sz.cx+10, sz.cy+8);
		
		Fl_Hold_Browser	*pBrowser=new Fl_Hold_Browser(10, 10, w()-20, h()-sz.cy-50);

		addItem(100, pBrowser);
		addButton(FX_IDOK, "OK", 1, w()-200, h()-40, 90, 30);
		addButton(FX_IDCANCEL, "Cancel", 0, w()-100, h()-40, 90, 30);

		setItemFocus(100);

		for (int i=0;i<(int)values.size();i++)
			{
			pBrowser->add(values.get(i));
			}
		}


FXPromptListDialog::~FXPromptListDialog()
		{
		}


void
FXPromptListDialog::onOK()
		{
		bool	ok=true;

		m_selected = -1;
		Fl_Hold_Browser	*pBrowser=(Fl_Hold_Browser *)getItem(100);

		for (int row=0;row<pBrowser->size();row++)
			{
			if (pBrowser->selected(row+1))
				{
				m_selected = row;
				break;
				}
			}

		
		if (m_selected >= 0) FXDialog::onOK();
		}
