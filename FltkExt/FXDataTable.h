#ifndef __FltkExt_FXDataTable_h__
#define __FltkExt_FXDataTable_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXButtonBar.h>
#include <FltkExt/FXScrollBar.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>
#include <swift/SWStringArray.h>
#include <swift/SWThreadMutex.h>

class FLTKEXT_DLL_EXPORT FXDataTableSelection
		{
public:
		FXDataTableSelection();
		FXDataTableSelection(const FXDataTableSelection &other);

		FXDataTableSelection &	operator=(const FXDataTableSelection &other);

		bool	contains(int row, int col);
		bool	containsRow(int row);
		bool	containsColumn(int col);

public:
		int		firstRow;
		int		lastRow;
		int		firstColumn;
		int		lastColumn;
		};


/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXDataTable : public FXWidget
		{
public:
		enum SelectionMode
			{
			SelectNone=0,
			SelectRows=1,
			SelectColumns=2,
			SelectCells=4,
			SelectMultiple=8
			};

		enum AttrFlags
			{
			HasFont		= 0x01,
			HasFgColor	= 0x02,
			HasBgColor	= 0x04,
			};

		class Attributes
			{
		public:
			Attributes();

			void setFont(const FXFont &v);
			void setFgColor(const FXColor &v);
			void setBgColor(const FXColor &v);
			bool hasFgColor();
			bool hasBgColor();
			bool hasFont();

		public:
			sw_uint32_t	attrFlags;	///< The atrribute flags
			sw_uint32_t	drawFlags;	///< The drawing flags (valign, halign, etc)
			FXColor		fgcolor;	///< The foreground color
			FXColor		bgcolor;	///< The background color
			FXFont		font;		///< The font
			};

		class Column
			{
		public:
			Column();
			~Column();

			bool			getFont(FXFont &font);
			FXColor			getFgColor();
			FXColor			getBgColor();
			sw_uint32_t		getDrawFlags();

		public:
			SWString	title;		///< The column title
			int			minWidth;	///< The minimum column width
			int			maxWidth;	///< The maximum column width (0=unlimited)
			int			width;		///< The current width
			Attributes	*pAttributes;
			};

		class Row;
		class Cell
			{
		public:
			Cell();
			~Cell();

			bool			getFont(Row &row, Column &col, FXFont &font);
			FXColor			getFgColor(Row &row, Column &col);
			FXColor			getBgColor(Row &row, Column &col);
			sw_uint32_t		getDrawFlags(Row &row, Column &col);

		public:
			SWString	contents;	///< The cell contents
			FXSize		size;		///< The current size
			Attributes	*pAttributes;
			};

		class Row
			{
		public:
			Row();
			~Row();

			bool			getFont(FXFont &font);
			FXColor			getFgColor();
			FXColor			getBgColor();
			sw_uint32_t		getDrawFlags();

		public:
			SWArray<Cell>	cells;		///< The cell array
			int				height;		///< The current size
			int				flags;
			Attributes		*pAttributes;
			};

public:
		FXDataTable(int X=0, int Y=0, int W=10, int H=10);
		virtual ~FXDataTable();
		
		void			addColumn(const SWString &title, int align);
		void			setColumnTitle(int col, const SWString &contents);
		SWString		getColumnTitle(int col) const;

		int				getColumnCount() const	{ return (int)m_columns.size(); }
		int				getRowCount() const		{ return (int)m_rows.size(); }

		void			removeAllRows();
		int				addRow(bool batchmode=false);
		int				addRow(const SWStringArray &values, bool batchmode=false);
		void			updateRow(int row, const SWStringArray &values);
		void			setRowValues(int row, const SWStringArray &values);
		void			setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor);
		int				insertRow(int pos, bool batchmode=false);
		int				insertRow(int pos, const SWStringArray &values, bool batchmode=false);
		void			deleteRow(int pos, bool batchmode=false);
		void			endBatchMode();

		void			setCell(int row, int col, const SWString &contents, const FXColor &fgcolor=FX_COLOR_DEFAULT, const FXColor &bgcolor=FX_COLOR_DEFAULT);
		void			setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor);
		void			setCellString(int row, int col, const SWString &contents);
		SWString		getCellString(int row, int col) const;

		void			invertRowColorsOnSelection(bool v)	{ m_invertCellColorsOnSelection = v; }
		void			invertCellColorsOnSelection(bool v)	{ m_invertCellColorsOnSelection = v; }

		bool			isCellSelected(int row, int col);
		void			setSelectedRow(int row);
		int				getSelectedRow();
		void			setSelectedColumn(int col);
		int				getSelectedColumn();
		void			setSelection(int firstCol, int firstRow, int lastCol, int lastRow);
		void			getSelection(int &firstCol, int &firstRow, int &lastCol, int &lastRow);
		void			getSelection(SWArray<FXDataTableSelection> &selection);
		void			clearSelection();

		void			setTopRow(int row);

		void			setSelectionMode(int v)				{ clearSelection(); m_selectionMode = v; }

		void			getLastClickedCell(int &row, int &col)	{ col = m_lastClickedColumn; row = m_lastClickedRow; }
		void			setLastClickedCell(int row, int col)	{ m_lastClickedColumn = col; m_lastClickedRow = row; }

protected: /// FLTK overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);


protected:
		void			recalculate();
		void			calculateRowSize(Row &row);
		void			removeAllRowData();
		void			getCellColors(Row &row, int R, int C, FXColor &fgcolor, FXColor &bgcolor);

		static void		onEventCallback(Fl_Widget*, void* data);
		virtual void	onCallback();

		int				drawRow(Row &row, int R, int xpos, int ypos, int xlimit);
		int				drawCell(Row &row, int R, int C, int xpos, int ypos);
		void			getCellAddressAt(const FXPoint &pt, int &R, int &C);

		void			updateRowSelection(int row, bool multi, bool ctrl, bool shift);
		void			addRowToSelection(int row);
		void			removeRowFromSelection(int row);

		void			updateColumnSelection(int col, bool multi, bool ctrl, bool shift);
		void			addColumnToSelection(int col);
		void			removeColumnFromSelection(int col);

		void			updateCellSelection(int row, int col, bool multi, bool ctrl, bool shift);
		void			addCellToSelection(int row, int col);
		void			removeCellFromSelection(int row, int col);

protected:
		mutable SWThreadMutex	m_mutex;
		SWArray<Column>			m_columns;
		SWArray<Row *>			m_rows;
		Row						m_headerRow;				///< The header row
		bool					m_cellsChanged;
		bool					m_columnsChanged;
		FXColor					m_rowForegroundColor[2];	///< The colour of the even/odd row text
		FXColor					m_rowBackgroundColor[2];	///< The colour of the even/odd row background
		FXColor					m_selectedRowForegroundColor[2];	///< The colour of the even/odd row text
		FXColor					m_selectedRowBackgroundColor[2];	///< The colour of the even/odd row background
		int						m_columnGap;				///< The gap between columns
		int						m_rowGap;					///< The gap between rows
		FXRect					m_tableRect;				///< The table area rect
		FXColor					m_tableBackgroundColor;		///< The background colour of the table area
		FXColor					m_tableBorderColor;			///< The border color of the table area
		FXFont					m_tableFont;				///< The font for the header cells
		FXPadding				m_cellPadding;				///< The padding to apply to each cell

		bool					m_invertCellColorsOnSelection;
		//bool					m_invertRowColorsOnSelection;

		FXSize					m_tableSize;	///< The size of the table (ignoring scrolling)
		FXScrollBarObject		m_vScrollBar;
		bool					m_vTrackMouse;
		FXScrollBarObject		m_hScrollBar;
		bool					m_hTrackMouse;
		sw_uint32_t				m_idTimer;
		sw_uint32_t				m_scrollflags;
		int						m_selectionMode;
		int						m_lastClickedColumn;
		int						m_lastClickedRow;
		SWArray<FXDataTableSelection *>	m_selected;
		};

#endif // __FltkExt_FXDataTable_h__
