#ifndef __FltkExt_FXRadioButtonGroup_h__
#define __FltkExt_FXRadioButtonGroup_h__

#include <FltkExt/FXSize.h>
#include <FltkExt/FXRadioButton.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>

#include <FL/Fl_Group.H>

class FLTKEXT_DLL_EXPORT FXRadioButtonGroup : public FXGroup
		{
public:
		FXRadioButtonGroup(int x, int y, int w, int h, int lineheight, int xgap, int ygap, bool vertical);
		virtual ~FXRadioButtonGroup();

		void			addButton(const SWString &text, bool enabled=true);
		int				getButtonCount() const	{ return children(); }

		void			enableAllItems(bool v);

protected:
		int		m_lineheight;
		};

#endif // __FltkExt_FXRadioButtonGroup_h__
