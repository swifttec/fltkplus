#include "FXNamedDropdownList.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

FXNamedDropdownList::FXNamedDropdownList(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(X, Y, nameW, valueW, H)
		{
		init(name, value);
		}


FXNamedDropdownList::FXNamedDropdownList(const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(0, 0, nameW, valueW, H)
		{
		init(name, value);
		}

void
FXNamedDropdownList::init(const SWString &name, const SWString &value)
		{
		FXDropdownList	*pInput;

		pInput = new FXDropdownList(0, 0, m_nameW, h()-2);
		FXNamedWidget::init(name, pInput, value);
		}


void
FXNamedDropdownList::setChoices(const SWString &v)
		{
		((FXDropdownList *)m_pValue)->setChoices(v);
		}


void
FXNamedDropdownList::setChoices(const SWStringArray &v)
		{
		((FXDropdownList *)m_pValue)->setChoices(v);
		}


void
FXNamedDropdownList::setChoices(const SWArray<FXDropdownList::Choice> &v)
		{
		((FXDropdownList *)m_pValue)->setChoices(v);
		}

void
FXNamedDropdownList::addChoice(const FXDropdownList::Choice &v)
		{
		((FXDropdownList *)m_pValue)->addChoice(v);
		}


void
FXNamedDropdownList::addChoice(int id, const SWString &text, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		((FXDropdownList *)m_pValue)->addChoice(id, text, fgcolor, bgcolor);
		}


int
FXNamedDropdownList::selected()
		{
		return ((FXDropdownList *)m_pValue)->selected();
		}


int
FXNamedDropdownList::value()
		{
		return ((FXDropdownList *)m_pValue)->value();
		}


void
FXNamedDropdownList::value(int v)
		{
		((FXDropdownList *)m_pValue)->value(v);
		}


SWValue
FXNamedDropdownList::getValue()
		{
		return value();
		}
