/****************************************************************************
**	FXImage.cpp	A class to save the DC and restore it on destruction
****************************************************************************/

#include <FltkExt/FXImage.h>

#ifdef FX_USE_CXIMAGE
#include <CxImage/ximage.h>
#endif

FLTKEXT_DLL_EXPORT bool	FXImage::m_initialised=false;

void
FXImage::initialise()
		{
		if (!m_initialised)
			{
			m_initialised = true;
			fl_register_images();
			}
		}


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

FXImage::FXImage() :
	m_idPicture(0),
	m_type(ImageNone),
	m_loaded(false),
	m_tempfile(false),
#ifndef FX_USE_CXIMAGE
	m_pSizedImage(NULL),
#endif
	m_pImage(NULL)
		{
		initialise();
		}


FXImage::FXImage(sw_uint32_t id) :
	m_idPicture(id),
	m_type(ImageNone),
	m_loaded(false),
	m_tempfile(false),
#ifndef FX_USE_CXIMAGE
	m_pSizedImage(NULL),
#endif
	m_pImage(NULL)
		{
		initialise();
		load(id);
		}


FXImage::FXImage(const SWString &filename, sw_uint32_t id) :
	m_idPicture(id),
	m_type(ImageNone),
	m_loaded(false),
	m_tempfile(false),
#ifndef FX_USE_CXIMAGE
	m_pSizedImage(NULL),
#endif
	m_pImage(NULL)
		{
		initialise();
		load(filename, id);
		}


FXImage::FXImage(const FXImage &other) :
	m_idPicture(0),
	m_type(ImageNone),
	m_loaded(false),
	m_tempfile(false),
#ifndef FX_USE_CXIMAGE
	m_pSizedImage(NULL),
#endif
	m_pImage(NULL)
		{
		initialise();
		*this = other;
		}


FXImage::~FXImage()
		{
		clear();
		}


void
FXImage::createSizedImage(int w, int h)
		{
		if (m_pImage != NULL)
			{
			if (m_pSizedImage == NULL || m_szSizedImage.cx != w || m_szSizedImage.cy != h)
				{
				if (m_pSizedImage != NULL)
					{
					safe_delete(m_pSizedImage);
					}

				m_pSizedImage = m_pImage->copy(w, h);
				m_szSizedImage.cx = w;
				m_szSizedImage.cy = h;
				}
			}
		}


void
FXImage::clear()
		{
		if (m_loaded)
			{
#ifdef FX_USE_CXIMAGE
			m_pImage->Destroy();
			delete m_pImage;
			m_pImage = NULL;
#else
			if (m_pImage != NULL)
				{
				m_pImage->release();
				m_pImage = NULL;
				}

			if (m_pSizedImage != NULL)
				{
				safe_delete(m_pSizedImage);
				}
#endif
			m_loaded = false;
			}
			
		if (m_tempfile && !m_filename.isEmpty())
			{
			SWFile::remove(m_filename);
			}

		m_tempfile = false;
		m_filename.clear();
		m_idPicture = 0;
		m_type = ImageNone;
		}


FXImage &
FXImage::operator=(const FXImage &other)
		{
		clear();
		
		switch (other.type())
			{
			case ImageResource:
				load(other.id());
				break;
			
			case ImageFile:
				load(other.filename(), other.id());
				break;
			
			case ImageOther:
				copy(other);
				break;
			}

		return *this;
		}



bool
FXImage::operator==(const FXImage &other)
		{
		bool	res;
		
		res = (m_type == other.m_type);
		if (res)
			{
			switch (other.type())
				{
				case ImageResource:
					res = (m_idPicture == other.m_idPicture);
					break;
				
				case ImageFile:
					res = (m_filename == other.m_filename);
					break;
				
				case ImageOther:
					res = false;
					break;
				}
			}
		
		return res;
		}


FXSize
FXImage::getImageSize()
		{
		FXSize	sz(0, 0);

		if (loaded())
			{
#ifdef FX_USE_CXIMAGE
			sz = FXSize(m_pImage->GetWidth(), m_pImage->GetHeight());
#endif
			}

		return sz;
		}


bool
FXImage::load(const SWString &filename, sw_uint32_t id)
		{
		clear();

#ifdef FX_USE_CXIMAGE
		m_pImage = new CxImage();
		m_loaded = (m_pImage->Load(filename) != 0);

		if (m_loaded)
			{
			m_filename = filename;
			m_idPicture = id;
			m_type = ImageFile;
			m_size = FXSize(m_pImage->GetWidth(), m_pImage->GetHeight());
			}
		else
			{
			delete m_pImage;
			m_pImage = NULL;
			}
#else
		m_pImage = Fl_Shared_Image::get(filename);
		m_loaded = (m_pImage != NULL);
		if (m_loaded)
			{
			m_filename = filename;
			m_idPicture = id;
			m_type = ImageFile;
			m_size = FXSize(m_pImage->w(), m_pImage->h());
			}
#endif

		return m_loaded;
		}


FXRect
FXImage::render(FXRect &tr, int flags)
		{
		FXRect	dr;

		draw(tr, flags, 255, &dr);

		return dr;
		}


void
FXImage::draw(const FXRect &rcDest, sw_uint32_t flags, sw_int16_t alpha, FXRect *pImageRect)
		{
		// Don't even try if the image is not loaded.
		if (!m_loaded) return;

		FXRect	rcSrc(0, 0, width()-1, height()-1);
		draw(rcDest, rcSrc, flags, alpha, pImageRect);
		}


void
FXImage::draw(const FXRect &rcDest, const FXRect &rcFrom, sw_uint32_t flags, sw_int16_t alpha, FXRect *pImageRect)
		{
		// Don't even try if the image is not loaded.
		if (!m_loaded) return;

		FXRect	rcImage(0, 0, width()-1, height()-1);
		FXRect	rcSrc(rcFrom);
		FXRect	rcDraw(rcDest);

		if ((flags & XI_TILE) != 0)
			{
#ifdef FX_USE_CXIMAGE
			m_pImage->Tile(*pDC, rcDraw);
#endif
			}
		else
			{
			if ((flags & XI_STRETCH_TO_FIT) != 0)
				{
				if ((flags & XI_PROPORTIONAL) != 0)
					{
					// We need to keep the proportions of the original picture
					int	    w, h;

					w = (rcDest.Height() * rcImage.Width()) / rcImage.Height();
					h = (rcDest.Width() * rcImage.Height()) / rcImage.Width();


					// Check for a hint from the caller
					if ((flags & XI_STRETCH_TO_WIDTH) != 0)
						{
						rcDraw.left = rcDest.left;
						rcDraw.right = rcDest.right;
						rcDraw.top = rcDest.top;
						if (h > rcDest.Height())
							{
							rcSrc.bottom = rcImage.Height() * rcDest.Height() / h;
							rcDraw.bottom = rcDest.bottom;
							}
						else
							{
							rcDraw.bottom = rcDest.top + h;
							}
						}
					else if ((flags & XI_STRETCH_TO_HEIGHT) != 0)
						{
						rcDraw.left = rcDest.left;
						rcDraw.top = rcDest.top;
						rcDraw.bottom = rcDest.bottom;
						if (w > rcDest.Width())
							{
							rcSrc.right = rcImage.Width() * rcDest.Width() / w;
							rcDraw.right = rcDest.right;
							}
						else
							{
							rcDraw.right = rcDest.left + w;
							}
						}
					else
						{
						// Get the best fit
						if (w > rcDest.Width())
							{
							rcDraw.left = rcDest.left;
							rcDraw.right = rcDest.right;
							rcDraw.top = rcDest.top;
							if (h > rcDest.Height())
								{
								rcSrc.bottom = rcImage.Height() * rcDest.Height() / h;
								rcDraw.bottom = rcDest.bottom;
								}
							else
								{
								rcDraw.bottom = rcDest.top + h;
								}
							}
						else
							{
							rcDraw.left = rcDest.left;
							rcDraw.top = rcDest.top;
							rcDraw.bottom = rcDest.bottom;
							if (w > rcDest.Width())
								{
								rcSrc.right = rcImage.Width() * rcDest.Width() / w;
								rcDraw.right = rcDest.right;
								}
							else
								{
								rcDraw.right = rcDest.left + w;
								}
							}
						}
					}
				}
			else
				{
				//rcDraw.IntersectRect(&rcImage, &rcDest);
				rcDraw.left = rcDest.left;
				rcDraw.right = rcDest.left + rcImage.Width();
				if (rcDraw.right > rcDest.right) rcDraw.right = rcDest.right;
				rcDraw.top = rcDest.top;
				rcDraw.bottom = rcDest.top + rcImage.Height();
				if (rcDraw.bottom > rcDest.bottom) rcDraw.bottom = rcDest.bottom;

				if ((flags & XI_PROPORTIONAL) != 0)
					{
					// rcSrc.IntersectRect(&rcImage, &rcDraw);
					rcSrc = rcImage;
					if (rcSrc.Width() > rcDraw.Width()) rcSrc.right = rcSrc.left + rcDraw.Width();
					if (rcSrc.Height() > rcDraw.Height()) rcSrc.bottom = rcSrc.top + rcDraw.Height();
					}
				}

			// Horizontal position
			if ((flags & FX_HALIGN_MASK) == FX_HALIGN_CENTRE)
				{
				if (rcSrc.Width() != rcImage.Width())
					{
					int w = (rcImage.Width() - rcSrc.Width()) / 2;

					rcSrc.left += w;
					rcSrc.right += w;
					}
				else
					{
					int w = (rcDest.Width() - rcDraw.Width()) / 2;

					rcDraw.left += w;
					rcDraw.right += w;
					}
				}
			else if ((flags & FX_HALIGN_MASK) == FX_HALIGN_RIGHT)
				{
				if (rcSrc.Width() != rcImage.Width())
					{
					int w = rcImage.Width() - rcSrc.Width();

					rcSrc.left += w;
					rcSrc.right += w;
					}
				else
					{
					int w = rcDest.Width() - rcDraw.Width();

					rcDraw.left += w;
					rcDraw.right += w;
					}
				}

			// Vertical position
			if ((flags & FX_VALIGN_MASK) == FX_VALIGN_CENTRE)
				{
				if (rcSrc.Height() != rcImage.Height())
					{
					int h = (rcImage.Height() - rcSrc.Height()) / 2;

					rcSrc.top += h;
					rcSrc.bottom += h;
					}
				else
					{
					int h = (rcDest.Height() - rcDraw.Height()) / 2;

					rcDraw.top += h;
					rcDraw.bottom += h;
					}
				}
			else if ((flags & FX_VALIGN_MASK) == FX_VALIGN_BOTTOM)
				{
				if (rcSrc.Height() != rcImage.Height())
					{
					int h = rcImage.Height() - rcSrc.Height();

					rcSrc.top += h;
					rcSrc.bottom += h;
					}
				else
					{
					int h = rcDest.Height() - rcDraw.Height();

					rcDraw.top += h;
					rcDraw.bottom += h;
					}
				}

/*
			if (pDC->IsPrinting())
				{
				FXImage	img;
				FXRect	tr(0, 0, rcDraw.Width(), rcDraw.Height());

				img.Create(tr.Width(), tr.Height(), m_pImage->GetBpp());

				HDC	hDC=img.GetDC();
			
				m_pImage->Draw(hDC, tr, NULL, true);

				img.ReleaseDC();

				img.Draw(*pDC, rcDraw);
				}
			else
				{
				m_pImage->Draw(*pDC, rcDraw);
				}
*/
			// Render the actual image
			createSizedImage(rcDraw.width(), rcDraw.Height());
			if (m_pSizedImage != NULL) m_pSizedImage->draw(rcDraw.left, rcDraw.top, rcDraw.width(), rcDraw.height(), rcSrc.left, rcSrc.top);
			}

		if (pImageRect != NULL)
			{
			*pImageRect = rcDraw;
			}
		}


#ifdef NOT_ANY_MORE
void	
FXImage::setTransparentColorIndex(int idx)
		{
		m_pImage->SetTransColor((LONG)idx);
		}


void
FXImage::copy(const FXImage &from)
		{
		if (m_pImage == NULL)
			{
			m_pImage = new CxImage();
			}

		FXSize	sz(from.GetWidth(), from.GetHeight());
		
		size(sz);
		
		XDC		dc;
		FXRect	cr(0, 0, sz.cx, sz.cy);
		
		dc.Attach(m_pImage->GetDC());
		from.BitBlt(dc, cr, CPoint(0, 0));
		dc.Detach();
		m_pImage->ReleaseDC();

		m_type = ImageOther;
		}
#endif


void
FXImage::copy(const FXImage &from)
		{
#ifdef FX_USE_CXIMAGE
		if (from.m_pImage != NULL)
			{
			if (m_pImage == NULL)
				{
				m_pImage = new CxImage();
				}

			m_pImage->Copy(*from.m_pImage);
			}
#endif

		m_type = ImageOther;
		}


void
FXImage::size(const FXSize &sz)
		{
		size(sz.cx, sz.cy);
		}


void
FXImage::size(int width, int height)
		{
#ifdef FX_USE_CXIMAGE
		if (!m_pImage->IsValid())
			{
			if (m_pImage->GetWidth() != width || m_pImage->GetHeight() != height)
				{
				unload();
				}
			}
		
		if (m_pImage->IsValid())
			{
			m_pImage->Create(width, height, 32, 0);
			}
#endif
		
		m_size = FXSize(width, height);
		m_type = ImageOther;
		m_loaded = true;
		}

