#include <FltkExt/FXMargin.h>

FXMargin::FXMargin(int tm, int bm, int lm, int rm) :
	top(tm),
	bottom(bm),
	left(lm),
	right(rm)
		{
		}


FXMargin::FXMargin(const FXMargin &other)
		{
		*this = other;
		}


FXMargin::~FXMargin()
		{
		}


FXMargin &
FXMargin::operator=(const FXMargin &other)
		{
#define COPY(x)	x = other.x
		COPY(top);
		COPY(bottom);
		COPY(left);
		COPY(right);
#undef COPY

		return *this;
		}

bool
FXMargin::operator==(const FXMargin &other)
		{
		return (left == other.left && right == other.right && top == other.top && bottom == other.bottom);
		}
