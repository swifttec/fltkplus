#include "FXGroupBox.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>


FXGroupBox::FXGroupBox(int X, int Y, int W, int H, const SWString &title, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H),
	m_font(FXFont::getDefaultFont(FXFont::General)),
	m_title(title)
		{
		box(FL_NO_BOX);
		setBackgroundColor(bgcolor);
		}



void
FXGroupBox::onDraw()
		{
		FXRect	cr;

		fx_select(m_font);

		getClientRect(cr);

		int		fh=m_font.height();

		cr.deflate(0, fh/2, 0, 0);

		fl_draw_box(FL_ENGRAVED_BOX, cr.left, cr.top, cr.width(), cr.height(), getBackgroundColor().to_fl_color());

		if (!m_title.isEmpty())
			{
			FXSize	sz;
			int		space;

			getClientRect(cr);
			sz = fx_getTextExtent("  ");
			space = sz.cx / 2;
			cr.left += sz.cx;

			sz = fx_getTextExtent(m_title);
			cr.right = cr.left + sz.cx + space;
			cr.bottom = cr.top + sz.cy;

			fx_draw_fillRect(cr, getBackgroundColor());

			FXColor	color=getForegroundColor();

			if (!active())
				{
				color = color.toGrayscale().toOpposite().toShade(0.5);
				}
			fx_draw_text(m_title, cr, FX_ALIGN_MIDDLE_CENTRE, m_font, color);
			}
		}

