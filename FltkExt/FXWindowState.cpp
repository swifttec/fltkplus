/*
**	FXWindowState.cpp		A class representing the state of a window
*/

#include <FltkExt/FXWindowState.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXRect.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWFile.h>
#include <swift/SWFileInputStream.h>
#include <swift/SWFileOutputStream.h>
#include <swift/sw_registry.h>


FXWindowState::FXWindowState() :
	flags(Visible)
		{
		}



sw_status_t
FXWindowState::loadFromFile(const SWString &filename)
		{
		sw_status_t	res;
		SWFile		file;

		res = file.open(filename, SWFile::ModeReadOnly|SWFile::ModeBinary);
		if (res == SW_STATUS_SUCCESS)
			{
			SWFileInputStream	stream(&file, false);

			if (res == SW_STATUS_SUCCESS) res = stream.read(position.x);
			if (res == SW_STATUS_SUCCESS) res = stream.read(position.y);
			if (res == SW_STATUS_SUCCESS) res = stream.read(size.cx);
			if (res == SW_STATUS_SUCCESS) res = stream.read(size.cy);
			if (res == SW_STATUS_SUCCESS) res = stream.read(flags);

			file.close();
			}

		return res;
		}



sw_status_t
FXWindowState::saveToFile(const SWString &filename)
		{
		sw_status_t	res;
		SWFile		file;

		res = file.open(filename, SWFile::ModeWriteOnly|SWFile::ModeBinary|SWFile::ModeCreate|SWFile::ModeTruncate);
		if (res == SW_STATUS_SUCCESS)
			{
			SWFileOutputStream	stream(&file, false);

			if (res == SW_STATUS_SUCCESS) res = stream.write(position.x);
			if (res == SW_STATUS_SUCCESS) res = stream.write(position.y);
			if (res == SW_STATUS_SUCCESS) res = stream.write(size.cx);
			if (res == SW_STATUS_SUCCESS) res = stream.write(size.cy);
			if (res == SW_STATUS_SUCCESS) res = stream.write(flags);
			stream.flush();
			file.close();
			}

		return res;
		}


sw_status_t
FXWindowState::loadFromRegistry(SWRegistryHive hive, const SWString &key)
		{
		position.x = sw_registry_getInteger(hive, key, "xpos");
		position.y = sw_registry_getInteger(hive, key, "ypos");
		size.cx = sw_registry_getInteger(hive, key, "width");
		size.cy = sw_registry_getInteger(hive, key, "height");
		flags = sw_registry_getInteger(hive, key, "flags", Visible);

		return SW_STATUS_SUCCESS;
		}


sw_status_t
FXWindowState::saveToRegistry(SWRegistryHive hive, const SWString &key)
		{
		sw_registry_setInteger(hive, key, "xpos", position.x);
		sw_registry_setInteger(hive, key, "ypos", position.y);
		sw_registry_setInteger(hive, key, "width", size.cx);
		sw_registry_setInteger(hive, key, "height", size.cy);
		sw_registry_setInteger(hive, key, "flags", flags);

		return SW_STATUS_SUCCESS;
		}


void
FXWindowState::ensureOnScreen()
		{
		bool	ok=false;
		FXRect	rDesktop;
		int		sx, sy, sw, sh;

		if (size.cx != 0 && size.cy != 0)
			{
			int		nscreens=Fl::screen_count();

			for (int i=0;i<nscreens;i++)
				{
				Fl::screen_xywh(sx, sy, sw, sh, i);

				rDesktop.left = sx;
				rDesktop.top = sy;
				rDesktop.right = rDesktop.left + sw - 1;
				rDesktop.bottom = rDesktop.top + sh - 1;
				
				if (rDesktop.contains(position))
					{
					ok = true;
					break;
					}
				}
			}

		if (!ok)
			{
			Fl::screen_work_area(sx, sy, sw, sh);

			// Last screen position not suitable so use current desktop
			position.x = sx;
			position.y = sy;
			size.cx = sw;
			size.cy = sh;
			}

//		if ((flags & Visible) == 0) flags |= Visible;
		}


void
FXWindowState::maximizeOnScreen()
		{
		bool	ok=false;
		FXRect	rDesktop;
		int		sx, sy, sw, sh, bx=0, by=0, cy=0;

#if defined(SW_PLATFORM_WINDOWS)
		bx = GetSystemMetrics(SM_CXBORDER);
		by = GetSystemMetrics(SM_CYBORDER);
		/*
		bx = GetSystemMetrics(SM_CXSIZEFRAME);
		by = GetSystemMetrics(SM_CYSIZEFRAME);
		*/
		cy = GetSystemMetrics(SM_CYCAPTION) + (GetSystemMetrics(SM_CYEDGE) * 2);
#endif

		if (size.cx != 0 && size.cy != 0)
			{
			int		nscreens=Fl::screen_count();

			for (int i=0;i<nscreens;i++)
				{
				Fl::screen_work_area(sx, sy, sw, sh, i);

				rDesktop.left = sx;
				rDesktop.top = sy;
				rDesktop.right = rDesktop.left + sw - 1;
				rDesktop.bottom = rDesktop.top + sh - 1;
				
				if (rDesktop.contains(position))
					{
					position.x = sx + bx;
					position.y = sy + by + cy;
					size.cx = sw - (bx * 2);
					size.cy = sh - (by * 2) - cy;
					ok = true;
					break;
					}
				}
			}

		if (!ok)
			{
			Fl::screen_work_area(sx, sy, sw, sh);

			// Last screen position not suitable so use current desktop
			position.x = sx;
			position.y = sy;
			size.cx = sw;
			size.cy = sh;
			}
		}