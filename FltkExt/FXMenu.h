/**
*** @file		FXMenu.h
*** @brief		A generic menu item class
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXMenu_h__
#define __FltkExt_FXMenu_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXSize.h>

#include <FL/Enumerations.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>

class FXMenuItem;


/**
*** A object to hold a set of menu items as FXMenuItem objects
***
*** Every menu is treated as a single array of items, but every item can have a sub-menu
*** which is part of the item.
**/
class FLTKEXT_DLL_EXPORT FXMenu
		{
friend class FXMenuItem;

public:
		FXMenu(FXMenuItem *pOwner=NULL);
		FXMenu(const FXMenu &other);
		virtual ~FXMenu();

		FXMenu &			operator=(const FXMenu &other);

		/// Remove all the items from the menu
		void				clear();

		/// Get the number of items in the menu
		sw_size_t			size() const	{ return m_itemlist.size(); }

		/// Add an item to the menu
		FXMenuItem *		add(const FXMenuItem &item);

		/// Add an item to the menu
		FXMenuItem *		add(sw_uint32_t id, const SWString &text, sw_uint32_t shortcut=0, Fl_Callback *pCallback=NULL, void *pUserData=NULL, sw_uint32_t flags=0);

		/// Insert an item in the menu at the specified position
		FXMenuItem *		insert(int pos, const FXMenuItem &item);

		/// Insert an item in the menu at the specified position
		FXMenuItem *		insert(int pos, sw_uint32_t id, const SWString &text, sw_uint32_t shortcut=0, Fl_Callback *pCallback=NULL, void *pUserData=NULL, sw_uint32_t flags=0);

		/// Get the index of the given item or -1 if not found
		int					indexOf(FXMenuItem *pItem) const;

		/// Measure the item at the specified position
		FXSize				measureItemAt(int idx) const;

		/// Get the item at the specified position
		FXMenuItem *		getItemAt(int idx) const;

		/// Get the item at the specified position
		FXMenuItem *		operator[](int idx) const	{ return getItemAt(idx); }

		/// Remove the item at the specified position
		void				removeItemAt(int idx);

		/// Find the item that corresponds to the given path
		FXMenuItem *		findItemByPath(const SWString &path) const;

		/// Find the item that corresponds to the given path
		FXMenuItem *		findItemByCallback(Fl_Callback *cb) const;

		/// Find the item that corresponds to the given path
		FXMenuItem *		findItemByShortcut(sw_uint32_t id) const;

		/// Find the item that corresponds to the given ID
		FXMenuItem *		findItemByID(sw_uint32_t id) const;


		/// Get the default font for items in this menu
		FXFont *			font() const;

		/// Get the default font for items this menu
		FXColor *			textColor() const;

		/// Get the default font for items this menu
		FXColor *			backgroundColor() const;

		/// Get the default font for items this menu
		FXColor *			selectedBackgroundColor() const;

		/**
		  This box type is used to surround the currently-selected items in the
		  menus.  If this is FL_NO_BOX then it acts like
		  FL_THIN_UP_BOX and selection_color() acts like
		  FL_WHITE, for back compatibility.
		*/
		Fl_Boxtype			box() const;
		
		/**    See Fl_Boxtype FXMenuBar::down_box() const   */
		void				box(int b)			{ m_boxType = b; }

		/**
		  This box type is used to surround the currently-selected items in the
		  menus.  If this is FL_NO_BOX then it acts like
		  FL_THIN_UP_BOX and selection_color() acts like
		  FL_WHITE, for back compatibility.
		*/
		Fl_Boxtype			down_box() const;
		
		/**    See Fl_Boxtype FXMenuBar::down_box() const   */
		void				down_box(int b)			{ m_downBoxType = b; }

	
		FXMenuItem *		pulldown(int X, int Y, int W, int H, FXMenuItem *initial_item, Fl_Widget *pbutton, FXMenuItem *t, int menubar);

		int					textsize() const	{ return font()->height(); }
		int					textfont() const	{ return font()->fl_face(); }
		Fl_Color			color() const		{ return backgroundColor()->to_fl_color(); }

public: // Static methods

		/// Get the default font for items in this menu
		static FXFont *		defaultFont();

		/// Get the default font for items this menu
		static FXColor *	defaultTextColor();

		/// Get the default font for items this menu
		static FXColor *	defaultBackgroundColor();

		/// Get the default font for items this menu
		static FXColor *	defaultSelectedBackgroundColor();

		/// Get the default font for items this menu
		static Fl_Boxtype	defaultBoxType();

		/// Get the default font for items this menu
		static Fl_Boxtype	defaultDownBoxType();

protected:
		static FXFont			*m_pDefaultFont;			///< The default font for items in this menu
		static FXColor			*m_pDefaultTextColor;		///< The default text color for items in this menu
		static FXColor			*m_pDefaultBackgroundColor;	///< The default background color for items in this menu
		static FXColor			*m_pDefaultSelectedBackgroundColor;	///< The default background color for items in this menu
		static int				m_defaultBoxType;
		static int				m_defaultDownBoxType;

protected:
		SWArray<FXMenuItem *>	m_itemlist;						///< The list of menu items
		FXMenuItem				*m_pOwner;						///< The item which owns this menu (may be NULL)
		FXFont					*m_pFont;						///< The default font for items in this menu
		FXColor					*m_pTextColor;					///< The default text color for items in this menu
		FXColor					*m_pBackgroundColor;			///< The default background color for items in this menu
		FXColor					*m_pSelectedBackgroundColor;	///< The default background color for items in this menu
		int						m_boxType;
		int						m_downBoxType;
		};

#endif // __FltkExt_FXMenu_h__