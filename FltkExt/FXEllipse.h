/*
**	FXEllipse.h	A generic dialog class
*/

#ifndef __FltkExt_FXEllipse_h__
#define __FltkExt_FXEllipse_h__

#include <FltkExt/FXWidget.h>

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXEllipse : public FXWidget
		{
public:
		FXEllipse(const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor=FX_COLOR_NONE);
		FXEllipse(int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor=FX_COLOR_NONE);
		FXEllipse(int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor=FX_COLOR_NONE);

protected: /// FXWidget overrides

		/// Called to draw the widget outline
		virtual void	onDraw();

		/// Called to draw the widget background
		virtual void	onDrawBackground();

protected:
		FXColor		m_reflectionColor;
		};


#endif // __FltkExt_FXEllipse_h__
