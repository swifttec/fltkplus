/*
**	FXDropdownList.h	A generic dialog class
*/

#ifndef __FltkExt_FXDropdownList_h__
#define __FltkExt_FXDropdownList_h__

#include <FltkExt/FXWidget.h>

#include <FL/Fl_Choice.H>

class FLTKEXT_DLL_EXPORT FXDropdownList : public Fl_Choice
		{
public:
		class Choice
			{
		public:
			int			id;
			SWString	text;
			FXColor		fgcolor;
			FXColor		bgcolor;
			};
public:
		FXDropdownList(int X, int Y, int W, int H);

		void		clearChoices();

		void		setChoices(const SWString &v);
		void		setChoices(const SWStringArray &sa);
		void		setChoices(const SWArray<Choice> &ca);

		void		addChoice(const Choice &v);
		void		addChoice(int id, const SWString &text, const FXColor &fgcolor="#000", const FXColor &bgcolor="#fff");

		int			selected();

		int			value();
		void		value(int v);

protected:
		SWArray<Choice>	m_choices;
		};


#endif // __FltkExt_FXDropdownList_h__
