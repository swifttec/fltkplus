#include <FltkExt/FXTextStyle.h>
#include <FltkExt/fx_draw.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXTextStyle::FXTextStyle()
		{
		flags = 0;
		fgColor = FXColor(0, 0, 0);				// The current fg/text color
		bgColor = FXColor(255, 255, 255);		// The current bg color
		shadowColor = FX_COLOR_NONE;			// The current shadow color
		shadowXOffset = 0;						// The current shadow X offset
		shadowYOffset = 0;						// The current shadow Y offset
		outlineColor = FX_COLOR_NONE;			// The current outline color
		outlineWidth = 0;						// The current outline width
		}
