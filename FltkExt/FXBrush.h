/*
**	FXBrush.h
*/

#ifndef __FltkExt_FXBrush_h__
#define __FltkExt_FXBrush_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>

class FLTKEXT_DLL_EXPORT FXBrush
		{
public:
		FXBrush()					: color(FXColor(0, 0, 0))	{ }
		FXBrush(const FXColor &v)	: color(v)					{ }

		void	set(const FXColor &v)					{ color = v; }


public: //MFC Compatability methods
		void	CreateSolidBrush(const FXColor &v)		{ color = v; }

public:
		FXColor	color;
		};



#endif // __FltkExt_FXBrush_h__