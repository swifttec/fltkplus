#include "FXRadioButtonGroup.h"
#include "FXGroup.h"
#include "FXWnd.h"
#include "FXPackedLayout.h"
#include "FXRadioButton.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>



FXRadioButtonGroup::FXRadioButtonGroup(int X, int Y, int W, int H, int lineheight, int xgap, int ygap, bool vertical) :
	FXGroup(X, Y, W, H),
	m_lineheight(lineheight)
		{
		setBackgroundColor(FX_COLOR_NONE);
		setForegroundColor("#000");
		setBorderColor(FX_COLOR_NONE);
		setLayout(new FXPackedLayout(FXPackedLayout::ByRows, FX_HALIGN_LEFT, xgap, ygap));

		end();
		}


FXRadioButtonGroup::~FXRadioButtonGroup()
		{
		}



void
FXRadioButtonGroup::addButton(const SWString &text, bool enabled)
		{
		FXFont	font=FXFont::getDefaultFont();
		FXSize	sz=fx_getTextExtent(text);

		sz.cx += m_lineheight;
		FXRadioButton	*pButton = new FXRadioButton(text, 0, 0, sz.cx, m_lineheight);

		if (enabled) pButton->activate();
		else pButton->deactivate();

		add(0, pButton, true);
		layoutChildren();
		}


void
FXRadioButtonGroup::enableAllItems(bool v)
		{
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*pWidget=*a++;

			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}
