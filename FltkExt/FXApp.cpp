#include <FltkExt/FXApp.h>
#include <FltkExt/fx_functions.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>
#include <FL/x.H>
#include <FL/Fl_Shared_Image.H>



FXApp::FXApp(SWAppConfig *pAppConfig, bool mainapp) :
	SWApp(pAppConfig, SWApp::GUI, mainapp),
	m_ppMainWnd(NULL)
		{
		fl_register_images();
		fx_invalidate_init();
		fx_setAppConfig(pAppConfig);

		// Set better image scaling
		Fl_Image::RGB_scaling(FL_RGB_SCALING_BILINEAR);

		// Set default text size
		FL_NORMAL_SIZE = 11;
		}


bool
FXApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=SWApp::initInstance(prog, args);

		if (res)
			{
#if !defined(SW_PLATFORM_WINDOWS)
			fl_open_display();
#endif
			
			int		r=0;

			if (!r)
				{
				r = Fl::visual(FL_DOUBLE|FL_RGB8);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_BILINEAR);
				}
			
			if (!r)
				{
				r = Fl::visual(FL_DOUBLE|FL_RGB);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_BILINEAR);
				}
			
			if (!r)
				{
				r = Fl::visual(FL_RGB8);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_BILINEAR);
				}
			
			if (!r)
				{
				r = Fl::visual(FL_RGB);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_BILINEAR);
				}
			
			if (!r)
				{
				r = Fl::visual(FL_DOUBLE|FL_INDEX);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_NEAREST);
				}

			if (!r)
				{
				r = Fl::visual(FL_INDEX);
				if (r) Fl_Image::RGB_scaling(FL_RGB_SCALING_NEAREST);
				}
			}

		return res;
		}


void
FXApp::runInstance()
		{
		Fl::lock();
		fx_invalidate_init();

		for (;;)
			{
			while ((m_ppMainWnd == NULL || (m_ppMainWnd != NULL && *m_ppMainWnd != NULL)) && Fl::wait() > 0)
				{
				fx_invalidate_process();
				}

			if (m_ppMainWnd != NULL && *m_ppMainWnd != NULL) sw_thread_sleep(10);
			else break;
			}

 		m_exitcode = 0;
		}
