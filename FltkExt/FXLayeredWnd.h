#ifndef __FltkExt_FXLayeredWnd_h__
#define __FltkExt_FXLayeredWnd_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXLayeredGroup.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

/**
*** A special group to manage the child widgets as panes in a window
**/
class FLTKEXT_DLL_EXPORT FXLayeredWnd : public FXWnd
		{
public:
		FXLayeredWnd(const SWString &title, int W, int H, bool fixedSize=false, Fl_Widget *pOwner=NULL);
		virtual ~FXLayeredWnd();


		/// Add the given widget as a layer
		void		addLayer(Fl_Widget *o);

		/// Add the given widget as a layer with ID
		void		addLayer(int id, Fl_Widget *o);

		/// Show the layer with the specified ID
		bool		showLayer(int id, bool hideIfNotFound=false);

		/// Show the layer the corresponds to the given widget
		bool		showLayer(Fl_Widget *o, bool hideIfNotFound=false);

		/// Hide the current layer
		bool		hideLayer();
		
		/// Return a pointer to the current layer widget	
		Fl_Widget *	getCurrentLayer()	{ return m_pLayeredGroup->getCurrentLayer(); }

		void		add(Fl_Widget &w);
		void		add(Fl_Widget* o);

		/// Set the foreground / text color
		void			setBackgroundColor(const FXColor &v);

protected:
		virtual int			onKeyDown(int code, const SWString &text, const FXPoint &pt);		
		virtual int			onShortcut(int code, const FXPoint &pt);

protected:
		FXLayeredGroup	*m_pLayeredGroup;
		};

#endif // __FltkExt_FXLayeredWnd_h__
