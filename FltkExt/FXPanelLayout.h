#ifndef __FltkExt_FXPanelLayout_h__
#define __FltkExt_FXPanelLayout_h__

#include <FltkExt/FXLayout.h>

/**
*** A layout class based on a table-like grid of rows and columns
**/
class FLTKEXT_DLL_EXPORT FXPanelLayout : public FXLayout
		{
public:
		class ChildSizes
			{
		public:
			int		minsize;	// The minimum size
			int		maxsize;	// The maximum size (if zero no limit)
			};
			
public:
		/// Constructor
		FXPanelLayout(bool vertical, int gapBetweenChildren=0, const FXPadding &padding=FXPadding());
		
		/// Destructor
		virtual ~FXPanelLayout();


public: // FXLayout overrides

		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget);

		/**
		*** Get the minimum size of this layout based on the current children.
		***
		*** The minimum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		**/
		virtual FXSize		getMinSize() const;


		/**
		*** Get the maximum size of this layout based on the current children.
		***
		*** The maximum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		***
		*** If a dimension is zero this implies that there is no maximum size for that
		*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
		*** the width is restricted to 1000 pixels, but the height is unrestricted.
		**/
		virtual FXSize		getMaxSize() const;

		/// Called to layout the children
		virtual void		layoutChildren();

protected:
		ChildSizes			getChildSizes(int idx);

protected:
		bool		m_vertical;
		};

#endif // __FltkExt_FXPanelLayout_h__
