/*
**	FXHtmlStatic.h	A generic dialog class
*/

#ifndef __FltkExt_FXHtmlStatic_h__
#define __FltkExt_FXHtmlStatic_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>

#include <swift/SWString.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXHtmlStatic : public Fl_Box
		{
public:
		FXHtmlStatic(int X, int Y, int W, int H, const SWString &text, Fl_Align textalign=FL_ALIGN_LEFT, Fl_Color fgcolor=FL_BLACK, Fl_Color bgcolor=0, int textheight=0, Fl_Font font=FL_HELVETICA, Fl_Boxtype boxtype=FL_NO_BOX);
		virtual ~FXHtmlStatic();

		void				setText(const SWString &v);
		void				setText(const SWString &v, Fl_Color c);
		const SWString &	getText() const					{ return m_text; }

		void				setTextColor(Fl_Color v);

		void				text(const SWString &v)			{ setText(v); }
		const SWString &	text() const					{ return getText(); }
protected:
		SWString		m_text;
		};


#endif // __FltkExt_FXHtmlStatic_h__
