#include "FXDatePicker.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <swift/SWLocale.h>
#include <swift/SWTokenizer.h>

#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXDialog.h>
#include <FltkExt/FXCalendarPicker.h>


class FXCalendarPopup : public FXWnd
		{
public:
		FXCalendarPopup(int X, int Y, Fl_Widget *pOwner);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

		/// Called to handle an item callback (needs to be public)
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

		bool			wasDateSelected() const		{ return m_selected; }
		SWDate			getDateSelected() const		{ return m_date; }
		int				runModalLoop();

public:
		bool	m_selected;
		SWDate	m_date;
		bool	m_done;
		};


static void
itemCallback(Fl_Widget *pWidget)
		{
		FXCalendarPopup	*p=(FXCalendarPopup *)(pWidget->parent());

		p->onItemCallback(pWidget, NULL);
		}



FXCalendarPopup::FXCalendarPopup(int X, int Y, Fl_Widget *pOwner) :
	FXWnd("", 200, 160, true, pOwner)
		{
		FXCalendarPicker	*p;

		clear_border();
		add(p = new FXCalendarPicker(0, 0));
		p->callback(itemCallback);

		resize(X, Y, p->w(), p->h());
		m_selected = false;
		}


void
FXCalendarPopup::onItemCallback(Fl_Widget *pWidget, void *)
		{
		FXCalendarPicker	*p=(FXCalendarPicker *)pWidget;

		m_selected = true;
		m_date = p->getSelectedDate();
		m_done = true;
		}


int
FXCalendarPopup::onShortcut(int code, const FXPoint &pt)
		{
		if (code == 27 || code == FL_Escape)
			{
			m_done = true;
			m_selected = false;
			}
		
		return 0;
		}


int
FXCalendarPopup::runModalLoop()
		{
		m_done = false;

		set_modal();
		show();

		m_modalResult = 0;
		
		// Give the focus to the first child
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=child(i);

			if (pWidget->take_focus()) break;
			}

		while (shown() && !m_done)
			{
			Fl::wait();
			}
		
		set_non_modal();

		return m_modalResult;
		}



FXDatePicker::FXDatePicker(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_btnColor("#ccc"),
	m_arrowColor("#000")
		{
		SWDate	dt;

		dt.update();
		init(dt);
		}


void
FXDatePicker::init(const SWDate &dt)
		{
		m_font = FXFont::getDefaultFont(FXFont::StaticText);
		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, "#444", 1, 0);
		m_date = dt;
		m_selectedBgColor = "#008";
		m_selectedFgColor = "#fff";

		SWLocale		lc=SWLocale::getCurrentLocale();
		SWString		sep=lc.dateSeparator();
		SWString		fmt=lc.dateFormat();
		SWTokenizer	tok(fmt, sep);

		if (tok.getTokenCount() == 3)
			{
			fx_select(m_font);

			int	fpos=0;

			for (int i=0;i<3;i++)
				{
				SWString	s=tok.getToken(i);

				if (i > 0)
					{
					Field	&field=m_fields[fpos++];

					field.value = sep;
					field.type = 0;
					field.selected = false;
					}

				Field	&field=m_fields[fpos++];

				if (s.startsWith("D", SWString::ignoreCase))
					{
					field.type = 1;
					}
				else if (s.startsWith("M", SWString::ignoreCase))
					{
					field.type = 2;
					}
				else if (s.startsWith("Y", SWString::ignoreCase))
					{
					field.type = 3;
					}
				else
					{
					field.type = 0;
					}

				field.selected = false;
				}
			}
		else
			{
			// Not sure what to do!
			}

		}



void
FXDatePicker::onDraw()
		{
		FXRect	cr;
		SWString	s;

		getClientRect(cr);

		fx_select(m_font);
	
		int		xpos=x()+1;
		int		ypos=y()+1;
		int		H=h()-2;
		int		totalwidth=0;

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			switch (field.type)
				{
				case 1:	field.value.format("%02d", m_date.day());	break;
				case 2:	field.value.format("%02d", m_date.month());	break;
				case 3:	field.value.format("%04d", m_date.year());	break;
				}

			int		width=fx_getTextExtent(field.value).cx;

			field.rect.left = xpos;
			field.rect.right = xpos + width - 1;
			field.rect.top = ypos;
			field.rect.bottom = ypos + H - 1;

			xpos += width + 1;
			totalwidth += width;
			}

		m_rButton.top = cr.top + 1;
		m_rButton.bottom = cr.bottom - 1;
		m_rButton.right = cr.right - 1;
		m_rButton.left = m_rButton.right - h() + 2;

		int		available=m_rButton.left - cr.left - totalwidth - 3;

		if (available > 0)
			{
			int		pos=0;
			int		xadj=0;

			while (pos < (int)m_fields.size() && available > 0)
				{
				int		extra=available / ((int)m_fields.size() - pos);
				Field	&field=m_fields[pos];

				field.rect.left += xadj;
				field.rect.right += xadj + extra;
				xadj += extra;
				available -= extra;
				pos++;
				}
			}

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			if (field.type != 0 && field.selected)
				{
				fx_draw_fillRect(field.rect, m_selectedBgColor);
				fx_draw_text(field.value, field.rect, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, m_selectedFgColor);
				}
			else
				{
				fx_draw_text(field.value, field.rect, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, getForegroundColor());
				}
			}
		
		fx_draw_arrowButton(m_rButton, FX_ARROW_DOWN, m_btnColor, m_arrowColor, false);
		}


int
FXDatePicker::onLeftMouseButton(int action, const FXPoint &pt)
		{
		if (action == FX_BUTTON_DOWN)
			{

			m_selectedField = -1;

			for (int i=0;i<(int)m_fields.size();i++)
				{
				Field	&field=m_fields[i];

				if (field.type != 0 && field.rect.contains(pt))
					{
					field.selected = true;
					m_selectedField = i;
					Fl::focus(this);
					}
				else
					{
					field.selected = false;
					}
				}

			if (m_rButton.contains(pt))
				{
				int		xpos, ypos;

				fx_widget_getScreenCoordinates(this, xpos, ypos);

				FXCalendarPopup	dlg(xpos, ypos, this);

				dlg.runModalLoop();
				if (dlg.wasDateSelected())
					{
					m_date = dlg.getDateSelected();
					do_callback(this);
					}
				}
			}

		invalidate();
		fx_widget_getWindow(this)->redraw();

		return 0;
		}


int
FXDatePicker::onFocus()
		{
		if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

		if ((Fl::event_state() & FL_SHIFT) != 0) m_selectedField = (int)m_fields.size() - 1;
		else m_selectedField = 0;
						
		if (m_selectedField >= 0 && m_selectedField < (int)m_fields.size())
			{
			m_fields[m_selectedField].selected = true;
			}

		invalidate();
		fx_widget_getWindow(this)->redraw();
		
		Fl::focus(this);

		return 1;
		}


int
FXDatePicker::onUnfocus()
		{
		if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;
		m_selectedField = -1;
		invalidate();
		fx_widget_getWindow(this)->redraw();

		return 0;
		}



int
FXDatePicker::handleKey(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;
		
		if (code >= FL_KP && code <= FL_KP_Last && text.length() == 1)
			{
			code = text.getCharAt(0);
			}

		if (Fl::focus() == this)
			{
			if (m_selectedField >= 0 && code >= '0' && code <= '9')
				{
				Field	&field=m_fields[m_selectedField];
				int		n;

				switch (field.type)
					{
					case 1:	// Day
						{
						SWDate	dt;
						int		m=m_date.month()+1;
						int		y=m_date.year();

						if (m > 12)
							{
							m = 1;
							y++;
							}

						dt.set(1, m, y);
						dt -= 1;

						n = m_date.day();
						n = (n * 10) + code - '0';
						if (n > dt.day()) n = code - '0';
						m_date.set(n, m_date.month(), m_date.year());
						}
						break;

					case 2: // Month
						n = m_date.month();
						n = (n * 10) + code - '0';
						if (n > 12) n = code - '0';
						m_date.set(m_date.day(), n, m_date.year());
						break;

					case 3: /// Year
						n = m_date.year();
						n = (n * 10) + code - '0';
						if (n > 9999) n = code - '0';
						m_date.set(m_date.day(), m_date.month(), n);
						break;
					}
			
				res = 1;
				}
			else if (code == '\t' || code == FL_Tab || code == FL_Right)
				{
				if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

				if (m_fields.size() > 0)
					{
					bool	done=false;

					while (!done)
						{
						if ((Fl::event_state() & FL_SHIFT) != 0) m_selectedField--;
						else m_selectedField++;
						
						if (m_selectedField < 0 || m_selectedField >= (int)m_fields.size())
							{
							done = true;
							m_selectedField = -1;
							res = 0;
							}
						
						if (m_selectedField >=0 && m_fields[m_selectedField].type != 0)
							{
							m_fields[m_selectedField].selected = true;
							done = true;
							res = 1;
							}
						}
					}
				}
			else if (code == FL_Left)
				{
				if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

				if (m_fields.size() > 0)
					{
					bool	done=false;

					while (!done)
						{
						m_selectedField--;
						if (m_selectedField < 0)
							{
							m_selectedField = (int)m_fields.size()-1;
							}

						if (m_fields[m_selectedField].type != 0)
							{
							m_fields[m_selectedField].selected = true;
							done = true;
							res = 1;
							}
						}
					}
				}

			invalidate();
			fx_widget_getWindow(this)->redraw();
			}

		return res;
		}


int
FXDatePicker::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		return handleKey(code, text, pt);
		}


int
FXDatePicker::onShortcut(int code, const FXPoint &pt)
		{
		return handleKey(code, "", pt);
		}

