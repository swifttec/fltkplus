#include "FXTestWnd.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXTestWnd::FXTestWnd(const SWString &text, int X, int Y, int W, int H, int fgcolor, int bgcolor, int frcolor) :
	Fl_Widget(X, Y, W, H),
	m_text(text),
	m_drawcount(0),
	m_bgColor(bgcolor),
	m_fgColor(fgcolor),
	m_frColor(frcolor)
		{
//		resizable(*this);
		}


FXTestWnd::~FXTestWnd()
		{
		}


void
FXTestWnd::draw()
		{
		fl_rectf(x(), y(), w(), h(), m_bgColor);
		fl_rect(x(), y(), w(), h(), m_frColor);
		
		SWString		s;

		s.format("%s\ndraw %d - %d x %d", m_text.c_str(), ++m_drawcount, w(), h());
		fl_font(FL_HELVETICA, 10);
		fl_color(m_fgColor);
		fl_draw(s, x(), y(), w(), h(), FL_ALIGN_CENTER);
		}