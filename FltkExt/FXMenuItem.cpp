#include "FXMenu.h"
#include "FXMenuItem.h"
#include "FXMenuWindow.h"

#include <FL/fl_draw.H>
#include <FL/Fl_Menu_Item.H>

FXMenuItem::FXMenuItem()
		{
		init(0, "", 0);
		}


FXMenuItem::FXMenuItem(const FXMenuItem &item)
		{
		init(0, "", 0);
		*this = item;
		}


FXMenuItem::FXMenuItem(sw_uint32_t id, const SWString &text, sw_uint32_t flags)
		{
		init(id, text, flags);
		}


FXMenuItem::~FXMenuItem()
		{
		clear();
		}


void
FXMenuItem::init(sw_uint32_t id, const SWString &text, sw_uint32_t flags)
		{
		m_id = id;
		m_text = text;
		m_flags = flags;
		m_shortcut = 0;
		m_pCallback = NULL;
		m_pUserData = NULL;
		m_labeltype = 0;
		m_pFont = NULL;
		m_pTextColor = NULL;
		m_pBackgroundColor = NULL;
		m_pSelectedBackgroundColor = NULL;
		m_pSubMenu = NULL;

		// Used by FXPopupMenu but to be phased out
		m_pFltkMenuItem = NULL;
		}


void
FXMenuItem::clear()
		{
		m_id = 0;
		m_text.clear();
		m_flags = 0;
		m_shortcut = 0;
		m_pCallback = NULL;
		m_pUserData = NULL;
		m_labeltype = 0;
		m_pFont = NULL;
		m_pTextColor = NULL;
		m_pBackgroundColor = NULL;
		m_pSelectedBackgroundColor = NULL;

		safe_delete(m_pSubMenu);
		}


FXMenuItem &
FXMenuItem::operator=(const FXMenuItem &item)
		{
#define COPY(x)	x = item.x
		COPY(m_id);
		COPY(m_text);
		COPY(m_flags);
		COPY(m_shortcut);
		COPY(m_pCallback);
		COPY(m_pUserData);
		COPY(m_labeltype);

		safe_delete(m_pSubMenu);
		if (item.m_pSubMenu != NULL) m_pSubMenu = new FXMenu(*item.m_pSubMenu);

		if (item.m_pFont != NULL)
			{
			if (m_pFont == NULL) m_pFont = new FXFont(*item.m_pFont);
			else *m_pFont = *item.m_pFont;
			}
		else
			{
			safe_delete(m_pFont);
			}

		if (item.m_pTextColor != NULL)
			{
			if (m_pTextColor == NULL) m_pTextColor = new FXColor(*item.m_pTextColor);
			else *m_pTextColor = *item.m_pTextColor;
			}
		else
			{
			safe_delete(m_pTextColor);
			}

		// Used by FXPopupMenu but to be phased out
		COPY(m_pFltkMenuItem);
#undef COPY

		return *this;
		}


FXMenu *
FXMenuItem::createSubMenu()
		{
		if (m_pSubMenu == NULL)
			{
			m_pSubMenu = new FXMenu();
			}

		return m_pSubMenu;
		}



FXFont *
FXMenuItem::font() const
		{
		FXFont	*p = FXMenu::defaultFont();

		if (m_pFont != NULL) p = m_pFont;
		else if (m_pOwner != NULL) p = m_pOwner->font();

		return p;
		}


FXColor *
FXMenuItem::textColor() const
		{
		FXColor	*p = FXMenu::defaultTextColor();

		if (m_pTextColor != NULL) p = m_pTextColor;
		else if (m_pOwner != NULL) p = m_pOwner->textColor();

		return p;
		}


FXColor *
FXMenuItem::backgroundColor() const
		{
		FXColor	*p = FXMenu::defaultBackgroundColor();

		if (m_pBackgroundColor != NULL) p = m_pBackgroundColor;
		else if (m_pOwner != NULL) p = m_pOwner->backgroundColor();

		return p;
		}


FXColor *
FXMenuItem::selectedBackgroundColor() const
		{
		FXColor	*p = FXMenu::defaultSelectedBackgroundColor();

		if (m_pSelectedBackgroundColor != NULL) p = m_pSelectedBackgroundColor;
		else if (m_pOwner != NULL) p = m_pOwner->selectedBackgroundColor();

		return p;
		}


/**
  Measures width of label, including effect of & characters.
  Optionally, can get height if hp is not NULL.
*/
FXSize
FXMenuItem::measure(const FXMenu *m) const
		{
		Fl_Label	l;
		FXSize		sz;
		FXFont		*pFont = font();
		FXColor		*pTextColor = textColor();

		l.value = m_text;
		l.image = 0;
		l.deimage = 0;
		l.type = m_labeltype;
		l.font = pFont->fl_face();
		l.size = pFont->height();
		l.color = pTextColor->to_fl_color();

		fl_draw_shortcut = 1;
		int w = 0; int h = 0;
		l.measure(sz.cx, sz.cy);
		fl_draw_shortcut = 0;

		if (isFlagSet(CheckBox|Radio)) sz.cx += FL_NORMAL_SIZE;

		return sz;
		}

#define LEADING 4 // extra vertical leading

/** Draws the menu item in bounding box x,y,w,h, optionally selects the item. */
void
FXMenuItem::draw(int x, int y, int w, int h, const FXMenu *m)
		{
		m_lastDrawnRect.setXYWH(x, y, w, h);

		Fl_Label	l;
		FXFont		*pFont = font();
		Fl_Color	fgColor = textColor()->to_fl_color();
		Fl_Color	bgColor = backgroundColor()->to_fl_color();

		l.value = m_text;
		l.image = 0;
		l.deimage = 0;
		l.type = m_labeltype;
		l.font = pFont->fl_face();
		l.size = pFont->height();
		l.color = fgColor;

		if (!active()) l.color = fl_inactive((Fl_Color)l.color);

		if (isFlagSet(Selected))
			{
			Fl_Color	r = selectedBackgroundColor()->to_fl_color();
			Fl_Boxtype	b = m && m->down_box() ? m->down_box() : FL_FLAT_BOX;
			int			selected=1;

			if (fl_contrast(r, bgColor) != r)
				{ // back compatibility boxtypes
				if (selected == 2)
					{ // menu title
					r = bgColor;
					b = m ? m->box() : FL_UP_BOX;
					}
				else
					{
					r = (Fl_Color)(FL_COLOR_CUBE - 1); // white
					l.color = fl_contrast(fgColor, r);
					}
				}
			else
				{
				l.color = fl_contrast(fgColor, r);
				}
			if (selected == 2)
				{ // menu title
				fl_draw_box(b, x, y, w, h, r);
				x += 3;
				w -= 8;
				}
			else
				{
				fl_draw_box(b, x + 1, y - (LEADING - 2) / 2, w - 2, h + (LEADING - 2), r);
				}
			}

		if (isFlagSet(CheckBox | Radio))
			{
			int d = (h - FL_NORMAL_SIZE + 1) / 2;
			int W = h - 2 * d;

			if (isFlagSet(Radio))
				{
				fl_draw_box(FL_OVAL_BOX, x + 2, y + d, W, W, FL_BACKGROUND2_COLOR);
				if (value())
					{
					int tW = (W - Fl::box_dw(FL_OVAL_BOX)) / 2 + 1;
					if ((W - tW) & 1) tW++;	// Make sure difference is even to center
					int td = (W - tW) / 2;
					if (Fl::is_scheme("gtk+"))
						{
						fl_color(FL_SELECTION_COLOR);
						tW--;
						fl_pie(x + td + 1, y + d + td - 1, tW + 3, tW + 3, 0.0, 360.0);
						fl_color(fl_color_average(FL_WHITE, FL_SELECTION_COLOR, 0.2f));
						}
					else fl_color(fgColor);

					switch (tW)
						{
						// Larger circles draw fine...
							default:
								fl_pie(x + td + 2, y + d + td, tW, tW, 0.0, 360.0);
								break;

								// Small circles don't draw well on many systems...
							case 6:
								fl_rectf(x + td + 4, y + d + td, tW - 4, tW);
								fl_rectf(x + td + 3, y + d + td + 1, tW - 2, tW - 2);
								fl_rectf(x + td + 2, y + d + td + 2, tW, tW - 4);
								break;

							case 5:
							case 4:
							case 3:
								fl_rectf(x + td + 3, y + d + td, tW - 2, tW);
								fl_rectf(x + td + 2, y + d + td + 1, tW, tW - 2);
								break;

							case 2:
							case 1:
								fl_rectf(x + td + 2, y + d + td, tW, tW);
								break;
						}

					if (Fl::is_scheme("gtk+"))
						{
						fl_color(fl_color_average(FL_WHITE, FL_SELECTION_COLOR, 0.5));
						fl_arc(x + td + 2, y + d + td, tW + 1, tW + 1, 60.0, 180.0);
						}
					}
				}
			else
				{
				fl_draw_box(FL_BORDER_BOX, x + 2, y + d, W, W, FL_BACKGROUND2_COLOR);
				if (value())
					{
					if (Fl::is_scheme("gtk+"))
						{
						fl_color(FL_SELECTION_COLOR);
						}
					else
						{
						fl_color(fgColor);
						}
					int tx = x + 5;
					int tw = W - 6;
					int d1 = tw / 3;
					int d2 = tw - d1;
					int ty = y + d + (W + d2) / 2 - d1 - 2;
					for (int n = 0; n < 3; n++, ty++)
						{
						fl_line(tx, ty, tx + d1, ty + d1);
						fl_line(tx + d1, ty + d1, tx + tw - 1, ty + d1 - d2 + 1);
						}
					}
				}
			x += W + 3;
			w -= W + 3;
			}

		if (!fl_draw_shortcut) fl_draw_shortcut = 1;
		l.draw(x + 3, y, w > 6 ? w - 6 : 0, h, FL_ALIGN_LEFT);
		fl_draw_shortcut = 0;
		}


SWString
FXMenuItem::path() const
		{
		SWString	res;

		if (m_pOwner != NULL && m_pOwner->m_pOwner != NULL)
			{
			res = m_pOwner->m_pOwner->path() + "/";
			}

		res += m_text;
		
		return res;
		}


void
FXMenuItem::do_callback(Fl_Widget* o) const
		{
		if (m_pCallback != NULL)
			{
			m_pCallback(o, m_pUserData);
			}
		}


void
FXMenuItem::do_callback(Fl_Widget *o, void *arg) const
		{
		if (m_pCallback != NULL)
			{
			m_pCallback(o, arg);
			}
		}


void
FXMenuItem::do_callback(Fl_Widget *o, long arg) const
		{
		if (m_pCallback != NULL)
			{
			m_pCallback(o, (void *)(fl_intptr_t)arg);
			}
		}

