/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the definition of the FXUniformLayout class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXUniformLayout_h__
#define __FltkExt_FXUniformLayout_h__

#include <FltkExt/FXLayout.h>

/**
*** A layout class where all the components get assigned the same size
*** regardless of their min/max size
**/
class FLTKEXT_DLL_EXPORT FXUniformLayout : public FXLayout
		{
public:
		/// Constructor
		FXUniformLayout(bool vertical, int gap, int maxsize=0);


public: // FXLayout overrides

		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget);

		/**
		*** Get the minimum size of this layout based on the current children.
		***
		*** The minimum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		**/
		virtual FXSize		getMinSize() const;


		/**
		*** Get the maximum size of this layout based on the current children.
		***
		*** The maximum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		***
		*** If a dimension is zero this implies that there is no maximum size for that
		*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
		*** the width is restricted to 1000 pixels, but the height is unrestricted.
		**/
		virtual FXSize		getMaxSize() const;

		/// Called to layout the children
		virtual void		layoutChildren();

protected:
		bool	m_vertical;
		int		m_gap;
		int		m_maxsize;
		};

#endif // __FltkExt_FXUniformLayout_h__
