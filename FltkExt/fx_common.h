/*
**	fx_common.h	Include commonly used headers
*/

#ifndef __FltkExt_fx_common_h__
#define __FltkExt_fx_common_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXPoint.h>
#include <FltkExt/FXRect.h>
#include <FltkExt/FXBrush.h>

#include <swift/SWString.h>

typedef FXRect	CRect;
typedef FXBrush	CBrush;

#endif // __FltkExt_fx_common_h__
