/*
**	FXStaticImage.h	A generic dialog class
*/

#ifndef __FltkExt_FXStaticImage_h__
#define __FltkExt_FXStaticImage_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXStaticImage : public FXWidget
		{
public:
		FXStaticImage(int X, int Y, int W, int H, const SWString &filename, int flags, const FXColor &bgcolor=FXColor::NoColor);
		FXStaticImage(const SWString &filename, int flags, const FXColor &bgcolor=FXColor::NoColor);

		virtual ~FXStaticImage();

		bool		load(const SWString &filename);
		void		unload();
		bool		loaded() const				{ return (m_pImage != NULL); }

		void		flags(int v)				{ m_flags = v; }
		int			flags() const				{ return m_flags; }

protected: /// FXWidget Override
		
		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

protected:
		Fl_Shared_Image		*m_pImage;
		Fl_Image			*m_pImageCopy;	///< The resized image used for drawing
		int					m_flags;	///< The image flags
		};


#endif // __FltkExt_FXStaticImage_h__
