#include "FXRadioButton.h"
#include "FXDialog.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXRadioButton::FXRadioButton(const SWString &text, int X, int Y, int W, int H, const FXColor &textcolor) :
	Fl_Light_Button(X, Y, W, H),
	m_text(text)
		{
		type(FL_RADIO_BUTTON);
		label(m_text);
		labelcolor(textcolor.to_fl_color());
		box(FL_NO_BOX);
		down_box(FL_OVAL_BOX);
		selection_color(FL_FOREGROUND_COLOR);
		color(FL_WHITE);
		}


FXRadioButton::~FXRadioButton()
		{
		}


void
FXRadioButton::setFont(const FXFont &font)
		{
		labelfont(font.fl_face());
		labelsize(font.height());
		}
