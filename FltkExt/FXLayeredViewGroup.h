#ifndef __FltkExt_FXLayeredViewGroup_h__
#define __FltkExt_FXLayeredViewGroup_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXGroup.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

/**
*** A special group to manage the child widgets as panes in a window
**/
class FLTKEXT_DLL_EXPORT FXLayeredViewGroup : public FXGroup
		{
public:
		FXLayeredViewGroup();
		virtual ~FXLayeredViewGroup();

		/// Called when the view is shown or opened
		virtual void		onViewOpen();

		/// Called when the view is shown or opened
		virtual void		onViewClose();
		};

#endif // __FltkExt_FXLayeredViewGroup_h__
