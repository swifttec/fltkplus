#include <FltkExt/fx_functions.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBoxedText.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXFontFamily.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXNamedWidget.h>
#include <FltkExt/FXCheckBox.h>
#include <FltkExt/FXDropdownList.h>
#include <FltkExt/FXRadioButtonGroup.h>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Float_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <FL/fl_message.H>
#include <FL/Fl_Valuator.H>
#include <FL/Fl_Menu_Bar.H>

#include <swift/SWList.h>
#include <swift/SWAppConfig.h>
#include <swift/SWThreadMutex.h>

class InvalidateEntry
		{
public:
		InvalidateEntry(Fl_Widget *p, int d) : pWidget(p), damage(d)		{ }

		Fl_Widget	*pWidget;
		int			damage;
		};

static SWList<InvalidateEntry *>	*g_pInvalidateList=NULL;
static sw_thread_id_t				g_mainThreadID=0;
static SWAppConfig					*g_pAppConfig=NULL;
static SWThreadMutex				*g_pMutex=NULL;

FLTKEXT_DLL_EXPORT void
fx_setAppConfig(SWAppConfig *pAppConfig)
		{
		if (g_pMutex == NULL)
			{
			g_pMutex = new SWThreadMutex(true);
			}

		SWMutexGuard	guard(g_pMutex);

		g_pAppConfig = pAppConfig;
		}


FLTKEXT_DLL_EXPORT SWString
fx_getResourceFile(const SWString &filename)
		{
		SWMutexGuard	guard(g_pMutex);
		SWString		res;

		if (g_pAppConfig != NULL)
			{
			res = g_pAppConfig->getResourceFile(filename);
			}
		
		return res;
		}


FLTKEXT_DLL_EXPORT void
fx_invalidate_init()
		{
		if (g_pInvalidateList == NULL)
			{
			g_pInvalidateList = new SWList<InvalidateEntry *>();
			g_pInvalidateList->synchronize();
			g_mainThreadID = sw_thread_self();
			}
		}


FLTKEXT_DLL_EXPORT void
fx_invalidate(Fl_Widget *pWidget)
		{
		if (pWidget != NULL)
			{
			/*
			if (g_pInvalidateList == NULL) fx_invalidate_init();
			if (g_pInvalidateList == NULL || g_mainThreadID == sw_thread_self())
			*/
			if (g_pInvalidateList == NULL)
				{
				pWidget->redraw();
				}
			else
				{
				g_pInvalidateList->lock();
				
				SWIterator<InvalidateEntry *>	it=g_pInvalidateList->iterator();
				InvalidateEntry					*pEntry;
				bool							found=false;

				while (it.hasNext())
					{
					pEntry = *it.next();

					if (pEntry->pWidget == pWidget)
						{
						// Already in list - don't repeat
						found = true;
						break;
						}
					}

				if (!found)
					{
					InvalidateEntry	*pEntry=new InvalidateEntry(pWidget, 0);
					Fl::watch_widget_pointer(pEntry->pWidget);
					g_pInvalidateList->addTail(pEntry);
					}
				
				g_pInvalidateList->unlock();

				Fl::awake();
				}
			}
		}


FLTKEXT_DLL_EXPORT void
fx_invalidate_process()
		{
		if (g_pInvalidateList != NULL)
			{
			SWList<InvalidateEntry *>	ilist;

			g_pInvalidateList->lock();
			ilist = *g_pInvalidateList;
			g_pInvalidateList->clear();
			g_pInvalidateList->unlock();

			InvalidateEntry	*pEntry;

			while (ilist.removeHead(&pEntry))
				{
				if (pEntry->pWidget != NULL)
					{
					pEntry->pWidget->redraw();
					}
				Fl::release_widget_pointer(pEntry->pWidget);
				delete pEntry;
				}
			}
		}






FLTKEXT_DLL_EXPORT void
fx_group_showItem(Fl_Group *pGroup, int itemid, bool v)
		{
		Fl_Widget	*pWidget=fx_group_getItem(pGroup, itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->show();
			else pWidget->hide();
			}
		}


FLTKEXT_DLL_EXPORT void
fx_group_enableItem(Fl_Group *pGroup, int itemid, bool v)
		{
		Fl_Widget	*pWidget=fx_group_getItem(pGroup, itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}


FLTKEXT_DLL_EXPORT void
fx_group_enableAllItemsWithID(Fl_Group *pGroup, int itemid, bool v)
		{
		Fl_Widget	*const *ca=pGroup->array();

		for (int i=0;i<pGroup->children();i++)
			{
			Fl_Widget	*pWidget=*ca++;

			if (pWidget->id() == (ulong)itemid)
				{
				if (v) pWidget->activate();
				else pWidget->deactivate();
				}
			else
				{
				Fl_Group	*pSubGroup=dynamic_cast<Fl_Group *>(pWidget);

				if (pSubGroup != NULL)
					{
					fx_group_enableAllItemsWithID(pSubGroup, itemid, v);
					}
				}
			}
		}


FLTKEXT_DLL_EXPORT void
fx_group_addItem(Fl_Group *pGroup, int itemid, Fl_Widget *pItem)
		{
		pItem->id(itemid);
		if (pItem->parent() != pGroup) pGroup->add(pItem);
		}


FLTKEXT_DLL_EXPORT Fl_Widget *
fx_group_getItem(Fl_Group *pGroup, int itemid)
		{
		Fl_Widget	*const *ca=pGroup->array();
		Fl_Widget	*pFoundWidget=NULL;

		for (int i=0;i<pGroup->children();i++)
			{
			Fl_Widget	*pWidget=*ca++;

			if (pWidget->id() == (ulong)itemid)
				{
				pFoundWidget = pWidget;
				break;
				}
			else
				{
				Fl_Group	*pSubGroup=dynamic_cast<Fl_Group *>(pWidget);

				if (pSubGroup != NULL)
					{
					pFoundWidget = fx_group_getItem(pSubGroup, itemid);
					if (pFoundWidget != NULL) break;
					}
				}
			}

		return pFoundWidget;
		}


FLTKEXT_DLL_EXPORT Fl_Widget *
fx_group_getItemAt(Fl_Group *pGroup, const FXPoint &pt)
		{
		Fl_Widget	*const *ca=pGroup->array();
		Fl_Widget	*pFoundWidget=NULL;

		for (int i=0;i<pGroup->children();i++)
			{
			Fl_Widget	*pWidget=*ca++;
			Fl_Group	*pSubGroup=dynamic_cast<Fl_Group *>(pWidget);

			if (pSubGroup != NULL)
				{
				pFoundWidget = fx_group_getItemAt(pSubGroup, pt);
				if (pFoundWidget != NULL) break;
				}
			else
				{
				FXRect	r(pWidget->x(), pWidget->y(), pWidget->x() + pWidget->w() - 1, pWidget->y() + pWidget->h() - 1);

				if (r.contains(pt))
					{
					pFoundWidget = pWidget;
					break;
					}
				}
			}

		return pFoundWidget;
		}


FLTKEXT_DLL_EXPORT bool
fx_group_setItemValue(Fl_Group *pGroup, int itemid, const SWValue &v, int limitsize)
		{
		return fx_widget_setValue(fx_group_getItem(pGroup, itemid), v, limitsize);
		}


FLTKEXT_DLL_EXPORT bool
fx_group_getItemValue(Fl_Group *pGroup, int itemid, SWValue &v)
		{
		return fx_widget_getValue(fx_group_getItem(pGroup, itemid), v);
		}


FLTKEXT_DLL_EXPORT bool
fx_group_getItemValue(Fl_Group *pGroup, int itemid, SWString &v)
		{
		return fx_widget_getValue(fx_group_getItem(pGroup, itemid), v);
		}

FLTKEXT_DLL_EXPORT bool
fx_group_getItemValue(Fl_Group *pGroup, int itemid, int &v)
		{
		return fx_widget_getValue(fx_group_getItem(pGroup, itemid), v);
		}


FLTKEXT_DLL_EXPORT bool
fx_group_getItemValue(Fl_Group *pGroup, int itemid, bool &v)
		{
		return fx_widget_getValue(fx_group_getItem(pGroup, itemid), v);
		}


FLTKEXT_DLL_EXPORT bool
fx_group_getItemValue(Fl_Group *pGroup, int itemid, double &v)
		{
		return fx_widget_getValue(fx_group_getItem(pGroup, itemid), v);
		}


FLTKEXT_DLL_EXPORT bool
fx_widget_setValue(Fl_Widget *pWidget, const SWValue &v, int limitsize)
		{
		bool		done=false;

		if (pWidget != NULL)
			{
			if (!done)
				{
				Fl_Input	*pInput=dynamic_cast<Fl_Input *>(pWidget);

				if (pInput != NULL)
					{
					done = true;

					if (limitsize > 0 && (int)v.length() > limitsize)
						{
						pInput->value(v.toString().left(limitsize));
						}
					else
						{
						pInput->value(v.toString());
						}

					if (limitsize > 0)
						{
						pInput->maximum_size(limitsize);
						}
					}
				}

			if (!done)
				{
				FXBoxedText	*pWnd = dynamic_cast<FXBoxedText *>(pWidget);

				if (pWnd != NULL)
					{
					done = true;
					pWnd->text(v.toString());
					}
				}

			if (!done)
				{
				FXNamedWidget	*pWnd = dynamic_cast<FXNamedWidget *>(pWidget);

				if (pWnd != NULL)
					{
					done = true;
					pWnd->setValue(v.toString());
					}
				}

			if (!done)
				{
				FXStaticText	*pWnd = dynamic_cast<FXStaticText *>(pWidget);

				if (pWnd != NULL)
					{
					done = true;
					pWnd->text(v.toString());
					}
				}

			if (!done)
				{
				Fl_Check_Button	*pInput=dynamic_cast<Fl_Check_Button *>(pWidget);

				if (pInput != NULL)
					{
					pInput->value(v.toBoolean()?1:0);
					done = true;
					}
				}

			if (!done)
				{
				FXRadioButtonGroup	*pGroup=dynamic_cast<FXRadioButtonGroup *>(pWidget);

				if (pGroup != NULL)
					{
					int		nchildren=pGroup->children();
					int		idx=v.toInt32();

					for (int i=0;i<nchildren;i++)
						{
						FXRadioButton	*p=dynamic_cast<FXRadioButton *>(pGroup->child(i));

						if (p != NULL)
							{
							p->value((idx == i)?1:0);
							}
						}

					done = true;
					}
				}

			if (!done)
				{
				FXCheckBox	*pInput=dynamic_cast<FXCheckBox *>(pWidget);

				if (pInput != NULL)
					{
					pInput->setState(v.toBoolean()?(FXCheckBox::ItemTicked):(FXCheckBox::ItemUnselected));
					done = true;
					}
				}

			if (!done)
				{
				FXDropdownList	*pInput=dynamic_cast<FXDropdownList *>(pWidget);

				if (pInput != NULL)
					{
					pInput->value(v.toInt32());
					done = true;
					}
				}

			if (!done)
				{
				Fl_Choice	*pInput=dynamic_cast<Fl_Choice *>(pWidget);

				if (pInput != NULL)
					{
					pInput->value(v.toInt32());
					done = true;
					}
				}

			if (!done)
				{
				Fl_Valuator	*pInput=dynamic_cast<Fl_Valuator *>(pWidget);

				if (pInput != NULL)
					{
					pInput->value(v.toInt32());
					done = true;
					}
				}
			}
		
		return done;
		}


FLTKEXT_DLL_EXPORT bool
fx_widget_getValue(Fl_Widget *pWidget, SWValue &v)
		{
		bool		done=false;

		if (pWidget != NULL)
			{
			if (!done)
				{
				Fl_Input	*pInput=dynamic_cast<Fl_Input *>(pWidget);

				if (pInput != NULL)
					{
					v = pInput->value();
					done = true;
					}
				}

			if (!done)
				{
				FXNamedWidget	*pBox = dynamic_cast<FXNamedWidget *>(pWidget);

				if (pBox != NULL)
					{
					done = true;
					v = pBox->getValue();
					}
				}

			if (!done)
				{
				FXStaticText	*pBox = dynamic_cast<FXStaticText *>(pWidget);

				if (pBox != NULL)
					{
					done = true;
					v = pBox->text();
					}
				}

			if (!done)
				{
				Fl_Check_Button	*pInput=dynamic_cast<Fl_Check_Button *>(pWidget);

				if (pInput != NULL)
					{
					v = (pInput->value() != 0);
					done = true;
					}
				}

			if (!done)
				{
				FXRadioButtonGroup	*pGroup=dynamic_cast<FXRadioButtonGroup *>(pWidget);

				if (pGroup != NULL)
					{
					int		nchildren=pGroup->children();
					int		idx=0;

					for (int i=0;i<nchildren;i++)
						{
						FXRadioButton	*p=dynamic_cast<FXRadioButton *>(pGroup->child(i));

						if (p != NULL)
							{
							if (p->value() != 0)
								{
								idx = i;
								break;
								}
							}
						}

					v = idx;
					done = true;
					}
				}

			if (!done)
				{
				FXCheckBox	*pInput=dynamic_cast<FXCheckBox *>(pWidget);

				if (pInput != NULL)
					{
					v = (pInput->getState() == FXCheckBox::ItemTicked);
					done = true;
					}
				}

			if (!done)
				{
				FXDropdownList	*pInput=dynamic_cast<FXDropdownList *>(pWidget);

				if (pInput != NULL)
					{
					v = pInput->value();
					done = true;
					}
				}

			if (!done)
				{
				Fl_Choice	*pInput=dynamic_cast<Fl_Choice *>(pWidget);

				if (pInput != NULL)
					{
					v = pInput->value();
					done = true;
					}
				}

			if (!done)
				{
				Fl_Valuator *pInput=dynamic_cast<Fl_Valuator *>(pWidget);

				if (pInput != NULL)
					{
					v = pInput->value();
					done = true;
					}
				}
			}
		
		return done;
		}


FLTKEXT_DLL_EXPORT bool
fx_widget_getValue(Fl_Widget *pWidget, SWString &v)
		{
		SWValue	vobj;
		bool		done=fx_widget_getValue(pWidget, vobj);

		if (done) v = vobj.toString();

		return done;
		}
		
				
FLTKEXT_DLL_EXPORT bool
fx_widget_getValue(Fl_Widget *pWidget, int &v)
		{
		SWValue	vobj;
		bool		done=fx_widget_getValue(pWidget, vobj);

		if (done) v = vobj.toInt32();

		return done;
		}

		
FLTKEXT_DLL_EXPORT bool
fx_widget_getValue(Fl_Widget *pWidget, double &v)
		{
		SWValue	vobj;
		bool		done=fx_widget_getValue(pWidget, vobj);

		if (done) v = vobj.toDouble();

		return done;
		}


FLTKEXT_DLL_EXPORT bool
fx_widget_getValue(Fl_Widget *pWidget, bool &v)
		{
		SWValue	vobj;
		bool		done=fx_widget_getValue(pWidget, vobj);

		if (done) v = vobj.toBoolean();

		return done;
		}


FLTKEXT_DLL_EXPORT bool
fx_group_isOwnerOf(Fl_Group *pGroup, Fl_Widget *pWidget)
		{
		bool	res=false;

		if (!res && pWidget != NULL)
			{
			Fl_Widget	*const *ca=pGroup->array();

			for (int i=0;i<pGroup->children();i++)
				{
				Fl_Widget	*pChildWidget=*ca++;

				if (pChildWidget == pWidget)
					{
					res = true;
					break;
					}
				else
					{
					Fl_Group	*pSubGroup=dynamic_cast<Fl_Group *>(pChildWidget);

					if (pSubGroup != NULL)
						{
						res = fx_group_isOwnerOf(pSubGroup, pWidget);
						if (res) break;
						}
					}
				}
			}

		return res;
		}


FLTKEXT_DLL_EXPORT void
fx_group_moveChildren(Fl_Group *pGroup, int xoff, int yoff)
		{
		if (xoff != 0 || yoff != 0)
			{
			// Move all the child positions
			int				nchildren=pGroup->children();
			Fl_Widget		*const *a=pGroup->array();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*oa=*a++;

				oa->position(oa->x() + xoff, oa->y() + yoff);

				/*
				Fl_Group	*pSubGroup=dynamic_cast<Fl_Group *>(oa);
			
				if (pSubGroup != NULL)
					{
					fx_group_moveChildren(pSubGroup, xoff, yoff);
					}
				*/
				}
			}
		}

FLTKEXT_DLL_EXPORT bool
fx_widget_getScreenCoordinates(Fl_Widget *pWidget, int &xpos, int &ypos)
		{
		bool	res=false;
		
		if (pWidget != NULL)
			{
			xpos = ypos = 0;

			if (fx_widget_isMainWindow(pWidget))
				{
				xpos = pWidget->x();
				ypos = pWidget->y();
				}
			else
				{
				// Center us over our parent window
				int		px=0;
				int		py=0;

				// Find the coordinates of the containing window if possible
				// limit search to 100 levels so we never get stuck here if the widget heirachy
				// gets corrupted
				Fl_Widget	*p=pWidget->parent();
				int			level=0;

				while (p != NULL && level++ < 100)
					{
					if (dynamic_cast<Fl_Window *>(p) != NULL)
						{
						// We have found a window!
						px = p->x();
						py = p->y();
						break;
						}

					p = p->parent();
					}
				
				xpos = px + pWidget->x();
				ypos = py + pWidget->y();
				}

			res = true;
			}

		return res;
		}


FLTKEXT_DLL_EXPORT void
fx_widget_centerOver(Fl_Widget *pWidget, Fl_Widget *pUnderWidget)
		{
		int		xpos=0, ypos=0;

		if (fx_widget_isMainWindow(pWidget))
			{
			if (pUnderWidget != NULL)
				{
				// Center us over our parent window
				int		px=pUnderWidget->x();
				int		py=pUnderWidget->y();
				int		pw=pUnderWidget->w();
				int		ph=pUnderWidget->h();

				if (!fx_widget_isMainWindow(pUnderWidget))
					{
					// Find the coordinates of the containing window if possible
					// limit search to 100 levels so we never get stuck here if the widget heirachy
					// gets corrupted
					Fl_Widget	*p=pUnderWidget->parent();
					int			level=0;

					while (p != NULL && level++ < 100)
						{
						if (dynamic_cast<Fl_Window *>(p) != NULL)
							{
							// We have foud a window!
							px += p->x();
							py += p->y();
							break;
							}

						p = p->parent();
						}
					}

				xpos = px + ((pw-pWidget->w())/2);
				ypos = py + ((ph-pWidget->h())/2);
				}
			else
				{
				// Center us over the main screen
				xpos = Fl::x() + ((Fl::w()-pWidget->w())/2);
				ypos = Fl::y() + ((Fl::h()-pWidget->h())/2);
				}

			pWidget->position(xpos, ypos);
			}
		}


FLTKEXT_DLL_EXPORT int
fx_widget_handleCommand(Fl_Widget *pw, sw_uint32_t id)
		{
		int	res=0;

		if (res == 0)
			{
			FXWnd		*pWnd=dynamic_cast<FXWnd *>(pw);

			if (pWnd != NULL) res = pWnd->onCommand(id);
			}

		if (res == 0)
			{
			FXGroup		*pGroup=dynamic_cast<FXGroup *>(pw);

			if (pGroup != NULL) res = pGroup->onCommand(id);
			}

		if (res == 0)
			{
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(pw);

			if (pWidget != NULL) pWidget->onCommand(id);
			}

		if (res == 0)
			{
			if (pw->parent() != NULL) res = fx_widget_handleCommand(pw->parent(), id);
			}

		return res;
		}



FLTKEXT_DLL_EXPORT int
AfxMessageBox(const SWString &text, int flags)
		{
		fl_message(text);

		return 0;
		}


FLTKEXT_DLL_EXPORT int
FXHtmlMessageBox(const SWString &text, int flags)
		{
		int		r=0;

		switch (flags & 0xf)
			{
			case FXMB_OK:
				fl_alert(text);
				r = FX_IDOK;
				break;

			case FXMB_OKCANCEL:
				r = fl_choice(text, "OK", "Cancel", NULL);
				if (r == 0) r = FX_IDOK;
				else r = FX_IDCANCEL;
				break;

			case FXMB_ABORTRETRYIGNORE:
				r = fl_choice(text, "Abort", "Retry", "Ignore", NULL);
				if (r == 0) r = FX_IDABORT;
				else if (r == 1) r = FX_IDRETRY;
				else r = FX_IDIGNORE;
				break;

			case FXMB_YESNOCANCEL:
				r = fl_choice(text, "Yes", "No", "Cancel", NULL);
				if (r == 0) r = FX_IDYES;
				else if (r == 1) r = FX_IDNO;
				else r = FX_IDCANCEL;
				break;
			
			case FXMB_YESNO:
				r = fl_choice(text, "Yes", "No", NULL);
				if (r == 0) r = FX_IDYES;
				else r = FX_IDNO;
				break;
			
			case FXMB_RETRYCANCEL:
				r = fl_choice(text, "Retry", "Cancel", NULL);
				if (r == 0) r = FX_IDRETRY;
				else r = FX_IDCANCEL;
				break;

			default:
				fl_message(text);
				r = 0;
				break;
			}

		return r;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextSize(const SWString &text)
		{
		int		w=0, h=0;

		fl_measure(text, w, h);

		return FXSize(w, h);
		}



FLTKEXT_DLL_EXPORT FXSize
fx_getMinSize(Fl_Widget *pWidget, bool assumeFixedSize)
		{
		bool	done=false;
		FXSize	res;

		if (!done)
			{
			FXWidget	*p=dynamic_cast<FXWidget *>(pWidget);

			if (p != NULL)
				{
				res = p->getMinSize();
				done = true;
				}
			}

		if (!done)
			{
			FXGroup	*p=dynamic_cast<FXGroup *>(pWidget);

			if (p != NULL)
				{
				res = p->getMinSize();
				done = true;
				}
			}

		if (!done)
			{
			FXWnd	*p=dynamic_cast<FXWnd *>(pWidget);

			if (p != NULL)
				{
				res = p->getMinSize();
				done = true;
				}
			}

		if (!done)
			{
			Fl_Menu_Bar	*p=dynamic_cast<Fl_Menu_Bar *>(pWidget);

			if (p != NULL)
				{
				res.cx = 64;
				res.cy = pWidget->h();
				done = true;
				}
			}

		if (!done)
			{
			if (assumeFixedSize)
				{
				/// Fl_Widgets are assumed to have a fixed size
				res.cx = pWidget->w();
				res.cy = pWidget->h();
				}
			}

		return res;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getMaxSize(Fl_Widget *pWidget, bool assumeFixedSize)
		{
		bool	done=false;
		FXSize	res;

		if (!done)
			{
			FXWidget	*p=dynamic_cast<FXWidget *>(pWidget);

			if (p != NULL)
				{
				res = p->getMaxSize();
				done = true;
				}
			}

		if (!done)
			{
			FXGroup	*p=dynamic_cast<FXGroup *>(pWidget);

			if (p != NULL)
				{
				res = p->getMaxSize();
				done = true;
				}
			}

		if (!done)
			{
			FXWnd	*p=dynamic_cast<FXWnd *>(pWidget);

			if (p != NULL)
				{
				res = p->getMaxSize();
				done = true;
				}
			}

		if (!done)
			{
			Fl_Menu_Bar	*p=dynamic_cast<Fl_Menu_Bar *>(pWidget);

			if (p != NULL)
				{
				res.cx = 0;
				res.cy = pWidget->h();
				done = true;
				}
			}

		if (!done)
			{
			if (assumeFixedSize)
				{
				/// Fl_Widgets are assumed to have a fixed size
				res.cx = pWidget->w();
				res.cy = pWidget->h();
				}
			}

		return res;
		}




FLTKEXT_DLL_EXPORT FXWidgetSizes
fx_getWidgetSizes(Fl_Widget *pWidget, bool assumeFixedSize)
		{
		bool			done=false;
		FXWidgetSizes	res;

		if (pWidget != NULL)
			{
			if (!done)
				{
				FXWidget	*p=dynamic_cast<FXWidget *>(pWidget);

				if (p != NULL)
					{
					res.currSize = p->getSize();
					res.minSize = p->getMinSize();
					res.maxSize = p->getMaxSize();
					done = true;
					}
				}

			if (!done)
				{
				FXGroup	*p=dynamic_cast<FXGroup *>(pWidget);

				if (p != NULL)
					{
					res.currSize = p->getSize();
					res.minSize = p->getMinSize();
					res.maxSize = p->getMaxSize();
					done = true;
					}
				}

			if (!done)
				{
				FXWnd	*p=dynamic_cast<FXWnd *>(pWidget);

				if (p != NULL)
					{
					res.currSize = p->getSize();
					res.minSize = p->getMinSize();
					res.maxSize = p->getMaxSize();
					done = true;
					}
				}

			if (!done)
				{
				if (assumeFixedSize)
					{
					/// Fl_Widgets are assumed to have a fixed size
					res.currSize.cx = res.minSize.cx = res.maxSize.cx = pWidget->w();
					res.currSize.cy = res.minSize.cy = res.maxSize.cy = pWidget->h();
					}
				}
			}

		return res;
		}


FLTKEXT_DLL_EXPORT void fx_getBorderWidths(Fl_Widget *pWidget, FXMargin &margins)
		{
		bool			done=false;
		FXWidgetSizes	res;

		margins.clear();

		if (pWidget != NULL)
			{
			if (!done)
				{
				FXWidget	*p=dynamic_cast<FXWidget *>(pWidget);

				if (p != NULL)
					{
					int		v=p->getBorderWidth();

					margins.left = margins.right = margins.top = margins.bottom = v;
					done = true;
					}
				}

			if (!done)
				{
				FXGroup	*p=dynamic_cast<FXGroup *>(pWidget);

				if (p != NULL)
					{
					int		v=p->getBorderWidth();

					margins.left = margins.right = margins.top = margins.bottom = v;
					done = true;
					}
				}

			if (!done)
				{
				FXWnd	*p=dynamic_cast<FXWnd *>(pWidget);

				if (p != NULL)
					{
					int		v=p->getBorderWidth();

					margins.left = margins.right = margins.top = margins.bottom = v;
					done = true;
					}
				}

			if (!done)
				{
				if (pWidget->box() != FL_NO_BOX)
					{
					margins.left = margins.right = margins.top = margins.bottom = 1;
					}
				}
			}
		}


FLTKEXT_DLL_EXPORT void fx_getContentsRect(Fl_Widget *pWidget, FXRect &rect)
		{
		bool			done=false;

		rect = FXRect();

		if (pWidget != NULL)
			{
			if (!done)
				{
				FXWidget	*p=dynamic_cast<FXWidget *>(pWidget);

				if (p != NULL)
					{
					p->getContentsRect(rect);
					done = true;
					}
				}

			if (!done)
				{
				FXGroup	*p=dynamic_cast<FXGroup *>(pWidget);

				if (p != NULL)
					{
					p->getContentsRect(rect);
					done = true;
					}
				}

			if (!done)
				{
				FXWnd	*p=dynamic_cast<FXWnd *>(pWidget);

				if (p != NULL)
					{
					p->getContentsRect(rect);
					done = true;
					}
				}

			if (!done)
				{
				if (fx_widget_isMainWindow(pWidget))
					{
					rect.left = 0;
					rect.top = 0;
					}
				else
					{
					rect.left = pWidget->x();
					rect.top = pWidget->y();
					}

				rect.right = rect.left + pWidget->w() - 1;
				rect.bottom = rect.top + pWidget->h() - 1;
				if (pWidget->box() != FL_NO_BOX)
					{
					rect.deflate(1, 1, 1, 1);
					}
				}
			}
		}

FLTKEXT_DLL_EXPORT bool
fx_widget_isMainWindow(Fl_Widget *pWidget)
		{
		bool	done=false;
		bool	res=false;

		if (pWidget != NULL)
			{
			if (!done)
				{
				Fl_Window	*p=dynamic_cast<Fl_Window *>(pWidget);

				if (p != NULL)
					{
					res = (p->parent() == NULL);
					done = true;
					}
				}

			}

		return res;
		}



FLTKEXT_DLL_EXPORT SWString
fx_event_name(int code)
		{
		SWString	res;

		switch (code)
			{
			case FL_NO_EVENT:						res = "FL_NO_EVENT";		break;
			case FL_PUSH:							res = "FL_PUSH";			break;
			case FL_RELEASE:						res = "FL_RELEASE";			break;
			case FL_ENTER:							res = "FL_ENTER";			break;
			case FL_LEAVE:							res = "FL_LEAVE";			break;
			case FL_DRAG:							res = "FL_DRAG";			break;
			case FL_FOCUS:							res = "FL_FOCUS";			break;
			case FL_UNFOCUS:						res = "FL_UNFOCUS";			break;
			case FL_KEYDOWN:						res = "FL_KEYDOWN";			break;
			case FL_KEYUP:							res = "FL_KEYUP";			break;
			case FL_CLOSE:							res = "FL_CLOSE";			break;
			case FL_MOVE:							res = "FL_MOVE";			break;
			case FL_SHORTCUT:						res = "FL_SHORTCUT";		break;
			case FL_DEACTIVATE:						res = "FL_DEACTIVATE";		break;
			case FL_ACTIVATE:						res = "FL_ACTIVATE";		break;
			case FL_HIDE:							res = "FL_HIDE";			break;
			case FL_SHOW:							res = "FL_SHOW";			break;
			case FL_PASTE:							res = "FL_PASTE";			break;
			case FL_SELECTIONCLEAR:					res = "FL_SELECTIONCLEAR";	break;
			case FL_MOUSEWHEEL:						res = "FL_MOUSEWHEEL";		break;
			case FL_DND_ENTER:						res = "FL_DND_ENTER";		break;
			case FL_DND_DRAG:						res = "FL_DND_DRAG";		break;
			case FL_DND_LEAVE:						res = "FL_DND_LEAVE";		break;
			case FL_DND_RELEASE:					res = "FL_DND_RELEASE";		break;
			case FL_FULLSCREEN:						res = "FL_FULLSCREEN";		break;
			case FL_SCREEN_CONFIGURATION_CHANGED:	res = "FL_SCREEN_CONFIGURATION_CHANGED";		break;
			default:								res.format("%d", code);		break;
			}

		return res;
		}


FLTKEXT_DLL_EXPORT bool
fx_event_isKeyPressed(int key)
		{
		bool	res=false;

		switch (key)
			{
			case FX_KEY_SHIFT:		res = (Fl::event_shift() != 0);		break;
			case FX_KEY_CONTROL:	res = (Fl::event_ctrl() != 0);		break;
			case FX_KEY_ALT:		res = (Fl::event_alt() != 0);		break;
			}

		return res;
		}


FLTKEXT_DLL_EXPORT void
fx_setWindowTransparency(Fl_Window *pWindow, sw_byte_t alpha)
		{
#if defined(SW_PLATFORM_WINDOWS)
		HWND		hwnd = fl_xid(pWindow);
		LONG_PTR	exstyle = GetWindowLongPtr(hwnd, GWL_EXSTYLE);
		
		if (!(exstyle & WS_EX_LAYERED))
			{
			SetWindowLongPtr(hwnd, GWL_EXSTYLE, exstyle | WS_EX_LAYERED);
			}
		
		SetLayeredWindowAttributes(hwnd, 0, alpha, LWA_ALPHA);
#elif defined(SW_PLATFORM_LINUX)
		Atom atom = XInternAtom(fl_display, "_NET_WM_WINDOW_OPACITY", False);
		sw_uint32_t opacity = (0xffffffff / 0xff) * alpha; // is_transparent ? 0xC0000000 : 0xFFFFFFFF;
		XChangeProperty(fl_display, fl_xid(pWindow), atom, XA_CARDINAL, 32, PropModeReplace, (unsigned char *)&opacity, 1);
		XFlush(fl_display);
#endif
		}


FLTKEXT_DLL_EXPORT Fl_Window *
fx_widget_getWindow(Fl_Widget *pWidget)
		{
		Fl_Window	*pWindow=NULL;

		if (pWidget != NULL)
			{
			pWindow = dynamic_cast<Fl_Window *>(pWidget);
			while (pWindow == NULL)
				{
				if (pWidget == pWidget->parent()) break;
				pWidget = pWidget->parent();
				if (pWidget == NULL) break;
				pWindow = dynamic_cast<Fl_Window *>(pWidget);
				}
			}

		return pWindow;
		}

