/**
***	fx_generic_ids.h
**/

#ifndef __FltkExt_fx_generic_ids_h__
#define __FltkExt_fx_generic_ids_h__

// Common Generic IDs
#define FX_IDSTATIC		0
#define FX_IDOK			1
#define FX_IDCANCEL		2
#define FX_IDABORT		3
#define FX_IDRETRY		4
#define FX_IDIGNORE		5
#define FX_IDYES		6
#define FX_IDNO			7
#define FX_IDCLOSE		8
#define FX_IDHELP		9
#define FX_IDTRYAGAIN	10
#define FX_IDCONTINUE	11

// Common event keys
#define FX_KEY_SHIFT	0xf0000001
#define FX_KEY_CONTROL	0xf0000002
#define FX_KEY_ALT		0xf0000003

#endif // __FltkExt_fx_generic_ids_h__