#include "FXStdButton.h"
#include "FXDialog.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXStdButton::FXStdButton(const SWString &text, bool defbtn, int X, int Y, int W, int H, const FXColor &textcolor) :
	Fl_Button(X, Y, W, H),
	m_text(text),
	m_defaultButton(defbtn)
		{
		label(m_text);
		labelcolor(textcolor.to_fl_color());

		if (m_defaultButton)
			{
			addShortcutKey(FL_Enter);
			addShortcutKey(FL_KP_Enter);
			}
		}


FXStdButton::~FXStdButton()
		{
		}


void
FXStdButton::setFont(const FXFont &font)
		{
		labelfont(font.fl_face());
		labelsize(font.height());
		}


void
FXStdButton::addShortcutKey(int v)
		{
		m_shortcutKeys.add(v);
		}


bool
FXStdButton::handleShortcutKey(int v)
		{
		bool	res=false;

		for (int i=0;i<(int)m_shortcutKeys.size();i++)
			{
			if (m_shortcutKeys[i] == v)
				{
				res = true;
				break;
				}
			}

		return res;
		}


int
FXStdButton::handle(int eventcode)
		{
		int		res=0;

		if (eventcode == FL_SHORTCUT && handleShortcutKey(Fl::event_key()))
			{
			simulate_key_action();
			do_callback();
			res = 1;
			}
		else
			{
			res = Fl_Button::handle(eventcode);
			}

		return res;
		}
