#include <FltkExt/FXFixedLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>

FXFixedLayout::FXFixedLayout(int layoutFlags) :
	FXLayout(layoutFlags)
		{
		}


FXFixedLayout::~FXFixedLayout()
		{
		}


FXSize
FXFixedLayout::getMinSize() const
		{
		FXSize	res;

		if (!isFlagSet(NoMinSize))
			{
			int		nchildren=getChildCount();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*pWidget=m_pOwner->child(i);

				res.cx = sw_math_max(res.cx, pWidget->x() + pWidget->w());
				res.cy = sw_math_max(res.cy, pWidget->y() + pWidget->h());
				}
			}

		return res;
		}


FXSize
FXFixedLayout::getMaxSize() const
		{
		FXSize	res;

		if (!isFlagSet(NoMaxSize))
			{
			int		nchildren=getChildCount();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*pWidget=m_pOwner->child(i);

				res.cx = sw_math_max(res.cx, pWidget->x() + pWidget->w());
				res.cy = sw_math_max(res.cy, pWidget->y() + pWidget->h());
				}
			}

		return res;
		}


void
FXFixedLayout::addWidget(Fl_Widget *pWidget)
		{
		addWidget(pWidget, pWidget->x(), pWidget->y(), pWidget->w(), pWidget->h());
		}



void
FXFixedLayout::addWidget(Fl_Widget *pWidget, int xpos, int ypos)
		{
		addWidget(pWidget, xpos, ypos, pWidget->w(), pWidget->h());
		}


void
FXFixedLayout::addWidget(ulong id, Fl_Widget *pWidget, int xpos, int ypos)
		{
		addWidget(id, pWidget, xpos, ypos, pWidget->w(), pWidget->h());
		}


void
FXFixedLayout::addWidget(ulong id, Fl_Widget *pWidget, int cx, int cy, int cw, int ch)
		{
		pWidget->id(id);
		addWidget(pWidget, cx, cy, cw, ch);
		}


void
FXFixedLayout::addWidget(Fl_Widget *pWidget, int cx, int cy, int cw, int ch)
		{
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		pWidget->resize(cx + X, cy + Y, cw, ch);
		m_pOwner->add(pWidget);
		
		FXPoint	pt;

		pt.x = cx;
		pt.y = cy;
		m_origins.add(pt);
		}


void
FXFixedLayout::layoutChildren()
		{
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		int		nchildren=getChildCount();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);

			pWidget->position(X+m_origins[i].x, Y+m_origins[i].y);
			}
		}
