/**
*** @file		FXMenuBarItem.h
*** @brief		A generic menu item class
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXMenuBarItem_h__
#define __FltkExt_FXMenuBarItem_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FL/Fl_Widget.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Menu_Item.H>
#include <FL/Fl.H>
#include <FL/fl_draw.H>



class FXMenuBar;

class FLTKEXT_DLL_EXPORT FXMenuBarItem
		{
public:
		const char *text;	    ///< menu item text, returned by label()
		int shortcut_;	    ///< menu item shortcut
		Fl_Callback *callback_;   ///< menu item callback
		void *user_data_;	    ///< menu item user_data for the menu's callback
		int flags;		    ///< menu item flags like FL_MENU_TOGGLE, FL_MENU_RADIO
		uchar labeltype_;	    ///< how the menu item text looks like
		Fl_Font labelfont_;	    ///< which font for this menu item text
		Fl_Fontsize labelsize_;   ///< size of menu item text
		Fl_Color labelcolor_;	    ///< menu item text color

		// advance N items, skipping submenus:
		const FXMenuBarItem *next(int=1) const;

		/**
		Advances a pointer by n items through a menu array, skipping
		the contents of submenus and invisible items. There are two calls so
		that you can advance through const and non-const data.
		*/
		FXMenuBarItem *next(int i=1) {
		return (FXMenuBarItem*)(((const FXMenuBarItem*)this)->next(i));}

		/** Returns the first menu item, same as next(0). */
		const FXMenuBarItem *first() const { return next(0); }

		/** Returns the first menu item, same as next(0). */
		FXMenuBarItem *first() { return next(0); }

		// methods on menu items:
		/**
		Returns the title of the item.
		A NULL here indicates the end of the menu (or of a submenu).
		A '&' in the item will print an underscore under the next letter,
		and if the menu is popped up that letter will be a "shortcut" to pick
		that item.  To get a real '&' put two in a row.
		*/
		const char* label() const {return text;}

		/**    See const char* FXMenuBarItem::label() const   */
		void label(const char* a) {text=a;}

		/**    See const char* FXMenuBarItem::label() const   */
		void label(Fl_Labeltype a,const char* b) {labeltype_ = a; text = b;}

		/**
		Returns the menu item's labeltype.
		A labeltype identifies a routine that draws the label of the
		widget.  This can be used for special effects such as emboss, or to use
		the label() pointer as another form of data such as a bitmap.
		The value FL_NORMAL_LABEL prints the label as text.
		*/
		Fl_Labeltype labeltype() const {return (Fl_Labeltype)labeltype_;}

		/**
		Sets the menu item's labeltype.
		A labeltype identifies a routine that draws the label of the
		widget.  This can be used for special effects such as emboss, or to use
		the label() pointer as another form of data such as a bitmap.
		The value FL_NORMAL_LABEL prints the label as text.
		*/
		void labeltype(Fl_Labeltype a) {labeltype_ = a;}

		/**
		Gets the menu item's label color.
		This color is passed to the labeltype routine, and is typically the
		color of the label text.  This defaults to FL_BLACK.  If this
		color is not black fltk will \b not use overlay bitplanes to draw
		the menu - this is so that images put in the menu draw correctly.
		*/
		Fl_Color labelcolor() const {return labelcolor_;}

		/**
		Sets the menu item's label color.
		\see Fl_Color FXMenuBarItem::labelcolor() const
		*/
		void labelcolor(Fl_Color a) {labelcolor_ = a;}
		/**
		Gets the menu item's label font.
		Fonts are identified by small 8-bit indexes into a table. See the
		enumeration list for predefined fonts. The default value is a
		Helvetica font. The function Fl::set_font() can define new fonts.
		*/
		Fl_Font labelfont() const {return labelfont_;}

		/**
		Sets the menu item's label font.
		Fonts are identified by small 8-bit indexes into a table. See the
		enumeration list for predefined fonts. The default value is a
		Helvetica font.  The function Fl::set_font() can define new fonts.
		*/
		void labelfont(Fl_Font a) {labelfont_ = a;}

		/** Gets the label font pixel size/height. */
		Fl_Fontsize labelsize() const {return labelsize_;}

		/** Sets the label font pixel size/height.*/
		void labelsize(Fl_Fontsize a) {labelsize_ = a;}

		/**
		Returns the callback function that is set for the menu item.
		Each item has space for a callback function and an argument for that
		function. Due to back compatibility, the FXMenuBarItem itself
		is not passed to the callback, instead you have to get it by calling
		((FXMenuBar*)w)->mvalue() where w is the widget argument.
		*/
		Fl_Callback_p callback() const {return callback_;}

		/**
		Sets the menu item's callback function and userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void callback(Fl_Callback* c, void* p) {callback_=c; user_data_=p;}

		/**
		Sets the menu item's callback function.
		This method does not set the userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void callback(Fl_Callback* c) {callback_=c;}

		/**
		Sets the menu item's callback function.
		This method does not set the userdata() argument.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void callback(Fl_Callback0*c) {callback_=(Fl_Callback*)c;}

		/**
		Sets the menu item's callback function and userdata() argument.
		This method does not set the userdata() argument.
		The argument \p is cast to void* and stored as the userdata()
		for the menu item's callback function.
		\see Fl_Callback_p Fl_MenuItem::callback() const
		*/
		void callback(Fl_Callback1*c, long p=0) {callback_=(Fl_Callback*)c; user_data_=(void*)(fl_intptr_t)p;}

		/**
		Gets the user_data() argument that is sent to the callback function.
		*/
		void* user_data() const {return user_data_;}
		/**
		Sets the user_data() argument that is sent to the callback function.
		*/
		void user_data(void* v) {user_data_ = v;}
		/**
		Gets the user_data() argument that is sent to the callback function.
		For convenience you can also define the callback as taking a long
		argument.  This method casts the stored userdata() argument to long
		and returns it as a \e long value.
		*/
		long argument() const {return (long)(fl_intptr_t)user_data_;}
		/**
		Sets the user_data() argument that is sent to the callback function.
		For convenience you can also define the callback as taking a long
		argument.  This method casts the given argument \p v to void*
		and stores it in the menu item's userdata() member.
		This may not be portable to some machines.
		*/
		void argument(long v) {user_data_ = (void*)(fl_intptr_t)v;}

		/** Gets what key combination shortcut will trigger the menu item. */
		int shortcut() const {return shortcut_;}

		/**
		Sets exactly what key combination will trigger the menu item.  The
		value is a logical 'or' of a key and a set of shift flags, for instance 
		FL_ALT+'a' or FL_ALT+FL_F+10 or just 'a'.  A value of
		zero disables the shortcut.

		The key can be any value returned by Fl::event_key(), but will usually 
		be an ASCII letter. Use a lower-case letter unless you require the shift 
		key to be held down.

		The shift flags can be any set of values accepted by Fl::event_state().
		If the bit is on that shift key must be pushed.  Meta, Alt, Ctrl, 
		and Shift must be off if they are not in the shift flags (zero for the 
		other bits indicates a "don't care" setting).
		*/
		void shortcut(int s) {shortcut_ = s;}
		/**
		Returns true if either FL_SUBMENU or FL_SUBMENU_POINTER
		is on in the flags. FL_SUBMENU indicates an embedded submenu
		that goes from the next item through the next one with a NULL
		label(). FL_SUBMENU_POINTER indicates that user_data()
		is a pointer to another menu array.
		*/
		int submenu() const {return flags&(FL_SUBMENU|FL_SUBMENU_POINTER);}
		/**
		Returns true if a checkbox will be drawn next to this item.
		This is true if FL_MENU_TOGGLE or FL_MENU_RADIO is set in the flags.
		*/
		int checkbox() const {return flags&FL_MENU_TOGGLE;}
		/**
		Returns true if this item is a radio item.
		When a radio button is selected all "adjacent" radio buttons are
		turned off.  A set of radio items is delimited by an item that has
		radio() false, or by an item with FL_MENU_DIVIDER turned on.
		*/
		int radio() const {return flags&FL_MENU_RADIO;}
		/** Returns the current value of the check or radio item.
		This is zero (0) if the menu item is not checked and
		non-zero otherwise. You should not rely on a particular value,
		only zero or non-zero.
		\note The returned value for a checked menu item as of FLTK 1.3.2
		is FL_MENU_VALUE (4), but may be 1 in a future version.
		*/
		int value() const {return flags&FL_MENU_VALUE;}
		/**
		Turns the check or radio item "on" for the menu item. Note that this
		does not turn off any adjacent radio items like set_only() does.
		*/
		void set() {flags |= FL_MENU_VALUE;}

		/** Turns the check or radio item "off" for the menu item. */
		void clear() {flags &= ~FL_MENU_VALUE;}

		void setonly();

		/** Gets the visibility of an item. */
		int visible() const {return !(flags&FL_MENU_INVISIBLE);}

		/** Makes an item visible in the menu. */
		void show() {flags &= ~FL_MENU_INVISIBLE;}

		/** Hides an item in the menu. */
		void hide() {flags |= FL_MENU_INVISIBLE;}

		/** Gets whether or not the item can be picked. */
		int active() const {return !(flags&FL_MENU_INACTIVE);}

		/** Allows a menu item to be picked. */
		void activate() {flags &= ~FL_MENU_INACTIVE;}
		/**
		Prevents a menu item from being picked. Note that this will also cause
		the menu item to appear grayed-out.
		*/
		void deactivate() {flags |= FL_MENU_INACTIVE;}
		/** Returns non 0 if FL_INACTIVE and FL_INVISIBLE are cleared, 0 otherwise. */
		int activevisible() const {return !(flags & (FL_MENU_INACTIVE|FL_MENU_INVISIBLE));}

		// compatibility for FLUID so it can set the image of a menu item...

		/** compatibility api for FLUID, same as a->label(this) */
		//void image(Fl_Image* a) {a->label(this);}

		/** compatibility api for FLUID, same as a.label(this) */
		//void image(Fl_Image& a) {a.label(this);}

		// used by menubar:
		int measure(int* h, const FXMenuBar*) const;
		void draw(int x, int y, int w, int h, const FXMenuBar*, int t=0) const;

		// popup menus without using an FXMenuBar widget:
		const FXMenuBarItem* popup(
		int X, int Y,
		const char *title = 0,
		const FXMenuBarItem* picked=0,
		const FXMenuBar* = 0) const;
		const FXMenuBarItem* pulldown(
		int X, int Y, int W, int H,
		const FXMenuBarItem* picked = 0,
		const FXMenuBar* = 0,
		const FXMenuBarItem* title = 0,
		int menubar=0) const;
		const FXMenuBarItem* test_shortcut() const;
		const FXMenuBarItem* find_shortcut(int *ip=0, const bool require_alt = false) const;

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		The callback is called with the stored user_data() as its second argument.
		You must first check that callback() is non-zero before calling this.
		*/
		void do_callback(Fl_Widget* o) const {callback_(o, user_data_);}

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		This call overrides the callback's second argument with the given value \p arg.
		You must first check that callback() is non-zero before calling this.
		*/
		void do_callback(Fl_Widget* o,void* arg) const {callback_(o, arg);}

		/**
		Calls the FXMenuBarItem item's callback, and provides the Fl_Widget argument.
		This call overrides the callback's second argument with the
		given value \p arg. long \p arg is cast to void* when calling
		the callback.
		You must first check that callback() is non-zero before calling this.
		*/
		void do_callback(Fl_Widget* o,long arg) const {callback_(o, (void*)(fl_intptr_t)arg);}

		// back-compatibility, do not use:

		/** back compatibility only \deprecated. */
		int checked() const {return flags&FL_MENU_VALUE;}

		/** back compatibility only \deprecated. */
		void check() {flags |= FL_MENU_VALUE;}

		/** back compatibility only \deprecated. */
		void uncheck() {flags &= ~FL_MENU_VALUE;}

		//int insert(int,const char*,int,Fl_Callback*,void* =0, int =0);
		//int add(const char*, int shortcut, Fl_Callback*, void* =0, int = 0);

		/** See int add(const char*, int shortcut, Fl_Callback*, void*, int) */
		//int add(const char*a, const char* b, Fl_Callback* c, void* d = 0, int e = 0) { return add(a,fl_old_shortcut(b),c,d,e);}

		int size() const ;
		};

#endif // __FltkExt_FXMenuBarItem_h__