#include "FXWnd.h"

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/fl_draw.H>

#include <FltkExt/fx_functions.h>
#include <FltkExt/FXLayout.h>
#include <FltkExt/fx_timer.h>

#include <swift/SWFileInfo.h>

void
FXWnd::classCallback(Fl_Widget *pWidget)
		{
		FXWnd	*pWnd=(FXWnd *)pWidget;

		if (pWnd->canCloseWindow())
			{
			pWnd->onClose();
			pWnd->hide();
			}
		}


static void
itemCallback(Fl_Widget *pWidget, void *p)
		{
		FXWnd *pGroup=dynamic_cast<FXWnd *>(pWidget->parent());

		if (pGroup != NULL)
			{
			pGroup->onItemCallback(pWidget, p);
			}
		}


FXWnd::FXWnd(const SWString &title, int W, int H, bool fixedSize, Fl_Widget *pOwner) :
	Fl_Double_Window(W, H),
	m_title(title),
	m_pOwner(pOwner),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL),
	m_pLayout(NULL),
	m_deleteLayoutOnClose(false),
	m_modalResult(0)
		{
		end();
		init();

		callback(classCallback);

		box(FL_NO_BOX);

		if (!fixedSize) resizable(*this);
		setTitle(title);

		if (m_pOwner != NULL) fx_widget_centerOver(this, m_pOwner);
		}


FXWnd::~FXWnd()
		{
		fx_timer_destroyAll(this);
		clearLayout();
		clearBackgroundImage();
		}



void
FXWnd::init()
		{
		m_flags = 0;
		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, FX_COLOR_NONE, 0, 0);

		onCreate();
		}


void
FXWnd::setTitle(const SWString &title)
		{
		m_title = title;
		label(m_title);
		}




void
FXWnd::getContentsRect(FXRect &rect) const
		{
		FXRect	ir;
		FXRect	cr;

		getInnerRect(ir);
		getClientRect(cr);

		ir.deflate(m_padding.left, m_padding.top, m_padding.right, m_padding.bottom);
		rect.left = sw_math_max(ir.left, cr.left);
		rect.top = sw_math_max(ir.top, cr.top);
		rect.right = sw_math_min(ir.right, cr.right);
		rect.bottom = sw_math_min(ir.bottom, cr.bottom);
		}


void
FXWnd::getClientRect(FXRect &rect) const
		{
		getInnerRect(rect);

		int	bw=getBorderWidth();
		int	cw=getCornerWidth();
		
		if (cw > bw)
			{
			bw = (int)((cw-bw) * 0.293);
			rect.deflate(bw, bw, bw, bw);
			}
		}

void
FXWnd::getInnerRect(FXRect &rect) const
		{
		rect.left = 0;
		rect.right = w()-1;
		rect.top = 0;
		rect.bottom = h()-1;
		
		int	bw=getBorderWidth();

		rect.deflate(bw, bw, bw, bw);
		}


void
FXWnd::getWidgetRect(FXRect &rect) const
		{
		rect.left = x();
		rect.right = x()+w()-1;
		rect.top = y();
		rect.bottom = y()+h()-1;
		}



FXSize
FXWnd::getChildrenSize() const
		{
		FXRect	cr;

		getChildrenRect(cr);

		return cr.size();
		}


void
FXWnd::getChildrenRect(FXRect &rect) const
		{
		rect = FXRect(x(), y(), x(), y());
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=child(i);
			int			l=pWidget->x();
			int			t=pWidget->y();
			int			r=l+pWidget->w()-1;
			int			b=t+pWidget->h()-1;

			if (i == 0 || l < rect.left) rect.left = l;
			if (i == 0 || t < rect.top) rect.top = t;
			if (i == 0 || r > rect.right) rect.right = r;
			if (i == 0 || b > rect.bottom) rect.bottom = b;
			}
		}



bool
FXWnd::getWindowState(FXWindowState &state)
		{
		Fl_Window_State	ws;

		saveWindowState(ws);
		state.position.x = ws.xpos;
		state.position.y = ws.ypos;
		state.size.cx = ws.width;
		state.size.cy = ws.height;
		state.flags = ws.flags;

		return true;
		}


bool
FXWnd::isFullScreen() const
		{
		return ((flags() & Fl_Widget::FULLSCREEN) != 0);
		}



bool
FXWnd::setWindowState(const FXWindowState &state)
		{
		if (state.size.cx > 0 && state.size.cy > 0)
			{
			Fl_Window_State	ws;

			ws.xpos = state.position.x;
			ws.ypos = state.position.y;
			ws.width = state.size.cx;
			ws.height = state.size.cy;
			ws.flags = state.flags;
			restoreWindowState(ws);
			if ((ws.flags & FXWindowState::Fullscreen) != 0)
				{
				 if (!isFullScreen())
					{
					fullscreen();
					}
				}
			else
				{
				 if (isFullScreen())
					{
					fullscreen_off();
					}
				}
			}

		return true;
		}



int
FXWnd::runModalLoop()
		{
		fx_widget_centerOver(this, m_pOwner);

		set_modal();
		show();

		m_modalResult = 0;
		
		// Give the focus to the first child
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=child(i);

			if (pWidget->take_focus()) break;
			}

		while (shown())
			{
			Fl::wait();
			}
		
		set_non_modal();

		return m_modalResult;
		}


void
FXWnd::endModalLoop(int r)
		{
		m_modalResult = r;
		hide();	
		}


// Definitions to support FXWidget FXGroup and FXWnd Common Methods
// to make the job of keeping FXWidget and FXWnd in sync easier

#define FLAG_LEFT_MOUSE		0x01		///< The left mouse button is down
#define FLAG_MIDDLE_MOUSE	0x02		///< The middle mouse button is down
#define FLAG_RIGHT_MOUSE	0x04		///< The right mouse button is down
#define FLAG_MOUSE_OVER		0x08		///< The mouse is over the widget
#define FLAG_FOCUS			0x10		///< The widget has focus


// FXGroup Common Methods


FXSize
FXWnd::getMinSize() const
		{
		FXSize	res;

		if (m_pLayout != NULL)
			{
			res = m_pLayout->getMinSize();
			}

		return res;
		}


FXSize
FXWnd::getMaxSize() const
		{
		FXSize	res;

		if (m_pLayout != NULL)
			{
			res = m_pLayout->getMaxSize();
			}

		return res;
		}

void
FXWnd::layoutChildren()
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->layoutChildren();
			}
		}


void
FXWnd::clearLayout()
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->setOwner(NULL);
			if (m_deleteLayoutOnClose) delete m_pLayout;
			m_pLayout = NULL;
			m_deleteLayoutOnClose = false;
			}
		}

void
FXWnd::setLayout(FXLayout *pLayout, bool delflag)
		{
		clearLayout();
		if (pLayout != NULL)
			{
			m_pLayout = pLayout;
			m_deleteLayoutOnClose = delflag;
			m_pLayout->setOwner(this);
			}
		}

  
/// Add the widget to the layout
void
FXWnd::add(Fl_Widget *pWidget)
		{
		add(pWidget, false);
		}


void
FXWnd::add(sw_uint32_t id, Fl_Widget *pWidget, bool addToGroupCallback)
		{
		pWidget->id(id);
		add(pWidget, addToGroupCallback);
		}


void
FXWnd::add(Fl_Widget *pWidget, bool addToGroupCallback)
		{
		if (m_pLayout != NULL)
			{
			m_pLayout->addWidget(pWidget);
			}
		else
			{
			Fl_Double_Window::add(pWidget);
			}

		if (addToGroupCallback)
			{
			pWidget->callback(itemCallback);
			}
		}


sw_uint32_t
FXWnd::setIntervalTimer(sw_uint32_t localid, double seconds, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, seconds, this, pData);
		}


sw_uint32_t
FXWnd::setIntervalTimerMS(sw_uint32_t localid, int ms, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, (double)ms / 1000.0, this, pData);
		}


void
FXWnd::killIntervalTimer(sw_uint32_t id)
		{
		fx_timer_destroy(this, id);
		}


// FXWidget Common Methods
void
FXWnd::invalidate()
		{
		fx_invalidate(this);
		}


// FXWidget Common Methods
void
FXWnd::invalidateAll()
		{
		for (int i=0;i<children();i++)
			{
			fx_invalidate(child(i));
			}

		fx_invalidate(this);
		}

void
FXWnd::clearBackgroundImage()
		{
		if (m_pBgImage != NULL)
			{
			m_pBgImage->release();
			m_pBgImage = NULL;
			}

		delete m_pBgImageCopy;
		m_pBgImageCopy = NULL;
		}

void
FXWnd::setBackgroundImage(const SWString &filename)
		{
		clearBackgroundImage();
		if (SWFileInfo::isFile(filename))
			{
			m_pBgImage = Fl_Shared_Image::get(filename);
			}
		}


void
FXWnd::setBorder(int style, const FXColor &color, int width, int corner)
		{
		m_bdStyle = style;
		m_bdColor = color;
		m_bdWidth = width;
		m_bdCorner = corner;
		}




void
FXWnd::onDrawBorder()
		{
		FXRect	cr;

		getClientRect(cr);

		if (m_bdColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;

			fx_draw_rect(cr, m_bdWidth, corner, m_bdColor, FX_COLOR_NONE); 
			}
		}


void
FXWnd::onDrawBackground()
		{
		FXRect	cr;

		getClientRect(cr);

		if (m_bgColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;

			fx_draw_rect(cr, m_bdWidth, corner, FX_COLOR_NONE, m_bgColor); 
			}

		if (m_pBgImage != NULL)
			{
			if (m_pBgImageCopy != NULL)
				{
				if (m_pBgImageCopy->w() != w() || m_pBgImageCopy->h() != h())
					{
					delete m_pBgImageCopy;
					m_pBgImageCopy = NULL;
					}
				}

			if (m_pBgImageCopy == NULL)
				{
				m_pBgImageCopy = m_pBgImage->copy(w(), h());
				}
			
			if (m_pBgImageCopy != NULL)
				{
				m_pBgImageCopy->draw(cr.left, cr.top, cr.width(), cr.height(), 0, 0);
				}
			}
		}


void
FXWnd::onDraw()
		{
		Fl_Double_Window::draw();
		}


void
FXWnd::draw()
		{
		if (damage() & ~FL_DAMAGE_CHILD)
			{
			onDrawBackground();
			onDrawBorder();
			}

		onDraw();
		}

void
FXWnd::onCreate()
		{
		// Call onCreate for all our children
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*p=*a++;
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(p);
			FXGroup		*pGroup=dynamic_cast<FXGroup *>(p);
			FXWnd		*pWnd=dynamic_cast<FXWnd *>(p);

			if (pWidget != NULL) pWidget->onCreate();
			else if (pGroup != NULL) pGroup->onCreate();
			else if (pWnd != NULL) pWnd->onCreate();
			}
		}

void
FXWnd::onClose()
		{
		// Call onClose for all our children
		int		nwidgets=children();
		Fl_Widget *const*a=array();

		for (int i=0;i<nwidgets;i++)
			{
			Fl_Widget	*p=*a++;
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(p);
			FXGroup		*pGroup=dynamic_cast<FXGroup *>(p);
			FXWnd		*pWnd=dynamic_cast<FXWnd *>(p);

			if (pWidget != NULL) pWidget->onClose();
			else if (pGroup != NULL) pGroup->onClose();
			else if (pWnd != NULL) pWnd->onClose();
			}
		}
		
		
bool		FXWnd::canCloseWindow()												{ return true; }
bool		FXWnd::hasFocus() const												{ return ((m_flags & FLAG_FOCUS) != 0); }
bool		FXWnd::hasMouseOver() const											{ return ((m_flags & FLAG_MOUSE_OVER) != 0); }
int			FXWnd::onFocus()													{ return 0; }
int			FXWnd::onUnfocus()													{ return 0; }
int			FXWnd::onMouseEnter()												{ return 1; }
int			FXWnd::onMouseLeave()												{ return 1; }
int			FXWnd::onLeftMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXWnd::onMiddleMouseButton(int action, const FXPoint &pt)			{ return 0; }
int			FXWnd::onRightMouseButton(int action, const FXPoint &pt)			{ return 0; }
int			FXWnd::onMouseMove(const FXPoint &pt)								{ return 0; }
int			FXWnd::onMouseDrag(const FXPoint &pt)								{ return 0; }
int			FXWnd::onMouseWheel(int dx, int dy, const FXPoint &pt)				{ return 0; }
bool		FXWnd::disabled() const												{ return !enabled(); }
bool		FXWnd::enabled() const												{ return (active() != 0); }
int			FXWnd::onDragAndDrop(int action, const FXPoint &pt)					{ return 0; }
int			FXWnd::onPaste(const SWString &text, const FXPoint &pt)				{ return 0; }
int			FXWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
int			FXWnd::onKeyUp(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
bool		FXWnd::onTimer(sw_uint32_t id, void *data)							{ return true; }
void		FXWnd::onItemCallback(Fl_Widget *pWidget, void *p)					{ }
int			FXWnd::onCommand(sw_uint32_t id)									{ return 0; }
int			FXWnd::onShow()														{ return 0; }
int			FXWnd::onHide()														{ return 0; }


int
FXWnd::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;
			}

		return res;
		}


int
FXWnd::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(FX_BUTTON_UP, pt);
				break;
			}

		return res;
		}


// Scrolling support
void		FXWnd::onHScroll(int pos)									{ }
void		FXWnd::onVScroll(int pos)									{ }
void		FXWnd::onScrollUp()										{ }
void		FXWnd::onScrollDown()										{ }
void		FXWnd::onScrollLeft()										{ }
void		FXWnd::onScrollRight()									{ }
void		FXWnd::onScrollHome()										{ }
void		FXWnd::onScrollEnd()										{ }
void		FXWnd::onScrollPageUp()									{ }
void		FXWnd::onScrollPageDown()									{ }
void		FXWnd::onScrollPageHome()									{ }
void		FXWnd::onScrollPageEnd()									{ }
void		FXWnd::onScrollPageLeft()									{ }
void		FXWnd::onScrollPageRight()								{ }

void
FXWnd::onScroll(int bar, int action, int pos)
		{

		if (bar == FX_SCROLLBAR_HORIZONTAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollLeft();			break;
				case FX_SCROLL_DOWN	:		onScrollRight();		break;
				case FX_SCROLL_FIRST:		onScrollHome();			break;
				case FX_SCROLL_LAST:		onScrollEnd();			break;
				case FX_SCROLL_PAGEUP:		onScrollPageLeft();		break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageRight();	break;
				case FX_SCROLL_TRACK:		onHScroll(pos);			break;
				}
			}
		else if (bar == FX_SCROLLBAR_VERTICAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollUp();		break;
				case FX_SCROLL_DOWN	:		onScrollDown();		break;
				case FX_SCROLL_FIRST:		onScrollPageHome();	break;
				case FX_SCROLL_LAST:		onScrollPageEnd();	break;
				case FX_SCROLL_PAGEUP:		onScrollPageUp();	break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageDown();	break;
				case FX_SCROLL_TRACK:		onVScroll(pos);		break;
				}
			}

		invalidate();
		}



int
FXWnd::onShortcut(int code, const FXPoint &pt)
		{
		int		res=0;

		if (Fl::event_key() == FL_Escape)
			{
			res = 1;
			}
		
		return res;
		}


int
FXWnd::handle(int eventcode)
		{
		int		res=0;

		m_lastEventCode = eventcode;
		m_lastEventPoint.x = Fl::event_x();
		m_lastEventPoint.y = Fl::event_y();

		switch (eventcode)
			{
			case FL_FOCUS:
				m_flags |= FLAG_FOCUS;
				res = onFocus();
				break;
			
			case FL_UNFOCUS:
				m_flags &= ~FLAG_FOCUS;
				res = onUnfocus();
				break;
			
			case FL_ENTER:
				m_flags |= FLAG_MOUSE_OVER;
				res = onMouseEnter();
				break;
			
			case FL_LEAVE:
				m_flags &= ~(FLAG_MOUSE_OVER|FLAG_LEFT_MOUSE|FLAG_MIDDLE_MOUSE|FLAG_RIGHT_MOUSE);
				res = onMouseLeave();
				break;
				
			case FL_MOVE:
				res = onMouseMove(m_lastEventPoint);
				break;
				
			case FL_DRAG:
				res = onMouseDrag(m_lastEventPoint);
				break;
		
			case FL_PUSH:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags |= FLAG_LEFT_MOUSE;		break;
					case FL_MIDDLE_MOUSE:	m_flags |= FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags |= FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseDown(button, m_lastEventPoint);
				}
				break;

			case FL_RELEASE:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags &= ~FLAG_LEFT_MOUSE;	break;
					case FL_MIDDLE_MOUSE:	m_flags &= ~FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags &= ~FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseUp(button, m_lastEventPoint);
				}
				break;
			
			case FL_MOUSEWHEEL:
				res = onMouseWheel(Fl::event_dx(), Fl::event_dy(), m_lastEventPoint);
				break;

			case FL_CLOSE:
				onClose();
				break;

			case FL_DND_ENTER:
				res = onDragAndDrop(1, m_lastEventPoint);
				break;

			case FL_DND_DRAG:
				res = onDragAndDrop(2, m_lastEventPoint);
				break;

			case FL_DND_LEAVE:
				res = onDragAndDrop(3, m_lastEventPoint);
				break;

			case FL_DND_RELEASE:
				res = onDragAndDrop(4, m_lastEventPoint);
				break;

			case FL_PASTE:
				res = onPaste(Fl::event_text(), m_lastEventPoint);
				break;
   			
			case FL_KEYDOWN:
				res = onKeyDown(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;
   			
			case FL_KEYUP:
				res = onKeyUp(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;

			case FL_SHORTCUT:
				res = onShortcut(Fl::event_key(), m_lastEventPoint);
				break;
			
			case FL_SHOW:
				res = onShow();
				break;
			
			case FL_HIDE:
				res = onHide();
				break;
			}

		// TRACE("FXWnd(%x)::handle(%s) at %d,%d = %d\n", this, fx_event_name(eventcode).c_str(), m_lastEventPoint.x, m_lastEventPoint.y, res);
		if (res == 0) res = Fl_Double_Window::handle(eventcode);

		return res;
		}
	


void
FXWnd::resize(int X, int Y, int W, int H)
		{
		if (m_pLayout != NULL)
			{
			// We are handling the resizing, so disable it whilst the underlying code works
			Fl_Widget	*tmp=resizable();

			resizable(NULL);
			Fl_Double_Window::resize(X,Y,W,H);
			resizable(tmp);

			FXSize	minsize, maxsize;

			minsize = m_pLayout->getMinSize();
			maxsize = m_pLayout->getMaxSize();

			int		width=W, height=H;

			if (maxsize.cx != 0 && width > maxsize.cx) width = maxsize.cx;
			if (width < minsize.cx) width = minsize.cx;

			if (maxsize.cy != 0 && height > maxsize.cy) height = maxsize.cy;
			if (height < minsize.cy) height = minsize.cy;

			m_pLayout->layoutChildren();
			}
		else
			{
			Fl_Double_Window::resize(X,Y,W,H);
			}
		}


void
FXWnd::setItemFocus(int itemid)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			pWidget->take_focus();
			}
		}
