/*
**	FXWaitCursor.h
*/

#ifndef __FltkExt_FXWaitCursor_h__
#define __FltkExt_FXWaitCursor_h__

#include <FltkExt/FltkExt.h>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Window.H>

class FLTKEXT_DLL_EXPORT FXWaitCursor
		{
public:
		FXWaitCursor(Fl_Widget *pWidget);
		~FXWaitCursor();

protected:
		Fl_Window	*m_pWindow;
		};

#endif // __FltkExt_FXWaitCursor_h__