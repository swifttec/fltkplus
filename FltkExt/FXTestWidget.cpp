#include "FXTestWidget.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


FXTestWidget::FXTestWidget(int X, int Y, int W, int H, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H),
	m_drawcount(0)
		{
		setBackgroundColor(bgcolor);
		setForegroundColor(bgcolor.toOpposite().toMonochrome());
		setBorder(0, bgcolor.toShade(0.5), 3, 0);
		setMinSize(100, 20);
		}


FXTestWidget::FXTestWidget(const FXColor &bgcolor) :
	m_drawcount(0)
		{
		setBackgroundColor(bgcolor);
		setForegroundColor(bgcolor.toOpposite().toMonochrome());
		setBorder(0, bgcolor.toShade(0.5), 3, 0);
		setMinSize(100, 20);
		}

FXTestWidget::FXTestWidget(const FXColor &bgcolor, const FXColor &fgcolor) :
	m_drawcount(0)
		{
		setBackgroundColor(bgcolor);
		setForegroundColor(fgcolor);
		setBorder(0, bgcolor.toShade(0.5), 3, 0);
		}

FXTestWidget::~FXTestWidget()
		{
		}


void
FXTestWidget::onDraw()
		{
		SWString		s;
		FXRect			cr;

		getClientRect(cr);

		int			ypos=cr.top + m_bdWidth + 2;
		int			xpos=cr.left + m_bdWidth + 2;

		fl_push_clip(cr.left + 1, cr.top + 1, cr.width() - (2 * m_bdWidth), cr.height() - (2 * m_bdWidth));

#define LINEHEIGHT	11

		s.format("draw %d - at %d,%d", ++m_drawcount, x(), y());
		fl_font(FL_HELVETICA, LINEHEIGHT-2);
		fl_color(m_fgColor.to_fl_color());
		fl_draw(s, xpos, ypos, w(), LINEHEIGHT, FL_ALIGN_LEFT);

		FXSize	sz=getSize();
		FXSize	minsize=getMinSize();
		FXSize	maxsize=getMaxSize();

		s.format("size = %d x %d", sz.cx, sz.cy);
		fl_draw(s, xpos, ypos += 10, w(), LINEHEIGHT, FL_ALIGN_LEFT);

		s.format("minsize = %d x %d, maxsize = %d x %d", minsize.cx, minsize.cy, maxsize.cx, maxsize.cy);
		fl_draw(s, xpos, ypos += 10, w(), LINEHEIGHT, FL_ALIGN_LEFT);

		s.format("rect left=%d top=%d right=%d bottom=%d", cr.left, cr.top, cr.right, cr.bottom);
		fl_draw(s, xpos, ypos += 10, w(), LINEHEIGHT, FL_ALIGN_LEFT);

		s.format("Last event %d at %d x %d", m_lastEventCode, m_lastEventPoint.x, m_lastEventPoint.y);

		if ((m_flags & 1) != 0)	s += ", left";
		if ((m_flags & 2) != 0)	s += ", middle";
		if ((m_flags & 4) != 0)	s += ", right";

		fl_draw(s, xpos, ypos += 10, w(), LINEHEIGHT, FL_ALIGN_LEFT);

		fl_pop_clip();
		}


int	FXTestWidget::onMouseDown(int button, const FXPoint &pt)	{ redraw(); return 1; }
int	FXTestWidget::onMouseUp(int button, const FXPoint &pt)		{ redraw(); return 1; }
int	FXTestWidget::onMouseMove(const FXPoint &pt)				{ redraw(); return 1; }
int	FXTestWidget::onMouseDrag(const FXPoint &pt)				{ redraw(); return 1; }
int	FXTestWidget::onFocus()										{ redraw(); return 1; }
int	FXTestWidget::onUnfocus()									{ redraw(); return 1; }
int	FXTestWidget::onMouseEnter()								{ redraw(); return 1; }
int	FXTestWidget::onMouseLeave()								{ redraw(); return 1; }


/*
int
FXTestWidget::handle(int eventcode)
		{
		int		res=0;

		m_lastEventCode = eventcode;
		m_lastEventPoint.x = Fl::event_x();
		m_lastEventPoint.y = Fl::event_y();

		switch (eventcode)
			{
			case FL_ENTER:
				res = 1;
				break;

			case FL_PUSH:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_mouseButtons |= 1;	break;
					case FL_MIDDLE_MOUSE:	m_mouseButtons |= 2;	break;
					case FL_RIGHT_MOUSE:	m_mouseButtons |= 4;	break;
					}

				res = 1;
				}
				break;

			case FL_RELEASE:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_mouseButtons &= ~1;	break;
					case FL_MIDDLE_MOUSE:	m_mouseButtons &= ~2;	break;
					case FL_RIGHT_MOUSE:	m_mouseButtons &= ~4;	break;
					}

				res = m_mouseButtons?1:0;
				}
				break;

			case FL_DRAG:
				// Return 1 
				res = 1;
				break;
			}

		redraw();


		return res;
		}
*/