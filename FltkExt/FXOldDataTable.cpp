#include <FltkExt/FXDataTable.h>
#include <FltkExt/FXWnd.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

void
FXDataTable::onEventCallback(Fl_Widget*, void* data)
		{
		FXDataTable	*o = (FXDataTable *)data;
		
		o->onCallback();
		}

void
FXDataTable::onCallback()
		{
		int				R = callback_row();
		int				C = callback_col();
		TableContext	context = callback_context();

		if (context == Fl_Table::CONTEXT_COL_HEADER)
			{
			set_selection(-1, -1, -1, -1);
			}
		
		FXWnd	*pWnd=dynamic_cast<FXWnd *>(parent());
		FXGroup	*pGroup=dynamic_cast<FXGroup *>(parent());

		if (pGroup != NULL)
			{
			pGroup->onItemCallback(this);
			}
		else if (pWnd != NULL)
			{
			pWnd->onItemCallback(this);
			}
		}



FXDataTable::FXDataTable(int X, int Y, int W, int H) :
	Fl_Table(X, Y, W, H),
	m_columnsChanged(false),
	m_cellsChanged(false),
	m_mutex(true)
		{
		m_columnGap = 0;
		m_rowGap = 1;
		m_tableBackgroundColor = "#444";
		m_tableBorderColor = "#222";
		m_cellPadding.left = m_cellPadding.right = 4;
		m_cellPadding.top = m_cellPadding.bottom = 2;

		m_headerFont = FXFont("Arial", 12, FXFont::Bold);
		m_headerRow.bgcolor = "#bbb";
		m_headerRow.fgcolor = "#000";

		m_cellFont = FXFont("Arial", 12, FXFont::Normal);
		m_rowForegroundColor[0] = m_rowForegroundColor[1] = "#000";
		m_rowBackgroundColor[0] = "#f8f8f8";
		m_rowBackgroundColor[1] = "#fff";

		m_selectedRowForegroundColor[0] = m_selectedRowForegroundColor[1] = "#000";
		m_selectedRowBackgroundColor[0] = "#def";
		m_selectedRowBackgroundColor[1] = "#def";

		m_invertCellColorsOnSelection = false;
		m_invertRowColorsOnSelection = false;


		callback(onEventCallback, this);
		}


FXDataTable::~FXDataTable()
		{
		removeAllRowData();
		}


void
FXDataTable::addColumn(const SWString &title, int align)
		{
		SWMutexGuard	guard(m_mutex);

		Column	col;

		col.title = title;
		col.align = align;
		col.width = col.maxWidth = col.minWidth = 0;

		m_columns.add(col);
		m_columnsChanged = true;

		fl_font(m_headerFont.fl_face(), m_headerFont.height());

		Cell	&cell=m_headerRow.cells[(int)m_columns.size()-1];

		cell.contents = title;
		cell.size = fx_getTextSize(cell.contents);
		cell.size.cx += m_cellPadding.left + m_cellPadding.right;
		cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;

		cols((int)m_columns.size());
		col_header(1);
		}


void
FXDataTable::calculateRowSize(Row &row)
		{
		SWMutexGuard	guard(m_mutex);

		int		ncols=getColumnCount();

		row.size = FXSize(0, 0);
		for (int i=0;i<ncols;i++)
			{
			Cell	&cell=row.cells[i];
			Column	&column=m_columns[i];

			column.width = sw_math_max(column.width, cell.size.cx);
			row.size.cy = sw_math_max(row.size.cy, cell.size.cy);
			}
		}


void
FXDataTable::recalculate()
		{
		SWMutexGuard	guard(m_mutex);

		int		ncols=getColumnCount();
		int		nrows=getRowCount();

		m_cellsChanged = m_columnsChanged = false;
		for (int i=0;i<ncols;i++)
			{
			Column	&column=m_columns[i];

			column.width = 0;
			}

		calculateRowSize(m_headerRow);

		for (int j=0;j<nrows;j++)
			{
			calculateRowSize(*m_rows[j]);
			}

		for (int i=0;i<ncols;i++)
			{
			col_width(i, m_columns[i].width, true);
			}

		table_resized();
		}



void
FXDataTable::removeAllRowData()
		{
		SWMutexGuard	guard(m_mutex);

		for (int i=0;i<(int)m_rows.size();i++)
			{
			delete m_rows[i];
			}

		m_rows.clear();
		rows(0);
		}


void
FXDataTable::removeAllRows()
		{
		SWMutexGuard	guard(m_mutex);

		removeAllRowData();
		recalculate();
		}

int
FXDataTable::addRow(bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		int		index=(int)getRowCount();
		Row		*pRow=new Row();
		
		m_rows[index] = pRow;
		rows(index+1, batchmode);

		return index;
		}


void
FXDataTable::setRowValues(int row, const SWStringArray &values)
		{
		SWMutexGuard	guard(m_mutex);

		for (int i=0;i<(int)values.size();i++)
			{
			setCellString(row, i, values.get(i));
			}
		}


int
FXDataTable::addRow(const SWStringArray &values, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		int		index=addRow(batchmode);
		int		ncols=getColumnCount();
		Row		&row=*m_rows[index];

		fl_font(m_cellFont.fl_face(), m_cellFont.height());
		for (int i=0;i<ncols;i++)
			{
			Cell	&cell=row.cells[i];
			SWString	s;

			if (i < (int)values.size()) s = values.get(i);
			cell.contents = s;
			cell.size = fx_getTextSize(s);
			cell.size.cx += m_cellPadding.left + m_cellPadding.right;
			cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;
			}

		m_cellsChanged = true;

		return index;
		}


int
FXDataTable::insertRow(int atidx, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		int		count=(int)getRowCount();

		if (atidx < 0) atidx = 0;
		if (atidx > count) atidx = count;

		Row		*pRow=new Row();

		pRow->bgcolor = pRow->fgcolor = FX_COLOR_DEFAULT;
		m_rows.insert(pRow, atidx);
		rows((int)m_rows.size(), batchmode);

		return atidx;
		}

void
FXDataTable::deleteRow(int row, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >= 0 && row < (int)m_rows.size())
			{
			delete m_rows[row];
			m_rows.remove(row);
			rows((int)m_rows.size(), batchmode);
			}
		}


void
FXDataTable::endBatchMode()
		{
		table_resized();
		}



int
FXDataTable::insertRow(int atidx, const SWStringArray &values, bool batchmode)
		{
		SWMutexGuard	guard(m_mutex);

		int		index=insertRow(atidx);
		int		ncols=getColumnCount();
		Row		&row=*m_rows[index];

		fl_font(m_cellFont.fl_face(), m_cellFont.height());
		for (int i=0;i<ncols;i++)
			{
			Cell	&cell=row.cells[i];
			SWString	s;

			if (i < (int)values.size()) s = values.get(i);
			cell.contents = s;
			cell.size = fx_getTextSize(s);
			cell.size.cx += m_cellPadding.left + m_cellPadding.right;
			cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;
			}

		m_cellsChanged = true;

		return index;
		}

void
FXDataTable::setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			m_rows[row]->fgcolor = fgcolor;
			m_rows[row]->bgcolor = bgcolor;
			}
		}


void
FXDataTable::setCell(int row, int col, const SWString &contents, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		setCellString(row, col, contents);
		setCellColors(row, col, fgcolor, bgcolor);
		}


void
FXDataTable::setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				Cell	&cell=m_rows[row]->cells[col];

				cell.fgcolor = fgcolor;
				cell.bgcolor = bgcolor;
				}
			}
		}

void
FXDataTable::setCellString(int row, int col, const SWString &contents)
		{
		SWMutexGuard	guard(m_mutex);

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				Cell	&cell=m_rows[row]->cells[col];

				fl_font(m_cellFont.fl_face(), m_cellFont.height());
				cell.contents = contents;
				cell.size = fx_getTextSize(contents);
				cell.size.cx += m_cellPadding.left + m_cellPadding.right;
				cell.size.cy += m_cellPadding.top + m_cellPadding.bottom;
				m_cellsChanged = true;
				}
			}
		}


SWString
FXDataTable::getCellString(int row, int col) const
		{
		SWMutexGuard	guard(m_mutex);
		SWString		res;

		if (row >=0 && row < getRowCount())
			{
			if (col >= 0 && col < getColumnCount())
				{
				res = m_rows.get(row)->cells.get(col).contents;
				}
			}
		
		return res;
		}


/*
int
FXDataTable::drawRow(int idx, Row &row, int xpos, int ypos)
		{
		int		ncols=getColumnCount();
		FXColor	fgcolor, bgcolor;


		if (idx < 0)
			{
			// Header row
			fl_font(m_headerFont.fl_face(), m_headerFont.height());
			}
		else
			{
			// Normal row
			fl_font(m_cellFont.fl_face(), m_cellFont.height());
			}

		for (int i=0;i<ncols;i++)
			{
			Cell	&cell=row.cells[i];
			Column	&column=m_columns[i];

			if (idx < 0)
				{
				// Header row
				bgcolor = row.bgcolor;
				fgcolor = row.fgcolor;
				}
			else
				{
				// Normal row
				if (row.bgcolor == FX_COLOR_DEFAULT) bgcolor = m_rowBackgroundColor[idx % 2];
				else bgcolor = row.bgcolor;

				if (row.fgcolor == FX_COLOR_DEFAULT) fgcolor = m_rowForegroundColor[idx % 2];
				else fgcolor = row.fgcolor;
				}

			if (row.bgcolor != FX_COLOR_NONE) fl_rectf(xpos, ypos, column.width, row.size.cy, bgcolor.to_fl_color());

			fl_color(fgcolor.to_fl_color());
			fx_draw_text(cell.contents, xpos, ypos, column.width, row.size.cy, column.align | FX_VALIGN_MIDDLE);
			xpos += column.width + m_columnGap;
			}

		return row.size.cy;
		}
*/

/*
void
FXDataTable::onDraw()
		{
		if (m_columnsChanged || m_cellsChanged) recalculate();

		fl_rectf(m_tableRect.left, m_tableRect.top, m_tableRect.width(), m_tableRect.height(), m_tableBackgroundColor.to_fl_color());
		fl_rect(m_tableRect.left, m_tableRect.top, m_tableRect.width(), m_tableRect.height(), m_tableBorderColor.to_fl_color());

		int	xpos=m_tableRect.left+1;
		int	ypos=m_tableRect.top+1;

		ypos += drawRow(-1, m_headerRow, xpos, ypos) + m_rowGap;

		int		nrows=getRowCount();

		for (int j=0;j<nrows;j++)
			{
			Row	&row=m_rows[j];

			ypos += drawRow(j, row, xpos, ypos) + m_rowGap;
			}

		FXWidget::onDraw();
		}
*/

void
FXDataTable::getCellColors(int R, int C, FXColor &fgcolor, FXColor &bgcolor)
		{
		Row		&row=*m_rows[R];
		Cell	&cell=row.cells[C];
		Column	&column=m_columns[C];

		bgcolor = fgcolor = FX_COLOR_DEFAULT;
		if (is_selected(R, C))
			{
			if (bgcolor == FX_COLOR_DEFAULT)
				{
				if (m_invertCellColorsOnSelection) bgcolor = cell.fgcolor;
				else bgcolor = cell.bgcolor;
				}
			if (bgcolor == FX_COLOR_DEFAULT)
				{
				if (m_invertRowColorsOnSelection) bgcolor = row.fgcolor;
				else bgcolor = row.bgcolor;
				}
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = m_selectedRowBackgroundColor[R % 2];
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = "#def";

			if (fgcolor == FX_COLOR_DEFAULT)
				{
				if (m_invertCellColorsOnSelection) fgcolor = cell.bgcolor;
				else fgcolor = cell.fgcolor;
				}
			if (fgcolor == FX_COLOR_DEFAULT)
				{
				if (m_invertRowColorsOnSelection) fgcolor = row.bgcolor;
				else fgcolor = row.fgcolor;
				}
			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = m_selectedRowForegroundColor[R % 2];
			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = "#000";
			}
		else
			{
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = cell.bgcolor;
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = row.bgcolor;
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = m_rowBackgroundColor[R % 2];
			if (bgcolor == FX_COLOR_DEFAULT) bgcolor = "#fff";

			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = cell.fgcolor;
			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = row.fgcolor;
			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = m_rowForegroundColor[R % 2];
			if (fgcolor == FX_COLOR_DEFAULT) fgcolor = "#000";
			}
		}


void
FXDataTable::draw()
		{
		SWMutexGuard	guard(m_mutex);

		Fl_Table::draw();
		}


int
FXDataTable::handle(int event)
		{
		SWMutexGuard	guard(m_mutex);
		int		res=0;

		res = Fl_Table::handle(event);
		if (event == FL_PUSH && Fl::event_button() == FL_LEFT_MOUSE)
			{
			int	rt, cl, rb, cr;

			get_selection(rt, cl, rb, cr);
			set_selection(rt, 0, rb, cols());
			}

		return res;
		}


void
FXDataTable::draw_cell(TableContext context, int R, int C, int X, int Y, int W, int H)
		{
		SWMutexGuard	guard(m_mutex);
		FXColor		fgcolor, bgcolor;
		SWString		s;

		s.format("%d/%d", R, C);              // text for each cell

		int		CX=X+m_cellPadding.left;
		int		CY=Y+m_cellPadding.top;
		int		CW=W-m_cellPadding.left-m_cellPadding.right;
		int		CH=H-m_cellPadding.top-m_cellPadding.bottom;

		switch (context)
			{
			case CONTEXT_STARTPAGE:             // Fl_Table telling us its starting to draw page
				if (m_columnsChanged || m_cellsChanged) recalculate();
				break;

			case CONTEXT_ROW_HEADER:            // Fl_Table telling us it's draw row/col headers
				break;
    
			case CONTEXT_COL_HEADER:
				fl_push_clip(X, Y, W, H);
				{
				fl_font(m_headerFont.fl_face(), m_headerFont.height());
				bgcolor = m_headerRow.bgcolor;
				fgcolor = m_headerRow.fgcolor;

				fl_rectf(X, Y, W, H, bgcolor.to_fl_color());

				fl_color(fgcolor.to_fl_color());
				fx_draw_text(m_headerRow.cells[C].contents, CX, CY, CW, CH, m_columns[C].align | FX_VALIGN_MIDDLE);
				}
				fl_pop_clip();
				break;
        
			case CONTEXT_CELL:                  // Fl_Table telling us to draw cells
				fl_push_clip(X, Y, W, H);
				{
				Row		&row=*m_rows[R];
				Cell	&cell=row.cells[C];
				Column	&column=m_columns[C];

				fl_font(m_cellFont.fl_face(), m_cellFont.height());

				getCellColors(R, C, fgcolor, bgcolor);

				fl_rectf(X, Y, W, H, bgcolor.to_fl_color());

				fl_color(fgcolor.to_fl_color());
				fx_draw_text(cell.contents, CX, CY, CW, CH, column.align | FX_VALIGN_MIDDLE);
				}
				fl_pop_clip();
				break;
 			}
		}