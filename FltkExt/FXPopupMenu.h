/*
**	FXPopupMenu.h	A generic popup menu class
*/

#ifndef __FltkExt_FXPopupMenu_h__
#define __FltkExt_FXPopupMenu_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXMenu.h>
#include <FltkExt/FXMenuItem.h>
#include <FltkExt/FXPoint.h>

#include <FL/Fl_Widget.H>

class FLTKEXT_DLL_EXPORT FXPopupMenu
		{
public:
		FXPopupMenu(Fl_Widget *pOwner);
		virtual ~FXPopupMenu();

		void			displayAndTrack(const FXPoint &pt);

		void			add(sw_uint32_t id, const SWString &text);
		void			add(const FXMenuItem &item);
		void			addSeparator();
		void			check(sw_uint32_t id, bool v);
		void			enable(sw_uint32_t id, bool v);
		FXMenuItem *	findItem(sw_uint32_t id);

protected:
		static void		menuCallback(Fl_Widget *pWidget, void *userdata);

protected:
		Fl_Widget		*m_pOwner;
		FXMenu			m_topLevelMenu;
		FXMenu			*m_pMenu;
		};

#endif // __FltkExt_FXPopupMenu_h__