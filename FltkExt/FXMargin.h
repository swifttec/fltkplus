#ifndef __FltkExt_FXMargin_h__
#define __FltkExt_FXMargin_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

// Represent the margins and/or padding around an object
class FLTKEXT_DLL_EXPORT FXMargin
		{
public:
		FXMargin(int tm=0, int bm=0, int lm=0, int rm=0);
		FXMargin(const FXMargin &other);
		virtual ~FXMargin();

		FXMargin &	operator=(const FXMargin &other);
		bool		operator==(const FXMargin &other);
		void		clear()		{ top = bottom = right = left = 0; }

public:
		int		top;
		int		bottom;
		int		left;
		int		right;
		};

typedef FXMargin	FXPadding;

#endif // __FltkExt_FXMargin_h__
