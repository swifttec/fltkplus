/**
*** @file		FXMenuWindow.h
*** @brief		A generic menu item class
*** @version	1.0
*** @author		Simon Sparkes
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXMenuWindow_h__
#define __FltkExt_FXMenuWindow_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FL/Fl_Single_Window.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_Menu_Item.H>

/**
  The FXMenuWindow widget is a window type used for menus. By
  default the window is drawn in the hardware overlay planes if they are
  available so that the menu don't force the rest of the window to
  redraw.
*/
class FLTKEXT_DLL_EXPORT FXMenuWindow : public Fl_Single_Window
		{
public:
		/** Creates a new FXMenuWindow widget using the given position, size, and label string. */
		FXMenuWindow(int X, int Y, int W, int H, const char *l = 0);

		/** Creates a new FXMenuWindow widget using the given size, and label string. */
		FXMenuWindow(int W, int H, const char *l = 0);

		~FXMenuWindow();

		void show();
		void erase();
		void flush();
		void hide();
		/** Tells if hardware overlay mode is set */
		unsigned int overlay() {return !(flags()&NO_OVERLAY);}
		/** Tells FLTK to use hardware overlay planes if they are available.  */
		void set_overlay() {clear_flag(NO_OVERLAY);}
		/** Tells FLTK to use normal drawing planes instead of overlay planes.
			This is usually necessary if your menu contains multi-color pixmaps. */
		void clear_overlay() {set_flag(NO_OVERLAY);}
		};


#endif // __FltkExt_FXMenuWindow_h__