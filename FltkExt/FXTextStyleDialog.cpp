#include "FXTextStyleDialog.h"

#include "FXStaticText.h"
#include "FXDropdownList.h"
#include "FXCheckBox.h"
#include "FXStdButton.h"

#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Hold_Browser.H>
#include <FL/Fl_Color_Chooser.H>

#include <swift/sw_functions.h>

enum ItemID
	{
	IDC_SAMPLE=100,
	IDC_FONT_NAME,
	IDC_FONT_SIZE,
	IDC_BOLD,
	IDC_ITALIC,
	IDC_FGCOLOR,
	IDC_BGCOLOR,
#ifdef FX_OUTLINE
	IDC_OUTLINE,
	IDC_OUTLINECOLOR,
#endif // FX_OUTLINE
	IDC_SHADOW,
	IDC_SHADOWCOLOR,
	IDC_SHADOW_X,
	IDC_SHADOW_Y,
	};

FXTextStyleDialog::FXTextStyleDialog(const SWString &title, const FXTextStyle &value, int flags, Fl_Widget *pParent) :
	FXDialog(title, 600, 400, pParent),
	m_title(title),
	m_value(value),
	m_flags(flags)
		{
		if (m_flags == 0)
			{
			m_flags = FlagFontName | FlagFontSize | FlagFontStyle | FlagTextColor;
			}

		FXFontFamily::loadFamilyList(true);
		FXFontFamily::getFamilyList(m_fontlist);

		FXDropdownList	*pList;

		addButton(FX_IDCANCEL, "Cancel", 0, w() - 60, h() - 30, 50, 20);
		addButton(FX_IDOK, "OK", 1, w() - 120, h() - 30, 50, 20);

#define YH	24
		
		int		ypos=10;

		if ((m_flags & FlagFontName) != 0)
			{
			addStaticText(0, "Font:", 10, ypos, 40, YH);
			addItem(IDC_FONT_NAME, pList = new FXDropdownList(50, ypos, 200, YH));
			ypos += YH+2;
		
			int		nfonts=(int)m_fontlist.size();
			int		idx=-1;

			for (int i=0;i<nfonts;i++)
				{
				FXFontFamily	&ff=m_fontlist[i];

				pList->addChoice(i, ff.name());
				if (idx < 0 && ff.name() == m_value.font.name()) idx = i;
				}

			if (idx < 0 && nfonts > 0)
				{
				m_value.font.name(m_fontlist[0].name());
				pList->value(0);
				}
			else
				{
				pList->value(idx);
				}
			}

		if ((m_flags & FlagFontSize) != 0)
			{
			addStaticText(0, "Size:", 10, ypos, 40, YH);
			addItem(IDC_FONT_SIZE, new Fl_Int_Input(50, ypos, 50, YH));
			ypos += YH+2;
			getItem(IDC_FONT_SIZE)->when(FL_WHEN_CHANGED);

			setItemValue(IDC_FONT_SIZE, (int)m_value.font.pointsize());
			}

		int			xpos=50;

		if ((m_flags & FlagFontStyle) != 0)
			{
			addStaticText(0, "Style:", 10, ypos, 40, YH);

			FXCheckBox	*pCheckBox;

			addItem(IDC_BOLD, pCheckBox = new FXCheckBox("Bold", 0, xpos, ypos, 60, YH));
			pCheckBox->setAlignment(FX_VALIGN_CENTER | FX_HALIGN_LEFT);
			
			xpos += 60;
			addItem(IDC_ITALIC, pCheckBox = new FXCheckBox("Italic", 0, xpos, ypos, 60, YH));
			pCheckBox->setAlignment(FX_VALIGN_CENTER | FX_HALIGN_LEFT);

			setItemValue(IDC_BOLD, (m_value.font.style() & FXFont::Bold) != 0);
			setItemValue(IDC_ITALIC, (m_value.font.style() & FXFont::Italic) != 0);

#ifdef FX_OUTLINE
			if ((m_flags & FlagOutline) != 0)
				{
				xpos += 60;
				addItem(IDC_OUTLINE, pCheckBox = new FXCheckBox("Outline", 0, xpos, ypos, 60, YH));
				pCheckBox->setAlignment(FX_VALIGN_CENTER | FX_HALIGN_LEFT);
				setItemValue(IDC_OUTLINE, ((m_value.flags & FX_OUTLINE) != 0 && m_value.outlineColor != FX_COLOR_NONE));
				}
#endif // FX_OUTLINE

			if ((m_flags & FlagShadow) != 0)
				{
				xpos += 60;
				addItem(IDC_SHADOW, pCheckBox = new FXCheckBox("Shadow", 0, xpos, ypos, 60, YH));
				pCheckBox->setAlignment(FX_VALIGN_CENTER | FX_HALIGN_LEFT);
				setItemValue(IDC_SHADOW, ((m_value.flags & FX_SHADOW) != 0 && m_value.shadowColor != FX_COLOR_NONE));

				xpos += 60;

				addStaticText(0, "X", xpos, ypos, 12, YH);
				xpos += 14;
				addItem(IDC_SHADOW_X, new Fl_Int_Input(xpos, ypos, 24, YH));
				xpos += 30;
				getItem(IDC_SHADOW_X)->when(FL_WHEN_CHANGED);
				setItemValue(IDC_SHADOW_X, (int)m_value.shadowXOffset);

				addStaticText(0, "Y", xpos, ypos, 12, YH);
				xpos += 14;
				addItem(IDC_SHADOW_Y, new Fl_Int_Input(xpos, ypos, 24, YH));
				xpos += 30;
				getItem(IDC_SHADOW_Y)->when(FL_WHEN_CHANGED);
				setItemValue(IDC_SHADOW_Y, (int)m_value.shadowXOffset);
				}

			ypos += YH+2;
			}

		xpos = 50;
		if ((m_flags & FlagTextColor) != 0)
			{
			addItem(IDC_FGCOLOR, new FXStdButton("Text Color...", false, xpos, ypos, 100, YH));
			xpos += 110;
			}

		if ((m_flags & FlagBGColor) != 0)
			{
			addItem(IDC_BGCOLOR, new FXStdButton("Background Color...", false, xpos, ypos, 120, YH));
			xpos += 130;
			}

		if ((m_flags & FlagFontStyle) != 0)
			{
#ifdef FX_OUTLINE
			if ((m_flags & FlagOutline) != 0)
				{
				addItem(IDC_OUTLINECOLOR, new FXStdButton("Outline Color...", false, xpos, ypos, 100, YH));
				xpos += 110;
				}
#endif // FX_OUTLINE

			if ((m_flags & FlagShadow) != 0)
				{
				addItem(IDC_SHADOWCOLOR, new FXStdButton("Shadow Color...", false, xpos, ypos, 100, YH));
				xpos += 110;
				}
			}

		ypos += YH * 2;
		addStaticText(100, "The quick brown fox jumped over the lazy dogs", 10, ypos, w()-20, h()-ypos-70);

		updateSample();
		}


FXTextStyleDialog::~FXTextStyleDialog()
		{
		}


void
FXTextStyleDialog::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		int		id=pWidget->id();

		switch (id)
			{
			case IDC_FONT_NAME:
				{
				FXDropdownList	*pList=dynamic_cast<FXDropdownList *>(getItem(id));

				if (pList != NULL)
					{
					m_value.font.name(m_fontlist[pList->value()].name());
					}
				}
				break;

			case IDC_BOLD:
				{
				bool	v;

				getItemValue(id, v);
				if (v) m_value.font.addStyle(FXFont::Bold);
				else m_value.font.removeStyle(FXFont::Bold);
				}
				break;

			case IDC_ITALIC:
				{
				bool	v;

				getItemValue(id, v);
				if (v) m_value.font.addStyle(FXFont::Italic);
				else m_value.font.removeStyle(FXFont::Italic);
				}
				break;

			case IDC_FONT_SIZE:
				{
				int		v;

				getItemValue(id, v);
				if (v < 4) v = 4;
				else if (v > 200) v = 200;
				
				m_value.font.pointsize(v);
				}
				break;

			case IDC_FGCOLOR:
				if (Fl::event() == FL_RELEASE)
					{
					sw_byte_t	r, g, b;

					r = m_value.fgColor.red();
					g = m_value.fgColor.green();
					b = m_value.fgColor.blue();

					if (fl_color_chooser("Select Text Colour", r, g, b) != 0)
						{
						m_value.fgColor.red(r);
						m_value.fgColor.green(g);
						m_value.fgColor.blue(b);
						}
					}
				break;

			case IDC_BGCOLOR:
				if (Fl::event() == FL_RELEASE)
					{
					sw_byte_t	r, g, b;

					r = m_value.bgColor.red();
					g = m_value.bgColor.green();
					b = m_value.bgColor.blue();

					if (fl_color_chooser("Select Background Colour", r, g, b) != 0)
						{
						m_value.bgColor.red(r);
						m_value.bgColor.green(g);
						m_value.bgColor.blue(b);
						}
					}
				break;

			case IDC_SHADOWCOLOR:
				if (Fl::event() == FL_RELEASE)
					{
					sw_byte_t	r, g, b;

					r = m_value.shadowColor.red();
					g = m_value.shadowColor.green();
					b = m_value.shadowColor.blue();

					if (fl_color_chooser("Select Shadow Colour", r, g, b) != 0)
						{
						m_value.shadowColor.red(r);
						m_value.shadowColor.green(g);
						m_value.shadowColor.blue(b);
						}
					}
				break;

#ifdef FX_OUTLINE
			case IDC_OUTLINECOLOR:
				if (Fl::event() == FL_RELEASE)
					{
					sw_byte_t	r, g, b;

					r = m_value.outlineColor.red();
					g = m_value.outlineColor.green();
					b = m_value.outlineColor.blue();

					if (fl_color_chooser("Select Outline Colour", r, g, b) != 0)
						{
						m_value.outlineColor.red(r);
						m_value.outlineColor.green(g);
						m_value.outlineColor.blue(b);
						}
					}
				break;
#endif // FX_OUTLINE

			default:
				FXDialog::onItemCallback(pWidget, NULL);
				break;
			}

		updateSample();
		}


void
FXTextStyleDialog::onOK()
		{
		bool	ok=true;

		if (ok) FXDialog::onOK();
		}


void
FXTextStyleDialog::updateSample()
		{
		FXStaticText	*pText=dynamic_cast<FXStaticText *>(getItem(100));

		if (pText != NULL)
			{
			pText->setTextStyle(m_value);
			pText->invalidate();
			}

		bool	b;
		
		getItemValue(IDC_SHADOW, b);
		enableItem(IDC_SHADOW_X, b);
		enableItem(IDC_SHADOW_Y, b);
		enableItem(IDC_SHADOWCOLOR, b);

#ifdef FX_OUTLINE
		getItemValue(IDC_OUTLINE, b);
		enableItem(IDC_OUTLINECOLOR, b);
#endif // FX_OUTLINE
		}

