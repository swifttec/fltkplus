/*
** fx_types.h
*/
#ifndef __FltkExt_fx_types_h__
#define __FltkExt_fx_types_h__

#include <xplatform/sw_types.h>
#include <swift/swift.h>
#include <swift/SWString.h>
#include <swift/SWStringArray.h>
#include <swift/SWFlags.h>
#include <swift/SWArray.h>
#include <swift/SWIntegerArray.h>
#include <swift/SWFileInfo.h>
#include <swift/SWFile.h>
#include <swift/SWLockableObject.h>


typedef struct _fx_rgbvalue_
	{
	sw_byte_t	r;
	sw_byte_t	g;
	sw_byte_t	b;
	} FXRGBValue;

#endif // __FltkExt_fx_types_h__
