#include "FXWidget.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_timer.h>

#include <swift/SWFileInfo.h>
#include <xplatform/sw_math.h>

// Definitions to support FXWidget FXGroup and FXWnd Common Methods
// to make the job of keeping FXWidget and FXWnd in sync easier
#define FLAG_LEFT_MOUSE		0x01		///< The left mouse button is down
#define FLAG_MIDDLE_MOUSE	0x02		///< The middle mouse button is down
#define FLAG_RIGHT_MOUSE	0x04		///< The right mouse button is down
#define FLAG_MOUSE_OVER		0x08		///< The mouse is over the widget
#define FLAG_FOCUS			0x10		///< The widget has focus


FXWidget::FXWidget() :
	Fl_Widget(0, 0, 0, 0),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();
		}


FXWidget::FXWidget(int X, int Y, int W, int H) :
	Fl_Widget(X, Y, W, H),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();
		}


FXWidget::FXWidget(int W, int H) :
	Fl_Widget(0, 0, W, H),
	m_pBgImage(NULL),
	m_pBgImageCopy(NULL)
		{
		init();
		}


FXWidget::~FXWidget()
		{
		fx_timer_destroyAll(this);
		onClose();
		clearBackgroundImage();
		}


void
FXWidget::init()
		{
		m_flags = 0;
		m_bgColor = FXColor::NoColor;
		m_fgColor = FXColor(0, 0, 0);
		m_bdColor = FXColor::NoColor;
		m_bdWidth = 0;
		m_bdCorner = 0;
		m_bdStyle = 0;
		m_lastEventCode = 0;
		m_cursor = FL_CURSOR_DEFAULT;

		onCreate();
		}



void
FXWidget::getContentsRect(FXRect &rect) const
		{
		FXRect	ir;
		FXRect	cr;

		getInnerRect(ir);
		getClientRect(cr);

		ir.deflate(m_padding.left, m_padding.top, m_padding.right, m_padding.bottom);
		rect.left = sw_math_max(ir.left, cr.left);
		rect.top = sw_math_max(ir.top, cr.top);
		rect.right = sw_math_min(ir.right, cr.right);
		rect.bottom = sw_math_min(ir.bottom, cr.bottom);
		}


void
FXWidget::getClientRect(FXRect &rect) const
		{
		getInnerRect(rect);

		int	bw=getBorderWidth();
		int	cw=getCornerWidth();
		
		if (cw > bw)
			{
			bw = (int)((cw-bw) * 0.293);
			rect.deflate(bw, bw, bw, bw);
			}
		}

void
FXWidget::getInnerRect(FXRect &rect) const
		{
		getWidgetRect(rect);
		
		int	bw=getBorderWidth();

		rect.deflate(bw, bw, bw, bw);
		}


void
FXWidget::getWidgetRect(FXRect &rect) const
		{
		rect.left = x();
		rect.right = x()+w()-1;
		rect.top = y();
		rect.bottom = y()+h()-1;
		}


sw_uint32_t
FXWidget::setIntervalTimer(sw_uint32_t localid, double seconds, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, seconds, this, pData);
		}


sw_uint32_t
FXWidget::setIntervalTimerMS(sw_uint32_t localid, int ms, void *pData)
		{
		return fx_timer_create(localid, FX_TIMER_INTERVAL, (double)ms / 1000.0, this, pData);
		}


void
FXWidget::killIntervalTimer(sw_uint32_t id)
		{
		fx_timer_destroy(this, id);
		}


// FXWidget Common Methods
void
FXWidget::invalidate()
		{
		fx_invalidate(this);
		}


void
FXWidget::clearBackgroundImage()
		{
		if (m_pBgImage != NULL)
			{
			m_pBgImage->release();
			m_pBgImage = NULL;
			}

		delete m_pBgImageCopy;
		m_pBgImageCopy = NULL;
		}


void
FXWidget::setBackgroundImage(const SWString &filename)
		{
		clearBackgroundImage();
		if (SWFileInfo::isFile(filename))
			{
			m_pBgImage = Fl_Shared_Image::get(filename);
			}
		}


void
FXWidget::setBorder(int style, const FXColor &color, int width, int corner)
		{
		m_bdStyle = style;
		m_bdColor = color;
		m_bdWidth = width;
		m_bdCorner = corner;
		}


void
FXWidget::onDrawBorder()
		{
		if (m_bdColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;

			FXColor		color=m_bdColor;

			// if (Fl::focus() == this) color = "#f00";

			fx_draw_rect(FXRect(x(), y(), x()+w()-1, y()+h()-1), m_bdWidth, corner, color, FX_COLOR_NONE); 
			}
		}


void
FXWidget::onDrawBackground()
		{
		if (m_bgColor != FX_COLOR_NONE)
			{
			int		style=m_bdStyle;
			int		corner=m_bdCorner;

			if (m_bdCorner <= 0) style = 0;
			if (style == 0) corner = 0;

			fx_draw_rect(FXRect(x(), y(), x()+w()-1, y()+h()-1), m_bdWidth, corner, FX_COLOR_NONE, m_bgColor); 
			}

		if (m_pBgImage != NULL)
			{
			if (m_pBgImageCopy != NULL)
				{
				if (m_pBgImageCopy->w() != w() || m_pBgImageCopy->h() != h())
					{
					delete m_pBgImageCopy;
					m_pBgImageCopy = NULL;
					}
				}

			if (m_pBgImageCopy == NULL)
				{
				m_pBgImageCopy = m_pBgImage->copy(w(), h());
				}
			
			if (m_pBgImageCopy != NULL)
				{
				m_pBgImageCopy->draw(x(), y(), w(), h(), 0, 0);
				}
			}
		}


void
FXWidget::onDraw()
		{
		}


void
FXWidget::draw()
		{
		fl_push_clip(x(),y(),w(),h());

		onDrawBackground();
		onDrawBorder();
		onDraw();

		fl_pop_clip();
		}


void
FXWidget::onSize(const FXPoint &pos, const FXSize &sz)
		{
		Fl_Widget::resize(pos.x, pos.y, sz.cx, sz.cy);
		}


void
FXWidget::resize(int X, int Y, int W, int H)
		{
		onSize(FXPoint(X, Y), FXSize(W, H));
		}


void		FXWidget::onCreate()													{ }
void		FXWidget::onClose()														{ }
bool		FXWidget::hasFocus() const												{ return ((m_flags & FLAG_FOCUS) != 0); }
bool		FXWidget::hasMouseOver() const											{ return ((m_flags & FLAG_MOUSE_OVER) != 0); }
int			FXWidget::onFocus()														{ return 0; }
int			FXWidget::onUnfocus()													{ return 0; }
int			FXWidget::onLeftMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXWidget::onMiddleMouseButton(int action, const FXPoint &pt)			{ return 0; }
int			FXWidget::onRightMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXWidget::onMouseMove(const FXPoint &pt)								{ return 0; }
int			FXWidget::onMouseDrag(const FXPoint &pt)								{ return 0; }
int			FXWidget::onMouseWheel(int dx, int dy, const FXPoint &pt)				{ return 0; }
bool		FXWidget::disabled() const												{ return !enabled(); }
bool		FXWidget::enabled() const												{ return (active() != 0); }
int			FXWidget::onKeyDown(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
int			FXWidget::onKeyUp(int code, const SWString &text, const FXPoint &pt)	{ return 0; }
int			FXWidget::onShortcut(int code, const FXPoint &pt)						{ return 0; }
bool		FXWidget::onTimer(sw_uint32_t id, void *data)							{ return true; }
int			FXWidget::onCommand(sw_uint32_t id)										{ return 0; }
int			FXWidget::onShow()														{ return 0; }
int			FXWidget::onHide()														{ return 0; }

int
FXWidget::onMouseEnter()
		{
		Fl_Window *pWnd=fx_widget_getWindow(this);

		if (pWnd != NULL)
			{
			m_cursor.applyToWindow(pWnd);
			}

		return 1;
		}


int
FXWidget::onMouseLeave()
		{
		Fl_Window *pWnd=fx_widget_getWindow(this);

		if (pWnd != NULL)
			{
			pWnd->cursor(FL_CURSOR_DEFAULT);
			}

		return 1;
		}


int
FXWidget::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;
			}

		return res;
		}


int
FXWidget::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(FX_BUTTON_UP, pt);
				break;
			}

		return res;
		}


// Scrolling support	
void		FXWidget::onHScroll(int pos)											{ }
void		FXWidget::onVScroll(int pos)											{ }
void		FXWidget::onScrollUp()													{ }
void		FXWidget::onScrollDown()												{ }
void		FXWidget::onScrollLeft()												{ }
void		FXWidget::onScrollRight()												{ }
void		FXWidget::onScrollHome()												{ }
void		FXWidget::onScrollEnd()													{ }
void		FXWidget::onScrollPageUp()												{ }
void		FXWidget::onScrollPageDown()											{ }
void		FXWidget::onScrollPageHome()											{ }
void		FXWidget::onScrollPageEnd()												{ }
void		FXWidget::onScrollPageLeft()											{ }
void		FXWidget::onScrollPageRight()											{ }

void
FXWidget::onScroll(int bar, int action, int pos)
		{

		if (bar == FX_SCROLLBAR_HORIZONTAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollLeft();			break;
				case FX_SCROLL_DOWN	:		onScrollRight();		break;
				case FX_SCROLL_FIRST:		onScrollHome();			break;
				case FX_SCROLL_LAST:		onScrollEnd();			break;
				case FX_SCROLL_PAGEUP:		onScrollPageLeft();		break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageRight();	break;
				case FX_SCROLL_TRACK:		onHScroll(pos);			break;
				}
			}
		else if (bar == FX_SCROLLBAR_VERTICAL)
			{
			switch (action)
				{
				case FX_SCROLL_UP:			onScrollUp();		break;
				case FX_SCROLL_DOWN	:		onScrollDown();		break;
				case FX_SCROLL_FIRST:		onScrollPageHome();	break;
				case FX_SCROLL_LAST:		onScrollPageEnd();	break;
				case FX_SCROLL_PAGEUP:		onScrollPageUp();	break;
				case FX_SCROLL_PAGEDOWN:	onScrollPageDown();	break;
				case FX_SCROLL_TRACK:		onVScroll(pos);		break;
				}
			}

		invalidate();
		}

int
FXWidget::handle(int eventcode)
		{
		int		res=0;

		m_lastEventCode = eventcode;
		m_lastEventPoint.x = Fl::event_x();
		m_lastEventPoint.y = Fl::event_y();

		switch (eventcode)
			{
			case FL_FOCUS:
				m_flags |= FLAG_FOCUS;
				res = onFocus();
				break;
			
			case FL_UNFOCUS:
				m_flags &= ~FLAG_FOCUS;
				res = onUnfocus();
				break;
			
			case FL_ENTER:
				m_flags |= FLAG_MOUSE_OVER;
				res = onMouseEnter();
				break;
			
			case FL_LEAVE:
				m_flags &= ~(FLAG_MOUSE_OVER|FLAG_LEFT_MOUSE|FLAG_MIDDLE_MOUSE|FLAG_RIGHT_MOUSE);
				res = onMouseLeave();
				break;
				
			case FL_MOVE:
				res = onMouseMove(m_lastEventPoint);
				break;
				
			case FL_DRAG:
				res = onMouseDrag(m_lastEventPoint);
				break;
		
			case FL_PUSH:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags |= FLAG_LEFT_MOUSE;		break;
					case FL_MIDDLE_MOUSE:	m_flags |= FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags |= FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseDown(button, m_lastEventPoint);
				}
				break;

			case FL_RELEASE:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags &= ~FLAG_LEFT_MOUSE;	break;
					case FL_MIDDLE_MOUSE:	m_flags &= ~FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags &= ~FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseUp(button, m_lastEventPoint);
				}
				break;
			
			case FL_MOUSEWHEEL:
				res = onMouseWheel(Fl::event_dx(), Fl::event_dy(), m_lastEventPoint);
				break;

			case FL_KEYDOWN:
				res = onKeyDown(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;
   			
			case FL_KEYUP:
				res = onKeyUp(Fl::event_key(), Fl::event_text(), m_lastEventPoint);
				break;

			case FL_SHORTCUT:
				res = onShortcut(Fl::event_key(), m_lastEventPoint);
				break;
			
			case FL_SHOW:
				res = onShow();
				break;
			
			case FL_HIDE:
				res = onHide();
				break;
			}

		// TRACE("FXWidget(%x)::handle(%s) at %d,%d = %d\n", this, fx_event_name(eventcode).c_str(), m_lastEventPoint.x, m_lastEventPoint.y, res);

		return res;
		}



void
FXWidget::setCursor(Fl_Cursor v)
		{
		m_cursor = v;

		Fl_Window *pWnd=fx_widget_getWindow(this);

		if (pWnd != NULL)
			{
			m_cursor.applyToWindow(pWnd);
			}
		}



void
FXWidget::setCursor(const FXCursor &v)
		{
		m_cursor = v;

		Fl_Window *pWnd=fx_widget_getWindow(this);

		if (pWnd != NULL)
			{
			m_cursor.applyToWindow(pWnd);
			}
		}
