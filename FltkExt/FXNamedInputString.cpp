#include "FXNamedInputString.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <FltkExt/FXPackedLayout.h>

FXNamedInputString::FXNamedInputString(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(X, Y, nameW, valueW, H)
		{
		init(name, value);
		}


FXNamedInputString::FXNamedInputString(const SWString &name, int nameW, const SWString &value, int valueW, int H) :
	FXNamedWidget(0, 0, nameW, valueW, H)
		{
		init(name, value);
		}

void
FXNamedInputString::init(const SWString &name, const SWString &value)
		{
		Fl_Input	*pInput;

		pInput = new Fl_Input(0, 0, m_nameW, h()-2);
		pInput->when(FL_WHEN_CHANGED);
		FXNamedWidget::init(name, pInput, value);
		}
