/*
**	FXCursor.h	A generic cursor class
*/

#ifndef __FltkExt_FXCursor_h__
#define __FltkExt_FXCursor_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXImage.h>

#include <swift/SWString.h>
#include <swift/SWList.h>

#include <FL/Enumerations.H>
#include <FL/Fl_PNG_Image.H>

class FXCursor;

class FLTKEXT_DLL_EXPORT FXInternalCursor
		{
friend class FXCursor;

public:
		enum CursorType
			{
			BuiltIn=0,
			Image
			};

public:
		static FXInternalCursor *	create(Fl_Cursor id);
		static FXInternalCursor *	create(const SWString &filename, int imageHotspotX, int imageHotspotY);
		static FXInternalCursor *	create(FXInternalCursor *pCursor);
		static void					destroy(FXInternalCursor *pCursor);


protected:
		FXInternalCursor();
		FXInternalCursor(Fl_Cursor cursor);
		FXInternalCursor(const SWString &filename, int imageHotspotX, int imageHotspotY);

public:
		virtual ~FXInternalCursor();

protected:
		static SWList<FXInternalCursor *>	*m_pCursorList;
		
protected:
		int				m_type;
		int				m_refcount;
		Fl_Cursor		m_cursor;
		Fl_PNG_Image	*m_pImage;
		SWString		m_imagefile;
		int				m_imageHotspotX;
		int				m_imageHotspotY;
		};


class FLTKEXT_DLL_EXPORT FXCursor
		{
public:
		FXCursor();
		FXCursor(Fl_Cursor cursor);
		virtual ~FXCursor();

		FXCursor &	operator=(const FXCursor &other);
		FXCursor &	operator=(Fl_Cursor cursor);
		bool		operator==(const FXCursor &other);
		bool		operator!=(const FXCursor &other);

		void		applyToWindow(Fl_Window *pWnd);
		void		load(const SWString &filename, int imageHotspotX, int imageHotspotY);

protected:
		FXInternalCursor	*m_pCursor;
		};
#endif // __FltkExt_FXCursor_h__
