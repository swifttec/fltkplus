#include <FltkExt/FXFontFamily.h>
#include <FltkExt/FXFont.h>
#include <FL/Fl.H>

// The global list of fonts which we create on first request
static SWArray<FXFontFamily>	*pGlobalFontList=NULL;


bool
FXFontFamily::isFamilyListLoaded()
		{
		return (pGlobalFontList != NULL);
		}


void
FXFontFamily::loadFamilyList(bool reload)
		{
		if (pGlobalFontList == NULL || reload)
			{
			pGlobalFontList = new SWArray<FXFontFamily>();

			SWArray<FXFontFamily>	&fontlist=*pGlobalFontList;

			SWString	name;
			int			nfonts=Fl::set_fonts("*");
			int			attribs=0;
			int			familyidx=-1;

			for (int i=0;i<nfonts;i++)
				{
				name = Fl::get_font_name(i, &attribs);

				if (attribs == 0)
					{
					// First in a set
					familyidx += 1;

					FXFontFamily	&family=fontlist[familyidx];

					family.m_name = name;
					family.m_fontnum[0] = i;

					int		nsizes;
					int		*sizelist;

					nsizes = Fl::get_font_sizes(i, sizelist);

					for (int j=0;j<nsizes;j++)
						{
						int		fs=sizelist[j];

						if (fs != 0) family.m_fontsizes.add(fs);
						}
					}
				else
					{
					FXFontFamily	&family=fontlist[familyidx];

					if ((attribs & (FL_BOLD|FL_ITALIC)) == (FL_BOLD|FL_ITALIC)) family.m_fontnum[FXFont::BoldItalic] = i;
					else if ((attribs & FL_BOLD) != 0) family.m_fontnum[FXFont::Bold] = i;
					else if ((attribs & FL_ITALIC) != 0) family.m_fontnum[FXFont::Italic] = i;
					}
				}
			}
		}


void
FXFontFamily::getFamilyList(SWArray<FXFontFamily> &fontlist)
		{
		loadFamilyList();
		fontlist = *pGlobalFontList;
		}


int
FXFontFamily::getFamilyCount()
		{
		loadFamilyList();
		return (int)pGlobalFontList->size();
		}


const FXFontFamily &
FXFontFamily::getFamily(int idx)
		{
		loadFamilyList();
		return pGlobalFontList->get(idx);
		}


int
FXFontFamily::find(const SWString &name)
		{
		int		res=-1;

		loadFamilyList();

		SWArray<FXFontFamily>	&fontlist=*pGlobalFontList;

		int		count=getFamilyCount();

		// Case sensitive search
		for (int i=0;res<0&&i<count;i++)
			{
			if (fontlist[i].name() == name)
				{
				res = i;
				break;
				}
			}

		// Case insensitive search if not found yet
		for (int i=0;res<0&&i<count;i++)
			{
			if (fontlist[i].name().compareNoCase(name) == 0)
				{
				res = i;
				break;
				}
			}

		return res;
		}


void
FXFontFamily::find(int fontid, int &family, int &style)
		{
		int		res=-1;

		loadFamilyList();

		SWArray<FXFontFamily>	&fontlist=*pGlobalFontList;

		family = style = 0;

		int		count=getFamilyCount();
		bool	found=false;

		for (int i=0;!found&&i<count;i++)
			{
			FXFontFamily	&ff=fontlist[i];

			for (int j=0;!found&&j<(int)ff.m_fontnum.size();i++)
				{
				if (ff.m_fontnum == fontid)
					{
					family = i;
					style = j;
					found = true;
					}
				}
			}
		}


FXFontFamily::FXFontFamily()
		{
		}


SWString
FXFontFamily::variantName(int v)
		{
		SWString	res="Normal";

		switch (v)
			{
			case FXFont::Bold:			res = "Bold";	break;
			case FXFont::Italic:		res = "Italic";	break;
			case FXFont::BoldItalic:	res = "BoldItalic";	break;
			}

		return res;
		}



bool
FXFontFamily::hasStyle(int v) const
		{
		bool		res=false;

		switch (v)
			{
			case FXFont::Normal:
				res = true;
				break;

			default:
				if (v > 0 && v < (int)m_fontnum.size()) res = (m_fontnum.get(v) != 0);
				break;
			}

		return res;
		}



int
FXFontFamily::fl_font_face(int v) const
		{
		int		res=0;

		if (v >= 0 && v < (int)m_fontnum.size()) res = m_fontnum.get(v);

		return res;
		}