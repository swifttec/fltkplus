/****************************************************************************
**	FXCachedImage.h	An class to represent a cached image object
****************************************************************************/

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXImage.h>

#ifndef __FltkExt_FXCachedImage_h__
#define __FltkExt_FXCachedImage_h__

class FLTKEXT_DLL_EXPORT FXCachedImage : public FXImage
		{
friend class FXCachedImageManager;

protected:
		FXCachedImage();

protected:
		void			incRefCount()		{ m_refCount++; }
		void			decRefCount()		{ --m_refCount; }
		int				refCount() const	{ return m_refCount; }

public:
		virtual ~FXCachedImage();

protected:
		int				m_id;
		FXCachedImage	*m_pNext;
		FXCachedImage	*m_pPrev;
		int				m_refCount;
		};

#endif // __FltkExt_FXCachedImage_h__
