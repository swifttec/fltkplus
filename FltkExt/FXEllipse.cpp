#include "FXEllipse.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

FXEllipse::FXEllipse(const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor) :
	m_reflectionColor(rfcolor)
		{
		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		}

FXEllipse::FXEllipse(int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor) :
	FXWidget(W, H),
	m_reflectionColor(rfcolor)
		{
		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		}

FXEllipse::FXEllipse(int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &rfcolor) :
	FXWidget(X, Y, W, H),
	m_reflectionColor(rfcolor)
		{
		setForegroundColor(fgcolor);
		setBackgroundColor(bgcolor);
		}

void
FXEllipse::onDrawBackground()
		{
		FXColor	color=getBackgroundColor();

		if (color != FX_COLOR_NONE)
			{
			fl_color(color.to_fl_color());
			fl_pie(x(), y(), w(), h(), 0.0, 360.0);
			}
		}


void
FXEllipse::onDraw()
		{
		FXColor	color=getForegroundColor();

		if (color != FX_COLOR_NONE)
			{
			fl_color(color.to_fl_color());
			fl_arc(x(), y(), w(), h(), 0.0, 360.0);
			}

		if (m_reflectionColor != FX_COLOR_NONE)
			{
			FXRect	cr;

			getClientRect(cr);

			int	rw=cr.width() / 5;
			int	rh=cr.height() / 5;

			cr.DeflateRect(rw, rh, rw, rh);
			fl_color(m_reflectionColor.to_fl_color());
			fl_arc(cr.left, cr.top, cr.width(), cr.height(), 90.0, 150.0);
			}
		}