/*
**	FXInputInteger.h	A generic dialog class
*/

#ifndef __FltkExt_FXInputInteger_h__
#define __FltkExt_FXInputInteger_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Box.H>
#include <FL/Fl_Int_Input.H>

class FLTKEXT_DLL_EXPORT FXInputInteger : public Fl_Int_Input
		{
public:
		FXInputInteger(int W, int H);
		FXInputInteger(int X, int Y, int W, int H);
		};


#endif // __FltkExt_FXInputInteger_h__
