#include <FltkExt/fx_functions.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBoxedText.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Button.H>
#include <FL/fl_draw.H>
#include <FL/fl_message.H>

#include <swift/SWList.h>

static FXTextStyle	s_textStyle;


FLTKEXT_DLL_EXPORT void
fx_setTextColor(const FXColor &v)
		{
		s_textStyle.fgColor = v;
		}


FLTKEXT_DLL_EXPORT void
fx_setBkColor(const FXColor &v)
		{
		s_textStyle.bgColor = v;
		}


FLTKEXT_DLL_EXPORT void
fx_select(const FXFont &obj)
		{
		s_textStyle.font = obj;
		obj.select();
		}


FLTKEXT_DLL_EXPORT void
fx_select(const FXColor &obj)
		{
		s_textStyle.fgColor = obj;
		obj.select();
		}


FLTKEXT_DLL_EXPORT bool
fx_draw_isPrinting()
		{
		return false;
		}



FLTKEXT_DLL_EXPORT void
fx_draw_getDPI(double &h, double &v)
		{
		float	fh, fv;

		Fl::screen_dpi(fh, fv);
		h = fh;
		v = fv;
		}


FLTKEXT_DLL_EXPORT double
fx_draw_mmToPixelsX(double v)
		{
		double	xdpi, ydpi;

		fx_draw_getDPI(xdpi, ydpi);

		return ((xdpi * v) / 25.4);
		}


FLTKEXT_DLL_EXPORT double
fx_draw_mmToPixelsY(double v)
		{
		double	xdpi, ydpi;

		fx_draw_getDPI(xdpi, ydpi);

		return ((ydpi * v) / 25.4);
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text)
		{
		FXSize	sz(0, 0);

		fl_measure(text, sz.cx, sz.cy);

		return sz;
		}



FLTKEXT_DLL_EXPORT int
fx_getTextHeight(const SWString &text, int width, const FXTextStyle &style)
		{
		return fx_getTextExtent(text, width, style).cy;
		}




FLTKEXT_DLL_EXPORT int
fx_getTextHeight(const SWString &text, int width)
		{
		return fx_getTextExtent(text, width).cy;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text, int width, const FXTextStyle &style)
		{
		FXSize	sz(0, 0);

		if (text.length())
			{
			if ((style.flags & FX_SHADOW) != 0)
				{
				width -= style.shadowXOffset;
				}

			sz.cx = width;
			sz.cy = 0;
			fl_font(style.font.fl_face(), style.font.height());
			fl_measure(text, sz.cx, sz.cy);

			if ((style.flags & FX_SHADOW) != 0)
				{
				sz.cx += style.shadowXOffset;
				sz.cy += style.shadowYOffset;
				}
			}

		return sz;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text, int width, const FXFont &font)
		{
		font.select();

		return fx_getTextExtent(text, width);
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text, int width, int flags)
		{
		FXSize	sz(0, 0);

		if (text.length())
			{
			sz.cx = width;
			sz.cy = 0;
			fl_measure(text, sz.cx, sz.cy);
			}

		return sz;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text, const FXFont &font)
		{
		FXSize	sz(0, 0);

		fl_font(font.fl_face(), font.height());
		fl_measure(text, sz.cx, sz.cy);

		return sz;
		}


FLTKEXT_DLL_EXPORT FXSize
fx_getTextExtent(const SWString &text, const FXTextStyle &style)
		{
		FXSize	sz(0, 0);

		fl_font(style.font.fl_face(), style.font.height());
		fl_measure(text, sz.cx, sz.cy);

		return sz;
		}



static int
fx_shrinkTextToFit(const SWString &text, const FXRect &dr, const FXTextStyle &style, int minFontHeight)
		{
		FXFont	font(style.font);
		bool	ok=false;
		FXSize	sz;
		int		width=dr.Width();
		int		height=dr.Height();
		int		fontheight=font.height();

		if ((style.flags & FX_SHADOW) != 0)
			{
			width -= style.shadowXOffset;
			height -= style.shadowYOffset;
			}

		while (!ok)
			{
			sz = fx_getTextExtent(text, width, font);
			if (sz.cy > height || sz.cx > width)
				{
				if (fontheight > minFontHeight)
					{
					font.height(--fontheight);
					}
				else
					{
					ok = true;
					}
				}
			else
				{
				ok = true;
				}
			}
			
		return fontheight;
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &dr, const FXTextStyle &textstyle)
		{
		sw_uint32_t	dtflags=0;
		FXTextStyle	style=textstyle;

		if ((style.flags & FX_SHRINKTOFIT) != 0)
			{
			style.font.height(fx_shrinkTextToFit(text, dr, style, 4));
			}

		if ((style.flags & FX_BG_OPAQUE) != 0)
			{
			fx_draw_fillRect(dr, style.bgColor);
			}
		
		if ((style.flags & FX_NOWRAP) == 0)
			{
			dtflags |= FL_ALIGN_WRAP;
			}
		
		if ((style.flags & XD_NOCLIP) == 0)
			{
			dtflags |= FL_ALIGN_CLIP;
			}

		switch (style.flags & FX_HALIGN_MASK)
			{
			case FX_HALIGN_LEFT:	dtflags |= FL_ALIGN_LEFT;	break;
			case FX_HALIGN_CENTER:	dtflags |= FL_ALIGN_CENTER;	break;
			case FX_HALIGN_RIGHT:	dtflags |= FL_ALIGN_RIGHT;	break;
			default:				dtflags |= FL_ALIGN_LEFT;	break;
			}

		FXRect	tr(dr);
		FXSize	szText=fx_getTextExtent(text, tr.width(), style);

		if ((style.flags & XD_SHADOW) != 0)
			{
			tr.right -= style.shadowXOffset;
			tr.bottom -= style.shadowYOffset;
			}

		int	valign=style.flags & FX_VALIGN_MASK;

		if (valign == FX_VALIGN_MIDDLE)
			{
			tr = dr;
			tr.top += (dr.Height() - szText.cy)/2;
			tr.bottom = tr.top + szText.cy - 1;
			}
		else if (valign == FX_VALIGN_BOTTOM)
			{
			tr = dr;
			tr.top = dr.bottom - szText.cy + 1;
			}
		else
			{
			tr = dr;
			tr.bottom = dr.top + szText.cy - 1;
			}

		fl_font(style.font.fl_face(), style.font.height());
		if ((style.flags & FX_SHADOW) != 0)
			{
			FXRect	sr(tr);

			sr.left += style.shadowXOffset;
			sr.right += style.shadowXOffset;
			sr.top += style.shadowYOffset;
			sr.bottom += style.shadowYOffset;

			fl_color(style.shadowColor.to_fl_color());
			fl_draw(text, sr.left, sr.top, sr.width(), sr.height(), dtflags);
			}

/*
		if ((flags & FX_OUTLINE) != 0)
			{
			m_pDC->SetBkMode(TRANSPARENT);
			m_pDC->BeginPath();
			m_pDC->DrawText(text, &tr, dtflags);
			m_pDC->EndPath();
		
			FXPen	pen(FX_PEN_SOLID, style.outlineWidth, style.outlineColor);
			FXBrush	brush(m_pDC->GetTextColor());

			m_pDC->SelectObject(&pen);
			m_pDC->SelectObject(&brush);
			m_pDC->StrokeAndFillPath();
			}
		else
*/
			{
			fl_color(style.fgColor.to_fl_color());
			fl_draw(text, tr.left, tr.top, tr.width(), tr.height(), dtflags);
			}
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, int left, int top, int width, int height, int align)
		{
		fx_draw_text(text, FXRect(left, top, left+width, top+height), align);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXFont &font)
		{
		fl_font(font.fl_face(), font.height());
		fx_draw_text(text, rect, align);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXColorFont &font)
		{
		fl_font(font.fl_face(), font.height());
		fl_color(font.color().to_fl_color());
		fx_draw_text(text, rect, align);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &rect, int align, const FXColor &color)
		{
		fl_color(color.to_fl_color());
		fx_draw_text(text, rect, align);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &rect, int align)
		{
		FXSize	sz=fx_getTextSize(text);
		int		xpos, ypos;

		switch (align & FX_HALIGN_MASK)
			{
			case FX_HALIGN_RIGHT:
				xpos = rect.right-sz.cx;
				break;
			
			case FX_HALIGN_CENTER:
				xpos = rect.left+((rect.width()-sz.cx)/2);
				break;

			default: // Left align
				xpos = rect.left;
				break;
			}

		switch (align & FX_VALIGN_MASK)
			{
			case FX_VALIGN_BOTTOM:
				ypos = rect.bottom - sz.cy;
				break;
			
			case FX_VALIGN_MIDDLE:
				ypos = rect.top+((rect.height()-sz.cy)/2);
				break;

			default: // Top align
				ypos = rect.top;
				break;
			}

		fl_draw(text, xpos, ypos-fl_descent()+fl_height());
		}




FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXColorFont &srcfont, double minsize, double *pSizeUsed)
		{
		fx_draw_text(text, dr, flags, srcfont, srcfont.color(), minsize, pSizeUsed);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_text(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXFont &srcfont, const FXColor &textcolor, double minsize, double *pSizeUsed)
		{
		FXTextStyle	ts;

		ts.font = srcfont;
		ts.fgColor = textcolor;
		ts.flags = flags;

		if ((flags & XD_SHRINKTOFIT) != 0)
			{
			bool	ok=false;
			FXSize	sz;
			int		width=dr.Width();
			int		height=dr.Height();
			double	ptsize=ts.font.pointsize();

			if ((flags & XD_SHADOW) != 0)
				{
				width -= ts.shadowXOffset;
				height -= ts.shadowYOffset;
				}

			while (!ok)
				{
				sz = fx_getTextExtent(text, width, ts);
				if (sz.cy > height || sz.cx > width)
					{
					if (ptsize > minsize)
						{
						ptsize -= 1.0;
						ts.font.pointsize(ptsize);
						}
					else
						{
						ok = true;
						}
					}
				else
					{
					ok = true;
					}
				}
			
			if (pSizeUsed != NULL) *pSizeUsed = ptsize;
			}

		fx_draw_text(text, dr, ts);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_box(const FXRect &br, const FXColor &borderColor, int borderWidth, const FXColor &bgcolor, int cornerDepth)
		{
		fx_draw_rect(br, borderWidth, cornerDepth, borderColor, bgcolor);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_fillRect(const FXRect &rect, const FXBrush &brush)
		{
		fx_draw_fillRect(rect, brush.color);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_frameRect(const FXRect &rect, const FXColor &color)
		{
		fl_rect(rect.left, rect.top, rect.width(), rect.height(), color.to_fl_color());
		}


FLTKEXT_DLL_EXPORT void
fx_draw_frameRect(const FXRect &rect, const FXBrush &brush)
		{
		fx_draw_frameRect(rect, brush.color);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_fillRect(const FXRect &rect, const FXColor &color)
		{
		int		r, g, b, a;

		color.getComponents(8, r, g, b, a);

		fl_rectf(rect.left, rect.top, rect.width(), rect.height(), r, g, b);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_rect(const FXRect &rect, int borderWidth, int cornerDepth, const FXColor &borderColor, const FXColor &fillColor)
		{
		if (cornerDepth == 0)
			{
			// Nice easy rectangle!
			if (fillColor != FX_COLOR_NONE)
				{
				fl_rectf(rect.left, rect.top, rect.width(), rect.height(), fillColor.red(), fillColor.green(), fillColor.blue());
				}
			
			if (borderColor != FX_COLOR_NONE)
				{
				FXRect	br(rect);
				int		w=borderWidth;

				while (w-- > 0)
					{
					fl_rect(br.left, br.top, br.width(), br.height(), borderColor.to_fl_color());
					br.deflate(1, 1, 1, 1);
					}
				}
			}
		else
			{
			double	dTop=rect.top;
			double	dLeft=rect.left;
			double	dRight=rect.right;
			double	dBottom=rect.bottom;

			double	dr;
			double	cd=cornerDepth;
			double	bw=borderWidth;
			double	hbw=bw/2;
			double	adj=1.0;

			if (bw < 1.0) adj = 0.0;
			dr = cd - hbw;

			for (int loop=0;loop<2;loop++)
				{
				if (loop == 0)
					{
					if (fillColor == FX_COLOR_NONE) continue;

					fl_begin_complex_polygon();
					fl_color(fillColor.to_fl_color());
					fl_line_style(FL_SOLID|FL_CAP_ROUND|FL_JOIN_ROUND, 0);
					}
				else
					{
					if (borderColor == FX_COLOR_NONE || borderWidth == 0) continue;

					fl_begin_loop();
					fl_color(borderColor.to_fl_color());
					fl_line_style(FL_SOLID|FL_CAP_ROUND|FL_JOIN_ROUND , borderWidth);
					}

				fl_arc(dRight - cd + adj, dTop + cd, dr, 0, 90);
				fl_arc(dLeft + cd, dTop + cd, dr, 90, 180);
				fl_arc(dLeft + cd, dBottom - cd + adj, dr, 180, 270);
				fl_arc(dRight - cd + adj, dBottom - cd + adj, dr, 270, 360);

				if (loop == 0)
					{
					fl_end_complex_polygon();
					}
				else
					{
					fl_end_loop();
					}

				fl_line_style(0);
				}
			}
		}





FLTKEXT_DLL_EXPORT void
fx_draw_arrowButton(const FXRect &dr, int style, const FXColor &btnColor, const FXColor &arrowColor, bool selected)
		{
		if (btnColor != FX_COLOR_NONE)
			{
			fx_draw_rect(dr, 0, 0, FX_COLOR_NONE, btnColor);

			btnColor.toShade(selected?0.5:1.5).select();
			fl_line(dr.left, dr.top+dr.height()-1, dr.left, dr.top, dr.left+dr.width()-1, dr.top);
			btnColor.toShade(selected?1.5:0.5).select();
			fl_line(dr.left, dr.top+dr.height()-1, dr.left+dr.width()-1, dr.top+dr.height()-1, dr.left+dr.width()-1, dr.top);
			}

		if (arrowColor != FX_COLOR_NONE)
			{
			arrowColor.select();

			if (style == FX_ARROW_UP || style == FX_ARROW_DOWN || style == FX_ARROW_LEFT || style == FX_ARROW_RIGHT)
				{
				double	dx=dr.left+1, dy=dr.top+1, dw=dr.width()-2, dh=dr.height()-2;
				double	h1=0.35, h2=0.65;
				double	v1=0.5, v2=0.25, v3=0.75;

				fl_begin_complex_polygon();

				switch (style)
					{
					case FX_ARROW_UP:
						fl_vertex(dx + dw * v1, dy + dh * h1); 
						fl_vertex(dx + dw * v2, dy + dh * h2); 
						fl_vertex(dx + dw * v3, dy + dh * h2); 
						fl_vertex(dx + dw * v1, dy + dh * h1); 
						break;

					case FX_ARROW_DOWN:
						fl_vertex(dx + dw * v1, dy + dh * h2); 
						fl_vertex(dx + dw * v2, dy + dh * h1); 
						fl_vertex(dx + dw * v3, dy + dh * h1); 
						fl_vertex(dx + dw * v1, dy + dh * h2); 
						break;

					case FX_ARROW_LEFT:
						fl_vertex(dx + dw * h1, dy + dh * v1); 
						fl_vertex(dx + dw * h2, dy + dh * v2); 
						fl_vertex(dx + dw * h2, dy + dh * v3); 
						fl_vertex(dx + dw * h1, dy + dh * v1); 
						break;

					case FX_ARROW_RIGHT:
						fl_vertex(dx + dw * h2, dy + dh * v1); 
						fl_vertex(dx + dw * h1, dy + dh * v2); 
						fl_vertex(dx + dw * h1, dy + dh * v3); 
						fl_vertex(dx + dw * h2, dy + dh * v1); 
						break;

					}

				fl_end_complex_polygon();
				}
			}
		}




FLTKEXT_DLL_EXPORT void
fx_draw_tick(const FXRect &br, const FXColor &color)
		{
		double	gap=(double)br.width() / 10.0;
		double	width=(double)br.width() - (gap * 2);
		double	height=(double)br.height() - (gap * 2);
		double	dleft=br.left + gap;
		double	dtop=br.top + gap;

		color.select();
		fl_begin_complex_polygon();
		fl_vertex(dleft + (width * 0.9), dtop + (height * 0.0));
		fl_vertex(dleft + (width * 1.0), dtop + (height * 0.1));
		fl_vertex(dleft + (width * 0.3), dtop + (height * 1.0));
		fl_vertex(dleft + (width * 0.0), dtop + (height * 0.7));
		fl_vertex(dleft + (width * 0.1), dtop + (height * 0.55));
		fl_vertex(dleft + (width * 0.28), dtop + (height * 0.75));
		fl_vertex(dleft + (width * 0.9), dtop + (height * 0.0));
		fl_end_complex_polygon();
		}




FLTKEXT_DLL_EXPORT void
fx_draw_cross(const FXRect &br, const FXColor &color)
		{
		double	gap=(double)br.width() / 10.0;
		double	width=(double)br.width() - (gap * 2);
		double	height=(double)br.height() - (gap * 2);
		double	dleft=br.left + gap;
		double	dtop=br.top + gap;
		double	m1=0.15;
		double	m2=0.35;
		double	m3=1.0-m1;
		double	m4=1-m2;

		color.select();
		fl_begin_complex_polygon();
		fl_vertex(dleft + (width * m1), dtop + (height * 0.0));
		fl_vertex(dleft + (width * 0.5), dtop + (height * m2));
		fl_vertex(dleft + (width * m3), dtop + (height * .00));
		fl_vertex(dleft + (width * 1.0), dtop + (height * m1));
		fl_vertex(dleft + (width * m4), dtop + (height * 0.5));
		fl_vertex(dleft + (width * 1.0), dtop + (height * m3));
		fl_vertex(dleft + (width * m3), dtop + (height * 1.0));
		fl_vertex(dleft + (width * 0.5), dtop + (height * m4));
		fl_vertex(dleft + (width * m1), dtop + (height * 1.0));
		fl_vertex(dleft + (width * 0.0), dtop + (height * m3));
		fl_vertex(dleft + (width * m2), dtop + (height * 0.5));
		fl_vertex(dleft + (width * 0.0), dtop + (height * m1));
		fl_vertex(dleft + (width * m1), dtop + (height * 0.0));
		fl_end_complex_polygon();
		}



FLTKEXT_DLL_EXPORT void
fx_draw_setDefaultLineStyle()
		{
		fl_line_style(FL_SOLID, 1, NULL);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_setLineStyle(const FXPen &pen)
		{
		fx_draw_setLineStyle(pen.color, pen.style, pen.width);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_setLineStyle(const FXColor &color, int linestyle, int linewidth)
		{		
		char		m_linePattern[16];		///< A line drawing pattern used for dashed and dotted lines

		int	lw=linewidth;

		if (lw < 1) lw = 1;

		int	dw=lw;

		if (dw > 7) dw = 7;

		fl_color(color.to_fl_color());

		memset(m_linePattern, 0, sizeof(m_linePattern));

		switch (linestyle)
			{
			case FX_LINESTYLE_SOLID:
				// Solid
				fl_line_style(FL_SOLID,		lw, NULL);
				break;

			case FX_LINESTYLE_DASHES:
				// Even Dashes
				m_linePattern[0] = 8 * dw;
				m_linePattern[1] = 8 * dw;
				fl_line_style(FL_DASH,		lw, m_linePattern);
				break;

			case FX_LINESTYLE_DASHDOTS:
				// Dash Dot
				m_linePattern[0] = 16 * dw;
				m_linePattern[1] = 4 * dw;
				m_linePattern[2] = 4 * dw;
				m_linePattern[3] = 4 * dw;
				fl_line_style(FL_DASHDOT,	lw, m_linePattern);
				break;

			case FX_LINESTYLE_DOTS:
				// Dots
				m_linePattern[0] = 1 * dw;
				m_linePattern[1] = 2 * dw;
				fl_line_style(FL_DOT,	lw, m_linePattern);
				break;

			case FX_LINESTYLE_FINEDASHES:
				// Fine Dashes
				m_linePattern[0] = 6 * dw;
				m_linePattern[1] = 2 * dw;
				fl_line_style(FL_DASH,		lw, m_linePattern);
				break;

			default:
				// Default to a solid line
				fl_line_style(FL_SOLID,		lw, NULL);
				break;
			}
		}

static FXPoint	s_lastPoint;

FLTKEXT_DLL_EXPORT void
fx_draw_moveTo(int X, int Y)
		{
		s_lastPoint.x = X;
		s_lastPoint.y = Y;
		}


FLTKEXT_DLL_EXPORT void
fx_draw_lineTo(int X, int Y)
		{
		fl_line(s_lastPoint.x, s_lastPoint.y, X, Y);
		s_lastPoint.x = X;
		s_lastPoint.y = Y;
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(int fromX, int fromY, int toX, int toY)
		{
		fl_line(fromX, fromY, toX, toY);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo)
		{
		fl_line(ptFrom.x, ptFrom.y, ptTo.x, ptTo.y);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(int fromX, int fromY, int toX, int toY, const FXColor &color, int linestyle, int linewidth)
		{
		fx_draw_setLineStyle(color, linestyle, linewidth);
		fl_line(fromX, fromY, toX, toY);
		fl_line_style(FL_SOLID, 1, NULL);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(int fromX, int fromY, int toX, int toY, const FXPen &pen)
		{
		fx_draw_line(fromX, fromY, toX, toY, pen.color, pen.style, pen.width);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo, const FXPen &pen)
		{
		fx_draw_line(ptFrom.x, ptFrom.y, ptTo.x, ptTo.y, pen.color, pen.style, pen.width);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_line(const FXPoint &ptFrom, const FXPoint &ptTo, const FXColor &color, int style, int linewidth)
		{
		fx_draw_line(ptFrom.x, ptFrom.y, ptTo.x, ptTo.y, color, style, linewidth);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_triangle(int x1, int y1, int x2, int y2, int x3, int y3, const FXColor &color, const FXColor &fillColor)
		{
		if (color != FX_COLOR_NONE)
			{
			color.select();
			fl_begin_loop();
			fl_vertex(x1, y1);
			fl_vertex(x2, y2);
			fl_vertex(x3, y3);
			fl_end_loop();
			}
		
		if (fillColor != FX_COLOR_NONE)
			{
			fillColor.select();
			fl_begin_polygon();
			fl_vertex(x1, y1);
			fl_vertex(x2, y2);
			fl_vertex(x3, y3);
			fl_end_polygon();
			}
		}


FLTKEXT_DLL_EXPORT void
fx_draw_ellipse(const FXRect &rect)
		{
		fl_arc(rect.left, rect.top, rect.width(), rect.height(), 0.0, 360.0);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_ellipse(const FXRect &rect, const FXColor &color)
		{
		color.select();
		fl_arc(rect.left, rect.top, rect.width(), rect.height(), 0.0, 360.0);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_arc(const FXRect &rect, double aStart, double aFinish)
		{
		fl_arc(rect.left, rect.top, rect.width(), rect.height(), aStart, aFinish);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_arc(const FXRect &rect, const FXColor &color, double aStart, double aFinish)
		{
		color.select();
		fl_arc(rect.left, rect.top, rect.width(), rect.height(), aStart, aFinish);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_pie(const FXRect &rect, double aStart, double aFinish)
		{
		fl_pie(rect.left, rect.top, rect.width(), rect.height(), aStart, aFinish);
		}

FLTKEXT_DLL_EXPORT void
fx_draw_pie(const FXRect &rect, const FXColor &color, double aStart, double aFinish)
		{
		color.select();
		fl_pie(rect.left, rect.top, rect.width(), rect.height(), aStart, aFinish);
		}





FLTKEXT_DLL_EXPORT void
fx_draw_image(sw_byte_t *pData, const FXRect &dr, const FXRect &sr, int bytesPerPixel, int bytesPerLine)
		{
		if (bytesPerLine == 0) bytesPerLine = bytesPerPixel * dr.width();
		fx_draw_image(pData, dr.left, dr.top, dr.width(), dr.height(), sr.left, sr.top, bytesPerPixel, bytesPerLine);
		}


FLTKEXT_DLL_EXPORT void
fx_draw_image(sw_byte_t *pData, int posX, int posY, int W, int H, int offX, int offY, int bytesPerPixel, int bytesPerLine)
		{
		if (bytesPerLine == 0) bytesPerLine = bytesPerPixel * W;
		if (W > 0 && H > 0) fl_draw_image(pData, posX, posY, W, H, bytesPerPixel, bytesPerLine);
		}