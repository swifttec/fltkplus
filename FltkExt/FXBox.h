/*
**	FXBox.h	A generic dialog class
*/

#ifndef __FltkExt_FXBox_h__
#define __FltkExt_FXBox_h__

#include <FltkExt/FXWidget.h>

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXBox : public FXWidget
		{
public:
		FXBox(const FXColor &bgcolor);
		FXBox(int X, int Y, int W, int H, const FXColor &bgcolor="");
		};


#endif // __FltkExt_FXBox_h__
