#include "FXScrollableGroup.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <FltkExt/FXSeparator.h>
#include <FltkExt/FXWnd.h>


void
FXScrollableGroup::genericCallback(Fl_Widget *pWidget, void *pUserData)
		{
		FXScrollableGroup	*pSheet=(FXScrollableGroup *)pUserData;

		pSheet->onCallback(pWidget);
		}


void
FXScrollableGroup::itemCallback(Fl_Widget *pWidget)
		{
		FXScrollableGroup	*pSheet=(FXScrollableGroup *)pWidget->parent();

		pSheet->onCallback(pWidget);
		}


FXScrollableGroup::FXScrollableGroup(int flags) :
	m_vScrollBar(true),
	m_hScrollBar(false)
		{
		init(flags);
		}


FXScrollableGroup::FXScrollableGroup(int flags, int W, int H) :
	FXGroup(W, H),
	m_vScrollBar(true),
	m_hScrollBar(false)
		{
		init(flags);
		}

/// Constructor with width and position
FXScrollableGroup::FXScrollableGroup(int flags, int X, int Y, int W, int H) :
	FXGroup(X, Y, W, H),
	m_vScrollBar(true),
	m_hScrollBar(false)
		{
		init(flags);
		}

/// Constructor
FXScrollableGroup::FXScrollableGroup(int flags, int X, int Y, int W, int H, const FXColor &bgcolor, const FXColor &bdcolor, const FXColor &fgcolor) :
	FXGroup(X, Y, W, H, bgcolor, bdcolor, fgcolor),
	m_vScrollBar(true),
	m_hScrollBar(false)
		{
		init(flags);
		}

void
FXScrollableGroup::init(int flags)
		{
		int		width=w();
		int		height=h();

		m_scrollflags = flags;
		m_idTimer = 0;

		if ((m_scrollflags & VScrollBar) != 0) width -= 16;
		if ((m_scrollflags & HScrollBar) != 0) height -= 16;
		}


void
FXScrollableGroup::scrollToTopLeft()
		{
		FXScrollInfo	si;

		m_vScrollBar.setScrollPosition(0);
		m_hScrollBar.setScrollPosition(0);
		invalidate();
		}


void
FXScrollableGroup::scrollToTop()
		{
		FXScrollInfo	si;

		m_vScrollBar.setScrollPosition(0);
		invalidate();
		}


void
FXScrollableGroup::onCallback(Fl_Widget *pWidget)
		{
		Fl_Widget	*pParent=parent();
		bool		handled=false;

		if (!handled)
			{
			FXWnd	*pWnd=dynamic_cast<FXWnd *>(pParent);

			if (pWnd != NULL)
				{
				pWnd->onItemCallback(pWidget, NULL);
				handled = true;
				}
			}

		if (!handled)
			{
			FXGroup	*pGroup=dynamic_cast<FXGroup *>(pParent);

			if (pGroup != NULL)
				{
				pGroup->onItemCallback(pWidget, NULL);
				handled = true;
				}
			}
		}




void
FXScrollableGroup::onDraw()
		{
		FXSize	szContents=getChildrenSize();

		int				width=w();
		int				height=h();
		int				xadj=0, yadj=0;
		FXScrollInfo	si;

		if ((m_scrollflags & VScrollBar) != 0) width -= 16;
		if ((m_scrollflags & HScrollBar) != 0) height -= 16;

		if ((m_scrollflags & VScrollBar) != 0)
			{
			FXRect			br(x()+width, y(), x()+w()-1, y()+height-1);
			int				linesize=height/20;
			
			if (linesize < 1) linesize = 1;

			m_vScrollBar.getScrollInfo(si);
			m_vScrollBar.setScrollRange(0, szContents.cy, height);
			m_vScrollBar.setScrollLineSize(linesize);
			m_vScrollBar.onDraw(br);
			yadj = -si.position();
			}

		if ((m_scrollflags & HScrollBar) != 0)
			{
			FXRect	br(x(), y()+height, x()+width-1, y()+h()-1);
			int				linesize=height/20;
			
			if (linesize < 1) linesize = 1;

			m_hScrollBar.getScrollInfo(si);
			m_hScrollBar.setScrollRange(0, szContents.cx, width);
			m_hScrollBar.setScrollLineSize(linesize);
			m_hScrollBar.onDraw(br);
			xadj = -si.position();
			}

		fl_push_clip(x(), y(), width, height);
		FXGroup::onDraw();
		fl_pop_clip();
		}


int
FXScrollableGroup::onMouseDown(int button, const FXPoint &pt)
		{
		int				res=0;
		FXScrollInfo	hi, vi;

		saveScrollInfo(hi, vi);

		if ((m_scrollflags & VScrollBar) != 0 && m_vScrollBar.contains(pt))
			{
			res = m_vScrollBar.onMouseDown(button, pt);
			if (res) m_vTrackMouse = true;
			}
		else if ((m_scrollflags & HScrollBar) != 0 && m_hScrollBar.contains(pt))
			{
			res = m_hScrollBar.onMouseDown(button, pt);
			if (res) m_hTrackMouse = true;
			}
		
		if (adjustChildren(hi, vi)) res = 1;

		if (res)
			{
			if (res > 1) m_idTimer = setIntervalTimerMS(1, 100, NULL);
			invalidate();
			}

		return res;
		}


int
FXScrollableGroup::onMouseUp(int button, const FXPoint &pt)
		{
		int				res=0;
		FXScrollInfo	hi, vi;
		
		saveScrollInfo(hi, vi);

		if (m_vTrackMouse)
			{
			res = m_vScrollBar.onMouseUp(button, pt);
			m_vTrackMouse = false;
			}
		else if (m_hTrackMouse)
			{
			res = m_hScrollBar.onMouseUp(button, pt);
			m_hTrackMouse = false;
			}

		if (adjustChildren(hi, vi)) res = 1;

		if (res)
			{
			invalidate();
			}

		if (m_idTimer != 0)
			{
			killIntervalTimer(1);
			m_idTimer = 0;
			}

		return res;
		}


int
FXScrollableGroup::onMouseDrag(const FXPoint &pt)
		{
		int				res=0;
		FXScrollInfo	hi, vi;

		saveScrollInfo(hi, vi);
		
		if (m_vTrackMouse)
			{
			res = m_vScrollBar.onMouseDrag(pt);
			}
		else if (m_hTrackMouse)
			{
			res = m_hScrollBar.onMouseDrag(pt);
			}

		if (adjustChildren(hi, vi)) res = 1;
		if (res) invalidate();

		return res;
		}

int
FXScrollableGroup::onMouseWheel(int dx, int dy, const FXPoint &pt)
		{
		int				res=0;
		FXScrollInfo	hi, vi;

		saveScrollInfo(hi, vi);

		if ((m_scrollflags & VScrollBar) != 0 && dy != 0)
			{
			res = m_vScrollBar.onMouseWheel(dx, dy, pt);
			}
		else if ((m_scrollflags & HScrollBar) != 0 && dx != 0)
			{
			res = m_hScrollBar.onMouseWheel(dx, dy, pt);
			}
		
		if (adjustChildren(hi, vi)) res = 1;

		if (res) invalidate();

		return res;
		}


bool
FXScrollableGroup::onTimer(sw_uint32_t id, void *data)
		{
		bool	changed=false;

		FXScrollInfo	hi, vi;

		saveScrollInfo(hi, vi);

		if (m_vScrollBar.onTimer(id, data)) changed = true;
		if (m_hScrollBar.onTimer(id, data)) changed = true;

		if (adjustChildren(hi, vi)) changed = true;

		if (changed) invalidate();

		return true;
		}



FXSize
FXScrollableGroup::getMinSize() const
		{
		FXSize	res=m_minSize; // =FXGroup::getMinSize();

		if ((m_scrollflags & VScrollBar) != 0)
			{
			res.cx += 16;
			res.cy = sw_math_min(res.cy, 32);
			}

		if ((m_scrollflags & HScrollBar) != 0)
			{
			res.cy += 16;
			res.cx = sw_math_min(res.cx, 32);
			}

		return res;
		}


FXSize
FXScrollableGroup::getMaxSize() const
		{
		FXSize	res=FXGroup::getMaxSize();

		if ((m_scrollflags & VScrollBar) != 0 && res.cx != 0) res.cx += 16;
		if ((m_scrollflags & HScrollBar) != 0 && res.cy != 0) res.cy += 16;

		return res;
		}


void
FXScrollableGroup::getContentsRect(FXRect &rect) const
		{
		FXGroup::getContentsRect(rect);

		if ((m_scrollflags & VScrollBar) != 0)
			{
			FXScrollInfo	si;
			
			m_vScrollBar.getScrollInfo(si);
			rect.right -= 16;
			rect.top -= si.currentPos();
			rect.bottom -= si.currentPos();
			}
		
		if ((m_scrollflags & HScrollBar) != 0)
			{
			FXScrollInfo	si;
			
			m_hScrollBar.getScrollInfo(si);
			rect.bottom -= 16;
			rect.left -= si.currentPos();
			rect.right -= si.currentPos();
			}
		}


// Save the scroll info from the horz and vert bars
void
FXScrollableGroup::saveScrollInfo(FXScrollInfo &hi, FXScrollInfo &vi)
		{
		hi.clear();
		vi.clear();
		if ((m_scrollflags & HScrollBar) != 0) m_hScrollBar.getScrollInfo(hi);
		if ((m_scrollflags & VScrollBar) != 0) m_vScrollBar.getScrollInfo(vi);
		}


// Adjust the children position using the saved scroll info as a reference
bool
FXScrollableGroup::adjustChildren(const FXScrollInfo &hi, const FXScrollInfo &vi)
		{
		int		adjx=0, adjy=0;
		bool	res=false;

		if ((m_scrollflags & HScrollBar) != 0)
			{
			FXScrollInfo	si;
			
			m_hScrollBar.getScrollInfo(si);
			adjx = hi.currentPos() - si.currentPos();
			}
		
		if ((m_scrollflags & VScrollBar) != 0)
			{
			FXScrollInfo	si;
			
			m_vScrollBar.getScrollInfo(si);
			adjy = vi.currentPos() - si.currentPos();
			}

		if (adjx != 0 || adjy != 0)
			{
			Fl_Widget		*const *a;
			int				nchildren=children();

			a = array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				o->position(o->x() + adjx, o->y() + adjy);
				}

			res = true;
			}

		return res;
		}

