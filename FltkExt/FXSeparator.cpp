#include "FXSeparator.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>


FXSeparator::FXSeparator(bool vertical, const FXColor &color) :
	FXWidget(3, 3),
	m_vertical(vertical),
	m_width(3)
		{
		}


FXSeparator::FXSeparator(int X, int Y, int W, int H, bool vertical, const FXColor &color, int linewidth) :
	FXWidget(X, Y, W, H),
	m_vertical(vertical),
	m_width(linewidth),
	m_color(color)
		{
		}


void
FXSeparator::onDraw()
		{
		FXColor	color=getBackgroundColor().toShade(0.8);

		if (m_color != FX_COLOR_DEFAULT) color = m_color;
		
		color.select();
		fl_line_style(FL_SOLID, m_width);

		if (m_vertical)
			{
			fl_line(x()+w()/2, y(), x()+w()/2, y()+h()-1);
			}
		else
			{
			fl_line(x(), y()+h()/2, x()+w()-1, y()+h()/2);
			}

		fl_line_style(FL_SOLID);
		}
