#ifndef __FltkExt_FXLayout_h__
#define __FltkExt_FXLayout_h__

#include <FltkExt/FXSize.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXGroup.h>

/**
*** A base class to represent a child layout manager
***
*** A layout manager needs to layout the children in a window when they are added
*** or the window is resized. The FXLayout class provides a base level class which 
*** specific layout types will derive from.
**/
class FLTKEXT_DLL_EXPORT FXLayout
		{
public:
		enum FlagIDs
			{
			NoMaxSize	= 0x0001,	///< If set the layout has no maximum size (ignores child widgets)
			NoMinSize	= 0x0002	///< If set the layout has no minumum size (ignores child widgets)
			};
		
public:
		/// Constructor
		FXLayout(int layoutFlags=0);
		
		/// Destructor
		virtual ~FXLayout();
		
		/**
		*** Set the owner of this layout
		**/
		void				setOwner(Fl_Group *pOwner);
		
		/**
		*** Set the padding (outer margins) of this layout
		**/
		void				padding(const FXPadding &v)				{ m_padding = v; }

		/**
		*** Get the padding (outer margins) of this layout
		**/
		const FXPadding &	padding() const							{ return m_padding; }
		
		/**
		*** Set the padding (outer margins) of this layout
		**/
		void				gapBetweenChildren(const FXSize &v)		{ m_gapBetweenChildren = v; }

		/**
		*** Get the padding (outer margins) of this layout
		**/
		const FXSize &		gapBetweenChildren() const				{ return m_gapBetweenChildren; }

		/// Test to see if the given flag is set
		bool				isFlagSet(int v) const					{ return ((m_flags & v) != 0); }

public: // Overrideable methods
		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget);

		/**
		*** Get the minimum size of this layout based on the current children.
		***
		*** The minimum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		**/
		virtual FXSize		getMinSize() const;


		/**
		*** Get the maximum size of this layout based on the current children.
		***
		*** The maximum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		***
		*** If a dimension is zero this implies that there is no maximum size for that
		*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
		*** the width is restricted to 1000 pixels, but the height is unrestricted.
		**/
		virtual FXSize		getMaxSize() const;

		/// Called to layout the children
		virtual void		layoutChildren();

		/// Called to resize the layout and move the children
		virtual int			getChildCount() const;

protected:
		Fl_Group	*m_pOwner;				///< The group that owns this layout
		FXPadding	m_padding;				///< The outer padding
		FXSize		m_gapBetweenChildren;
		int			m_flags;				///< Flags that determine how widgets are layout
		};

#endif // __FltkExt_FXLayout_h__
