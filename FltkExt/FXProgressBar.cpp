#include "FXProgressBar.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

FXProgressBar::FXProgressBar(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H)
		{
		init();
		}

FXProgressBar::FXProgressBar(int W, int H) :
	FXWidget(0, 0, W, H)
		{
		init();
		}


FXProgressBar::~FXProgressBar()
		{
		}


void
FXProgressBar::init()
		{
		FXWidget::init();
		m_barColor = FXColor(128, 224, 255);
		m_fontface = FL_HELVETICA;
		m_fontsize = 10;
		m_minpos = 0;
		m_maxpos = 100;
		m_position = 0;
		m_barstyle = 0;
		setBackgroundColor("#fff");
		}

void
FXProgressBar::onDraw()
		{
		sw_int64_t		X=x()+1;
		sw_int64_t		Y=y()+1;
		sw_int64_t		W=w()-2;
		sw_int64_t		H=h()-2;

		sw_int64_t		pos=m_position;

		if (pos < m_minpos) pos = m_minpos;
		if (pos > m_maxpos) pos = m_maxpos;

		sw_int64_t		range=(m_maxpos - m_minpos);

		if (pos > 0 && range > 0)
			{
			pos -= m_minpos;
		
			m_barColor.select();
			switch (m_barstyle)
				{
				case 1: // Right to left
					{
					sw_int64_t		bs=((W * pos) / range);

					fl_rectf((int)(X+W-bs), (int)Y, (int)bs, (int)H);
					}
					break;
					
				case 2: // Bottom to top
					{
					sw_int64_t		bs=((H * pos) / range);

					fl_rectf((int)X, (int)(Y+H-bs), (int)W, (int)bs);
					}
					break;
					
				case 3: // Top to bottom
					fl_rectf((int)X, (int)Y, (int)W, (int)(((H * pos) / range)));
					break;
					
				default: // Left to right
					fl_rectf((int)X, (int)Y, (int)(((W * pos) / range)), (int)H);
					break;
				}
			}


		fl_font(m_fontface, m_fontsize);
		m_fgColor.select();
		fl_draw(m_text, (int)X, (int)Y, (int)W, (int)H, FL_ALIGN_CENTER);
		}


// Update the progress bar
void
FXProgressBar::update(const SWString &text, int pos, bool redrawflag)
		{
		m_text = text;
		m_position = pos;
		if (redrawflag) redraw();
		}


// Update the progress bar
void
FXProgressBar::setText(const SWString &text, bool redrawflag)
		{
		m_text = text;
		if (redrawflag) redraw();
		}


// Update the progress bar
void
FXProgressBar::setPosition(int pos, bool redrawflag)
		{
		m_position = pos;
		if (redrawflag) redraw();
		}