#include "FXBlank.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

FXBlank::FXBlank()
		{
		setForegroundColor(FX_COLOR_NONE);
		setBackgroundColor(FX_COLOR_NONE);
		setBorderColor(FX_COLOR_NONE);
		}

FXBlank::FXBlank(int W, int H) :
	FXWidget(W, H)
		{
		setForegroundColor(FX_COLOR_NONE);
		setBackgroundColor(FX_COLOR_NONE);
		setBorderColor(FX_COLOR_NONE);
		}

FXBlank::FXBlank(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H)
		{
		setForegroundColor(FX_COLOR_NONE);
		setBackgroundColor(FX_COLOR_NONE);
		setBorderColor(FX_COLOR_NONE);
		}
