#include "FXScrollBar.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXGroup.h>




FXScrollBar::FXScrollBar(int X, int Y, int W, int H, bool vertical, Fl_Widget *pNotify) :
	FXWidget(X, Y, W, H),
	m_object(vertical, pNotify),
	m_idTimer(0)
		{
		setBackgroundColor(FX_COLOR_NONE);
		setBorder(0, FX_COLOR_NONE, 0, 0);

		if (vertical)
			{
			// Width is fixed
			setMinSize(W, (W*2)+8);
			setMaxSize(W, 0);
			}
		else
			{
			// Height is fixed
			setMinSize((H*2)+8, H);
			setMaxSize(0, H);
			}
		}



void
FXScrollBar::onDraw()
		{
		FXRect	cr;

		getClientRect(cr);
		m_object.onDraw(cr);
		}


int
FXScrollBar::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=m_object.onMouseDown(button, pt);

		if (res)
			{
			if (res > 1) m_idTimer = setIntervalTimerMS(1, 100, NULL);
			invalidate();
			}

		return res;
		}


int
FXScrollBar::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=m_object.onMouseUp(button, pt);

		if (res) invalidate();
		if (m_idTimer != 0)
			{
			killIntervalTimer(m_idTimer);
			m_idTimer = 0;
			}

		return res;
		}


int
FXScrollBar::onMouseDrag(const FXPoint &pt)
		{
		int		res=m_object.onMouseDrag(pt);

		if (res) invalidate();

		return res;
		}

bool
FXScrollBar::onTimer(sw_uint32_t id, void *data)
		{
		bool	res=m_object.onTimer(id, data);

		if (res) invalidate();

		return true;
		}

int
FXScrollBar::onMouseWheel(int dx, int dy, const FXPoint &pt)
		{
		int	res=m_object.onMouseWheel(dx, dy, pt);

		if (res) invalidate();

		return res;
		}

enum
	{
	ArrowNone=0,
	ArrowUp,
	ArrowDown,
	ArrowLeft,
	ArrowRight
	};


FXScrollBarObject::FXScrollBarObject(bool vertical, Fl_Widget *pNotify) :
	m_vertical(vertical),
	m_btnColor("#ccc"),
	m_arrowColor("#000"),
	m_scrollInfo(0, 100, 20, 0),
	m_btnIndex(-1),
	m_pNotifyWidget(pNotify)
		{
		}


void
FXScrollBarObject::onDraw(const FXRect &dr)
		{
		FXRect	cr(dr);

		fx_draw_rect(dr, 1, 0, "#444", "#888");
		cr.deflate(1, 1, 1, 1);

		m_rect = cr;

		int	range=m_scrollInfo.rangeTo()-m_scrollInfo.rangeFrom();
		if (range < 1) range = 1;

		int	pos=m_scrollInfo.position()-m_scrollInfo.rangeFrom();

		if (pos < 0) pos = 0;
		if (pos >= range) pos = range;

		if (m_vertical)
			{
			drawButton(cr.left, cr.top, cr.width(), cr.width(), ArrowUp, m_btnRect[0], m_btnIndex == 0);
			drawButton(cr.left, cr.bottom-cr.width()+1, cr.width(), cr.width(), ArrowDown, m_btnRect[1], m_btnIndex == 1);

			m_barSize = cr.height() - (cr.width() * 2);
			int		startpos=(pos * m_barSize) / range;
			int		endpos=((pos + m_scrollInfo.pageSize()) * m_barSize) / range;

#define MINSIZE	4
			if ((endpos - startpos) < MINSIZE) endpos = startpos + MINSIZE;
			if (endpos > m_barSize) endpos = m_barSize;
			if ((endpos - startpos) < MINSIZE) startpos = endpos - MINSIZE;
			if (startpos < 0) startpos = 0;

			drawButton(cr.left, cr.top+cr.width()+startpos, cr.width(), endpos-startpos, ArrowNone, m_btnRect[2], m_btnIndex == 2);

			FXRect	*pGap;

			pGap = &m_gapRect[0];
			pGap->left = cr.left;
			pGap->right = cr.right;
			pGap->top = m_btnRect[0].bottom+1;
			pGap->bottom = m_btnRect[2].top-1;

			pGap = &m_gapRect[1];
			pGap->left = cr.left;
			pGap->right = cr.right;
			pGap->top = m_btnRect[2].bottom+1;
			pGap->bottom = m_btnRect[1].top-1;
			}
		else
			{
			drawButton(cr.left, cr.top, cr.height(), cr.height(), ArrowLeft, m_btnRect[0], m_btnIndex == 0);
			drawButton(cr.right-cr.height()+1, cr.top, cr.height(), cr.height(), ArrowRight, m_btnRect[1], m_btnIndex == 1);

			m_barSize = cr.width() - (cr.height() * 2);
			int		startpos=(pos * m_barSize) / range;
			int		endpos=((pos + m_scrollInfo.pageSize()) * m_barSize) / range;

			if ((endpos - startpos) < MINSIZE) endpos = startpos + MINSIZE;
			if (endpos > m_barSize) endpos = m_barSize;
			if ((endpos - startpos) < MINSIZE) startpos = endpos - MINSIZE;
			if (startpos < 0) startpos = 0;

			drawButton(cr.left+cr.height()+startpos, cr.top, endpos-startpos, cr.height(), ArrowNone, m_btnRect[2], m_btnIndex == 2);

			FXRect	*pGap;

			pGap = &m_gapRect[0];
			pGap->top = cr.top;
			pGap->bottom = cr.bottom;
			pGap->left = m_btnRect[0].right+1;
			pGap->right = m_btnRect[2].left-1;

			pGap = &m_gapRect[1];
			pGap->top = cr.top;
			pGap->bottom = cr.bottom;
			pGap->left = m_btnRect[2].right+1;
			pGap->right = m_btnRect[1].left-1;
			}
		}


void
FXScrollBarObject::drawButton(int X, int Y, int W, int H, int style, FXRect &dr, bool selected)
		{
		dr = FXRect(X, Y, X+W-1, Y+H-1);

		fx_draw_fillRect(dr, m_btnColor);

		m_btnColor.toShade(selected?0.5:1.5).select();
		fl_line(dr.left, dr.top+H-1, dr.left, dr.top, dr.left+W-1, dr.top);
		m_btnColor.toShade(selected?1.5:0.5).select();
		fl_line(dr.left, dr.top+H-1, dr.left+W-1, dr.top+H-1, dr.left+W-1, dr.top);

		m_arrowColor.select();

		if (style != ArrowNone)
			{
			double	dx=X, dy=Y, dw=W-1, dh=H-1;
			double	h1=0.35, h2=0.65;
			double	v1=0.5, v2=0.25, v3=0.75;

			fl_begin_polygon();

			switch (style)
				{
				case ArrowUp:
					fl_vertex(dx + dw * v1, dy + dh * h1); 
					fl_vertex(dx + dw * v2, dy + dh * h2); 
					fl_vertex(dx + dw * v3, dy + dh * h2); 
					fl_vertex(dx + dw * v1, dy + dh * h1); 
					break;

				case ArrowDown:
					fl_vertex(dx + dw * v1, dy + dh * h2); 
					fl_vertex(dx + dw * v2, dy + dh * h1); 
					fl_vertex(dx + dw * v3, dy + dh * h1); 
					fl_vertex(dx + dw * v1, dy + dh * h2); 
					break;

				case ArrowLeft:
					fl_vertex(dx + dw * h1, dy + dh * v1); 
					fl_vertex(dx + dw * h2, dy + dh * v2); 
					fl_vertex(dx + dw * h2, dy + dh * v3); 
					fl_vertex(dx + dw * h1, dy + dh * v1); 
					break;

				case ArrowRight:
					fl_vertex(dx + dw * h2, dy + dh * v1); 
					fl_vertex(dx + dw * h1, dy + dh * v2); 
					fl_vertex(dx + dw * h1, dy + dh * v3); 
					fl_vertex(dx + dw * h2, dy + dh * v1); 
					break;

				}

			fl_end_polygon();
			}
		}



int
FXScrollBarObject::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		if (button == FL_LEFT_MOUSE)
			{
			bool	changed=false;

			m_dragStartPosition = m_scrollInfo.position();
			m_dragStartPoint = pt;
			if (m_btnRect[0].contains(pt))
				{
				m_btnIndex = 0;
				res = 2;
				if (m_scrollInfo.position() > m_scrollInfo.firstPosition())
					{
					m_scrollInfo.setPosition(m_scrollInfo.position() - m_scrollInfo.lineSize());
					doNotify(FX_SCROLL_UP);
					}
				}
			else if (m_btnRect[1].contains(pt))
				{
				m_btnIndex = 1;
				res = 2;
				if (m_scrollInfo.position() < m_scrollInfo.lastPosition())
					{
					m_scrollInfo.setPosition(m_scrollInfo.position() + m_scrollInfo.lineSize());
					doNotify(FX_SCROLL_DOWN);
					}
				}
			else if (m_btnRect[2].contains(pt))
				{
				m_btnIndex = 2;
				res = 1;
				}
			else if (m_gapRect[0].contains(pt))
				{
				m_btnIndex = 3;
				res = 2;
				if (m_scrollInfo.position() > m_scrollInfo.firstPosition())
					{
					m_scrollInfo.setPosition(m_scrollInfo.position() - m_scrollInfo.pageSize());
					doNotify(FX_SCROLL_PAGEUP);
					}
				}
			else if (m_gapRect[1].contains(pt))
				{
				m_btnIndex = 4;
				res = 2;
				if (m_scrollInfo.position() < m_scrollInfo.lastPosition())
					{
					m_scrollInfo.setPosition(m_scrollInfo.position() + m_scrollInfo.pageSize());
					doNotify(FX_SCROLL_PAGEUP);
					}
				}
			}

		return res;
		}


int
FXScrollBarObject::onMouseUp(int button, const FXPoint &pt)
		{
		m_btnIndex = -1;

		return 1;
		}


int
FXScrollBarObject::onMouseDrag(const FXPoint &pt)
		{
		if (m_btnIndex == 2)
			{
			int	range=m_scrollInfo.rangeSize();
			int	pos=m_dragStartPosition;
			int	page=m_scrollInfo.pageSize();

			if (m_vertical)
				{
				pos -= (m_dragStartPoint.y - pt.y) * range / m_barSize;
				}
			else
				{
				pos -= (m_dragStartPoint.x - pt.x) * range / m_barSize;
				}

			if (pos < 0) pos = 0;
			if (pos >= (range-page)) pos = range-page;

			
			m_scrollInfo.setPosition(pos);

			doNotify(FX_SCROLL_TRACK);
			}

		return 1;
		}


int
FXScrollBarObject::onMouseWheel(int dx, int dy, const FXPoint &pt)
		{
		int		change=m_vertical?dy:dx;

		if (change < 0)
			{
			if (m_scrollInfo.position() > m_scrollInfo.rangeFrom())
				{
				m_scrollInfo.changePositionByLines(change);
				doNotify(FX_SCROLL_UP);
				}
			}
		else if (change > 0)
			{
			if (m_scrollInfo.position() < (m_scrollInfo.rangeTo() - m_scrollInfo.pageSize()))
				{
				m_scrollInfo.changePositionByLines(change);
				doNotify(FX_SCROLL_DOWN);
				}
			}
		
		return 1;
		}


bool
FXScrollBarObject::onTimer(sw_uint32_t id, void *data)
		{
		bool	changed=false;

		if (m_btnIndex == 0)
			{
			if (m_scrollInfo.position() > m_scrollInfo.rangeFrom())
				{
				m_scrollInfo.setPosition(m_scrollInfo.position() - m_scrollInfo.lineSize());
				changed = true;
				doNotify(FX_SCROLL_UP);
				}
			}
		else if (m_btnIndex == 1)
			{
			if (m_scrollInfo.position() < m_scrollInfo.lastPosition())
				{
				m_scrollInfo.setPosition(m_scrollInfo.position() + m_scrollInfo.lineSize());
				changed = true;
				doNotify(FX_SCROLL_DOWN);
				}
			}
		else if (m_btnIndex == 3)
			{
			if (m_scrollInfo.position() > m_scrollInfo.rangeFrom())
				{
				m_scrollInfo.setPosition(m_scrollInfo.position() - m_scrollInfo.pageSize());
				changed = true;
				doNotify(FX_SCROLL_PAGEUP);
				}
			}
		else if (m_btnIndex == 4)
			{
			if (m_scrollInfo.position() < m_scrollInfo.lastPosition())
				{
				m_scrollInfo.setPosition(m_scrollInfo.position() + m_scrollInfo.pageSize());
				changed = true;
				doNotify(FX_SCROLL_PAGEUP);
				}
			}

		return changed;
		}


void
FXScrollBarObject::doNotify(int action)
		{
		if (m_pNotifyWidget != NULL)
			{
			int		bar=m_vertical?FX_SCROLLBAR_VERTICAL:FX_SCROLLBAR_HORIZONTAL;
			bool	done=false;
			FXSize	res;

			if (!done)
				{
				FXWidget	*p=dynamic_cast<FXWidget *>(m_pNotifyWidget);

				if (p != NULL)
					{
					p->onScroll(bar, action, m_scrollInfo.position());
					done = true;
					}
				}

			if (!done)
				{
				FXGroup	*p=dynamic_cast<FXGroup *>(m_pNotifyWidget);

				if (p != NULL)
					{
					p->onScroll(bar, action, m_scrollInfo.position());
					done = true;
					}
				}

			if (!done)
				{
				FXWnd	*p=dynamic_cast<FXWnd *>(m_pNotifyWidget);

				if (p != NULL)
					{
					p->onScroll(bar, action, m_scrollInfo.position());
					done = true;
					}
				}
			}
		}