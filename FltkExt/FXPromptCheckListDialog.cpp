#include "FXPromptCheckListDialog.h"
#include "FXStaticText.h"
#include "fx_draw.h"

#include <swift/sw_functions.h>

#define LINEHEIGHT	24

FXPromptCheckListDialog::FXPromptCheckListDialog(const SWString &title, const SWString &prompt, SWArray<FXCheckListEntry> &values, Fl_Widget *pParent, int W, int H) :
	FXDialog(title, W, H, pParent),
	m_list(values)
		{
		FXSize	sz=fx_getTextExtent(prompt, W-20);

		addStaticText(FX_IDSTATIC, prompt, 10, 10, sz.cx, sz.cy);
		
		FXCheckList	*pBrowser=new FXCheckList(10, 10 + sz.cy+10, w()-20, h()-sz.cy-40-LINEHEIGHT, "#000");

		addItem(100, pBrowser);
		addButton(FX_IDOK, "OK", 1, w()-200, h()-LINEHEIGHT-10, 90, LINEHEIGHT);
		addButton(FX_IDCANCEL, "Cancel", 0, w()-100, h()-LINEHEIGHT-10, 90, LINEHEIGHT);

		setItemFocus(100);

		for (int i=0;i<(int)values.size();i++)
			{
			pBrowser->addItem(values.get(i));
			}

		pBrowser->layoutChildren();
		pBrowser->invalidate();
		}


FXPromptCheckListDialog::~FXPromptCheckListDialog()
		{
		}


void
FXPromptCheckListDialog::onOK()
		{
		FXCheckList	*pBrowser=(FXCheckList *)getItem(100);
		int			nrows=pBrowser->getItemCount();

		for (int row=0;row<nrows;row++)
			{
			pBrowser->getItemStateAt(row, m_list[row].state);
			}
		
		FXDialog::onOK();
		}
