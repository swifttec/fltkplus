#ifndef __FltkExt_FXStdButton_h__
#define __FltkExt_FXStdButton_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>

#include <FL/Fl_Button.H>


class FLTKEXT_DLL_EXPORT FXStdButton : public Fl_Button
		{
public:
		FXStdButton(const SWString &text, bool defbtn, int x=0, int y=0, int w=100, int h=100, const FXColor &textcolor=FXColor(0, 0, 0));
		virtual ~FXStdButton();

		/// Set the font for the button text
		void		setFont(const FXFont &font);

		/// Add a shortkey this button responds to
		void		addShortcutKey(int v);

protected:
		virtual int	handle(int eventcode);

		bool		handleShortcutKey(int v);

protected:
		SWString		m_text;
		bool			m_defaultButton;
		SWIntegerArray	m_shortcutKeys;
		};

#endif // __FltkExt_FXStdButton_h__
