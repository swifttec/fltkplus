#include "FXCalendarPicker.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <swift/SWLocale.h>
#include <swift/SWTokenizer.h>

#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXStaticText.h>

enum
	{
	ArrowNone=0,
	ArrowUp,
	ArrowDown,
	ArrowLeft,
	ArrowRight
	};


#define COLWIDTH	32
#define ROWHEIGHT	20
#define TITLEHEIGHT	12

FXCalendarPicker::FXCalendarPicker(int X, int Y) :
	FXWidget(X, Y, COLWIDTH * 7, TITLEHEIGHT + (ROWHEIGHT * 9))
		{
		SWDate	dt;

		dt.update();
		init(dt);
		}


void
FXCalendarPicker::init(const SWDate &dt)
		{
		m_selectedDate = dt;

		m_fTitle = FXFont("Arial", 12, FXFont::Bold);
		m_fDays = FXFont("Arial", 11, FXFont::Bold);
		m_fDigits = FXFont("Arial", 11, FXFont::Normal);

		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, "#444", 1, 0);

		m_day = dt.day();
		m_month = dt.month();
		m_year = dt.year();

		m_selectedBgColor = "#008";
		m_selectedFgColor = "#fff";

		m_btnColor = "#ccc";
		m_arrowColor = "#000";
		}



void
FXCalendarPicker::onDraw()
		{
		FXRect	cr, dr;

		getClientRect(cr);

		dr = cr;
		dr.bottom = dr.top + TITLEHEIGHT - 1;
		fx_draw_fillRect(dr, "#08f");
		fx_draw_text("Click on date to select", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", TITLEHEIGHT-2, FXFont::Normal), "#fff");

		dr.left = dr.right - TITLEHEIGHT - 1;
		fx_draw_fillRect(dr, "#f00");
		fx_draw_text("X", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", TITLEHEIGHT, FXFont::Bold), "#fff");

		m_rLeftArrow.left = cr.left + 4;
		m_rLeftArrow.top = cr.top + 4 + TITLEHEIGHT;
		m_rLeftArrow.right = m_rLeftArrow.left + ROWHEIGHT - 1;
		m_rLeftArrow.bottom = m_rLeftArrow.top + ROWHEIGHT - 1;

		m_rRightArrow.left = cr.right - 4 - ROWHEIGHT;
		m_rRightArrow.top = cr.top + 4 + TITLEHEIGHT;
		m_rRightArrow.right = m_rRightArrow.left + ROWHEIGHT - 1;
		m_rRightArrow.bottom = m_rRightArrow.top + ROWHEIGHT - 1;

		drawButton(m_rLeftArrow, ArrowLeft, false);
		drawButton(m_rRightArrow, ArrowRight, false);

		SWString	month, year;
		SWLocale	lc=SWLocale::getCurrentLocale();

		month = lc.monthName(m_month-1, false);
		year = SWString::valueOf(m_year);

		fx_select(m_fTitle);

		FXSize	szSpace=fx_getTextExtent(" ");
		FXSize	szMonth=fx_getTextExtent(month);
		FXSize	szYear=fx_getTextExtent(year);

		m_rMonth.top = cr.top + 4 + TITLEHEIGHT;
		m_rMonth.bottom = m_rMonth.top + ROWHEIGHT - 1;
		m_rMonth.left = cr.center().x - ((szSpace.cx + szMonth.cx + szYear.cx) / 2);
		m_rMonth.right = m_rMonth.left + szMonth.cx - 1;

		m_rYear.top = cr.top + 4 + TITLEHEIGHT;
		m_rYear.bottom = m_rYear.top + ROWHEIGHT - 1;
		m_rYear.left = m_rMonth.right + szSpace.cx;
		m_rYear.right = m_rYear.left + szYear.cx - 1;

		fx_draw_text(month, m_rMonth, FX_ALIGN_MIDDLE_CENTRE, "#000");
		fx_draw_text(year, m_rYear, FX_ALIGN_MIDDLE_CENTRE, "#000");

		// Draw the grid
		SWString		s;
		int			ypos=TITLEHEIGHT+ROWHEIGHT;
		SWDate		dt(1, m_month, m_year);

		while (dt.dayOfWeek() > 0)
			{
			dt--;
			}

		for (int row=0;row<7;row++)
			{
			int		xpos=0;

			if (row == 0) fx_select(m_fDays);
			else fx_select(m_fDigits);

			for (int i=0;i<7;i++)
				{
				FXRect	dr;

				dr.left = xpos;
				dr.right = dr.left + COLWIDTH - 1;
				dr.top = ypos;
				dr.bottom = dr.top + ROWHEIGHT - 1;

				if (row == 0)
					{
					s = lc.dayName(i, true);
					fx_draw_text(s, dr, FX_ALIGN_MIDDLE_CENTRE);
					}
				else
					{
					s = SWString::valueOf(dt.day());
					fx_draw_text(s, dr, FX_ALIGN_MIDDLE_CENTRE, (dt.month() == m_month)?"#000":"#ccc");
					dt++;
					}

				xpos += COLWIDTH;
				}

			ypos += ROWHEIGHT;
			}
		}


int
FXCalendarPicker::onLeftMouseButton(int action, const FXPoint &pt)
		{
		if (action == FX_BUTTON_DOWN || action == FX_BUTTON_DOUBLE_CLICK)
			{
			FXRect	dr;

			getClientRect(dr);
			dr.bottom = dr.top + TITLEHEIGHT - 1;
			dr.left = dr.right - TITLEHEIGHT - 1;

			if (dr.contains(pt))
				{
				do_callback();
				}
			else if (m_rLeftArrow.contains(pt))
				{
				if (--m_month < 1)
					{
					m_year--;
					m_month = 12;
					}
				}
			else if (m_rRightArrow.contains(pt))
				{
				if (++m_month > 12)
					{
					m_year++;
					m_month = 1;
					}
				}
			else
				{
				int			ypos=TITLEHEIGHT + (ROWHEIGHT*2);
				SWDate		dt(1, m_month, m_year);

				while (dt.dayOfWeek() > 0)
					{
					dt--;
					}

				for (int row=1;row<7;row++)
					{
					int		xpos=0;

					for (int i=0;i<7;i++)
						{
						FXRect	dr;

						dr.left = xpos;
						dr.right = dr.left + COLWIDTH - 1;
						dr.top = ypos;
						dr.bottom = dr.top + ROWHEIGHT - 1;

						if (dr.contains(pt))
							{
							m_selectedDate = dt;
							do_callback();
							break;
							}

						dt++;
						xpos += COLWIDTH;
						}

					ypos += ROWHEIGHT;
					}
				}

			invalidate();
			fx_widget_getWindow(this)->redraw();
			}

		return 1;
		}



void
FXCalendarPicker::drawButton(const FXRect &dr, int style, bool selected)
		{
		fx_draw_fillRect(dr, m_btnColor);

		m_btnColor.toShade(selected?0.5:1.5).select();
		fl_line(dr.left, dr.top+dr.height()-1, dr.left, dr.top, dr.left+dr.width()-1, dr.top);
		m_btnColor.toShade(selected?1.5:0.5).select();
		fl_line(dr.left, dr.top+dr.height()-1, dr.left+dr.width()-1, dr.top+dr.height()-1, dr.left+dr.width()-1, dr.top);

		m_arrowColor.select();

		if (style != ArrowNone)
			{
			double	dx=dr.left+1, dy=dr.top+1, dw=dr.width()-2, dh=dr.height()-2;
			double	h1=0.35, h2=0.65;
			double	v1=0.5, v2=0.25, v3=0.75;

			fl_begin_polygon();

			switch (style)
				{
				case ArrowUp:
					fl_vertex(dx + dw * v1, dy + dh * h1); 
					fl_vertex(dx + dw * v2, dy + dh * h2); 
					fl_vertex(dx + dw * v3, dy + dh * h2); 
					fl_vertex(dx + dw * v1, dy + dh * h1); 
					break;

				case ArrowDown:
					fl_vertex(dx + dw * v1, dy + dh * h2); 
					fl_vertex(dx + dw * v2, dy + dh * h1); 
					fl_vertex(dx + dw * v3, dy + dh * h1); 
					fl_vertex(dx + dw * v1, dy + dh * h2); 
					break;

				case ArrowLeft:
					fl_vertex(dx + dw * h1, dy + dh * v1); 
					fl_vertex(dx + dw * h2, dy + dh * v2); 
					fl_vertex(dx + dw * h2, dy + dh * v3); 
					fl_vertex(dx + dw * h1, dy + dh * v1); 
					break;

				case ArrowRight:
					fl_vertex(dx + dw * h2, dy + dh * v1); 
					fl_vertex(dx + dw * h1, dy + dh * v2); 
					fl_vertex(dx + dw * h1, dy + dh * v3); 
					fl_vertex(dx + dw * h2, dy + dh * v1); 
					break;

				}

			fl_end_polygon();
			}
		}
