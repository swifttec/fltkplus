#ifndef __FltkExt_FXLayeredGroup_h__
#define __FltkExt_FXLayeredGroup_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXGroup.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

#include <FL/Fl_Group.H>

/**
*** A special group to manage the child widgets as panes in a window
**/
class FLTKEXT_DLL_EXPORT FXLayeredGroup : public FXGroup
		{
public:
		FXLayeredGroup(int x, int y, int w, int h);
		virtual ~FXLayeredGroup();

		/// Add the given widget as a layer
		void		addLayer(Fl_Widget *o);

		/// Add the given widget as a layer with ID
		void		addLayer(int id, Fl_Widget *o);

		/// Show the layer with the specified ID
		bool		showLayer(int id, bool hideIfNotFound=false);

		/// Show the layer the corresponds to the given widget
		bool		showLayer(Fl_Widget *o, bool hideIfNotFound=false);

		/// Hide the current layer
		bool		hideLayer();
		
		/// Return a pointer to the current layer widget	
		Fl_Widget *	getCurrentLayer()	{ return m_pCurrentLayer; }

public:
		virtual int			onKeyDown(int code, const SWString &text, const FXPoint &pt);		
		virtual int			onShortcut(int code, const FXPoint &pt);

protected: // Override Fl_Group methods we want to hide
		void	add(Fl_Widget &w);
		void	add(Fl_Widget* o);

		bool	activateLayer(Fl_Widget *o);

protected:
		SWArray<Fl_Widget *>	m_layers;
		Fl_Widget				*m_pCurrentLayer;
		};

#endif // __FltkExt_FXLayeredGroup_h__
