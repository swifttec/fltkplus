#include "FXDialog.h"

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Secret_Input.H>

#include <FltkExt/fx_functions.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXTextButton.h>
#include <FltkExt/FXStdButton.h>
#include <FltkExt/FXFont.h>

#include <swift/SWFileInfo.h>

void
FXDialog::dialogCallback(Fl_Widget *pWidget, void *pUserData)
		{
		FXDialog	*pSheet=(FXDialog *)pUserData;

		pSheet->onCancel();
		}


void
FXDialog::itemCallback(Fl_Widget *pWidget, void *p)
		{
		FXDialog	*pSheet=(FXDialog *)pWidget->parent();

		pSheet->onItemCallback(pWidget, p);
		}


FXDialog::FXDialog(const SWString &title, int W, int H, Fl_Widget *pOwner, const SWString &iconimage) :
	FXWnd(title, W, H, true, pOwner),
	m_pOwner(pOwner),
	m_modified(false),
	m_result(0),
	m_pIconImage(NULL),
	m_pBgImage(NULL)
		{
		m_font = FXFont::getDefaultFont(0);

		setBackgroundColor("#eee");
		setForegroundColor("#000");
		setBorder(0, FX_COLOR_NONE, 0, 0);

		// Set up a generic callback
		callback(dialogCallback, this);

		end();

		if (!iconimage.isEmpty())
			{
			m_pIconImage = new FXImage();

			if (!m_pIconImage->load(iconimage))
				{
				safe_delete(m_pIconImage);
				}
			}
		}


FXDialog::~FXDialog()
		{
		}





int
FXDialog::onShortcut(int code, const FXPoint &pt)
		{
		int		res=0;

		if (Fl::event_key() == FL_Escape)
			{
			onCancel();
			res = 1;
			}
		
		if (res == 0) res = FXWnd::onShortcut(code, pt);

		return res;
		}


int
FXDialog::onButtonClicked(Fl_Widget *pWidget)
		{
		return 0;
		}


void
FXDialog::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		int		handled=0;
		int		itemid=pWidget->id();

		if (Fl::event() == FL_RELEASE || Fl::event() == FL_KEYUP)
			{
			handled = onButtonClicked(pWidget);

			if (handled == 0)
				{
				if (itemid == FX_IDOK)
					{
					onOK();
					}
				else if (itemid == FX_IDCANCEL)
					{
					onCancel();
					}
				else if (pWidget == this)
					{
					onCancel();
					}
				}
			}
		else if (Fl::event() == FL_SHORTCUT)
			{
			if (itemid == FX_IDOK)
				{
				onOK();
				}
			else if (itemid == FX_IDCANCEL)
				{
				onCancel();
				}
			}
		}


void
FXDialog::updateData(bool save)
		{
		FXDataExchange	dx(this, save?FXDataExchange::Save:FXDataExchange::Load);

		doDataExchange(&dx);
		}


void
FXDialog::onOK()
		{
		endDialog(FX_IDOK);
		}


void
FXDialog::onCancel()
		{
		endDialog(FX_IDCANCEL);
		}


int
FXDialog::showDialog(bool modal)
		{
		onInitDialog();

		fx_widget_centerOver(this, m_pOwner);

		if (modal) set_modal();
		show();

		// Give the focus to the first child
		int		nchildren=children();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=child(i);

			if (pWidget->take_focus()) break;
			}

		if (modal)
			{
			while (shown())
				{
				Fl::wait();
				fx_invalidate_process();
				}
			set_non_modal();
			}

		return m_result;
		}


int
FXDialog::doModal()
		{
		return showDialog(true);
		}


void
FXDialog::doNonModal()
		{
		showDialog(false);
		}


void
FXDialog::endDialog(int r)
		{
		m_result = r;
		hide();	
		}


void
FXDialog::setModified()
		{
		m_modified = true;
		}


void
FXDialog::onInitDialog()
		{
		// By default just load the data
		updateData(false);
		}

void
FXDialog::addItem(int itemid, Fl_Widget *pItem)
		{
		fx_group_addItem(this, itemid, pItem);
		pItem->callback(itemCallback);
		pItem->when(FL_WHEN_CHANGED);
		}


void
FXDialog::setItemFocus(int itemid)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			pWidget->take_focus();
			}
		}


void
FXDialog::enableItem(int itemid, bool v)
		{
		Fl_Widget	*pWidget=getItem(itemid);

		if (pWidget != NULL)
			{
			if (v) pWidget->activate();
			else pWidget->deactivate();
			}
		}


void
FXDialog::doDataExchange(FXDataExchange *pDX)
		{
		// By default do nothing - should be overridden by any deriving class
		}


Fl_Widget *
FXDialog::addStaticText(ulong id, const SWString &text, int X, int Y, int W, int H)
		{
		FXStaticText	*pWidget;

		if (W == 0 || H == 0)
			{
			fx_select(m_font);

			FXSize		sz=fx_getTextExtent(text);
			
			if (W == 0) W = sz.width();
			if (H == 0) H = sz.height();
			}

		pWidget = new FXStaticText(X, Y, W, H, text, m_font, FX_ALIGN_MIDDLE_LEFT, getForegroundColor(), getBackgroundColor());

		addItem(id, pWidget);

		return pWidget;
		}


Fl_Widget *
FXDialog::addTextInput(ulong id, int X, int Y, int W, int H)
		{
		Fl_Input	*pWidget;

		if (W == 0 || H == 0)
			{
			fx_select(m_font);

			FXSize		sz=fx_getTextExtent("ABCD");
			
			if (W == 0) W = sz.width();
			if (H == 0) H = sz.height();
			}

		pWidget = new Fl_Input(X, Y, W, H);
		pWidget->textfont(m_font.fl_face());
		pWidget->textsize(m_font.height());
		pWidget->textcolor(getForegroundColor().to_fl_color());

		addItem(id, pWidget);

		return pWidget;
		}


Fl_Widget *
FXDialog::addPasswordInput(ulong id, int X, int Y, int W, int H)
		{
		Fl_Input	*pWidget;

		if (W == 0 || H == 0)
			{
			fx_select(m_font);

			FXSize		sz=fx_getTextExtent("ABCD");
			
			if (W == 0) W = sz.width();
			if (H == 0) H = sz.height();
			}

		pWidget = new Fl_Secret_Input(X, Y, W, H);
		pWidget->textfont(m_font.fl_face());
		pWidget->textsize(m_font.height());
		pWidget->textcolor(getForegroundColor().to_fl_color());

		addItem(id, pWidget);

		return pWidget;
		}


Fl_Widget *
FXDialog::addNumberInput(ulong id, int X, int Y, int W, int H)
		{
		Fl_Input	*pWidget;

		if (W == 0 || H == 0)
			{
			fx_select(m_font);

			FXSize		sz=fx_getTextExtent("ABCD");
			
			if (W == 0) W = sz.width();
			if (H == 0) H = sz.height();
			}

		pWidget = new Fl_Int_Input(X, Y, W, H);
		pWidget->textfont(m_font.fl_face());
		pWidget->textsize(m_font.height());
		pWidget->textcolor(getForegroundColor().to_fl_color());

		addItem(id, pWidget);

		return pWidget;
		}


Fl_Widget *
FXDialog::addButton(ulong id, const SWString &text, int type, int X, int Y, int W, int H)
		{
		Fl_Widget	*pWidget=NULL;

		if (W == 0 || H == 0)
			{
			fx_select(m_font);

			FXSize		sz=fx_getTextExtent(text);
			
			if (W == 0) W = sz.width() + (fx_getTextExtent(text).cy * 2);
			if (H == 0) H = sz.height() + 8;
			}

		FXTextButton	*pButton;

		pButton = new FXTextButton(text, m_font.toStyle(type==1?FXFont::Bold:FXFont::Normal), X, Y, W, H, getForegroundColor());
		pWidget = pButton;

		if (type == 1)
			{
			pButton->addShortcutKey(FL_Enter);
			pButton->addShortcutKey(FL_KP_Enter);
			}

		/*
		switch (type)
			{
			case 0:
			case 1:
				{
				FXStdButton	*pButton;

				pButton = new FXStdButton(text, (type == 1), X, Y, W, H, getForegroundColor());
				pButton->setFont(m_font.toStyle(type==1?FXFont::Bold:FXFont::Normal));
				pWidget = pButton;
				}
				break;

			default:
				{
				FXTextButton	*pButton;

				pButton = new FXTextButton(text, m_font.toStyle(type==1?FXFont::Bold:FXFont::Normal), X, Y, W, H, getForegroundColor());
				pWidget = pButton;
				}
				break;
			}
		*/

		if (pWidget != NULL) addItem(id, pWidget);

		return pWidget;
		}
