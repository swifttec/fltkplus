/*
**	FXStaticText.h	A generic dialog class
*/

#ifndef __FltkExt_FXStaticText_h__
#define __FltkExt_FXStaticText_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXFont.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXStaticText : public FXWidget
		{
public:
		FXStaticText(int W, int H, const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor=FXColor::NoColor);
		FXStaticText(int W, int H, const SWString &text, int align, const FXColor &fgcolor, const FXColor &bgcolor=FXColor::NoColor);

		FXStaticText(int X, int Y, int W, int H, const SWString &text, const FXFont &font, int align=FX_ALIGN_MIDDLE_LEFT, const FXColor &fgcolor="#000", const FXColor &bgcolor=FXColor::NoColor);
		FXStaticText(int X, int Y, int W, int H, const SWString &text, int align=FX_ALIGN_MIDDLE_LEFT, const FXColor &fgcolor="#000", const FXColor &bgcolor=FXColor::NoColor);

		FXStaticText(const SWString &text, const FXFont &font=FXFont::getDefaultFont(FXFont::StaticText), int align=FX_ALIGN_MIDDLE_LEFT, const FXColor &fgcolor="#000", const FXColor &bgcolor=FXColor::NoColor);

		virtual ~FXStaticText();

		void				setText(const SWString &v);
		void				setText(const SWString &v, Fl_Color c);
		void				setText(const SWString &v, const FXColor &c);
		const SWString &	getText() const					{ return m_text; }

		void				setTextColor(Fl_Color v);
		void				setTextColor(const FXColor &v);

		void				text(const SWString &v)			{ setText(v); }
		const SWString &	text() const					{ return getText(); }

		/// Set the text style for this text
		void				setTextStyle(const FXTextStyle &v);

		/// Set the font for this text
		void				setFont(const FXFont &font)			{ m_style.font = font; }

		/// Set the font for this text
		void				setTextAlign(int v)					{ m_style.flags = (m_style.flags & ~FX_ALIGN_MASK) | (v & FX_ALIGN_MASK); }

#ifdef FX_OUTLINE
		/// Set the text outline style
		void				setTextOutline(int width, const FXColor &color);
#endif // FX_OUTLINE

		/// Set the text shadow style
		void				setTextShadow(int xoff, int yoff, const FXColor &color);

		/// Recalculate the min/max sizes for this widget
		void				calcSizes(int width);

		void				padding(const FXPadding &v)		{ m_padding = v; }
		const FXPadding &	padding() const					{ return m_padding; }

protected:
		void			init(const SWString &text, const FXFont &font, int align, const FXColor &fgcolor, const FXColor &bgcolor);

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

public:
		static FXFont	m_defaultFont;
			
protected:
		SWString	m_text;				///< The actual text
		FXTextStyle	m_style;			///< The text style
		/*
		int			m_align;			///< The text alignment
		FXFont		m_font;				///< The font to use
		FXPadding	m_padding;			///< The padding for drawing text
		FXColor		m_outlineColor;		///< The outline color
		int			m_outlineWidth;		///< The outline width
		FXColor		m_shadowColor;		///< The shadow color
		int			m_shadowOffsetX;	///< The shadow X offset
		int			m_shadowOffsetY;	///< The shadow Y offset
		*/
		};


#endif // __FltkExt_FXStaticText_h__
