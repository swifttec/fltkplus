#include <FltkExt/fx_common_dialogs.h>
#include <FltkExt/FXColorDialog.h>

FLTKEXT_DLL_EXPORT int
fx_selectColor(FXColor &color, const SWString &title, Fl_Widget *pOwner)
		{
		FXColorDialog	dlg(color, title, pOwner);
		int				r;

		if ((r = dlg.doModal()) == FX_IDOK)
			{
			color = dlg.getColor();
			}

		return r;
		}
