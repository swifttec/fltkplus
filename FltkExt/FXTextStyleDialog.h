/*
**	FXTextStyleDialog.h	A dialog to prompt for a string
*/

#ifndef __FltkExt_FXTextStyleDialog_h__
#define __FltkExt_FXTextStyleDialog_h__

#include <FltkExt/FXDialog.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFontFamily.h>
#include <FltkExt/FXTextStyle.h>


/**
*** A popup dialog window for entering a number
**/
class FLTKEXT_DLL_EXPORT FXTextStyleDialog : public FXDialog
		{
public:
		enum Flags
			{
			FlagFontName	= 0x001,
			FlagFontSize	= 0x002,
			FlagFontStyle	= 0x004,
			FlagTextColor	= 0x008,
			FlagBGColor		= 0x010,
			FlagHAlign		= 0x020,
			FlagVAlign		= 0x040,
			FlagShadow		= 0x080,
#ifdef FX_OUTLINE
			FlagOutline		= 0x100
#endif // FX_OUTLINE
			};
public:
		FXTextStyleDialog(const SWString &title, const FXTextStyle &value, int flags=0, Fl_Widget *pParent=NULL);
		virtual ~FXTextStyleDialog();

		virtual void		onOK();

		const FXTextStyle &	value() const	{ return m_value; }

protected:
		void			updateSample();

		/// Called when a item issues a callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

protected:
		SWString				m_title;
		FXTextStyle				m_value;
		SWArray<FXFontFamily>	m_fontlist;
		int						m_flags;
		};


#endif // __FltkExt_FXTextStyleDialog_h__
