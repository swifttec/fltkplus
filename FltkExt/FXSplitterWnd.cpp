/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the implementation of the FXSplitterWnd class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#include "FXSplitterWnd.h"
#include "FXSplitterPane.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

#include <swift/SWTokenizer.h>

FXSplitterWnd::FXSplitterWnd(int X, int Y, int W, int H, bool vertical) :
	Fl_Group(X, Y, W, H),
	m_flags(0),
	m_barColor(FXColor(150, 150, 150)),
	m_cursor(-1),
	m_bgColor(RGB(0, 0, 0))
		{
		setFlag(FLAG_VERTICAL, vertical);
		}


void
FXSplitterWnd::draw()
		{
		FXRect		rc;	

		getClientRect(rc);
		
		fl_rectf(rc.left, rc.top, rc.width(), rc.height(), m_bgColor.to_fl_color());

		for (int i=0;i<(int)m_bars.size();i++)
			{
			FXSplitterBar	&bar=m_bars[i];

			bar.draw();
			}

		Fl_Group::draw();

		/*
		CBrush	fgBrush(RGB(255, 255, 255));
		CBrush	brBrush(m_barColor);
		
		fx_draw_fillRect(&rc, &fgBrush);
		
		for (int i=1;i<(int)m_panes.size();i++)
			{
			CRect	dr;
			
			dr = rc;
			if (isVerticalSplit())
				{
				dr.bottom = m_panes[i].offset - m_barPadding;
				dr.top = dr.bottom - m_barWidth;
				}
			else
				{
				dr.right = m_panes[i].offset - m_barPadding;
				dr.left = dr.right - m_barWidth;
				}
			fx_draw_fillRect(&dr, &brBrush);
			}
		*/
		}

void
FXSplitterWnd::resize(int X, int Y, int W, int H)
		{
		// We are handling the resizing, so disable it whilst the underlying code works
		Fl_Widget	*tmp=resizable();

		resizable(NULL);
		Fl_Group::resize(X, Y, W, H);
		resizable(tmp);

		int		cx=W, cy=H;

		if (cx && cy)
			{
			FXSize	minsize(0, 0);
			// int		barsize=(m_barPadding * 2) + m_barWidth;
			int		nshrinkable=0;
			int		ngrowable=0;
			int		panesize=0;
			
			// Calculate the window minimum size
			calcMinSize(minsize, panesize, ngrowable, nshrinkable);
				
			if (cx < minsize.cx) cx = minsize.cx;
			if (cy < minsize.cy) cy = minsize.cy;
			
			// setMinSize(minsize);

			// TRACE("FXSplitterWnd 0x%08x cx=%d cy=%d size=%dx%d\n", this, cx, cy, size().cx, size().cy);
			
			int		slack=0;

			if (isVerticalSplit())
				{
				slack = cy - panesize;
				}
			else
				{
				slack = cx - panesize;
				}

			if (slack > 0 && ngrowable > 0)
				{
				// We are growing
				int		adj=slack / ngrowable;
				int		npanes=ngrowable;

				if (adj == 0) adj = 1;

				for (int i=0;slack&&npanes&&i<(int)m_panes.size();i++)
					{
					FXSplitterPane	&p=m_panes[i];

					if (!p.maxSize() || p.currentSize() < p.maxSize())
						{
						// Growable pane
						if (npanes == 1) adj = slack;
						adjustSize(i, adj, -1);
						slack -= adj;
						npanes--;
						}
					}
				}
			else if (slack < 0 && nshrinkable > 0)
				{
				// We are shrinking
				while (slack && nshrinkable)
					{
					// TRACE("LOOP - slack = %d, nshrinkable = %d\n", slack, nshrinkable);

					int		adj=slack / nshrinkable;
					int		npanes=nshrinkable;

					if (adj == 0) adj = -1;

					for (int i=0;slack&&npanes&&i<(int)m_panes.size();i++)
						{
						FXSplitterPane	&p=m_panes[i];
						int				minsize=p.minSize();
						int				currsize=p.currentSize();

						if (currsize > minsize)
							{
							// Shrinkable pane
							if (npanes == 1) adj = slack;
							
							int		amt=adj;
							int		newsize=currsize+adj;

							if (newsize < minsize) newsize = minsize;

							// if ((currsize - amt) < minsize) amt = currsize - minsize;

							// TRACE("  pane %d - currsize=%d, minsize=%d, newsize=%d\n", i, currsize, minsize, newsize);

							if (newsize != currsize)
								{
								p.currentSize(newsize); // adjustSize(i, amt, -1);
								slack += currsize-newsize;
								npanes--;

								if (currsize == minsize) nshrinkable--;
								}
							}
						}
					}

				// TRACE("END - slack = %d, nshrinkable = %d\n", slack, nshrinkable);
				}
			else
				{
				int		x=0;
				}
			
			movePanes();
			
			TRACE("set size %d x %d\n", cx, cy);
//			FXManagerWnd::onSize(type, cx, cy);
			}
		}
/*

void
FXSplitterWnd::resize(int X, int Y, int W, int H)
		{
		int	nchildren=children();
 		int	*p = sizes(); // save initial sizes and positions
		int	minsize=0, maxsize=0, unlimited=0;

		for (int i=0;i<nchildren;i++)
			{
			Sizes		&cs=getChildSizes(i);

			minsize += cs.minsize;
			maxsize += cs.maxsize;
			if (cs.maxsize == 0) unlimited++;
			}

		if (m_vertical)
			{
			if (maxsize > 0 && H > maxsize && !unlimited) H = maxsize;
			else if (H < minsize) H = minsize;
			}
		else
			{
			if (maxsize > 0 && W > maxsize && !unlimited) W = maxsize;
			else if (W < minsize) W = minsize;
			}

		Fl_Widget::resize(X,Y,W,H); // make new xywh values visible for children



		if (nchildren > 0)
			{
			Fl_Widget	*const *a = array();
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalExtraSpace=0;

			a = array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				if (m_vertical) totalChildSpace += o->h();
				else totalChildSpace += o->w();
				}

			if (m_vertical) ds = H-totalChildSpace;
			else ds = W-totalChildSpace;

			int		canchange=0;
			SWFlags	changeflags;

			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				a = array();
				for (int i=0;i<nchildren;i++)
					{
					Fl_Widget	*o=*a++;
					int			width=o->w(), height=o->h();

					if (pass == 0 || changeflags.getBit(i))
					{
					Sizes		&cs=getChildSizes(i);

					if (ds != 0)
						{
						if (pass == 0)
							{
							cds = ds / (nchildren-i);
							}
						else
							{
							cds = ds / canchange;
							}

						if (cds == 0)
							{
							if (ds < 0) cds = -1;
							else cds = 1;
							}
						}

					if (m_vertical)
						{
						// Fixed width (same as parent)
						width = W;

						// calculate new height
						int		old=height;

						height += cds;
						if (height < cs.minsize) height = cs.minsize;
						if (cs.maxsize != 0 && height > cs.maxsize) height = cs.maxsize;
						else if (pass == 0 && (cs.maxsize == 0 || height < cs.maxsize))
							{
							canchange++;
							changeflags.setBit(i);
							}
						ds -= height-old;
						}
					else
						{
						// Fixed height (same as parent)
						height = H;

						// Variable width
						int		old=width;

						width += cds;
						if (width < cs.minsize) width = cs.minsize;
						if (cs.maxsize != 0 && width > cs.maxsize) width = cs.maxsize;
						else if (pass == 0 && (cs.maxsize == 0 || width < cs.maxsize))
							{
							canchange++;
							changeflags.setBit(i);
							}
						ds -= width-old;
						}
					
					if (pass == 1) canchange--;
					}

					o->resize(X+xpos, Y+ypos, width, height);

					if (m_vertical) ypos += o->h();
					else xpos += o->w();
					}
				}
			}
		}
*/




int
FXSplitterWnd::getPaneCount() const
		{
		return (int)m_panes.size();
		}


void
FXSplitterWnd::clearPanes()
		{
		clear();
		m_panes.clear();
		m_bars.clear();
		}



FXSplitterPane *
FXSplitterWnd::findPane(Fl_Widget *pWnd)
		{
		FXSplitterPane *pPane=NULL;

		for (int i=0;i<(int)m_panes.size();i++)
			{
			if (m_panes[i].getWidget() == pWnd)
				{
				pPane = &m_panes[i];
				break;
				}
			}

		return pPane;
		}


int
FXSplitterWnd::getPaneIndex(Fl_Widget *pWnd)
		{
		int		res=-1;

		for (int i=0;i<(int)m_panes.size();i++)
			{
			if (m_panes[i].getWidget() == pWnd)
				{
				res = i;
				break;
				}
			}

		return res;
		}


void
FXSplitterWnd::saveSizesToRegistry(SWRegistryHive reghive, const SWString &regkey, const SWString &param)
		{
		SWString	s;
		
		for (int i=0;i<(int)m_panes.size();i++)
			{
			if (!s.isEmpty()) s += ",";
			s += SWString::valueOf(m_panes[i].preferredSize());
			}

		sw_registry_setString(reghive, regkey, param, s);
		}


void
FXSplitterWnd::loadSizesFromRegistry(SWRegistryHive reghive, const SWString &regkey, const SWString &param)
		{
		SWTokenizer	tok(sw_registry_getString(reghive, regkey, param), ",");
		int			pos=0;

		while (tok.hasMoreTokens() && pos < (int)m_panes.size())
			{
			int		v=tok.nextToken().toInt32(10);

			setPaneSize(pos++, v);
			}
		}



FXSize
FXSplitterWnd::getMinSize() const
		{
		FXSize	minsize(0, 0);
		int		nshrinkable=0;
		int		ngrowable=0;
		int		panesize=0;

		calcMinSize(minsize, panesize, ngrowable, nshrinkable);

		return minsize;
		}


void
FXSplitterWnd::calcMinSize(FXSize &minsize, int &panesize, int &ngrowable, int &nshrinkable) const
		{
		minsize = FXSize(0, 0);

		panesize = 0;
		nshrinkable = 0;
		ngrowable = 0;
			
		// Calculate the window minimum size
		for (int i=0;i<(int)m_panes.size();i++)
			{
			FXSplitterPane	&p=m_panes.get(i);
			FXSize			sz=p.minWidgetSize();

			if (isVerticalSplit())
				{
				minsize.cx = sw_math_max(sz.cx, minsize.cx);
				minsize.cy += sz.cy;
				}
			else
				{
				minsize.cx += sz.cx;
				minsize.cy = sw_math_max(sz.cy, minsize.cy);
				}
				
			panesize += p.currentSize();
				
			if (!p.maxSize() || p.currentSize() < p.maxSize()) ngrowable++;
			if (p.currentSize() > p.minSize()) nshrinkable++;

			if (i)
				{
				int		barsize=m_bars.get(i-1).size();

				panesize += barsize;
						
				if (isVerticalSplit())
					{
					minsize.cy += barsize;
					}
				else
					{
					minsize.cx += barsize;
					}
				}
			}
		}


#ifdef NEVER


void
FXSplitterWnd::onLayoutChildren()
		{
		// Do nothing
		}

#endif



void
FXSplitterWnd::setPane(int idx, Fl_Widget *pWnd)
		{
		int		npanes=getPaneCount();

		if (idx >= 0 && idx < npanes)
			{
			m_panes[idx].setWidget(pWnd);

			remove(idx * 2);
			insert(*pWnd, idx * 2);
			adjustPanes();
			}
		}


void
FXSplitterWnd::addPane(Fl_Widget *pWnd, int prefSize, int barWidth, int barPadding, const FXColor &fgBarColor, const FXColor &bgBarColor, bool fixedSize) // , int minSize, int maxSize, bool useSavedPrefSize)
		{
		add(pWnd);

		FXRect	rect;
//		int		barsize=(m_barPadding * 2) + m_barWidth;
		int		barspace=0;
		int		rsize=0;
		int		minSize=0, maxSize=0;
		bool	fixed=false;
		
		for (int i=0;i<(int)m_bars.size();i++)
			{
			barspace += m_bars[i].size();
			}
			
		getClientRect(rect);
		rsize = isVerticalSplit()?rect.Height():rect.Width();
		if (!fixedSize)
			{
			if (isVerticalSplit())
				{
				minSize = fx_getMinSize(pWnd).cy;
				maxSize = fx_getMaxSize(pWnd).cy;
				}
			else
				{
				minSize = fx_getMinSize(pWnd).cx;
				maxSize = fx_getMaxSize(pWnd).cx;
				}

			fixed = (minSize == maxSize && maxSize != 0);
			}
		else if (prefSize)
			{
			fixed = true;
			minSize = maxSize = prefSize;
			}


/*
		if (useSavedPrefSize)
			{
			SWString		regkey = "windows/";
			
			regkey += m_regkey;
			
			SWString	s; //=theAppConfig.getRegistryString(regkey, "split");
			SWTokenizer	tok(s, ",");
			int			idx=(int)m_panes.size();
			
			if (tok.getTokenCount() > idx)
				{
				prefSize = tok.getToken(idx).toInt32(10);
				}
			}
*/
			
		FXSplitterPane	pane(this);
		
		pane.vertical((m_flags & FLAG_VERTICAL) != 0);
		pane.setWidget(pWnd);
		pane.preferredSize(prefSize);
		pane.minSize(minSize);
		pane.maxSize(maxSize);
		pane.fixed(fixed); // (fixedSize && prefSize != 0);
		if (pane.minSize() < 8) pane.minSize(8);
		
		if (pane.fixed())
			{
			pane.currentSize(prefSize);
			pane.maxSize(prefSize);
			pane.minSize(prefSize);
			}
		
		int		available=rsize;	// The space available based on prefsize
		
		for (int i=0;i<(int)m_panes.size();i++)
			{
			FXSplitterPane	&p=m_panes[i];
			
			available -= p.preferredSize();
			
			if (i)
				{
				available -= m_bars[i-1].size();
				}
			}
		
		int		wanted;
		
		wanted = pane.preferredSize();
		if (wanted < pane.minSize()) wanted = pane.minSize();
		if (m_panes.size())
			{
			wanted += (barPadding * 2) + barWidth;
			}
		
		int		pos=0;
		bool	usepref=(available >= wanted);

		// Adjust sizes based on preferred size
		available = rsize;
		for (int i=0;i<(int)m_panes.size();i++)
			{
			FXSplitterPane	&p=m_panes[i];

			if (i > 0)
				{
				FXSplitterBar	&bar=m_bars[i-1];
				FXRect			&br=bar.rect;

				if (isVerticalSplit())
					{
					br.left = rect.left;
					br.right = rect.right;
					br.top = pos;
					br.bottom = pos + bar.size();
					}
				else
					{
					br.left = pos;
					br.right = pos + bar.size();
					br.top = rect.top;
					br.bottom = rect.bottom;
					}
				
				pos += bar.size();
				available -= bar.size();
				}

			if (usepref)
				{
				p.currentSize(wanted);
				}
			else
				{
				p.currentSize(p.minSize());
				}
				
			p.offset(pos);
			pos += p.currentSize();
			available -= p.currentSize();
			
			if (isVerticalSplit())
				{
				p.getWidget()->resize(x(), y()+p.offset(), rect.Width(), p.currentSize());
				}
			else
				{
				p.getWidget()->resize(x()+p.offset(), y(), p.currentSize(), rect.Height());
				}
			}
		
		pane.currentSize(available);
		if (pane.currentSize() < pane.minSize())
			{
			pane.currentSize(pane.minSize());
			}
		
		pane.offset(pos);
		m_panes.add(pane);

		if (m_panes.size() > 1)
			{
			FXSplitterBar	&bar=m_bars[(int)m_panes.size()-2];

			bar.vertical = isVerticalSplit();
			bar.padding = barPadding;
			bar.width = barWidth;
			bar.bgColor = bgBarColor;
			bar.fgColor = fgBarColor;
			}
		

		if (isVerticalSplit())
			{
			pWnd->resize(x(), y()+pos, rect.Width(), pane.currentSize());
			}
		else
			{
			pWnd->resize(x()+pos, y(), pane.currentSize(), rect.Height());
			}

		movePanes();
		}



int
FXSplitterWnd::barIndex(const FXPoint &pt)
		{
		int		res=-1;

		for (int i=0;i<(int)m_bars.size();i++)
			{
			if (m_bars[i].rect.contains(pt))
				{
				res = i;
				break;
				}
			}
		
		return res;
		}


/*
bool
FXSplitterWnd::onSetCursor(UINT nHitTest, UINT message)
		{
		HCURSOR			cursor=NULL;
		LPTSTR			id=IDC_ARROW;
		CPoint			pt;
		bool			res=false;
		
		GetCursorPos(&pt);
		ScreenToClient(&pt);

		if (dragging())
			{
			id = IDC_HAND;
			res = true;
			}
		else if (barIndex(pt) >= 0)
			{
			id = isVerticalSplit()?IDC_SIZENS:IDC_SIZEWE;
			res = true;
			}

		if (res)
			{
			cursor = ::AfxGetApp()->LoadStandardCursor(id);
			SetCursor(cursor);
			}
		else
			{
			res = FXManagerWnd::onSetCursor(nHitTest, message);
			}
		
		return res;
		}

*/



void
FXSplitterWnd::setPaneSize(int idx, int size)
		{
		if (idx >= 0 && idx < (int)m_panes.size())
			{
			int		off=size-m_panes[idx].currentSize();

			if (canAdjustSize(idx, off, -1) && canAdjustSize(idx+1, -off, 1))
				{
				adjustSize(idx, off, -1);
				adjustSize(idx+1, -off, 1);
				movePanes();
					
				// Save the settings as preferences
				for (int i=0;i<(int)m_panes.size();i++)
					{
					FXSplitterPane	&p=m_panes[i];
						
					p.preferredSize(p.currentSize());
					}
				}
			}
		}


void
FXSplitterWnd::setPaneSizes(int idx, int prefsize, int minsize, int maxsize)
		{
		if (idx >= 0 && idx < (int)m_panes.size())
			{
			if (minsize > maxsize)
				{
				int		v=minsize;

				minsize = maxsize;
				maxsize = v;
				}
			if (prefsize < minsize) prefsize = minsize;
			if (prefsize > maxsize) prefsize = maxsize;

			m_panes[idx].preferredSize(prefsize);
			m_panes[idx].minSize(minsize);
			m_panes[idx].maxSize(maxsize);
			}
		}


void
FXSplitterWnd::setPaneSize(int idx, int size, bool fixed)
		{
		TRACE("FXSplitterWnd::setPaneSize(%d, %d, %d)\n", idx, size, fixed);

		if (idx >= 0 && idx < (int)m_panes.size())
			{
			bool	wasfixed=m_panes[idx].fixed();

			m_panes[idx].fixed(false);

			int		off=size-m_panes[idx].currentSize();
			bool	adjcurr=canAdjustSize(idx, off, -1);
			bool	adjnext=canAdjustSize(idx+1, -off, 1);
			bool	adjprev=canAdjustSize(idx-1, -off, -1);

			if (adjcurr && (adjprev || adjnext))
				{
				m_panes[idx].fixed(false);
				adjustSize(idx, off, -1);
				if (adjnext) adjustSize(idx+1, -off, 1);
				else adjustSize(idx-1, -off, -1);
//				m_panes[idx].currSize = size;
				m_panes[idx].fixed(fixed);
				movePanes();
					
				// Save the settings as preferences
				for (int i=0;i<(int)m_panes.size();i++)
					{
					FXSplitterPane	&p=m_panes[i];
						
					p.preferredSize(p.currentSize());
					}
				}
			else
				{
				m_panes[idx].fixed(wasfixed);
				}
			}
		}


bool
FXSplitterWnd::canAdjustSize(int idx, int off, int step)
		{
		bool	res=false;
		
		if (idx >= 0 && idx < (int)m_panes.size())
			{
			FXSplitterPane	&p=m_panes[idx];
			int				newsize=p.currentSize() + off;
			int				contentsize=0;
			
			/*
			Fl_Widget		*pChildWnd=p.getWidget();
			
			if (pChildWnd != NULL)
				{
				FXSize	sz;
				
				sz = pChildWnd->getContentsSize();
				if (this->isVerticalSplit()) contentsize = sz.cy;
				else contentsize = sz.cx;
				}
			*/

			if (p.fixed())
				{
				// Can't adjust this one, but perhaps the next
				res = canAdjustSize(idx+step, off, step);
				}
			else if (newsize >= contentsize && newsize >= p.minSize())
				{
				// Yes we can
				res = true;
				}
			else if (newsize >= p.preferredSize() && newsize >= p.minSize())
				{
				// Yes we can
				res = true;
				}
			else
				{
				// No we can't based on prefsize only
				res = canAdjustSize(idx+step, newsize-p.preferredSize(), step);
				if (!res)
					{
					res = (newsize >= p.minSize());
					}
				if (!res)
					{
					res = canAdjustSize(idx+step, newsize-p.minSize(), step);
					}
				}
			}
		
		return res;
		}


bool
FXSplitterWnd::adjustSize(int idx, int off, int step)
		{
		bool	res=false;

		TRACE("  adjustSize(%d, %d, %d)\n", idx, off, step);
		
		if (!off)
			{
			res = true;
			}
		else if (idx >= 0 && idx < (int)m_panes.size())
			{
			FXSplitterPane	&p=m_panes[idx];
			int				newsize=p.currentSize() + off;
			int				contentsize=0;
			
			/*
			FXChildWnd		*pChildWnd=p.getWnd();

			TRACE("    currsize=%d, newsize=%d, fixed=%d\n", p.currentSize(), newsize, p.fixed());
			
			if (pChildWnd != NULL)
				{
				CSize	sz;
				
				sz = pChildWnd->getContentsSize();
				if (this->isVerticalSplit()) contentsize = sz.cy;
				else contentsize = sz.cx;
				}
			*/

			if (p.fixed())
				{
				// Can't adjust this one, but perhaps the next
				res = adjustSize(idx+step, off, step);
				}
			else if (newsize >= contentsize && newsize >= p.minSize())
				{
				// Yes we can
				res = true;
				p.currentSize(p.currentSize() + off);
				TRACE("      ok(1)");
				}
			else if (newsize >= p.preferredSize() && newsize >= p.minSize())
				{
				// Yes we can
				res = true;
				p.currentSize(p.currentSize() + off);
				TRACE("      ok(2)");
				}
			else if (p.preferredSize())
				{
				// No we can't based on prefsize only
				res = canAdjustSize(idx+step, newsize-p.preferredSize(), step);
				if (res)
					{
					// Set to pref size and ajust down the line
					p.currentSize(p.preferredSize());
					res = adjustSize(idx+step, newsize-p.preferredSize(), step);
					}
				else
					{
					if (newsize >= p.minSize())
						{
						// Yes we can
						res = true;
						p.currentSize(p.currentSize() + off);
						TRACE("      ok(3)");
						}
					else
						{
						p.currentSize(p.minSize());
						res = adjustSize(idx+step, newsize-p.minSize(), step);
						}
					}
				}
			}
		
		TRACE("  adjustSize(%d, %d, %d) = %d\n", idx, off, step, res);

		return res;
		}



void
FXSplitterWnd::getClientRect(FXRect &rect)
		{
		rect.left = x();
		rect.right = x() + w() - 1;
		rect.top = y();
		rect.bottom = y() + h() - 1;
		}


void
FXSplitterWnd::movePanes()
		{
		FXRect	rect;
		int		pos=0;
		// int		barsize=(m_barPadding * 2) + m_barWidth;

		getClientRect(rect);

		// Adjust sizes based on preferred size
		for (int i=0;i<(int)m_panes.size();i++)
			{
			FXSplitterPane	&p=m_panes[i];

			if (i > 0)
				{
				FXSplitterBar	&bar=m_bars[i-1];
				FXRect			&br=bar.rect;

				if (isVerticalSplit())
					{
					br.left = rect.left;
					br.right = rect.right;
					br.top = rect.top + pos;
					br.bottom = br.top + bar.size() - 1;
					}
				else
					{
					br.left = rect.left + pos;
					br.right = br.left + bar.size() - 1;
					br.top = rect.top;
					br.bottom = rect.bottom;
					}
				
				pos += bar.size();
				}
			
			p.offset(pos);
			pos += p.currentSize();
			
			TRACE("Pane %d - size=%d  offset=%d\n", i, p.currentSize(), p.offset());

			if (isVerticalSplit())
				{
				p.getWidget()->resize(x(), y()+p.offset(), rect.Width(), p.currentSize());
				}
			else
				{
				p.getWidget()->resize(x()+p.offset(), y(), p.currentSize(), rect.Height());
				}
			}

		damage(FL_DAMAGE_ALL);
		fx_invalidate(this);
		}

int			FXSplitterWnd::onMiddleMouseButton(int action, const FXPoint &pt)			{ return 0; }
int			FXSplitterWnd::onRightMouseButton(int action, const FXPoint &pt)				{ return 0; }
int			FXSplitterWnd::onMouseWheel(int dx, int dy, const FXPoint &pt)	{ return 0; }

int
FXSplitterWnd::onMouseMove(const FXPoint &pt)
		{
		int		res=0;
		int		cur=FL_CURSOR_DEFAULT;
		int		bar;

		bar = barIndex(pt);
		if (bar >= 0)
			{
			if (isVerticalSplit())
				{
				cur = FL_CURSOR_NS;
				}
			else
				{
				cur = FL_CURSOR_WE;
				}
				
			res = 1;
			}
		
		if (m_cursor != cur)
			{
			m_cursor = cur;
			fl_cursor((Fl_Cursor)cur);
			}

		return res;
		}


int
FXSplitterWnd::onLeftMouseButton(int action, const FXPoint &pt)
		{
		int		res=0;

		if (action == FX_BUTTON_UP)
			{
			if (dragging())
				{
				dragging(false);
				res = 0;
			
				TRACE("Stop dragging - dragBar=%d\n", m_dragBar);
				}
			}
		else if (action == FX_BUTTON_DOWN)
			{
			m_dragBar = barIndex(pt);
			if (m_dragBar >= 0)
				{
				FXSplitterBar	&bar=m_bars[m_dragBar];

				if (isVerticalSplit())
					{
					m_dragOffset = pt.y - bar.rect.top + bar.padding + (bar.width / 2);
					}
				else
					{
					m_dragOffset = pt.x - bar.rect.left + bar.padding + (bar.width / 2);
					}
			
				dragging(true);
				res = 1;
				TRACE("Start dragging - dragOffset=%d  dragBar=%d\n", m_dragOffset, m_dragBar);
				}
			}

		return res;
		}


int
FXSplitterWnd::onMouseDrag(const FXPoint &pt)
		{
		int		mouse;
			
		if (isVerticalSplit())
			{
			mouse = pt.y;
			}
		else
			{
			mouse = pt.x;
			}

		FXSplitterBar	&bar=m_bars[m_dragBar];

		int		adjustedmouse=mouse - m_dragOffset + bar.padding + (bar.width / 2);
		int		baroffset=bar.vertical?bar.rect.top:bar.rect.left;
		int		off=0;
			
		off = adjustedmouse - baroffset;
			
		TRACE("mouse=%d  dragOffset=%d  adjustedmouse=%d  dragBar=%d  baroff=%d  off=%d\n", mouse, m_dragOffset, adjustedmouse, m_dragBar, baroffset, off);
			
		if (off)
			{
			if (canAdjustSize(m_dragBar, off, -1) && canAdjustSize(m_dragBar+1, -off, 1))
				{
				adjustSize(m_dragBar, off, -1);
				adjustSize(m_dragBar+1, -off, 1);
				movePanes();
					
				// Save the settings as preferences
				for (int i=0;i<(int)m_panes.size();i++)
					{
					FXSplitterPane	&p=m_panes[i];
						
					p.preferredSize(p.currentSize());
					}
				}
			}

		return 1;
		}


int
FXSplitterWnd::onMouseDown(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(Fl::event_clicks()?FX_BUTTON_DOUBLE_CLICK:FX_BUTTON_DOWN, pt);
				break;
			}

		return res;
		}


int
FXSplitterWnd::onMouseUp(int button, const FXPoint &pt)
		{
		int		res=0;

		switch (button)
			{
			case FL_LEFT_MOUSE:
				res = onLeftMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_RIGHT_MOUSE:
				res = onRightMouseButton(FX_BUTTON_UP, pt);
				break;

			case FL_MIDDLE_MOUSE:
				res = onMiddleMouseButton(FX_BUTTON_UP, pt);
				break;
			}

		return res;
		}


int
FXSplitterWnd::handle(int eventcode)
		{
		int		res=0;

		m_lastEventCode = eventcode;
		m_lastEventPoint.x = Fl::event_x();
		m_lastEventPoint.y = Fl::event_y();

		switch (eventcode)
			{
			case FL_ENTER:
				res = 1;
				break;
			
			case FL_LEAVE:
				res = 1;
				break;

			case FL_MOVE:
				res = onMouseMove(m_lastEventPoint);
				break;
				
			case FL_DRAG:
				res = onMouseDrag(m_lastEventPoint);
				break;
		
			case FL_PUSH:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags |= FLAG_LEFT_MOUSE;		break;
					case FL_MIDDLE_MOUSE:	m_flags |= FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags |= FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseDown(button, m_lastEventPoint);
				}
				break;

			case FL_RELEASE:
				{
				int		button=Fl::event_button();

				switch (button)
					{
					case FL_LEFT_MOUSE:		m_flags &= ~FLAG_LEFT_MOUSE;	break;
					case FL_MIDDLE_MOUSE:	m_flags &= ~FLAG_MIDDLE_MOUSE;	break;
					case FL_RIGHT_MOUSE:	m_flags &= ~FLAG_RIGHT_MOUSE;	break;
					}

				res = onMouseUp(button, m_lastEventPoint);
				}
				break;
			}

		// TRACE("FXWidget::handle(%d) at %d,%d = %d\n", eventcode, m_lastEventPoint.x, m_lastEventPoint.y, res);

		if (res == 0) res = Fl_Group::handle(eventcode);

		return res;
		}

