/*
**	FXSeparator.h	A generic dialog class
*/

#ifndef __FltkExt_FXSeparator_h__
#define __FltkExt_FXSeparator_h__

#include <FltkExt/FXWidget.h>

/**
*** A simple coloured box that will expand to the right size
**/
class FLTKEXT_DLL_EXPORT FXSeparator : public FXWidget
		{
public:
		FXSeparator(bool vertical=true, const FXColor &color=FX_COLOR_DEFAULT);
		FXSeparator(int X, int Y, int W, int H, bool vertical=true, const FXColor &color=FX_COLOR_DEFAULT, int linewidth=1);

		int		getLineWidth() const	{ return m_width; }

protected:

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();
		
protected:
		bool	m_vertical;
		FXColor	m_color;
		int		m_width;
		};


#endif // __FltkExt_FXSeparator_h__
