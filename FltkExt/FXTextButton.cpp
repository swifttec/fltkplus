#include "FXTextButton.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWFileInfo.h>

FXTextButton::FXTextButton(sw_uint32_t btnid, const SWString &text, const FXFont &font, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &frcolor) :
	FXButton(X, Y, W, H)
		{
		id(btnid);
		init();
		initStyles(font);
		setText(text);
		}


FXTextButton::FXTextButton(const SWString &text, const FXFont &font, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &frcolor) :
	FXButton(X, Y, W, H)
		{
		init();
		initStyles(font);
		setText(text);
		}


FXTextButton::FXTextButton(int X, int Y, int W, int H, const SWString &text, const FXFont &font, const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &frcolor) :
	FXButton(X, Y, W, H)
		{
		init();
		initStyles(font);
		setText(text);
		}


void
FXTextButton::initStyles(const FXFont &font)
		{
		FXButtonStyle	*pStyle;

		pStyle = &m_style[Disabled];
		pStyle->bdColor = RGB(150, 150, 150);
		pStyle->bdWidth = 1;
		pStyle->bgColor = RGB(200, 200, 200);
		pStyle->fgColor = RGB(220, 220, 220);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Normal];
		pStyle->bdColor = "#aaa";
		pStyle->bdWidth = 1;
		pStyle->bgColor = "#ddd";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Focused];
		pStyle->bdColor = "#248";
		pStyle->bdWidth = 2;
		pStyle->bgColor = "#ddd";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Hover];
		pStyle->bdColor = "#9ce";
		pStyle->bdWidth = 1;
		pStyle->bgColor = "#def";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		pStyle = &m_style[Pressed];
		pStyle->bdColor = "#8ac";
		pStyle->bdWidth = 1;
		pStyle->bgColor = "#cde";
		pStyle->fgColor = RGB(0, 0, 0);
		pStyle->bdCornerSize = 0;
		pStyle->font = font;

		m_style[SelectedNormal] = m_style[Normal];
		m_style[SelectedDisabled] = m_style[Disabled];
		m_style[SelectedFocused] = m_style[Focused];
		m_style[SelectedHover] = m_style[Hover];
		m_style[SelectedPressed] = m_style[Pressed];
		}


FXTextButton::~FXTextButton()
		{
		}
