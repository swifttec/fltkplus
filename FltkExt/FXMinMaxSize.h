#ifndef __FltkExt_FXMinMaxSize_h__
#define __FltkExt_FXMinMaxSize_h__

#include <FltkExt/FXSize.h>

class FLTKEXT_DLL_EXPORT FXMinMaxSize
		{
public:
		FXMinMaxSize(int minW=0, int minH=0, int maxW=0, int maxH=0);
		FXMinMaxSize(const FXSize &vmin, const FXSize &vmax);
		FXMinMaxSize(const FXMinMaxSize &other);
		virtual ~FXMinMaxSize();

		FXMinMaxSize &	operator=(const FXMinMaxSize &other);
		bool			operator==(const FXMinMaxSize &other);
		void			clear();

public:
		FXSize	minSize;
		FXSize	maxSize;
		};

#endif // __FltkExt_FXMinMaxSize_h__
