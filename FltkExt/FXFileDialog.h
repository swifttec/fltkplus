#ifndef __FltkExt_FXFileDialog_h__
#define __FltkExt_FXFileDialog_h__

#include <FltkExt/FltkExt.h>

#include <FL/Fl_Native_File_Chooser.H>

class FLTKEXT_DLL_EXPORT FXFileDialog : public Fl_Native_File_Chooser
		{
public:
		enum Action
			{
			OpenFile=0,
			SaveFile=1,
			OpenMulti=2,
			SINGLE=0,
			MULTI=2,
			CREATE=1
			};

public:
		/*
		pattern Sets the filename filters used for browsing.

		The default is NULL, which browses all files.

		The filter string can be any of:

			A single wildcard (eg. "*.txt")
			Multiple wildcards (eg. "*.{cxx,h,H}")
			A descriptive name followed by a "\t" and a wildcard (eg. "Text Files\t*.txt")
			A list of separate wildcards with a "\n" between each (eg. "*.{cxx,H}\n*.txt")
			A list of descriptive names and wildcards (eg. "C++ Files\t*.{cxx,H}\nTxt Files\t*.txt")

		The format of each filter is a wildcard, or an optional user description followed by '\t' and the wildcard.

		On most platforms, each filter is available to the user via a pulldown menu in the file chooser. The 'All Files' option is always available to the user.
		*/
		FXFileDialog(const SWString &folder, const SWString &pattern, Action action, const SWString &title);
		~FXFileDialog();

		int			doModal();
		
		int			getFilenameCount()	{ return count(); }
		SWString	getFilename(int i)	{ return filename(i); }

protected:
		SWString	m_title;
		SWString	m_folder;
		};


#endif // __FltkExt_FXFileDialog_h__
