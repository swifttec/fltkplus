/*
**	FXPropertyPage.h	A generic dialog class
*/

#ifndef __FltkExt_FXPropertyPage_h__
#define __FltkExt_FXPropertyPage_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXDataExchange.h>

#include <swift/SWString.h>



#include <FL/Fl_Group.H>

class FXPropertySheet;

class FLTKEXT_DLL_EXPORT FXPropertyPage : public Fl_Group
		{
friend class FXPropertySheet;

public:
		FXPropertyPage(const SWString &title);
		FXPropertyPage(int id, const SWString &title);
		virtual ~FXPropertyPage();

		bool				modified() const	{ return m_modified; }

		int					pageid() const		{ return m_pageid; }
		const SWString &	pagetitle() const	{ return m_pagetitle; }

		void				addItem(int itemid, Fl_Widget *pItem);

		/// Get the widget that corresponds to the given ID
		Fl_Widget *			getItem(int itemid)												{ return fx_group_getItem(this, itemid); }

		/// Enable / disable the widget that corresponds to the given ID
		void				enableItem(int itemid, bool v);

		void				setItemValue(int itemid, const char *v)							{ fx_group_setItemValue(this, itemid, v, 0); }
		void				setItemValue(int itemid, const wchar_t *v)						{ fx_group_setItemValue(this, itemid, v, 0); }
		void				setItemValue(int itemid, const SWString &v, int limitsize=0)	{ fx_group_setItemValue(this, itemid, v, limitsize); }
		void				setItemValue(int itemid, int v, int limitsize=0)				{ fx_group_setItemValue(this, itemid, v, limitsize); }
		void				setItemValue(int itemid, bool v)								{ fx_group_setItemValue(this, itemid, v); }
		void				setItemValue(int itemid, double v)								{ fx_group_setItemValue(this, itemid, v); }

		void				getItemValue(int itemid, SWString &v)							{ fx_group_getItemValue(this, itemid, v); }
		void				getItemValue(int itemid, int &v)								{ fx_group_getItemValue(this, itemid, v); }
		void				getItemValue(int itemid, bool &v)								{ fx_group_getItemValue(this, itemid, v); }
		void				getItemValue(int itemid, double &v)								{ fx_group_getItemValue(this, itemid, v); }
		void				getItemValue(int itemid, sw_uint16_t &v)						{ int	t; fx_group_getItemValue(this, itemid, t); v = (sw_uint16_t)t; }

		FXPropertySheet *	getPropertySheet()												{ return m_pSheet; }

		/// Call to update the data
		void				updateData(bool save=true);

		/// Get the widget that corresponds to the given ID (MFC compatible method)
		Fl_Widget *			GetDlgItem(int itemid)											{ return fx_group_getItem(this, itemid); }

		/// Get the background color
		FXColor				getBackgroundColor();

protected:

		/// Called to create the page after positioning
		virtual void		onCreate();

		virtual void		onLoadData();
		virtual void		onSaveData();
		virtual void		onShowPage();
		virtual bool		canEnableOK();

		void				setModified();

		/// Called to exchange data between dialog items and the application when updateData is called
		virtual void		doDataExchange(FXDataExchange *pDX);

protected:
		static void			childCallback(Fl_Widget *pWidget);
		static void			classCallback(Fl_Widget *pWidget, void *pUserData);
		virtual void		onCallback(Fl_Widget *pWidget);

protected:
		FXPropertySheet	*m_pSheet;
		SWString		m_pagetitle;
		int				m_pageid;
		bool			m_modified;
		};


#endif // __FltkExt_FXPropertyPage_h__

