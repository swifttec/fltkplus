/*
**	FXDialog.h	A generic dialog class
*/

#ifndef __FltkExt_FXPromptCheckListDialog_h__
#define __FltkExt_FXPromptCheckListDialog_h__

#include <FltkExt/FXDialog.h>
#include <FltkExt/FXCheckList.h>


/**
*** A popup dialog window for entering a number
**/
class FLTKEXT_DLL_EXPORT FXPromptCheckListDialog : public FXDialog
		{
public:
		FXPromptCheckListDialog(const SWString &title, const SWString &prompt, SWArray<FXCheckListEntry> &values, Fl_Widget *pParent=NULL, int W=400, int H=300);
		virtual ~FXPromptCheckListDialog();

		virtual void	onOK();

protected:
		SWArray<FXCheckListEntry>	&m_list;
		};


#endif // __FltkExt_FXPromptCheckListDialog_h__
