/*
**	FXMenuBar.h	A generic popup menu class
*/

#ifndef __FltkExt_FXMenuBar_h__
#define __FltkExt_FXMenuBar_h__

#ifndef __FltkExt_h__
#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXMenuItem.h>
#include <FltkExt/FXMenu.h>
#include <FltkExt/FXPoint.h>
#include <FltkExt/FXMenuItem.h>
#include <FltkExt/FXWidget.h>

class FLTKEXT_DLL_EXPORT FXMenuBar : public FXWidget
		{
protected:
		FXMenu			*m_pMenu;
		FXMenuItem		*m_pLastSelectedItem;

		// FXMenuItem *menu_;
		const FXMenuItem *value_;

		// uchar alloc;			// flag indicates if menu_ is a dynamic copy (=1) or not (=0)
		uchar down_box_;
		Fl_Font textfont_;
		Fl_Fontsize textsize_;
		Fl_Color textcolor_;

protected:
		void draw();

public:
		/**
		  Creates a new Fl_Menu_Bar widget using the given position,
		  size, and label string. The default boxtype is FL_UP_BOX.

		  The constructor sets menu() to NULL.  See
		  Fl_Menu_ for the methods to set or change the menu.

		  labelsize(), labelfont(), and labelcolor()
		  are used to control how the menubar items are drawn.  They are
		  initialized from the Fl_Menu static variables, but you can
		  change them if desired.

		  label() is ignored unless you change align() to
		  put it outside the menubar.

		  The destructor removes the Fl_Menu_Bar widget and all of its
		  menu items.
		*/

		FXMenuBar();
		FXMenuBar(int, int, int, int);
		~FXMenuBar();

		const FXMenuItem* picked(const FXMenuItem*);
		const FXMenuItem* find_item(const SWString &name);
		const FXMenuItem* find_item(Fl_Callback*);

		/**
		  Returns the menu item with the entered shortcut (key value).

		  This searches the complete menu() for a shortcut that matches the
		  entered key value.  It must be called for a FL_KEYBOARD or FL_SHORTCUT
		  event.

		  If a match is found, the menu's callback will be called.

		  \return matched FXMenuItem or NULL.
		const FXMenuItem* test_shortcut() { return picked(menu()->test_shortcut()); }
		*/
		void global();

		/**
		  Returns a pointer to the array of FXMenuItems.  This will either be
		  the value passed to menu(value) or the private copy.
		  \sa size() -- returns the size of the FXMenuItem array.

		  \b Example: How to walk the array:
		  \code
		  for ( int t=0; t<menubar->size(); t++ ) {                // walk array of items
			  const FXMenuItem &item = menubar->menu()[t];       // get each item
			  fprintf(stderr, "item #%d -- label=%s, value=%s type=%s\n",
				  t,
				  item.label() ? item.label() : "(Null)",          // menu terminators have NULL labels
				  (item.flags & FL_MENU_VALUE) ? "set" : "clear",  // value of toggle or radio items
				  (item.flags & FL_SUBMENU) ? "Submenu" : "Item"); // see if item is a submenu or actual item
		  }
		  \endcode

		*/
		FXMenu *menu() const { return m_pMenu; }

		void	insert(int index, const SWString &text, int shortcut, Fl_Callback*, void* = 0, int = 0);
		void	add(sw_uint32_t id, const SWString &, int shortcut, Fl_Callback*, void* = 0, int = 0); // see src/FXMenuBaradd.cxx
		void	add(const SWString &, int shortcut, Fl_Callback*, void* = 0, int = 0); // see src/FXMenuBaradd.cxx
		void	add(const SWString &s);
		int  size() const;
		void size(int W, int H) { Fl_Widget::size(W, H); }
		void clear();
		int clear_submenu(int index);
		void replace(int idx, const SWString &s);
		void remove(int);
		/** Changes the shortcut of item \p i to \p s. */
		void shortcut(int i, int s) { m_pMenu->getItemAt(i)->shortcut(s); }
		
		/** Sets the flags of item i.  For a list of the flags, see FXMenuItem.  */
		void mode(int i, int fl)	{ m_pMenu->getItemAt(i)->flags(fl); }
		
		/** Gets the flags of item i.  For a list of the flags, see FXMenuItem.  */
		int  mode(int i) const		{ return m_pMenu->getItemAt(i)->flags(); }

		/** Returns a pointer to the last menu item that was picked.  */
		const FXMenuItem *mvalue() const { return value_; }

		/** Returns the index into menu() of the last item chosen by the user.  It is zero initially. */
		//int value() const { return value_ ? (int)(value_ - menu_) : -1; }
		int value(const FXMenuItem*);
		/**
		  The value is the index into menu() of the last item chosen by
		  the user.  It is zero initially.  You can set it as an integer, or set
		  it with a pointer to a menu item.  The set routines return non-zero if
		  the new value is different than the old one.
		*/
		// int value(int i) { return value(menu_ + i); }
		
		/** Returns the title of the last item chosen.  */
		SWString	text() const		{ return value_ ? value_->text() : SWString(); }
		
		/** Returns the title of item i.  */
		SWString	text(int i) const	{ return m_pMenu->getItemAt(i)->text(); }

		/** Gets the current font of menu item labels.  */
		Fl_Font textfont() const { return textfont_; }

		/**  Sets the current font of menu item labels.  */
		void textfont(Fl_Font c) { textfont_ = c; }

		/**  Gets the font size of menu item labels.  */
		Fl_Fontsize textsize() const { return textsize_; }

		/**  Sets the font size of menu item labels.  */
		void textsize(Fl_Fontsize c) { textsize_ = c; }

		/** Get the current color of menu item labels.  */
		Fl_Color textcolor() const { return textcolor_; }

		/** Sets the current color of menu item labels. */
		void textcolor(Fl_Color c) { textcolor_ = c; }

		/**
		  This box type is used to surround the currently-selected items in the
		  menus.  If this is FL_NO_BOX then it acts like
		  FL_THIN_UP_BOX and selection_color() acts like
		  FL_WHITE, for back compatibility.
		*/
		Fl_Boxtype down_box() const { return (Fl_Boxtype)down_box_; }
		/**    See Fl_Boxtype FXMenuBar::down_box() const   */
		void down_box(Fl_Boxtype b) { down_box_ = b; }

		/** For back compatibility, same as selection_color() */
		Fl_Color down_color() const { return selection_color(); }
		/** For back compatibility, same as selection_color() */
		void down_color(unsigned c) { selection_color(c); }
		void setonly(FXMenuItem* item);
		void			add(sw_uint32_t id, const SWString &text);
		void			addTo(sw_uint32_t pid, sw_uint32_t id, const SWString &text);
		void			add(const FXMenuItem &item);
		void			addSeparator();
		void			check(sw_uint32_t id, bool v);
		void			enable(sw_uint32_t id, bool v);
		FXMenuItem *	findItem(sw_uint32_t id);

protected:

		/// Called when the mouse moves over the widget
		virtual int		onMouseEnter();

		/// Called when the mouse moves over the widget
		virtual int		onMouseLeave();

		/// Called when the mouse is moved
		virtual int		onMouseMove(const FXPoint &pt);

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

protected:
		static void		menuCallback(Fl_Widget *pWidget, void *userdata);

protected:
		Fl_Widget		*m_pOwner;
		FXMenu			m_menu;
		};

#endif // __FltkExt_FXMenuBar_h__
