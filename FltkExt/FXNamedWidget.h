/*
**	FXNamedWidget.h	A generic dialog class
*/

#ifndef __FltkExt_FXNamedWidget_h__
#define __FltkExt_FXNamedWidget_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXStaticText.h>

#include <swift/SWValue.h>

class FLTKEXT_DLL_EXPORT FXNamedWidget : public FXGroup
		{
public:
		FXNamedWidget(int X, int Y, int nameW, int valueW, int H);

		void				setName(const SWString &v)						{ m_pName->setText(v); }
		SWString			getName()										{ return m_pName->text(); }

		virtual void		setValue(const SWValue &v, int limitsize=0);
		virtual SWValue		getValue();

		virtual void		activate();
		virtual void		deactivate();

		/// Called to handle an item callback
		virtual void		onItemCallback(Fl_Widget *pWidget, void *p);

protected:
		void			init(const SWString &name, Fl_Widget *pValueWidget, const SWValue &value);

protected:
		FXStaticText	*m_pName;	///< The name widget
		Fl_Widget		*m_pValue;	///< The value widget
		int				m_nameW;
		int				m_valueW;
		};


#endif // __FltkExt_FXNamedWidget_h__
