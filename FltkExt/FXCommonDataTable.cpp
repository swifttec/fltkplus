#include "FXCommonDataTable.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>


#include <xplatform/sw_math.h>

FXCommonDataTable::FXCommonDataTable()
		{
		m_padding.left = m_padding.right = m_padding.top = m_padding.bottom = 10;
		m_buttonWidth = 100;
		m_buttonGapX = 10;
		setBackgroundColor("#088");

		add(m_pDataTable = new FXDataTable());
		add(m_pButtonBar = new FXButtonBar(0, 0, 0, 0, true, 64, 5, 5));
		}


FXCommonDataTable::~FXCommonDataTable()
		{
		}


void
FXCommonDataTable::addColumn(const SWString &title, int align)
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->addColumn(title, align);
			}
		}


void
FXCommonDataTable::removeAllRows()
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->removeAllRows();
			}
		}



int
FXCommonDataTable::addRow(const SWStringArray &values)
		{
		int		res=0;

		if (m_pDataTable != NULL)
			{
			res = m_pDataTable->addRow(values);
			}

		return res;
		}


void
FXCommonDataTable::setRowColors(int row, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->setRowColors(row, fgcolor, bgcolor);
			}
		}


void
FXCommonDataTable::setCell(int row, int col, const SWString &contents, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->setCell(row, col, contents, fgcolor, bgcolor);
			}
		}


void
FXCommonDataTable::setCellColors(int row, int col, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->setCellColors(row, col, fgcolor, bgcolor);
			}
		}

void
FXCommonDataTable::setCellString(int row, int col, const SWString &contents)
		{
		if (m_pDataTable != NULL)
			{
			m_pDataTable->setCellString(row, col, contents);
			}
		}


SWString
FXCommonDataTable::getCellString(int row, int col) const
		{
		SWString	res;

		if (m_pDataTable != NULL)
			{
			res = m_pDataTable->getCellString(row, col);
			}

		return res;
		}


void
FXCommonDataTable::addButton(int id, const SWString &text, const SWString &image, bool enabled)
		{
		if (m_pButtonBar != NULL)
			{
			m_pButtonBar->addButton(id, text, image, enabled);
			}
		}


void
FXCommonDataTable::resize(int X, int Y, int W, int H)
		{
		FXGroup::resize(X, Y, W, H);

		int			bwidth=0, bgap=0;

		if (m_pButtonBar != NULL && m_pButtonBar->getButtonCount() > 0)
			{
			bwidth = m_buttonWidth;
			bgap = m_buttonGapX;
			}


		if (m_pDataTable != NULL)
			{
			m_pDataTable->resize(
				X + m_padding.left,
				Y + m_padding.top,
				W - m_padding.left-m_padding.right - bwidth - bgap,
				H - m_padding.top-m_padding.bottom);
			}

		if (m_pButtonBar != NULL)
			{
			m_pButtonBar->resize(X + W - m_padding.right - bwidth,
								Y + m_padding.top,
								bwidth,
								H - m_padding.top-m_padding.bottom);
			}
		}

