#ifndef __FltkExt_FXProgressBar_h__
#define __FltkExt_FXProgressBar_h__

#include <FltkExt/FXWidget.h>

/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXProgressBar : public FXWidget
		{
public:
		FXProgressBar(int w, int h);
		FXProgressBar(int x, int y, int w, int h);
		virtual ~FXProgressBar();

		// Update the progress bar
		void			setText(const SWString &text, bool redraw=true);

		// Update the progress bar
		void			setPosition(int pos, bool redraw=true);

		// Update the progress bar
		void			update(const SWString &text, int pos, bool redraw=true);

		// Set the range of values for position
		void			setRange(int minpos, int maxpos)	{ m_minpos = minpos; m_maxpos = maxpos; }

		// Set bar style (0=left-right, 1=right-left, 2=bottom-top, 3=top-bottom)
		void			setBarStyle(int v)					{ m_barstyle = v; }

		// Get bar style (0=left-right, 1=right-left, 2=bottom-top, 3=top-bottom)
		int				getBarStyle() const					{ return m_barstyle; }

		// Set the border color
		void			setBarColor(const FXColor &v)		{ m_barColor = v; }

		// Get the border color
		const FXColor &	getBarColor() const					{ return m_barColor; }

protected:
		virtual void	init();
		
		virtual void	onDraw();

protected:
		SWString		m_text;
		int				m_fontface;
		int				m_fontsize;
		int				m_position;
		int				m_minpos;
		int				m_maxpos;
		int				m_barstyle;

		FXColor			m_barColor;	// Bar color
		};

#endif // __FltkExt_FXProgressBar_h__
