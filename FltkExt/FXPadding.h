#ifndef __FltkExt_FXPadding_h__
#define __FltkExt_FXPadding_h__

// Padding is just the same as margin, so simple include needed.
#include <FltkExt/FXMargin.h>

#endif // __FltkExt_FXPadding_h__
