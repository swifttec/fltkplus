#include "FXWaitCursor.h"


FXWaitCursor::FXWaitCursor(Fl_Widget *pWidget) :
	m_pWindow(NULL)
		{
		Fl_Window	*pWindow=NULL;

		if (pWidget != NULL)
			{
			pWindow = dynamic_cast<Fl_Window *>(pWidget);
			while (pWindow == NULL)
				{
				if (pWidget == pWidget->parent()) break;
				pWidget = pWidget->parent();
				if (pWidget == NULL) break;
				pWindow = dynamic_cast<Fl_Window *>(pWidget);
				}
			}

		if (pWindow != NULL)
			{
			m_pWindow = pWindow;
			m_pWindow->cursor(FL_CURSOR_WAIT);
			}
		}


FXWaitCursor::~FXWaitCursor()
		{
		if (m_pWindow != NULL)
			{
			m_pWindow->cursor(FL_CURSOR_DEFAULT);
			m_pWindow = NULL;
			}
		}
