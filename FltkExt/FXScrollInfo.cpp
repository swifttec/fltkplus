#include "FXScrollInfo.h"

FXScrollInfo::FXScrollInfo() 
		{
		clear();
		}


FXScrollInfo::FXScrollInfo(int from, int to, int page, int pos)
		{
		clear();
		setRange(from, to, page);
		setPosition(pos);
		}

		
void
FXScrollInfo::clear()
		{
		m_rangeFrom = 0;
		m_rangeTo = 1;
		m_pageSize = 1;
		m_position = 0;
		m_lineSize = 1;
		}


void
FXScrollInfo::setRange(int from, int to, int page)
		{
		if (from > to)
			{
			m_rangeFrom = to;
			m_rangeTo = from;
			}
		else
			{
			m_rangeFrom = from;
			m_rangeTo = to;
			}

		m_pageSize = page;

		if ((m_position + m_pageSize) > m_rangeTo)
			{
			m_position = m_rangeTo - m_pageSize;
			if (m_position < m_rangeFrom) m_position = m_rangeFrom;
			}
		}


void
FXScrollInfo::setPageSize(int v)
		{
		m_pageSize = v;
		}


void
FXScrollInfo::setLineSize(int v)
		{
		m_lineSize = v;
		}

void
FXScrollInfo::setRange(int from, int to)
		{
		setRange(from, to, m_pageSize);
		}

void
FXScrollInfo::setPosition(int pos)
		{
		m_position = pos;
		if (m_position > (m_rangeTo - m_pageSize)) m_position = m_rangeTo - m_pageSize;
		if (m_position < m_rangeFrom) m_position = m_rangeFrom;
		}