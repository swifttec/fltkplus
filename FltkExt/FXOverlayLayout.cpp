#include <FltkExt/FXOverlayLayout.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>
#include <xplatform/sw_math.h>

FXOverlayLayout::FXOverlayLayout()
		{
		}


FXOverlayLayout::~FXOverlayLayout()
		{
		}


FXSize
FXOverlayLayout::getMinSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();

		for (int i=0;i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		sz=fx_getMinSize(pWidget);

			res.cx = sw_math_max(res.cx, sz.cx);
			res.cy = sw_math_max(res.cy, sz.cy);
			}

		return res;
		}


FXSize
FXOverlayLayout::getMaxSize() const
		{
		FXSize	res;
		int		nchildren=getChildCount();
		bool	unlimitedWidth=false;
		bool	unlimitedHeight=false;

		for (int i=0;!(unlimitedWidth && unlimitedHeight)&&i<nchildren;i++)
			{
			Fl_Widget	*pWidget=m_pOwner->child(i);
			FXSize		sz=fx_getMaxSize(pWidget);

			if (sz.cx == 0) unlimitedWidth = true;
			else res.cx = sw_math_min(res.cx, sz.cx);

			if (sz.cy == 0) unlimitedHeight = true;
			else res.cy = sw_math_min(res.cy, sz.cy);
			}

		if (unlimitedWidth) res.cx = 0;
		if (unlimitedHeight) res.cy = 0;

		return res;
		}


void
FXOverlayLayout::addWidget(Fl_Widget *pWidget)
		{
		m_pOwner->add(pWidget);
		}


void
FXOverlayLayout::layoutChildren()
		{
		Fl_Widget	*const *a;
		int			nchildren=m_pOwner->children();
		int			minsize=0, maxsize=0, unlimited=0;
		FXRect		cr;

		fx_getContentsRect(m_pOwner, cr);

		int			X=cr.left, Y=cr.top;
		int			W=cr.width(), H=cr.height();

		W -= m_padding.left + m_padding.right;
		H -= m_padding.top + m_padding.bottom;
		X += m_padding.left;
		Y += m_padding.top;

		if (nchildren > 0)
			{
			a = m_pOwner->array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				o->resize(X, Y, W, H);
				}
			}
		}
