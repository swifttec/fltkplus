#include "FXLayeredWnd.h"



FXLayeredWnd::FXLayeredWnd(const SWString &title, int W, int H, bool fixedSize, Fl_Widget *pOwner) :
	FXWnd(title, W, H, fixedSize, pOwner)
		{
		end();

		FXWnd::add(m_pLayeredGroup = new FXLayeredGroup(0, 0, W, H));
		}


FXLayeredWnd::~FXLayeredWnd()
		{
		}



void
FXLayeredWnd::add(Fl_Widget &w)
		{
		m_pLayeredGroup->addLayer(&w);
		}


void
FXLayeredWnd::add(Fl_Widget *o)
		{
		m_pLayeredGroup->addLayer(o);
		}


void
FXLayeredWnd::addLayer(int id, Fl_Widget *o)
		{
		m_pLayeredGroup->addLayer(id, o);
		}


void
FXLayeredWnd::addLayer(Fl_Widget *o)
		{
		m_pLayeredGroup->addLayer(o);
		}


bool
FXLayeredWnd::showLayer(int id, bool hideIfNotFound)
		{
		return m_pLayeredGroup->showLayer(id, hideIfNotFound);
		}


bool
FXLayeredWnd::showLayer(Fl_Widget *p, bool hideIfNotFound)
		{
		return m_pLayeredGroup->showLayer(p, hideIfNotFound);
		}


bool
FXLayeredWnd::hideLayer()
		{
		return m_pLayeredGroup->hideLayer();
		}

void
FXLayeredWnd::setBackgroundColor(const FXColor &v)
		{
		FXWnd::setBackgroundColor(v);
		m_pLayeredGroup->setBackgroundColor(v);
		}


int
FXLayeredWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (m_pLayeredGroup != NULL)
			{
			res = m_pLayeredGroup->onKeyDown(code, text, pt);
			}

		if (res == 0)
			{
			res = FXWnd::onKeyDown(code, text, pt);
			}

		return res;
		}


int
FXLayeredWnd::onShortcut(int code, const FXPoint &pt)
		{
		int		res=0;

		if (m_pLayeredGroup != NULL)
			{
			res = m_pLayeredGroup->onShortcut(code, pt);
			}

		if (res == 0)
			{
			res = FXWnd::onShortcut(code, pt);
			}

		return res;
		}

