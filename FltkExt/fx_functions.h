/**
***	fx_functions.h
**/

#ifndef __FltkExt_fx_functions_h__
#define __FltkExt_fx_functions_h__

#ifndef __FltkExt_h__
	#include <FltkExt/FltkExt.h>
#endif

#include <FltkExt/FXRect.h>
#include <FltkExt/fx_draw.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/fx_msgbox.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>
#include <swift/SWValue.h>
#include <swift/SWAppConfig.h>



// Forward declarations
class Fl_Group;
class Fl_Widget;
class Fl_Window;

FLTKEXT_DLL_EXPORT void			fx_invalidate_init();
FLTKEXT_DLL_EXPORT void			fx_invalidate_process();
FLTKEXT_DLL_EXPORT void			fx_invalidate(Fl_Widget *pWidget);

FLTKEXT_DLL_EXPORT void			fx_group_addItem(Fl_Group *pGroup, int itemid, Fl_Widget *pItem);
FLTKEXT_DLL_EXPORT Fl_Widget * 	fx_group_getItem(Fl_Group *pGroup, int itemid);
FLTKEXT_DLL_EXPORT Fl_Widget * 	fx_group_getItemAt(Fl_Group *pGroup, const FXPoint &pt);
FLTKEXT_DLL_EXPORT void			fx_group_enableItem(Fl_Group *pGroup, int itemid, bool enabled);
FLTKEXT_DLL_EXPORT void			fx_group_enableAllItemsWithID(Fl_Group *pGroup, int itemid, bool enabled);
FLTKEXT_DLL_EXPORT void			fx_group_showItem(Fl_Group *pGroup, int itemid, bool visible);

FLTKEXT_DLL_EXPORT bool			fx_group_setItemValue(Fl_Group *pGroup, int itemid, const SWValue &v, int limitsize=0);
FLTKEXT_DLL_EXPORT bool			fx_group_getItemValue(Fl_Group *pGroup, int itemid, SWValue &v);

FLTKEXT_DLL_EXPORT bool			fx_group_getItemValue(Fl_Group *pGroup, int itemid, SWString &v);
FLTKEXT_DLL_EXPORT bool			fx_group_getItemValue(Fl_Group *pGroup, int itemid, int &v);
FLTKEXT_DLL_EXPORT bool			fx_group_getItemValue(Fl_Group *pGroup, int itemid, bool &v);
FLTKEXT_DLL_EXPORT bool			fx_group_getItemValue(Fl_Group *pGroup, int itemid, double &v);

FLTKEXT_DLL_EXPORT void			fx_group_moveChildren(Fl_Group *pGroup, int xoff, int yoff);
FLTKEXT_DLL_EXPORT bool			fx_group_isOwnerOf(Fl_Group *pGroup, Fl_Widget *pWidget);


FLTKEXT_DLL_EXPORT void			fx_widget_centerOver(Fl_Widget *pWidget, Fl_Widget *pUnderWidget);
FLTKEXT_DLL_EXPORT int			fx_widget_handleCommand(Fl_Widget *pWidget, sw_uint32_t id);
FLTKEXT_DLL_EXPORT bool			fx_widget_isMainWindow(Fl_Widget *pWidget);
FLTKEXT_DLL_EXPORT bool			fx_widget_getScreenCoordinates(Fl_Widget *pWidget, int &x, int &y);

FLTKEXT_DLL_EXPORT bool			fx_widget_setValue(Fl_Widget *pWidget, const SWValue &v, int limitsize=0);
FLTKEXT_DLL_EXPORT bool			fx_widget_getValue(Fl_Widget *pWidget, SWValue &v);

FLTKEXT_DLL_EXPORT bool			fx_widget_getValue(Fl_Widget *pWidget, SWString &v);
FLTKEXT_DLL_EXPORT bool			fx_widget_getValue(Fl_Widget *pWidget, int &v);
FLTKEXT_DLL_EXPORT bool			fx_widget_getValue(Fl_Widget *pWidget, bool &v);
FLTKEXT_DLL_EXPORT bool			fx_widget_getValue(Fl_Widget *pWidget, double &v);

FLTKEXT_DLL_EXPORT Fl_Window *	fx_widget_getWindow(Fl_Widget *pWidget);


/// Get the size of the text in the currently selected font
FLTKEXT_DLL_EXPORT FXSize		fx_getTextSize(const SWString &text);

FLTKEXT_DLL_EXPORT int			AfxMessageBox(const SWString &text, int flags);
FLTKEXT_DLL_EXPORT int			FXHtmlMessageBox(const SWString &html, int flags);

/**
*** Get the minimum size of the given widget
***
*** This function determines the minimum size of any widget. We have this because
*** not all widgets are derived from Fl_Widget so we use the RTTI class info to
*** to determine the type of object and thus work out the minimum size according
*** to the object type. This is most often used by layout managers.
**/
FLTKEXT_DLL_EXPORT FXSize		fx_getMinSize(Fl_Widget *pWidget, bool assumeFixedSize=false);

/**
*** Get the maximum size of the given widget
***
*** This function determines the maximum size of any widget. We have this because
*** not all widgets are derived from Fl_Widget so we use the RTTI class info to
*** to determine the type of object and thus work out the maximum size according
*** to the object type. This is most often used by layout managers.
***
*** If a dimension is zero this implies that there is no maximum size for that
*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
*** the width is restricted to 1000 pixels, but the height is unrestricted.
**/
FLTKEXT_DLL_EXPORT FXSize		fx_getMaxSize(Fl_Widget *pWidget, bool assumeFixedSize=false);

/**
*** Get the sizes of the given widget
***
*** This function determines the current, minimum and maximum size of any widget.
*** We have this because not all widgets are derived from Fl_Widget so we use the RTTI class info to
*** to determine the type of object and thus work out the sizes according
*** to the object type. This is most often used by layout managers.
***
*** If a maximum dimension is zero this implies that there is no maximum size for that
*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
*** the width is restricted to 1000 pixels, but the height is unrestricted.
**/
FLTKEXT_DLL_EXPORT FXWidgetSizes		fx_getWidgetSizes(Fl_Widget *pWidget, bool assumeFixedSize=false);


/// Get the size of the borders around the given widget
FLTKEXT_DLL_EXPORT void					fx_getBorderWidths(Fl_Widget *pWidget, FXMargin &margins);


/// Get the rect of the contents area of the given widget
FLTKEXT_DLL_EXPORT void					fx_getContentsRect(Fl_Widget *pWidget, FXRect &rect);

/// Get the event name
FLTKEXT_DLL_EXPORT SWString				fx_event_name(int code);

/// Test if the specified key is pressed
FLTKEXT_DLL_EXPORT bool					fx_event_isKeyPressed(int code);

/// Set a window transparency
FLTKEXT_DLL_EXPORT void					fx_setWindowTransparency(Fl_Window *w, sw_byte_t alpha);

/// Add a resource folder
FLTKEXT_DLL_EXPORT void					fx_setAppConfig(SWAppConfig *pAppConfig);

FLTKEXT_DLL_EXPORT SWString				fx_getResourceFile(const SWString &filename);

#endif // __FltkExt_fx_functions_h__
