#ifndef __FltkExt_FXTestWnd_h__
#define __FltkExt_FXTestWnd_h__

#include <FltkExt/FltkExt.h>

#include <FL/Fl_Group.H>

/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXTestWnd : public Fl_Widget
		{
public:
		FXTestWnd(const SWString &text, int x=0, int y=0, int w=100, int h=100, int fgcolor=FL_WHITE, int bgcolor=FL_DARK1, int frcolor=FL_BLACK);
		virtual ~FXTestWnd();
		
		virtual void	draw();

protected:
		SWString	m_text;
		int			m_drawcount;
		int			m_bgColor;
		int			m_fgColor;
		int			m_frColor;
		};

#endif // __FltkExt_FXTestWnd_h__
