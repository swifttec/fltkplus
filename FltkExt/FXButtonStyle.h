/*
**	FXButtonStyle.h		A class representing the state of a window
*/

#ifndef __FltkExt_FXButtonStyle_h__
#define __FltkExt_FXButtonStyle_h__

#include <FltkExt/FXPoint.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXColor.h>

#include <FL/Fl_Shared_Image.H>


// A button display style
class FLTKEXT_DLL_EXPORT FXButtonStyle
		{
public:
		FXButtonStyle();
		FXButtonStyle(const FXColor &fgcolor, const FXColor &bgcolor, const FXColor &bdcolor=FXColor::NoColor, int bdstyle=1, int bdWidth=1, int cornerSize=8, const FXFont &font=FXFont("Arial", 10, FXFont::Normal));
		~FXButtonStyle();

		void				clearImages();
		void				setSharedImage(const SWString &filename);
		Fl_Shared_Image *	getSharedImage()	{ return m_pSharedImage; }
		Fl_Image *			getImage(Fl_Shared_Image *pImage, int iw, int ih, bool desaturate);

		FXFont			font;			// Font
		FXColor			fgColor;		// Foreground / text color
		FXColor			bgColor;		// Background color
		FXColor			bdColor;		// Border color
		int				bdStyle;
		int				bdWidth;
		int				bdCornerSize;

protected:
		Fl_Shared_Image	*m_pSharedImage;	// The image to draw (if any)
		Fl_Image		*m_pImage;		// The image to draw (if any)
		};

#endif // __FltkExt_FXButtonStyle_h__