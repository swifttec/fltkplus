/*
**	FXDialog.h	A generic dialog class
*/

#ifndef __FltkExt_FXDialog_h__
#define __FltkExt_FXDialog_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXWnd.h>
#include <FltkExt/FXDataExchange.h>

#include <swift/SWString.h>

#include <FL/Fl_Double_Window.H>

#define FX_BTN_STANDARD		0
#define FX_BTN_DEFAULT		1
#define FX_BTN_HOVER		2

class FLTKEXT_DLL_EXPORT FXDialog : public FXWnd
		{
public:
		FXDialog(const SWString &title, int W, int H, Fl_Widget *pOwner=NULL, const SWString &iconimage="");
		~FXDialog();

		virtual int		doModal();
		virtual void	doNonModal();
		virtual void	endDialog(int r);

		/// Called when the OK button is pressed - closes the dialog with result = FX_IDOK
		virtual void	onOK();

		/// Called when the Cancel button is pressed - closes the dialog with result = FX_IDCANCEL
		virtual void	onCancel();

		/// Called when a dialog value/item is changed to set the modified flag
		virtual void	setModified();

		/// Called just before the dialog is shown
		virtual void	onInitDialog();

		void			addItem(int itemid, Fl_Widget *pItem);
		Fl_Widget *		getItem(int itemid)												{ return fx_group_getItem(this, itemid); }
		void			enableItem(int itemid, bool v);

		void			setItemValue(int itemid, const SWString &v, int limitsize=0)	{ fx_group_setItemValue(this, itemid, v, limitsize); }
		void			setItemValue(int itemid, int v, int limitsize=0)				{ fx_group_setItemValue(this, itemid, v, limitsize); }
		void			setItemValue(int itemid, bool v)								{ fx_group_setItemValue(this, itemid, v); }

		void			getItemValue(int itemid, SWString &v)							{ fx_group_getItemValue(this, itemid, v); }
		void			getItemValue(int itemid, int &v)								{ fx_group_getItemValue(this, itemid, v); }
		void			getItemValue(int itemid, bool &v)								{ fx_group_getItemValue(this, itemid, v); }

		void			setItemFocus(int itemid);
		void			updateData(bool save=true);

public: // Easy build methods
		
		/// Adds a static text widget to the dialog
		Fl_Widget *		addStaticText(ulong id, const SWString &text, int X, int Y, int W=0, int H=0);
		
		/// Add a text input box to the dialog
		Fl_Widget *		addTextInput(ulong id, int X, int Y, int W=0, int H=0);
		
		/// Add a password input box to the dialog
		Fl_Widget *		addPasswordInput(ulong id, int X, int Y, int W=0, int H=0);
		
		/// Add a number input box to the dialog
		Fl_Widget *		addNumberInput(ulong id, int X, int Y, int W=0, int H=0);

		/// Add a button to the dialog
		Fl_Widget *		addButton(ulong id, const SWString &text, int type, int X, int Y, int W=0, int H=0);

protected:
		static void		dialogCallback(Fl_Widget *pWidget, void *pUserData);
		static void		itemCallback(Fl_Widget *pWidget, void *p);

		/// Called when a item issues a callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);
		
		/// Called when a button is clicked - return non-zero handled
		virtual int		onButtonClicked(Fl_Widget *pWidget);

		/// Called to exchange data between dialog items and the application when updateData is called
		virtual void	doDataExchange(FXDataExchange *pDX);

		virtual int		onShortcut(int code, const FXPoint &pt);

		virtual int		showDialog(bool modal);

protected:
		Fl_Widget		*m_pOwner;
		SWString		m_title;
		bool			m_modified;
		int				m_result;
		FXImage			*m_pBgImage;
		FXFont			m_font;
		FXImage			*m_pIconImage;
		};

#endif // __FltkExt_FXDialog_h__
