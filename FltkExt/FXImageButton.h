#ifndef __FltkExt_FXImageButton_h__
#define __FltkExt_FXImageButton_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXButton.h>

#include <FL/Fl_Button.H>


class FLTKEXT_DLL_EXPORT FXImageButton : public FXButton
		{
public:
		FXImageButton(const SWString &filename, int W, int H);
		FXImageButton(const SWString &filename, int X, int Y, int W, int H, bool stdbutton=false);
		FXImageButton(sw_uint32_t id, const SWString &filename, int W, int H);
		FXImageButton(sw_uint32_t id, const SWString &filename, int X, int Y, int W, int H, bool stdbutton=false);
		FXImageButton(const SWString &imagefolder, const SWString &imagebase, int W, int H);

protected:

		/// Called to draw the widget which by default does nothing
//		virtual void	onDraw();
		};

#endif // __FltkExt_FXImageButton_h__
