#ifndef __FltkExt_FXLayeredViewWidget_h__
#define __FltkExt_FXLayeredViewWidget_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXGroup.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

/**
*** A special group to manage the child widgets as panes in a window
**/
class FLTKEXT_DLL_EXPORT FXLayeredViewWidget : public FXWidget
		{
public:
		FXLayeredViewWidget();
		virtual ~FXLayeredViewWidget();

		/// Called when the view is shown or opened
		virtual void		onViewOpen();

		/// Called when the view is shown or opened
		virtual void		onViewClose();
		};

#endif // __FltkExt_FXLayeredViewWidget_h__
