#ifndef __FltkExt_FXToolBar_h__
#define __FltkExt_FXToolBar_h__

#include <FltkExt/FXSize.h>
#include <FltkExt/FXButton.h>
#include <FltkExt/FXMargin.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXScrollBar.h>
#include <FltkExt/fx_functions.h>

#include <swift/SWArray.h>

class FLTKEXT_DLL_EXPORT FXScrollableGroup : public FXGroup
		{
public:
		enum Flags
			{
			VScrollBar=1,
			HScrollBar=2
			};

public:
		/// Default constructor
		FXScrollableGroup(int flags);

		/// Constructor with width only
		FXScrollableGroup(int flags, int W, int H);

		/// Constructor with width and position
		FXScrollableGroup(int flags, int X, int Y, int W, int H);

		/// Constructor
		FXScrollableGroup(int flags, int X, int Y, int W, int H, const FXColor &bgcolor, const FXColor &bdcolor="#000", const FXColor &fgcolor="#000");

		/// Get the minimum size of this widget, primarily used by layout managers
		virtual FXSize	getMinSize() const;

		/// Get the maximum size of this widget, primarily used by layout managers
		virtual FXSize	getMaxSize() const;

		/// Get the contents rect for this widget
		virtual void	getContentsRect(FXRect &rect) const;

		virtual void	scrollToTop();

		virtual void	scrollToTopLeft();

protected: /// FLTK overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseWheel(int dx, int dy, const FXPoint &pt);

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);


protected:
		static void		genericCallback(Fl_Widget *pWidget, void *pUserData);
		static void		itemCallback(Fl_Widget *pWidget);
		virtual void	onCallback(Fl_Widget *pWidget);

		void			init(int flags);

		/// Save the scroll info from the horz and vert bars
		void			saveScrollInfo(FXScrollInfo &hi, FXScrollInfo &vi);
		
		/// Adjust the children position using the saved scroll info as a reference
		bool			adjustChildren(const FXScrollInfo &hi, const FXScrollInfo &vi);

protected:
		int					m_scrollflags;
		FXScrollBarObject	m_vScrollBar;
		bool				m_vTrackMouse;
		FXScrollBarObject	m_hScrollBar;
		bool				m_hTrackMouse;
		sw_uint32_t			m_idTimer;
		};

#endif // __FltkExt_FXToolBar_h__
