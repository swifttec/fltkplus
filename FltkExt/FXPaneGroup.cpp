#include "FXPaneGroup.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWFlags.h>

FXPaneGroup::FXPaneGroup(int X, int Y, int W, int H, bool vertical) :
	Fl_Group(X, Y, W, H),
	m_vertical(vertical)
		{
		}


FXPaneGroup::~FXPaneGroup()
		{
		}

/*
FXPaneGroup::Sizes &
FXPaneGroup::getChildSizes(int idx)
		{
		int		nsizes=(int)m_childSizes.size();

		while (idx >= nsizes)
			{
			Sizes	&cs=m_childSizes[nsizes++];

			cs.minsize = 0;
			cs.maxsize = 0;
			}

		return m_childSizes[idx];
		}


void
FXPaneGroup::setChildSizes(int idx, int minsize, int maxsize)
		{
		Sizes	&cs=getChildSizes(idx);

		cs.minsize = minsize;
		cs.maxsize = maxsize;
		}
*/


void
FXPaneGroup::getChildSizes(int idx, int &minsize, int &maxsize)
		{
		minsize = maxsize = 0;

		if (idx >= 0 && idx < children())
			{
			FXWidget	*pWidget=dynamic_cast<FXWidget *>(this->child(idx));

			if (pWidget != 0)
				{
				if (m_vertical)
					{
					minsize = pWidget->getMinSize().cy;
					maxsize = pWidget->getMaxSize().cy;
					}
				else
					{
					minsize = pWidget->getMinSize().cx;
					maxsize = pWidget->getMaxSize().cx;
					}
				}
			}
		}


void
FXPaneGroup::resize(int X, int Y, int W, int H)
		{
		int	nchildren=children();
 		int	*p = sizes(); // save initial sizes and positions
		int	minsize=0, maxsize=0, unlimited=0;

		for (int i=0;i<nchildren;i++)
			{
			int		childMinSize, childMaxSize;
			
			getChildSizes(i, childMinSize, childMaxSize);

			minsize += childMinSize;
			maxsize += childMaxSize;
			if (childMaxSize == 0) unlimited++;
			}

		if (m_vertical)
			{
			if (maxsize > 0 && H > maxsize && !unlimited) H = maxsize;
			else if (H < minsize) H = minsize;
			}
		else
			{
			if (maxsize > 0 && W > maxsize && !unlimited) W = maxsize;
			else if (W < minsize) W = minsize;
			}

		Fl_Widget::resize(X,Y,W,H); // make new xywh values visible for children

		if (nchildren > 0)
			{
			Fl_Widget	*const *a = array();
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalExtraSpace=0;

			a = array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				if (m_vertical) totalChildSpace += o->h();
				else totalChildSpace += o->w();
				}

			if (m_vertical) ds = H-totalChildSpace;
			else ds = W-totalChildSpace;

			int		canchange=0;
			SWFlags	changeflags;

			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				a = array();
				for (int i=0;i<nchildren;i++)
					{
					Fl_Widget	*o=*a++;
					int			width=o->w(), height=o->h();

					if (pass == 0 || changeflags.getBit(i))
						{
						int		childMinSize, childMaxSize;
			
						getChildSizes(i, childMinSize, childMaxSize);

						if (ds != 0)
							{
							if (pass == 0)
								{
								cds = ds / (nchildren-i);
								}
							else
								{
								cds = ds / canchange;
								}

							if (cds == 0)
								{
								if (ds < 0) cds = -1;
								else cds = 1;
								}
							}

						if (m_vertical)
							{
							// Fixed width (same as parent)
							width = W;

							// calculate new height
							int		old=height;

							height += cds;
							if (height < childMinSize) height = childMinSize;
							if (childMaxSize != 0 && height > childMaxSize) height = childMaxSize;
							else if (pass == 0 && (childMaxSize == 0 || height < childMaxSize))
								{
								canchange++;
								changeflags.setBit(i);
								}
							ds -= height-old;
							}
						else
							{
							// Fixed height (same as parent)
							height = H;

							// Variable width
							int		old=width;

							width += cds;
							if (width < childMinSize) width = childMinSize;
							if (childMaxSize != 0 && width > childMaxSize) width = childMaxSize;
							else if (pass == 0 && (childMaxSize == 0 || width < childMaxSize))
								{
								canchange++;
								changeflags.setBit(i);
								}
							ds -= width-old;
							}
					
						if (pass == 1) canchange--;
						}

					o->resize(X+xpos, Y+ypos, width, height);

					if (m_vertical) ypos += o->h();
					else xpos += o->w();
					}
				}
			}
		}
