/**
*** @file
*** @author		Simon Sparkes
*** @brief		Contains the definition of the FXSplitterBar class.
**/

/*********************************************************************************
Copyright (c) 2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/

#ifndef __FltkExt_FXSplitterBar_h__
#define __FltkExt_FXSplitterBar_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>

#include <FL/Fl_Group.H>

class FXSplitterWnd;

class FLTKEXT_DLL_EXPORT FXSplitterBar
		{
public:
		FXSplitterBar();
		FXSplitterBar(const FXSplitterBar &other);
		FXSplitterBar(bool vertical, int w=2, int p=2, const FXColor &fg=FXColor(200, 200, 200), const FXColor &bg=FX_COLOR_NONE);
		
		FXSplitterBar &	operator=(const FXSplitterBar &other);
		
		int			size() const	{ return ((2 * padding) + width); }

		void		draw();

public:
		bool		vertical;
		int			width;
		int			padding;
		FXRect		rect;
		FXColor		fgColor;
		FXColor		bgColor;
		};

#endif // __FltkExt_FXSplitterBar_h__
