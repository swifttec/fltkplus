/*
**	FXNamedDropdownList.h	A generic dialog class
*/

#ifndef __FltkExt_FXNamedDropdownList_h__
#define __FltkExt_FXNamedDropdownList_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXNamedWidget.h>
#include <FltkExt/FXDropdownList.h>


#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXNamedDropdownList : public FXNamedWidget
		{
public:
		FXNamedDropdownList(const SWString &name, int nameW, const SWString &value, int valueW, int H);
		FXNamedDropdownList(int X, int Y, const SWString &name, int nameW, const SWString &value, int valueW, int H);

		void		setChoices(const SWString &v);
		void		setChoices(const SWStringArray &sa);
		void		setChoices(const SWArray<FXDropdownList::Choice> &ca);

		void		addChoice(const FXDropdownList::Choice &v);
		void		addChoice(int id, const SWString &text, const FXColor &fgcolor="#000", const FXColor &bgcolor="#fff");

		int			value();
		void		value(int v);

		int			selected();

		virtual SWValue		getValue();

protected:
		void		init(const SWString &name, const SWString &value);
		};


#endif // __FltkExt_FXNamedDropdownList_h__
