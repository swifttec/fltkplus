#ifndef __FltkExt_FXTestWnd_h__
#define __FltkExt_FXTestWnd_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>

/*
** A test window - useful when doing initial window layouts
*/
class FLTKEXT_DLL_EXPORT FXTestWidget : public FXWidget
		{
public:
		FXTestWidget(const FXColor &bgcolor);
		FXTestWidget(const FXColor &bgcolor, const FXColor &fgcolor);
		FXTestWidget(int X, int Y, int W, int H, const FXColor &bgcolor);
		virtual ~FXTestWidget();
		
protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the widget gets the focus
		virtual int		onFocus();

		/// Called when the widget loses the focus
		virtual int		onUnfocus();

		/// Called when the mouse moves over the widget
		virtual int		onMouseEnter();

		/// Called when the mouse moves over the widget
		virtual int		onMouseLeave();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		/// Called when a mouse button is released
		virtual int		onMouseUp(int button, const FXPoint &pt);

		/// Called when the mouse is moved
		virtual int		onMouseMove(const FXPoint &pt);

		/// Called when the mouse is dragged with a button down
		virtual int		onMouseDrag(const FXPoint &pt);

protected:
		int			m_drawcount;
		};

#endif // __FltkExt_FXTestWnd_h__
