#include "FXDropdownList.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Box.H>

#include <swift/SWTokenizer.h>

FXDropdownList::FXDropdownList(int X, int Y, int W, int H) :
	Fl_Choice(X, Y, W, H)
		{
		}


void
FXDropdownList::setChoices(const SWString &v)
		{
		SWTokenizer		tok(v, "|");
		SWStringArray	sa;

		while (tok.hasMoreTokens()) sa.add(tok.nextToken());
		setChoices(sa);
		}


void
FXDropdownList::setChoices(const SWStringArray &sa)
		{
		SWArray<Choice>	clist;
		int						p;

		for (int i=0;i<(int)sa.size();i++)
			{
			SWString	s=sa.get(i);
			Choice	choice;

			// set defaults			
			choice.id = 0;
			choice.text = s;
			choice.fgcolor = "#000";
			choice.bgcolor = "#fff";

			// expect id=name;fg;bg
			if ((p = s.first('=')) >= 0)
				{
				choice.id = s.left(p).toInt32(10);
				s.remove(0, p+1);
				choice.text = s;
				}

			if ((p = s.first(';')) >= 0)
				{
				choice.text = s.left(p);
				if (choice.text.isEmpty()) choice.text = " ";

				s.remove(0, p+1);
				if (s.length() > 0) choice.fgcolor = s;
				}

			if ((p = s.first(';')) >= 0)
				{
				choice.fgcolor = s.left(p);
				s.remove(0, p+1);
				if (s.length() > 0) choice.bgcolor = s;
				}

			clist.add(choice);
			}

		setChoices(clist);
		}


void
FXDropdownList::setChoices(const SWArray<Choice> &ca)
		{
		m_choices = ca;

		clear();

		for (int i=0;i<(int)m_choices.size();i++)
			{
			add(m_choices[i].text, 0, NULL);
			}
		}


void
FXDropdownList::clearChoices()
		{
		m_choices.clear();
		clear();
		}


void
FXDropdownList::addChoice(const Choice &v)
		{
		m_choices.add(v);
		add(v.text, 0, NULL);
		}


void
FXDropdownList::addChoice(int id, const SWString &text, const FXColor &fgcolor, const FXColor &bgcolor)
		{
		Choice	v;

		v.id = id;
		v.text = text;
		v.fgcolor = fgcolor;
		v.bgcolor = bgcolor;

		addChoice(v);
		}

int
FXDropdownList::value()
		{
		int		res=-1;
		int		idx=Fl_Choice::value();

		if (idx >= 0 && idx < (int)m_choices.size())
			{
			res = m_choices[idx].id;
			}

		return res;
		}


int
FXDropdownList::selected()
		{
		return Fl_Choice::value();
		}


void
FXDropdownList::value(int v)
		{
		int		idx=-1;

		for (int i=0;i<(int)m_choices.size();i++)
			{
			if (m_choices[i].id == v)
				{
				idx = i;
				break;
				}
			}

		if (idx >= 0)
			{
			Fl_Choice::value(idx);
			}
		}

