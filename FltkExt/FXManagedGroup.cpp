#include "FXManagedGroup.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

FXManagedGroup::FXManagedGroup(int X, int Y, int W, int H, bool vertical) :
	Fl_Group(X, Y, W, H),
	m_vertical(vertical)
		{
//		resizable(*this);
		}


FXManagedGroup::~FXManagedGroup()
		{
		}


FXManagedGroup::Sizes &
FXManagedGroup::getChildSizes(int idx)
		{
		int		nsizes=(int)m_childSizes.size();

		while (idx >= nsizes)
			{
			Sizes	&cs=m_childSizes[nsizes++];

			cs.minsize = 0;
			cs.maxsize = 0;
			}

		return m_childSizes[idx];
		}


void
FXManagedGroup::setChildSizes(int idx, int minsize, int maxsize)
		{
		Sizes	&cs=getChildSizes(idx);

		cs.minsize = minsize;
		cs.maxsize = maxsize;
		}


void
FXManagedGroup::resize(int X, int Y, int W, int H)
		{
		int	nchildren=children();
 		int	*p = sizes(); // save initial sizes and positions
		int	minsize=0, maxsize=0, unlimited=0;

		for (int i=0;i<nchildren;i++)
			{
			Sizes		&cs=getChildSizes(i);

			minsize += cs.minsize;
			maxsize += cs.maxsize;
			if (cs.maxsize == 0) unlimited++;
			}

		if (m_vertical)
			{
			if (maxsize > 0 && H > maxsize && !unlimited) H = maxsize;
			else if (H < minsize) H = minsize;
			}
		else
			{
			if (maxsize > 0 && W > maxsize && !unlimited) W = maxsize;
			else if (W < minsize) W = minsize;
			}

		Fl_Widget::resize(X,Y,W,H); // make new xywh values visible for children



		if (nchildren > 0)
			{
			Fl_Widget	*const *a = array();
			int			ds=0;	// The overall size change
			int			cds=0;	// The overall size change per child
			int			totalChildSpace=0;
			int			totalExtraSpace=0;

			a = array();
			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				if (m_vertical) totalChildSpace += o->h();
				else totalChildSpace += o->w();
				}

			if (m_vertical) ds = H-totalChildSpace;
			else ds = W-totalChildSpace;

			int		canchange=0;
			SWFlags	changeflags;

			for (int pass=0;pass<2;pass++)
				{
				int			xpos=0, ypos=0;

				if (pass == 1 && (ds == 0 || !canchange))
					{
					// We have no further scope to change
					break;
					}

				a = array();
				for (int i=0;i<nchildren;i++)
					{
					Fl_Widget	*o=*a++;
					int			width=o->w(), height=o->h();

					if (pass == 0 || changeflags.getBit(i))
					{
					Sizes		&cs=getChildSizes(i);

					if (ds != 0)
						{
						if (pass == 0)
							{
							cds = ds / (nchildren-i);
							}
						else
							{
							cds = ds / canchange;
							}

						if (cds == 0)
							{
							if (ds < 0) cds = -1;
							else cds = 1;
							}
						}

					if (m_vertical)
						{
						// Fixed width (same as parent)
						width = W;

						// calculate new height
						int		old=height;

						height += cds;
						if (height < cs.minsize) height = cs.minsize;
						if (cs.maxsize != 0 && height > cs.maxsize) height = cs.maxsize;
						else if (pass == 0 && (cs.maxsize == 0 || height < cs.maxsize))
							{
							canchange++;
							changeflags.setBit(i);
							}
						ds -= height-old;
						}
					else
						{
						// Fixed height (same as parent)
						height = H;

						// Variable width
						int		old=width;

						width += cds;
						if (width < cs.minsize) width = cs.minsize;
						if (cs.maxsize != 0 && width > cs.maxsize) width = cs.maxsize;
						else if (pass == 0 && (cs.maxsize == 0 || width < cs.maxsize))
							{
							canchange++;
							changeflags.setBit(i);
							}
						ds -= width-old;
						}
					
					if (pass == 1) canchange--;
					}

					o->resize(X+xpos, Y+ypos, width, height);

					if (m_vertical) ypos += o->h();
					else xpos += o->w();
					}
				}
			}
		}


void
FXManagedGroup::draw()
		{
		Fl_Group::draw();

#ifdef PACK_CODE
		int			tx = x()+Fl::box_dx(box());
		int			ty = y()+Fl::box_dy(box());
		int			tw = w()-Fl::box_dw(box());
		int			th = h()-Fl::box_dh(box());
		int			rw, rh;
		int			current_position = horizontal() ? tx : ty;
		int			maximum_position = current_position;
		uchar		d = damage();
		Fl_Widget	*const* a = array();
		
		if (horizontal())
			{
			rw = -m_spacing;
			rh = th;
    
			for (int i = children(); i--;)
				{
				if (child(i)->visible())
					{
					if (child(i) != this->resizable()) rw += child(i)->w();
					rw += m_spacing;
					}
				}
			}
		else
			{
			rw = tw;
			rh = -m_spacing;
    
			for (int i = children(); i--;)
				{
				if (child(i)->visible())
					{
					if (child(i) != this->resizable()) rh += child(i)->h();
					rh += m_spacing;
					}
				}
			}
		
		for (int i = children(); i--;)
			{
			Fl_Widget	*o = *a++;

			if (o->visible())
				{
				int X,Y,W,H;
				if (horizontal())
					{
					X = current_position;
					W = o->w();
					Y = ty;
					H = th;
					}
				else
					{
					X = tx;
					W = tw;
					Y = current_position;
					H = o->h();
					}
				
				// Last child, if resizable, takes all remaining room
				if(i == 0 && o == this->resizable())
					{
					if(horizontal()) W = tw - rw;
					else H = th - rh;
					}
				
				if (m_spacing && current_position>maximum_position && box() && (X != o->x() || Y != o->y() || d&FL_DAMAGE_ALL))
					{
					fl_color(color());
					if (horizontal()) fl_rectf(maximum_position, ty, m_spacing, th);
					else fl_rectf(tx, maximum_position, tw, m_spacing);
					}
				
				if (X != o->x() || Y != o->y() || W != o->w() || H != o->h())
					{
					o->resize(X,Y,W,H);
					o->clear_damage(FL_DAMAGE_ALL);
					}
				
				if (d&FL_DAMAGE_ALL)
					{
					draw_child(*o);
					draw_outside_label(*o);
					}
				else
					{
					update_child(*o);
					}
				
				// child's draw() can change it's size, so use new size:
				current_position += (horizontal() ? o->w() : o->h());
				if (current_position > maximum_position) maximum_position = current_position;
				current_position += m_spacing;
				}
			}
  
		if (horizontal())
			{
			if (maximum_position < tx+tw && box())
				{
				fl_color(color());
				fl_rectf(maximum_position, ty, tx+tw-maximum_position, th);
				}
			tw = maximum_position-tx;
			}
		else
			{
			if (maximum_position < ty+th && box())
				{
				fl_color(color());
				fl_rectf(tx, maximum_position, tw, ty+th-maximum_position);
				}
			th = maximum_position-ty;
			}
  
		tw += Fl::box_dw(box());
		if (tw <= 0) tw = 1;
		
		th += Fl::box_dh(box());
		if (th <= 0) th = 1;
		
		if (tw != w() || th != h())
			{
			Fl_Widget::resize(x(),y(),tw,th);
			d = FL_DAMAGE_ALL;
			}
		
		if (d & FL_DAMAGE_ALL)
			{
			draw_box();
			draw_label();
			}
#endif
		}