#include "FXCheckBox.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Scrollbar.H>

#include <FltkExt/FXPanelLayout.h>


FXCheckBox::FXCheckBox(const SWString &text, int state, int X, int Y, int W, int H, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H)
		{
		setBackgroundColor(bgcolor);
		setForegroundColor(fgcolor);

		m_font = FXFont::getDefaultFont(FXFont::StaticText);
		setBorder(0, "#000", 0, 0);

		m_align = FX_VALIGN_CENTER|FX_HALIGN_LEFT;
		m_style = 0;
		m_state = 0;
		m_boxwidth = 1;
		m_visible = true;
		m_selected = false;
		m_text = text;
		m_state = state;
		}


FXCheckBox::FXCheckBox(int X, int Y, int W, int H, const SWString &text, int state, const FXColor &fgcolor, const FXColor &bgcolor) :
	FXWidget(X, Y, W, H)
		{
		setBackgroundColor(bgcolor);
		setForegroundColor(fgcolor);

		m_font = FXFont::getDefaultFont(FXFont::StaticText);
		setBorder(0, "#000", 0, 0);

		m_align = FX_VALIGN_CENTER|FX_HALIGN_LEFT;
		m_style = 0;
		m_state = 0;
		m_boxwidth = 1;
		m_visible = true;
		m_selected = false;
		m_text = text;
		m_state = state;
		}


void
FXCheckBox::update(const SWString &text, int state)
		{
		m_text = text;
		m_state = state;
		invalidate();
		}



void
FXCheckBox::update(const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor)
		{
		m_text = text;
		m_state = state;
		m_font = font;
		setForegroundColor(textcolor);
		m_boxcolor = boxcolor;
		}


int
FXCheckBox::getState()
		{
		return m_state;
		}


void
FXCheckBox::setState(int v)
		{
		m_state = v;
		invalidate();
		}

void
FXCheckBox::onDraw()
		{
		FXRect	cr;

		getContentsRect(cr);

		int		fh=m_font.height();

		if (m_selected)
			{
			fx_draw_fillRect(cr, "#bdf");
			}

		FXSize		textsize;
		SWString	text;

		if (!m_text.isEmpty())
			{
			text = " " + m_text;
			textsize = fx_getTextExtent(text);
			}

		m_rItem = cr;

		switch (m_align & FX_VALIGN_MASK)
			{
			case FX_VALIGN_TOP:
				m_rCheckbox.top = cr.top;
				m_rCheckbox.bottom = m_rCheckbox.top + fh - 1;
				break;

			case FX_VALIGN_BOTTOM:
				m_rCheckbox.bottom = cr.bottom;
				m_rCheckbox.top = m_rCheckbox.bottom - fh + 1;
				break;

			default:
				m_rCheckbox.top = cr.top + ((cr.height() - fh) / 2);
				m_rCheckbox.bottom = m_rCheckbox.top + fh;
				break;
			}

		switch (m_align & FX_HALIGN_MASK)
			{
			case FX_HALIGN_RIGHT:
				m_rCheckbox.right = cr.right;
				m_rCheckbox.left = m_rCheckbox.right - fh + 1;
				break;

			case FX_HALIGN_CENTER:
				m_rCheckbox.left = cr.left + ((cr.width() - fh - textsize.cx - 2) / 2);
				m_rCheckbox.right = m_rCheckbox.left + fh - 1;
				break;

			default:
				m_rCheckbox.left = cr.left;
				m_rCheckbox.right = m_rCheckbox.left + fh - 1;
				break;
			}


		FXColor	boxfillcolor="#fff";
		FXColor	fgcolor=getForegroundColor();
		FXColor	boxcolor=m_boxcolor;

		if (!active())
			{
			boxcolor = fgcolor = fgcolor.toGrayscale().toOpposite().toShade(0.5);
			}
		else
			{
			if (m_state < 0)
				{
				boxfillcolor = "#ccc";
				fgcolor = "#888";
				}
			else if (m_style == 1)
				{
				switch (m_state)
					{
					case FXCheckBox::ItemTicked:
						boxfillcolor = "#0f0";
						break;
	
					case FXCheckBox::ItemCrossed:
						boxfillcolor = "#f00";
						break;
					}
				}
			}

		fx_draw_box(m_rCheckbox, boxcolor, m_boxwidth, boxfillcolor, 0);

		if (m_style == 0)
			{
			switch (m_state)
				{
				case FXCheckBox::ItemTicked:
					drawTick(m_rCheckbox, fgcolor);
					break;

				case FXCheckBox::ItemCrossed:
					drawCross(m_rCheckbox, fgcolor);
					break;
				}
			}

		cr.left = m_rCheckbox.right + 2;

		fx_select(m_font);
		fx_select(fgcolor);
		fx_draw_text(text, cr, FX_HALIGN_LEFT|FX_VALIGN_CENTER);
		}


void
FXCheckBox::drawTick(const FXRect &dr, const FXColor &color)
		{
		FXRect	br(dr);

		br.DeflateRect(m_boxwidth * 2, m_boxwidth * 2, m_boxwidth * 2, m_boxwidth * 2);
		fx_draw_tick(br, color);
		}

void
FXCheckBox::drawCross(const FXRect &dr, const FXColor &color)
		{
		FXRect	br(dr);

		br.DeflateRect(m_boxwidth * 2, m_boxwidth * 2, m_boxwidth * 2, m_boxwidth * 2);
		fx_draw_cross(br, color);
		}


int
FXCheckBox::onMouseDown(int button, const FXPoint &pt)
		{
		if (button == FL_LEFT_MOUSE)
			{
			if (m_visible && m_rCheckbox.contains(pt))
				{
				if (m_state == ItemUnselected) m_state = ItemTicked;
				else if (m_state == ItemTicked) m_state = ItemUnselected;

				set_changed();
				invalidate();
				do_callback();
				}
			}

		return 0;
		}

