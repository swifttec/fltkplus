#include "FXTimePicker.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Input.H>

#include <swift/SWLocale.h>
#include <swift/SWTokenizer.h>

#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXDialog.h>
#include <FltkExt/FXCalendarPicker.h>
#include <FltkExt/fx_draw.h>

#define TITLEHEIGHT	12

class FXTimePopup : public FXWnd
		{
public:
		class Field
			{
		public:
			int		type;
			FXRect	rText;
			FXRect	rUp;
			FXRect	rDown;
			SWString	value;
			bool	selected;
			};

public:
		FXTimePopup(int X, int Y, int type, int t, Fl_Widget *pOwner);

		void			set(int h, int m, int s);
		void			get(int &h, int &m, int &s);

		int				hour() const	{ return ((m_time / 3600) % 24); }
		int				minute() const	{ return ((m_time / 60) % 60); }
		int				second() const	{ return (m_time % 60); }

		bool			wasTimeSelected() const		{ return m_selected; }
		int				getTimeSelected() const		{ return m_time; }
		int				runModalLoop();
		void			init(int t);

protected: /// FXWidget Override

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when the left mouse button is actioned (pressed, released) return non-zero to track
		virtual int		onLeftMouseButton(int action, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onShortcut(int code, const FXPoint &pt);

		/// Called to handle a shortcut
		virtual int		onUnfocus();

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);

protected:
		bool				m_selected;
		bool				m_done;
		int					m_type;			///< Selection type 0=HH:MM, 1=HH:MM:SS, 2=HH:00
		int					m_time;			///< The time in seconds since midnight 0 = 00:00:00
		FXFont				m_font;			///< The font to use
		SWArray<Field>		m_fields;
		FXColor				m_btnColor;
		FXColor				m_arrowColor;
		int					m_inputValue;
		int					m_mouseField;
		int					m_mouseDelay;
		};




FXTimePopup::FXTimePopup(int X, int Y, int type, int t, Fl_Widget *pOwner) :
	FXWnd("", 70, 54 + TITLEHEIGHT, true, pOwner),
	m_type(type),
	m_time(t),
	m_mouseField(0),
	m_mouseDelay(0)
		{
		clear_border();
		m_selected = false;
		init(t);
		
		setIntervalTimer(0, 0.1, NULL);

		position(X, Y);
		}


void
FXTimePopup::get(int &h, int &m, int &s)
		{
		h = hour();
		m = minute();
		s = second();
		}


void
FXTimePopup::set(int h, int m, int s)
		{
		if (m_type == FXTimePicker::HoursAndMinutes) s = 0;
		else if (m_type == FXTimePicker::HoursAndZeroMinutes)
			{
			s = m = 0;
			}

		m_time = ((h % 24) * 3600) + ((m % 60) * 60) + (s % 60);
		}


void
FXTimePopup::init(int t)
		{
		m_time = t;
		m_font = FXFont("Arial", 16, FXFont::Bold);
		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, "#444", 1, 0);
		m_btnColor = "#ccc";
		m_arrowColor = "#000";

		SWLocale		lc=SWLocale::getCurrentLocale();
		SWString		sep=lc.timeSeparator();
		SWString		fmt=lc.dateFormat();

		int	fpos=0;
		int	nfields=(m_type==FXTimePicker::HoursMinutesAndSeconds?3:2);

		for (int i=0;i<nfields;i++)
			{
			if (i > 0)
				{
				Field	&field=m_fields[fpos++];

				field.value = sep;
				field.type = 0;
				field.selected = false;
				}

			Field	&field=m_fields[fpos++];

			field.type = i+1;
			field.selected = false;
			}

		}



int
FXTimePopup::onShortcut(int code, const FXPoint &pt)
		{
		if (code == 27 || code == FL_Escape)
			{
			m_done = true;
			m_selected = false;
			}
		
		return 0;
		}

int
FXTimePopup::onUnfocus()
		{
		return 1;
		}


int
FXTimePopup::runModalLoop()
		{
		m_done = false;
		// fx_widget_centerOver(this, m_pOwner);

		set_modal();
		show();

		m_modalResult = 0;
		
		take_focus();

		while (shown() && !m_done)
			{
			Fl::wait();
			}
		
		set_non_modal();

		return m_modalResult;
		}


void
FXTimePopup::onDraw()
		{
		take_focus();

		FXRect	cr, dr;
		SWString	s;

		getClientRect(cr);

		dr = cr;
		dr.bottom = dr.top + TITLEHEIGHT - 1;
		dr.right = dr.right - TITLEHEIGHT;
		fx_draw_fillRect(dr, "#08f");
		fx_draw_text("Select Time", dr, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, FXFont("Arial", TITLEHEIGHT-2, FXFont::Normal), "#fff");

		dr = cr;
		dr.bottom = dr.top + TITLEHEIGHT - 1;
		dr.left = dr.right - TITLEHEIGHT - 1;
		fx_draw_fillRect(dr, "#f00");
		fx_draw_text("X", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", TITLEHEIGHT, FXFont::Bold), "#fff");

		fx_select(m_font);
	
		int		xpos=cr.left+3;
		int		ypos=cr.top+1+TITLEHEIGHT;
		int		H=18;
		int		totalwidth=0;

		int		width=0;
		
		for (int i=10;i<60;i++)
			{
			width = sw_math_max(width, fx_getTextExtent(SWString::valueOf(i)).cx);
			}

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			switch (field.type)
				{
				case 1:	field.value.format("%02d", hour());		break;
				case 2:	field.value.format("%02d", minute());	break;
				case 3:	field.value.format("%02d", second());	break;
				}

			int		btnwidth=12;
			int		fieldwidth;

			if (field.type == 0)
				{
				fieldwidth = fx_getTextExtent(field.value).cx;
				}
			else
				{
				fieldwidth = width;
				}

			ypos = TITLEHEIGHT + 4;

			field.rUp.left = xpos + ((width-btnwidth) / 2);
			field.rUp.right = field.rUp.left + btnwidth - 1;
			field.rUp.top = ypos;
			field.rUp.bottom = field.rUp.top + btnwidth - 1;

			ypos += btnwidth + 2;
			field.rText.left = xpos;
			field.rText.right = field.rText.left + fieldwidth - 1;
			field.rText.top = ypos;
			field.rText.bottom = field.rText.top + H - 1;

			ypos += 20;
			field.rDown.left = xpos + ((width-btnwidth) / 2);
			field.rDown.right = field.rDown.left + btnwidth - 1;
			field.rDown.top = ypos;
			field.rDown.bottom = field.rDown.top + btnwidth - 1;

			xpos += fieldwidth + 1;
			totalwidth += fieldwidth;
			}

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			fx_draw_text(field.value, field.rText, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, getForegroundColor());

			if (field.type != 0 && !(field.type == 2 && m_type == FXTimePicker::HoursAndZeroMinutes))
				{
				fx_draw_arrowButton(field.rUp, FX_ARROW_UP, m_btnColor, m_arrowColor, false);
				fx_draw_arrowButton(field.rDown, FX_ARROW_DOWN, m_btnColor, m_arrowColor, false);
				}
			}
		
		}

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
bool
FXTimePopup::onTimer(sw_uint32_t id, void *data)
		{
		if (m_mouseField != 0)
			{
			if (m_mouseDelay > 0) m_mouseDelay--;
			else
				{
				int		h=hour(), m=minute(), s=second();

				switch (m_mouseField)
					{
					case 3:
						s = ((s + 1) % 60);
						if (s != 0) break;

					case 2:
						m = ((m + 1) % 60);
						if (m != 0) break;

					case 1:
						h = ((h + 1) % 24);
						break;

					case -3:
						s = ((s + 59) % 60);
						if (s != 59) break;

					case -2:
						m = ((m + 59) % 60);
						if (m != 59) break;

					case -1:
						h = ((h + 23) % 24);
						break;
					}

				set(h, m, s);
				invalidate();
				fx_widget_getWindow(this)->redraw();
				}
			}

		return true;
		}

int
FXTimePopup::onLeftMouseButton(int action, const FXPoint &pt)
		{
		int		res=0;

		if (action == FX_BUTTON_DOWN || action == FX_BUTTON_DOUBLE_CLICK)
			{
			FXRect	dr;

			getClientRect(dr);
			dr.bottom = dr.top + TITLEHEIGHT - 1;
			dr.left = dr.right - TITLEHEIGHT - 1;

			if (dr.contains(pt))
				{
				m_done = true;
				m_selected = false;
				do_callback();
				}
			else
				{
				for (int i=0;i<(int)m_fields.size();i++)
					{
					Field	&field=m_fields[i];

					if (field.type != 0 && !(field.type == 2 && m_type == FXTimePicker::HoursAndZeroMinutes))
						{
						int		h=hour(), m=minute(), s=second();

						if (field.rUp.contains(pt))
							{
							m_mouseDelay = 5;
							m_mouseField = field.type;
							switch (field.type)
								{
								case 3:
									s = ((s + 1) % 60);
									if (s != 0) break;

								case 2:
									m = ((m + 1) % 60);
									if (m != 0) break;

								case 1:
									h = ((h + 1) % 24);
									break;
								}
							}
						else if (field.rDown.contains(pt))
							{
							m_mouseDelay = 5;
							m_mouseField = -field.type;
							switch (field.type)
								{
								case 3:
									s = ((s + 59) % 60);
									if (s != 59) break;

								case 2:
									m = ((m + 59) % 60);
									if (m != 59) break;

								case 1:
									h = ((h + 23) % 24);
									break;
								}
							}

						set(h, m, s);
						invalidate();
						fx_widget_getWindow(this)->redraw();
						res = 1;
						}
					}
				}
			}
		else
			{
			m_mouseField = 0;
			}

		return res;
		}
		

FXTimePicker::FXTimePicker(int X, int Y, int W, int H, int type) :
	FXWidget(X, Y, W, H),
	m_type(type),
	m_time(0),
	m_btnColor("#ccc"),
	m_arrowColor("#000")
		{
		init(0);
		}


void
FXTimePicker::init(int t)
		{
		m_time = t;
		m_font = FXFont::getDefaultFont(FXFont::StaticText);
		setBackgroundColor("#fff");
		setForegroundColor("#000");
		setBorder(0, "#444", 1, 0);
		m_selectedBgColor = "#008";
		m_selectedFgColor = "#fff";

		SWLocale		lc=SWLocale::getCurrentLocale();
		SWString		sep=lc.timeSeparator();
		SWString		fmt=lc.dateFormat();

		fx_select(m_font);

		int	fpos=0;
		int	nfields=(m_type==HoursMinutesAndSeconds?3:2);

		for (int i=0;i<nfields;i++)
			{
			if (i > 0)
				{
				Field	&field=m_fields[fpos++];

				field.value = sep;
				field.type = 0;
				field.selected = false;
				}

			Field	&field=m_fields[fpos++];

			field.type = i+1;
			field.selected = false;
			}

		}


void
FXTimePicker::set(const SWTime &t)
		{
		set(t.hour(), t.minute(), t.second());
		}


void
FXTimePicker::set(int h, int m, int s)
		{
		if (m_type == HoursAndMinutes) s = 0;
		else if (m_type == HoursAndZeroMinutes)
			{
			m = s = 0;
			}

		m_time = ((h % 24) * 3600) + ((m % 60) * 60) + (s % 60);
		}


void
FXTimePicker::get(int &h, int &m, int &s)
		{
		h = hour();
		m = minute();
		s = second();
		}


void
FXTimePicker::onDraw()
		{
		FXRect	cr;
		SWString	s;

		getClientRect(cr);

		fx_select(m_font);
	
		int		xpos=x()+1;
		int		ypos=y()+1;
		int		H=h()-2;
		int		totalwidth=0;

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			switch (field.type)
				{
				case 1:	field.value.format("%02d", hour());	break;
				case 2:	field.value.format("%02d", minute());	break;
				case 3:	field.value.format("%02d", second());	break;
				}

			int		width=fx_getTextExtent(field.value).cx;

			field.rect.left = xpos;
			field.rect.right = xpos + width - 1;
			field.rect.top = ypos;
			field.rect.bottom = ypos + H - 1;

			xpos += width + 1;
			totalwidth += width;
			}

		m_rButton.top = cr.top + 1;
		m_rButton.bottom = cr.bottom - 1;
		m_rButton.right = cr.right - 1;
		m_rButton.left = m_rButton.right - h() + 2;

		int		available=m_rButton.left - cr.left - totalwidth - 3;

		if (available > 0)
			{
			int		pos=0;
			int		xadj=0;

			while (pos < (int)m_fields.size() && available > 0)
				{
				int		extra=available / ((int)m_fields.size() - pos);
				Field	&field=m_fields[pos];

				field.rect.left += xadj;
				field.rect.right += xadj + extra;
				xadj += extra;
				available -= extra;
				pos++;
				}
			}

		for (int i=0;i<(int)m_fields.size();i++)
			{
			Field	&field=m_fields[i];

			if (field.type != 0 && field.selected)
				{
				fx_draw_fillRect(field.rect, m_selectedBgColor);
				fx_draw_text(field.value, field.rect, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, m_selectedFgColor);
				}
			else
				{
				fx_draw_text(field.value, field.rect, FX_ALIGN_MIDDLE_CENTRE | FX_SHRINKTOFIT, getForegroundColor());
				}
			}
		
		fx_draw_arrowButton(m_rButton, FX_ARROW_DOWN, m_btnColor, m_arrowColor, false);
		}


int
FXTimePicker::onLeftMouseButton(int action, const FXPoint &pt)
		{
		if (action == FX_BUTTON_DOWN)
			{
			Fl::focus(this);

			m_selectedField = -1;

			for (int i=0;i<(int)m_fields.size();i++)
				{
				Field	&field=m_fields[i];

				if (field.type != 0 && field.rect.contains(pt))
					{
					field.selected = true;
					m_selectedField = i;
					}
				else
					{
					field.selected = false;
					}
				}

			if (m_rButton.contains(pt))
				{
				int		xpos, ypos;

				fx_widget_getScreenCoordinates(this, xpos, ypos);

				FXTimePopup	dlg(xpos, ypos, m_type, m_time, this);

				dlg.runModalLoop();
				m_time = dlg.getTimeSelected();
				}
			}

		invalidate();
		fx_widget_getWindow(this)->redraw();

		return 0;
		}


int
FXTimePicker::onFocus()
		{
		if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

		if ((Fl::event_state() & FL_SHIFT) != 0) m_selectedField = (int)m_fields.size() - 1;
		else m_selectedField = 0;
						
		if (m_selectedField >= 0 && m_selectedField < (int)m_fields.size())
			{
			m_fields[m_selectedField].selected = true;
			}
		
		invalidate();
		fx_widget_getWindow(this)->redraw();
		
		Fl::focus(this);

		return 1;
		}


int
FXTimePicker::onUnfocus()
		{
		if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;
		m_selectedField = -1;
		invalidate();
		fx_widget_getWindow(this)->redraw();

		return 0;
		}

		

int
FXTimePicker::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		return handleKey(code, text, pt);
		}


int
FXTimePicker::onShortcut(int code, const FXPoint &pt)
		{
		return handleKey(code, "", pt);
		}


int
FXTimePicker::handleKey(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (code >= FL_KP && code <= FL_KP_Last && text.length() == 1)
			{
			code = text.getCharAt(0);
			}

		if (Fl::focus() == this)
			{
			if (m_selectedField >= 0 && code >= '0' && code <= '9')
				{
				Field	&field=m_fields[m_selectedField];
				int		n;

				switch (field.type)
					{
					case 1:	// Hour
						n = hour();
						n = (n * 10) + code - '0';
						if (n > 23) n = code - '0';
						set(n, minute(), second());
						break;

					case 2: // Minute
						n = minute();
						n = (n * 10) + code - '0';
						if (n > 59) n = code - '0';
						set(hour(), n, second());
						break;

					case 3: /// Second
						n = second();
						n = (n * 10) + code - '0';
						if (n > 59) n = code - '0';
						set(hour(), minute(), n);
						break;
					}
			
				res = 1;
				}
			else if (code == '\t' || code == FL_Tab || code == FL_Right)
				{
				if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

				if (m_fields.size() > 0)
					{
					bool	done=false;

					while (!done)
						{
						if ((Fl::event_state() & FL_SHIFT) != 0) m_selectedField--;
						else m_selectedField++;
						
						if (m_selectedField < 0 || m_selectedField >= (int)m_fields.size())
							{
							done = true;
							m_selectedField = -1;
							res = 0;
							}
						
						if (m_selectedField >= 0 && m_fields[m_selectedField].type != 0)
							{
							m_fields[m_selectedField].selected = true;
							done = true;
							res = 1;
							}
						}
					}

				}
			else if (code == FL_Left)
				{
				if (m_selectedField >= 0) m_fields[m_selectedField].selected = false;

				if (m_fields.size() > 0)
					{
					bool	done=false;

					while (!done)
						{
						m_selectedField--;
						if (m_selectedField < 0)
							{
							m_selectedField = (int)m_fields.size()-1;
							}

						if (m_fields[m_selectedField].type != 0)
							{
							m_fields[m_selectedField].selected = true;
							done = true;
							res = 1;
							}
						}
					}

				}

			invalidate();
			fx_widget_getWindow(this)->redraw();
			}

		return res;
		}

