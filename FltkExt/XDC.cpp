#include <FltkExt/XDC.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/fx_draw.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#ifdef USE_XDC

static SWArray<XDC>	dcContextList;

XDC &
XDC::getDC(int context)
		{
		if (context < 0 || context > MaxContext) context = 0;

		return dcContextList[context];
		}


XDCInfo::XDCInfo()
		{
		fgColor = FXColor(0, 0, 0);				// The current fg/text color
		bgColor = FXColor(255, 255, 255);		// The current bg color
		bgMode = FX_BG_OPAQUE;					// The current bg mode (opaque or transparent)
		shadowColor = FXColor(200, 200, 200);	// The current shadow color
		shadowXOffset = 2;						// The current shadow X offset
		shadowYOffset = 2;						// The current shadow Y offset
		outlineColor = FXColor(0, 0, 0);		// The current outline color
		outlineWidth = 1;						// The current outline width
		point.set(0, 0);						// The current point (used in drawing operations)
		}


XDC::XDC()
		{
		}


XDC::~XDC()
		{
		}


void
XDC::draw(const SWString &text, FXRect &dr, sw_uint32_t flags)
		{
		drawText(text, dr, flags);
		}


void
XDC::drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXColorFont &srcfont, double minsize, double *pSizeUsed)
		{
		drawText(text, dr, flags, srcfont, srcfont.color(), minsize, pSizeUsed);
		}


void
XDC::drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXFont &srcfont, const FXColor &textcolor, double minsize, double *pSizeUsed)
		{
		FXFont	font(srcfont);

		if ((flags & XD_SHRINKTOFIT) != 0)
			{
			bool	ok=false;
			FXSize	sz;
			int		width=dr.Width();
			int		height=dr.Height();
			double	ptsize=font.pointsize();

			if ((flags & XD_SHADOW) != 0)
				{
				width -= m_info.shadowXOffset;
				height -= m_info.shadowYOffset;
				}

			while (!ok)
				{
				select(font);
				sz = getTextExtent(text, width, flags);
				if (sz.cy > height || sz.cx > width)
					{
					if (ptsize > minsize)
						{
						ptsize -= 1.0;
						font.pointsize(ptsize);
						}
					else
						{
						ok = true;
						}
					}
				else
					{
					ok = true;
					}
				}
			
			if (pSizeUsed != NULL) *pSizeUsed = ptsize;
			}
		else
			{
			select(font);
			}

		setTextColor(textcolor);
		drawText(text, dr, flags);
		}


void
XDC::drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags)
		{
		XDCSave		save(this);
		FXTextStyle	style;

		style.font = m_info.font;
		style.flags = flags;
		style.fgColor = m_info.fgColor;
		style.bgColor = m_info.bgColor;
		style.shadowColor = m_info.shadowColor;
		style.shadowXOffset = m_info.shadowXOffset;
		style.shadowYOffset = m_info.shadowYOffset;
		style.outlineColor = m_info.outlineColor;
		style.outlineWidth = m_info.outlineWidth;

		fx_draw_text(text, dr, style);
		}

int
XDC::getTextHeight(const SWString &text, int width, sw_uint32_t flags) const
		{
		return getTextExtent(text, width, flags).cy;
		}


FXSize
XDC::getTextExtent(const SWString &text, int width, sw_uint32_t flags) const
		{
		FXSize	sz(0, 0);

		if (text.length())
			{
			if ((flags & FX_SHADOW) != 0)
				{
				width -= m_info.shadowXOffset;
				}

			sz.cx = width;
			sz.cy = 0;
			fl_font(m_info.font.fl_face(), m_info.font.height());
			fl_measure(text, sz.cx, sz.cy);

			if ((flags & FX_SHADOW) != 0)
				{
				sz.cx += m_info.shadowXOffset;
				sz.cy += m_info.shadowYOffset;
				}
			}

		return sz;
		}


FXSize
XDC::getTextExtent(const SWString &text) const
		{
		FXSize	sz(0, 0);

		fl_font(m_info.font.fl_face(), m_info.font.height());
		fl_measure(text, sz.cx, sz.cy);

		return sz;
		}


void
XDC::drawBox(const FXRect &br, const FXColor &borderColor, int borderWidth, const FXColor &bgcolor, int cornerDepth)
		{
		fx_draw_rect(br, borderWidth, cornerDepth, borderColor, bgcolor);
/*
		FXRect		cr(br);	

		if (bdwidth < 0) bdwidth = 0;
		if (bdcolor == FX_COLOR_NONE) bdwidth = 0;

		// Adjust the box according to the border width
		int		badj1=bdwidth/2;
		int		badj2=bdwidth-badj1;

		if (bdwidth > 0 && badj1 == 0) badj1 = 1;
		cr.deflate(badj1, badj1, badj2, badj2);

		FXPen	oldpen=m_info.pen;
		FXPen	pen;
		FXPen	nullpen(FX_PEN_NULL, 1, RGB(0, 0, 0));
		FXBrush	oldbrush=m_info.brush;
		FXBrush	brush(bgcolor);

		if (bdwidth > 0 && bdcolor != FX_COLOR_NONE)
			{
			pen.set(FX_PEN_SOLID, bdwidth, bdcolor);
			}
		else
			{
			pen.set(FX_PEN_NULL, 1, RGB(0, 0, 0));
			}

		select(pen);
		select(brush);

		if (cornersize <= 0)
			{
			if (bgcolor != FX_COLOR_NONE)
				{
				fillRect(cr, bgcolor);
				}
				
			if (bdwidth > 0)
				{
				bdcolor.select();
				moveTo(cr.left, cr.top);
				lineTo(cr.right, cr.top);
				lineTo(cr.right, cr.bottom);
				lineTo(cr.left, cr.bottom);
				lineTo(cr.left, cr.top);
				}
			}
		else
			{
			FXRect	dr;
			FXRect	ir(cr);
			int		dfl=cornersize; //+(bdwidth/2);

			ir.DeflateRect(dfl, dfl, dfl, dfl);

			if (bgcolor != FX_COLOR_NONE)
				{
				fillRect(ir, bgcolor);
				}

			// Top left
			dr.top = cr.top;
			dr.left = cr.left;
			dr.bottom = dr.top + (2 * cornersize);
			dr.right = dr.left + (2 * cornersize);
			if (bgcolor != FX_COLOR_NONE)
				{
				select(nullpen);
				ellipse(dr, 0, 90);
				select(pen);
				}

			arc(dr, FXPoint(dr.left + cornersize, dr.top), FXPoint(dr.left, dr.top + cornersize));

			// Top Right
			dr.top = cr.top;
			dr.right = cr.right;
			dr.bottom = dr.top + (2 * cornersize);
			dr.left = dr.right - (2 * cornersize);
			if (bgcolor != FX_COLOR_NONE)
				{
				select(nullpen);
				ellipse(dr, 90, 180);
				select(pen);
				}
			
			arc(dr, FXPoint(dr.right, dr.top + cornersize), FXPoint(dr.left + cornersize, dr.top));


			// Bottom Right
			dr.bottom = cr.bottom;
			dr.right = cr.right;
			dr.top = dr.bottom - (2 * cornersize);
			dr.left = dr.right - (2 * cornersize);
			if (bgcolor != FX_COLOR_NONE)
				{
				select(nullpen);
				ellipse(dr, 180, 270);
				select(pen);
				}
			arc(dr, FXPoint(dr.left + cornersize, dr.bottom), FXPoint(dr.right, dr.bottom - cornersize));

			// Bottom Left
			dr.bottom = cr.bottom;
			dr.left = cr.left;
			dr.top = dr.bottom - (2 * cornersize);
			dr.right = dr.left + (2 * cornersize);
			if (bgcolor != FX_COLOR_NONE)
				{
				select(nullpen);
				ellipse(dr, 270, 360);
				select(pen);
				}
				
			arc(dr, FXPoint(dr.left, dr.bottom - cornersize), FXPoint(dr.left + cornersize, dr.bottom));

			// Top
			if (bgcolor != FX_COLOR_NONE)
				{
				dr.left = ir.left;
				dr.right = ir.right;
				dr.top = cr.top;
				dr.bottom = ir.top;
				fillRect(dr, bgcolor);
				}
			moveTo(cr.left + cornersize, cr.top);
			lineTo(cr.right - cornersize, cr.top);

			// Right
			if (bgcolor != FX_COLOR_NONE)
				{
				dr.left = ir.right;
				dr.right = cr.right;
				dr.top = ir.top;
				dr.bottom = ir.bottom;
				fillRect(dr, bgcolor);
				}
			moveTo(cr.right, cr.top + cornersize);
			lineTo(cr.right, cr.bottom - cornersize);

			// Bottom
			if (bgcolor != FX_COLOR_NONE)
				{
				dr.left = ir.left;
				dr.right = ir.right;
				dr.top = ir.bottom;
				dr.bottom = cr.bottom;
				fillRect(dr, bgcolor);
				}
			moveTo(cr.left + cornersize, cr.bottom);
			lineTo(cr.right - cornersize, cr.bottom);

			// Left
			if (bgcolor != FX_COLOR_NONE)
				{
				dr.left = cr.left;
				dr.right = ir.left;
				dr.top = ir.top;
				dr.bottom = ir.bottom;
				fillRect(dr, bgcolor);
				}
			moveTo(cr.left, cr.top + cornersize);
			lineTo(cr.left, cr.bottom - cornersize);
			}

#ifdef DRAW_TEST_BOX
		cr = br;
		cr.DeflateRect(0, 0, 1, 1);
		
		FXFont	font("Arial", 7.0);

		font.select(dc);
		SWString	s;

		s.format("Border Width = %d, Corner Size = %d", bdwidth, cornersize);
		draw(s, cr, XD_MIDDLE|XD_CENTER);

		FXPen	bpen(FX_PEN_SOLID, 1, RGB(0, 0, 0));

		pDC->SelectObject(&bpen);

		MoveTo(cr.left, cr.top-100);
		LineTo(cr.left, cr.top+100);
		MoveTo(cr.left-100, cr.top);
		LineTo(cr.left+100, cr.top);

		MoveTo(cr.right, cr.top-100);
		LineTo(cr.right, cr.top+100);
		MoveTo(cr.right-100, cr.top);
		LineTo(cr.right+100, cr.top);

		MoveTo(cr.left, cr.bottom-100);
		LineTo(cr.left, cr.bottom+100);
		MoveTo(cr.left-100, cr.bottom);
		LineTo(cr.left+100, cr.bottom);

		MoveTo(cr.right, cr.bottom-100);
		LineTo(cr.right, cr.bottom+100);
		MoveTo(cr.right-100, cr.bottom);
		LineTo(cr.right+100, cr.bottom);
#endif // DRAW_TEST_BOX

		select(oldpen);
		select(oldbrush);
*/
		}


/*
FXPen *
XDC::SelectObject(FXPen *pObject)
		{
		return m_pDC->SelectObject(pObject);
		}


FXBrush *
XDC::SelectObject(FXBrush *pObject)
		{
		return m_pDC->SelectObject(pObject);
		}


FXSize
XDC::GetTextExtent(const SWString &str) const
		{
		FXSize	sz(0, 0);

		if (str.length())
			{
			sz = m_pDC->GetTextExtent(str);
			}

		return sz;
		}


void
XDC::FillRect(LPCRECT lpRect, FXColor color)
		{
		FXBrush	brush(color);

		m_pDC->FillRect(lpRect, &brush);
		}


void
XDC::FrameRect(LPCRECT lpRect, FXColor color)
		{
		FXBrush	brush(color);

		m_pDC->FrameRect(lpRect, &brush);
		}

void		XDC::FillRect(LPCRECT lpRect, FXBrush* pBrush) 	{ m_pDC->FillRect(lpRect, pBrush); }
void		XDC::FrameRect(LPCRECT lpRect, FXBrush* pBrush) 	{ m_pDC->FrameRect(lpRect, pBrush); }
CPoint		XDC::MoveTo(int x, int y)						{ return m_pDC->MoveTo(x, y); }
CPoint		XDC::MoveTo(POINT point)						{ return m_pDC->MoveTo(point); }
BOOL		XDC::LineTo(int x, int y)						{ return m_pDC->LineTo(x, y); }
BOOL		XDC::LineTo(POINT point)						{ return m_pDC->LineTo(point); }
void		XDC::GetTextMetrics(TEXTMETRIC *pTM)			{ m_pDC->GetTextMetrics(pTM); }
*/


void		XDC::setOutlineColor(const FXColor &v)			{ m_info.outlineColor = v; }
void		XDC::setOutlineWidth(int v)						{ m_info.outlineWidth = v; }
void		XDC::setShadowColor(const FXColor & v)			{ m_info.shadowColor = v; }
void		XDC::setShadowOffset(int v)						{ m_info.shadowXOffset = m_info.shadowYOffset = v; }
void		XDC::setShadowOffset(int x, int y)				{ m_info.shadowXOffset = x; m_info.shadowYOffset = y; }
void		XDC::setTextColor(const FXColor &v)				{ m_info.fgColor = v; }
void		XDC::setBgColor(const FXColor &v)				{ m_info.bgColor = v; }

const XDCInfo &
XDC::saveDC()
		{
		return m_info;
		}


void
XDC::restoreDC(const XDCInfo &v)
		{
		m_info = v;
		}



void
XDC::fillRect(const FXRect &dr, const FXColor &color)
		{
		int		r, g, b, a;

		color.getComponents(8, r, g, b, a);

		fl_rectf(dr.left, dr.top, dr.width(), dr.height(), r, g, b);
		}


		
void
XDC::FillSolidRect(FXRect *pr, const FXColor &color)
		{
		int		r, g, b, a;

		color.getComponents(8, r, g, b, a);

		fl_rectf(pr->left, pr->top, pr->width(), pr->height(), r, g, b);
		}

		
void
XDC::FillRect(FXRect *pr, FXBrush *pBrush)
		{
		int		r, g, b, a;

		pBrush->color.getComponents(8, r, g, b, a);

		fl_rectf(pr->left, pr->top, pr->width(), pr->height(), r, g, b);
		}

void
XDC::fillRect(const FXRect &dr, const FXBrush &brush)
		{
		int		r, g, b, a;

		brush.color.getComponents(8, r, g, b, a);

		fl_rectf(dr.left, dr.top, dr.width(), dr.height(), r, g, b);
		}


void
XDC::frameRect(const FXRect &dr, const FXColor &color)
		{
		select(color);
		fl_rect(dr.left, dr.top, dr.width(), dr.height());
		}


void
XDC::frameRect(const FXRect &dr, const FXBrush &brush)
		{
		select(brush.color);
		fl_rect(dr.left, dr.top, dr.width(), dr.height());
		}


		
void
XDC::FrameRect(FXRect *pr, FXBrush *pBrush)
		{
		select(pBrush->color);
		fl_rect(pr->left, pr->top, pr->width(), pr->height(), pBrush->color.to_fl_color());
		}


void
XDC::select(const FXColor &color)
		{
		int		r, g, b, a;

		color.getComponents(8, r, g, b, a);

		m_info.fgColor = color;
		fl_color(r, g, b);
		}


void
XDC::select(const FXFont &font)
		{
		m_info.font = font;
		fl_font(font.fl_face(), font.height());
		fl_color(FL_BLACK);
		}

void
XDC::select(const FXPen &pen)
		{
		m_info.pen = pen;
		}


void
XDC::select(const FXBrush &brush)
		{
		m_info.brush = brush;
		}


void
XDC::setBgMode(int mode)
		{
		m_info.bgMode = mode;
		}



int
XDC::getBgMode() const
		{
		return m_info.bgMode;
		}


void
XDC::SetBkMode(int v)
		{
		// Do nothin
		}


void
XDC::SelectObject(const FXFont *font)
		{
		select(*font);
		}


void
XDC::SelectObject(const FXPen *pen)
		{
		select(*pen);
		}



void
XDC::SelectObject(const FXBrush *brush)
		{
		select(*brush);
		}


void
XDC::SelectObject(const FXColor &color)
		{
		select(color);
		}


#endif // USE_XDC
