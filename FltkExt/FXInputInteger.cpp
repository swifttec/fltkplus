#include "FXInputInteger.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

FXInputInteger::FXInputInteger(int X, int Y, int W, int H) :
	Fl_Int_Input(X, Y, W, H)
		{
		when(FL_WHEN_CHANGED);
		}


FXInputInteger::FXInputInteger(int W, int H) :
	Fl_Int_Input(0, 0, W, H)
		{
		when(FL_WHEN_CHANGED);
		}

