#include <FltkExt/FXSize.h>

FXSize::FXSize() :
	cx(0), cy(0)
		{
		}


FXSize::FXSize(int w, int h) :
	cx(w), cy(h)
		{
		}


FXSize &
FXSize::operator=(const FXSize &other)
		{
		cx = other.cx;
		cy = other.cy;
		
		return *this;
		}


bool
FXSize::operator==(const FXSize &other)
		{
		return (cx == other.cx && cy == other.cy);
		}


bool
FXSize::operator!=(const FXSize &other)
		{
		return (cx != other.cx || cy != other.cy);
		}


void
FXSize::set(int w, int h)
		{
		cx = w;
		cy = h;
		}

void		FXSize::width(int v)			{ cx = v; }
int			FXSize::width() const			{ return cx; }

void		FXSize::height(int v)			{ cy = v; }
int			FXSize::height() const			{ return cy; }

void		FXSize::SetSize(int w, int h)	{ set(w, h); }
