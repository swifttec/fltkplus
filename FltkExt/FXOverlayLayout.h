#ifndef __FltkExt_FXOverlayLayout_h__
#define __FltkExt_FXOverlayLayout_h__

#include <FltkExt/FXLayout.h>

/**
*** A layout class based on all children occupying the same space, i.e. overlaid on
*** top of each other. Either the children will be invisible or transparent and so 
*** drawn in layers
**/
class FLTKEXT_DLL_EXPORT FXOverlayLayout : public FXLayout
		{
public:
		/// Constructor
		FXOverlayLayout();
		
		/// Destructor
		virtual ~FXOverlayLayout();


public: // FXLayout overrides

		/**
		*** Add a child widget at the "next" available postion
		**/
		virtual void		addWidget(Fl_Widget *pWidget);

		/**
		*** Get the minimum size of this layout based on the current children.
		***
		*** The minimum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		**/
		virtual FXSize		getMinSize() const;


		/**
		*** Get the maximum size of this layout based on the current children.
		***
		*** The maximum size is normally calculated by adding up the min sizes
		*** of each of the children. However layouts can do this in any way they want.
		***
		*** If a dimension is zero this implies that there is no maximum size for that
		*** give dimension. So if getMaxSize returns a dimension of 1000 x 0 it means that
		*** the width is restricted to 1000 pixels, but the height is unrestricted.
		**/
		virtual FXSize		getMaxSize() const;

		/// Called to layout the children
		virtual void		layoutChildren();
		};

#endif // __FltkExt_FXOverlayLayout_h__
