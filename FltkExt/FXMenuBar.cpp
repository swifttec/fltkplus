#include "FXMenuBar.h"

#include <FL/Fl_Menu.H>

#include <FltkExt/FXMenuBar.h>

#include <FL/Fl.H>
#include <FL/flstring.h>

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXWidget.h>
#include <FltkExt/FXGroup.h>

void
FXMenuBar::menuCallback(Fl_Widget *pWidget, void *userdata)
	{

	}

FXMenuBar::FXMenuBar() :
	m_pMenu(NULL),
	m_pLastSelectedItem(NULL)
		{
		setFixedHeight(30);
		set_flag(SHORTCUT_LABEL);
		box(FL_UP_BOX);
		when(FL_WHEN_RELEASE_ALWAYS);
		value_ = 0;
		selection_color(FL_SELECTION_COLOR);
		textfont(FL_HELVETICA);
		textsize(FL_NORMAL_SIZE);
		textcolor(FL_FOREGROUND_COLOR);
		down_box(FL_NO_BOX);

		m_pMenu = new FXMenu();
		}

/**
 Creates a new FXMenuBar widget using the given position, size,
 and label string.  menu() is initialized to null.
 */
FXMenuBar::FXMenuBar(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H),
	m_pMenu(NULL),
	m_pLastSelectedItem(NULL)
		{
		setFixedHeight(H);
		set_flag(SHORTCUT_LABEL);
		box(FL_UP_BOX);
		when(FL_WHEN_RELEASE_ALWAYS);
		value_ = 0;
		selection_color(FL_SELECTION_COLOR);
		textfont(FL_HELVETICA);
		textsize(FL_NORMAL_SIZE);
		textcolor(FL_FOREGROUND_COLOR);
		down_box(FL_NO_BOX);

		m_pMenu = new FXMenu();
		}


FXMenuBar::~FXMenuBar()
		{
		clear();
		safe_delete(m_pMenu);
		}


/**
  Same as menu(NULL), set the array pointer to null, indicating
  a zero-length menu.

  Menus must not be cleared during a callback to the same menu.
*/
void
FXMenuBar::clear()
		{
		m_pMenu->clear();
		m_pLastSelectedItem = NULL;
		}


/*

FXMenuBar::FXMenuBar(Fl_Widget *pOwner) :
	Fl_Widget(0, 0, 100, 30),
	m_pOwner(pOwner)
		{
		}


FXMenuBar::FXMenuBar(int X, int Y, int W, int H, Fl_Widget *pOwner) :
	Fl_Widget(X, Y, W, H),
	m_pOwner(pOwner)
		{
		}


FXMenuBar::~FXMenuBar()
		{
		}
*/

void
FXMenuBar::addTo(sw_uint32_t pid, sw_uint32_t id, const SWString &text)
		{
		add(FXMenuItem(id, text));
		}


void
FXMenuBar::add(sw_uint32_t id, const SWString &text)
		{
		m_pMenu->add(id, text);
		}


void
FXMenuBar::add(const FXMenuItem &item)
		{
		m_pMenu->add(item);
		}


void
FXMenuBar::addSeparator()
		{
		if (m_pMenu->size() != 0)
			{
			m_pMenu->getItemAt((int)m_pMenu->size()-1)->setFlags(FXMenuItem::SeparatorAfter);
			}
		}

/*
void
FXMenuBar::check(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::CheckBox);
			pItem->setFlags(FXMenuItem::Checked, v);
			}
		}


void
FXMenuBar::enable(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::Disabled, !v);
			}
		}


FXMenuItem *
FXMenuBar::findItem(sw_uint32_t id)
		{
		FXMenuItem *pItem=NULL;

		for (int i=0;i<(int)m_menuItems.size();i++)
			{
			if (m_menuItems[i].id() == id)
				{
				pItem = &m_menuItems[i];
				break;
				}
			}

		return pItem;
		}




void
FXMenuBar::displayAndTrack(const FXPoint &pt)
		{
		int			nitems=(int)m_menuItems.size();
		FXMenuItem *menu;
		int			idx=0;

		menu = new FXMenuItem[nitems+1];
		memset(menu, 0, (nitems+1) * sizeof(FXMenuItem));

		for (int i=0;i<nitems;i++)
			{
			FXMenuItem		&item=m_menuItems[i];
			FXMenuItem	*p=&menu[idx];

			if (item.type() == FXMenuItem::Item)
				{
				item.setMenuItem(p);
				p->shortcut_ = 0;
				p->flags = 0;
				p->labeltype_ = 0;
				p->labelfont_ = 0;
				p->labelsize_ = 0;
				p->labelcolor_ = 0;
				p->text = item.text();
				p->callback_ = menuCallback;
				p->user_data_ = this;

				if (item.isFlagSet(FXMenuItem::CheckBox)) p->flags |= FL_MENU_TOGGLE;
				if (item.isFlagSet(FXMenuItem::Checked)) p->flags |= FL_MENU_VALUE;
				if (item.isFlagSet(FXMenuItem::Disabled)) p->flags |= FL_MENU_INACTIVE;
				idx++;
				}
			else if (item.type() == FXMenuItem::Separator)
				{
				item.setMenuItem(NULL);
				if (idx > 0) menu[idx-1].flags |= FL_MENU_DIVIDER;
				}
			}

		const FXMenuItem *m = menu->popup(pt.x, pt.y, 0, 0, 0);
		if (m != NULL)
			{
			sw_uint32_t	id=0;

			for (int i=0;i<nitems;i++)
				{
				FXMenuItem		&item=m_menuItems[i];

				if (item.getMenuItem() == m)
					{
					id = item.id();
					break;
					}
				}

			if (id != 0)
				{
				fx_widget_handleCommand(m_pOwner, id);
				}

			m->do_callback(0, m->user_data());
			}

		delete [] menu;
		}
*/



void
FXMenuBar::draw()
		{
		draw_box();
		if (m_pMenu == NULL) return;

		int		nitems=(int)m_pMenu->size();
		int		X = x() + 6;

		for (int i=0;i<nitems;i++)
			{
			FXMenuItem	*m=m_pMenu->getItemAt(i);

			int W = m->measure(m_pMenu).cx + 16;
			m->draw(X, y(), W, h(), m_pMenu);
			X += W;
			if (m->flags() & FXMenuItem::SeparatorAfter)
				{
				int y1 = y() + Fl::box_dy(box());
				int y2 = y1 + h() - Fl::box_dh(box()) - 1;

				// Draw a vertical divider between menus...
				fl_color(FL_DARK3);
				fl_yxline(X - 6, y1, y2);
				fl_color(FL_LIGHT3);
				fl_yxline(X - 5, y1, y2);
				}
			}
		}


int
FXMenuBar::onMouseEnter()
		{
		return 1;
		}


int
FXMenuBar::onMouseLeave()
		{
		for (int i=0;i<(int)m_pMenu->size();i++)
			{
			m_pMenu->getItemAt(i)->clearFlags(FXMenuItem::Selected);
			}

		invalidate();

		return 1;
		}


int
FXMenuBar::onMouseMove(const FXPoint &pt)
		{
		/*
		for (int i=0;i<(int)m_pMenu->size();i++)
			{
			FXMenuItem	*pItem=m_pMenu->getItemAt(i);

			if (pItem->lastDrawnRect().contains(pt))
				{
				pItem->setFlags(FXMenuItem::Selected);
				}
			else
				{
				pItem->clearFlags(FXMenuItem::Selected);
				}
			}

		invalidate();
		*/

		return 1;
		}


int
FXMenuBar::onMouseDown(int button, const FXPoint &pt)
		{
		FXMenuItem	*pClickedItem=NULL;

		for (int i=0;i<(int)m_pMenu->size();i++)
			{
			FXMenuItem	*pItem=m_pMenu->getItemAt(i);

			if (pItem->lastDrawnRect().contains(pt))
				{
				pClickedItem = pItem;
				break;
				}
			}
		
		if (pClickedItem != NULL)
			{
			picked(menu()->pulldown(x(), y(), w(), h(), pClickedItem, this, NULL, 1));
			}

		return 1;
		}


int
FXMenuBar::onShortcut(int code, const FXPoint &pt)
		{
		/*
		if (visible_r())
			{
			v = menu()->find_shortcut(0, true);
			if (v && v->submenu())
				{
				v = menu()->pulldown(x(), y(), w(), h(), v, this, 0, 1);
				picked(v);
				}
			}
			*/

		return test_shortcut() != 0;
		}



/**
 Find the menu item for a given menu \p pathname, such as "Edit/Copy".

 This method finds a menu item in the menu array, also traversing submenus, but
 not submenu pointers (FL_SUBMENU_POINTER).

 To get the menu item's index, use find_index(const char*)

  \b Example:
  \code
	FXMenuBarBar *menubar = new FXMenuBarBar(..);
	menubar->add("File/&Open");
	menubar->add("File/&Save");
	menubar->add("Edit/&Copy");
	// [..]
	FXMenuItem *item;
	if ( ( item = (FXMenuItem*)menubar->find_item("File/&Open") ) != NULL ) {
	item->labelcolor(FL_RED);
	}
	if ( ( item = (FXMenuItem*)menubar->find_item("Edit/&Copy") ) != NULL ) {
	item->labelcolor(FL_GREEN);
	}
  \endcode

  \param pathname The path and name of the menu item
  \returns The item found, or NULL if not found
  \see find_index(const char*), find_item(Fl_Callback*), item_pathname()
*/
const FXMenuItem *
FXMenuBar::find_item(const SWString &pathname)
		{
		return m_pMenu->findItemByPath(pathname);
		}


/**
 Find the menu item for the given callback \p cb.

 This method finds a menu item in a menu array, also traversing submenus, but
 not submenu pointers. This is useful if an application uses
 internationalisation and a menu item can not be found using its label. This
 search is also much faster.

 \param[in] cb find the first item with this callback
 \returns The item found, or NULL if not found
 \see find_item(const char*)
 */
const FXMenuItem *
FXMenuBar::find_item(Fl_Callback *cb)
		{
		return m_pMenu->findItemByCallback(cb);
		}

/**
  The value is the index into menu() of the last item chosen by
  the user.  It is zero initially.  You can set it as an integer, or set
  it with a pointer to a menu item.  The set routines return non-zero if
  the new value is different than the old one.
*/
int FXMenuBar::value(const FXMenuItem* m)
	{
	clear_changed();
	if (value_ != m) { value_ = m; return 1; }
	return 0;
	}

/**
 When user picks a menu item, call this.  It will do the callback.
 Unfortunately this also casts away const for the checkboxes, but this
 was necessary so non-checkbox menus can really be declared const...
*/
const FXMenuItem *
FXMenuBar::picked(const FXMenuItem* v)
		{
		if (v)
			{
			if (v->radio())
				{
				if (!v->value())
					{ // they are turning on a radio item
					set_changed();
					setonly((FXMenuItem*)v);
					}
				redraw();
				}
			else if (v->flags() & FXMenuItem::CheckBox)
				{
				set_changed();
				((FXMenuItem*)v)->flags(v->flags() ^ FXMenuItem::Checked);
				redraw();
				}
			else if (v != value_)
				{ // normal item
				set_changed();
				}
			value_ = v;
			if (when()&(FL_WHEN_CHANGED | FL_WHEN_RELEASE))
				{
				if (changed() || when()&FL_WHEN_NOT_CHANGED)
					{
					if (value_)
						{
						if (value_->callback()) value_->do_callback((Fl_Widget*)this);
						else do_callback(this, (void *)value_);
						}
					else
						{
						do_callback();
						}
					}
				}
			}
		return v;
		}

#ifdef NOT_YET
/* Scans an array of FXMenuItem's that begins at start, searching for item.
 Returns NULL if item is not found.
 If item is present, returns start, unless item belongs to an
 FL_SUBMENU_POINTER-adressed array of items, in which case the first item of this array is returned.
 */
static FXMenuItem *
first_submenu_item(FXMenuItem *item, FXMenuItem *start)
		{
		FXMenuItem* m = start;
		int nest = 0; // will indicate submenu nesting depth
		while (1)
			{ // loop over all menu items
			if (!m->text)
				{ // m is a null item
				if (!nest) return NULL; // item was not found
				nest--; // m marks the end of a submenu -> decrement submenu nesting depth
				}
			else
				{ // a true item
				if (m == item) return start; // item is found, return menu start item
				if (m->flags & FL_SUBMENU_POINTER)
					{
					// scan the detached submenu which begins at m->user_data()
					FXMenuItem *first = first_submenu_item(item, (FXMenuItem*)m->user_data());
					if (first) return first; // if item was found in the submenu, return
					}
				else if (m->flags & FL_SUBMENU)
					{ // a direct submenu
					nest++; // increment submenu nesting depth
					}
				}
			m++; // step to next menu item
			}
		}
#endif // NOT_YET


/** Turns the radio item "on" for the menu item and turns "off" adjacent radio items of the same group. */
void
FXMenuBar::setonly(FXMenuItem *item)
		{
		FXMenu	*pMenu=item->owner();

		if (pMenu != NULL && item->isFlagSet(FXMenuItem::Radio))
			{
			int		itemindex=pMenu->indexOf(item);

			if (itemindex >= 0)
				{
				int		nitems=(int)pMenu->size();
				int		pos;

				item->flags(item->flags() | FXMenuItem::Checked);
				for (pos=itemindex-1;pos>=0;pos--)
					{
					item = pMenu->getItemAt(pos);
					if (item->isFlagSet(FXMenuItem::SeparatorAfter) || !item->isFlagSet(FXMenuItem::Radio)) break;
					item->flags(item->flags() & ~FXMenuItem::Checked);
					}

				for (pos=itemindex+1;pos<nitems;pos++)
					{
					item = pMenu->getItemAt(pos);
					if (!item->isFlagSet(FXMenuItem::Radio)) break;
					item->flags(item->flags() & ~FXMenuItem::Checked);
					if (item->isFlagSet(FXMenuItem::SeparatorAfter) || !item->isFlagSet(FXMenuItem::Radio)) break;
					}
				}
			}
		}


/**
  This returns the number of FXMenuItem structures that make up the
  menu, correctly counting submenus.  This includes the "terminator"
  item at the end.  To copy a menu array you need to copy
  size()*sizeof(FXMenuItem) bytes.  If the menu is
  NULL this returns zero (an empty menu will return 1).
*/
int FXMenuBar::size() const
		{
		return (int)m_pMenu->size();
		}

/**
	Sets the menu array pointer directly.  If the old menu is private it is
	deleted.  NULL is allowed and acts the same as a zero-length
	menu.  If you try to modify the array (with add(), replace(), or
	remove()) a private copy is automatically done.
void FXMenuBar::menu(const FXMenuItem* m)
	{
	clear();
	value_ = menu_ = (FXMenuItem*)m;
	}
*/

// this version is ok with new FXMenuBaradd code with fl_menu_array_owner:

/**
  Sets the menu array pointer with a copy of m that will be automatically deleted.
  If userdata \p ud is not NULL, then all user data pointers are changed in the menus as well.
  See void FXMenuBar::menu(const FXMenuItem* m).
void FXMenuBar::copy(const FXMenuItem* m, void* ud)
	{
	int n = m->size();
	FXMenuItem* newMenu = new FXMenuItem[n];
	memcpy(newMenu, m, n * sizeof(FXMenuItem));
	menu(newMenu);
	alloc = 1; // make destructor free array, but not strings
	// for convenience, provide way to change all the user data pointers:
	if (ud) for (; n--;)
		{
		if (newMenu->callback_) newMenu->user_data_ = ud;
		newMenu++;
		}
	}
*/


/**
 Clears the specified submenu pointed to by \p index of all menu items.

 This method is useful for clearing a submenu so that it can be
 re-populated with new items. Example: a "File/Recent Files/..." submenu
 that shows the last few files that have been opened.

 The specified \p index must point to a submenu.

 The submenu is cleared with remove().
 If the menu array was directly set with menu(x), then copy()
 is done to make a private array.

 \warning Since this method can change the internal menu array, any menu
 item pointers or indices the application may have cached can become
 stale, and should be recalculated/refreshed.

 \b Example:
 \code
   int index = menubar->find_index("File/Recent");    // get index of "File/Recent" submenu
   if ( index != -1 ) menubar->clear_submenu(index);  // clear the submenu
   menubar->add("File/Recent/Aaa");
   menubar->add("File/Recent/Bbb");
   [..]
 \endcode

 \param index The index of the submenu to be cleared
 \returns 0 on success, -1 if the index is out of range or not a submenu
 \see remove(int)
 */
#ifdef NOT_YET
int FXMenuBar::clear_submenu(int index)
	{
	if (index < 0 || index >= size()) return(-1);
	if (!(menu_[index].flags & FL_SUBMENU)) return(-1);
	++index;					// advance to first item in submenu
	while (index < size())
		{                    // keep remove()ing top item until end is reached
		if (menu_[index].text == 0) break;	// end of this submenu? done
		remove(index);				// remove items/submenus
		}
	return(0);
	}
#endif // NOT_YET

//
// End of "$Id: FXMenuBar.cxx 11801 2016-07-09 17:06:46Z AlbrechtS $".
//

//
// "$Id: FXMenuBaradd.cxx 11801 2016-07-09 17:06:46Z AlbrechtS $"
//
// Menu utilities for the Fast Light Tool Kit (FLTK).
//
// Copyright 1998-2016 by Bill Spitzak and others.
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//

// Methods to alter the menu in an FXMenuBar widget.

// These are for Forms emulation and for dynamically changing the
// menus.  They are in this source file so they are not linked in if
// not used, which is what will happen if the program only uses
// constant menu tables.

// Not at all guaranteed to be Forms compatible, especially with any
// string with a % sign in it!


// If the array is this, we will double-reallocate as necessary:
static FXMenuItem* local_array = 0;
static int local_array_alloc = 0; // number allocated
static int local_array_size = 0; // == size(local_array)

// For historical reasons there are matching methods that work on a
// user-allocated array of FXMenuItem.  These methods are quite
// depreciated and should not be used.  These old methods use the
// above pointers to detect if the array belongs to an FXMenuBar
// widget, and if so it reallocates as necessary.



#ifdef NOT_YET
// Insert a single FXMenuItem into an array of size at offset n,
// if this is local_array it will be reallocated if needed.
static FXMenuItem* array_insert(
  FXMenuItem* array,  // array to modify
  int size,             // size of array
  int n,                // index of new insert position
  const char *text,     // text of new item (copy is made)
  int flags             // flags for new item
)
	{
	if (array == local_array && size >= local_array_alloc)
		{
		local_array_alloc = 2 * size;
		FXMenuItem* newarray = new FXMenuItem[local_array_alloc];
		memmove(newarray, array, size * sizeof(FXMenuItem));
		delete[] local_array;
		local_array = array = newarray;
		}
	// move all the later items:
	memmove(array + n + 1, array + n, sizeof(FXMenuItem)*(size - n));
	// create the new item:
	FXMenuItem* m = array + n;
	m->text = text ? strdup(text) : 0;
	m->shortcut_ = 0;
	m->callback_ = 0;
	m->user_data_ = 0;
	m->flags = flags;
	m->labeltype_ = m->labelsize_ = m->labelcolor_ = 0;
	m->labelfont_ = FL_HELVETICA;
	return array;
	}
#endif // NOT_YET



// Comparison that does not care about deleted '&' signs:
static int compare(const char* a, const char* b)
	{
	for (;;)
		{
		int n = *a - *b;
		if (n)
			{
			if (*a == '&') a++;
			else if (*b == '&') b++;
			else return n;
			}
		else if (*a)
			{
			a++; b++;
			}
		else
			{
			return 0;
			}
		}
	}


/** Adds a menu item.

  The text is split at '/' characters to automatically
  produce submenus (actually a totally unnecessary feature as you can
  now add submenu titles directly by setting FL_SUBMENU in the flags).

  \returns the index into the menu() array, where the entry was added
  \see FXMenuItem::insert(int, const char*, int, Fl_Callback*, void*, int)
*/
void
FXMenuBar::add(const SWString &mytext, int sc, Fl_Callback *cb, void *data, int myflags)
		{
		m_pMenu->add(0, mytext, sc, cb, data, myflags);
		}

void
FXMenuBar::add(sw_uint32_t id, const SWString &mytext, int sc, Fl_Callback *cb, void *data, int myflags)
		{
		m_pMenu->add(id, mytext, sc, cb, data, myflags);
		}

/**
 Inserts an item at position \p index.

 If \p index is -1, the item is added the same way as FXMenuItem::add().

 If 'mytext' contains any un-escaped front slashes (/), it's assumed
 a menu pathname is being specified, and the value of \p index
 will be ignored.

 In all other aspects, the behavior of insert() is the same as add().

 \param[in] index	insert new items here
 \param[in] mytext	new label string, details see above
 \param[in] sc		keyboard shortcut for new item
 \param[in] cb		callback function for new item
 \param[in] data	user data for new item
 \param[in] myflags	menu flags as described in FL_Menu_Item

 \returns the index into the menu() array, where the entry was added
*/
void
FXMenuBar::insert(int index, const SWString &mytext, int sc, Fl_Callback *cb, void *data, int myflags)
		{
		m_pMenu->insert(index, 0, mytext, sc, cb, data, myflags);
		}




/**
  This is a Forms (and SGI GL library) compatible add function, it
  adds many menu items, with '|' separating the menu items, and tab
  separating the menu item names from an optional shortcut string.

  The passed string is split at any '|' characters and then
  add(s,0,0,0,0) is done with each section. This is
  often useful if you are just using the value, and is compatible
  with Forms and other GL programs. The section strings use the
  same special characters as described for the long version of add().

  No items must be added to a menu during a callback to the same menu.

  \param str string containing multiple menu labels as described above
  \returns the index into the menu() array, where the entry was added
*/
void
FXMenuBar::add(const SWString &str)
		{
		m_pMenu->add(0, str);
		}



/**
  Changes the text of item \p i.  This is the only way to get
  slash into an add()'ed menu item.  If the menu array was directly set
  with menu(x) then copy() is done to make a private array.

  \param i index into menu array
  \param str new label for menu item at index i
*/
void
FXMenuBar::replace(int i, const SWString &str)
		{
		FXMenuItem	*pItem=m_pMenu->getItemAt(i);

		if (pItem != NULL)
			{
			pItem->text(str);
			}
		}



/**
  Deletes item \p i from the menu.  If the menu array was directly
  set with menu(x) then copy() is done to make a private array.

  No items must be removed from a menu during a callback to the same menu.

  \param i index into menu array
*/
void
FXMenuBar::remove(int i)
		{
		m_pMenu->removeItemAt(i);
		}


void
FXMenuBar::check(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::Checked, v);
			}
		}


void
FXMenuBar::enable(sw_uint32_t id, bool v)
		{
		FXMenuItem	*pItem=findItem(id);

		if (pItem != NULL)
			{
			pItem->setFlags(FXMenuItem::Disabled, !v);
			}
		}


FXMenuItem *
FXMenuBar::findItem(sw_uint32_t id)
		{
		FXMenuItem	*pFoundItem=NULL;

		if (m_pMenu != NULL)
			{
			pFoundItem = m_pMenu->findItemByID(id);
			}

		return pFoundItem;
		}
