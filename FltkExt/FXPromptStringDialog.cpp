#include "FXPromptStringDialog.h"

#include "FXStaticText.h"

#include <FL/Fl_Input.H>
#include <FL/Fl_Return_Button.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Text_Display.H>

#include <swift/sw_functions.h>



FXPromptStringDialog::FXPromptStringDialog(const SWString &title, const SWString &prompt, const SWString &value, Fl_Widget *pParent) :
	FXDialog(title, 400, 90, pParent),
	m_title(title),
	m_prompt(prompt),
	m_value(value)
		{
		FXSize	sz=fx_getTextSize(prompt);

		addStaticText(FX_IDSTATIC, prompt, 10, 10, sz.cx+10, sz.cy+8);
		addTextInput(100, sz.cx+30, 10, w()-40-sz.cx, sz.cy+8);
		addButton(FX_IDOK, "OK", 1, 200, 50);
		addButton(FX_IDCANCEL, "Cancel", 0, 300, 50);

		setItemValue(100, value);
		setItemFocus(100);
		}


FXPromptStringDialog::~FXPromptStringDialog()
		{
		}



void
FXPromptStringDialog::onOK()
		{
		bool	ok=true;

		getItemValue(100, m_value);
		
		if (ok) FXDialog::onOK();
		}
