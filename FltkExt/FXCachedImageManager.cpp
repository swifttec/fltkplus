#include "FXCachedImageManager.h"

#include <swift/SWGuard.h>
#include <swift/sw_trace.h>

// GLobals
FLTKEXT_DLL_EXPORT FXCachedImageManager	theImageMgr;

#define SHOW_REFERENCE_TRACE

FXCachedImageManager::FXCachedImageManager() :
	m_pHead(NULL),
	m_pTail(NULL),
	m_count(0),
	m_referenced(0),
	m_unreferenced(0),
	m_nextid(1)
		{
		synchronize();
		}


FXCachedImageManager::~FXCachedImageManager()
		{
#if defined(_DEBUG)
		TRACE("CachedImageManager - There are %d images remaining on the image list\n", m_count);
		TRACE("CachedImageManager - %d unreferenced, %d referenced\n", m_unreferenced, m_referenced);
#endif
		}


FXCachedImage *
FXCachedImageManager::add(const SWString &filename)
		{

		return findOrCreate(filename);
		}


FXCachedImage *
FXCachedImageManager::findOrCreate(const SWString &filename)
		{
		FXCachedImage	*pObject=NULL;

		if (!filename.isEmpty())
			{
			SWGuard	guard(this);

			pObject = find(filename);
			if (pObject == NULL)
				{
				pObject = new FXCachedImage();
				if (!pObject->load(filename))
					{
					// delete pObject;
					}
				else
					{
					pObject->id(m_nextid++);
					}

				add(pObject);
				}
			}

		return pObject;
		}


FXCachedImage *
FXCachedImageManager::add(FXCachedImage *pObject)
		{
		if (pObject != NULL)
			{
			SWGuard	guard(this);

			if (pObject->refCount() < 0)
				{
				// This is the first time we've seen this object - add it to our list.
				// Add the list element to the tail of the list
				pObject->m_pNext = NULL;
				pObject->m_pPrev = m_pTail;
				if (m_pTail != NULL) m_pTail->m_pNext = pObject;
				if (m_pHead == NULL) m_pHead = pObject;
				m_pTail = pObject;
				m_count++;

				pObject->incRefCount();
				}

			if (pObject->refCount() == 0)
				{
				m_referenced++;
				}

			pObject->incRefCount();
		

#if defined(_DEBUG) && defined(SHOW_REFERENCE_TRACE)
		TRACE("CachedImageManager::add 0x%x, refCount = %d\n", pObject, pObject->refCount());
		TRACE("CachedImageManager::list = %d objects\n", m_count);
#endif
			}

		return pObject;
		}


FXCachedImage *
FXCachedImageManager::find(const SWString &filename)
		{
		SWGuard		guard(this);
		FXCachedImage	*pFoundImage=NULL;
		FXCachedImage	*pImage=m_pHead;

		while (pImage != NULL)
			{
			if (pImage->filename() == filename)
				{
				pFoundImage = pImage;
				break;
				}

			pImage = pImage->m_pNext;
			}

		return pFoundImage;
		}


void
FXCachedImageManager::remove(const SWString &filename)
		{
		SWGuard		guard(this);
		FXCachedImage	*pImage=find(filename);

		if (pImage != NULL) remove(pImage);
		}


void
FXCachedImageManager::remove(FXCachedImage *pObject)
		{
		if (pObject != NULL)
			{
			SWGuard	guard(this);

			if (pObject->refCount() > 0)
				{
				pObject->decRefCount();
		
#if defined(_DEBUG) && defined(SHOW_REFERENCE_TRACE)
			TRACE("CachedImageManager::remove 0x%x, refCount = %d\n", pObject, pObject->refCount());
#endif
				if (pObject->refCount() == 0)
					{
					m_referenced--;
					m_unreferenced++;
					}
				}
#if defined(_DEBUG)
			else
				{
				TRACE("CachedImageManager::remove 0x%x, attempt to remove unreferenced image\n", pObject);
				}
#endif


			/*
			if (pObject->refCount() <= 0)
				{
				CachedImage	*n, *p;

				n = pObject->m_pNext;
				p = pObject->m_pPrev;
				if (m_pHead == pObject) m_pHead = pObject->m_pNext;
				if (m_pTail == pObject) m_pTail = pObject->m_pPrev;
				if (n) n->m_pPrev = pObject->m_pPrev;
				if (p) p->m_pNext = pObject->m_pNext;

				delete pObject;
				m_count--;
				}
			*/

#if defined(_DEBUG) && defined(SHOW_REFERENCE_TRACE)
		TRACE("CachedImageManager::list = %d objects\n", m_count);
#endif
			}
		}
