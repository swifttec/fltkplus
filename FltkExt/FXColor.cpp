#include <FltkExt/FXColor.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

#define FX_COLOR_FLAG_NONE		0x8000	// Special color - no color
#define FX_COLOR_FLAG_DEFAULT	0x4000	// Special color - default color
#define FX_COLOR_BITS_MASK	0x003f

const FXColor FXColor::NoColor=FXColor(FX_COLOR_FLAG_NONE, 0, 0, 0, 0, 0);
const FXColor FXColor::DefaultColor=FXColor(FX_COLOR_FLAG_DEFAULT, 0, 0, 0, 0, 0);

FXColor::FXColor(int flags, int bitsPerComponent, int r, int g, int b, int a)
		{
		set(bitsPerComponent, r, g, b, a);
		m_flags = flags;
		}


FXColor::FXColor()
		{
		set(8, 0, 0, 0, 0xff);
		}


FXColor::FXColor(const FXColor &other)
		{
		*this = other;
		}


FXColor::FXColor(COLORREF v)
		{
		*this = v;
		}

FXColor::FXColor(int bitsPerComponent, int r, int g, int b, int a)
		{
		set(bitsPerComponent, r, g, b, a);
		}

FXColor::FXColor(const char *name)
		{
		set(name, FXColor());
		}

FXColor::FXColor(const wchar_t *name)
		{
		set(name, FXColor());
		}

FXColor::FXColor(const SWString &name)
		{
		set(name, FXColor());
		}
		
FXColor::FXColor(const SWString &name, const FXColor &defaultColor)
		{
		set(name, defaultColor);
		}


FXColor::FXColor(int r, int g, int b, int a)
		{
		set(8, r, g, b, a);
		}


bool
FXColor::operator!=(const FXColor &other) const
		{
		return !operator==(other);
		}


bool
FXColor::operator==(const FXColor &other) const
		{
		bool	res=true;

		res = ((m_flags & ~FX_COLOR_BITS_MASK) == (other.m_flags & ~FX_COLOR_BITS_MASK));
		if (res)
			{
			int		nbits=sw_math_max(bits(), other.bits());
			int		r1, g1, b1, a1;
			int		r2, g2, b2, a2;
			
			getComponents(nbits, r1, g1, b1, a1);
			getComponents(nbits, r2, g2, b2, a2);

			res = (r1 == r2 && g1 == g2 && b1 == b2 && a1 == a2);
			}

		return res;
		}


FXColor &
FXColor::operator=(const FXColor &other)
		{
#define COPY(x)	x = other.x
		COPY(m_red);
		COPY(m_green);
		COPY(m_blue);
		COPY(m_alpha);
		COPY(m_mask);
		COPY(m_flags);
#undef COPY

		return *this;
		}


FXColor &
FXColor::operator|=(const FXColor &other)
		{
		int		nbits=sw_math_max(bits(), other.bits());
		int		r1, g1, b1, a1;
		int		r2, g2, b2, a2;
		int		maxv=(1 << nbits)-1;
			
		getComponents(nbits, r1, g1, b1, a1);
		other.getComponents(nbits, r2, g2, b2, a2);

		r1 = sw_math_min(maxv, r1 + r2);
		g1 = sw_math_min(maxv, g1 + g2);
		b1 = sw_math_min(maxv, b1 + b2);
		a1 = sw_math_min(maxv, a1 + a2);

		set(nbits, r1, g1, b1, a1);

		return *this;
		}

int
FXColor::bits() const
		{
		return (m_flags & FX_COLOR_BITS_MASK);
		}


void
FXColor::clear()
		{
		set(0, 0, 0, ~0);
		}


void
FXColor::set(int r, int g, int b, int a)
		{
		set(m_flags, r, g, b, a);
		}


void
FXColor::set(int bitsPerComponent, int r, int g, int b, int a)
		{
		m_flags = bitsPerComponent;
		m_mask = ~0;

		int		shiftbits=(sizeof(m_mask)*8)-bits();

		m_mask >>= shiftbits;
		m_red = r & m_mask;
		m_green = g & m_mask;
		m_blue = b & m_mask;
		m_alpha = a & m_mask;
		}


void
FXColor::set(COLORREF color)
		{
		m_flags = 8;
		m_mask = 0xff;
		m_red = GetRValue(color);
		m_green = GetGValue(color);
		m_blue = GetBValue(color);
		m_alpha = 0xff;
		}


FXColor &
FXColor::operator=(COLORREF color)
		{
		set(color);

		return *this;
		}


FXColor &
FXColor::operator|=(COLORREF color)
		{
		m_red |= GetRValue(color);
		m_green |= GetGValue(color);
		m_blue |= GetBValue(color);

		return *this;
		}


void
FXColor::set(const FXColor &other)
		{
		*this = other;
		}



// Table entry
class ColorTableEntry
	{
public:
	const char	*name;
	int			color;
	};



// Start group a
static ColorTableEntry	ctab_a[] =
{
	{ "alice blue",		0xf0f8ff },
	{ "AliceBlue",		0xf0f8ff },
	{ "antique white",	0xfaebd7 },
	{ "AntiqueWhite",	0xfaebd7 },
	{ "AntiqueWhite1",	0xffefdb },
	{ "AntiqueWhite2",	0xeedfcc },
	{ "AntiqueWhite3",	0xcdc0b0 },
	{ "AntiqueWhite4",	0x8b8378 },
	{ "aquamarine",	0x7fffd4 },
	{ "aquamarine1",	0x7fffd4 },
	{ "aquamarine2",	0x76eec6 },
	{ "aquamarine3",	0x66cdaa },
	{ "aquamarine4",	0x458b74 },
	{ "azure",	0xf0ffff },
	{ "azure1",	0xf0ffff },
	{ "azure2",	0xe0eeee },
	{ "azure3",	0xc1cdcd },
	{ "azure4",	0x838b8b },
	{ 0,	0 }
};


// Start group b
static ColorTableEntry	ctab_b[] =
{
	{ "beige",	0xf5f5dc },
	{ "bisque",	0xffe4c4 },
	{ "bisque1",	0xffe4c4 },
	{ "bisque2",	0xeed5b7 },
	{ "bisque3",	0xcdb79e },
	{ "bisque4",	0x8b7d6b },
	{ "black",	0x000000 },
	{ "blanched almond",	0xffebcd },
	{ "BlanchedAlmond",	0xffebcd },
	{ "blue",	0x0000ff },
	{ "blue violet",	0x8a2be2 },
	{ "blue1",	0x0000ff },
	{ "blue2",	0x0000ee },
	{ "blue3",	0x0000cd },
	{ "blue4",	0x00008b },
	{ "BlueViolet",	0x8a2be2 },
	{ "brown",	0xa52a2a },
	{ "brown1",	0xff4040 },
	{ "brown2",	0xee3b3b },
	{ "brown3",	0xcd3333 },
	{ "brown4",	0x8b2323 },
	{ "burlywood",	0xdeb887 },
	{ "burlywood1",	0xffd39b },
	{ "burlywood2",	0xeec591 },
	{ "burlywood3",	0xcdaa7d },
	{ "burlywood4",	0x8b7355 },
	{ 0,	0 }
};


// Start group c
static ColorTableEntry	ctab_c[] =
{
	{ "cadet blue",	0x5f9ea0 },
	{ "CadetBlue",	0x5f9ea0 },
	{ "CadetBlue1",	0x98f5ff },
	{ "CadetBlue2",	0x8ee5ee },
	{ "CadetBlue3",	0x7ac5cd },
	{ "CadetBlue4",	0x53868b },
	{ "chartreuse",	0x7fff00 },
	{ "chartreuse1",	0x7fff00 },
	{ "chartreuse2",	0x76ee00 },
	{ "chartreuse3",	0x66cd00 },
	{ "chartreuse4",	0x458b00 },
	{ "chocolate",	0xd2691e },
	{ "chocolate1",	0xff7f24 },
	{ "chocolate2",	0xee7621 },
	{ "chocolate3",	0xcd661d },
	{ "chocolate4",	0x8b4513 },
	{ "coral",	0xff7f50 },
	{ "coral1",	0xff7256 },
	{ "coral2",	0xee6a50 },
	{ "coral3",	0xcd5b45 },
	{ "coral4",	0x8b3e2f },
	{ "cornflower blue",	0x6495ed },
	{ "CornflowerBlue",	0x6495ed },
	{ "cornsilk",	0xfff8dc },
	{ "cornsilk1",	0xfff8dc },
	{ "cornsilk2",	0xeee8cd },
	{ "cornsilk3",	0xcdc8b1 },
	{ "cornsilk4",	0x8b8878 },
	{ "cyan",	0x00ffff },
	{ "cyan1",	0x00ffff },
	{ "cyan2",	0x00eeee },
	{ "cyan3",	0x00cdcd },
	{ "cyan4",	0x008b8b },
	{ 0,	0 }
};


// Start group d
static ColorTableEntry	ctab_d[] =
{
	{ "dark blue",	0x00008b },
	{ "dark cyan",	0x008b8b },
	{ "dark goldenrod",	0xb8860b },
	{ "dark gray",	0xa9a9a9 },
	{ "dark green",	0x006400 },
	{ "dark grey",	0xa9a9a9 },
	{ "dark khaki",	0xbdb76b },
	{ "dark magenta",	0x8b008b },
	{ "dark olive green",	0x556b2f },
	{ "dark orange",	0xff8c00 },
	{ "dark orchid",	0x9932cc },
	{ "dark red",	0x8b0000 },
	{ "dark salmon",	0xe9967a },
	{ "dark sea green",	0x8fbc8f },
	{ "dark slate blue",	0x483d8b },
	{ "dark slate gray",	0x2f4f4f },
	{ "dark slate grey",	0x2f4f4f },
	{ "dark turquoise",	0x00ced1 },
	{ "dark violet",	0x9400d3 },
	{ "DarkBlue",	0x00008b },
	{ "DarkCyan",	0x008b8b },
	{ "DarkGoldenrod",	0xb8860b },
	{ "DarkGoldenrod1",	0xffb90f },
	{ "DarkGoldenrod2",	0xeead0e },
	{ "DarkGoldenrod3",	0xcd950c },
	{ "DarkGoldenrod4",	0x8b6508 },
	{ "DarkGray",	0xa9a9a9 },
	{ "DarkGreen",	0x006400 },
	{ "DarkGrey",	0xa9a9a9 },
	{ "DarkKhaki",	0xbdb76b },
	{ "DarkMagenta",	0x8b008b },
	{ "DarkNavy",		0x000040 },
	{ "DarkOliveGreen",	0x556b2f },
	{ "DarkOliveGreen1",	0xcaff70 },
	{ "DarkOliveGreen2",	0xbcee68 },
	{ "DarkOliveGreen3",	0xa2cd5a },
	{ "DarkOliveGreen4",	0x6e8b3d },
	{ "DarkOrange",	0xff8c00 },
	{ "DarkOrange1",	0xff7f00 },
	{ "DarkOrange2",	0xee7600 },
	{ "DarkOrange3",	0xcd6600 },
	{ "DarkOrange4",	0x8b4500 },
	{ "DarkOrchid",	0x9932cc },
	{ "DarkOrchid1",	0xbf3eff },
	{ "DarkOrchid2",	0xb23aee },
	{ "DarkOrchid3",	0x9a32cd },
	{ "DarkOrchid4",	0x68228b },
	{ "DarkRed",	0x8b0000 },
	{ "DarkSalmon",	0xe9967a },
	{ "DarkSeaGreen",	0x8fbc8f },
	{ "DarkSeaGreen1",	0xc1ffc1 },
	{ "DarkSeaGreen2",	0xb4eeb4 },
	{ "DarkSeaGreen3",	0x9bcd9b },
	{ "DarkSeaGreen4",	0x698b69 },
	{ "DarkSlateBlue",	0x483d8b },
	{ "DarkSlateGray",	0x2f4f4f },
	{ "DarkSlateGray1",	0x97ffff },
	{ "DarkSlateGray2",	0x8deeee },
	{ "DarkSlateGray3",	0x79cdcd },
	{ "DarkSlateGray4",	0x528b8b },
	{ "DarkSlateGrey",	0x2f4f4f },
	{ "DarkTurquoise",	0x00ced1 },
	{ "DarkViolet",	0x9400d3 },
	{ "deep pink",	0xff1493 },
	{ "deep sky blue",	0x00bfff },
	{ "DeepPink",	0xff1493 },
	{ "DeepPink1",	0xff1493 },
	{ "DeepPink2",	0xee1289 },
	{ "DeepPink3",	0xcd1076 },
	{ "DeepPink4",	0x8b0a50 },
	{ "DeepSkyBlue",	0x00bfff },
	{ "DeepSkyBlue1",	0x00bfff },
	{ "DeepSkyBlue2",	0x00b2ee },
	{ "DeepSkyBlue3",	0x009acd },
	{ "DeepSkyBlue4",	0x00688b },
	{ "dim gray",	0x696969 },
	{ "dim grey",	0x696969 },
	{ "DimGray",	0x696969 },
	{ "DimGrey",	0x696969 },
	{ "dodger blue",	0x1e90ff },
	{ "DodgerBlue",	0x1e90ff },
	{ "DodgerBlue1",	0x1e90ff },
	{ "DodgerBlue2",	0x1c86ee },
	{ "DodgerBlue3",	0x1874cd },
	{ "DodgerBlue4",	0x104e8b },
	{ 0,	0 }
};

static ColorTableEntry	ctab_e[] =
{
	{ 0,	0 }
};


// Start group f
static ColorTableEntry	ctab_f[] =
{
	{ "firebrick",	0xb22222 },
	{ "firebrick1",	0xff3030 },
	{ "firebrick2",	0xee2c2c },
	{ "firebrick3",	0xcd2626 },
	{ "firebrick4",	0x8b1a1a },
	{ "floral white",	0xfffaf0 },
	{ "FloralWhite",	0xfffaf0 },
	{ "forest green",	0x228b22 },
	{ "ForestGreen",	0x228b22 },
	{ 0,	0 }
};


// Start group g
static ColorTableEntry	ctab_g[] =
{
	{ "gainsboro",	0xdcdcdc },
	{ "ghost white",	0xf8f8ff },
	{ "GhostWhite",	0xf8f8ff },
	{ "gold",	0xffd700 },
	{ "gold1",	0xffd700 },
	{ "gold2",	0xeec900 },
	{ "gold3",	0xcdad00 },
	{ "gold4",	0x8b7500 },
	{ "goldenrod",	0xdaa520 },
	{ "goldenrod1",	0xffc125 },
	{ "goldenrod2",	0xeeb422 },
	{ "goldenrod3",	0xcd9b1d },
	{ "goldenrod4",	0x8b6914 },
	{ "gray",	0xbebebe },
	{ "gray0",	0x000000 },
	{ "gray1",	0x030303 },
	{ "gray10",	0x1a1a1a },
	{ "gray11",	0x1c1c1c },
	{ "gray12",	0x1f1f1f },
	{ "gray13",	0x212121 },
	{ "gray14",	0x242424 },
	{ "gray15",	0x262626 },
	{ "gray16",	0x292929 },
	{ "gray17",	0x2b2b2b },
	{ "gray18",	0x2e2e2e },
	{ "gray19",	0x303030 },
	{ "gray2",	0x050505 },
	{ "gray20",	0x333333 },
	{ "gray21",	0x363636 },
	{ "gray22",	0x383838 },
	{ "gray23",	0x3b3b3b },
	{ "gray24",	0x3d3d3d },
	{ "gray25",	0x404040 },
	{ "gray26",	0x424242 },
	{ "gray27",	0x454545 },
	{ "gray28",	0x474747 },
	{ "gray29",	0x4a4a4a },
	{ "gray3",	0x080808 },
	{ "gray30",	0x4d4d4d },
	{ "gray31",	0x4f4f4f },
	{ "gray32",	0x525252 },
	{ "gray33",	0x545454 },
	{ "gray34",	0x575757 },
	{ "gray35",	0x595959 },
	{ "gray36",	0x5c5c5c },
	{ "gray37",	0x5e5e5e },
	{ "gray38",	0x616161 },
	{ "gray39",	0x636363 },
	{ "gray4",	0x0a0a0a },
	{ "gray40",	0x666666 },
	{ "gray41",	0x696969 },
	{ "gray42",	0x6b6b6b },
	{ "gray43",	0x6e6e6e },
	{ "gray44",	0x707070 },
	{ "gray45",	0x737373 },
	{ "gray46",	0x757575 },
	{ "gray47",	0x787878 },
	{ "gray48",	0x7a7a7a },
	{ "gray49",	0x7d7d7d },
	{ "gray5",	0x0d0d0d },
	{ "gray50",	0x7f7f7f },
	{ "gray51",	0x828282 },
	{ "gray52",	0x858585 },
	{ "gray53",	0x878787 },
	{ "gray54",	0x8a8a8a },
	{ "gray55",	0x8c8c8c },
	{ "gray56",	0x8f8f8f },
	{ "gray57",	0x919191 },
	{ "gray58",	0x949494 },
	{ "gray59",	0x969696 },
	{ "gray6",	0x0f0f0f },
	{ "gray60",	0x999999 },
	{ "gray61",	0x9c9c9c },
	{ "gray62",	0x9e9e9e },
	{ "gray63",	0xa1a1a1 },
	{ "gray64",	0xa3a3a3 },
	{ "gray65",	0xa6a6a6 },
	{ "gray66",	0xa8a8a8 },
	{ "gray67",	0xababab },
	{ "gray68",	0xadadad },
	{ "gray69",	0xb0b0b0 },
	{ "gray7",	0x121212 },
	{ "gray70",	0xb3b3b3 },
	{ "gray71",	0xb5b5b5 },
	{ "gray72",	0xb8b8b8 },
	{ "gray73",	0xbababa },
	{ "gray74",	0xbdbdbd },
	{ "gray75",	0xbfbfbf },
	{ "gray76",	0xc2c2c2 },
	{ "gray77",	0xc4c4c4 },
	{ "gray78",	0xc7c7c7 },
	{ "gray79",	0xc9c9c9 },
	{ "gray8",	0x141414 },
	{ "gray80",	0xcccccc },
	{ "gray81",	0xcfcfcf },
	{ "gray82",	0xd1d1d1 },
	{ "gray83",	0xd4d4d4 },
	{ "gray84",	0xd6d6d6 },
	{ "gray85",	0xd9d9d9 },
	{ "gray86",	0xdbdbdb },
	{ "gray87",	0xdedede },
	{ "gray88",	0xe0e0e0 },
	{ "gray89",	0xe3e3e3 },
	{ "gray9",	0x171717 },
	{ "gray90",	0xe5e5e5 },
	{ "gray91",	0xe8e8e8 },
	{ "gray92",	0xebebeb },
	{ "gray93",	0xededed },
	{ "gray94",	0xf0f0f0 },
	{ "gray95",	0xf2f2f2 },
	{ "gray96",	0xf5f5f5 },
	{ "gray97",	0xf7f7f7 },
	{ "gray98",	0xfafafa },
	{ "gray99",	0xfcfcfc },
	{ "green",	0x00ff00 },
	{ "green yellow",	0xadff2f },
	{ "green1",	0x00ff00 },
	{ "green2",	0x00ee00 },
	{ "green3",	0x00cd00 },
	{ "green4",	0x008b00 },
	{ "GreenYellow",	0xadff2f },
	{ "grey",	0xbebebe },
	{ "grey0",	0x000000 },
	{ "grey1",	0x030303 },
	{ "grey10",	0x1a1a1a },
	{ "grey11",	0x1c1c1c },
	{ "grey12",	0x1f1f1f },
	{ "grey13",	0x212121 },
	{ "grey14",	0x242424 },
	{ "grey15",	0x262626 },
	{ "grey16",	0x292929 },
	{ "grey17",	0x2b2b2b },
	{ "grey18",	0x2e2e2e },
	{ "grey19",	0x303030 },
	{ "grey2",	0x050505 },
	{ "grey20",	0x333333 },
	{ "grey21",	0x363636 },
	{ "grey22",	0x383838 },
	{ "grey23",	0x3b3b3b },
	{ "grey24",	0x3d3d3d },
	{ "grey25",	0x404040 },
	{ "grey26",	0x424242 },
	{ "grey27",	0x454545 },
	{ "grey28",	0x474747 },
	{ "grey29",	0x4a4a4a },
	{ "grey3",	0x080808 },
	{ "grey30",	0x4d4d4d },
	{ "grey31",	0x4f4f4f },
	{ "grey32",	0x525252 },
	{ "grey33",	0x545454 },
	{ "grey34",	0x575757 },
	{ "grey35",	0x595959 },
	{ "grey36",	0x5c5c5c },
	{ "grey37",	0x5e5e5e },
	{ "grey38",	0x616161 },
	{ "grey39",	0x636363 },
	{ "grey4",	0x0a0a0a },
	{ "grey40",	0x666666 },
	{ "grey41",	0x696969 },
	{ "grey42",	0x6b6b6b },
	{ "grey43",	0x6e6e6e },
	{ "grey44",	0x707070 },
	{ "grey45",	0x737373 },
	{ "grey46",	0x757575 },
	{ "grey47",	0x787878 },
	{ "grey48",	0x7a7a7a },
	{ "grey49",	0x7d7d7d },
	{ "grey5",	0x0d0d0d },
	{ "grey50",	0x7f7f7f },
	{ "grey51",	0x828282 },
	{ "grey52",	0x858585 },
	{ "grey53",	0x878787 },
	{ "grey54",	0x8a8a8a },
	{ "grey55",	0x8c8c8c },
	{ "grey56",	0x8f8f8f },
	{ "grey57",	0x919191 },
	{ "grey58",	0x949494 },
	{ "grey59",	0x969696 },
	{ "grey6",	0x0f0f0f },
	{ "grey60",	0x999999 },
	{ "grey61",	0x9c9c9c },
	{ "grey62",	0x9e9e9e },
	{ "grey63",	0xa1a1a1 },
	{ "grey64",	0xa3a3a3 },
	{ "grey65",	0xa6a6a6 },
	{ "grey66",	0xa8a8a8 },
	{ "grey67",	0xababab },
	{ "grey68",	0xadadad },
	{ "grey69",	0xb0b0b0 },
	{ "grey7",	0x121212 },
	{ "grey70",	0xb3b3b3 },
	{ "grey71",	0xb5b5b5 },
	{ "grey72",	0xb8b8b8 },
	{ "grey73",	0xbababa },
	{ "grey74",	0xbdbdbd },
	{ "grey75",	0xbfbfbf },
	{ "grey76",	0xc2c2c2 },
	{ "grey77",	0xc4c4c4 },
	{ "grey78",	0xc7c7c7 },
	{ "grey79",	0xc9c9c9 },
	{ "grey8",	0x141414 },
	{ "grey80",	0xcccccc },
	{ "grey81",	0xcfcfcf },
	{ "grey82",	0xd1d1d1 },
	{ "grey83",	0xd4d4d4 },
	{ "grey84",	0xd6d6d6 },
	{ "grey85",	0xd9d9d9 },
	{ "grey86",	0xdbdbdb },
	{ "grey87",	0xdedede },
	{ "grey88",	0xe0e0e0 },
	{ "grey89",	0xe3e3e3 },
	{ "grey9",	0x171717 },
	{ "grey90",	0xe5e5e5 },
	{ "grey91",	0xe8e8e8 },
	{ "grey92",	0xebebeb },
	{ "grey93",	0xededed },
	{ "grey94",	0xf0f0f0 },
	{ "grey95",	0xf2f2f2 },
	{ "grey96",	0xf5f5f5 },
	{ "grey97",	0xf7f7f7 },
	{ "grey98",	0xfafafa },
	{ "grey99",	0xfcfcfc },
	{ 0,	0 }
};


// Start group h
static ColorTableEntry	ctab_h[] =
{
	{ "honeydew",	0xf0fff0 },
	{ "honeydew1",	0xf0fff0 },
	{ "honeydew2",	0xe0eee0 },
	{ "honeydew3",	0xc1cdc1 },
	{ "honeydew4",	0x838b83 },
	{ "hot pink",	0xff69b4 },
	{ "HotPink",	0xff69b4 },
	{ "HotPink1",	0xff6eb4 },
	{ "HotPink2",	0xee6aa7 },
	{ "HotPink3",	0xcd6090 },
	{ "HotPink4",	0x8b3a62 },
	{ 0,	0 }
};


// Start group i
static ColorTableEntry	ctab_i[] =
{
	{ "indian red",	0xcd5c5c },
	{ "IndianRed",	0xcd5c5c },
	{ "IndianRed1",	0xff6a6a },
	{ "IndianRed2",	0xee6363 },
	{ "IndianRed3",	0xcd5555 },
	{ "IndianRed4",	0x8b3a3a },
	{ "ivory",	0xfffff0 },
	{ "ivory1",	0xfffff0 },
	{ "ivory2",	0xeeeee0 },
	{ "ivory3",	0xcdcdc1 },
	{ "ivory4",	0x8b8b83 },
	{ 0,	0 }
};


static ColorTableEntry	ctab_j[] =
{
	{ 0,	0 }
};


// Start group k
static ColorTableEntry	ctab_k[] =
{
	{ "khaki",	0xf0e68c },
	{ "khaki1",	0xfff68f },
	{ "khaki2",	0xeee685 },
	{ "khaki3",	0xcdc673 },
	{ "khaki4",	0x8b864e },
	{ 0,	0 }
};


// Start group l
static ColorTableEntry	ctab_l[] =
{
	{ "lavender",	0xe6e6fa },
	{ "lavender blush",	0xfff0f5 },
	{ "LavenderBlush",	0xfff0f5 },
	{ "LavenderBlush1",	0xfff0f5 },
	{ "LavenderBlush2",	0xeee0e5 },
	{ "LavenderBlush3",	0xcdc1c5 },
	{ "LavenderBlush4",	0x8b8386 },
	{ "lawn green",	0x7cfc00 },
	{ "LawnGreen",	0x7cfc00 },
	{ "lemon chiffon",	0xfffacd },
	{ "LemonChiffon",	0xfffacd },
	{ "LemonChiffon1",	0xfffacd },
	{ "LemonChiffon2",	0xeee9bf },
	{ "LemonChiffon3",	0xcdc9a5 },
	{ "LemonChiffon4",	0x8b8970 },
	{ "light blue",	0xadd8e6 },
	{ "light coral",	0xf08080 },
	{ "light cyan",	0xe0ffff },
	{ "light goldenrod",	0xeedd82 },
	{ "light goldenrod yellow",	0xfafad2 },
	{ "light gray",	0xd3d3d3 },
	{ "light green",	0x90ee90 },
	{ "light grey",	0xd3d3d3 },
	{ "light pink",	0xffb6c1 },
	{ "light salmon",	0xffa07a },
	{ "light sea green",	0x20b2aa },
	{ "light sky blue",	0x87cefa },
	{ "light slate blue",	0x8470ff },
	{ "light slate gray",	0x778899 },
	{ "light slate grey",	0x778899 },
	{ "light steel blue",	0xb0c4de },
	{ "light yellow",	0xffffe0 },
	{ "LightBlue",	0xadd8e6 },
	{ "LightBlue1",	0xbfefff },
	{ "LightBlue2",	0xb2dfee },
	{ "LightBlue3",	0x9ac0cd },
	{ "LightBlue4",	0x68838b },
	{ "LightCoral",	0xf08080 },
	{ "LightCyan",	0xe0ffff },
	{ "LightCyan1",	0xe0ffff },
	{ "LightCyan2",	0xd1eeee },
	{ "LightCyan3",	0xb4cdcd },
	{ "LightCyan4",	0x7a8b8b },
	{ "LightGoldenrod",	0xeedd82 },
	{ "LightGoldenrod1",	0xffec8b },
	{ "LightGoldenrod2",	0xeedc82 },
	{ "LightGoldenrod3",	0xcdbe70 },
	{ "LightGoldenrod4",	0x8b814c },
	{ "LightGoldenrodYellow",	0xfafad2 },
	{ "LightGray",	0xd3d3d3 },
	{ "LightGreen",	0x90ee90 },
	{ "LightGrey",	0xd3d3d3 },
	{ "LightPink",	0xffb6c1 },
	{ "LightPink1",	0xffaeb9 },
	{ "LightPink2",	0xeea2ad },
	{ "LightPink3",	0xcd8c95 },
	{ "LightPink4",	0x8b5f65 },
	{ "LightSalmon",	0xffa07a },
	{ "LightSalmon1",	0xffa07a },
	{ "LightSalmon2",	0xee9572 },
	{ "LightSalmon3",	0xcd8162 },
	{ "LightSalmon4",	0x8b5742 },
	{ "LightSeaGreen",	0x20b2aa },
	{ "LightSkyBlue",	0x87cefa },
	{ "LightSkyBlue1",	0xb0e2ff },
	{ "LightSkyBlue2",	0xa4d3ee },
	{ "LightSkyBlue3",	0x8db6cd },
	{ "LightSkyBlue4",	0x607b8b },
	{ "LightSlateBlue",	0x8470ff },
	{ "LightSlateGray",	0x778899 },
	{ "LightSlateGrey",	0x778899 },
	{ "LightSteelBlue",	0xb0c4de },
	{ "LightSteelBlue1",	0xcae1ff },
	{ "LightSteelBlue2",	0xbcd2ee },
	{ "LightSteelBlue3",	0xa2b5cd },
	{ "LightSteelBlue4",	0x6e7b8b },
	{ "LightYellow",	0xffffe0 },
	{ "LightYellow1",	0xffffe0 },
	{ "LightYellow2",	0xeeeed1 },
	{ "LightYellow3",	0xcdcdb4 },
	{ "LightYellow4",	0x8b8b7a },
	{ "lime green",	0x32cd32 },
	{ "LimeGreen",	0x32cd32 },
	{ "linen",	0xfaf0e6 },
	{ 0,	0 }
};


// Start group m
static ColorTableEntry	ctab_m[] =
{
	{ "magenta",	0xff00ff },
	{ "magenta1",	0xff00ff },
	{ "magenta2",	0xee00ee },
	{ "magenta3",	0xcd00cd },
	{ "magenta4",	0x8b008b },
	{ "maroon",	0xb03060 },
	{ "maroon1",	0xff34b3 },
	{ "maroon2",	0xee30a7 },
	{ "maroon3",	0xcd2990 },
	{ "maroon4",	0x8b1c62 },
	{ "medium aquamarine",	0x66cdaa },
	{ "medium blue",	0x0000cd },
	{ "medium orchid",	0xba55d3 },
	{ "medium purple",	0x9370db },
	{ "medium sea green",	0x3cb371 },
	{ "medium slate blue",	0x7b68ee },
	{ "medium spring green",	0x00fa9a },
	{ "medium turquoise",	0x48d1cc },
	{ "medium violet red",	0xc71585 },
	{ "MediumAquamarine",	0x66cdaa },
	{ "MediumBlue",	0x0000cd },
	{ "MediumOrchid",	0xba55d3 },
	{ "MediumOrchid1",	0xe066ff },
	{ "MediumOrchid2",	0xd15fee },
	{ "MediumOrchid3",	0xb452cd },
	{ "MediumOrchid4",	0x7a378b },
	{ "MediumPurple",	0x9370db },
	{ "MediumPurple1",	0xab82ff },
	{ "MediumPurple2",	0x9f79ee },
	{ "MediumPurple3",	0x8968cd },
	{ "MediumPurple4",	0x5d478b },
	{ "MediumSeaGreen",	0x3cb371 },
	{ "MediumSlateBlue",	0x7b68ee },
	{ "MediumSpringGreen",	0x00fa9a },
	{ "MediumTurquoise",	0x48d1cc },
	{ "MediumVioletRed",	0xc71585 },
	{ "midnight blue",	0x191970 },
	{ "MidnightBlue",	0x191970 },
	{ "mint cream",	0xf5fffa },
	{ "MintCream",	0xf5fffa },
	{ "misty rose",	0xffe4e1 },
	{ "MistyRose",	0xffe4e1 },
	{ "MistyRose1",	0xffe4e1 },
	{ "MistyRose2",	0xeed5d2 },
	{ "MistyRose3",	0xcdb7b5 },
	{ "MistyRose4",	0x8b7d7b },
	{ "moccasin",	0xffe4b5 },
	{ 0,	0 }
};


// Start group n
static ColorTableEntry	ctab_n[] =
{
	{ "navajo white",	0xffdead },
	{ "NavajoWhite",	0xffdead },
	{ "NavajoWhite1",	0xffdead },
	{ "NavajoWhite2",	0xeecfa1 },
	{ "NavajoWhite3",	0xcdb38b },
	{ "NavajoWhite4",	0x8b795e },
	{ "navy",	0x000080 },
	{ "navy blue",	0x000080 },
	{ "NavyBlue",	0x000080 },
	{ 0,	0 }
};


// Start group o
static ColorTableEntry	ctab_o[] =
{
	{ "old lace",	0xfdf5e6 },
	{ "OldLace",	0xfdf5e6 },
	{ "olive drab",	0x6b8e23 },
	{ "OliveDrab",	0x6b8e23 },
	{ "OliveDrab1",	0xc0ff3e },
	{ "OliveDrab2",	0xb3ee3a },
	{ "OliveDrab3",	0x9acd32 },
	{ "OliveDrab4",	0x698b22 },
	{ "orange",	0xffa500 },
	{ "orange red",	0xff4500 },
	{ "orange1",	0xffa500 },
	{ "orange2",	0xee9a00 },
	{ "orange3",	0xcd8500 },
	{ "orange4",	0x8b5a00 },
	{ "OrangeRed",	0xff4500 },
	{ "OrangeRed1",	0xff4500 },
	{ "OrangeRed2",	0xee4000 },
	{ "OrangeRed3",	0xcd3700 },
	{ "OrangeRed4",	0x8b2500 },
	{ "orchid",	0xda70d6 },
	{ "orchid1",	0xff83fa },
	{ "orchid2",	0xee7ae9 },
	{ "orchid3",	0xcd69c9 },
	{ "orchid4",	0x8b4789 },
	{ 0,	0 }
};


// Start group p
static ColorTableEntry	ctab_p[] =
{
	{ "pale goldenrod",	0xeee8aa },
	{ "pale green",	0x98fb98 },
	{ "pale turquoise",	0xafeeee },
	{ "pale violet red",	0xdb7093 },
	{ "PaleGoldenrod",	0xeee8aa },
	{ "PaleGreen",	0x98fb98 },
	{ "PaleGreen1",	0x9aff9a },
	{ "PaleGreen2",	0x90ee90 },
	{ "PaleGreen3",	0x7ccd7c },
	{ "PaleGreen4",	0x548b54 },
	{ "PaleTurquoise",	0xafeeee },
	{ "PaleTurquoise1",	0xbbffff },
	{ "PaleTurquoise2",	0xaeeeee },
	{ "PaleTurquoise3",	0x96cdcd },
	{ "PaleTurquoise4",	0x668b8b },
	{ "PaleVioletRed",	0xdb7093 },
	{ "PaleVioletRed1",	0xff82ab },
	{ "PaleVioletRed2",	0xee799f },
	{ "PaleVioletRed3",	0xcd6889 },
	{ "PaleVioletRed4",	0x8b475d },
	{ "papaya whip",	0xffefd5 },
	{ "PapayaWhip",	0xffefd5 },
	{ "peach puff",	0xffdab9 },
	{ "PeachPuff",	0xffdab9 },
	{ "PeachPuff1",	0xffdab9 },
	{ "PeachPuff2",	0xeecbad },
	{ "PeachPuff3",	0xcdaf95 },
	{ "PeachPuff4",	0x8b7765 },
	{ "peru",	0xcd853f },
	{ "pink",	0xffc0cb },
	{ "pink1",	0xffb5c5 },
	{ "pink2",	0xeea9b8 },
	{ "pink3",	0xcd919e },
	{ "pink4",	0x8b636c },
	{ "plum",	0xdda0dd },
	{ "plum1",	0xffbbff },
	{ "plum2",	0xeeaeee },
	{ "plum3",	0xcd96cd },
	{ "plum4",	0x8b668b },
	{ "powder blue",	0xb0e0e6 },
	{ "PowderBlue",	0xb0e0e6 },
	{ "purple",	0xa020f0 },
	{ "purple1",	0x9b30ff },
	{ "purple2",	0x912cee },
	{ "purple3",	0x7d26cd },
	{ "purple4",	0x551a8b },
	{ 0,	0 }
};

static ColorTableEntry	ctab_q[] =
{
	{ 0,	0 }
};


// Start group r
static ColorTableEntry	ctab_r[] =
{
	{ "red",	0xff0000 },
	{ "red1",	0xff0000 },
	{ "red2",	0xee0000 },
	{ "red3",	0xcd0000 },
	{ "red4",	0x8b0000 },
	{ "rosy brown",	0xbc8f8f },
	{ "RosyBrown",	0xbc8f8f },
	{ "RosyBrown1",	0xffc1c1 },
	{ "RosyBrown2",	0xeeb4b4 },
	{ "RosyBrown3",	0xcd9b9b },
	{ "RosyBrown4",	0x8b6969 },
	{ "royal blue",	0x4169e1 },
	{ "RoyalBlue",	0x4169e1 },
	{ "RoyalBlue1",	0x4876ff },
	{ "RoyalBlue2",	0x436eee },
	{ "RoyalBlue3",	0x3a5fcd },
	{ "RoyalBlue4",	0x27408b },
	{ 0,	0 }
};


// Start group s
static ColorTableEntry	ctab_s[] =
{
	{ "saddle brown",	0x8b4513 },
	{ "SaddleBrown",	0x8b4513 },
	{ "salmon",	0xfa8072 },
	{ "salmon1",	0xff8c69 },
	{ "salmon2",	0xee8262 },
	{ "salmon3",	0xcd7054 },
	{ "salmon4",	0x8b4c39 },
	{ "sandy brown",	0xf4a460 },
	{ "SandyBrown",	0xf4a460 },
	{ "sea green",	0x2e8b57 },
	{ "SeaGreen",	0x2e8b57 },
	{ "SeaGreen1",	0x54ff9f },
	{ "SeaGreen2",	0x4eee94 },
	{ "SeaGreen3",	0x43cd80 },
	{ "SeaGreen4",	0x2e8b57 },
	{ "seashell",	0xfff5ee },
	{ "seashell1",	0xfff5ee },
	{ "seashell2",	0xeee5de },
	{ "seashell3",	0xcdc5bf },
	{ "seashell4",	0x8b8682 },
	{ "sienna",	0xa0522d },
	{ "sienna1",	0xff8247 },
	{ "sienna2",	0xee7942 },
	{ "sienna3",	0xcd6839 },
	{ "sienna4",	0x8b4726 },
	{ "sky blue",	0x87ceeb },
	{ "SkyBlue",	0x87ceeb },
	{ "SkyBlue1",	0x87ceff },
	{ "SkyBlue2",	0x7ec0ee },
	{ "SkyBlue3",	0x6ca6cd },
	{ "SkyBlue4",	0x4a708b },
	{ "slate blue",	0x6a5acd },
	{ "slate gray",	0x708090 },
	{ "slate grey",	0x708090 },
	{ "SlateBlue",	0x6a5acd },
	{ "SlateBlue1",	0x836fff },
	{ "SlateBlue2",	0x7a67ee },
	{ "SlateBlue3",	0x6959cd },
	{ "SlateBlue4",	0x473c8b },
	{ "SlateGray",	0x708090 },
	{ "SlateGray1",	0xc6e2ff },
	{ "SlateGray2",	0xb9d3ee },
	{ "SlateGray3",	0x9fb6cd },
	{ "SlateGray4",	0x6c7b8b },
	{ "SlateGrey",	0x708090 },
	{ "snow",	0xfffafa },
	{ "snow1",	0xfffafa },
	{ "snow2",	0xeee9e9 },
	{ "snow3",	0xcdc9c9 },
	{ "snow4",	0x8b8989 },
	{ "spring green",	0x00ff7f },
	{ "SpringGreen",	0x00ff7f },
	{ "SpringGreen1",	0x00ff7f },
	{ "SpringGreen2",	0x00ee76 },
	{ "SpringGreen3",	0x00cd66 },
	{ "SpringGreen4",	0x008b45 },
	{ "steel blue",	0x4682b4 },
	{ "SteelBlue",	0x4682b4 },
	{ "SteelBlue1",	0x63b8ff },
	{ "SteelBlue2",	0x5cacee },
	{ "SteelBlue3",	0x4f94cd },
	{ "SteelBlue4",	0x36648b },
	{ 0,	0 }
};


// Start group t
static ColorTableEntry	ctab_t[] =
{
	{ "tan",	0xd2b48c },
	{ "tan1",	0xffa54f },
	{ "tan2",	0xee9a49 },
	{ "tan3",	0xcd853f },
	{ "tan4",	0x8b5a2b },
	{ "thistle",	0xd8bfd8 },
	{ "thistle1",	0xffe1ff },
	{ "thistle2",	0xeed2ee },
	{ "thistle3",	0xcdb5cd },
	{ "thistle4",	0x8b7b8b },
	{ "tomato",	0xff6347 },
	{ "tomato1",	0xff6347 },
	{ "tomato2",	0xee5c42 },
	{ "tomato3",	0xcd4f39 },
	{ "tomato4",	0x8b3626 },
	{ "turquoise",	0x40e0d0 },
	{ "turquoise1",	0x00f5ff },
	{ "turquoise2",	0x00e5ee },
	{ "turquoise3",	0x00c5cd },
	{ "turquoise4",	0x00868b },
	{ 0,	0 }
};

static ColorTableEntry	ctab_u[] =
{
	{ 0,	0 }
};


// Start group v
static ColorTableEntry	ctab_v[] =
{
	{ "violet",	0xee82ee },
	{ "violet red",	0xd02090 },
	{ "VioletRed",	0xd02090 },
	{ "VioletRed1",	0xff3e96 },
	{ "VioletRed2",	0xee3a8c },
	{ "VioletRed3",	0xcd3278 },
	{ "VioletRed4",	0x8b2252 },
	{ 0,	0 }
};


// Start group w
static ColorTableEntry	ctab_w[] =
{
	{ "wheat",	0xf5deb3 },
	{ "wheat1",	0xffe7ba },
	{ "wheat2",	0xeed8ae },
	{ "wheat3",	0xcdba96 },
	{ "wheat4",	0x8b7e66 },
	{ "white",	0xffffff },
	{ "white smoke",	0xf5f5f5 },
	{ "WhiteSmoke",	0xf5f5f5 },
	{ 0,	0 }
};

static ColorTableEntry	ctab_x[] =
{
	{ 0,	0 }
};


// Start group y
static ColorTableEntry	ctab_y[] =
{
	{ "yellow",	0xffff00 },
	{ "yellow green",	0x9acd32 },
	{ "yellow1",	0xffff00 },
	{ "yellow2",	0xeeee00 },
	{ "yellow3",	0xcdcd00 },
	{ "yellow4",	0x8b8b00 },
	{ "YellowGreen",	0x9acd32 },
	{ 0,	0 }
};



static ColorTableEntry	ctab_z[] =
{
	{ 0,	0 }
};



static ColorTableEntry *ctab[26] =
	{
	ctab_a,
	ctab_b,
	ctab_c,
	ctab_d,
	ctab_e,
	ctab_f,
	ctab_g,
	ctab_h,
	ctab_i,
	ctab_j,
	ctab_k,
	ctab_l,
	ctab_m,
	ctab_n,
	ctab_o,
	ctab_p,
	ctab_q,
	ctab_r,
	ctab_s,
	ctab_t,
	ctab_u,
	ctab_v,
	ctab_w,
	ctab_x,
	ctab_y,
	ctab_z
	};



void
FXColor::set(const SWString &name, const FXColor &defaultColor)
		{
		*this = defaultColor;

		char	first=0;
		int		v;

		if (name.length() > 0) first = name[0];

		if (first != 0)
			{
			if (first == '#')
				{
				// We expect a color value in the form #rrggbb or #rgb
				SWString	s=name.mid(1);
				int			r=0, g=0, b=0;

				v = s.toInt32(16); //_stscanf(name+1, _T("%x"), &v);
				if (s.length() == 3)
					{
					int	t;

					t = (v>>8)&0xf;
					r = t | (t << 4);

					t = (v>>4)&0xf;
					g = t | (t << 4);

					t = v&0xf;
					b = t | (t << 4);
					}
				else
					{
					r = (v>>16)&0xff;
					g = (v>>8)&0xff;
					b = v&0xff;
					}

				set(8, r, g, b, 255);
				}
/*
			else if (name.compareNoCase("none") == 0)
				{
				color = CLR_NONE;
				}
*/
			else
				{
				if (first >= 'A' && first <= 'Z') first = first - 'A' + 'a';
				if (first >= 'a' && first <= 'z')
					{
					ColorTableEntry	*tp = ctab[first-'a'];

					while (tp->name)
						{
						if (name.compareNoCase(tp->name) == 0)
							{
							v = tp->color;

							int		r, g, b;

							r = (v>>16)&0xff;
							g = (v>>8)&0xff;
							b = v&0xff;
							set(8, r, g, b, 255);
							break;
							}
						tp++;
						}
					}
				}
			}
		}


void
FXColor::select() const
		{
		int r, g, b, a;

		getComponents(8, r, g, b, a);
		fl_color(r, g, b);
		}


sw_uint32_t
FXColor::rgba32(sw_byte_t r, sw_byte_t g, sw_byte_t b, sw_byte_t a)
		{
		sw_uint32_t	res=0;

		res = ((sw_uint32_t)a << 24) | ((sw_uint32_t)b << 16) | ((sw_uint32_t)g << 8) | r;

		return res;
		}


sw_uint32_t
FXColor::toRGBA32() const
		{
		int r, g, b, a;

		getComponents(8, r, g, b, a);

		return rgba32(r, g, b, a);
		}


sw_uint32_t
FXColor::toRGB32() const
		{
		int r, g, b, a;

		getComponents(8, r, g, b, a);

		return rgba32(r, g, b, 0);
		}



COLORREF
FXColor::toCOLORREF() const
		{
		int r, g, b, a;

		getComponents(8, r, g, b, a);

		return RGB(r, g, b);
		}


int
FXColor::to_fl_color() const
		{
		int r, g, b, a;

		getComponents(8, r, g, b, a);
		return fl_rgb_color(r, g, b);
		}

void
FXColor::getComponents(int nbits, int &r, int &g, int &b) const
		{
		int		a;

		getComponents(nbits, r, g, b, a);
		}

void
FXColor::getComponents(int nbits, int &r, int &g, int &b, int &a) const
		{
		r = m_red;
		g = m_green;
		b = m_blue;
		a = m_alpha;

		if (nbits > bits())
			{
			int	shift=nbits-bits();

			r <<= shift;
			g <<= shift;
			b <<= shift;
			a <<= shift;
			}
		else if (nbits < bits())
			{
			int	shift=bits()-nbits;

			r >>= shift;
			g >>= shift;
			b >>= shift;
			a >>= shift;
			}
		}

SWString
FXColor::name(bool webonly) const
		{
		SWString		name;
		ColorTableEntry	*entry=NULL;
		int				r;
		int				g;
		int				b;

		getComponents(8, r, g, b);

		if (!webonly)
			{
			/*
			if (color == CLR_NONE)
				{
				name = "none";
				}
			else
			*/
				{
				int	c = r<<16|g<<8|b;
				int	i;
				ColorTableEntry	*tp;

				for (i=0;entry==NULL&&i<26;i++)
					{
					tp = ctab[i];

					while (tp->name)
						{
						if (tp->color == c && strchr(tp->name, ' ') == NULL)
							{
							entry = tp;
							break;
							}

						tp++;
						}
					}
				}
			}

		if (name.isEmpty())
			{
			if (entry == NULL) name.format(_T("#%02x%02x%02x"), r, g, b);
			else name = entry->name;
			}

		return name;
		}


/*
bool
SelectColor(COLORREF &color, CWnd *pParent)
		{
		bool			res=false;
		CColorDialog	dlg(color, CC_FULLOPEN, pParent);
		
		if (dlg.DoModal() == IDOK)
			{
			color = dlg.m_cc.rgbResult;
			res = true;
			}

		return res;
		}
*/


FXColor
FXColor::toMonochrome() const
		{
		FXColor res(*this);

		if (res != FX_COLOR_NONE)
			{
			int		v=(int)((red() * 0.30) + (green() * 0.59) + (blue() * 0.11));
			int		maxvalue;

			maxvalue = (1 << bits()) - 1;

			if (v < (maxvalue / 2)) v = 0;
			else v = maxvalue;

			res.set(bits(), v, v, v, alpha());
			}
		
		return res;
		}


FXColor
FXColor::toGrayscale() const
		{
		FXColor res(*this);

		if (res != FX_COLOR_NONE)
			{
			int		v=(int)((red() * 0.30) + (green() * 0.59) + (blue() * 0.11));

			res.set(bits(), v, v, v, alpha());
			}
		
		return res;
		}

FXColor
FXColor::toOpposite() const
		{
		FXColor res(*this);

		if (res != FX_COLOR_NONE)
			{
			int		nbits=bits(), r, g, b, a;
			int		maxvalue;

			getComponents(nbits, r, g, b, a);
			maxvalue = (1 << nbits) - 1;

			r = maxvalue - r;
			g = maxvalue - g;
			b = maxvalue - b;

			res.set(nbits, r, g, b, a);
			}
		
		return res;
		}


FXColor
FXColor::toShade(double factor) const
		{
		FXColor res(*this);

		if (res != FX_COLOR_NONE)
			{
			if (factor < 0.0) factor = 0.0;

			double	r = factor * (double)res.m_red;
			double	g = factor * (double)res.m_green;
			double	b = factor * (double)res.m_blue;

			if (r > m_mask) r = m_mask;
			if (g > m_mask) g = m_mask;
			if (b > m_mask) b = m_mask;
		
			res.m_red = (int)r;
			res.m_green = (int)g;
			res.m_blue = (int)b;
			}

		return res;
		}

FXColor
FXColor::getColorByName(const SWString &name, const FXColor &defaultColor)
		{
		return FXColor(name, defaultColor);
		}

SWString
FXColor::getColorName(const FXColor &color, bool webonly)
		{
		return color.name(webonly);
		}

FXColor
FXColor::getGrayscale(const FXColor &color)
		{
		return color.toGrayscale();
		}

FXColor
FXColor::getShade(const FXColor &color, double factor)
		{
		return color.toShade(factor);
		}

FXColor
FXColor::fromFlColor(Fl_Color v)
		{
		uchar	r, g, b;

		Fl::get_color(v, r, g, b);
		
		return FXColor(r, g, b);
		}