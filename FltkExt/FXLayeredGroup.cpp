#include "FXLayeredGroup.h"
#include "FXLayeredViewGroup.h"
#include "FXLayeredViewWidget.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWFlags.h>

FXLayeredGroup::FXLayeredGroup(int X, int Y, int W, int H) :
	FXGroup(X, Y, W, H),
	m_pCurrentLayer(NULL)
		{
		}


FXLayeredGroup::~FXLayeredGroup()
		{
		}



void
FXLayeredGroup::add(Fl_Widget &w)
		{
		addLayer(&w);
		}


void
FXLayeredGroup::add(Fl_Widget *o)
		{
		addLayer(o);
		}

void
FXLayeredGroup::addLayer(int id, Fl_Widget *o)
		{
		o->id(id);
		addLayer(o);
		}

void
FXLayeredGroup::addLayer(Fl_Widget *o)
		{
		m_layers.add(o);
		if (m_layers.size() == 1)
			{
			Fl_Group::add(o);
			activateLayer(o);
			}
		else
			{
			o->hide();
			}
		}




bool
FXLayeredGroup::showLayer(int id, bool hideIfNotFound)
		{
		bool	res=false;
		bool	found=false;

		for (int i=0;i<(int)m_layers.size();i++)
			{
			if ((int)m_layers[i]->id() == id)
				{
				found = true;
				res = activateLayer(m_layers[i]);
				break;
				}
			}
		
		if (!found && hideIfNotFound)
			{
			res = activateLayer(NULL);
			}
		
		return res;
		}


bool
FXLayeredGroup::showLayer(Fl_Widget *pPanel, bool hideIfNotFound)
		{
		bool	res=false;
		bool	found=false;

		for (int i=0;i<(int)m_layers.size();i++)
			{
			if (m_layers[i] == pPanel)
				{
				found = true;
				res = activateLayer(m_layers[i]);
				break;
				}
			}
		
		if (!found && hideIfNotFound)
			{
			res = activateLayer(NULL);
			}

		return res;
		}


bool
FXLayeredGroup::hideLayer()
		{
		return activateLayer(NULL);
		}


bool
FXLayeredGroup::activateLayer(Fl_Widget *pPanel)
		{
		bool	res=true;

		if (m_pCurrentLayer != pPanel)
			{
			if (m_pCurrentLayer != NULL)
				{
				FXLayeredViewGroup	*pViewGroup=dynamic_cast<FXLayeredViewGroup *>(m_pCurrentLayer);
				FXLayeredViewWidget	*pViewWidget=dynamic_cast<FXLayeredViewWidget *>(m_pCurrentLayer);

				if (pViewGroup != NULL) pViewGroup->onViewClose();
				if (pViewWidget != NULL) pViewWidget->onViewClose();
				m_pCurrentLayer->hide();
				Fl_Group::remove(m_pCurrentLayer);
				m_pCurrentLayer = NULL;
				}

			if (res && pPanel != NULL)
				{
				Fl_Group::add(pPanel);

				m_pCurrentLayer = pPanel;
				m_pCurrentLayer->resize(x(), y(), w(), h());
				m_pCurrentLayer->show();
				m_pCurrentLayer->take_focus();

				FXLayeredViewGroup	*pViewGroup=dynamic_cast<FXLayeredViewGroup *>(m_pCurrentLayer);
				FXLayeredViewWidget	*pViewWidget=dynamic_cast<FXLayeredViewWidget *>(m_pCurrentLayer);

				if (pViewGroup != NULL) pViewGroup->onViewOpen();
				if (pViewWidget != NULL) pViewWidget->onViewOpen();
				}
			}

		invalidate();

		return res;
		}


int
FXLayeredGroup::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (res == 0)
			{
			FXGroup	*p=dynamic_cast<FXGroup *>(m_pCurrentLayer);

			if (p != NULL)
				{
				res = p->onKeyDown(code, text, pt);
				}
			}

		if (res == 0)
			{
			FXWidget *p=dynamic_cast<FXWidget *>(m_pCurrentLayer);

			if (p != NULL)
				{
				res = p->onKeyDown(code, text, pt);
				}
			}


		if (res == 0)
			{
			res = FXGroup::onKeyDown(code, text, pt);
			}

		return res;
		}


int
FXLayeredGroup::onShortcut(int code, const FXPoint &pt)
		{
		int		res=0;

		if (res == 0)
			{
			FXGroup	*p=dynamic_cast<FXGroup *>(m_pCurrentLayer);

			if (p != NULL)
				{
				res = p->onShortcut(code, pt);
				}
			}

		if (res == 0)
			{
			FXWidget *p=dynamic_cast<FXWidget *>(m_pCurrentLayer);

			if (p != NULL)
				{
				res = p->onShortcut(code, pt);
				}
			}

		if (res == 0)
			{
			res = FXGroup::onShortcut(code, pt);
			}

		return res;
		}