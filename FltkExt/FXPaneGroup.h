#ifndef __FltkExt_FXPaneGroup_h__
#define __FltkExt_FXPaneGroup_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>

#include <FL/Fl_Group.H>

/**
*** A special group to manage the child widgets as panes in a window
**/
class FLTKEXT_DLL_EXPORT FXPaneGroup : public Fl_Group
		{
public:
		FXPaneGroup(int x, int y, int w, int h, bool vertical);
		virtual ~FXPaneGroup();

		virtual void	resize(int X, int Y, int W, int H);

		// void			setChildSizes(int idx, int minsize, int maxsize);
		void			getChildSizes(int idx, int &minsize, int &maxsize);

protected: // Override Fl_Group methods we want to hide
		void	add(Fl_Widget &w)	{ Fl_Group::add(w); }
		void	add(Fl_Widget* o)	{ Fl_Group::add(*o); }

protected:
		bool			m_vertical;
		};

#endif // __FltkExt_FXPaneGroup_h__
