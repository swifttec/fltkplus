/*
**	FXDialog.h	A generic dialog class
*/

#ifndef __FltkExt_FXPromptNumberDialog_h__
#define __FltkExt_FXPromptNumberDialog_h__

#include <FltkExt/FXDialog.h>


/**
*** A popup dialog window for entering a number
**/
class FLTKEXT_DLL_EXPORT FXPromptNumberDialog : public FXDialog
		{
public:
		FXPromptNumberDialog(const SWString &title, const SWString &prompt, int value, Fl_Widget *pParent=NULL);
		virtual ~FXPromptNumberDialog();

		int				value() const	{ return m_value; }
		
		void			setValueRange(int minv, int maxv);

		virtual void	onOK();

protected:
		int			m_value;
		bool		m_hasminmax;
		int			m_minValue;
		int			m_maxValue;
		};


#endif // __FltkExt_FXPromptNumberDialog_h__
