#ifndef __FltkExt_XDC_h__
#define __FltkExt_XDC_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXRect.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXFont.h>
#include <FltkExt/FXSize.h>
#include <FltkExt/FXBrush.h>
#include <FltkExt/FXPen.h>
#include <FltkExt/FXColorFont.h>
#include <FltkExt/fx_draw.h>
// #include <FltkExt/GObject.h>
// #include <FltkExt/CXMemDC.h>

#include <swift/SWString.h>


#define XD_LEFT			FX_HALIGN_LEFT
#define XD_CENTER		FX_HALIGN_CENTER
#define XD_RIGHT		FX_HALIGN_RIGHT

#define XD_TOP			FX_VALIGN_TOP
#define XD_MIDDLE		FX_VALIGN_MIDDLE
#define XD_BOTTOM		FX_VALIGN_BOTTOM

#define XD_OPAQUE		FX_BG_OPAQUE
#define XD_TRANSPARENT	FX_BG_TRANSPARENT

#define XD_OUTLINE		FX_OUTLINE
#define XD_SHADOW		FX_SHADOW
#define XD_NOCLIP		FX_NOCLIP
#define XD_NOWRAP		FX_NOWRAP
#define XD_SHRINKTOFIT	FX_SHRINKTOFIT
#define XD_OPAQUE_TEXT	FX_OPAQUE_TEXT

#ifdef USE_XDC
class XMemDC;

class FLTKEXT_DLL_EXPORT XDCInfo
		{
public:
		XDCInfo();

public:
		FXColor	fgColor;			// The current fg/text color
		FXColor	bgColor;			// The current bg color
		int		bgMode;				// The current bg mode (opaque or transparent)
		FXColor	shadowColor;		// The current shadow color
		int		shadowXOffset;		// The current shadow X offset
		int		shadowYOffset;		// The current shadow Y offset
		FXColor	outlineColor;		// The current outline color
		int		outlineWidth;		// The current outline width
		FXPoint	point;				// The current point (used in drawing operations)
		FXPen	pen;
		FXBrush	brush;
		FXFont	font;
		};


class FLTKEXT_DLL_EXPORT XDC
		{
friend class XDCSave;
friend class XMemDC;

public:
		enum Contexts
			{
			Screen=0,

			// Last one
			MaxContext
			};

public:
		XDC();
		virtual ~XDC();

		void			draw(const SWString &text, FXRect &dr, sw_uint32_t flags);

		// Draw text - does not support shrink to fit flag
		void			drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags);

		// Draw text - with support for shrink to fit flag
		void			drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXColorFont &font, double minsize=4.0, double *pSizeUsed=NULL);
		void			drawText(const SWString &text, const FXRect &dr, sw_uint32_t flags, const FXFont &font, const FXColor &textcolor=FXColor(0, 0, 0), double minsize=4.0, double *pSizeUsed=NULL);
		void			drawBox(const FXRect &br, const FXColor &bdcolor, int bdwidth, const FXColor &bgcolor, int cornersize);

		int				getTextHeight(const SWString &str, int width, sw_uint32_t flags) const;
		FXSize			getTextExtent(const SWString &str, int width, sw_uint32_t flags) const;
		FXSize			getTextExtent(const SWString &str) const;
		FXSize			GetTextExtent(const SWString &str) const	{ return getTextExtent(str); }

		void			setOutlineColor(const FXColor &v);
		void			setOutlineWidth(int v);
		void			setShadowColor(const FXColor &v);
		void			setShadowOffset(int v);
		void			setShadowOffset(int x, int y);
		void			setTextColor(const FXColor &v);
		void			setBgColor(const FXColor &v);

		const XDCInfo &	saveDC();
		void			restoreDC(const XDCInfo &v);

		void			select(const FXFont &font);
		void			select(const FXPen &pen);
		void			select(const FXBrush &brush);
		void			select(const FXColor &color);

		void			fillRect(const FXRect &dr, const FXColor &color);
		void			fillRect(const FXRect &dr, const FXBrush &brush);
		void			frameRect(const FXRect &dr, const FXColor &color);
		void			frameRect(const FXRect &dr, const FXBrush &brush);

		void			setBgMode(int mode);
		int				getBgMode() const;

		// MFC Emulation
		void			SetBkMode(int v);
		void			SetTextColor(const FXColor &v)		{ setTextColor(v); }
		void			SetBkColor(const FXColor &v)		{ setBgColor(v); }
		void			FrameRect(FXRect *pRect, FXBrush *pBrush);
		void			FrameRect(const FXRect &dr, const FXColor &color)	{ frameRect(dr, color); }
		void			FillRect(const FXRect &dr, const FXColor &color)	{ fillRect(dr, color); }
		void			FillRect(FXRect *pRect, FXBrush *pBrush);
		void			FillSolidRect(FXRect *pRect, const FXColor &color);
		void			SelectObject(const FXFont *font);
		void			SelectObject(const FXPen *pen);
		void			SelectObject(const FXBrush *brush);
		void			SelectObject(const FXColor &color);

		operator XDC *()									{ return this; }

public:
		static XDC &	getDC(int context=XDC::Screen);

protected:
		XDCInfo		m_info;
		};

//////////////////////////////////////////////////
// XMemDC - memory DC - based on CMemDC code but reworked for SwiftTec extended XDC class
//
// Author: Keith Rule
// Email:  keithr@europa.com
// Copyright 1996-1997, Keith Rule
//
// You may freely use or modify this code provided this
// Copyright is included in all derived versions.
//
// History - 10/3/97 Fixed scrolling bug.
//                   Added print support.
//           25 feb 98 - fixed minor assertion bug
//
// This class implements a memory Device Context

class FLTKEXT_DLL_EXPORT XMemDC : public XDC
		{
friend class XDC;

public:
		// constructor sets up the memory DC
		XMemDC(XDC &xdc);
		XMemDC(XDC *pXDC);

		// Destructor copies the contents of the mem DC to the original DC
		~XMemDC();

		// Allow usage as a pointer
		XMemDC * operator->()	{ return this; }
        
		// Allow usage as a pointer
		operator XMemDC *()		{ return this; }

protected:
		void	init(XDC *pXDC);

protected:
		XDC			*m_pXDC;		// Saves XDC passed in constructor
		FXRect	    m_rect;			// Rectangle of drawing area.
		bool	    m_bMemDC;		// TRUE if XDC really is a Memory DC.
		};


/**
*** @brief	Auto save/restore of DC.
***
*** The XDCSave class is used to save the current DC context on construction
*** and restore it on destruction.
***
*** When an instance of a XDCSave object is created the SaveDC method is
*** called on the given device context. At destruction the RestoreDC method
*** is called on the device context. A typical use would be to declare a 
*** XDCSave object at the start of a function that will change the DC so that it
*** will be restored when the function is exited.
**/
class FLTKEXT_DLL_EXPORT XDCSave
	{
public:
		XDCSave(XDC *dc);
		XDCSave(XDC &dc);
		virtual ~XDCSave();

private:
		XDC		*m_pDC;	    ///! The device context
		XDCInfo	m_savedDC;   ///! The integer returned from SaveDC
		};

#endif // USE_XDC

#endif // __FltkExt_XDC_h__
