/*
**	FXCheckBox.h	A generic dialog class
*/

#ifndef __FltkExt_FXCheckBox_h__
#define __FltkExt_FXCheckBox_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/fx_functions.h>
#include <FltkExt/fx_generic_ids.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXScrollBar.h>

#include <FL/Fl_Box.H>

class FLTKEXT_DLL_EXPORT FXCheckBox : public FXWidget
		{
public:
		enum
			{
			ItemDisabled=-1,
			ItemUnselected=0,
			ItemTicked=1,
			ItemCrossed=2
			};

public:
		FXCheckBox(const SWString &text, int state, int X=0, int Y=0, int W=100, int H=24, const FXColor &fgcolor="#000", const FXColor &bgcolor=FXColor::NoColor);
		FXCheckBox(int X, int Y, int W, int H, const SWString &text, int state=0, const FXColor &fgcolor="#000", const FXColor &bgcolor=FXColor::NoColor);

		/// Set the font for this text
		void		setFont(const FXFont &font)		{ m_font = font; }

		void		update(const SWString &text, int state);
		void		update(const SWString &text, int state, const FXFont &font, const FXColor &textcolor, const FXColor &boxcolor="#000");
		int			getState();
		void		setState(int v);

		void		setAlignment(int v)				{ m_align = v; }

protected:

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onMouseDown(int button, const FXPoint &pt);

		void	drawTick(const FXRect &dr, const FXColor &color);
		void	drawCross(const FXRect &dr, const FXColor &color);

protected:
		SWString	m_text;			///< The actual text
		int			m_align;		///< The text alignment
		FXFont		m_font;			///< The font to use
		int			m_style;		///< The draw style
		int			m_state;		///< The check state
		FXRect		m_rItem;		///< The rect of the checkbox
		FXRect		m_rCheckbox;	///< The rect of the checkbox
		FXColor		m_boxcolor;
		int			m_boxwidth;
		bool		m_visible;
		bool		m_selected;
		};


#endif // __FltkExt_FXCheckBox_h__
