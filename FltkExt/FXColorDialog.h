#ifndef __FltkExt_FXColorDialog_h__
#define __FltkExt_FXColorDialog_h__

#include <FltkExt/FltkExt.h>
#include <FltkExt/FXColor.h>
#include <FltkExt/FXDialog.h>

#include <FL/Fl_Color_Chooser.H>

class FLTKEXT_DLL_EXPORT FXColorDialog : public FXDialog
		{
public:
		FXColorDialog(const FXColor &color, const SWString &title, Fl_Widget *pOwner=NULL);
		~FXColorDialog();

		virtual void	onOK();
		
		FXColor		getColor();

protected:
		/// Called when a item issues a callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

		/// Called to exchanged data between dialog items and the application
		virtual void	doDataExchange(FXDataExchange &dx);

protected:
		FXColor				m_color;
		SWString			m_name;
		Fl_Color_Chooser	*m_pChooser;
		};


#endif // __FltkExt_FXColorDialog_h__
