#include <FltkExt/FXFont.h>
#include <FltkExt/XDC.h>
#include <FltkExt/FXFontFamily.h>
#include <FltkExt/fx_draw.h>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

static FXFont		**defaultFontList=NULL;
static int			defaultFontCapacity=0;

static void initDefaultFonts()
		{
		if (defaultFontList == NULL)
			{
			defaultFontCapacity = FXFont::MaxDefaultFonts;
			defaultFontList = new FXFont*[FXFont::MaxDefaultFonts];
			memset(defaultFontList, 0, FXFont::MaxDefaultFonts * sizeof(FXFont *));

			for (int i=0;i<FXFont::MaxDefaultFonts;i++)
				{
				defaultFontList[i] = new FXFont("Arial", 11, 0);
				}
			}
		}

FXFont
FXFont::getDefaultFont(int idx)
		{
		FXFont	font;

		if (defaultFontList == NULL) initDefaultFonts();

		if (idx < 0 || idx >= MaxDefaultFonts) idx = General;

		if (idx >= 0 && idx < MaxDefaultFonts)
			{
			if (defaultFontList[idx] == NULL) idx = General;
			font = *defaultFontList[idx];
			}
		
		return font;
		}


void
FXFont::setDefaultFont(int idx, const FXFont &font)
		{
		if (defaultFontList == NULL) initDefaultFonts();

		if (idx >= 0 && idx < MaxDefaultFonts)
			{
			if (defaultFontList[idx] == NULL)
				{
				defaultFontList[idx] = new FXFont();
				}

			*defaultFontList[idx] = font;
			}
		}


FXFont::FXFont()
		{
		// Special constructor to avoid the use of FXFontFamily
		m_name = "";
		m_style = 0;
		m_ptsize = pixelsToPointSize(11, 96.0);
		m_flface = 0;
		m_family = 0;
		}


FXFont::FXFont(const SWString &facename, double ptsize, int nstyle)
		{
		set(facename, ptsize, nstyle);
		}


FXFont::FXFont(const SWString &facename, int lineheight, int nstyle)
		{
		set(facename, lineheight, nstyle);
		}


void
FXFont::set(int fontid, int lineheight)
		{
		FXFontFamily::find(fontid, m_family, m_style);

		FXFontFamily	ff=FXFontFamily::getFamily(m_family);
		double			ptsize=pixelsToPointSize(lineheight);

		m_name = ff.name();
		if (ff.isScalable())
			{
			m_ptsize = ptsize;
			}
		else
			{
			// Find the nearest size
			const SWIntegerArray &	fslist=ff.getFontSizes();
			bool					found=false;

			for (int i=0;i<(int)fslist.size();i++)
				{
				int		fs=fslist.get(i);

				if (fs == (int)ptsize)
					{
					m_ptsize = fs;
					found = true;
					break;
					}
				else if (fs > (int)ptsize)
					{
					if (i > 0) --i;
					m_ptsize = fslist.get(i);
					found = true;
					break;
					}
				}

			if (!found)
				{
				if ((int)ptsize < fslist.get(0)) m_ptsize = fslist.get(0);
				else m_ptsize = fslist.get((int)fslist.size()-1);
				}
			}
		}


FXFont
FXFont::toStyle(int nstyle) const
		{
		FXFont	font;

		font.m_name = m_name;
		font.m_ptsize = m_ptsize;
		font.m_family = m_family;

		FXFontFamily	ff=FXFontFamily::getFamily(m_family);


		int	fstyle=nstyle & 3;

		if (ff.hasStyle(fstyle))
			{
			font.m_flface = ff.fl_font_face(fstyle);
			font.m_style = fstyle;
			}
		else if ((fstyle & Bold) != 0 && ff.hasStyle(Bold))
			{
			font.m_flface = ff.fl_font_face(Bold);
			font.m_style = Bold;
			}
		else if ((fstyle & Italic) != 0 && ff.hasStyle(Italic))
			{
			font.m_flface = ff.fl_font_face(Italic);
			font.m_style = Italic;
			}
		else
			{
			font.m_flface = ff.fl_font_face(Normal);
			font.m_style = Normal;
			}
			
		return font;
		}


void
FXFont::set(const SWString &facename, double ptsize, int nstyle)
		{
		m_family = FXFontFamily::find(facename);
		if (m_family < 0) m_family = 0;

		FXFontFamily	ff=FXFontFamily::getFamily(m_family);

		m_name = ff.name();
		if (ff.isScalable())
			{
			m_ptsize = ptsize;
			}
		else
			{
			// Find the nearest size
			const SWIntegerArray &	fslist=ff.getFontSizes();
			bool					found=false;

			for (int i=0;i<(int)fslist.size();i++)
				{
				int		fs=fslist.get(i);

				if (fs == (int)ptsize)
					{
					m_ptsize = fs;
					found = true;
					break;
					}
				else if (fs > (int)ptsize)
					{
					if (i > 0) --i;
					m_ptsize = fslist.get(i);
					found = true;
					break;
					}
				}

			if (!found)
				{
				if ((int)ptsize < fslist.get(0)) m_ptsize = fslist.get(0);
				else m_ptsize = fslist.get((int)fslist.size()-1);
				}
			}

		int	fstyle=nstyle & 3;

		if (ff.hasStyle(fstyle))
			{
			m_flface = ff.fl_font_face(fstyle);
			m_style = fstyle;
			}
		else if ((fstyle & Bold) != 0 && ff.hasStyle(Bold))
			{
			m_flface = ff.fl_font_face(Bold);
			m_style = Bold;
			}
		else if ((fstyle & Italic) != 0 && ff.hasStyle(Italic))
			{
			m_flface = ff.fl_font_face(Italic);
			m_style = Italic;
			}
		else
			{
			m_flface = ff.fl_font_face(Normal);
			m_style = Normal;
			}
		}


void
FXFont::set(const SWString &facename, int lineheight, int nstyle)
		{
		set(facename, pixelsToPointSize(lineheight), nstyle);
		}


FXFont::~FXFont()
		{
		}



/**
*** Copy constructor
**/
FXFont::FXFont(const FXFont &other)
		{
		*this = other;
		}



/**
*** Assignment operator
**/
FXFont &	
FXFont::operator=(const FXFont &other)
		{
#define COPY(x)	x = other.x
		COPY(m_name);
		COPY(m_style);
		COPY(m_ptsize);
		COPY(m_flface);
		COPY(m_family);
#undef COPY

		return *this;
		}


void
FXFont::select() const
		{
		fl_font(fl_face(), height());
		}

void
FXFont::name(const SWString &name)
		{
		set(name, m_ptsize, m_style);
		}


void
FXFont::pointsize(double v)
		{
		set(m_name, v, m_style);
		}


double
FXFont::pointsize() const
		{
		return m_ptsize;
		}

int
FXFont::pointSizeToPixels(double v, double dpi)
		{
		return (int)((v * dpi) / 72.0);
		}

int
FXFont::pointSizeToPixels(double v)
		{
		double	dpih, dpiv;

		fx_draw_getDPI(dpih, dpiv);

		return pointSizeToPixels(v, dpiv);
		}


double
FXFont::pixelsToPointSize(int v, double dpi)
		{
		return ((double)v * 72.0)/dpi;
		}

double
FXFont::pixelsToPointSize(int v)
		{
		double	dpih, dpiv;

		fx_draw_getDPI(dpih, dpiv);

		return pixelsToPointSize(v, dpiv);
		}


void
FXFont::height(int v)
		{
		set(m_name,  v, m_style);
		}


int
FXFont::height() const
		{
		return pointSizeToPixels(m_ptsize);
		}


void
FXFont::style(int v)
		{
		set(m_name,  m_ptsize, v);
		}


int
FXFont::style() const
		{
		return m_style;
		}


void
FXFont::addStyle(int v)
		{
		set(m_name,  m_ptsize, m_style | v);
		}


void
FXFont::removeStyle(int v)
		{
		set(m_name,  m_ptsize, m_style & ~v);
		}
