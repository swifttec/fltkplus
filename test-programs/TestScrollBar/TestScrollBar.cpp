
// TestScrollBarApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestScrollBar.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestScrollBar { TestScrollBarApp	theApp; }

FX_APP_EXECUTE(TestScrollBar::theApp);

namespace TestScrollBar {

TestScrollBarApp::TestScrollBarApp()
		{
		}


bool
TestScrollBarApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(800, 600);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestScrollBarApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
