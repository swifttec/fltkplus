#include "stdafx.h"

#include "TestScrollBar.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXScrollBar.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>

namespace TestScrollBar {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#fff");
		setForegroundColor("#000");

		add(m_pHScrollBar = new FXScrollBar(0, h()-24, w()-24, 24, false, this));
		add(m_pVScrollBar = new FXScrollBar(w()-24, 0, 24, h()-24, true, this));

		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;

		FXScrollInfo	si;

		m_pVScrollBar->getScrollInfo(si);
		si.setLineSize(5);
		m_pVScrollBar->setScrollInfo(si);
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}



void
MainWindow::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		SWString	s;
		
		s.format("Got callback for item %u", pWidget->id());

		redraw();
		}



void
MainWindow::onDraw()
		{
		FXWnd::onDraw();

		SWString	s;

		s.format("Window: %dx%d\n", w(), h());
		fl_font(FL_HELVETICA, 14);

		int		ypos=16;

		fl_draw(s, 10, ypos += 16);

		FXScrollInfo	si;

		m_pVScrollBar->getScrollInfo(si);
		s.format("Vertical: position=%d, range=%d-%d, page=%d\n", si.position(), si.rangeFrom(), si.rangeTo(), si.pageSize());
		fl_draw(s, 10, ypos += 16);

		m_pHScrollBar->getScrollInfo(si);
		s.format("Horizontal: position=%d, range=%d-%d, page=%d\n", si.position(), si.rangeFrom(), si.rangeTo(), si.pageSize());
		fl_draw(s, 10, ypos += 16);
		}


}
