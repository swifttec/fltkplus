/**
*** @defgroup	TestScrollBarApp	TestScrollBar
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestScrollBar {

class MainWindow;

/**
*** @ingroup TestScrollBarApp
**/
class TestScrollBarApp : public FXApp
		{
public:
		/// Constructor
		TestScrollBarApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestScrollBarApp theApp;

#define APP_TITLE	"TestScrollBar"

}
