#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>


#include <FltkExt/FXWnd.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXScrollBar.h>

namespace TestScrollBar {

/**
*** @ingroup TestScrollBarApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();
	
		/// Called to handle an item callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

protected:
		FXScrollBar		*m_pHScrollBar;
		FXScrollBar		*m_pVScrollBar;
		};

}

#endif // __MainWindow_h__
