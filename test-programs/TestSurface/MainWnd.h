// MainWnd.h : header file
//

#pragma once

#include <swift/SWThreadMutex.h>
#include <FltkExt/FXWnd.h>
#include <FL/Fl_Image_Surface.H>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();

public: // Overrides

		///
		virtual void	onDraw();

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);

		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);

protected:
		Fl_Offscreen		m_hSurface;
		FXSize				m_szSurface;
		SWThreadMutex		m_mutex;
		int					m_drawCount;
		int					m_frameCount;
		FXPoint				m_ptFrom;
		FXSize				m_szFrom;
		FXPoint				m_ptTo;
		FXSize				m_szTo;
		};
