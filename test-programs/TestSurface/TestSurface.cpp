
// TestSurface.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestSurface.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig;
TestSurface	theApp;

FX_APP_EXECUTE(theApp);

TestSurface::TestSurface() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestSurface::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestSurface::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
