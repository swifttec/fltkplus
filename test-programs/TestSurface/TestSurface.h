
// TestSurface.h : main header file for the TestSurface application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestSurface:
// See TestSurface.cpp for the implementation of this class
//
class MainWnd;

class TestSurface : public FXApp
		{
public:
		TestSurface();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestSurface theApp;
