// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestSurface.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Surface", 640, 480),
	m_hSurface(0),
	m_mutex(true),
	m_frameCount(0),
	m_drawCount(0),
	m_ptFrom(100, 100),
	m_szFrom(sw_math_randomInt32InRange(1,16), sw_math_randomInt32InRange(1,16)),
	m_ptTo(200, 200),
	m_szTo(sw_math_randomInt32InRange(1,16), sw_math_randomInt32InRange(1,16))
		{
		setBackgroundColor("#444");
		setIntervalTimer(1, 0.01, NULL);
		show();
		}



MainWnd::~MainWnd()
		{
		}




void
MainWnd::onDraw()
		{
		FXRect	cr;

		getContentsRect(cr);

		m_mutex.acquire();

		if (m_hSurface != 0)
			{
			fl_copy_offscreen(0, 0, m_szSurface.cx, m_szSurface.cy, m_hSurface, 0, 0); 
			}

		m_mutex.release();

		fx_draw_text(SWString().format("Draw %d, Frame %d", ++m_drawCount, m_frameCount), cr, FX_HALIGN_LEFT|FX_VALIGN_TOP, FXFont("Arial", 9, 0), "#f00");
		}

static void adjust(int &v, int &step, int min, int max)
		{
		v += step;
		if (v < min)
			{
			step = sw_math_randomInt32InRange(1,16);
			while (v < min) v += step;
			}
		else if (v > max)
			{
			step = -sw_math_randomInt32InRange(1,16);
			while (v > max) v += step;
			}
		}

bool
MainWnd::onTimer(sw_uint32_t id, void *data)
		{
		adjust(m_ptFrom.x, m_szFrom.cx, 0, m_szSurface.cx);
		adjust(m_ptFrom.y, m_szFrom.cy, 0, m_szSurface.cy);
		adjust(m_ptTo.x, m_szTo.cx, 0, m_szSurface.cx);
		adjust(m_ptTo.y, m_szTo.cy, 0, m_szSurface.cy);

		fl_begin_offscreen(m_hSurface);

		fl_color(fl_rgb_color(sw_math_randomUInt8(), sw_math_randomUInt8(), sw_math_randomUInt8()));
		fl_line(m_ptFrom.x, m_ptFrom.y, m_ptTo.x, m_ptTo.y);

		fl_end_offscreen();
		
		invalidate();

		return true;
		}



void
MainWnd::resize(int X, int Y, int W, int H)
		{
		FXWnd::resize(X, Y, W, H);

		FXRect	cr;

		getContentsRect(cr);

		m_mutex.acquire();

		Fl_Offscreen	hOldSurface=m_hSurface;

		if (cr.width() > 0 && cr.height() > 0)
			{
			if (m_szSurface.cx != cr.width() || m_szSurface.cy != cr.height())
				{
				m_hSurface = fl_create_offscreen(cr.width(), cr.height());

				m_szSurface.cx = cr.width();
				m_szSurface.cy = cr.height();

				fl_begin_offscreen(m_hSurface);

				fl_rectf(0, 0, cr.width(), cr.height(), FL_BLACK);

				if (hOldSurface != 0)
					{
					fl_copy_offscreen(0, 0, m_szSurface.cx, m_szSurface.cy, hOldSurface, 0, 0); 
					}

				fl_end_offscreen();

				if (hOldSurface != 0)
					{
					fl_delete_offscreen(hOldSurface);
					}
				}
			}
		else if (m_hSurface != 0)
			{
			fl_delete_offscreen(m_hSurface);
			m_hSurface = 0;
			m_szSurface.cx = m_szSurface.cy = 0;
			}

		m_mutex.release();
		}
