/**
*** @defgroup	TestDialogApp	TestDialog
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestDatePicker {

/**
*** @ingroup TestDialogApp
**/
class TestDatePicker : public FXApp
		{
public:
		/// Constructor
		TestDatePicker();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();
		};

extern TestDatePicker theApp;

}
