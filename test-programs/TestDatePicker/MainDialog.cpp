#include "stdafx.h"

#include "TestDatePicker.h"
#include "MainDialog.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXDatePicker.h>
#include <FltkExt/FXTimePicker.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXPromptNumberDialog.h>
#include <FltkExt/FXPromptStringDialog.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>
#include <FL/Fl_Return_Button.H>
#include <FL/Fl_Input.H>

namespace TestDatePicker {




#define DLGWIDTH	300
#define	DLGHEIGHT	250


MainDialog::MainDialog() :
	FXDialog("TestDatePicker", DLGWIDTH, DLGHEIGHT)
		{
		int		ypos=10;
		int		xpos=10;
		int		rowheight=FXFont::getDefaultFont().height()+8;
		int		itemid=1000;
		int		ygap=4;

		addItem(FX_IDSTATIC, new FXStaticText(10, ypos, 90, 20, "Normal Input:"));
		addItem(FX_IDSTATIC, new Fl_Input(110, ypos, 90, 20));

		ypos += 30;
		addItem(FX_IDSTATIC, new FXStaticText(10, ypos, 90, 20, "Date Picker:"));
		addItem(FX_IDSTATIC, new FXDatePicker(110, ypos, 90, 20));
		
		ypos += 30;
		addItem(FX_IDSTATIC, new FXStaticText(10, ypos, 90, 20, "Time Picker:"));
		addItem(FX_IDSTATIC, new FXTimePicker(110, ypos, 90, 20));

		ypos += 30;
		addItem(FX_IDSTATIC, new FXStaticText(10, ypos, 90, 20, "Normal Input 2:"));
		addItem(FX_IDSTATIC, new Fl_Input(110, ypos, 90, 20));
		

		addButton(FX_IDOK,		"OK",		FX_BTN_DEFAULT,				DLGWIDTH-200, DLGHEIGHT-rowheight-10, 90, rowheight);
		addButton(FX_IDCANCEL,	"Cancel",	FX_BTN_STANDARD,			DLGWIDTH-100, DLGHEIGHT-rowheight-10, 90, rowheight);
		}


MainDialog::~MainDialog()
		{
		}


int
MainDialog::onButtonClicked(Fl_Widget *pWidget)
		{
		int		res=0;

		switch (pWidget->id())
			{
			case 900:
				{
				FXPromptStringDialog	dlg("FXPromptStringDialog", "Please enter a number", "", this);

				if (dlg.doModal() == FX_IDOK)
					{
					}

				res = 1;
				}
				break;

			case 901:
				{
				FXPromptNumberDialog	dlg("FXPromptNumberDialog", "Please enter a number", 0, this);

				dlg.doModal();
				res = 1;
				}
				break;
			}
		
		return res;
		}

}	
