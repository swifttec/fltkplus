
// TestDatePicker.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDatePicker.h"
#include "MainDialog.h"

#include <FL/Fl_PNG_Image.H>

namespace TestDatePicker { TestDatePicker	theApp; }

FX_APP_EXECUTE(TestDatePicker::theApp);

namespace TestDatePicker {

TestDatePicker::TestDatePicker()
		{
		}


bool
TestDatePicker::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			MainDialog	dlg;

			dlg.doModal();

			res = true;
			}

		return res;
		}



int
TestDatePicker::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
