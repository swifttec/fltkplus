
// TestFXImageBuffer.h : main header file for the TestFXImageBuffer application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestFXImageBuffer:
// See TestFXImageBuffer.cpp for the implementation of this class
//
class MainWnd;

class TestFXImageBuffer : public FXApp
		{
public:
		TestFXImageBuffer();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestFXImageBuffer theApp;
