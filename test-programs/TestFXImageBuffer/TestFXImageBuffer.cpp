
// TestFXImageBuffer.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestFXImageBuffer.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig;
TestFXImageBuffer	theApp;

FX_APP_EXECUTE(theApp);

TestFXImageBuffer::TestFXImageBuffer() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestFXImageBuffer::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestFXImageBuffer::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
