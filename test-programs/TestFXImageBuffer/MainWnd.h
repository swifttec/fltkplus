// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXImageBuffer.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

public: // Overrides
		virtual void	onDraw();

protected:
		FXImageBuffer		m_image;
		};
