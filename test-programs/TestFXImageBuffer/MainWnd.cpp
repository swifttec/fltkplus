// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestFXImageBuffer.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

static int			cidx=0;
static const char	*clist[] = { "#f00", "#0f0", "#00f", "#ff0", "#f0f", "#0ff", "#fff", "#000" };

MainWnd::MainWnd() :
	FXWnd("Test Draw Image", 640, 480)
		{
		show();

		setBackgroundColor("#444");

		m_image.setSize(100, 100);
		m_image.fill(clist[cidx]);
		}



MainWnd::~MainWnd()
		{
		}




void
MainWnd::onDraw()
		{
		FXRect	cr;

		getContentsRect(cr);
		m_image.draw(100, 100, m_image.width(), m_image.height(), 0, 0);

		FXFont	font("Arial", 12);

		fx_draw_text(clist[cidx], cr, FX_HALIGN_LEFT | FX_VALIGN_TOP, font, "#fff");
		}


int
MainWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int		res=0;

		if (text == "+")
			{
			if (++cidx > 7) cidx = 0;
			res = 1;
			}
		else if (text == "-")
			{
			if (--cidx < 0) cidx = 7;
			res = 1;
			}

		if (res)
			{
			m_image.fill(clist[cidx]);
			invalidate();
			}

		return res;
		}
