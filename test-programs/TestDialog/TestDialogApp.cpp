
// TestDialogApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDialogApp.h"
#include "MainDialog.h"

#include <FL/Fl_PNG_Image.H>

namespace TestDialog
{
TestDialogApp	theApp;
SWAppConfig		theAppConfig;
}

FX_APP_EXECUTE(TestDialog::theApp);

namespace TestDialog {

TestDialogApp::TestDialogApp() :
	FXApp(&theAppConfig, true)
		{
		}


bool
TestDialogApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			MainDialog	dlg;

			dlg.doModal();

			res = true;
			}

		return res;
		}


}
