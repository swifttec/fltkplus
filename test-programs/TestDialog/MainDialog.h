#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXDialog.h>

namespace TestDialog {

/**
*** @ingroup TestDialogApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainDialog : public FXDialog
		{
public:
		/// Constructor - generates all the internal boxes
		MainDialog();

		/// Destructor
		virtual ~MainDialog();

protected: // FXDialog override

		/// Called when a button is clicked - return non-zero handled
		virtual int		onButtonClicked(Fl_Widget *pWidget);
		};

}

#endif // __MainWindow_h__
