/**
*** @defgroup	TestDialogApp	TestDialog
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestDialog {

/**
*** @ingroup TestDialogApp
**/
class TestDialogApp : public FXApp
		{
public:
		/// Constructor
		TestDialogApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);
		};

extern TestDialogApp theApp;

}
