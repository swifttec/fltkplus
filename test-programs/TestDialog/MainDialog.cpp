#include "stdafx.h"

#include "TestDialogApp.h"
#include "MainDialog.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXPromptNumberDialog.h>
#include <FltkExt/FXPromptStringDialog.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>
#include <FL/Fl_Return_Button.H>


namespace TestDialog {




#define DLGWIDTH	300
#define	DLGHEIGHT	250


MainDialog::MainDialog() :
	FXDialog("Test Dialog (root)", DLGWIDTH, DLGHEIGHT)
		{
		int		ypos=10;
		int		xpos=10;
		int		rowheight=FXFont::getDefaultFont().height()+16;
		int		itemid=1000;
		int		ygap=4;

		addStaticText(FX_IDSTATIC, "Static text:",						xpos,		ypos, 0, rowheight); 
		
		ypos += rowheight+ygap;
		addStaticText(FX_IDSTATIC,	"Text Box:",						xpos,		ypos, 0, rowheight); 
		addTextInput(itemid++,											xpos+100,	ypos, 100, rowheight);

		ypos += rowheight+ygap;
		addStaticText(FX_IDSTATIC,	"Button:",							xpos,		ypos, 0, rowheight); 
		addButton(900,				"Prompt String",	FX_BTN_HOVER,	xpos+100,	ypos, 90, rowheight);

		ypos += rowheight+ygap;
		addButton(901,				"Prompt Number",	FX_BTN_HOVER,	xpos+100,	ypos, 90, rowheight);

		addButton(FX_IDCANCEL,	"Cancel",	FX_BTN_STANDARD,			DLGWIDTH-100, DLGHEIGHT-rowheight-10, 90, rowheight);
		addButton(FX_IDOK,		"OK",		FX_BTN_DEFAULT,				DLGWIDTH-200, DLGHEIGHT-rowheight-10, 90, rowheight);
		}


MainDialog::~MainDialog()
		{
		}


int
MainDialog::onButtonClicked(Fl_Widget *pWidget)
		{
		int		res=0;

		switch (pWidget->id())
			{
			case 900:
				{
				FXPromptStringDialog	dlg("FXPromptStringDialog", "Please enter a number", "", this);

				if (dlg.doModal() == FX_IDOK)
					{
					}

				res = 1;
				}
				break;

			case 901:
				{
				FXPromptNumberDialog	dlg("FXPromptNumberDialog", "Please enter a number", 0, this);

				dlg.doModal();
				res = 1;
				}
				break;
			}
		
		return res;
		}

}	