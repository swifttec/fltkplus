/**
*** @defgroup	TestButtonBarApp	TestButtonBar
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestButtonBar {

class MainWindow;

/**
*** @ingroup TestButtonBarApp
**/
class TestButtonBarApp : public FXApp
		{
public:
		/// Constructor
		TestButtonBarApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestButtonBarApp theApp;

#define APP_TITLE	"TestButtonBar"

}
