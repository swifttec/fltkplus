#include "stdafx.h"

#include "TestButtonBar.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXImageButton.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>

namespace TestButtonBar {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#888");
		setForegroundColor("#fff");

		setLayout(new FXPanelLayout(true), true);

		add(m_pToolbar = new FXToolbar(32));
		add(m_pContents = new Fl_Text_Display(0, 0, w(), h()-128));
		add(m_pButtonBar = new FXButtonBar(0, h()-96, w(), 88, false));

		m_pTextBuffer = new Fl_Text_Buffer();
		m_pContents->buffer(m_pTextBuffer);

		addLine("First line");
		addLine("Second line");

		int		id=100;

		m_pToolbar->addItem(id++, new FXStaticText(60, 24, "60x24", FX_HALIGN_CENTER, "#000"));
		m_pToolbar->addItem(id++, new FXStaticText(80, 24, "80x24", FX_HALIGN_CENTER, "#f00", "#fff"));
		m_pToolbar->addSeparator();
		m_pToolbar->addItem(id++, new FXImageButton("../resources/TickBoxes.png", 24, 28));
		m_pToolbar->addSeparator();
		m_pToolbar->addItem(id++, new FXImageButton("../resources/Print.png", 24, 28));
		m_pToolbar->addItem(id++, new FXImageButton("../resources/SaveAs.png", 24, 28));
		m_pToolbar->addSeparator();
		m_pToolbar->addItem(id++, new FXImageButton("../resources/Close.png", 24, 28));
		m_pToolbar->addSeparator();
		m_pToolbar->addItem(id++, new FXImageButton("../resources/ZoomOut.png", 24, 28));
		m_pToolbar->addItem(id++, new FXImageButton("../resources/ZoomIn.png", 24, 28));
		m_pToolbar->addSeparator();
		m_pToolbar->addItem(id++, new FXImageButton("../resources/left.png", 24, 28));
		m_pToolbar->addItem(id++, new FXImageButton("../resources/right.png", 24, 28));
		m_pToolbar->addItem(id++, new FXImageButton("../resources/up.png", 24, 28));
		m_pToolbar->addItem(id++, new FXImageButton("../resources/down.png", 24, 28));


		m_pButtonBar->addButton(id++, "Print", "../resources/Print.png");
		m_pButtonBar->addButton(id++, "Close", "../resources/Close.png");
		m_pButtonBar->addButton(id++, "Left", "../resources/left.png");
		m_pButtonBar->addButton(id++, "Right", "../resources/right.png");
		m_pButtonBar->addButton(id++, "Up", "../resources/up.png");
		m_pButtonBar->addButton(id++, "Down", "../resources/down.png");

		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}

void
MainWindow::addLine(const SWString &s)
		{
		if (m_pTextBuffer != NULL)
			{
			m_pTextBuffer->append(s);
			m_pTextBuffer->append("\n");
			m_pContents->redraw();
			}
		}


void
MainWindow::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		SWString	s;
		
		s.format("Got callback for item %u", pWidget->id());
		addLine(s);

		redraw();
		}



}
