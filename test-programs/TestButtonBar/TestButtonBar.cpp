
// TestButtonBarApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestButtonBar.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestButtonBar { TestButtonBarApp	theApp; }

FX_APP_EXECUTE(TestButtonBar::theApp);

namespace TestButtonBar {

TestButtonBarApp::TestButtonBarApp()
		{
		}


bool
TestButtonBarApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(800, 600);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestButtonBarApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
