
// TestWndExt.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestScroll.h"

#include "MainWnd.h"

TestWndExt	theApp;

FX_APP_EXECUTE(theApp);

TestWndExt::TestWndExt()
		{
		}

bool
TestWndExt::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestWndExt::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		pMainWnd->show();
		}
