// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestScroll.h"

#include "MainWnd.h"

#include <FltkExt/FXScrollableGroup.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXFixedLayout.h>


MainWnd::MainWnd() :
	FXWnd("Test Scroll Group", 640, 480)
		{
		end();

		setBackgroundColor("#567");
		setLayout(new FXPanelLayout(false, 0, FXPadding(10, 10, 10, 10)));

		FXGroup			*pGroup;
		FXFixedLayout	*pLayout;

		pGroup = new FXScrollableGroup(FXScrollableGroup::VScrollBar|FXScrollableGroup::HScrollBar);
		pGroup->setBackgroundColor("#789");
		pGroup->setLayout(pLayout = new FXFixedLayout(FXLayout::NoMaxSize|FXLayout::NoMinSize));

		pLayout->addWidget(new FXTestWidget("#f00", "#fff"), 10, 10, 200, 100);
		pLayout->addWidget(new FXTestWidget("#0f0", "#fff"), 100, 110, 300, 200);
		pLayout->addWidget(new FXTestWidget("#00f", "#fff"), 400, 400, 200, 200);

		add(pGroup);
		}



MainWnd::~MainWnd()
		{
		}



