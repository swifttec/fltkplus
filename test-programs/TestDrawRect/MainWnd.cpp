// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawRect.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>
#include <FL/fl_draw.H>

MainWnd::MainWnd() :
	FXWnd("Test Draw Rect", 640, 480)
		{
		m_borderWidth=4;
		m_cornerSize=20;
		m_borderStep=1;
		m_cornerStep=1;
		show();
		}



MainWnd::~MainWnd()
		{
		}





void
MainWnd::onDraw()
		{
		FXRect	dr;

		getClientRect(dr);
		dr.DeflateRect(20, 20, 20, 20);

		int		a=5;

		fl_rect(dr.left-a, dr.top-a, dr.width()+(a*2), dr.height()+(a*2), FL_WHITE);

		fx_draw_rect(dr, m_borderWidth, m_cornerSize, "#f00", "#ff0");

		SWString	s;

		s.format("rect=%dx%d at %d,%d border=%d, corner=%d",
			dr.width(),
			dr.height(),
			dr.left,
			dr.top,
			m_borderWidth,
			m_cornerSize);

		fl_font(FL_HELVETICA, 10);
		fl_color(FL_BLACK);
		fx_draw_text(s, dr, FX_VALIGN_CENTER|FX_HALIGN_CENTER);

		m_borderWidth += m_borderStep;
		if (m_borderWidth < 0 || m_borderWidth > 20)
			{
			m_borderStep = -m_borderStep;
			m_borderWidth += m_borderStep;
		
/*
			m_cornerSize += m_cornerStep;
			if (m_cornerSize < 0 || m_cornerSize > 32)
				{
				m_cornerStep = -m_cornerStep;
				m_cornerSize += m_cornerStep;
				}
*/
			}
		}
