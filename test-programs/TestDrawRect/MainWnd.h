// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();


public: // Overrides
		virtual void		onDraw();

public:
		int		m_borderWidth;
		int		m_borderStep;
		int		m_cornerSize;
		int		m_cornerStep;
		};
