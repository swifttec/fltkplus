// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawArc.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Arc", 640, 480)
		{
		show();
		m_startangle = 0;
		m_endangle = 10;
		}



MainWnd::~MainWnd()
		{
		}




int
MainWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int	res=0;

		switch (code)
			{
			case FL_Left:
				m_startangle += 10.0;
				res = 1;
				break;

			case FL_Right:
				m_startangle -= 10.0;
				res = 1;
				break;

			case FL_Up:
				m_endangle += 10.0;
				res = 1;
				break;

			case FL_Down:
				m_endangle -= 10.0;
				res = 1;
				break;

				
			}

		if (res != 0) redraw();
		
		return res;
		}


void
MainWnd::onDraw()
		{
		FXRect	dr;

		getClientRect(dr);
		fx_draw_fillRect(dr, RGB(255, 255, 255));

		fl_color(FL_BLACK);

		FXPoint	pt=dr.center();
		
		int		r=sw_math_min(dr.width() / 2, dr.height() / 2);

		for (int i=0;i<10;i++)
			{
			r -= 5;

			fl_begin_line();
			fl_arc((double)pt.x, (double)pt.y, (double)r, m_startangle, m_endangle);
			fl_end_line();
			}

		SWString	s;

		s.format("Start = %.0f, End = %.0f", m_startangle, m_endangle);

		fl_font(FL_HELVETICA, 10);
		fl_draw(s, 10, 20);
		}
