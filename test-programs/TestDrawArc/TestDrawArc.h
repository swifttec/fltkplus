
// TestWndExt.h : main header file for the TestWndExt application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestWndExt:
// See TestWndExt.cpp for the implementation of this class
//
class MainWnd;

class TestWndExt : public FXApp
		{
public:
		TestWndExt();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestWndExt theApp;
