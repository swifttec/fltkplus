// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestSplitter.h"

#include "MainWnd.h"

#include <FltkExt/FXTestWidget.h>


MainWnd::MainWnd() :
	FXWnd("Test Splitter", 800, 600),
	m_pSplitter(NULL),
	m_mode(0)
		{
		m_fullscreen = false;
		}



MainWnd::~MainWnd()
		{
		// We do not delete our child windows as the manager code takles care of this
		}



void
MainWnd::init()
		{
		SWString	regkey = "windows/";
		
		m_fullscreen = false;
	
		bool	res=true;
		
		add(m_pSplitter = new FXSplitterWnd(20, 20, w()-40, h()-40, false));

#define TESTMODE	1

#if TESTMODE == 1
		// 2 Panes
		m_pSplitter->addPane(new TestView(), 0);
		m_pSplitter->addPane(new TestView(), 0);

		m_pSplitter->setPaneSize(0, 200);
		m_pSplitter->setPaneSize(1, 200);

		m_pSplitter->loadSizesFromRegistry(SW_REGISTRY_CURRENT_USER, "Software/SwiftTec/FltkExt/TestSplitter", "splitter");

#elif TESTMODE == 2
		// 2x2
		TestPane	*pPane;

		m_pSplitter->addPane(new FXTestWidget("#fff", "#000"), 0);
		m_pSplitter->addPane(pPane = new TestPane(false), 0);
		pPane->addPane(new FXTestWidget("#eff", "#000"), 0);
		pPane->addPane(new FXTestWidget("#fef", "#000"), 0);

#elif TESTMODE == 3

		// 2x2
		TestPane	*pPane;

		m_pSplitter->addPane(new FXTestWidget("#fff", "#000"), 0);
		m_pSplitter->addPane(pPane = new TestPane(true), 0);
		pPane->addPane(new FXTestWidget("#eff", "#000"), 0);
		pPane->addPane(new FXTestWidget("#fef", "#000"), 0);

		m_pSplitter->addPane(pPane = new TestPane(false), 0);
		pPane->addPane(new FXTestWidget("#eff", "#000"), 0);
		pPane->addPane(new FXTestWidget("#fef", "#000"), 0);

#endif

		show();
		}



void
MainWnd::onClose()
		{
		m_pSplitter->saveSizesToRegistry(SW_REGISTRY_CURRENT_USER, "Software/SwiftTec/FltkExt/TestSplitter", "splitter");
		}


int
MainWnd::onCommand(sw_uint32_t id)
		{
		return 0;
		}



void
MainWnd::toggleFullScreen()
		{
		m_fullscreen = !m_fullscreen;
		}
