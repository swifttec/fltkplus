
// TestSplitter.h : main header file for the TestSplitter application
//
#pragma once

#include <FltkExt/FXApp.h>

#include "MainWnd.h"

class TestSplitter : public FXApp
		{
public:
		TestSplitter();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestSplitter theApp;
