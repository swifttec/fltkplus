// TestView.cpp : implementation of the TestView class
//

#include "stdafx.h"
#include "TestView.h"


#include <xplatform/sw_math.h>

#define HEIGHT	24

TestView::TestView(COLORREF fgcolor, COLORREF bgcolor, COLORREF bdcolor) :
	m_fgColor(fgcolor),
	m_bgColor(bgcolor),
	m_bdColor(bdcolor)
		{
		m_font = FXFont("Arial", 8.0, FXFont::Normal);
		setMinSize(100, 40);
		}


TestView::~TestView()
		{
		}


void
TestView::putline(const SWString &s)
		{
		FXSize		sz;

		sz = fx_getTextExtent(s, m_rect.Width(), 0);

		FXRect		tr;

		tr.left = m_rect.left;
		tr.right = m_rect.right;
		tr.top = m_rect.top + m_ypos;
		tr.bottom = tr.top + sz.cy;

		fx_draw_text(s, tr, 0);
		m_ypos += sz.cy + 1;
		m_width = sw_math_max(m_width, sz.cx);
		}


void
TestView::onDraw()
		{
		FXRect		dr;
		FXSize		sz;
		SWString	s;

		getClientRect(m_rect);

		fx_draw_frameRect(m_rect, RGB(0, 0, 255));
		m_rect.DeflateRect(10, 10, 10, 10);
		fx_draw_fillRect(m_rect, m_bgColor);
		fx_draw_frameRect(m_rect, m_bdColor);
		m_rect.DeflateRect(10, 10, 10, 10);
		m_ypos = 0;
		m_width = 0;

		m_font.select();

		sz = getMinSize();
		s.format("minimumn size = %d x %d", sz.cx, sz.cy);
		putline(s);

		sz = getMaxSize();
		s.format("maximum size = %d x %d", sz.cx, sz.cy);
		putline(s);

		sz = getSize();
		s.format("actual size = %d x %d", sz.cx, sz.cy);
		putline(s);

		/*
		sz = getContentsSize();
		s.format("contents size = %d x %d", sz.cx, sz.cy);
		putline(dc, s);
		*/

		setMinSize(m_width + 40, m_ypos + 40);
		}



/*
void
TestView::onRightMouseButton(int action, CPoint pt)
		{
		if (action == WXM_BUTTON_DOWN)
			{
			for (int tt=0;tt<TT_MAX;tt++)
				{
				CRect	&rSample=m_rTextType[tt];

				if (rSample.PtInRect(pt))
					{
					theApp.m_idPopup = ID_ACTION_SONG_DEFAULT+tt;

					CMenu	menu, *pPopupMenu;
		
					ClientToScreen(&pt);
		
					// Load the menu from the resources
					menu.LoadMenu(IDR_POPUP_DEFAULT_TEXT);
		
					// Get the first sub-menu
					pPopupMenu = menu.GetSubMenu(0);
		
					// Display it!
					pPopupMenu->TrackPopupMenu(TPM_CENTERALIGN|TPM_RIGHTBUTTON, pt.x, pt.y, theApp.GetMainWnd(), NULL);

					// Cleanup
					menu.Detach();
					break;
					}
				}
			}
		}
*/
