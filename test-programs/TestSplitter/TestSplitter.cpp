
// TestSplitter.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestSplitter.h"

#include "MainWnd.h"

#include <swift/SWAppConfig.h>

SWAppConfig	theAppConfig;

// TestSplitter construction

TestSplitter::TestSplitter() :
	FXApp(&theAppConfig, true)
		{
		}

// The one and only TestSplitter object

TestSplitter theApp;

FX_APP_EXECUTE(theApp);

// TestSplitter initialization

bool
TestSplitter::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			// Init the windows
			MainWnd	*pMainWnd;
		
			pMainWnd = new MainWnd();
			pMainWnd->show();
			pMainWnd->init();
			m_pMainWnd = pMainWnd;
			}
		
		return res;
		}
