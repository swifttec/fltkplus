// TestPane.h : interface of the TestPane class
//
#pragma once

#include <FltkExt/FXSplitterWnd.h>

// TestPane window

class TestPane : public FXSplitterWnd
		{
public:
		TestPane(bool vertical);
		virtual ~TestPane();
		};

