// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>

#include "TestView.h"
#include "TestPane.h"


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();

		void				toggleFullScreen();
		bool				isFullScreen() const	{ return m_fullscreen; }

public: // Overrides
		virtual void		init();
		virtual int			onCommand(sw_uint32_t id);

		/// Called when the widget is closed
		virtual void	onClose();

protected:
		bool				m_fullscreen;
		FXSplitterWnd		*m_pSplitter;
		
		int					m_mode;
		};
