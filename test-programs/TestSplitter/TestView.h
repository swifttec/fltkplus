// TestView.h : interface of the TestView class
//
#pragma once

#include <FltkExt/FXWidget.h>
#include <FltkExt/FXFont.h>

// TestView window

class TestView : public FXWidget
		{
public:
		TestView(COLORREF fgcolor=RGB(0, 0, 0), COLORREF bgcolor=RGB(255, 255, 255), COLORREF bdcolor=RGB(255, 0, 0));
		virtual ~TestView();

		void	putline(const SWString &s);

protected:
		virtual void		onDraw();

protected:
		COLORREF	m_fgColor;
		COLORREF	m_bgColor;
		COLORREF	m_bdColor;
		FXFont		m_font;
		int			m_ypos;
		int			m_width;
		FXRect		m_rect;
		};

