// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();


public: // Overrides
		virtual void		onDraw();

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool		onTimer(sw_uint32_t id, void *data);

public:
		int		m_borderWidth;
		int		m_borderStep;
		int		m_cornerSize;
		int		m_cornerStep;
		int		m_alpha;
		int		m_alphaStep;
		};
