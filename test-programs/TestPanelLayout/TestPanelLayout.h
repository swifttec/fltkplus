/**
*** @defgroup	TestLayoutsApp	TestLayouts
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestLayouts {

class MainWindow;

/**
*** @ingroup TestLayoutsApp
**/
class TestLayoutsApp : public FXApp
		{
public:
		/// Constructor
		TestLayoutsApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestLayoutsApp theApp;

#define APP_TITLE	"TestLayouts"

}
