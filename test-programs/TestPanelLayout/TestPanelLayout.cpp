
// TestLayoutsApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestPanelLayout.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestLayouts
{
TestLayoutsApp		theApp;
SWAppConfig			theAppConfig;
}

FX_APP_EXECUTE(TestLayouts::theApp);

namespace TestLayouts {

TestLayoutsApp::TestLayoutsApp() :
	FXApp(&theAppConfig, true)
		{
		}


bool
TestLayoutsApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestLayoutsApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
