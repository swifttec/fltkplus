#include "stdafx.h"

#include "TestPanelLayout.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXGridLayout.h>
#include <FltkExt/FXPanelLayout.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestLayouts {


MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setLayout(new FXPanelLayout(false));

		FXTestWidget	*pWidget;
		FXGroup			*pVGroup=new FXGroup();

		pVGroup->setLayout(new FXPanelLayout(true));

		pWidget = new FXTestWidget("#880");
		pWidget->setMinSize(FXSize(100, 50));
		pVGroup->add(pWidget);

		pWidget = new FXTestWidget("#088");
		pWidget->setMinSize(FXSize(200, 100));
		pVGroup->add(pWidget);

		add(pVGroup);

		// BEGIN group 2

		pVGroup = new FXGroup();

			pVGroup->setLayout(new FXPanelLayout(true));
			pVGroup->setBackgroundColor("#ccc");

			pVGroup->add(pWidget = new FXTestWidget("#fff", "#000"));
			pWidget->setMinMaxSizes(200, 100, 0, 150);

			pVGroup->add(pWidget = new FXTestWidget("#444", "#fff"));
			pWidget->setMinMaxSizes(100, 100, 200, 200);

			pVGroup->add(pWidget = new FXTestWidget("#444", "#fff"));
			pWidget->setMinMaxSizes(250, 100, 0, 200);

		add(pVGroup);
		// END group 2

		pWidget = new FXTestWidget("#008");
		pWidget->setMinSize(FXSize(100, 50));
		add(pWidget);

		pWidget = new FXTestWidget("#808");
		pWidget->setMinSize(FXSize(100, 200));
		add(pWidget);

		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}

}
