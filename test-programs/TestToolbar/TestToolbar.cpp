
// TestToolbarApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestToolbar.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestToolbar { TestToolbarApp	theApp; }

FX_APP_EXECUTE(TestToolbar::theApp);

namespace TestToolbar {

TestToolbarApp::TestToolbarApp()
		{
		}


bool
TestToolbarApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(800, 600);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestToolbarApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
