/**
*** @defgroup	TestToolbarApp	TestToolbar
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestToolbar {

class MainWindow;

/**
*** @ingroup TestToolbarApp
**/
class TestToolbarApp : public FXApp
		{
public:
		/// Constructor
		TestToolbarApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestToolbarApp theApp;

#define APP_TITLE	"TestToolbar"

}
