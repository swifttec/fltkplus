#include "stdafx.h"

#include "TestDataTable.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>



MainWindow::MainWindow(int W, int H) :
	FXWnd("Test Data Table", 800, 600)
		{
		int				n=0;
		SWStringArray	sa;
		SWString		alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		end();

#undef MULTIPLE_TABLES

#ifdef MULTIPLE_TABLES
		int		width=(w()-30) / 2;
		int		height=(h()-30) / 2;

		for (int t=0;t<4;t++)
			{
			FXDataTable	*pTable=new FXDataTable(10 + ((t % 2) * (10 + width)), 10 + ((t / 2) * (10 + height)), width, height);

			add(pTable);

			pTable->addColumn("ID", FX_HALIGN_RIGHT);
			pTable->addColumn("Left", FX_HALIGN_LEFT);
			pTable->addColumn("Centre", FX_HALIGN_CENTER);
			pTable->addColumn("Right", FX_HALIGN_RIGHT);
			pTable->addColumn("Text", FX_HALIGN_LEFT);

			int				nrows=2000 + (t * 2000);

			for (int i=0;i<nrows;i++)
				{
				int		idx=0;


				sa[idx++] = SWString::valueOf(n++);
				sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
				sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
				sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
				sa[idx++] = "The quick brown fox jumped over the lazy dogs";

				pTable->addRow(sa);

				FXColor	fg, bg;

				if (t == 0)
					{
					fg = "#000";
					bg = FXColor(sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255));
					pTable->setRowColors(i, fg, bg);
					}
				else if (t == 1)
					{
					bg = "#000";
					fg = FXColor(sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255));
					pTable->setRowColors(i, fg, bg);
					}

				}
			}
#else
		FXDataTable	*pTable=new FXDataTable(10, 10, w()-20, h()-20);

		add(pTable);

		pTable->setSelectionMode(FXDataTable::SelectCells|FXDataTable::SelectMultiple);
		pTable->invertCellColorsOnSelection(true);

		pTable->addColumn("ID", FX_HALIGN_RIGHT);
		pTable->addColumn("Left", FX_HALIGN_LEFT);
		pTable->addColumn("Centre", FX_HALIGN_CENTER);
		pTable->addColumn("Right", FX_HALIGN_RIGHT);
		pTable->addColumn("Text", FX_HALIGN_LEFT);

		int				nrows=40;

		for (int i=0;i<nrows;i++)
			{
			int		idx=0;

			sa[idx++] = SWString::valueOf(n++);
			sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
			sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
			sa[idx++] = alphabet.left(sw_math_randomInt32InRange(1,26));
			sa[idx++] = "The quick brown fox jumped over the lazy dogs";

			pTable->addRow(sa);

			FXColor	fg, bg;

			fg = "#000";
			bg = FXColor(sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255), sw_math_randomIntegerInRange(200, 255));
			pTable->setRowColors(i, fg, bg);
			}
#endif
		}


MainWindow::~MainWindow()
		{
		}


int
MainWindow::onShortcut(int code, const FXPoint &pt)
		{
/*
		switch (code)
			{
			case '[':
				if (bw > 0) pWidget->setBorderWidth(bw-1);
				break;

			case ']':
				if (bw < 40) pWidget->setBorderWidth(bw+1);
				break;

			case '-':
				if (cw > 0) pWidget->setCornerWidth(cw-1);
				break;

			case '=':
				if (cw < 40) pWidget->setCornerWidth(cw+1);
				break;
			}

		invalidate();
*/
		return 1;
		}