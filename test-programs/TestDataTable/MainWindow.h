#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXDataTable.h>

/**
*** @ingroup TestWidgetsApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

		virtual int	onShortcut(int code, const FXPoint &pt);

protected:
		FXDataTable	*m_pTable[4];
		};

#endif // __MainWindow_h__
