
// TestDataTable.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDataTable.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

TestDataTable	theApp;

FX_APP_EXECUTE(theApp);

TestDataTable::TestDataTable()
		{
		}


bool
TestDataTable::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			m_pMainWnd = new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestDataTable::exitInstance()
		{
		return FXApp::exitInstance();
		}
