#include "stdafx.h"

#include "TestWidgetRects.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>

class MyWidget : public FXWidget
		{
public:
		MyWidget(int X, int Y, int W, int H);

		virtual void	onDraw();
		};
	

MyWidget::MyWidget(int X, int Y, int W, int H) :
	FXWidget(X, Y, W, H)
		{
		setBackgroundColor("#ccc");
		setForegroundColor("#888");
		setBorder(1, "#444", 10, 30);
		setPadding(FXPadding(10, 10, 20, 20));
		}


void
MyWidget::onDraw()
		{
		FXRect	dr;

		getWidgetRect(dr);
		fx_draw_rect(dr, 1, 0, "#000");

		getInnerRect(dr);
		fx_draw_rect(dr, 1, 0, "#0f0");

		getClientRect(dr);
		fx_draw_rect(dr, 1, 0, "#f00");

		getContentsRect(dr);
		fx_draw_rect(dr, 1, 0, "#00f");
		}




MainWindow::MainWindow(int width, int height) :
	FXWnd("Test Widget Rects", 800, 600)
		{
		add(new MyWidget(10, 10, w()-20, h()-20));
		}


MainWindow::~MainWindow()
		{
		}


int
MainWindow::onShortcut(int code, const FXPoint &pt)
		{
		MyWidget	*pWidget=(MyWidget *)child(0);
		int			bw=pWidget->getBorderWidth();
		int			cw=pWidget->getCornerWidth();

		switch (code)
			{
			case '[':
				if (bw > 0) pWidget->setBorderWidth(bw-1);
				break;

			case ']':
				if (bw < 40) pWidget->setBorderWidth(bw+1);
				break;

			case '-':
				if (cw > 0) pWidget->setCornerWidth(cw-1);
				break;

			case '=':
				if (cw < 40) pWidget->setCornerWidth(cw+1);
				break;
			}

		invalidate();

		return 1;
		}