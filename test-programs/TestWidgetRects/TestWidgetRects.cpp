
// TestWidgetRects.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestWidgetRects.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

TestWidgetRects	theApp;

FX_APP_EXECUTE(theApp);

TestWidgetRects::TestWidgetRects()
		{
		}


bool
TestWidgetRects::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			m_pMainWnd = new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestWidgetRects::exitInstance()
		{
		return FXApp::exitInstance();
		}
