// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawText.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Text", 640, 480)
		{
		show();
		}



MainWnd::~MainWnd()
		{
		}





void
MainWnd::onDraw()
		{
		FXRect		dr;
		SWString	s;

		getClientRect(dr);
		fx_draw_fillRect(dr, RGB(255, 255, 255));

		/*
		SWString	s="The quick brown fox jumped over the lazy dogs";
		FXColorFont	font("Arial", 24.0, FXFont::Normal, "#000");

		fx_draw_text(s, dr, XD_CENTER|XD_MIDDLE|XD_SHRINKTOFIT, font);
		*/

		#define NCOLORS	10

		int		ypos=10;
		const char	*ca[NCOLORS]	= {
			"#000",
			"#800",
			"#080",
			"#008",
			"#880",
			"#088",
			"#808",
			"#888",
			"#0ff",
			"#ff0",
			};

		for (int i=0;i<16;i++)
			{
			FXRect	tr(dr);
			int		yh=8+(i*2);

			tr.top = ypos;
			tr.bottom = ypos + yh;

			s.format("This is text in size %d - color %s", yh, ca[i%NCOLORS]);
			fx_draw_text(s, tr, FX_HALIGN_LEFT|FX_VALIGN_MIDDLE, FXColorFont("helvetica", yh, i%4, ca[i%NCOLORS]));
			ypos += yh + 2;
			}
		}
