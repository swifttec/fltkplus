
// TestDrawPolygon.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDrawPolygon.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig;
TestDrawPolygon	theApp;

FX_APP_EXECUTE(theApp);

TestDrawPolygon::TestDrawPolygon() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestDrawPolygon::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestDrawPolygon::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
