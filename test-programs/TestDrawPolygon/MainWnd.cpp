// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawPolygon.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Arc", 640, 480)
		{
		show();
		m_startangle = 0;
		m_endangle = 10;
		}



MainWnd::~MainWnd()
		{
		}




int
MainWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int	res=0;

		switch (code)
			{
			case FL_Left:
				m_startangle += 10.0;
				res = 1;
				break;

			case FL_Right:
				m_startangle -= 10.0;
				res = 1;
				break;

			case FL_Up:
				m_endangle += 10.0;
				res = 1;
				break;

			case FL_Down:
				m_endangle -= 10.0;
				res = 1;
				break;

				
			}

		if (res != 0) redraw();
		
		return res;
		}


void
MainWnd::onDraw()
		{
		FXRect	cr, dr;

		getContentsRect(cr);

		dr = cr;
		dr.deflate(0, 0, cr.width()/2, cr.height()/2);
		fx_draw_cross(dr, "#f00");
		fx_draw_rect(dr, 1, 0, "#f00");

		dr = cr;
		dr.deflate(cr.width()/2, cr.height()/2, 0, 0);
		fx_draw_tick(dr, "#0f0");
		fx_draw_rect(dr, 1, 0, "#0f0");

		dr = cr;
		dr.deflate(cr.width()/2, 0, 0, cr.height()/2);
		fx_draw_rect(dr, 1, 0, "#00f");

		fl_begin_polygon();
		fl_vertex((double)dr.left+10, (double)dr.top+10);
		fl_vertex((double)dr.right-10, (double)dr.bottom-10);
		fl_vertex((double)dr.left+10, (double)dr.bottom-10);
		fl_vertex((double)dr.left+10, (double)dr.top+10);

		fl_end_polygon();
		}
