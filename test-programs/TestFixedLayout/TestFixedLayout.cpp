
// TestFixedLayoutApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestFixedLayout.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestFixedLayout { TestFixedLayoutApp	theApp; }

FX_APP_EXECUTE(TestFixedLayout::theApp);

namespace TestFixedLayout {

SWAppConfig	theAppConfig;

TestFixedLayoutApp::TestFixedLayoutApp() :
	FXApp(&theAppConfig, true)
		{
		}


bool
TestFixedLayoutApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestFixedLayoutApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
