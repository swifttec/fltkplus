#include "stdafx.h"

#include "TestFixedLayout.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>
#include <FltkExt/FXGroup.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestFixedLayout {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		FXPanelLayout	*pMainLayout=new FXPanelLayout(true);

		setLayout(pMainLayout);

		// Main Panel 1
		add(new FXTestWidget("#444"));

		// Main Panel 2
		{
		FXGroup	*pGroup=new FXGroup();

		add(pGroup);

		FXFixedLayout	*pLayout=new FXFixedLayout(0);

		pGroup->setLayout(pLayout);

		FXTestWidget	*pWidget;
		FXBox			*pBox;
		int				ta=FX_VALIGN_MIDDLE|FX_HALIGN_CENTER;

		pLayout->addWidget(0, pWidget = new FXTestWidget("#800"), 10, 10, 100, 100);
		pLayout->addWidget(0, pWidget = new FXTestWidget("#080"), 120, 10, 200, 100);

		pWidget = new FXTestWidget("#008");
		pWidget->resize(10, 120, 300, 200);
		pLayout->addWidget(pWidget);
		
		pBox = new FXBox("#880");
		pBox->resize(320, 120, 50, 50);
		pLayout->addWidget(pBox);
		
		pBox = new FXBox("#808");
		pBox->resize(320, 180, 50, 50);
		pLayout->addWidget(pBox);
		
		pBox = new FXBox("#088");
		pBox->resize(320, 240, 50, 50);
		pLayout->addWidget(pBox);

		FXGroup		*pInnerGroup = new FXGroup(380, 10, 250, 460, "#444", "#f00");
		pLayout->addWidget(pInnerGroup);

		pInnerGroup->setLayout(pLayout = new FXFixedLayout());
		pLayout->addWidget(0, pWidget = new FXTestWidget("#def"), 10, 10, pInnerGroup->w()-20, 100);
		pLayout->addWidget(0, pWidget = new FXTestWidget("#fed"), 10, 120, pInnerGroup->w()-20, 300);
		}

		// Main Panel 3
		add(new FXTestWidget("#444"));

/*
		pLayout->setWidget(new FXStaticText(100, 20, "Top-Left", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_TOP|FX_HALIGN_LEFT, 0, 0);
		pLayout->setWidget(new FXBox("#f00"), 0, 1, 0);
		pLayout->setWidget(new FXStaticText(100, 20, "Top-Center", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_TOP|FX_HALIGN_CENTER, 2, 0);
		pLayout->setWidget(new FXBox("#0f0"), 0, 3, 0);
		pLayout->setWidget(new FXStaticText(100, 20, "Top-Right", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_TOP|FX_HALIGN_RIGHT, 4, 0);

		pLayout->setWidget(new FXBox("#555"), 0, 0, 1);
		pLayout->setWidget(new FXBox("#555"), 0, 1, 1);
		pLayout->setWidget(new FXBox("#555"), 0, 2, 1);
		pLayout->setWidget(new FXBox("#555"), 0, 3, 1);
		pLayout->setWidget(new FXBox("#555"), 0, 4, 1);

		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Left", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_MIDDLE|FX_HALIGN_LEFT, 0, 2);
		pLayout->setWidget(new FXBox("#00f"), 0, 1, 2);
		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Center", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_MIDDLE|FX_HALIGN_CENTER, 2, 2);
		pLayout->setWidget(new FXBox("#0ff"), 0, 3, 2);
		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Right", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_MIDDLE|FX_HALIGN_RIGHT, 4, 2);

		pLayout->setWidget(new FXBox("#555"), 0, 0, 3);
		pLayout->setWidget(new FXBox("#555"), 0, 1, 3);
		pLayout->setWidget(new FXBox("#555"), 0, 2, 3);
		pLayout->setWidget(new FXBox("#555"), 0, 3, 3);
		pLayout->setWidget(new FXBox("#555"), 0, 4, 3);

		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Left", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_BOTTOM|FX_HALIGN_LEFT, 0, 4);
		pLayout->setWidget(new FXBox("#ff0"), 0, 1, 4);
		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Center", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_BOTTOM|FX_HALIGN_CENTER, 2, 4);
		pLayout->setWidget(new FXBox("#f0f"), 0, 3, 4);
		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Right", ta, FL_WHITE, FL_RED, 10, FL_HELVETICA, FL_FLAT_BOX), FX_VALIGN_BOTTOM|FX_HALIGN_RIGHT, 4, 4);
*/

/*
		pWidget = new FXTestWidget("#800");
		pWidget->setMinSize(FXSize(100, 50));
		pLayout->setWidget(pWidget, FX_VALIGN_TOP|FX_HALIGN_RIGHT, 1, 1);


		pWidget = new FXTestWidget("#080");
		pWidget->setMinSize(FXSize(200, 100));
		pWidget->setMaxSize(FXSize(200, 100));
		pLayout->setWidget(pWidget, FX_VALIGN_TOP|FX_HALIGN_RIGHT, 3, 1);


		pWidget = new FXTestWidget("#008");
		pWidget->setMinSize(FXSize(200, 100));
		add(pWidget);

		pWidget = new FXTestWidget("#880");
		pWidget->setMinSize(FXSize(100, 50));
		pLayout->addWidget(pWidget);

		pWidget = new FXTestWidget("#808");
		pWidget->setMinSize(FXSize(100, 200));
		pWidget->setMaxSize(FXSize(100, 200));
		pLayout->addWidget(pWidget, FX_VALIGN_BOTTOM|FX_HALIGN_RIGHT);

		pWidget = new FXTestWidget("#088");
		pWidget->setMinSize(FXSize(100, 200));
		pLayout->addWidget(pWidget);
*/		
		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}

}
