#ifndef __EngineeringPanel_h__
#define __EngineeringPanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class EngineeringPanel : public Panel
		{
public:
		EngineeringPanel(int id);
		virtual ~EngineeringPanel();
		};

#endif // __EngineeringPanel_h__
