#include "stdafx.h"

#include "ReplayPanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

ReplayPanel::ReplayPanel(int id) :
	Panel(id)
		{
		}


ReplayPanel::~ReplayPanel()
		{
		}


void
ReplayPanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("Replay panel", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
