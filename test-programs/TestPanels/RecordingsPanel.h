#ifndef __RecordingsPanel_h__
#define __RecordingsPanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class RecordingsPanel : public Panel
		{
public:
		RecordingsPanel(int id);
		virtual ~RecordingsPanel();
		
		virtual void	draw();
		};

#endif // __RecordingsPanel_h__
