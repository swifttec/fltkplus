#ifndef __AdminPanel_h__
#define __AdminPanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class AdminPanel : public Panel
		{
public:
		AdminPanel(int id);
		virtual ~AdminPanel();
		
		virtual void	draw();
		};

#endif // __AdminPanel_h__
