#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>
#include <swift/SWArray.h>


#include <FltkExt/FXButtonBar.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>
#include "PanelWindow.h"

namespace TestPanels {

/**
*** @ingroup TestPanelsApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public Fl_Double_Window
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

		/// Handle the given event
		virtual int		handle(int event);

		/// Update the icon
		void			updateIcon();

///!@cond
		void			selectPanel(int id);

protected:
		void			createButtons();
		void			createPanels();
		void			updateButtons();

protected:
		FXButtonBar			*m_pSideBar;
		PanelWindow			*m_pPanelWindow;
		SWArray<Panel *>	m_panelList;
///!@endcond
		};

}

#endif // __MainWindow_h__
