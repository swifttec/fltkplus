#ifndef __PanelWindow_h__
#define __PanelWindow_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

namespace TestPanels {

/**
*** @ingroup TestPanelsApp
*** A test window - useful when doing initial window layouts
**/
class PanelWindow : public FXWidget
		{
public:
		/// Constructor
		PanelWindow();

		/// Destructor
		virtual ~PanelWindow();
		
		/// Draw handling
		virtual void	draw();

		/// Set the current panel
		bool			setPanel(Panel *pPanel);
		
		/// Get the id of the current panel
		int				getPanelID();

protected:
		Panel	*m_pPanel;	///< The current panel
		};

}

#endif // __PanelWindow_h__
