#include "stdafx.h"

#include "BlankPanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

BlankPanel::BlankPanel(int id) :
	Panel(id)
		{
		}


BlankPanel::~BlankPanel()
		{
		}


void
BlankPanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("This panel intentionally left blank!!!", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
