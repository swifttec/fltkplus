
// TestPanels.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestPanels.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestPanels {	TestPanelsApp	theApp; }

FX_APP_EXECUTE(TestPanels::theApp);

namespace TestPanels {

TestPanelsApp::TestPanelsApp()
		{
		}


bool
TestPanelsApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestPanelsApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
