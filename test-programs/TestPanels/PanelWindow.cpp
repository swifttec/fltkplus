#include "stdafx.h"

#include "PanelWindow.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

namespace TestPanels {


PanelWindow::PanelWindow() :
	FXWidget(0, 0, 100, 100),
	m_pPanel(NULL)
		{
		}


PanelWindow::~PanelWindow()
		{
		}


void
PanelWindow::draw()
		{
		if (m_pPanel != NULL)
			{
			if (m_pPanel->x() != x()
				|| m_pPanel->y() != y()
				|| m_pPanel->w() != w()
				|| m_pPanel->h() != h())
				{
				m_pPanel->resize(x(), y(), w(), h());
				}

			m_pPanel->draw();
			}
		else
			{
			fl_rectf(x(), y(), w(), h(), FL_BLACK);
			}
		}


bool
PanelWindow::setPanel(Panel *pPanel)
		{
		bool	res=true;

		if (m_pPanel != pPanel)
			{
			if (m_pPanel != NULL)
				{
				res = m_pPanel->onClose();
				}

			if (res)
				{
				m_pPanel = pPanel;
				m_pPanel->onOpen();
				}
			}

		redraw();

		return res;
		}


int
PanelWindow::getPanelID()
		{
		int		res=0;

		if (m_pPanel != NULL)
			{
			res = m_pPanel->id();
			}

		return res;
		}

}
