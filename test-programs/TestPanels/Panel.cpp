#include "stdafx.h"

#include "Panel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

Panel::Panel(int panelid) :
	FXManagedGroup(0, 0, 100, 100, true),
	m_bgColor(FL_BLACK),
	m_fgColor(FL_WHITE)
		{
		id(panelid);
		end();
		}


Panel::~Panel()
		{
		}


void
Panel::draw()
		{
		fl_rectf(x(), y(), w(), h(), m_bgColor);

		int	nchildren=children();

		if (nchildren > 0)
			{
			Fl_Widget	*const *a = array();

			for (int i=0;i<nchildren;i++)
				{
				Fl_Widget	*o=*a++;

				o->draw();
				}
			}
		}


bool
Panel::onOpen()
		{
		return true;
		}

bool
Panel::onClose()
		{
		return true;
		}

bool
Panel::onButtonClicked(int id)
		{
		return false;
		}
