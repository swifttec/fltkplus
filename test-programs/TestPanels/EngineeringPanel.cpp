#include "stdafx.h"

#include "EngineeringPanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FltkExt/FXTestWnd.h>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

EngineeringPanel::EngineeringPanel(int id) :
	Panel(id)
		{
		add(new FXTestWnd("Engineering 1"));
		add(new FXTestWnd("Engineering 2"));
		end();
		}


EngineeringPanel::~EngineeringPanel()
		{
		}

/*
void
EngineeringPanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("Engineering Panel", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
*/
