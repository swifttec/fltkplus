#ifndef __ReplayPanel_h__
#define __ReplayPanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class ReplayPanel : public Panel
		{
public:
		ReplayPanel(int id);
		virtual ~ReplayPanel();
		
		virtual void	draw();
		};

#endif // __ReplayPanel_h__
