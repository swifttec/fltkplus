#ifndef __Panel_h__
#define __Panel_h__

#include <FltkExt/FXWidget.h>
#include <FltkExt/FXManagedGroup.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include <FL/Fl_Group.H>

/*
** A test window - useful when doing initial window layouts
*/
class Panel : public FXManagedGroup
		{
public:
		Panel(int id);
		virtual ~Panel();
		
		virtual void	draw();

		virtual bool	onButtonClicked(int id);
		virtual bool	onOpen();
		virtual bool	onClose();
		 
protected:
		int		m_bgColor;
		int		m_fgColor;
		};

#endif // __Panel_h__
