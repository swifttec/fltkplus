#include "stdafx.h"

#include "QuarantinePanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

QuarantinePanel::QuarantinePanel(int id) :
	Panel(id)
		{
		}


QuarantinePanel::~QuarantinePanel()
		{
		}


void
QuarantinePanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("Quarantine panel", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
