/**
*** @defgroup	TestPanelsApp	TestWidgets
***
*** A test application for testing panels
**/

#pragma once

#include <FltkExt/FXApp.h>


namespace TestPanels {

class MainWindow;

/**
*** @ingroup TestPanelsApp
**/
class TestPanelsApp : public FXApp
		{
public:
		TestPanelsApp();

// Overrides
public:
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestPanelsApp theApp;

}
