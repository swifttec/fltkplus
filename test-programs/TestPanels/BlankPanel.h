#ifndef __BlankPanel_h__
#define __BlankPanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class BlankPanel : public Panel
		{
public:
		BlankPanel(int id);
		virtual ~BlankPanel();
		
		virtual void	draw();
		};

#endif // __BlankPanel_h__
