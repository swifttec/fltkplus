#ifndef __QuarantinePanel_h__
#define __QuarantinePanel_h__

#include <FltkExt/FXWidget.h>

#include <swift/SWString.h>
#include <swift/SWArray.h>



#include "Panel.h"

/*
** A test window - useful when doing initial window layouts
*/
class QuarantinePanel : public Panel
		{
public:
		QuarantinePanel(int id);
		virtual ~QuarantinePanel();
		
		virtual void	draw();
		};

#endif // __QuarantinePanel_h__
