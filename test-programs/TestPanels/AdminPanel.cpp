#include "stdafx.h"

#include "AdminPanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

AdminPanel::AdminPanel(int id) :
	Panel(id)
		{
		}


AdminPanel::~AdminPanel()
		{
		}


void
AdminPanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("Admin Panel", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
