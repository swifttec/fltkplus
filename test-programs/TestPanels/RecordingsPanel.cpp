#include "stdafx.h"

#include "RecordingsPanel.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <swift/SWIntegerArray.h>
#include <swift/SWFlags.h>

RecordingsPanel::RecordingsPanel(int id) :
	Panel(id)
		{
		}


RecordingsPanel::~RecordingsPanel()
		{
		}


void
RecordingsPanel::draw()
		{
		Panel::draw();

		fl_font(FL_HELVETICA, 20);
		fl_color(m_fgColor);
		fl_draw("Recordings panel", x(), y(), w(), h(), FL_ALIGN_CENTER);
		}
