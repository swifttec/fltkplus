#include "stdafx.h"

#include "TestPanels.h"
#include "MainWindow.h"

#include "BlankPanel.h"
#include "EngineeringPanel.h"
#include "QuarantinePanel.h"
#include "ReplayPanel.h"
#include "RecordingsPanel.h"
#include "AdminPanel.h"

#include <FltkExt/FXManagedGroup.h>
#include <FltkExt/FXTestWnd.h>
#include <FltkExt/FXStaticText.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>

namespace TestPanels {

enum ButtonIDs
	{
	BTN_ENGINEERING=100,
	BTN_REPLAY,
	BTN_QUARANTINE,
	BTN_RECORDINGS,
	BTN_SYSLOG,
	BTN_EVENTS,
	BTN_ADMIN,
	BTN_LOGIN,
	BTN_LOGOUT,
	BTN_EXIT
	};

MainWindow::MainWindow(int width, int height) :
	Fl_Double_Window(width, height),
	m_pSideBar(NULL),
	m_pPanelWindow(NULL)
		{
		box(FL_FLAT_BOX);
		color(FL_BLACK);

		resizable(*this);

		FXManagedGroup	*pHGroup=new FXManagedGroup(0, 0, w(), h(), false);

		pHGroup->begin();
			{
			m_pSideBar = new FXButtonBar(0, 0, 100, 100, true, 80, 8, 4);
			m_pPanelWindow = new PanelWindow();
			}
		pHGroup->end();
		
		pHGroup->setChildSizes(0, 170, 170);

		end();

		updateIcon();

		// Finally force a resize to keep Linux version happy
		pHGroup->resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;

		// Load the buttons
		createPanels();
		createButtons();
		selectPanel(0);
		}


void
MainWindow::createButtons()
		{
		m_pSideBar->setBackgroundColor("#000");
		m_pSideBar->addButton(1, "Engineering Checks", "");
		m_pSideBar->addButton(2, "Recorder Replay", "");
		m_pSideBar->addButton(3, "Quarantine", "");
		m_pSideBar->addButton(BTN_EXIT, "Exit", "");
		}



void
MainWindow::updateButtons()
		{
		int		id=m_pPanelWindow->getPanelID();

		// Disable all buttons!

		m_pSideBar->deselectAllItems();
		m_pSideBar->enableAllItems(true);
		m_pSideBar->selectItem(id);
		}


void
MainWindow::createPanels()
		{
		m_panelList.add(new BlankPanel(0));
		m_panelList.add(new EngineeringPanel(1));
		m_panelList.add(new ReplayPanel(2));
		m_panelList.add(new QuarantinePanel(3));
		m_panelList.add(new RecordingsPanel(4));
		}


void
MainWindow::selectPanel(int id)
		{
		Panel	*pPanel=NULL;

		for (int i=0;i<(int)m_panelList.size();i++)
			{
			Panel	*pListPanel=m_panelList[i];

			if ((int)pListPanel->id() == id)
				{
				pPanel = pListPanel;
				break;
				}
			}

		if (pPanel == NULL)
			{
			// If nothing pick the first one!
			pPanel = m_panelList[0];
			}

		m_pPanelWindow->setPanel(pPanel);
		updateButtons();
		}


void
MainWindow::updateIcon()
		{
		// Set our icon
		// Fl_PNG_Image	 pngicon(theAppConfig.getResourceFile("VeristoreX.png"));
		Fl_PNG_Image		pngicon("resources/VeristoreX.png");

		icon(&pngicon);
		iconlabel("VeristoreX");
		label("VeristoreX");
		xclass("VeristoreX");
		}


MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


int
MainWindow::handle(int event)
		{
		int		res=0;

		switch (event)
			{
			case FL_KEYDOWN: 
				// We will handle the keyboard
				res = 1;
				break;

			case FL_PUSH:
				// A button has been clicked
				Fl_Widget	*pWidget=Fl::belowmouse();

				if (pWidget != NULL)
					{
					FXButton	*pButton=dynamic_cast<FXButton *>(pWidget);

					if (pButton != NULL && pButton->active())
						{
						int		itemid=pButton->id();

						switch (itemid)
							{
							case BTN_EXIT:
								hide();
								break;

							default:
								selectPanel(itemid);
								break;
							}
						}
					}
				break;
			}

		if (res == 0) res = Fl_Double_Window::handle(event);

		return res;
		}

}
