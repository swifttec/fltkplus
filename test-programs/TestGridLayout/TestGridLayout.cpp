
// TestGridLayoutApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestGridLayout.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestGridLayout
{
TestGridLayoutApp	theApp;
SWAppConfig			theAppConfig;
}

FX_APP_EXECUTE(TestGridLayout::theApp);

namespace TestGridLayout {

TestGridLayoutApp::TestGridLayoutApp() :
	FXApp(&theAppConfig, true)
		{
		}


bool
TestGridLayoutApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestGridLayoutApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
