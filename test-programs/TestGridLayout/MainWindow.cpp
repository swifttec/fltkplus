#include "stdafx.h"

#include "TestGridLayout.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXGridLayout.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestGridLayout {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#000");
		FXPanelLayout	*pMainLayout=new FXPanelLayout(true);

		setLayout(pMainLayout);

		// Main Panel 1
		//add(new FXTestWidget("#444"));

		// Main Panel 2
		{
		FXGroup	*pGroup=new FXGroup();

		add(pGroup);

		FXGridLayout	*pLayout=new FXGridLayout(3, 1, true);

 		pLayout->padding(FXPadding(10, 10, 10, 10));
 		pLayout->gapBetweenChildren(FXSize(10, 10));
		pGroup->setLayout(pLayout);

		FXTestWidget	*pWidget;
		int				ta=FX_VALIGN_MIDDLE|FX_HALIGN_CENTER;

		pLayout->addWidget(pWidget = new FXTestWidget("#800"), FX_HALIGN_LEFT|FX_VALIGN_MIDDLE);
		pWidget->setFixedSize(200, 100);
		pLayout->addWidget(pWidget = new FXTestWidget("#080"), FX_HALIGN_CENTER|FX_VALIGN_MIDDLE);
		pWidget->setFixedSize(50, 50);
		pLayout->addWidget(pWidget = new FXTestWidget("#008"), FX_HALIGN_RIGHT|FX_VALIGN_MIDDLE);
		pWidget->setFixedSize(100, 200);
		}

		// Main Panel 3
		//add(new FXTestWidget("#444"));
/*
		FXGridLayout	*pLayout=new FXGridLayout(5, 5);

		setLayout(pLayout);

		FXTestWidget	*pWidget;
		FXStaticText	*pText;
		FXFont			font("Arial", 10);
		int				ta=FX_VALIGN_MIDDLE|FX_HALIGN_CENTER;

		pLayout->setWidget(new FXStaticText(100, 20, "Top-Left", font, ta, "#fff", "#400"), FX_VALIGN_TOP|FX_HALIGN_LEFT, 0, 0);
		pLayout->setWidget(new FXBox("#f00"), 0, 1, 0);
		pLayout->setWidget(new FXStaticText(100, 20, "Top-Center", font, ta, "#fff", "#800"), FX_VALIGN_TOP|FX_HALIGN_CENTER, 2, 0);
		pLayout->setWidget(new FXBox("#0f0"), 0, 3, 0);
		pLayout->setWidget(new FXStaticText(100, 20, "Top-Right", font, ta, "#fff", "#c00"), FX_VALIGN_TOP|FX_HALIGN_RIGHT, 4, 0);

		pLayout->setWidget(new FXBox("#111"), 0, 0, 1);
		pLayout->setWidget(new FXBox("#222"), 0, 1, 1);
		pLayout->setWidget(new FXBox("#333"), 0, 2, 1);
		pLayout->setWidget(new FXBox("#444"), 0, 3, 1);
		pLayout->setWidget(new FXBox("#555"), 0, 4, 1);

		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Left", font, ta, "#fff", "#0c0"), FX_VALIGN_MIDDLE|FX_HALIGN_LEFT, 0, 2);
		pLayout->setWidget(new FXBox("#00f"), 0, 1, 2);
		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Center", font, ta, "#fff", "#080"), FX_VALIGN_MIDDLE|FX_HALIGN_CENTER, 2, 2);
		pLayout->setWidget(new FXBox("#0ff"), 0, 3, 2);
		pLayout->setWidget(new FXStaticText(100, 20, "Middle-Right", font, ta, "#fff", "#040"), FX_VALIGN_MIDDLE|FX_HALIGN_RIGHT, 4, 2);

		pLayout->setWidget(new FXBox("#666"), 0, 0, 3);
		pLayout->setWidget(new FXBox("#777"), 0, 1, 3);
		pLayout->setWidget(new FXBox("#888"), 0, 2, 3);
		pLayout->setWidget(new FXBox("#999"), 0, 3, 3);
		pLayout->setWidget(new FXBox("#aaa"), 0, 4, 3);

		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Left", font, ta, "#fff", "#008"), FX_VALIGN_BOTTOM|FX_HALIGN_LEFT, 0, 4);
		pLayout->setWidget(new FXBox("#ff0"), 0, 1, 4);
		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Center", font, ta, "#fff", "#004"), FX_VALIGN_BOTTOM|FX_HALIGN_CENTER, 2, 4);
		pLayout->setWidget(new FXBox("#f0f"), 0, 3, 4);
		pLayout->setWidget(new FXStaticText(100, 20, "Bottom-Right", font, ta, "#fff", "#00c"), FX_VALIGN_BOTTOM|FX_HALIGN_RIGHT, 4, 4);
*/


/*
		pWidget = new FXTestWidget("#800");
		pWidget->setMinSize(FXSize(100, 50));
		pLayout->setWidget(pWidget, FX_VALIGN_TOP|FX_HALIGN_RIGHT, 1, 1);


		pWidget = new FXTestWidget("#080");
		pWidget->setMinSize(FXSize(200, 100));
		pWidget->setMaxSize(FXSize(200, 100));
		pLayout->setWidget(pWidget, FX_VALIGN_TOP|FX_HALIGN_RIGHT, 3, 1);


		pWidget = new FXTestWidget("#008");
		pWidget->setMinSize(FXSize(200, 100));
		add(pWidget);

		pWidget = new FXTestWidget("#880");
		pWidget->setMinSize(FXSize(100, 50));
		pLayout->addWidget(pWidget);

		pWidget = new FXTestWidget("#808");
		pWidget->setMinSize(FXSize(100, 200));
		pWidget->setMaxSize(FXSize(100, 200));
		pLayout->addWidget(pWidget, FX_VALIGN_BOTTOM|FX_HALIGN_RIGHT);

		pWidget = new FXTestWidget("#088");
		pWidget->setMinSize(FXSize(100, 200));
		pLayout->addWidget(pWidget);
*/		
		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}

}
