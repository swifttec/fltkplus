// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestFXCursor.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>
#include <FL/fl_draw.H>

MainWnd::MainWnd() :
	FXWnd("Test Draw Rect", 640, 480)
		{
		m_cursors[0] = FL_CURSOR_DEFAULT;
		m_cursors[1] = FL_CURSOR_INSERT;
		m_cursors[2].load("d:/Development/FltkPlus/test-programs/TestFXCursor/CustomCursor1.png", 1, 1);
		m_cursors[3].load("d:/Development/FltkPlus/test-programs/TestFXCursor/CustomCursor2.png", 1, 1);
		
		m_regions[0] = FXRect();
		m_regions[1] = FXRect();
		m_regions[2] = FXRect();
		m_regions[3] = FXRect();

		show();


		}



MainWnd::~MainWnd()
		{
		}





void
MainWnd::onDraw()
		{
		FXRect	dr;
		FXFont	font("Arial", 12);

		getClientRect(dr);

		// Top Left
		{
		FXRect	&tr=m_regions[0];

		tr.left = dr.left;
		tr.right = dr.left + (dr.width() / 2) - 1;
		tr.top = dr.top;
		tr.bottom = dr.top + (dr.height() / 2) - 1;
		fx_draw_fillRect(tr, "#fee");
		fx_draw_text("Default cursor", tr, FX_ALIGN_MIDDLE_CENTRE, font, "#000");
		}

		// Top Right
		{
		FXRect	&tr = m_regions[1];

		tr.left = dr.left + (dr.width() / 2);
		tr.right = dr.right;
		tr.top = dr.top;
		tr.bottom = dr.top + (dr.height() / 2) - 1;
		fx_draw_fillRect(tr, "#efe");
		fx_draw_text("I-Beam cursor", tr, FX_ALIGN_MIDDLE_CENTRE, font, "#000");
		}

		// Bottom Left
		{
		FXRect	&tr = m_regions[2];

		tr.left = dr.left;
		tr.right = dr.left + (dr.width() / 2) - 1;
		tr.top = dr.top + (dr.height() / 2);
		tr.bottom = dr.bottom;
		fx_draw_fillRect(tr, "#eef");
		fx_draw_text("Custom cursor 1", tr, FX_ALIGN_MIDDLE_CENTRE, font, "#000");
		}

		// Bottom Right
		{
		FXRect	&tr = m_regions[3];

		tr.left = dr.left + (dr.width() / 2);
		tr.right = dr.right;
		tr.top = dr.top + (dr.height() / 2);
		tr.bottom = dr.bottom;
		fx_draw_fillRect(tr, "#fef");
		fx_draw_text("Custom cursor 2", tr, FX_ALIGN_MIDDLE_CENTRE, font, "#000");
		}

		for (int i=0;i<(int)m_regions.size();i++)
			{
			FXRect	&tr = m_regions[i];

			SW_TRACE("Region %d : %d, %d, %d, %d\n", i, tr.left, tr.top, tr.right, tr.bottom);
			}
		}


int
MainWnd::onMouseMove(const FXPoint &pt)
		{
		for (int i=0;i<(int)m_regions.size();i++)
			{
			if (m_regions[i].contains(pt))
				{
				m_cursors[i].applyToWindow(this);
				break;
				}
			}

		return 1;
		}
