
// TestFXCursor.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestFXCursor.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig("SwiftTec", "FltkExt");
TestFXCursor	theApp;

FX_APP_EXECUTE(theApp);

TestFXCursor::TestFXCursor() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestFXCursor::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestFXCursor::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
