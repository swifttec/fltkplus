// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXCursor.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();


public: // Overrides
		virtual void		onDraw();

		/// Called when the mouse is moved
		virtual int			onMouseMove(const FXPoint &pt);

public:
		SWArray<FXRect>		m_regions;
		SWArray<FXCursor>	m_cursors;
		};
