// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawTriangle.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Triangles", 640, 480)
		{
		show();

		m_points[0] = FXPoint(150, 100);
		m_points[1] = FXPoint(100, 200);
		m_points[2] = FXPoint(250, 200);

		m_points[3] = FXPoint(450, 200);
		m_points[4] = FXPoint(300, 300);
		m_points[5] = FXPoint(550, 400);
		
		m_selected = 0;
		}



MainWnd::~MainWnd()
		{
		}




int
MainWnd::onKeyDown(int code, const SWString &text, const FXPoint &pt)
		{
		int	res=0;

		switch (code)
			{
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
				m_selected = code - '1';
				res = 1;
				break;

			case FL_Left:
				m_points[m_selected].x -= 10;
				res = 1;
				break;

			case FL_Right:
				m_points[m_selected].x += 10;
				res = 1;
				break;

			case FL_Up:
				m_points[m_selected].y -= 10;
				res = 1;
				break;

			case FL_Down:
				m_points[m_selected].y += 10;
				res = 1;
				break;
			
			}

		if (res != 0) redraw();
		
		return res;
		}


void
MainWnd::onDraw()
		{
		FXRect	dr;

		fx_draw_triangle(m_points[0].x, m_points[0].y, m_points[1].x, m_points[1].y, m_points[2].x, m_points[2].y, "#000");
		fx_draw_triangle(m_points[3].x, m_points[3].y, m_points[4].x, m_points[4].y, m_points[5].x, m_points[5].y, FX_COLOR_NONE, "#000");
		}
