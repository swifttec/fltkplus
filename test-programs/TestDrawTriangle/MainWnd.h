// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();


public: // Overrides
		virtual void	onDraw();

		/// Called when a mouse button is pressed, return non-zero to track
		virtual int		onKeyDown(int code, const SWString &text, const FXPoint &pt);

protected:
		FXPoint			m_points[6];
		int				m_selected;
		};
