
// TestListBox.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestListBox.h"
#include "MainDialog.h"

TestListBox		theApp;
SWAppConfig		theAppConfig;

FX_APP_EXECUTE(theApp);

TestListBox::TestListBox() :
	FXApp(&theAppConfig, true)
		{
		}


bool
TestListBox::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			MainDialog	dlg;

			dlg.doModal();

			res = true;
			}

		return res;
		}

