#include "stdafx.h"

#include "TestListBox.h"
#include "MainDialog.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXListBox.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>
#include <FL/Fl_Return_Button.H>



#define DLGWIDTH	300
#define	DLGHEIGHT	250


MainDialog::MainDialog() :
	FXDialog("Test List Box", DLGWIDTH, DLGHEIGHT)
		{
		int		ypos=10;
		int		xpos=10;
		int		rowheight=FXFont::getDefaultFont().height()+10;
		int		itemid=1000;
		int		ygap=4;

		addStaticText(FX_IDSTATIC, "This is an FXListBox:",				xpos,		ypos, 0, rowheight); 
		
		ypos += rowheight+ygap;

		FXListBox	*pListBox;

		addItem(1000, pListBox = new FXListBox(xpos, ypos, DLGWIDTH-(xpos*2), DLGHEIGHT-ypos-rowheight-20, FXListBox::ModeMultiSelect));

		SWStringArray	sa;

		for (int line=1;line<=20;line++)
			{
			sa[0].format("line %d, column 0", line);
			sa[1].format("This is actually column 1");

			pListBox->addRow(sa);
			}

		pListBox->resizeColumns();

		addButton(FX_IDCANCEL,	"Cancel",	FX_BTN_STANDARD,			DLGWIDTH-100, DLGHEIGHT-rowheight-10, 90, rowheight);
		addButton(FX_IDOK,		"OK",		FX_BTN_DEFAULT,				DLGWIDTH-200, DLGHEIGHT-rowheight-10, 90, rowheight);
		}


MainDialog::~MainDialog()
		{
		}


int
MainDialog::onButtonClicked(Fl_Widget *pWidget)
		{
		int		res=0;
		
		return res;
		}

void
MainDialog::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		bool	handled=false;

		SW_TRACE("Item %d - %x\n", pWidget->id(), p);
		if (!handled)
			{
			FXDialog::onItemCallback(pWidget, p);
			}
		}
