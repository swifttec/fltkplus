/**
*** @defgroup	TestListBox	TestDialog
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

/**
*** @ingroup TestListBox
**/
class TestListBox : public FXApp
		{
public:
		/// Constructor
		TestListBox();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);
		};

extern TestListBox theApp;