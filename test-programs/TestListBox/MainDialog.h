#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXDialog.h>

/**
*** @ingroup TestDialogApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainDialog : public FXDialog
		{
public:
		/// Constructor - generates all the internal boxes
		MainDialog();

		/// Destructor
		virtual ~MainDialog();

protected: // FXDialog override

		/// Called when a button is clicked - return non-zero handled
		virtual int		onButtonClicked(Fl_Widget *pWidget);

		/// Called when a item issues a callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);
		};

#endif // __MainWindow_h__
