
// TestDrawImage.h : main header file for the TestDrawImage application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestDrawImage:
// See TestDrawImage.cpp for the implementation of this class
//
class MainWnd;

class TestDrawImage : public FXApp
		{
public:
		TestDrawImage();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestDrawImage theApp;
