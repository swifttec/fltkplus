
// TestDrawImage.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDrawImage.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig;
TestDrawImage	theApp;

FX_APP_EXECUTE(theApp);

TestDrawImage::TestDrawImage() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestDrawImage::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestDrawImage::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
