// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawImage.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Image", 640, 480)
		{
		show();

		setBackgroundColor("#444");

		if (SWFileInfo::isFile("../resources/TestImage.png")) m_image.load("../resources/TestImage.png");
		else if (SWFileInfo::isFile("resources/TestImage.png")) m_image.load("resources/TestImage.png");
		}



MainWnd::~MainWnd()
		{
		}




void
MainWnd::onDraw()
		{
		FXRect	cr;

		getContentsRect(cr);
		m_image.render(cr, FX_ALIGN_MIDDLE_CENTRE);
		}
