
// TestSingleWidget.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestSingleWidget.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestSingleWidget { TestSingleWidget	theApp; }

FX_APP_EXECUTE(TestSingleWidget::theApp);

namespace TestSingleWidget {

TestSingleWidget::TestSingleWidget()
		{
		}


bool
TestSingleWidget::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestSingleWidget::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
