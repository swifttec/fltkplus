#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXWnd.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>

namespace TestSingleWidget {

/**
*** @ingroup TestWidgetsApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

		/// Update the icon
		void			updateIcon();
		};

}

#endif // __MainWindow_h__
