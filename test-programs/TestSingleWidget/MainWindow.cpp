#include "stdafx.h"

#include "TestSingleWidget.h"
#include "MainWindow.h"

#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXPanelLayout.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestSingleWidget {

#define WW	300
#define WH	250

MainWindow::MainWindow(int width, int height) :
	FXWnd("Test Single Widget", WW*3, WH*3)
		{
		setBackgroundColor("#000");

		setLayout(new FXPanelLayout(true));
		FXTestWidget	*p=new FXTestWidget("#444");

		p->resize(0, 0, w(), h());
		add(p);

		updateIcon();

		theApp.m_pMainWnd = this;
		}

void
MainWindow::updateIcon()
		{
		// Set our icon
		// Fl_PNG_Image	 pngicon(theAppConfig.getResourceFile("VeristoreX.png"));
		Fl_PNG_Image		pngicon("resources/TestSingleWidget.png");

		icon(&pngicon);
		iconlabel("TestSingleWidget");
		label("TestSingleWidget");
		xclass("TestSingleWidget");
		}


MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


}
