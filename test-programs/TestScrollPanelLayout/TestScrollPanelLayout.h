
// TestScrollPanelLayout.h : main header file for the TestScrollPanelLayout application
//
#pragma once

#include <FltkExt/FXApp.h>


// TestScrollPanelLayout:
// See TestScrollPanelLayout.cpp for the implementation of this class
//
class MainWnd;

class TestScrollPanelLayout : public FXApp
		{
public:
		TestScrollPanelLayout();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestScrollPanelLayout theApp;
