
// TestScrollPanelLayout.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestScrollPanelLayout.h"

#include "MainWnd.h"

TestScrollPanelLayout	theApp;

FX_APP_EXECUTE(theApp);

TestScrollPanelLayout::TestScrollPanelLayout()
		{
		}

bool
TestScrollPanelLayout::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestScrollPanelLayout::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		pMainWnd->show();
		}
