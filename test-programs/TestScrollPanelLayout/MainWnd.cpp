// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestScrollPanelLayout.h"

#include "MainWnd.h"

#include <FltkExt/FXScrollableGroup.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXFixedLayout.h>


MainWnd::MainWnd() :
	FXWnd("Test Scroll Panel Layout", 640, 480)
		{
		end();

		setBackgroundColor("#567");
		setLayout(new FXPanelLayout(false, 0, FXPadding(10, 10, 10, 10)));

		FXGroup			*pGroup;

		pGroup = new FXScrollableGroup(FXScrollableGroup::VScrollBar);
		pGroup->setBackgroundColor("#789");
		pGroup->setLayout(new FXPanelLayout(true, 2));

		FXTestWidget	*p;

		pGroup->add(p = new FXTestWidget("#f00", "#fff"));
		p->setMinMaxSizes(64, 64, 0, 64);
		
		pGroup->add(p = new FXTestWidget("#0f0", "#fff"));
		p->setMinMaxSizes(64, 64, 0, 64);
		
		pGroup->add(p = new FXTestWidget("#00f", "#fff"));
		p->setMinMaxSizes(64, 64, 0, 64);

		add(pGroup);
		}



MainWnd::~MainWnd()
		{
		}



