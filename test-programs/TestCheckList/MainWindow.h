#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>


#include <FltkExt/FXWnd.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXToolbar.h>
#include <FltkExt/FXCheckList.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Text_Display.H>

namespace TestCheckList {

/**
*** @ingroup TestCheckListApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();
	
		/// Called to handle an item callback
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

protected:
		FXCheckList		*m_pCheckList;
		};

}

#endif // __MainWindow_h__
