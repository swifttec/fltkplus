
// TestCheckListApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestCheckList.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestCheckList { TestCheckListApp	theApp; }

FX_APP_EXECUTE(TestCheckList::theApp);

namespace TestCheckList {

TestCheckListApp::TestCheckListApp()
		{
		}


bool
TestCheckListApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(800, 600);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestCheckListApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
