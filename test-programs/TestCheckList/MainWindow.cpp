#include "stdafx.h"

#include "TestCheckList.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>
#include <FltkExt/FXGroup.h>
#include <FltkExt/FXImageButton.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>

namespace TestCheckList {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#888");
		setForegroundColor("#fff");

		setLayout(new FXPanelLayout(true), true);

		add(m_pCheckList = new FXCheckList(0, h()-96, w(), 88, "#000"));

		int		id=100;
		int		group=1000, subgroup=2000;

		m_pCheckList->setDefaultFont(FXFont("Arial", 14, FXFont::Normal));
		m_pCheckList->setBackgroundColor("#def");

		m_pCheckList->addItem(++group, "Group 1", "../resources/BulbClear.png");

		m_pCheckList->addItemTo(group, ++subgroup, "AAA", "../resources/BulbYellow.png");
		m_pCheckList->addItemTo(subgroup, id++, "xxx");
		m_pCheckList->addItemTo(subgroup, id++, "yyy");
		m_pCheckList->addItemTo(subgroup, id++, "zzz");
		m_pCheckList->addItemTo(group, ++subgroup, "BBB", "../resources/BulbYellow.png");
		m_pCheckList->addItemTo(subgroup, id++, "qwerty");
		m_pCheckList->addItemTo(subgroup, id++, "asdfg");
		m_pCheckList->addItemTo(subgroup, id++, "zxcvb");
		m_pCheckList->addItemTo(group, ++subgroup, "CCC", "../resources/BulbYellow.png");
		m_pCheckList->addItemTo(subgroup, id++, "123456");
		m_pCheckList->addItemTo(subgroup, id++, "456789");
		m_pCheckList->addItemTo(group, id++, "G1A");
		m_pCheckList->addItemTo(group, id++, "G1B");

		m_pCheckList->addItem(++group, "Group 2", "../resources/BulbClear.png");
		m_pCheckList->addItemTo(group, id++, "G2C");
		m_pCheckList->addItemTo(group, id++, "G2D");
		m_pCheckList->addItemTo(group, id++, "G2E");

		m_pCheckList->addItem(id++, "Item 1");
		m_pCheckList->addItem(id++, "Item 2");

		m_pCheckList->addItem(id++, "Item 3");
		m_pCheckList->addItem(id++, "Item 4");
		
		m_pCheckList->addItem(++group, "Group 3", "../resources/BulbClear.png");
		m_pCheckList->addItemTo(group, id++, "Group 1 / Item 1");
		m_pCheckList->addItemTo(group, id++, "Group 1 / Item 2");
		m_pCheckList->addItemTo(group, ++subgroup, "AAA");
		m_pCheckList->addItemTo(subgroup, id++, "xxx");
		m_pCheckList->addItemTo(subgroup, id++, "yyy");
		m_pCheckList->addItemTo(subgroup, id++, "zzz");
		m_pCheckList->addItemTo(group, ++subgroup, "BBB");
		m_pCheckList->addItemTo(subgroup, id++, "qwerty");
		m_pCheckList->addItemTo(subgroup, id++, "asdfg");
		m_pCheckList->addItemTo(subgroup, id++, "zxcvb");
		m_pCheckList->addItemTo(group, ++subgroup, "CCC");
		m_pCheckList->addItemTo(subgroup, id++, "123456");
		m_pCheckList->addItemTo(subgroup, id++, "456789");
		m_pCheckList->addItemTo(group, id++, "Group 1 / Item 3");
		
		m_pCheckList->addItem(++group, "Group 2", "../resources/BulbClear.png");
		m_pCheckList->addItemTo(group, id++, "Group 2 / Item 1");
		m_pCheckList->addItemTo(group, id++, "Group 2 / Item 2");
		
		m_pCheckList->addItem(id++, "Item 5");
		m_pCheckList->addItem(id++, "Item 6");

		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


void
MainWindow::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		TRACE("Got callback for item %u", pWidget->id());
		}



}