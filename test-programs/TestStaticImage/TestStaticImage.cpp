
// TestStaticImage.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestStaticImage.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestStaticImage { TestStaticImage	theApp; }

FX_APP_EXECUTE(TestStaticImage::theApp);

namespace TestStaticImage {

TestStaticImage::TestStaticImage()
		{
		}


bool
TestStaticImage::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestStaticImage::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
