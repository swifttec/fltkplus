#include "stdafx.h"

#include "TestStaticImage.h"
#include "MainWindow.h"

#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXButtonBar.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestStaticImage {

enum CommandIDs
	{
	ID_HLEFT=100,
	ID_HCENTER,
	ID_HRIGHT,
	ID_VTOP,
	ID_VMIDDLE,
	ID_VBOTTOM,
	ID_NOSTRETCH,
	ID_STRETCH,
	ID_HSTRETCH,
	ID_WSTRETCH,
	ID_UNPROPORTIONAL,
	ID_PROPORTIONAL
	};

#define WW	800
#define WH	600

MainWindow::MainWindow(int width, int height) :
	FXWnd("Test Static Image", WW, WH)
		{
		setBackgroundColor("#def");
		setLayout(new FXPanelLayout(true));

		FXButtonBar		*pBar;

		add(pBar = new FXButtonBar(0, 0, w(), 32, false, 48, 4, 2));
		
		pBar->setMaxSize(0, 32);
		pBar->setMinSize(0, 32);
		pBar->addButton(ID_HLEFT,	"Left", "");
		pBar->addButton(ID_HCENTER,	"Centre", "");
		pBar->addButton(ID_HRIGHT,	"Right", "");
		pBar->addButton(ID_VTOP,	"Top", "");
		pBar->addButton(ID_VMIDDLE,	"Middle", "");
		pBar->addButton(ID_VBOTTOM,	"Bottom", "");
		pBar->addButton(ID_NOSTRETCH,	"No Stretch", "");
		pBar->addButton(ID_STRETCH,	"Fit", "");
		pBar->addButton(ID_WSTRETCH,	"Width", "");
		pBar->addButton(ID_HSTRETCH,	"Height", "");
		pBar->addButton(ID_UNPROPORTIONAL,	"Unproportional", "");
		pBar->addButton(ID_PROPORTIONAL,	"Proportional", "");

		add(m_pImage = new FXStaticImage(10, 10, w()-20, h()-20, "../resources/TestImage.png", FX_VALIGN_CENTER|FX_HALIGN_CENTER));
		m_pImage->setBorder(0, "#000", 1, 0);

		theApp.m_pMainWnd = this;
		}


MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


void
MainWindow::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		switch (pWidget->id())
			{
			case ID_HLEFT:
				m_pImage->flags((m_pImage->flags() & ~FX_HALIGN_MASK) | FX_HALIGN_LEFT);
				break;
			
			case ID_HCENTER:
				m_pImage->flags((m_pImage->flags() & ~FX_HALIGN_MASK) | FX_HALIGN_CENTER);
				break;
			
			case ID_HRIGHT:
				m_pImage->flags((m_pImage->flags() & ~FX_HALIGN_MASK) | FX_HALIGN_RIGHT);
				break;
			
			case ID_VTOP:
				m_pImage->flags((m_pImage->flags() & ~FX_VALIGN_MASK) | FX_VALIGN_TOP);
				break;
			
			case ID_VMIDDLE:
				m_pImage->flags((m_pImage->flags() & ~FX_VALIGN_MASK) | FX_VALIGN_CENTER);
				break;
			
			case ID_VBOTTOM:
				m_pImage->flags((m_pImage->flags() & ~FX_VALIGN_MASK) | FX_VALIGN_BOTTOM);
				break;
			
			case ID_NOSTRETCH:
				m_pImage->flags((m_pImage->flags() & ~(FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_STRETCH_TO_WIDTH)));
				break;
			
			case ID_STRETCH:
				m_pImage->flags((m_pImage->flags() & ~(FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_STRETCH_TO_WIDTH)) | FX_IMAGE_STRETCH_TO_HEIGHT| FX_IMAGE_STRETCH_TO_WIDTH);
				break;
			
			case ID_HSTRETCH:
				m_pImage->flags((m_pImage->flags() & ~(FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_STRETCH_TO_WIDTH)) | FX_IMAGE_STRETCH_TO_HEIGHT);
				break;
			
			case ID_WSTRETCH:
				m_pImage->flags((m_pImage->flags() & ~(FX_IMAGE_STRETCH_TO_HEIGHT|FX_IMAGE_STRETCH_TO_WIDTH)) | FX_IMAGE_STRETCH_TO_WIDTH);
				break;
			
			case ID_UNPROPORTIONAL:
				m_pImage->flags((m_pImage->flags() & ~(FX_IMAGE_PROPORTIONAL)));
				break;
			
			case ID_PROPORTIONAL:
				m_pImage->flags((m_pImage->flags() | FX_IMAGE_PROPORTIONAL));
				break;
			}
		
		invalidate();
		}

}
