#include "stdafx.h"

#include "TestLayers.h"
#include "MainWindow.h"

#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestLayers {


enum ButtonIDs
	{
	BTN_ENGINEERING=100,
	BTN_REPLAY,
	BTN_QUARANTINE,
	BTN_RECORDINGS,
	BTN_SYSLOG,
	BTN_EVENTS,
	BTN_ADMIN,
	BTN_LOGIN,
	BTN_LOGOUT,
	BTN_EXIT
	};

#define WW	300
#define WH	250

MainWindow::MainWindow(int width, int height) :
	FXWnd("TestLayers", WW*3, WH*3)
		{
		end();

		box(FL_FLAT_BOX);
		color(FL_BLACK);

		resizable(*this);

		end();

		m_pGroup = new FXLayeredGroup(0, 0, w(), h());

		add(m_pGroup);
		m_pGroup->addLayer(0, new FXTestWidget("#f00"));
		m_pGroup->addLayer(1, new FXTestWidget("#ff0"));
		m_pGroup->addLayer(2, new FXTestWidget("#0f0"));
		m_pGroup->addLayer(3, new FXTestWidget("#0ff"));
		m_pGroup->addLayer(4, new FXTestWidget("#00f"));
		m_pGroup->addLayer(5, new FXTestWidget("#f0f"));
		m_pGroup->addLayer(6, new FXTestWidget("#fff"));
		m_pGroup->addLayer(7, new FXTestWidget("#000"));

		updateIcon();

		// Finally force a resize to keep Linux version happy
		// pGroup->resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;

		setIntervalTimerMS(1, 1000, NULL);
		}


bool
MainWindow::onTimer(sw_uint32_t id, void *data)
		{
		m_pGroup->showLayer((m_pGroup->getCurrentLayer()->id() + 1) % 8);
		return true;
		}

void
MainWindow::updateIcon()
		{
		// Set our icon
		// Fl_PNG_Image	 pngicon(theAppConfig.getResourceFile("VeristoreX.png"));
		Fl_PNG_Image		pngicon("resources/VeristoreX.png");

		icon(&pngicon);
		iconlabel("VeristoreX");
		label("VeristoreX");
		xclass("VeristoreX");
		}


MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


int
MainWindow::handle(int event)
		{
		// TRACE("MainWindow::handle(%d)\n", event);

		return Fl_Double_Window::handle(event);
		}

}
