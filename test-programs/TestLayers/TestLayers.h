/**
*** @defgroup	TestLayers	TestLayers
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestLayers {

class MainWindow;

/**
*** @ingroup TestLayers
**/
class TestLayers : public FXApp
		{
public:
		/// Constructor
		TestLayers();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestLayers theApp;

}
