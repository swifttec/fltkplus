#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXWnd.h>
#include <FltkExt/FXLayeredGroup.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>

namespace TestLayers {

/**
*** @ingroup TestLayersApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

		/// Handle the given event
		virtual int		handle(int event);

		/// Update the icon
		void			updateIcon();

		/// Called to handle a timer callback. Return true to continue timer, false to cancel
		virtual bool	onTimer(sw_uint32_t id, void *data);

protected:
		FXLayeredGroup	*m_pGroup;
		};

}

#endif // __MainWindow_h__
