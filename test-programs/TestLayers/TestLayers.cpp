
// TestLayers.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestLayers.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestLayers { TestLayers	theApp; }

FX_APP_EXECUTE(TestLayers::theApp);

namespace TestLayers {

TestLayers::TestLayers()
		{
		}


bool
TestLayers::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestLayers::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
