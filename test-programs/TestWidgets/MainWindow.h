#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXButtonBar.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>

namespace TestWidgets {

/**
*** @ingroup TestWidgetsApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public Fl_Double_Window
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

		/// Handle the given event
		virtual int		handle(int event);

		/// Update the icon
		void			updateIcon();
		};

}

#endif // __MainWindow_h__
