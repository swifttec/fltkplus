
// TestWidgetsApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestWidgetsApp.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestWidgets { TestWidgetsApp	theApp; }

FX_APP_EXECUTE(TestWidgets::theApp);

namespace TestWidgets {

TestWidgetsApp::TestWidgetsApp()
		{
		}


bool
TestWidgetsApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestWidgetsApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
