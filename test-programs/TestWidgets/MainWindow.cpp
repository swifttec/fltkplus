#include "stdafx.h"

#include "TestWidgetsApp.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXStaticText.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestWidgets {


enum ButtonIDs
	{
	BTN_ENGINEERING=100,
	BTN_REPLAY,
	BTN_QUARANTINE,
	BTN_RECORDINGS,
	BTN_SYSLOG,
	BTN_EVENTS,
	BTN_ADMIN,
	BTN_LOGIN,
	BTN_LOGOUT,
	BTN_EXIT
	};

#define WW	300
#define WH	250

MainWindow::MainWindow(int width, int height) :
	Fl_Double_Window(WW*3, WH*3)
		{
		box(FL_FLAT_BOX);
		color(FL_BLACK);

		resizable(*this);

		FXPaneGroup	*pGroup=new FXPaneGroup(0, 0, w(), h(), false);

		pGroup->begin();
			{
			FXTestWidget	*pWidget;

			pWidget = new FXTestWidget("#800");
			pWidget->setMinSize(FXSize(100, 50));

			pWidget = new FXTestWidget("#080");
			pWidget->setMinSize(FXSize(200, 100));

			pWidget = new FXTestWidget("#008");
			pWidget->setMinSize(FXSize(100, 50));

			pWidget = new FXTestWidget("#808");
			pWidget->setMinSize(FXSize(100, 200));
			}
		pGroup->end();
		
//		pHGroup->setChildSizes(0, 170, 170);

		end();

		updateIcon();

		// Finally force a resize to keep Linux version happy
		pGroup->resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}

void
MainWindow::updateIcon()
		{
		// Set our icon
		// Fl_PNG_Image	 pngicon(theAppConfig.getResourceFile("VeristoreX.png"));
		Fl_PNG_Image		pngicon("resources/VeristoreX.png");

		icon(&pngicon);
		iconlabel("VeristoreX");
		label("VeristoreX");
		xclass("VeristoreX");
		}


MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


int
MainWindow::handle(int event)
		{
		// TRACE("MainWindow::handle(%d)\n", event);

		return Fl_Double_Window::handle(event);
		}

}
