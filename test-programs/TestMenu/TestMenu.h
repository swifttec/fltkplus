/**
*** @defgroup	TestMenuApp	TestMenu
***
*** A test application for testing various aspects of the FXWidget class
**/
#pragma once

#include <FltkExt/FXApp.h>

namespace TestMenu {

class MainWindow;

/**
*** @ingroup TestMenuApp
**/
class TestMenuApp : public FXApp
		{
public:
		/// Constructor
		TestMenuApp();

// Overrides
public:
		/// Called to intialise the instance of this app
		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

		/// Call to cleanup the instance of this app
		virtual int		exitInstance();

// Implementation

public:
		MainWindow	*m_pMainWnd;	///< The main window
		};

extern TestMenuApp theApp;

#define APP_TITLE	"TestMenu"

}
