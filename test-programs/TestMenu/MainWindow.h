#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>


#include <FltkExt/FXWnd.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>

namespace TestMenu {

/**
*** @ingroup TestMenuApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();
		
protected:
		virtual void	onItemCallback(Fl_Widget *pWidget, void *p);

		};

}

#endif // __MainWindow_h__
