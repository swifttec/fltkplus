#include "stdafx.h"

#include "TestMenu.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXPanelLayout.h>
#include <FltkExt/FXMenuBar.h>
#include <FltkExt/FXMenuItem.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestMenu {

enum
	{
	IDC_MENU=100,
	IDC_CONTENTS
	};


MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#888");
		setForegroundColor("#fff");
		
		setLayout(new FXPanelLayout(true));

		FXMenuBar	*pMenuBar;

		add(IDC_MENU, pMenuBar = new FXMenuBar(), true);
		add(IDC_CONTENTS, new FXStaticText("Select from the menu bar or right-click for a popup menu", FXFont("Arial", 12, 0)));

		int		id=1000;

		pMenuBar->add(id++,	"File/Open");
		pMenuBar->add(id++,	"File/_Close");
		pMenuBar->add(id++,	"File/Exit");

		pMenuBar->add(id++,	"Edit/Copy");
		pMenuBar->add(id++,	"Edit/Cut");
		pMenuBar->add(id++,	"Edit/Paste");

		pMenuBar->add(id++,	"Edit/Item/Text");
		pMenuBar->add(id++,	"Edit/Item/Properties");
		pMenuBar->add(id++,	"Edit/Item/Info");

		pMenuBar->add(id++,	"Checklist/.One");
		pMenuBar->add(id++,	"Checklist/.Two");
		pMenuBar->add(id++,	"Checklist/.Three");
		pMenuBar->add(id++,	"Checklist/.Four");

		pMenuBar->add(id++,	"Radio/:A-One");
		pMenuBar->add(id++,	"Radio/:A-Two");
		pMenuBar->add(id++,	"Radio/:A-Three");
		pMenuBar->add(id++,	"Radio/_:A-Four");

		pMenuBar->add(id++,	"Radio/:B-One");
		pMenuBar->add(id++,	"Radio/:B-Two");
		pMenuBar->add(id++,	"Radio/:B-Three");
		pMenuBar->add(id++,	"Radio/:B-Four");

		pMenuBar->add(id++,	"Help/About");

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}


void
MainWindow::onItemCallback(Fl_Widget *pWidget, void *p)
		{
		if (pWidget->id() == IDC_MENU)
			{
			FXMenuItem	*pItem=(FXMenuItem *)p;

			if (pItem != NULL)
				{
				SWString	s;

				s.format("Selected item #%d - %s", pItem->id(), pItem->path().c_str());

				FXStaticText	*pText=dynamic_cast<FXStaticText *>(getItem(IDC_CONTENTS));

				if (pText != NULL)
					{
					pText->setText(s);
					invalidate();
					}
				}
			}
		}

}
