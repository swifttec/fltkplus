
// TestMenuApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestMenu.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestMenu { TestMenuApp	theApp; }

FX_APP_EXECUTE(TestMenu::theApp);

namespace TestMenu {

TestMenuApp::TestMenuApp()
		{
		}


bool
TestMenuApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestMenuApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
