
// TestDrawing.h : main header file for the TestDrawing application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestDrawing:
// See TestDrawing.cpp for the implementation of this class
//
class MainWnd;

class TestDrawing : public FXApp
		{
public:
		TestDrawing();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestDrawing theApp;
