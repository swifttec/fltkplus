// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestDrawing.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

#include <FL/fl_draw.H>

#include <xplatform/sw_math.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Arc", 640, 480)
		{
		show();
		}



MainWnd::~MainWnd()
		{
		}



void
MainWnd::onDraw()
		{
		FXRect	dr;

		getClientRect(dr);
		fx_draw_fillRect(dr, RGB(255, 255, 255));

		fl_color(FL_BLACK);

		
		dr.left = 10;
		dr.top = 10;
		dr.bottom = dr.top + 100;
		dr.right = dr.left + 100;
		fx_draw_box(dr, "#f00", 1, FX_COLOR_NONE, 0);
		fx_draw_text("Empty box", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");

		dr.moveBy(110, 0);
		fx_draw_box(dr, "#f00", 1, "#400", 0);
		fx_draw_text("Filled box", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#fff");

		dr.moveBy(110, 0);
		fx_draw_ellipse(dr, "#00f");
		fx_draw_text("Ellipse", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");

		dr.moveTo(10, 120);
		fx_draw_pie(dr, "#00f", 0.0, 360.0);
		fx_draw_text("Pie 0-360", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");

		dr.moveBy(110, 0);
		fx_draw_pie(dr, "#00f", 0.0, 90.0);
		fx_draw_text("Pie 0-90", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");

		dr.moveBy(110, 0);
		fx_draw_arc(dr, "#00f", 0.0, 360.0);
		fx_draw_text("Arc 0-360", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");

		dr.moveBy(110, 0);
		fx_draw_arc(dr, "#00f", 0.0, 90.0);
		fx_draw_text("Arc 0-90", dr, FX_ALIGN_MIDDLE_CENTRE, FXFont("Arial", 10), "#000");
		}
