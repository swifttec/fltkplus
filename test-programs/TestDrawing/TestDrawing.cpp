
// TestDrawing.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDrawing.h"
#include "MainWnd.h"

SWAppConfig	theAppConfig;
TestDrawing	theApp;

FX_APP_EXECUTE(theApp);

TestDrawing::TestDrawing() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestDrawing::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestDrawing::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		}
