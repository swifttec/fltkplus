#ifndef __MainWindow_h__
#define __MainWindow_h__

#include <swift/SWString.h>

#include <FltkExt/FXWnd.h>

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_PNG_Image.H>

namespace TestDragAndDrop {

/**
*** @ingroup TestDragAndDropApp
***
*** The main display window which sets up all the windows (boxes) and handles
*** basic events.
**/
class MainWindow : public FXWnd
		{
public:
		/// Constructor - generates all the internal boxes
		MainWindow(int width, int height);

		/// Destructor
		virtual ~MainWindow();

protected: /// FXWidget overrides

		/// Called to draw the widget which by default does nothing
		virtual void	onDraw();

		/// Called when to handle drag and drop
		virtual int		onDragAndDrop(int action, const FXPoint &pt);

		/// Called when to handle a paste operation
		virtual int		onPaste(const SWString &text, const FXPoint &pt);

protected:
		SWString		m_contents;
		};

}

#endif // __MainWindow_h__
