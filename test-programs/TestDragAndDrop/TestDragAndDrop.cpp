
// TestDragAndDropApp.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestDragAndDrop.h"
#include "MainWindow.h"

#include <FL/Fl_PNG_Image.H>

namespace TestDragAndDrop { TestDragAndDropApp	theApp; }

FX_APP_EXECUTE(TestDragAndDrop::theApp);

namespace TestDragAndDrop {

TestDragAndDropApp::TestDragAndDropApp()
		{
		}


bool
TestDragAndDropApp::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=false;

		if (FXApp::initInstance(prog, args))
			{
			new MainWindow(600, 400);
			m_pMainWnd->show();

			res = true;
			}

		return res;
		}



int
TestDragAndDropApp::exitInstance()
		{
		return FXApp::exitInstance();
		}

}
