#include "stdafx.h"

#include "TestDragAndDrop.h"
#include "MainWindow.h"

#include <FltkExt/FXPaneGroup.h>
#include <FltkExt/FXTestWidget.h>
#include <FltkExt/FXFixedLayout.h>
#include <FltkExt/FXStaticText.h>
#include <FltkExt/FXBox.h>
#include <FltkExt/FXGroup.h>

#include <FL/fl_draw.H>
#include <FL/Fl_Tile.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl.H>


namespace TestDragAndDrop {

MainWindow::MainWindow(int width, int height) :
	FXWnd(APP_TITLE, 640, 480)
		{
		setBackgroundColor("#888");
		setForegroundColor("#fff");

		m_contents = "Test Drag and Drop / Paste";

		// Finally force a resize to keep Linux version happy
 		resize(x(), y(), w(), h());

		theApp.m_pMainWnd = this;
		}



MainWindow::~MainWindow()
		{
		theApp.m_pMainWnd = NULL;
		}



void
MainWindow::onDraw()
		{
		FXRect	cr;

		getClientRect(cr);

		fl_font(FL_HELVETICA, 12);
		fx_draw_text(m_contents, cr, FX_VALIGN_MIDDLE|FX_HALIGN_CENTER);
		}


int
MainWindow::onDragAndDrop(int action, const FXPoint &pt)
		{
		return 1;
		}


int
MainWindow::onPaste(const SWString &text, const FXPoint &pt)
		{
		m_contents = text;
		redraw();
		return 1;
		}

}
