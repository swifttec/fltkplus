
// TestWindowPosition.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "TestWindowPosition.h"
#include "MainWnd.h"

SWAppConfig			theAppConfig("SwiftTec", "FltkExt");
TestWindowPosition	theApp;

FX_APP_EXECUTE(theApp);

TestWindowPosition::TestWindowPosition() :
	FXApp(&theAppConfig, true)
		{
		}

bool
TestWindowPosition::initInstance(const SWString &prog, const SWStringArray &args)
		{
		bool	res=FXApp::initInstance(prog, args);
		
		if (res)
			{
			createWindows();
			}

		return res;
		}


void
TestWindowPosition::createWindows()
		{
		// Init the windows
		MainWnd	*pMainWnd;
		
		pMainWnd = new MainWnd();
		m_pMainWnd = pMainWnd;
		m_pMainWnd->restoreState();
		m_pMainWnd->show();
		}
