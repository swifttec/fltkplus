// MainWnd.h : header file
//

#pragma once

#include <FltkExt/FXWnd.h>


// MainWnd dialog
class MainWnd : public FXWnd
		{
public:
		MainWnd();
		virtual ~MainWnd();


public: // Overrides
		virtual void	onDraw();

		/// Handle the resize request
		virtual void	resize(int X, int Y, int W, int H);

		/// Handle the resize request
		virtual void	onClose();

		/// Called when the widget is created
		virtual void	restoreState();
		};
