
// TestWindowPosition.h : main header file for the TestWindowPosition application
//
#pragma once

#include <FltkExt/FXApp.h>

// TestWindowPosition:
// See TestWindowPosition.cpp for the implementation of this class
//
class MainWnd;

class TestWindowPosition : public FXApp
		{
public:
		TestWindowPosition();

		void			createWindows();

		virtual bool	initInstance(const SWString &prog, const SWStringArray &args);

protected:
		MainWnd			*m_pMainWnd;
		};

extern TestWindowPosition	theApp;
extern SWAppConfig			theAppConfig;
