// MainWnd.cpp : implementation file
//

#include "stdafx.h"

#include "TestWindowPosition.h"

#include "MainWnd.h"

#include <FltkExt/FXColorFont.h>
#include <FltkExt/FXTestWnd.h>

MainWnd::MainWnd() :
	FXWnd("Test Draw Text", 800, 600)
		{
		setBackgroundColor("#fff");
		}



MainWnd::~MainWnd()
		{
		}



void
MainWnd::onDraw()
		{
		FXWindowState	ws;
		FXRect			dr;
		SWString		text, s;

		getWindowState(ws);
		getClientRect(dr);

		text += s.format("WindowState: x=%d y=%d, w=%d, h=%d, flags=%x", ws.position.x, ws.position.y, ws.size.cx, ws.size.cy, ws.flags);
		text += "\n";
		text += s.format("Window: x=%d y=%d, w=%d, h=%d, decorated_w=%d, decorated_h=%d", x(), y(), w(), h(), decorated_w(), decorated_h());
		text += "\n";
		text += s.format("ClientRect: x=%d y=%d, w=%d, h=%d", dr.left, dr.top, dr.width(), dr.height());


		FXColorFont	font("Arial", 16.0, FXFont::Normal, "#000");

		fx_draw_text(text, dr, XD_CENTER|XD_MIDDLE|XD_SHRINKTOFIT, font);
		}


void
MainWnd::resize(int X, int Y, int W, int H)
		{
		FXWnd::resize(X, Y, W, H);
		invalidate();
		}


void
MainWnd::onClose()
		{
		FXWindowState	ws;
		SWString		filename=SWFilename::concatPaths(theAppConfig.getUserDataDir(), "TestWindowPosition.state");

		SW_TRACE("saveWindowState to %s\n", filename.c_str());
		getWindowState(ws);
		ws.saveToFile(filename);
		}


void
MainWnd::restoreState()
		{
		FXWindowState	ws;
		SWString		filename=SWFilename::concatPaths(theAppConfig.getUserDataDir(), "TestWindowPosition.state");

		SW_TRACE("restoreState from %s\n", filename.c_str());
		ws.loadFromFile(filename);

		// wait_for_expose();
		setWindowState(ws);
		}
