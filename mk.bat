@echo off

setlocal

rem Set to build release version for x64 and x86
set SW_BUILD_RELEASE_X86=Release x86
set SW_BUILD_RELEASE_X64=Release x64
set SW_BUILD_DEBUG_X86=
set SW_BUILD_DEBUG_X64=

..\build\scripts\mk %*
