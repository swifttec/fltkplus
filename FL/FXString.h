/**
*** @file		FXString.h
*** @brief		Defines a class for a simple single-byte char string.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the definition of the FXString class.
***
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#ifndef __FXString_h__
#define __FXString_h__

#include <FL/Fl.H>

/**
*** @brief	A simple single-byte character string
***
*** This string class is adapted from SWSimpleCString for fltk
*** which allows the string to be NULL or to have contents
**/
class FLTK_DLL_EXPORT FXString
		{
public:
		/**
		*** @brief	Default constructor
		***
		*** Constructs a blank string.
		**/
		FXString();

		/**
		*** @brief	Copy single-byte char string constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		FXString(const char *sp);
		
#ifdef FXString_Support_wchar_t
		/**
		*** @brief	Copy single-byte char string constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		FXString(const wchar_t *sp);
#endif // FXString_Support_wchar_t
		
		/**
		*** @brief	Copy constructor
		***
		*** Constructs a string object from the supplied single-byte char string.
		**/
		FXString(const FXString &sp);

		/**
		*** @brief	Destructor
		**/
		~FXString();

		/**
		*** @brief	Assignment operator(single-byte char string)
		**/
		FXString &    operator=(const char *s);

#ifdef FXString_Support_wchar_t
		/**
		*** @brief	Assignment operator(single-byte char string)
		**/
		FXString &    operator=(const wchar_t *s);
#endif // FXString_Support_wchar_t

		/**
		*** @brief	Assignment operator(FXString)
		**/
		FXString &    operator=(const FXString &s);


		/**
		*** @brief	Equality operator(single-byte char string)
		**/
		bool				 operator==(const char *s)		{ return (strcmp(m_sp, s) == 0); }

		/**
		*** @brief	Equality operator(FXString)
		**/
		bool				 operator==(const FXString &s)	{ return (strcmp(m_sp, s.m_sp) == 0); }

		/**
		*** @brief	Cast to single-byte char string.
		**/
		operator			const char *() const				{ return m_sp; }

		/**
		*** @brief	Return the length of the string.
		**/
		int					length() const;

		/**
		*** @brief	Return the string character size.
		**/
		sw_size_t			charSize() const					{ return sizeof(*m_sp); }

public:
		static char *	strnew(const char *s);

#ifdef FXString_Support_wchar_t
		static char *	strnew(const wchar_t *s);
#endif // FXString_Support_wchar_t

private:
		void	update();

private:
		char	*m_sp;	/// Points to the actual string data
		int		m_len;
		};





#endif // __FXString_h__
