#include "stdafx.h"

//
// "$Id: Fl_Window_State.cxx 10405 2014-10-29 15:53:52Z manolo $"
//
// Window widget class for the Fast Light Tool Kit (FLTK).
//
// Copyright 1998-2010 by Bill Spitzak and others.
//
// This library is free software. Distribution and use rights are outlined in
// the file "COPYING" which should have been included with this file.  If this
// file is missing or damaged, see the license at:
//
//     http://www.fltk.org/COPYING.php
//
// Please report all bugs and problems on the following page:
//
//     http://www.fltk.org/str.php
//

#include "stdafx.h"

// The Fl_Window_State is a window in the fltk library.
// This is the system-independent portions.  The huge amount of 
// crap you need to do to communicate with X is in Fl_x.cxx, the
// equivalent (but totally different) crap for MSWindows is in Fl_win32.cxx
#include <FL/config.h>
#include <FL/Fl.H>
#include <FL/x.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_Window_State.H>

Fl_Window_State::Fl_Window_State() :
	xpos(0),
	ypos(0),
	width(0),
	height(0),
	flags(0)
		{
		}

//
// End of "$Id: Fl_Window_State.cxx 10405 2014-10-29 15:53:52Z manolo $".
//
