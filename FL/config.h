#ifndef __FL_config_h__
#define __FL_config_h__

#include <xplatform/sw_platform.h>

#if defined(SW_PLATFORM_WINDOWS)
	#include <FL/config-windows.h>
#else
	#if SW_BUILD_OSID == SW_OSID_LINUX_RASPBIAN
		#include <FL/config-raspbian.h>
	#else
		#include <FL/config-linux.h>
	#endif
#endif

#endif // __FL_config_h__
