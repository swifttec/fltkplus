#############################################################################
## Makefile for the library
##
## LIBNAME	The base name of the library being built (e.g. FltkExt)
## SOURCES	The source files needed to build the library
#############################################################################

PROJROOT = ..

include $(PROJROOT)/Makefile.proj

# Read list of sources from Unix/Win32 common file
LIBTYPE			= dynamic
#LIBNAME			= $(shell basename $(CURDIR))
#LIBNAME_UPPER	= $(shell basename $(CURDIR) | tr 'a-z' 'A-Z')
LIBNAME			= fltk
LIBNAME_UPPER	= FLTK
LIBVERSION		= $(BUILD_MAJOR_VERSION).$(BUILD_MINOR_VERSION).$(BUILD_REVISION)
EXTRALIBS		= -L../lib -L../../SwiftLibs/lib -lswift -lxplatform `$(MASTERROOT)/FltkPlus/fltk-config --ldflags` -lpng -ljpeg -lzlib

ifeq ($(GENERIC_PLATFORM),Darwin)
EXTRALIBS		+= -framework Cocoa
endif

# Force recreate of Makefile.vcxfiles
dummy	:= $(shell perl $(MASTERROOT)/build/scripts/parsevcxproj.pl fltkdll.vcxproj Makefile.vcxfiles)
include Makefile.vcxfiles

# Must come after the vcxfiles include
HEADERS			= $(HFILES) $(CLASSES:.cpp=.h) $(FUNCTIONS:.c=.h)
CSOURCES		= $(CFILES) $(FUNCTIONS)
CPPSOURCES		= $(CPPFILES) $(CLASSES)
TARGETDIR		= ../lib
TARGET			= $(TARGETDIR)/lib$(LIBNAME)$(LIBTYPE_SUFFIX)$(SHARED_LIB_EXT)
OBJS			= $(CPPSOURCES:%.cpp=$(OBJDIR)/%.o) $(CSOURCES:%.c=$(OBJDIR)/%.o)
DEFINES			= -I.. -I../../SwiftLibs -D$(LIBNAME_UPPER)_BUILD_DLL

ifeq ($(OSNAME),Raspbian)
DEFINES			+= -I/usr/include/freetype2
endif

CFLAGS			+= $(CFLAGS_SHARED) $(DEFINES)
LDFLAGS			+= $(LDFLAGS_SHARED)


all: includes $(TARGET)

debug:
	@$(MAKE) BUILDTYPE=debug

check:
	@echo OSNAME = $(OSNAME)

check2:
	@echo TARGET = $(TARGET)
	@echo HEADERS = $(HEADERS)
	@echo CPPSOURCES = $(CPPSOURCES)
	@echo CFILES = $(CFILES)
	@echo FUNCTIONS = $(FUNCTIONS)
	@echo CSOURCES = $(CSOURCES)
	@echo OBJS = $(OBJS)
	@echo LDFLAGS = $(LDFLAGS)
	@echo EXTRALIBS = $(EXTRALIBS)

includes: buildinfo.h

buildinfo.h: build.info
	$(SILENT)echo Creating buildinfo.h
	$(GETBUILDINFO) --readonly --out-prefix $(LIBNAME_UPPER)_ --out C:buildinfo.h


clean:
	$(SILENT)rm -rf $(OBJS) $(LIB) $(PLATFORM)

$(TARGET): $(OBJS)
	@echo Building $(TARGET)
	$(SILENT)rm -f $(TARGET)
	$(SILENT)$(MKDIR) $(TARGETDIR)
	$(SILENT)$(LD) $(LDFLAGS) -o $(TARGET) $(OBJS) $(EXTRALIBS)
