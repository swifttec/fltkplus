/**
*** @file		FXString.cpp
*** @brief		Implements a class for a simple single-byte char string.
*** @version	1.0
*** @author		Simon Sparkes
***
*** Contains the implementation of the FXString class.
**/

/*********************************************************************************
Copyright (c) 2006-2018 Simon Sparkes T/A SwiftTec

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

*** THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*********************************************************************************/



#include "stdafx.h"

#include <FL/FXString.h>

char *
FXString::strnew(const char *s)
		{
		char		*sp=NULL;

		if (s != NULL)
			{
			sw_size_t	nchars;

			nchars = strlen(s) + 1;
			sp = new char[nchars];
			if (sp != NULL) strcpy(sp, s);
			}

		return sp;
		}

#ifdef FXString_Support_wchar_t
char *
FXString::strnew(const wchar_t *s)
		{
		char		*sp=NULL;

		if (s != NULL)
			{
			sw_size_t	slen, nlen;
		
			slen = wcslen(s);
			nlen = sw_wcstombs(NULL, s, slen+1, codepage);

			if (nlen != (sw_size_t)-1)
				{
				sp = new char[nlen+1];
				if (sp == NULL) throw 1;
				sw_wcstombs(sp, s, slen+1, codepage);
				sp[nlen] = 0;
				}
			else
				{
				sp = new char[1];
				*sp = 0;
				}
			}

		return sp;
		}
#endif // FXString_Support_wchar_t



FXString::FXString() :
	m_sp(0),
	m_len(0)
		{
		update();
		}


FXString::FXString(const char *sp) :
	m_sp(0),
	m_len(0)
		{
		m_sp = strnew(sp);
		update();
		}


#ifdef FXString_Support_wchar_t
FXString::FXString(const wchar_t *sp) :
	m_sp(0),
	m_len(0)
		{
		m_sp = strnew(sp);
		update();
		}
#endif // FXString_Support_wchar_t


FXString::FXString(const FXString &s) :
	m_sp(0),
	m_len(0)
		{
		m_sp = strnew(s.m_sp);
		update();
		}


FXString::~FXString()
		{
		delete [] m_sp;
		m_sp = NULL;
		m_len = 0;
		}


FXString &
FXString::operator=(const char *sp)
		{
		delete [] m_sp;
		m_sp = strnew(sp);
		update();
		return *this;
		}


#ifdef FXString_Support_wchar_t
FXString &
FXString::operator=(const wchar_t *sp)
		{
		delete [] m_sp;
		m_sp = strnew(sp);
		return *this;
		}
#endif // FXString_Support_wchar_t


FXString &
FXString::operator=(const FXString &s)
		{
		delete [] m_sp;
		m_sp = strnew(s.m_sp);
		update();
		return *this;
		}


void
FXString::update()
		{
		if (m_sp != NULL) m_len = (int)strlen(m_sp);
		else m_len = 0;
		}


int
FXString::length() const
		{
		return m_len;
		}





